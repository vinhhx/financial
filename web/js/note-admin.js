var object = $('.add-note');

jQuery(document).ready(function () {
    updateCountNotes('/admin/note/ajax-get-all-note');
});

function updateCountNotes(urlPost) {
    var tables = [];
    var ids = [];
    object.each(function (e) {
        var _this = $(this),
            table = _this.data('table'),
            id = _this.data('id');
        tables.push(table);
        ids.push(id);
    });
    tables = $.unique(tables);
    ids = $.unique(ids);
    if (tables.length > 0 && ids.length > 0) {
        $.post(urlPost, {
            tables: tables,
            ids: ids
        }, function (data) {
            object.each(function () {
                var _this = $(this),
                    table = _this.data('table'),
                    id = _this.data('id'),
                    key = table + '|' + id,
                    total = data[key];
                if ($.type(total) !== 'undefined') {
                    var badget = _this.find('span.label');
                    if (badget.length === 0) {
                        if (total > 0) {
                            _this.append(' <span class="label label-danger">' + total + '</span>');
                        } else {
                            _this.append(' <span></span>');
                        }
                    } else {
                        badget.html(total);
                    }
                }
            });
        });
    }
}

$('body').on('click', '.add-note', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var _this = jQuery(this);
    var _link = _this.attr("href");
    jQuery.post(_link, {table: _this.attr('data-table'), id: _this.attr('data-id'), type: 'saveForm'})
        .done(function (data) {
            jQuery("#pageModal .modal-body").html(data);
            jQuery("#pageModal").modal('show');
        })
    return false;
}).on('click', '.save-form-note', function (e) {
    var _form = jQuery('#form-add-note');
    var _action = _form.attr('action');
    _form.unbind('submit').submit(function (event) {
        $.ajax({
            url: _action,
            type: 'POST',
            dataType: 'json',
            async: false,
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.status === 'pass') {
                    jQuery("#pageModal").modal('hide');
                    alert('Send message success !');
                    _totalNote = parseInt($('.add-note[data-id="' + data.id + '"] span').text());
                    if (_totalNote > 0) {
                        $('.add-note[data-id="' + data.id + '"] span').text(_totalNote + 1).addClass('label label-danger');
                    } else {
                        $('.add-note[data-id="' + data.id + '"] span').text(1).addClass('label label-danger');
                    }
                } else {
                    var c = "";
                    for (var i in data) {
                        c += data[i] + "\\n";
                    }
                    alert("Error send message:\\n" + c);
                }
            }
        });
        return false;
    });
});
