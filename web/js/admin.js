/**
 * Created by vinhhx on 10-Aug-16.
 */

$(function () {
    $.fn.removeImage = function () {
        var _this = $(this);
        var _btnDelete = $('#action-delete-image', _this);
        _btnDelete.each(function () {
            $(this).on('click', function () {
                var _confirm = confirm('You sure want to delete this attachment ?');
                if (_confirm) {
                    var _parent = $(this).parent();
                    var _images = _parent.find('img.img-item');
                    if (_images.length) {
                        $.post('', {type: 'remove-image'}, function (data) {

                        }).done(function () {
                            location.reload();
                        });
                    }
                }


            })
        })
    }
}(document));
