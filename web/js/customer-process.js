var localRefresh = function () {
    document.location.href = window.location;
}
/*
 * plugin customer process
 * */
$(function () {
        $.fn.customerProcess = function (opt) {
            var defaultOption = {
                container: '.content',
                openModal: true,
                rootUrl: 'index',
                modalId: '#pageModal',
                modalContent: '#modalContent',
                withDrawnCommissionFee: 2.5
            }
            var _options = $.extend({}, defaultOption, opt);
            $(this).each(function () {
                    var container = $(this);
                    var modalId = _options.modalId;
                    var modalContent = _options.modalContent;
                    var openModal = function (html) {
                        //check modal nếu chưa open thì open còn ko thì append dữ liệu
                        if (html) {
                            $(modalId + " .modal-body").html(html);
                        }
                        $(modalId).modal({
                            show: true,
                            backdrop: 'static',
                            keyboard: false,
                        });
                    }
                    var closeModal = function (type) {
                        $(modalId).modal("hide");
                        type = typeof(type) ? type : true;
                        if (type) {
                            container.returnIndex();
                        }
                    }
                    container.setRequestPH = function () {
                        var html = '';
                        if ($('#has-ph').length) {
                            html = $('#has-ph').html();
                        }
                        openModal(html);
                    }
                    container.returnIndex = function () {
                        return document.location.href = _options.rootUrl;
                    }
                    container.cancelAction = function () {
                        var _btnCancel = $('.btn-submit-cancel', container);
                        _btnCancel.on('click', function () {
                            closeModal(true);
                        })
                    }
                    container.deleteAttachment = function () {
                        var _btnDeleteAttachment = $('.transaction-remove-attachment', container);
                        _btnDeleteAttachment.on('click', function (e) {
                            e.preventDefault();
                            var _confirm = confirm('You sure want to delete this attachment ?');
                            if (_confirm) {
                                var _parent = $(this).parent();
                                var _transactionId = $(this).attr('data-transaction-id');
                                var _images = _parent.find('img.img-item');
                                if (_images.length) {
                                    $.pjax({
                                        type: 'POST',
                                        url: _options.rootUrl,
                                        container: '#current-transactions',
                                        data: {type: 'remove-attachment', tid: _transactionId},
                                    }).done(function () {
                                        closeModal(false);

                                    }).fail(function () {
                                    });
                                }
                            }
                        });
                        container.executeTransaction();
                    };
                    container.uploadAttachment = function () {
                        var _formUpload = $('#upload-transaction-bill', container);
                        _formUpload.on('submit', function (e) {
                            e.preventDefault();
                            e.stopImmediatePropagation();
                            var _formData = new FormData(this);
                            _formData.append('type', 'upload');
                            $(this).find('#upload-bill').attr('disabled', 'disabled');
                            $(this).find('#upload-bill').html('processing ...');
                            $.pjax.submit(e, modalContent)
                                .done(function () {

                                })
                                .fail(function () {
                                });
                        });
                        container.executeTransaction();
                    }
                    container.approvedTransaction = function () {
                        var _btnApprovedTransaction = $('.btn-approved-transaction', container);
                        _btnApprovedTransaction.on('click', function (e) {
                            e.preventDefault();
                            var confirmation = confirm('Are you sure received enough BTC from other member ?');
                            if (confirmation) {
                                $(this).attr('disabled', 'disabled');
                                $(this).html('processing ...');
                                $.pjax({
                                    type: 'POST',
                                    url: _options.rootUrl,
                                    container: '#current-transactions',
                                    data: {type: 'approved', tid: $(this).attr('data-order-id')},
                                }).done(function () {
                                    closeModal(false);

                                }).fail(function () {
                                });
                            }
                        });
                        container.executeTransaction();
                    }
                    container.executeTransaction = function () {
                        var _btnViewSent = $('.open-modal-sent-view', container);
                        _btnViewSent.each(function () {
                            $(this).on('click', function (e) {
                                e.preventDefault();
                                $.pjax({
                                    type: 'POST',
                                    url: _options.rootUrl,
                                    container: modalContent,
                                    data: {type: 'view', tid: $(this).attr('data-order-id')},
                                    //dataType: 'JSON',
                                }).done(function () {
                                    openModal('');
                                    container.uploadAttachment();
                                    container.deleteAttachment();
                                    container.approvedTransaction();
                                }).fail(function () {
                                    closeModal();
                                });
                            })
                        })
                    }
                    container.submitCashOut = function () {
                        var _btnSubmitCashOut = $('#btn-submit-cash-out', container);
                        $(_btnSubmitCashOut).on('click', function (e) {
                            var _form = $('#customer-cash-out', container);
                            var _formData = _form.serializeArray();
                            _formData.push({name: 'type', value: 'cash-out'});
                            _btnSubmitCashOut.attr('disabled', 'disabled');
                            _btnSubmitCashOut.html('Processing ...');
                            $.pjax({
                                type: 'POST',
                                url: _options.rootUrl,
                                container: modalContent,
                                data: _formData,
                            }).done(function () {
                                openModal('');
                                container.submitCashOut();
                            }).fail(function () {

                            });
                        })
                    };
                    container.executeCashOut = function () {
                        var _btnRequestCashOut = $('#widthdraw-package', container);
                        _btnRequestCashOut.on('click', function (e) {
                            e.preventDefault();
                            $.pjax({
                                type: 'POST',
                                url: _options.rootUrl,
                                container: modalContent,
                                data: {type: 'open-request-widthdraw'},
                            }).done(function () {
                                openModal('');
                                container.submitCashOut();
                            }).fail(function () {

                            });

                        })
                    }
                    container.submitAction = function () {
                        var _btnSubmit = $('.btn-submit-question-add-request', container);
                        _btnSubmit.on('click', function (e) {
                            var _checkRequest = $('.check-request', container);
                            if (_checkRequest.is(':checked')) {
                                e.preventDefault();
                                $.pjax({
                                    type: 'POST',
                                    url: _options.rootUrl,
                                    container: modalContent,
                                    data: {type: 'open-package'},
                                    //dataType: 'JSON',
                                }).done(function () {
                                    container.cancelAction();
                                    container.submitAction();
                                }).fail(function () {
                                    closeModal();
                                });

                            }
                        });
                        var _btnCreatePackage = $('#btn-submit-create-package', container);
                        _btnCreatePackage.on('click', function (e) {
                            var _form = $('#create-new-package', container);
                            var _formData = _form.serializeArray();
                            _formData.push({name: 'type', value: 'create-package'});
                            _btnCreatePackage.attr('disabled', 'disabled');
                            _btnCreatePackage.html('Processing ...');

                            $.pjax({
                                type: 'POST',
                                url: _options.rootUrl,
                                container: modalContent,
                                data: _formData,
                                //dataType: 'JSON',
                            }).done(function () {
                            }).fail(function () {
                            });
                        });

                    }
                    container.CashOutCommission = function () {
                        var _formCommission = $('#customer-cash-out-commission', container);
                        _formCommission.on('beforeSubmit', function (e) {
                            e.preventDefault();
                            e.stopImmediatePropagation();
                            var _formData = $(this).serializeArray();
                            var _btnCommissionSubmit = $('#commission-submit-cash-out', container);
                            _btnCommissionSubmit.attr('disabled', 'disabled');
                            _btnCommissionSubmit.html('Processing ...');
                            $.pjax({
                                type: 'POST',
                                url: _options.rootUrl,
                                container: modalContent,
                                data: _formData,
                            }).done(function () {
                                openModal('');
                                container.CashOutCommission();
                            }).fail(function () {

                            });
                            return false;
                        })

                    }
                    container.executeCommission = function () {
                        var _btnOpenFormCommissionCashOut = $('#my-poundage-balance', container);
                        _btnOpenFormCommissionCashOut.on('click', function (e) {
                            e.preventDefault();
                            $.pjax({
                                type: 'POST',
                                url: _options.rootUrl,
                                container: modalContent,
                                data: {type: 'open-commission-cash-out'},
                                //dataType: 'JSON',
                            }).done(function () {
                                openModal('');
                                container.cancelAction();
                                container.CashOutCommission();
                                container.InputValueCashOutCommission();
                            }).fail(function () {
                                closeModal();
                            });

                        });

                    }
                    container.InputValueCashOutCommission = function () {
                        var _inputValueComission = $('#cashoutcommissionform-amount', container);
                        var _result = $(_inputValueComission.attr('data-result'));
                        _inputValueComission.keyup(function (event) {
                            var _inputValue =  parseFloat(_inputValueComission.val());
                            _inputValue=_inputValue.toFixed(8);
                            if (_inputValue <= 0 || _inputValue == 'undefined') {
                                _result.text(0);
                            } else {
                                var newValue = _inputValue - ((_inputValue / 100) * _options.withDrawnCommissionFee);
                                _result.text(newValue.toFixed(8))
                            }

                        }).keydown(function (event) {
                            if (event.which == 13) {
                                return true;
                            }
                        });
                    }

                    var _btnCreatePackage = $('#create-package', container);

                    /**
                     * Event
                     */

                    _btnCreatePackage[0].addEventListener('click', function () {
                        container.setRequestPH();
                        container.cancelAction();
                        container.submitAction();
                    });

                    /**
                     * default action
                     */
                    container.executeTransaction();
                    container.executeCashOut();
                    container.executeCommission();
                    $('.modal').on('hidden.bs.modal', function (e) {
                        closeModal(true);
                    })

                }
            );


        }
    }(jQuery)
)
;
