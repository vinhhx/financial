<?php

use yii\db\Migration;

class m160630_232808_add_column_balance_profit extends Migration
{
    public function up()
    {
        $this->addColumn('invest_customer_package', 'current_profit', \yii\db\Schema::TYPE_DECIMAL . "(10,4) NOT NULL DEFAULT 0 AFTER status");
        $this->addColumn('invest_customer_package', 'total_current_profit', \yii\db\Schema::TYPE_DECIMAL . "(10,4) NOT NULL DEFAULT 0 AFTER status");

    }

    public function down()
    {
        echo "m160630_232808_add_column_balance_profit cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
