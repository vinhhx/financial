<?php

use yii\db\Schema;
use yii\db\Migration;

class m160214_080914_add_column_to_requirecashout extends Migration
{
    public function up()
    {
        $this->addColumn('{{%customer_require_cashout}}','type',Schema::TYPE_SMALLINT."(2) DEFAULT 1 AFTER status");
        $this->addColumn('{{%customer_require_cashout}}','admin_created_by',Schema::TYPE_INTEGER." AFTER type");

    }

    public function down()
    {
        echo "m160214_080914_add_column_to_requirecacshout cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
