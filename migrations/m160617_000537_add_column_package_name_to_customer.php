<?php

use yii\db\Migration;

/**
 * Handles adding column_package_name to table `customer`.
 */
class m160617_000537_add_column_package_name_to_customer extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customer_package', 'package_name', \yii\db\Schema::TYPE_STRING . " AFTER package_id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
