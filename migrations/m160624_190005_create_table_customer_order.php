<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_customer_order`.
 */
class m160624_190005_create_table_customer_order extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        $this->createTable('customer_order', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unsigned(),
            'customer_username' => $this->string()->notNull(),
            'quantity' => $this->integer()->notNull()->unsigned(),
            'status' => $this->smallInteger(2)->notNull()->defaultValue(0)->unsigned(),
            'type' => $this->smallInteger(2)->notNull()->defaultValue(1)->unsigned(),
            'attachment' => $this->string(),
            'upload_attachment_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
            'approved_by' => $this->integer()->unsigned(),
            'approved_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
            'created_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
            'updated_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('table_customer_order');
    }
}
