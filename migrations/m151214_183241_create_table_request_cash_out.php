<?php

use yii\db\Schema;
use yii\db\Migration;

class m151214_183241_create_table_request_cash_out extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1';
        }

        $this->createTable('{{%customer_require_cashout}}', [
            'id' => Schema::TYPE_PK,
            'customer_id' => Schema::TYPE_INTEGER . ' UNSIGNED DEFAULT 0',
            'customer_username' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'amount' => Schema::TYPE_INTEGER . " NOT NULL DEFAULT 0",
            'status' => Schema::TYPE_SMALLINT . "(2) NOT NULL DEFAULT 1",
            'approved_by' => Schema::TYPE_STRING,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);
    }

    public function down()
    {
        echo "m151214_183241_create_table_request_cash_out cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
