<?php

use yii\db\Migration;

/**
 * Handles adding column_request_id to table `transaction`.
 */
class m160615_014059_add_column_request_id_to_transaction extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customer_transaction', 'request_id', \yii\db\Schema::TYPE_INTEGER . " NOT NULL AFTER id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
