<?php

use yii\db\Migration;

/**
 * Handles adding column_type_step to table `transaction_queue`.
 */
class m160615_022613_add_column_type_step_to_transaction_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customer_transaction_queue', 'type_step', \yii\db\Schema::TYPE_SMALLINT . "(2) NOT NULL AFTER type");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
