<?php

use yii\db\Migration;

class m161120_091920_add_column_strong_leg_balance_to_invest_customer_package extends Migration
{
    public function up()
    {
        $this->addColumn('invest_customer_package', 'strong_leg_balance', $this->decimal(16, 8)->notNull()->defaultValue(0)->unsigned());

    }

    public function down()
    {
        echo "m161120_091920_add_column_strong_leg_balance_to_invest_customer_package cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
