<?php

use yii\db\Migration;

class m160717_001959_alter_column_content_customer_token_transaction extends Migration
{
    public function up()
    {
        $this->alterColumn('customer_token_transaction', 'content', $this->text());
    }

    public function down()
    {
        echo "m160717_001959_alter_column_content_customer_token_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
