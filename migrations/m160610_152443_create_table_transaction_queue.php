<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_transaction_queue`.
 */
class m160610_152443_create_table_transaction_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        $this->createTable('customer_request', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unsigned(),
            'type' => $this->smallInteger(2)->notNull()->unsigned(),
            'status' => $this->smallInteger(2)->notNull()->unsigned(),
            'amount' => $this->decimal(10, 4)->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'package_id' => $this->integer(),
            'updated_at' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);

        $this->createTable('customer_transaction_queue', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unsigned(),
            'customer_request_id' => $this->integer()->unsigned(),
            'type' => $this->smallInteger(2)->notNull()->defaultValue(0),
            'status' => $this->smallInteger(2)->notNull()->defaultValue(1),
            'amount' => $this->decimal(10, 4)->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('table_transaction_queue');
    }
}
