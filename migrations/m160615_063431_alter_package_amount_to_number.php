<?php

use yii\db\Migration;

class m160615_063431_alter_package_amount_to_number extends Migration
{
    public function up()
    {
        $this->alterColumn('package', 'amount', \yii\db\Schema::TYPE_DECIMAL . "(10,4) NOT NULL DEFAULT 0");

    }

    public function down()
    {
        echo "m160615_063431_alter_package_amount_to_number cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
