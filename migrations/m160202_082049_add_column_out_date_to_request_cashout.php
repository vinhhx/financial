<?php

use yii\db\Schema;
use yii\db\Migration;

class m160202_082049_add_column_out_date_to_request_cashout extends Migration
{
    public function up()
    {
        $this->addColumn('{{%customer_require_cashout}}', 'out_of_date', Schema::TYPE_DATE);
        $this->addColumn('{{%customer}}', 'user_id_first_branch', Schema::TYPE_INTEGER." AFTER parent_id");

    }

    public function down()
    {
        echo "m160202_082049_add_column_out_date_to_request_cashout cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
