<?php

use yii\db\Migration;

class m160726_075012_add_column_package_config_to_package extends Migration
{
    public function up()
    {
        $this->addColumn('package', 'config', $this->text()->after('status'));
    }

    public function down()
    {
        echo "m160726_075012_add_column_package_config_to_package cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
