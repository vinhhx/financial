<?php

use yii\db\Migration;

/**
 * Handles adding column_bill_id to table `pack_his`.
 */
class m160710_151948_add_column_bill_id_to_pack_his extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('invest_customer_package_history', 'bill_id', \yii\db\Schema::TYPE_INTEGER." AFTER amount");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
