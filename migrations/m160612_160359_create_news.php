<?php

use yii\db\Migration;

/**
 * Handles the creation for table `news`.
 */
class m160612_160359_create_news extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(),
            'news_category_id' => $this->integer(),
            'title' => $this->string(255)->notNull(),
            'alias' => $this->string(255),
            'intro' => $this->text(),
            'content' => $this->text(),
            'image' => $this->string(255),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'deleted' => $this->smallInteger(1)->defaultValue(0),
            'created_at' => $this->integer(10),
            'updated_at' => $this->integer(10)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news');
    }
}
