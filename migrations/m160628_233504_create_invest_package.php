<?php

use yii\db\Migration;

/**
 * Handles the creation for table `invest_package`.
 */
class m160628_233504_create_invest_package extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        $this->createTable('invest_package', [
            'id' => $this->primaryKey(),
            'package_name' => $this->string()->notNull(),
            'amount_min' => $this->decimal(10, 4)->notNull()->defaultValue(0),
            'amount_max' => $this->decimal(10, 4)->notNull()->defaultValue(0),
            'increase_day' => $this->decimal(10, 4)->notNull()->defaultValue(0),
            'increase_month' => $this->decimal(10, 4)->notNull()->defaultValue(0),
            'status' => $this->smallInteger(2)->notNull()->defaultValue(1)->unsigned(),
            'created_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
            'updated_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
        ],$tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('invest_package');
    }
}
