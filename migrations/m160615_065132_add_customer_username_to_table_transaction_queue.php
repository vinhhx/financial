<?php

use yii\db\Migration;

/**
 * Handles adding customer_username to table `table_transaction_queue`.
 */
class m160615_065132_add_customer_username_to_table_transaction_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customer_transaction_queue', 'customer_username', \yii\db\Schema::TYPE_STRING . " NOT NULL AFTER customer_id");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
