<?php

use yii\db\Schema;
use yii\db\Migration;

class m151212_022108_add_feild_parent_id_to_customer extends Migration
{
    public function up()
    {
        $this->addColumn('{{%customer}}','parent_id',Schema::TYPE_INTEGER." AFTER package_id");

    }

    public function down()
    {
        echo "m151212_022108_add_feild_parent_id_to_customer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
