<?php

use yii\db\Migration;
use \yii\db\Schema;

class m160615_051749_alter_customer_transaction extends Migration
{
    public function up()
    {
        $this->renameColumn('customer_transaction', 'request_id', 'customer_sent_request_id');
        $this->renameColumn('customer_transaction', 'queue_id', 'customer_queue_id');
        $this->renameColumn('customer_transaction', 'customer_sent_bitcoin_wallet', 'customer_sent_bank_address');
        $this->renameColumn('customer_transaction', 'customer_receiver_bitcoin_wallet', 'customer_receiver_bank_address');

        $this->addColumn('customer_transaction', 'customer_receiver_request_id',Schema::TYPE_INTEGER." NOT NULL AFTER amount");
        $this->addColumn('customer_transaction', 'customer_receiver_queue_id',Schema::TYPE_INTEGER." NOT NULL AFTER customer_receiver_request_id");

    }

    public function down()
    {
        echo "m160615_051749_alter cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
