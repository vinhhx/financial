<?php

use yii\db\Migration;
use yii\db\Schema;

class m160607_020822_modify_datetime_to_customer extends Migration
{
    public function up()
    {
        $this->alterColumn('customer', 'date_join', Schema::TYPE_DATETIME);
        $this->alterColumn('customer', 'last_auto_increase', Schema::TYPE_DATETIME);
        $this->alterColumn('customer', 'status', Schema::TYPE_SMALLINT . "(2) DEFAULT 0");

    }

    public function down()
    {
        echo "m160607_020822_modify_datetime_to_customer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
