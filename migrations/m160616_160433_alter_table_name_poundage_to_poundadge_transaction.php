<?php

use yii\db\Migration;
use yii\db\Schema;

class m160616_160433_alter_table_name_poundage_to_poundadge_transaction extends Migration
{
    public function up()
    {
        $this->addColumn('customer_poundage', 'bill_id', Schema::TYPE_INTEGER . " NULL DEFAULT 0 AFTER id"); // Mã phiếu + hoặc trừ
        $this->addColumn('customer_poundage', 'sign', Schema::TYPE_SMALLINT . " NOT NULL DEFAULT 1 AFTER bill_id");
        $this->addColumn('customer_poundage', 'balance_before', Schema::TYPE_SMALLINT . " NOT NULL DEFAULT 1 AFTER amount");
        $this->addColumn('customer_poundage', 'balance_after', Schema::TYPE_SMALLINT . " NOT NULL DEFAULT 1 AFTER balance_before");
        $this->alterColumn('customer_poundage','from_id',Schema::TYPE_INTEGER." NULL DEFAULT 0");
        $this->alterColumn('customer_poundage','from_username',Schema::TYPE_STRING." NULL");
        $this->renameTable('customer_poundage', 'customer_poundage_transaction');

    }

    public function down()
    {
        echo "m160616_160433_alter_table_name_poundage_to_poundadge_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
