<?php

use yii\db\Migration;

/**
 * Handles adding column_is_investment to table `table_customer`.
 */
class m160620_223856_add_column_is_investment_to_table_customer extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customer','is_invest',\yii\db\Schema::TYPE_SMALLINT."(2) NOT NULL DEFAULT 0");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
