<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles adding date_start to table `queue`.
 */
class m160611_004529_add_date_start_to_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customer_transaction_queue', 'start_at', Schema::TYPE_INTEGER . " NOT NULL AFTER status");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
