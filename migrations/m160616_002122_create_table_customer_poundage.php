<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_customer_poundage`.
 */
class m160616_002122_create_table_customer_poundage extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        $this->createTable('customer_poundage', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unsigned(),
            'customer_username' => $this->string()->notNull(),
            'from_id' => $this->integer()->notNull()->unsigned(),
            'from_username' => $this->string()->notNull(),
            'amount' => $this->decimal(10, 4)->notNull()->defaultValue(0)->unsigned(),
            'level' => $this->smallInteger(4)->notNull()->defaultValue(0)->unsigned(),
            'type' => $this->smallInteger(2)->defaultValue(0)->unsigned(),
            'created_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
            'updated_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('table_customer_poundage');
    }
}
