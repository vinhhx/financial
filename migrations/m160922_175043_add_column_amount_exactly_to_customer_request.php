<?php

use yii\db\Migration;

class m160922_175043_add_column_amount_exactly_to_customer_request extends Migration
{
    public function up()
    {
        $this->addColumn('customer_request', 'amount_exactly', $this->decimal(16, 8)->notNull()->defaultValue(0)->unsigned()->after('amount'));

    }

    public function down()
    {
        echo "m160922_175043_add_column_amount_exactly_to_customer_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
