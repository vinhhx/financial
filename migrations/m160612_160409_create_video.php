<?php

use yii\db\Migration;

/**
 * Handles the creation for table `video`.
 */
class m160612_160409_create_video extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('video', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'alias' => $this->string(255),
            'video_url' => $this->string(255)->notNull(),
            'video_id' => $this->string(25),
            'use_thumb' => $this->smallInteger(1)->defaultValue(0),
            'video_thumb' => $this->string(255),
            'image' => $this->string(255),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'deleted' => $this->smallInteger(1)->defaultValue(0),
            'created_at' => $this->integer(10),
            'updated_at' => $this->integer(10)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('video');
    }
}
