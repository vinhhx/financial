<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_wallet`.
 */
class m160531_023640_create_table_wallet extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1';
        }
        $this->createTable('customer_wallet', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unsigned()->unique(),
            'balance' => $this->decimal(10, 4)->unsigned()->notNull()->defaultValue(0),
            'poundage' => $this->decimal(10, 4)->unsigned()->notNull()->defaultValue(0),
            'type' => $this->smallInteger(4)->unsigned()->notNull()->defaultValue(10),
            'status' => $this->smallInteger(2)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->unsigned()->notNull()->defaultValue(0)
        ], $tableOptions);

        $this->createTable('customer_package', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unsigned(),
            'package_id' => $this->integer()->notNull()->unsigned(),
            'status' => $this->smallInteger(2)->notNull()->unsigned()->defaultValue(1),
            'is_active' => $this->smallInteger(2)->notNull()->unsigned()->defaultValue(10),
            'type' => $this->smallInteger(2)->notNull()->unsigned()->defaultValue(0),
            'created_at' => $this->integer()->notNull()->unsigned()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->unsigned()->defaultValue(0),
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('table_wallet');
    }
}
