<?php

use yii\db\Migration;

/**
 * Handles adding colum_re_invesment_date_poundage to table `table_invest_customer_package`.
 */
class m160702_040556_add_colum_re_invesment_date_poundage_to_table_invest_customer_package extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('invest_customer_package', 'current_poundage', \yii\db\Schema::TYPE_DECIMAL . "(10,4) NOT NULL DEFAULT 0 after last_profit_receiver");
        $this->addColumn('invest_customer_package', 'last_cash_out_poundage', \yii\db\Schema::TYPE_INTEGER . " NOT NULL DEFAULT 0 after current_poundage");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
