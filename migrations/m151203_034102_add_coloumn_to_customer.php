<?php

use yii\db\Schema;
use yii\db\Migration;

class m151203_034102_add_coloumn_to_customer extends Migration
{
    public function up()
    {
        $this->addColumn('{{%customer}}','date_join',Schema::TYPE_DATE." AFTER status");
        $this->addColumn('{{%customer}}','dob',Schema::TYPE_DATE." AFTER sex");
    }

    public function down()
    {
        echo "m151203_034102_add_coloumn_to_customer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
