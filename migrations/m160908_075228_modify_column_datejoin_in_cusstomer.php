<?php

use yii\db\Migration;

class m160908_075228_modify_column_datejoin_in_cusstomer extends Migration
{
    public function up()
    {
        $this->alterColumn('customer','date_join',$this->integer()->unsigned()->defaultValue(0));
        $this->alterColumn('invest_customer','date_join',$this->integer()->unsigned()->defaultValue(0));

    }

    public function down()
    {
        echo "m160908_075228_modify_column_datejoin_in_cusstomer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
