<?php

use yii\db\Migration;

/**
 * Handles the creation for table `note`.
 */
class m160625_024805_create_note extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('note', [
            'id' => $this->primaryKey(),
            'object_id' => $this->integer(),
            'object_table' => $this->string(100),
            'content' => $this->string(),
            'type' => $this->smallInteger(2)->defaultValue(1),
            'created_id' => $this->integer(),
            'created_by' => $this->string(100),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('note');
    }
}
