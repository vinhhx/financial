<?php

use yii\db\Migration;

class m160629_211047_addColumn_bill_id_to_transaction extends Migration
{
    public function up()
    {
        $this->addColumn('invest_customer_transaction', 'bill_id', \yii\db\Schema::TYPE_INTEGER . " NOT NULL DEFAULT 0 AFTER customer_username");
    }

    public function down()
    {
        echo "m160629_211047_addColumn_bill_id_to_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
