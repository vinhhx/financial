<?php

use yii\db\Migration;

/**
 * Handles the creation for table `content`.
 */
class m160614_162418_create_content extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('content', [
            'id' => $this->primaryKey(),
            'type_id' => $this->smallInteger(2)->notNull(),
            'name' => $this->string(255)->notNull(),
            'alias' => $this->string(255),
            'content' => $this->text(),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'deleted' => $this->smallInteger(1)->defaultValue(0),
            'config' => $this->text(),
            'created_at' => $this->integer(10),
            'updated_at' => $this->integer(10)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('content');
    }
}
