<?php

use yii\db\Migration;

/**
 * Handles adding column_balance to table `customer_package`.
 */
class m160615_144215_add_column_balance_to_customer_package extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customer_package', 'balance', \yii\db\Schema::TYPE_DECIMAL . "(10,4) NOT NULL DEFAULT 0");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
