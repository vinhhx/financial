<?php

use yii\db\Migration;

class m160618_074945_modify_balance extends Migration
{
    public function up()
    {
        $this->alterColumn('customer_poundage_transaction','balance_before',$this->decimal(10,4)->defaultValue(0));
        $this->alterColumn('customer_poundage_transaction','balance_after',$this->decimal(10,4)->defaultValue(0));
    }

    public function down()
    {
        echo "m160618_074945_modify_balance cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
