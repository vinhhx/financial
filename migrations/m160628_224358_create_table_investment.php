<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation for table `table_investment`.
 */
class m160628_224358_create_table_investment extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        $this->createTable('invest_customer_case', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unsigned(),
            'parent_id' => $this->integer()->notNull()->unsigned(),
            'level' => $this->smallInteger(4)->notNull()->unsigned(),
            'position' => $this->smallInteger(2)->notNull()->unsigned()->defaultValue(1),
            'status' => $this->smallInteger(2)->notNull()->unsigned()->defaultValue(1),
            'created_at' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->unsigned()->notNull()->defaultValue(0)
        ], $tableOptions);

        $this->createTable('invest_customer_wallet', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unsigned()->unique(),
            'balance' => $this->decimal(10, 4)->unsigned()->notNull()->defaultValue(0),
            'poundage' => $this->decimal(10, 4)->unsigned()->notNull()->defaultValue(0),
            'type' => $this->smallInteger(4)->unsigned()->notNull()->defaultValue(10),
            'status' => $this->smallInteger(2)->unsigned()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->unsigned()->notNull()->defaultValue(0)
        ], $tableOptions);

        $this->createTable('invest_customer_package', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unsigned(),
            'package_id' => $this->integer()->notNull()->unsigned(),
            'amount' => $this->decimal(10, 4)->notNull()->unsigned()->defaultValue(0),
            'status' => $this->smallInteger(2)->notNull()->unsigned()->defaultValue(1),
            'is_active' => $this->smallInteger(2)->notNull()->unsigned()->defaultValue(10),
            'type' => $this->smallInteger(2)->notNull()->unsigned()->defaultValue(0),
            'last_cash_out_at' => $this->integer()->notNull()->unsigned()->defaultValue(0),
            'joined_at' => $this->integer()->notNull()->unsigned()->defaultValue(0),
            'created_at' => $this->integer()->notNull()->unsigned()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->unsigned()->defaultValue(0),
        ], $tableOptions);
        $this->createTable('{{%invest_customer_request}}', [
            'id' => Schema::TYPE_PK,
            'customer_id' => Schema::TYPE_INTEGER . ' UNSIGNED DEFAULT 0',
            'customer_username' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'amount' => Schema::TYPE_DECIMAL . "(10,4) NOT NULL DEFAULT 0",
            'bank_address' => Schema::TYPE_STRING . " NOT NULL ",
            'status' => Schema::TYPE_SMALLINT . "(2) NOT NULL DEFAULT 1",
            'type' => Schema::TYPE_SMALLINT . "(2) NOT NULL DEFAULT 1",
            'transaction_code' => Schema::TYPE_STRING,
            'approved_by' => Schema::TYPE_STRING,
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);

        $this->createTable('{{%invest_customer_transaction}}', [
            'id' => Schema::TYPE_PK,
            'customer_id' => Schema::TYPE_INTEGER . ' UNSIGNED DEFAULT 0',
            'customer_username' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'amount' => Schema::TYPE_DECIMAL . "(10,4) NOT NULL DEFAULT 0",
            'sign' => Schema::TYPE_SMALLINT . "(2) NOT NULL DEFAULT 1",
            'type' => Schema::TYPE_SMALLINT . "(2) NOT NULL DEFAULT 1",
            'balance_before' => Schema::TYPE_DECIMAL . "(10,4) NOT NULL DEFAULT 0",
            'balance_after' => Schema::TYPE_DECIMAL . "(10,4) NOT NULL DEFAULT 0",
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('table_investment');
    }
}
