<?php

use yii\db\Migration;

/**
 * Handles adding status to table `customer_type_poundage`.
 */
class m160620_194656_add_status_to_customer_type_poundage extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customer_poundage_transaction', 'status', \yii\db\Schema::TYPE_SMALLINT . "(2) NOT NULL DEFAULT 1 AFTER type ");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
