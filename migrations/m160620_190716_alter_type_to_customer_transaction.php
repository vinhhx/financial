<?php

use yii\db\Migration;

class m160620_190716_alter_type_to_customer_transaction extends Migration
{
    public function up()
    {
        $this->renameColumn('customer_transaction','type','customer_sent_type');
        $this->addColumn('customer_transaction','customer_receiver_type',\yii\db\Schema::TYPE_SMALLINT."(2) NOT NULL DEFAULT 0 AFTER customer_sent_type");
    }

    public function down()
    {
        echo "m160620_190716_alter_type_to_customer_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
