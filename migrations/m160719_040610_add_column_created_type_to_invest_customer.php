<?php

use yii\db\Migration;

class m160719_040610_add_column_created_type_to_invest_customer extends Migration
{
    public function up()
    {
        $this->addColumn('invest_customer', 'created_type', $this->smallInteger()->notNull()->defaultValue(0)->after('is_deleted'));
        $this->addColumn('invest_customer', 'created_by', $this->string()->after('created_type'));
    }

    public function down()
    {
        echo "m160719_040610_add_column_created_type_to_invest_customer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
