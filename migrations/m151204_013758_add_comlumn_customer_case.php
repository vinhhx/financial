<?php

use yii\db\Schema;
use yii\db\Migration;

class m151204_013758_add_comlumn_customer_case extends Migration
{
    public function up()
    {
        $this->addColumn('{{%customer_case}}','level',Schema::TYPE_SMALLINT."(2) DEFAULT 0 after parent_id");
        $this->addColumn('{{%customer}}','created_type',Schema::TYPE_SMALLINT."(2) AFTER status");
        $this->addColumn('{{%customer}}','created_by',Schema::TYPE_STRING."(255) AFTER created_type");

    }

    public function down()
    {
        echo "m151204_013758_add_comlumn_customer_case cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
