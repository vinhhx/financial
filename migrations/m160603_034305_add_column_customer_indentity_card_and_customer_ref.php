<?php

use yii\db\Migration;
use yii\db\Schema;

class m160603_034305_add_column_customer_indentity_card_and_customer_ref extends Migration
{
    public function up()
    {
        $this->addColumn('customer', 'identity_card', Schema::TYPE_STRING . "(20) NOT NULL  DEFAULT '' AFTER phone ");
        $this->addColumn('customer', 'ref_code', Schema::TYPE_STRING . "(255) NOT NULL unique DEFAULT '' after username");
    }

    public function down()
    {
        echo "m160603_034305_add_column_customer_indentity_card_and_customer_ref cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
