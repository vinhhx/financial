<?php

use yii\db\Migration;
use yii\db\Schema;

class m160226_024541_create_table_customer_loan extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1';
        }

        $this->createTable('customer_loan', [
            'id' => $this->primaryKey(),
            'customer_id' => Schema::TYPE_INTEGER . " NOT NULL",
            'amount_loan' => Schema::TYPE_DECIMAL . "(10,1) NOT NULL",
            'amount_loan_vnd' => Schema::TYPE_DECIMAL . "(10,1)",
            'status' => Schema::TYPE_SMALLINT . "(2) default 1",
            'type' => Schema::TYPE_SMALLINT . "(2) default 0",
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);

        $this->addColumn('{{%customer}}', 'is_loan_admin', Schema::TYPE_SMALLINT . "(2) DEFAULT 0 AFTER status");
    }

    public function down()
    {
        $this->dropTable('customer_loan');
    }
}
