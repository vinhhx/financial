<?php

use yii\db\Migration;

class m160615_082223_add_column_time_sent_completed extends Migration
{
    public function up()
    {
        $this->addColumn('customer_package', 'time_sent_completed', \yii\db\Schema::TYPE_INTEGER . " NOT NULL DEFAULT 0 AFTER status");
    }

    public function down()
    {
        echo "m160615_082223_add_column_time_sent_completed cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
