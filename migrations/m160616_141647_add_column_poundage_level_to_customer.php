<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles adding column_poundage_level to table `customer`.
 */
class m160616_141647_add_column_poundage_level_to_customer extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        $this->addColumn('customer', 'poundage_level', Schema::TYPE_INTEGER . " NOT NULL DEFAULT 0"); //Level hoa hồng đc nhận

        $this->createTable('re_calculate_poundage_queue', [
            'id' => $this->primaryKey(),
            'params' => $this->text(),
            'status' => $this->smallInteger(2)->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
