<?php

use yii\db\Migration;
use yii\db\Schema;

class m160611_225247_add_customer_request_block_chain_wallet_id extends Migration
{
    public function up()
    {

        $this->addColumn('customer_request', 'block_chain_wallet_id', Schema::TYPE_STRING . " AFTER customer_id");

    }

    public function down()
    {
        echo "m160611_225247_add_customer_request_block_chain_wallet_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
