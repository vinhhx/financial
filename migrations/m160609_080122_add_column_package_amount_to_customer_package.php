<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles adding column_package_amount to table `customer_package`.
 */
class m160609_080122_add_column_package_amount_to_customer_package extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->alterColumn('customer_package', 'package_id', Schema::TYPE_INTEGER . " NULL ");
        $this->addColumn('customer_package', 'package_amount', Schema::TYPE_DECIMAL . "(10,4) DEFAULT 0 AFTER package_id");
        $this->addColumn('customer_package', 'increase_amount', Schema::TYPE_DECIMAL . "(10,4) DEFAULT 0 AFTER package_amount");
        $this->addColumn('customer_package', 'total', Schema::TYPE_DECIMAL . "(10,4) DEFAULT 0 AFTER increase_amount");
        $this->addColumn('customer_package', 'is_reinvestment', Schema::TYPE_SMALLINT . "(2) DEFAULT 0 AFTER total");
        $this->addColumn('customer_package', 'reinvestment_date', Schema::TYPE_INTEGER . " AFTER is_reinvestment");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
