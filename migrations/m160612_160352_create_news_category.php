<?php

use yii\db\Migration;

/**
 * Handles the creation for table `news_category`.
 */
class m160612_160352_create_news_category extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('news_category', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(),
            'title' => $this->string(255)->notNull(),
            'alias' => $this->string(255),
            'intro' => $this->text(),
            'image' => $this->string(255),
            'status' => $this->smallInteger(1)->defaultValue(1),
            'deleted' => $this->smallInteger(1)->defaultValue(0),
            'created_at' => $this->integer(10),
            'updated_at' => $this->integer(10)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('news_category');
    }
}
