<?php

use yii\db\Migration;

class m160829_220626_add_column_package_amount_root_to_cusstomer_package extends Migration
{
    public function up()
    {
        $this->addColumn('customer_package','package_amount_root',$this->decimal(16,8)->notNull()->defaultValue(0)->unsigned());
    }

    public function down()
    {
        echo "m160829_220626_add_column_package_amount_root_to_cusstomer_package cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
