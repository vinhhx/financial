<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_customer_bank_address`.
 */
class m160613_021152_create_table_customer_bank_address extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        $this->createTable('customer_bank_address', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unsigned(),
            'bank_address' => $this->string()->notNull(),
            'status' => $this->smallInteger(2)->notNull()->defaultValue(1)->unsigned(),
            'created_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
            'updated_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
        ]);
        $this->createIndex('CUSTOMER_BANK_ADDRESS', 'customer_bank_address', ['customer_id', 'bank_address'], true);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('table_customer_bank_address');
    }
}
