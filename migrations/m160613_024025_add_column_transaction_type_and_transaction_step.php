<?php

use yii\db\Migration;
use yii\db\Schema;

class m160613_024025_add_column_transaction_type_and_transaction_step extends Migration
{
    public function up()
    {

        $this->addColumn('customer_transaction', 'type', Schema::TYPE_SMALLINT . "(2) NOT NULL DEFAULT 0 AFTER status");
        $this->addColumn('customer_transaction', 'type_step', Schema::TYPE_SMALLINT . "(4) NOT NULL DEFAULT 0 AFTER type");
    }

    public function down()
    {
        echo "m160613_024025_add_column_transaction_type_and_transaction_step cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
