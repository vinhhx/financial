<?php

use yii\db\Migration;

class m161001_025815_add_column_attachment_type_to_customer_transaction extends Migration
{
    public function up()
    {
        $this->addColumn('customer_transaction','attachment_type',$this->smallInteger(2)->notNull()->defaultValue(1));
        $this->addColumn('customer_order','attachment_type',$this->smallInteger(2)->notNull()->defaultValue(1));


    }

    public function down()
    {
        echo "m161001_025815_add_column_attachment_type_to_customer_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
