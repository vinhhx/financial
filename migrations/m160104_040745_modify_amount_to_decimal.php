<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_040745_modify_amount_to_decimal extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%customer_require_cashout}}', 'amount', Schema::TYPE_DECIMAL . "(10,1)");
        $this->alterColumn('{{%customer_transaction}}', 'amount', Schema::TYPE_DECIMAL . "(10,1)");
        $this->alterColumn('{{%customer_transaction}}', 'balance_before', Schema::TYPE_DECIMAL . "(10,1)");
        $this->alterColumn('{{%customer_transaction}}', 'balance_after', Schema::TYPE_DECIMAL . "(10,1)");
    }

    public function down()
    {
        echo "m160104_040745_modify_amount_to_decimal cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
