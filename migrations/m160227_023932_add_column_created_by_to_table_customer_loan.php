<?php

use yii\db\Migration;

class m160227_023932_add_column_created_by_to_table_customer_loan extends Migration
{
    public function up()
    {
        $this->addColumn('{{%customer_loan}}','created_by',\yii\db\Schema::TYPE_INTEGER." AFTER type");
    }

    public function down()
    {
    }
}
