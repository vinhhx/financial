<?php

use yii\db\Migration;

class m160708_172135_alter_column_increase_person_to_decimal extends Migration
{
    public function up()
    {
        $this->alterColumn('package', 'increase_percent', $this->decimal(10, 4)->notNull()->defaultValue(0));
    }

    public function down()
    {
        echo "m160708_172135_alter_column_increase_person_to_decimal cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
