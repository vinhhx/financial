<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_banner`.
 */
class m160619_050542_create_table_banner extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('banner', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'image' => $this->string(),
            'url' => $this->string(),
            'type' => $this->smallInteger(2)->defaultValue(1)->unsigned(),
            'status' => $this->smallInteger(2)->defaultValue(1)->unsigned(),
            'created_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
            'updated_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('banner');
    }
}
