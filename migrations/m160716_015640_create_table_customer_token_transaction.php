<?php

use yii\db\Migration;

class m160716_015640_create_table_customer_token_transaction extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        $this->createTable('customer_token_transaction', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unsigned(),
            'bill_id' => $this->integer()->notNull()->unsigned()->defaultValue(0),
            'quantity' => $this->integer()->notNull()->unsigned(),
            'sign' => $this->smallInteger()->notNull()->unsigned()->defaultValue(0),
            'receiver' => $this->string()->notNull()->defaultValue(''),
            'content' => $this->string()->notNull()->defaultValue(''),
            'type'=>$this->smallInteger()->notNull()->defaultValue(1),
            'created_at'=>$this->integer()->notNull()->unsigned()->defaultValue(0),
            'updated_at'=>$this->integer()->notNull()->unsigned()->defaultValue(0)
        ], $tableOptions);

    }

    public function down()
    {
        echo "m160716_015640_create_table_customer_token_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
