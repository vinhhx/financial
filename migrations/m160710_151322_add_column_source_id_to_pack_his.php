<?php

use yii\db\Migration;

/**
 * Handles adding column_source_id to table `pack_his`.
 */
class m160710_151322_add_column_source_id_to_pack_his extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('invest_customer_package_history', 'source_id', \yii\db\Schema::TYPE_INTEGER." AFTER amount");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
