<?php

use yii\db\Migration;

class m160629_230513_add_column_increase extends Migration
{
    public function up()
    {
        $this->addColumn('invest_package','branch_full',\yii\db\Schema::TYPE_DECIMAL."(10,4) NOT NULL AFTER increase_month");
        $this->addColumn('invest_package','branch_short',\yii\db\Schema::TYPE_DECIMAL."(10,4) NOT NULL AFTER branch_full");
        $this->addColumn('invest_package','matching',\yii\db\Schema::TYPE_DECIMAL."(10,4) NOT NULL AFTER branch_short");
    }

    public function down()
    {
        echo "m160629_230513_add_column_increase cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
