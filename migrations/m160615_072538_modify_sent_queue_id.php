<?php

use yii\db\Migration;

class m160615_072538_modify_sent_queue_id extends Migration
{
    public function up()
    {
        $this->renameColumn('customer_transaction', 'customer_queue_id', 'customer_sent_queue_id');
    }

    public function down()
    {
        echo "m160615_072538_modify_sent_queue_id cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
