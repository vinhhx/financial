<?php

use yii\db\Schema;
use yii\db\Migration;

class m160115_023024_add_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%customer_case}}','parent_username',Schema::TYPE_STRING." AFTER parent_id");
    }

    public function down()
    {
        echo "m160115_023024_add_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
