<?php

use yii\db\Migration;

class m160719_140852_add_column_type_module_to_customer_token_transaction extends Migration
{
    public function up()
    {
        $this->addColumn('customer_token_transaction','type_module',$this->smallInteger(2)->notNull()->defaultValue(1)->unsigned());

    }

    public function down()
    {
        echo "m160719_140852_add_column_type_module_to_customer_token_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
