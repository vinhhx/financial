<?php

use yii\db\Migration;

/**
 * Handles adding column_min_max to table `queue`.
 */
class m160619_031710_add_column_min_max_to_queue extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $this->addColumn('customer_transaction_queue','min_amount',\yii\db\Schema::TYPE_DECIMAL."(10,4) NOT NULL DEFAULT 0");
        $this->addColumn('customer_transaction_queue','max_amount',\yii\db\Schema::TYPE_DECIMAL."(10,4) NOT NULL DEFAULT 0 ");

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
