<?php

use yii\db\Migration;

class m160619_044607_change_require_wallet_block_chain extends Migration
{
    public function up()
    {
        $this->alterColumn('customer_package', 'block_chain_wallet_id', \yii\db\Schema::TYPE_STRING . " NULL");

    }

    public function down()
    {
        echo "m160619_044607_change_require_wallet_block_chain cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
