<?php

use yii\db\Migration;

class m160608_073135_update_table_customer_add_column_is_active extends Migration
{
    public function up()
    {
        $this->addColumn('customer','is_active',\yii\db\Schema::TYPE_SMALLINT."(2) NOT NULL  default 0 AFTER status");
    }

    public function down()
    {
        echo "m160608_073135_update_table_customer_add_column_is_active cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
