<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_014233_modify_column_amount_to_decimal_add_column_is_deleted extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%customer}}','amount',Schema::TYPE_DECIMAL."(10,1)");

        $this->addColumn('{{%customer}}','is_deleted',Schema::TYPE_SMALLINT."(2) NOT NULL DEFAULT 0 after status");
    }

    public function down()
    {
        echo "m160104_014233_modify_column_amount_to_decimal_add_column_is_deleted cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
