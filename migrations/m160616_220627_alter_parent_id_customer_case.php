<?php

use yii\db\Migration;

class m160616_220627_alter_parent_id_customer_case extends Migration
{
    public function up()
    {
        $this->alterColumn('customer_case', 'parent_id', $this->integer(11)->notNull()->unsigned());
    }

    public function down()
    {
        echo "m160616_220627_alter_parent_id_customer_case cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
