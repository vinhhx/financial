<?php

use yii\db\Migration;

class m160714_161041_add_column_in_customer_transaction extends Migration
{
    public function up()
    {
        $this->addColumn('customer_transaction', 'customer_sent_ref_code', $this->string()->after('customer_sent_id'));
        $this->addColumn('customer_transaction', 'customer_receiver_ref_code', $this->string()->after('customer_receiver_id'));
    }

    public function down()
    {
        echo "m160714_161041_add_column_in_customer_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
