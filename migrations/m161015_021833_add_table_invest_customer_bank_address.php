<?php

use yii\db\Migration;

class m161015_021833_add_table_invest_customer_bank_address extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        $this->createTable('invest_customer_bank_address', [
            'id'=>$this->primaryKey(),
            'invest_customer_id'=>$this->integer()->notNull()->unique()->unsigned(),
            'bank_address'=>$this->string(255)->notNull(),
            'status'=>$this->smallInteger(2)->notNull()->defaultValue(1),
            'created_at'=>$this->integer()->notNull()->defaultValue(0),
            'updated_at'=>$this->integer()->notNull()->defaultValue(0),
        ], $tableOptions);

    }

    public function down()
    {
        echo "m161015_021833_add_table_invest_customer_bank_address cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
