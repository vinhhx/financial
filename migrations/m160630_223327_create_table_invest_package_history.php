<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_invest_package_history`.
 */
class m160630_223327_create_table_invest_package_history extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        $this->createTable('invest_customer_package_history', [
            'id' => $this->primaryKey(),
            'customer_package_id' => $this->integer()->notNull()->unsigned(),
            'sign' => $this->smallInteger(2)->notNull()->defaultValue(1)->unsigned(),
            'amount' => $this->decimal(10, 4)->notNull()->unsigned(),
            'type' => $this->smallInteger(2)->notNull()->defaultValue(0)->unsigned(),
            'created_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
            'updated_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
        ], $tableOptions);

        $this->addColumn('invest_customer_package', 'last_profit_receiver', \yii\db\Schema::TYPE_INTEGER . " NOT NULL DEFAULT 0 AFTER last_cash_out_at");

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('table_invest_package_history');
    }
}
