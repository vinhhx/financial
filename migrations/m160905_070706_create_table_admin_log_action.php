<?php

use yii\db\Migration;

class m160905_070706_create_table_admin_log_action extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }

        $this->createTable('admin_log_action', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull()->unsigned(),
            'user_name' => $this->string()->notNull(),
            'ip_access' => $this->string()->notNull(),
            'controller_id' => $this->string()->notNull(),
            'controller_action_id' => $this->string()->notNull(),
            'action_type' => $this->smallInteger(4)->notNull()->unsigned(),
            'action_content' => $this->text(),
            'created_at' => $this->integer()->notNull()->unsigned()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->unsigned()->defaultValue(0),

        ], $tableOptions);

    }

    public function down()
    {
        echo "m160905_070706_create_table_admin_log_action cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
