<?php

use yii\db\Migration;
use yii\db\Schema;

class m160718_235015_create_table_invest_customer extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        //Bảng tài khoản hệ thống
        $this->createTable('{{%invest_customer}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'ref_code' => $this->string(32)->notNull()->unique(),
            'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
            'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
            'password_reset_token' => Schema::TYPE_STRING,
            'status'=>$this->smallInteger(2)->notNull()->defaultValue(0),
            'is_active'=>$this->smallInteger(2)->notNull()->defaultValue(0),
            'is_deleted'=>$this->smallInteger(2)->notNull()->defaultValue(0),
            'parent_id'=>$this->integer()->notNull(),
            'package_id'=>$this->integer()->notNull()->defaultValue(0),
            'amount'=>$this->decimal(16,8)->notNull()->defaultValue(0),
            'full_name'=>$this->string()->notNull(),
            'email' => Schema::TYPE_STRING . '(50) NOT NULL',
            'phone' => Schema::TYPE_STRING . '(20) NOT NULL',
            'sex'=>$this->smallInteger(2)->notNull()->defaultValue(0),
            'dob'=>$this->date(),
            'identity_card'=>$this->string(20)->notNull()->defaultValue(''),
            'nation'=>$this->string(45)->notNull()->defaultValue(''),
            'address'=>$this->string(255)->notNull()->defaultValue(''),
            'date_join' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
            'last_auto_increase'=>$this->dateTime(),
            'created_at' => $this->integer()->notNull()->unsigned()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->unsigned()->defaultValue(0)
        ], $tableOptions);
    }

    public function down()
    {
        echo "m160718_235015_create_table_invest_customer cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
