<?php

use yii\db\Migration;

class m160614_001619_alter_customer_id_is_null_for_customer_token extends Migration
{
    public function up()
    {
        $this->alterColumn('customer_token', 'customer_id', \yii\db\Schema::TYPE_INTEGER . " NULL");

    }

    public function down()
    {
        echo "m160614_001619_alter_customer_id_is_null_for_customer_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
