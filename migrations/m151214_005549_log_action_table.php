<?php

use yii\db\Schema;
use yii\db\Migration;

class m151214_005549_log_action_table extends Migration
{
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1';
        }

        $this->createTable('{{%log_action}}', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_SMALLINT . ' UNSIGNED DEFAULT 0',
            'params' => Schema::TYPE_TEXT,
            'user_id' => Schema::TYPE_INTEGER . ' UNSIGNED DEFAULT 0',
            'user_username' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'user_type' => Schema::TYPE_SMALLINT . '(1) UNSIGNED DEFAULT 0',
            'user_ip' => Schema::TYPE_STRING . '(20) DEFAULT NULL',
            'object_id' => Schema::TYPE_INTEGER . ' UNSIGNED DEFAULT 0',
            'object_table' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ], $tableOptions);

    }

    public function down()
    {
        echo "m151214_005549_log_action_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
