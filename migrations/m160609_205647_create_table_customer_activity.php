<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_customer_activity`.
 */
class m160609_205647_create_table_customer_activity extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1';
        }
        $this->createTable('customer_activity', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer()->notNull()->unsigned(),
            'customer_username' => $this->string(255)->notNull(),
            'type' => $this->smallInteger(4)->notNull()->defaultValue(0),
            'message' => $this->string(1000)->notNull()->defaultValue(''),
            'params' => $this->text(),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0)
        ], $tableOptions);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('table_customer_activity');
    }
}
