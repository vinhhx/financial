<?php

use yii\db\Schema;
use yii\db\Migration;

class m160115_075806_modify_table_parent_username_to_customer_username extends Migration
{
    public function up()
    {
        $this->renameColumn('{{%customer_case}}','parent_username','customer_username');

    }

    public function down()
    {
        echo "m160115_075806_modify_table_parent_username_to_customer_username cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
