<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Handles adding block_chain_wallet_id to table `customer_package`.
 */
class m160610_221321_add_block_chain_wallet_id_to_customer_package extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('customer_package','block_chain_wallet_id',Schema::TYPE_STRING." NOT NULL AFTER type");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
    }
}
