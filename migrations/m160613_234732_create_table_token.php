<?php

use yii\db\Migration;

/**
 * Handles the creation for table `table_token`.
 */
class m160613_234732_create_table_token extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        $this->createTable('customer_token', [
            'id' => $this->primaryKey(),
            'token' => $this->string()->notNull()->unique(),
            'customer_id' => $this->integer()->notNull()->unsigned(),
            'status' => $this->smallInteger(2)->notNull()->defaultValue(0),
            'type' => $this->smallInteger(2)->notNull()->defaultValue(0),
            'expired_at' => $this->integer()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull()->defaultValue(0),
            'updated_at' => $this->integer()->notNull()->defaultValue(0),
        ]);

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('table_token');
    }
}
