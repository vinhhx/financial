<?php

use yii\db\Migration;

class m160719_070547_add_column_tmoodumodule_to_customer_request extends Migration
{
    public function up()
    {
        $this->addColumn('customer_order', 'type_module', $this->smallInteger(2)->defaultValue(1)->after('type'));
    }

    public function down()
    {
        echo "m160719_070547_add_column_tmoodumodule_to_customer_request cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
