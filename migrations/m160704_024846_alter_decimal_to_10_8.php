<?php

use yii\db\Migration;

class m160704_024846_alter_decimal_to_10_8 extends Migration
{
    public function up()
    {
        $this->alterColumn('customer', 'amount', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");

        $this->alterColumn('customer_package', 'package_amount', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");

        $this->alterColumn('customer_package', 'total', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");

        $this->alterColumn('customer_poundage_transaction', 'amount', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");
        $this->alterColumn('customer_poundage_transaction', 'balance_before', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");
        $this->alterColumn('customer_poundage_transaction', 'balance_after', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");

        $this->alterColumn('customer_request', 'amount', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");

        $this->alterColumn('customer_transaction', 'amount', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");

        $this->alterColumn('customer_transaction_queue', 'amount', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");
        $this->alterColumn('customer_transaction_queue', 'min_amount', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");
        $this->alterColumn('customer_transaction_queue', 'max_amount', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");

        $this->alterColumn('customer_wallet', 'balance', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");
        $this->alterColumn('customer_wallet', 'poundage', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");

        $this->alterColumn('invest_customer_package', 'amount', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");
        $this->alterColumn('invest_customer_package', 'total_current_profit', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");
        $this->alterColumn('invest_customer_package', 'current_profit', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");
        $this->alterColumn('invest_customer_package', 'current_poundage', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");

        $this->alterColumn('invest_customer_package', 'amount', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");

        $this->alterColumn('invest_customer_request', 'amount', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");

        $this->alterColumn('invest_customer_transaction', 'amount', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");
        $this->alterColumn('invest_customer_transaction', 'balance_before', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");
        $this->alterColumn('invest_customer_transaction', 'balance_after', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");

        $this->alterColumn('invest_customer_wallet', 'balance', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");
        $this->alterColumn('invest_customer_wallet', 'poundage', \yii\db\Schema::TYPE_DECIMAL . "(10,8) NOT NULL DEFAULT 0");
    }

    public function down()
    {
        echo "m160704_024846_alter_decimal_to_10_8 cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
