<?php

use yii\db\Migration;
use yii\db\Schema;

class m160301_035326_create_table_email extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        $this->createTable('{{%email}}', [
            'id' => Schema::TYPE_PK,
            'to_email' => Schema::TYPE_STRING,
            'subject' => Schema::TYPE_STRING,
            'template' => Schema::TYPE_STRING,
            'params' => Schema::TYPE_TEXT,
            'result_code' => Schema::TYPE_SMALLINT . "(4) UNSIGNED DEFAULT 0",
            'result_message' => Schema::TYPE_STRING,
            'type' => Schema::TYPE_SMALLINT . "(2) UNSIGNED  DEFAULT 0",
            'status' => Schema::TYPE_SMALLINT . "(2) UNSIGNED DEFAULT 0",
            'created_at' => Schema::TYPE_INTEGER,
            'updated_at' => Schema::TYPE_INTEGER,
        ],$tableOptions);
    }

    public function down()
    {
        $this->dropTable('table_email');
    }
}
