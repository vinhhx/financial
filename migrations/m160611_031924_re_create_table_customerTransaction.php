<?php

use yii\db\Migration;

class m160611_031924_re_create_table_customerTransaction extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB AUTO_INCREMENT 1000000';
        }
        $this->dropTable('customer_transaction');
        $this->createTable('customer_transaction', [
            'id' => $this->primaryKey(),
            'queue_id' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
            'customer_sent_id' => $this->integer()->notNull()->unsigned(),
            'customer_sent_username' => $this->string()->notNull(),
            'customer_sent_bitcoin_wallet' => $this->string(),
            'amount' => $this->decimal(10, 4)->notNull()->unsigned(),
            'customer_receiver_id' => $this->integer()->notNull()->unsigned(),
            'customer_receiver_username' => $this->string()->notNull(),
            'customer_receiver_bitcoin_wallet' => $this->string(),
            'status' => $this->smallInteger(4)->notNull()->unsigned()->defaultValue(10),
            'expired_sent' => $this->integer()->notNull()->unsigned(),
            'expired_approve' => $this->integer()->notNull()->unsigned(),
            'created_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
            'updated_at' => $this->integer()->notNull()->defaultValue(0)->unsigned(),
            'attachment' => $this->string(),
            'note' => $this->string(),
        ], $tableOptions);
    }

    public function down()
    {
        echo "m160611_031924_re_create_table_customerTransaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
