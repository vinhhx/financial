<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\AdminLogActionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Admin Log Actions');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title
?>
<div class="box box-widget">
    <div class="box-body">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
</div>
<div class="admin-log-action-index">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user_name',
            'controller_id',
            'controller_action_id',
            [
                'attribute' => 'action_type',
                'format' => 'raw',
                'value' => function ($data) {
                    return \app\models\logs\AdminLogAction::getActionTypeLabels($data->action_type);
                }
            ],
            'created_at:datetime',
            'ip_access',
            // 'updated_at',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ]
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
