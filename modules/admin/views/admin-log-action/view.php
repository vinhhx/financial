<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\logs\AdminLogAction */

$this->title = $model->id;
$this->params['breadcrumbs'][Yii::t('app', 'Admin Log Actions')] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="admin-log-action-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_name',
            'ip_access',
            [
                'label' => 'controller/action',
                'format' => 'raw',
                'value' => $model->controller_id . '/' . $model->controller_action_id,
            ],
            [
                'attribute' => 'action_type',
                'format' => 'raw',
                'value' => $model::getActionTypeLabels($model->action_type),
            ],

            'created_at:datetime',
        ],
    ]) ?>
    <div class="box box-widget">
        <div class="box-header">
            <h3 class="box-title">
                Nội dung log:
            </h3>
        </div>
        <div class="box-body">
            <?php
            echo '<pre>';
            echo print_r(json_decode($model->action_content), true);
            echo '</pre>'
            ?>

        </div>
    </div>
</div>
