<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use app\models\investments\InvestCustomerRequest;

/* @var $this yii\web\View */
/* @var $model app\models\investments\InvestCustomerRequest */

$this->title = $model->id;
$this->params['breadcrumbs'][Yii::t('app', 'Invest Customer Requests')] = Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="invest-customer-request-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_username',
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => $model->getTypeLabel(),
            ],
            [
                'label' => 'BTC value',
                'value' => Yii::$app->formatter->asDecimal($model->amount,8),
            ],
            'bank_address',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => $model->getStatusLabel(),
            ],
            [
                'attribute' => 'transaction_code',
                'format' => 'raw',
                'value' => ($model->transaction_code) ? Html::a(Html::img(Yii::$app->image->getImg($model->transaction_code, '100x100'), ['class' => 'image image-responsive']), Yii::$app->image->getImg($model->transaction_code), ['target' => '_blank']) : null,
            ],
            'created_at:datetime',
        ],
    ]) ?>
    <?php
    switch ($model->type) {
        case InvestCustomerRequest::TYPE_GET_HELP:
        case InvestCustomerRequest::TYPE_CASH_OUT:
            switch ($model->status) {
                case InvestCustomerRequest::STATUS_CREATED:
                    $form = \yii\widgets\ActiveForm::begin(['id' => 'upload-attachment',
                        'options' => ['enctype' => 'multipart/form-data']]);
                    echo '<div class="bg-file-upload">';
                    echo $form->field($modelForm, 'file')->fileInput()->label(false);
                    echo '</div>';
                    echo Html::submitButton('Upload', ['class' => 'btn btn-danger btn-sm']);
                    \yii\widgets\ActiveForm::end();

                    break;
                default:
//                return Html::a('<span class="fa fa-eye"></span>&nbsp;' . Yii::t('app', 'Detail'), ['view', 'id' => $model->id], [
//                    'class' => 'btn btn-primary btn-xs',
//                ]);
                    break;
            }
            break;
        case InvestCustomerRequest::TYPE_PROVIDER_HELP:
        case InvestCustomerRequest::TYPE_LOAN_OPEN_PACKAGE:
            switch ($model->status) {
                case InvestCustomerRequest::STATUS_SENT:
                    echo Html::a('<span class=" fa fa-check-circle-o"></span>&nbsp;' . Yii::t('app', 'Approved'), ['approved', 'id' => $model->id], [
                        'class' => 'btn btn-success',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you check this transaction?. You will approved for ' . $model->customer_username . '?'),
                            'method' => 'post',
                        ],
                    ]);

                    break;
                default:
//                return Html::a('<span class="fa fa-eye"></span>&nbsp;' . Yii::t('app', 'Detail'), ['view', 'id' => $model->id], [
//                    'class' => 'btn btn-primary btn-xs',
//                ]);
                    break;
            }
            break;
        default:
            break;
    }
    ?>
</div>
