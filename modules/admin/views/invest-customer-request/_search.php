<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\forms\investments\InvestCustomerRequestSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invest-customer-request-search form-search">

    <?php $form = ActiveForm::begin([
        'action' => '',
        'method' => 'get',
    ]); ?>

    <ul style="margin: 0; padding: 0;">
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'id', array('placeholder' => $model->getAttributeLabel('id'), 'style' => 'width:100px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'customer_username', array('placeholder' => 'Customer Username', 'style' => 'width:120px')) ?>
        </li>

        <li class="form-item">
            <?= Html::activeDropDownList($model, 'status', \app\models\investments\InvestCustomerRequest::getStatusLabels(), array('prompt' => 'All status', 'style' => 'width:150px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeDropDownList($model, 'status', \app\models\investments\InvestCustomerRequest::getTypeLabels(), array('prompt' => 'All Types', 'style' => 'width:150px')) ?>
        </li>
        <li class="form-item">
            <div class="form-group form-group-date" style="width: 200px; margin-bottom: -25px;">
                <label>Date</label>

                <div style="margin-top: -15px;">
                    <?=
                    \app\components\WGroupDatePicker::widget([
                        'model' => $model,
                        'attributes' => ['start_date', 'end_date'],
                        'formInline' => true,
                        'dateFormat' => 'dd-MM-yyyy',
                    ]);
                    ?>
                </div>
            </div>
        </li>
        <li class="form-item">
            <?= Html::submitButton('<i class="fa fa-search"></i>&nbsp;Tìm kiếm', ['class' => 'btn btn-primary btn-sm']) ?>
        </li>
    </ul>


    <?php ActiveForm::end(); ?>

</div>
