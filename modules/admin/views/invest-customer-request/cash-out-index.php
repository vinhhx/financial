<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\investments\InvestCustomerRequest;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\investments\InvestCustomerRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Invest Customer Requests Cash out');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;

?>
<div class="invest-customer-request-index">

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?=
        Html::a('Export Excel', ['export-excel', 'type' => 'cash-out-index'] + Yii::$app->request->queryParams, [
            'class' => 'btn btn-info',
            'data-method' => 'post',
        ]);
        ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'customer_username',
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getTypeLabel();
                }

            ],
            [
                'attribute' => 'amount',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-right'],
                'value' => function ($model) {
                    return '<span class="text-red">' . Yii::$app->formatter->asDecimal($model->amount, 8) . '</span>';
                }
            ],
            'bank_address',
            'created_at:datetime',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getStatusLabel();
                }

            ],
            [
                'attribute' => 'transaction_code',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->transaction_code):
                        return Html::a(Html::img(Yii::$app->image->getImg($model->transaction_code, '100x100'), ['class' => 'image image-responsive']), Yii::$app->image->getImg($model->transaction_code), ['target' => '_blank']);
                    endif;
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        switch ($model->type) {
                            case InvestCustomerRequest::TYPE_GET_HELP:
                            case InvestCustomerRequest::TYPE_CASH_OUT:
                                switch ($model->status) {
                                    case InvestCustomerRequest::STATUS_CREATED:
                                        return Html::a('<span class="fa fa-cloud-upload"></span>&nbsp;' . Yii::t('app', 'Up File'), ['view', 'id' => $model->id], [
                                            'class' => 'btn btn-danger btn-xs',
                                        ]);
                                        break;
                                    default:
                                        return Html::a('<span class="fa fa-eye"></span>&nbsp;' . Yii::t('app', 'Detail'), ['view', 'id' => $model->id], [
                                            'class' => 'btn btn-primary btn-xs',
                                        ]);
                                        break;
                                }
                                break;
                            case InvestCustomerRequest::TYPE_PROVIDER_HELP:
                            case InvestCustomerRequest::TYPE_LOAN_OPEN_PACKAGE:
                                switch ($model->status) {
                                    case InvestCustomerRequest::STATUS_SENT:
                                        return Html::a('<span class="fa fa-check-circle"></span>&nbsp;' . Yii::t('app', 'Check'), ['view', 'id' => $model->id], [
                                            'class' => 'btn btn-success btn-xs',
                                        ]);

                                        break;
                                    default:
                                        return Html::a('<span class="fa fa-eye"></span>&nbsp;' . Yii::t('app', 'Detail'), ['view', 'id' => $model->id], [
                                            'class' => 'btn btn-primary btn-xs',
                                        ]);
                                        break;
                                }
                                break;
                            default:
                                break;
                        }
                        return Html::a('<span class="fa fa-eye"></span>&nbsp;' . Yii::t('app', 'Detail'), ['view', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-xs',
                        ]);
                    },

                ]
            ],
        ],
    ]); ?>
</div>
