<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\investments\InvestCustomerRequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Invest Customer Requests');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invest-customer-request-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Invest Customer Request'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'customer_id',
            'customer_username',
            [
                'attribute' => 'amount',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-right'],
                'value' => function ($model) {
                    return '<span class="text-red">' . Yii::$app->formatter->asDecimal($model->amount, 8) . '</span>';
                }
            ],
            'bank_address',
            // 'status',
            // 'type',
            // 'transaction_code',
            // 'approved_by',
            // 'created_at',
            // 'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
