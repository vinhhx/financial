<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\forms\search\CustomerPoundageTransactionSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="form-search">
    <?php $form = ActiveForm::begin([
        'action' => ['group-commission'],
        'method' => 'get',
    ]); ?>
    <ul>
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'customer_id', array('placeholder' => $model->getAttributeLabel('customer_id'), 'style' => 'width:100px')) ?>
        </li>

        <li class="form-item">
            <?= Html::activeInput('text', $model, 'customer_username', array('placeholder' => $model->getAttributeLabel('customer_username'), 'style' => 'width:100px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeDropDownList($model, 'sign',$model::getSignLabels(), array('prompt' => 'ADD / SUB', 'style' => 'width:100px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeDropDownList($model, 'status',$model::getStatusLabels(), array('prompt' => '-- Chọn '.$model->getAttributeLabel('status').' --', 'style' => 'width:150px')) ?>
        </li>
        <li class="form-item">
            <?= Html::submitButton('<i class="fa fa-search"></i>&nbsp;' . Yii::t('app', 'Search'), ['class' => 'btn btn-primary btn-sm']) ?>
            <?= Html::resetButton('<i class="fa fa-refresh"></i>&nbsp;' . Yii::t('app', 'reset'), ['class' => 'btn btn-default btn-sm btn-second-horizontal']) ?>
        </li>
    </ul>
    <?php ActiveForm::end(); ?>

</div>

