<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\search\CustomerPoundageTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customer Poundage Transactions');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="customer-poundage-transaction-index">

    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'customer_username',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center text-bold'],
                'value' => function ($model) {
                    return $model->customer_username;
                }
            ],

            [
                'label' => 'Transaction type',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center text-bold'],
                'value' => function ($model) {
                    if ($model->sign == \app\models\CustomerPoundageTransaction::SIGN_ADD) {
                        return '<b class="btn btn-xs btn-success btn-type">+</b>';
                    } else {
                        return '<b class="btn btn-xs btn-danger btn-type">-</b>';
                    }
                }

            ],
            [
                'attribute' => 'from_username',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center text-bold'],
                'value' => function ($model) {
                    return $model->from_username;
                }
            ],
            [
                'attribute' => 'balance_before',
                'format' => 'raw',
                'contentOptions' => ['class' => ' text-right text-red'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asDecimal($model->balance_before, 8);
                }

            ], [
                'attribute' => 'amount',
                'format' => 'raw',
                'contentOptions' => ['class' => ' text-right text-red'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asDecimal($model->amount, 8);
                }

            ],
            [
                'attribute' => 'balance_after',
                'format' => 'raw',
                'contentOptions' => ['class' => ' text-right text-red'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asDecimal($model->balance_after, 8);
                }

            ],
            [
                'attribute' => 'level',
                'format' => 'raw',
                'contentOptions' => ['class' => ' text-center text-blue text-bold'],
                'value' => function ($model) {
                    return 'F' . $model->level;
                }

            ],
            [
                'label' => 'Reason',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getTypeLabel();
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getStatusLabel();
                }
            ],

//            ['class' => 'yii\grid\ActionColumn',
//                'template' => '{view}'
//            ],
        ],
    ]); ?>
</div>
<style>
    .btn-type {
        width: 24px;
        height: 23px;
        line-height: 15px;
        text-align: center !important;
        font-weight: bold;
        font-size: 20px;
        border-radius: 99px;
        padding: 2px !important;
    }
</style>