<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\search\CustomerPoundageTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'List Compensate Pending');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="customer-poundage-transaction-index">

    <?php echo $this->render('_search-compensate', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'customer_username',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center text-bold'],
                'value' => function ($model) {
                    return $model->customer_username;
                }
            ],

            [
                'label' => 'Transaction type',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center text-bold'],
                'value' => function ($model) {
                    if ($model->sign == \app\models\CustomerPoundageTransaction::SIGN_ADD) {
                        return '<b class="btn btn-xs btn-success btn-type">+</b>';
                    } else {
                        return '<b class="btn btn-xs btn-danger btn-type">-</b>';
                    }
                }

            ],
            [
                'attribute' => 'from_username',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center text-bold'],
                'value' => function ($model) {
                    return $model->from_username;
                }
            ],
            [
                'attribute' => 'amount',
                'format' => 'raw',
                'contentOptions' => ['class' => ' text-right text-red'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asDecimal($model->amount, 8);
                }

            ],

            [
                'label' => 'Reason',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getTypeLabel();
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getStatusLabel();
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{approve}',
                'buttons' => [
                    'approve' => function ($url, $data) {
                       return  Html::a('<span class="fa fa-check"></span>&nbsp;' . Yii::t('app', 'Approve'), \yii\helpers\Url::to(['poundage-transaction/approve-compensate', 'id' => $data->id]), [
                            'class' => 'btn btn-flat btn-xs btn-success',
                            'title' => Yii::t('app', 'Approve this Compensate'),
                            'data-confirm' => Yii::t('app', 'Are you sure, you want to approve this compensate ?'),
                            'data-method' => 'post',
                            'data-pjax' => 0,
                            'aria-label' => Yii::t('app', 'Approve'),
                        ]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
<style>
    .btn-type {
        width: 24px;
        height: 23px;
        line-height: 15px;
        text-align: center !important;
        font-weight: bold;
        font-size: 20px;
        border-radius: 99px;
        padding: 2px !important;
    }
</style>