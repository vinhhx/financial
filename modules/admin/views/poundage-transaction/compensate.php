<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 01-Oct-16
 * Time: 3:37 PM
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Cộng / Trừ số dư hoa hồng';
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="col-sm-8 col-lg-6">
    <div class="row">
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title"><?= "User:" . $customer->username ?></h3>
            </div>
            <?php $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'fieldConfig' => [
                    'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                    'horizontalCssClasses' => [
                        'label' => 'col-sm-3',
                        'offset' => 'col-sm-offset-0',
                        'wrapper' => 'col-sm-9',
                        'error' => 'col-sm-12',
                        'hint' => 'col-sm-12',
                    ],
                ]
            ]); ?>

            <div class="box-body">
                <?php echo $form->field($model, 'error')->begin();
                echo Html::error($model, 'error', ['class' => 'help-block col-sm-12']); //error
                echo $form->field($model, 'error')->end();
                ?>
                <?= $form->field($model, 'type')->dropDownList($model::getTypeLabels())->hint('Vui lòng chọn đúng kiểu bù hoa hồng') ?>
                <?= $form->field($model, 'amount')->textInput()->hint('Số tiền hoa hồng sẽ được cộng hoặc trừ vào số dư của người chơi') ?>
                <div class="form-group">
                    <?= Html::submitButton('<span class="fa fa-floppy-o"></span> &nbsp; Lưu ', ['class' => 'btn btn-primary  btn-flat pull-right']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>