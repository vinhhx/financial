<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomerPoundageTransaction */

$this->title = Yii::t('app', 'Create Customer Poundage Transaction');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customer Poundage Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-poundage-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
