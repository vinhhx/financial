<?php

use yii\helpers\Html;
use app\extensions\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\search\CustomerPoundageTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Group Poundage Transactions');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="customer-poundage-transaction-index">

    <?php echo $this->render('_search-commission', ['model' => $searchModel]); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'customer_username',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center text-bold'],
                'value' => function ($model) {
                    return $model["customer_username"];
                }
            ],

            [
                'label' => 'Transaction type',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center text-bold'],
                'value' => function ($model) {
                    if ($model["sign"] == \app\models\CustomerPoundageTransaction::SIGN_ADD) {
                        return '<b class="btn btn-xs btn-success btn-type">+</b>';
                    } else {
                        return '<b class="btn btn-xs btn-danger btn-type">-</b>';
                    }
                }

            ],
            [
                'label' => 'Total amount',
                'format' => 'raw',
                'contentOptions' => ['class' => ' text-right text-red'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asDecimal($model["total_amount"], 8);
                }

            ],
            [
                'label' => 'Commission Balance',
                'format' => 'raw',
                'contentOptions' => ['class' => ' text-right text-red'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asDecimal($model["commission_balance"], 8);
                }

            ],


            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}{add-commission}',
                'buttons' => [
                    'view' => function ($url, $data) {
                        return Html::a('<span class="fa fa-eye"></span>Xem ALL', ['index', 'CustomerPoundageTransactionSearch[customer_id]' => $data["customer_id"]], ['class' => 'btn btn-flat btn-xs btn-primary']);
                    },
                    'add-commission' => function ($url, $data) {
                        return Html::a('<span class="fa fa-plus"></span> <b>/</b> <span class="fa fa-minus"></span>  HH', ['compensate-commission', 'id' => $data['customer_id']], ['class' => 'btn btn-flat btn-warning btn-xs', 'style' => 'margin-left:10px']);
                    }
                ]
            ],
        ],
        'tools' => [
            Html::a('<i class="fa fa-list-alt"></i>&nbsp;' . Yii::t('users', 'Xem toàn bộ lịch sử giao dịch'), ['index'], ['class' => 'btn btn-success btn-sm btn-flat'])
        ]
    ]); ?>
</div>
<style>
    .btn-type {
        width: 24px;
        height: 23px;
        line-height: 15px;
        text-align: center !important;
        font-weight: bold;
        font-size: 20px;
        border-radius: 99px;
        padding: 2px !important;
    }
</style>