<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\search\MenuSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Menus';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-index">

    <div class="row">
        <div class="col-lg-7">
            <?= \yii\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'title',
                        'content' => function ($model) {
                            return str_repeat('-- ', $model->level) . $model->title;
                        }
                    ],
                    'link',
                    [
                        'attribute' => 'type',
                        'content' => function ($model) {
                            return isset($model->typeLabels[$model->type]) ? $model->typeLabels[$model->type] : '--';
                        }
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'buttons' => [
                            'view' => function () {
                            },
                            'update' => function ($url, $model, $key) {
                                return Html::a(Yii::t('app', 'Update'), ['index', 'id' => $model->id], [
                                    'class' => 'btn btn-primary btn-xs'
                                ]);
                            },
                            'delete' => function ($url, $model, $key) {
                                return Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                                    'class' => 'btn btn-danger btn-xs',
                                    'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => '0',
                                ]);
                            }
                        ]
                    ],
                ],
            ]); ?>
        </div>

        <div class="col-lg-5">
            <?= $this->render('_form', [
                'model' => $model
            ]) ?>
        </div>
    </div>

</div>
