<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Menu */

$this->title = Yii::t('app', 'Create Menu');
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-lg-7">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'title',
                    'content' => function ($model) {
                        return str_repeat('-- ', $model->level) . $model->title;
                    }
                ],
                // 'image',
                [
                    'attribute' => 'status',
                    'content' => function ($model) {
                        return $model->status ? '<i class="fa fa-check-square-o text-success"></i>' :
                            '<i class="fa fa-square-o text-danger"></i>';
                    },
                    'contentOptions' => [
                        'class' => 'text-center'
                    ]
                ],
                // 'deleted',
                // 'created_at',
                // 'updated_at',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'buttons' => [
                        'view' => function () {
                        },
                        'update' => function ($url, $model, $key) {
                            return Html::a(Yii::t('app', 'Update'), ['index', 'id' => $model->id], [
                                'class' => 'btn btn-primary btn-xs'
                            ]);
                        },
                        'delete' => function ($url, $model, $key) {
                            return Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger btn-xs',
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>

    <div class="col-lg-5">
        <?= $this->render('_form', [
            'model' => $model
        ]) ?>
    </div>
</div>
