<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Menu */

$this->title = Yii::t('app', 'Update Menu') . $model->title;
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
