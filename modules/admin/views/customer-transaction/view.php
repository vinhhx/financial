<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerTransaction */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customer Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-transaction-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_sent_request_id',
            'customer_sent_queue_id',
            'customer_sent_id',
            'customer_sent_username',
            'customer_sent_bank_address',
            'amount',
            'customer_receiver_request_id',
            'customer_receiver_queue_id',
            'customer_receiver_id',
            'customer_receiver_username',
            'customer_receiver_bank_address',
            'status',
            'customer_sent_type',
            'customer_receiver_type',
            'type_step',
            'expired_sent',
            'expired_approve',
            'created_at',
            'updated_at',
            'attachment',
            'note',
        ],
    ]) ?>

</div>
