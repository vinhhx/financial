<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 12:13 PM
 */
use yii\helpers\Html;
use \app\models\CustomerTransaction;

?>
<div class="page-view-transaction">
    <h2 class="page-title">Detail Order Information</h2>

    <div class="page-content">
        <p class="order-code">Order: <?= $model->id ?></p>
        Member of this
        site <?= ($model->customer_sent_id == Yii::$app->user->id) ? ' request assistance ' : ' receiver ' ?> in the
        amount <?= $model->amount ?> BTC
        <table class="table table-bordered table-hover dataTable">
            <tbody>
            <tr>
                <td>Name</td>
                <td><?= $model->customer_receiver_username ?></td>
            </tr>

            <tr>
                <td>Bitcoin address</td>
                <td><?= $model->customer_receiver_bank_address ?></td>
            </tr>
            </tbody>
        </table>
        <div class="notice">
            After you receive assistant you need to confirm it by clicking appropriate buton !
            <p class="text-danger">
                Never confirm payment before funds reception, as confirmation can not be reversed and the system will
                believe, that you have received funds
            </p>
        </div>

        <table class="table table-bordered table-hover dataTable">
            <tbody>

            <tr>
                <td>Receipent</td>
                <td><?= $model->customer_receiver_username ?></td>
            </tr>
            <tr>
                <td>Sender</td>
                <td><?= $model->customer_sent_username ?></td>
            </tr>
            </tbody>
        </table>
        <?php if ($customer->id == $model->customer_sent_id && $model->status == CustomerTransaction::STATUS_SEND_PENDING && $modelForm->type = "sent"): ?>
            <?php $form = \yii\widgets\ActiveForm::begin([
                'id' => 'upload-transaction-bill',
                'options' => ['enctype' => 'multipart/form-data']
            ]) ?>
            <div class="bg-file-upload">
                <?= $form->field($modelForm, 'file')->fileInput()->label(false) ?>
            </div>
            <?= Html::submitButton('Upload', ['class' => 'btn btn-danger btn-sm']) ?>
            <?= $form->field($modelForm, 'transaction_id')->hiddenInput()->label(false) ?>
            <?= $form->field($modelForm, 'customer_id')->hiddenInput()->label(false) ?>
            <?php \yii\widgets\ActiveForm::end() ?>
        <?php else:
            if ($model->attachment):
                ?>

                <div class="attachment-file">
                    <a target="_blank" id="attachment-file"
                       href="<?= Yii::$app->image->getImg($model->attachment) ?>">
                        <?= Html::img(Yii::$app->image->getImg($model->attachment, '100x100'), ['class' => 'img-thumbnail img-item']) ?>

                    </a>
                    <?php if ($customer->id == $model->customer_sent_id && $model->status == CustomerTransaction::STATUS_SEND_COMPLETED): ?>
                        <span id="action-delete-image" class="glyphicon glyphicon-remove transaction-remove-attachment" data-toggle="tooltip"
                              data-placement="top" title="remove this attachment"
                              data-transaction-id="<?= $model->id ?>"></span>
                    <?php endif; ?>
                </div>

            <?php endif; ?>
        <?php endif; ?>

        <?php
        if ($customer->id == $model->customer_receiver_id && $model->status == CustomerTransaction::STATUS_SEND_COMPLETED && $modelForm->type == "receive"): ?>
            <div class="clearfix"></div>
            <?php echo Html::a('Approved', 'javacript:void(0)', ['class' => 'btn btn-sm btn-success btn-approved-transaction', 'data-order-id' => $model->id]); ?>
        <?php endif;
        ?>
    </div>
</div>
