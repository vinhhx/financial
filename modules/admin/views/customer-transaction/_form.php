<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerTransaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-transaction-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customer_sent_request_id')->textInput() ?>

    <?= $form->field($model, 'customer_sent_queue_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customer_sent_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customer_sent_username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customer_sent_bank_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customer_receiver_request_id')->textInput() ?>

    <?= $form->field($model, 'customer_receiver_queue_id')->textInput() ?>

    <?= $form->field($model, 'customer_receiver_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customer_receiver_username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customer_receiver_bank_address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'customer_sent_type')->textInput() ?>

    <?= $form->field($model, 'customer_receiver_type')->textInput() ?>

    <?= $form->field($model, 'type_step')->textInput() ?>

    <?= $form->field($model, 'expired_sent')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'expired_approve')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'updated_at')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'attachment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
