<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\search\CustomerTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customer Transactions');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="customer-transaction-index">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                [
                    'attribute' => 'amount',
                    'format' => 'raw',
                    'contentOptions' => ['class' => 'text-bold text-red text-right'],
                    'value' => function ($model) {
                        return Yii::$app->formatter->asDecimal($model->amount, 8);
                    }
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model::getStatusLabels($model->status);
                    }
                ],

                'customer_receiver_username',
                [
                    'attribute' => 'customer_receiver_type',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model::getTypeLabels($model->customer_receiver_type);
                    }
                ],
                'customer_receiver_bank_address',
                'customer_sent_username',
                [
                    'attribute' => 'customer_sent_type',
                    'format' => 'raw',
                    'value' => function ($model) {
                        return $model::getTypeLabels($model->customer_sent_type);
                    }
                ],
                [
                    'attribute' => 'attachment',
                    'format' => 'raw',
                    'value' => function ($model) {
                        if ($model->attachment):
                            return Html::a(Html::img(Yii::$app->image->getImg($model->attachment, '100x100'), ['class' => 'image image-responsive', 'style' => 'width:50px;']), Yii::$app->image->getImg($model->attachment), ['target' => '_blank']);
                        endif;
                    }
                ],

                // 'customer_sent_bank_address',
                // 'amount',
                // 'customer_receiver_request_id',
                // 'customer_receiver_queue_id',
                // 'customer_receiver_id',
                // 'customer_receiver_username',
                // 'customer_receiver_bank_address',
                // 'status',
                // 'customer_sent_type',
                // 'customer_receiver_type',
                // 'type_step',
                // 'expired_sent',
                // 'expired_approve',
                // 'created_at',
                // 'updated_at',
                // 'attachment',
                // 'note',

                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{process}',
                    'buttons' => [
                        'process' => function ($url, $model) {
                            if ($model->customer_receiver_type == \app\models\CustomerTransaction::TYPE_RECEIVER_BY_SPECIAL && $model->status != \app\models\CustomerTransaction::STATUS_FINISH) {
                                //Admin chuyen tien
                                return Html::a('<span class="fa fa-arrow-circle-right"></span>&nbsp; Admin get help', "javascript:void(0);", ['class' => 'btn btn-sm btn-success btn-get-help', 'pId' => $model->id, 'type' => 'receive']);
                            } elseif ($model->customer_sent_type == \app\models\CustomerTransaction::TYPE_SENT_BY_SPECIAL && $model->status != \app\models\CustomerTransaction::STATUS_FINISH) {
                                //admin nhan tien
                                return Html::a('<span class="fa fa-arrow-circle-left"></span>&nbsp; Admin Provider Help', "javascript:void(0);", ['class' => 'btn btn-sm btn-danger btn-provider-help', 'pId' => $model->id, 'type' => 'sent']);
                            }
                        }
                    ]
                ],

            ],
        ]); ?>
    </div>
</div>

<div class="modal fade" id="pageModal" tabindex="-1" role="dialog" aria-labelledby="pageModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
            </div>

        </div>
    </div>
</div>
<?php
$js = <<<EOD
    $('body').on('click','.btn-get-help,.btn-provider-help',function(e){
        e.preventDefault();
        var tid=$(this).attr('pId');
        if(tid){
            var type=$(this).attr('type');
            $.ajax({
                url:location.href,
                dataType:"JSON",
                type:"POST",
                data:{type:type,tid:tid},
                success:function(data){
                    $("#pageModal .modal-body").html(data);
                    $("#pageModal").modal("show");
                }
            });
        }
    }).on('submit', 'form#upload-transaction-bill', function (e) {
        e.preventDefault();
        e.stopImmediatePropagation();
        var formData = new FormData(this);
        formData.append('type', 'upload');
        var _this = jQuery(this);
        jQuery.ajax({
            url: jQuery(this).attr('action'),
            type: 'POST',
            dataType: 'JSON',
            async: false,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                if (data) {
                    if (data.status == 'success') {
                       $("#pageModal .modal-body").html(data.content);
                        $("#pageModal").modal("show");
                         location.reload();
                    } else if (data.status == "error") {
                        alert(data.message);
                    }
                }
            }
        });
    }).on('click','.btn-approved-transaction',function(e){
         e.preventDefault();
         var confirmation=confirm('Bạn có chắc chắn đã nhận được btc từ người gửi theo mẫu trên? ');
         if(confirmation){
             $.ajax({
                url:location.href,
                dataType:"JSON",
                type:"POST",
                data:{type:'approved',tid:$(this).attr('data-order-id')},
                success:function(data){
                        $("#pageModal .modal-body").html(data);
                        $("#pageModal").modal("show");
                        location.reload();
                    }
            });
        }

    }).on('click', '.transaction-remove-attachment', function (e) {
        var _confirm = confirm('You sure want to delete this attachment ?');
        if (_confirm) {
            var _parent = $(this).parent();
            var _transactionId = $(this).attr('data-transaction-id');
            var _images = _parent.find('img.img-item');
            if (_images.length) {
                $.post('', {type: 'remove-attachment', tid: _transactionId}, function (data) {
                }).done(function () {
                    location.reload();
                });
            }
        }
    });
EOD;
$this->registerJs($js);
?>
