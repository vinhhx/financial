<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\forms\search\CustomerTransactionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-transaction-search form-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <ul style="margin: 0; padding: 0;">
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'id', array('placeholder' => $model->getAttributeLabel('id'), 'style' => 'width:100px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'customer_sent_username', array('placeholder' => $model->getAttributeLabel('customer_sent_username'), 'style' => 'width:120px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'customer_receiver_username', array('placeholder' => $model->getAttributeLabel('customer_receiver_username'), 'style' => 'width:120px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeDropDownList($model, 'status', \app\models\CustomerTransaction::getStatusLabels(), array('prompt' =>'All status', 'style' => 'width:150px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeDropDownList($model, 'type', \app\models\CustomerTransaction::getTypeLabels(), array('prompt' => 'All Type', 'style' => 'width:150px')) ?>
        </li>
        <li class="form-item">
            <?= Html::submitButton('<i class="fa fa-search"></i>&nbsp;Tìm kiếm', ['class' => 'btn btn-primary btn-sm']) ?>
        </li>
    </ul>

    <?php ActiveForm::end(); ?>

</div>
