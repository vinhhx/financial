<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\investments\InvestPackage */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
        'modelClass' => 'Invest Package',
    ]) . $model->id;
$this->params['breadcrumbs'][Yii::t('app', 'Invest Packages')] = Url::to(['package/index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>
