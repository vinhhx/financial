<?php

use yii\helpers\Html;
use app\extensions\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\investments\PackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Packages Rate');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="box box-default">
    <div class="box-body">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
</div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'package_name',
        [
            'label' => 'amount',
            'contentOptions' => [
                'class' => 'text-red text-bold',
            ],
            'content' => function ($data) {
                if ($data->amount_min !== $data->amount_max) {
                    return Yii::$app->formatter->asDecimal($data->amount_min, 1) . '&nbsp; → &nbsp; ' . Yii::$app->formatter->asDecimal($data->amount_max, 1);
                }
                return Yii::$app->formatter->asDecimal($data->amount_min, 1);
            }
        ],
        [
            'attribute' => 'increase_day',
            'contentOptions' => [
                'class' => 'text-red text-bold',
            ],
            'content' => function ($data) {
                return Yii::$app->formatter->asDecimal($data->increase_day, 1) . '%';
            }
        ],
        [
            'attribute' => 'branch_full',
            'contentOptions' => [
                'class' => 'text-red text-bold',
            ],
            'content' => function ($data) {
                return Yii::$app->formatter->asDecimal($data->branch_full, 1) . '%';
            }
        ],
        [
            'attribute' => 'branch_short',
            'contentOptions' => [
                'class' => 'text-red text-bold',
            ],
            'content' => function ($data) {
                return Yii::$app->formatter->asDecimal($data->branch_short, 1) . '%';
            }
        ],[
            'attribute' => 'matching',
            'contentOptions' => [
                'class' => 'text-red text-bold',
            ],
            'content' => function ($data) {
                return Yii::$app->formatter->asDecimal($data->matching, 1) . '%';
            }
        ],
        [
            'attribute' => 'status',
            'content' => function ($data) {
                /* @var $data app\models\Package */
                return $data->getStatusLabel();
            }
        ],
        'created_at:datetime',
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Action',
            'template' => '{view}<br>{update}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<i class="fa fa-eye"></i>&nbsp;' . Yii::t('users', 'Detail'), ['view', 'id' => $model->id], ['class' => 'btn btn-xs btn-info']);
                },
                'update' => function ($url, $model) {
                    return Html::a('<i class="fa fa-pencil-square-o"></i>&nbsp;' . Yii::t('users', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-second-vertical']);
                }
            ]
        ],
    ],
    'tools' => [
        Html::a('<i class="fa fa-plus-square-o"></i>&nbsp;' . Yii::t('users', 'Create new package'), ['create'], ['class' => 'btn btn-success btn-sm'])
    ]
]); ?>
