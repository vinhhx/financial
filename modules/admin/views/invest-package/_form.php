<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\investments\InvestPackage */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invest-package-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'package_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount_min')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount_max')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'increase_day')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'branch_full')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'branch_short')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'matching')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
