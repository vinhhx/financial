<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\investments\InvestPackage */

$this->title = $model->id;
$this->params['breadcrumbs'][Yii::t('app', 'Invest Packages')] = Url::to(['package/index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="invest-package-view">

<!--    <p>-->
<?php echo Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<?php //echo Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
//                'method' => 'post',
//            ],
//        ]) ?>
<!--    </p>-->

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'package_name',
            'amount_min:decimal',
            'amount_max:decimal',
            'increase_day:decimal',
            'branch_full:decimal',
            'branch_short:decimal',
            'matching:decimal',
//            'increase_month:decimal',
//            'status',
            'created_at:datetime',
        ],
    ]) ?>

</div>
