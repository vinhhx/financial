<?php

use yii\helpers\Html;
use app\extensions\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\search\BannerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Banners';
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="banner-index">

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            [
                'label' => 'image',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::img($model->getImage(), ['style' => 'max-width:300px; max-height:100px;']);
                },
            ],
            'url',
            'type',
            [
                'attribute' => 'type',
                'content' => function ($model) {
                    return isset($model->getTypeLabels()[$model->type]) ? $model->getTypeLabels()[$model->type] : '--';
                }
            ],
            [
                'attribute' => 'status',
                'content' => function ($model) {
                    return isset($model->getStatusLabels()[$model->status]) ? $model->getStatusLabels()[$model->status] : '--';
                }
            ],
            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url, $model, $key) {
                        return Html::a(Yii::t('app', 'View'), ['view', 'id' => $model->id], [
                            'class' => 'btn btn-warning btn-xs'
                        ]);
                    },
                    'update' => function ($url, $model, $key) {
                        return Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-xs'
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        return Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger btn-xs',
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                    }
                ]
            ],
        ],
        'tools' => [
            Html::a('<i class="fa fa-plus-square-o"></i>&nbsp;' . Yii::t('app', 'Create Banner'), ['create'], ['class' => 'btn btn-success btn-sm'])
        ]
    ]); ?>
</div>
