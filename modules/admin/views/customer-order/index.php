<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searchs\CustomerOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customer Orders');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="customer-order-index">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?=
        Html::a('Export Excel', ['export-excel'] + Yii::$app->request->queryParams, [
            'class' => 'btn btn-info',
            'data-method' => 'post',
        ]);
        ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'customer_username',
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getTypeLabel();
                }

            ],
            [
                'attribute' => 'type_module',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getTypeModuleLabel();
                }

            ],
            'quantity',
            'created_at:datetime',
            [
                'attribute' => 'upload_attachment_at',
                'value' => function ($model) {
                    return (isset($model->upload_attachment_at) && $model->upload_attachment_at > 0) ? Yii::$app->formatter->asDatetime($model->upload_attachment_at) : '--';
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getStatusLabel();
                }

            ],
            [
                'attribute' => 'attachment',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->attachment && $model->type == \app\models\CustomerOrder::TYPE_BUY_TOKEN):
                        return Html::a(Html::img(Yii::$app->image->getImg($model->attachment, '100x100'), ['class' => 'image image-responsive']), Yii::$app->image->getImg($model->attachment), ['target' => '_blank']);
                    else:
                        return $model->attachment;
                    endif;
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        if ($model->status == \app\models\CustomerOrder::STATUS_PAID && $model->type == \app\models\CustomerOrder::TYPE_BUY_TOKEN):
                            return Html::a('<span class="fa fa-paper-plane-o"></span>&nbsp;' . Yii::t('app', 'Sent Token'), ['view', 'id' => $model->id], [
                                'class' => 'btn btn-warning btn-xs',
                            ]);
                        else:
                            return Html::a('<span class="fa fa-eye"></span>&nbsp;' . Yii::t('app', 'Detail'), ['view', 'id' => $model->id], [
                                'class' => 'btn btn-primary btn-xs',
                            ]);
                        endif;
                    },
                ],
            ],
        ],
    ]); ?>
</div>
