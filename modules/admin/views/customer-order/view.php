<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerOrder */

$this->title = 'Order id: #' . $model->id;
$this->params['breadcrumbs'][Yii::t('app', 'Customer Orders')] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="customer-order-view">
    <?php if ($model->status == \app\models\CustomerOrder::STATUS_PAID): ?>
        <p>
            <?= Html::a('<span class=" fa fa-check-circle-o"></span>&nbsp;' . Yii::t('app', 'Approved'), ['approved', 'id' => $model->id], [
                'class' => 'btn btn-success',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want add ' . $model->quantity . ' token to customer ' . $model->customer_username . '?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>
    <?php endif; ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_username',
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => $model->getTypeLabel(),
            ],
            'quantity',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => $model->getStatusLabel(),
            ],
            [
                'attribute' => 'attachment',
                'format' => 'raw',
                'value' => ($model->type == \app\models\CustomerOrder::TYPE_BUY_TOKEN) ? Html::a(Html::img(Yii::$app->image->getImg($model->attachment, '100x100'), ['class' => 'image image-responsive']), Yii::$app->image->getImg($model->attachment), ['target' => '_blank']) : $model->attachment,
            ],
            [
                'attribute' => 'upload_attachment_at',
                'value' => (isset($model->upload_attachment_at) && $model->upload_attachment_at > 0) ? Yii::$app->formatter->asDatetime($model->upload_attachment_at) : '--',
            ],
            'created_at:datetime',
        ],
    ]) ?>

</div>
