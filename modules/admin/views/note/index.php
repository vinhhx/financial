<?php

use yii\helpers\Html;
use app\extensions\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\search\News */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'News');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="news-index">

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'header' => 'Customer',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a(Yii::t('app', 'More and Answer'), ['customer/index', 'CustomerSearch[id]' => $model->object_id], ['target' => '_blank']);
                }
            ],
            'content',
            'created_by',
            'created_at:datetime',
        ],
    ]); ?>
</div>
