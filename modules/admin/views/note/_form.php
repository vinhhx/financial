<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Note */
/* @var $form yii\widgets\ActiveForm */
?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title"><?= Yii::t('customers', 'Tin nhắn') ?></h4>
    </div>
    <ul class="list-group" style="max-height: 300px; overflow: auto; position: relative;" id="note-content">
        <?php if ($listNote): ?>
            <?php foreach ($listNote as $note): ?>
                <li class="list-group-item" data-id="<?= $note->id ?>">
                    <p>
                        <?php if (!isset($note->created_by)): ?>
                            <strong style="color: blue;">Anonymous</strong>
                        <?php else: ?>
                            <strong
                                style="<?= $note->created_by == Yii::$app->user->identity->username ? 'color: red;' : 'color: blue;' ?>"><?= Html::encode($note->created_by) ?></strong>
                        <?php endif; ?>
                        -
                        <?= \Yii::$app->formatter->asDate($note->created_at, 'php:H:i:s d/m/Y'); ?>
                    </p>
                    <div><?= Html::encode($note->content); ?></div>
                </li>
            <?php endforeach; ?>
        <?php endif; ?>
    </ul>
<?php $form = ActiveForm::begin(['id' => 'form-add-note']); ?>
    <div class="modal-body">
        <div style="padding:20px;">
            <?= $form->field($model, 'content')->textarea() ?>
            <?= Html::hiddenInput('Note[object_id]', $objectId); ?>
            <?= Html::hiddenInput('Note[object_table]', $objectTable); ?>
        </div>
    </div>
    <div class="modal-footer">
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary save-form-note']) ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
<?php ActiveForm::end(); ?>