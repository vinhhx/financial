<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;

?>
<footer class="footer footer-admin">
    <div class="container">
        <div class="pull-left">&copy; BitOness <?= date('Y') ?></div>
    </div>
</footer>