<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Nav;

?>
<div style="background-color: #f4f4f4">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <a href="/">
                    <img src="/images/logo.png">
                </a>
            </div>
            <?php
            $listConfig = \app\models\Config::getConfigByType(\app\models\Config::TYPE_HEADER_STATICTICS);
            $i = 0;
            foreach ($listConfig as $itemConfig):
                $i++;
                switch ($i) {
                    case 1:
                        $linkIcon = '/images/icon-member.png';
                        $classColor = 'member';
                        break;
                    case 2:
                        $linkIcon = '/images/icon-exchange.png';
                        $classColor = 'exchange';
                        break;
                    case 3:
                        $linkIcon = '/images/icon-bitcoin.png';
                        $classColor = 'bitcoin';
                        break;
                    case 4:
                        $linkIcon = '/images/icon-bitcoin-2.png';
                        $classColor = 'bitcoin-2';
                        break;
                    default:
                        $linkIcon = '';
                        $classColor = '';
                        break;
                }
                //$configStatic = '1359636|0.5|1|0.75|2|1.5|0.1|0.3';
                $configStatic = $itemConfig->content;
                $configArray = explode('|', $configStatic);
                $startTime = 1469358938;
                $totalDay = ceil((time() - $startTime) / 46800);
                $startNumber = isset($configArray[0]) ? $configArray[0] : 0;
                $total = count($configArray);
                $currentNumber = $startNumber;
                for ($dk = 1; $dk < $totalDay; $dk++) {
                    if ($dk >= $total) {
                        $id = ($dk % $total);
                        if ($id == 0) {
                            $id = 1;
                        }
                    } else {
                        $id = $dk;
                    }
                    if (isset($configArray[$id])) {
                        $currentNumber += ceil(($currentNumber * $configArray[$id]));
                    }
                }
                ?>
                <div class="col-md-2">
                    <div class="header-transaction">
                        <div class="pull-left">
                            <img src="<?= $linkIcon ?>">
                        </div>
                        <div class="<?= $classColor ?>">
                            <div
                                class="text-big"><?= $i != 1 ? '<i class="fa fa-btc"></i>' : '' ?><?= $currentNumber ?></div>
                            <div class="text-small"><?= $itemConfig->title ?></div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<div class="header-logo clearfix">
    <div class="container">
        <?php
        $modelMenus = \app\models\Menu::getItemByParentId();
        $itemMenu = [];
        foreach ($modelMenus as $menu) {
            $menuDetail = [
                'label' => $menu->title,
                'url' => $menu->link,
            ];
            if ($menuChild = \app\models\Menu::getItemByParentId($menu->id)) {
                $listChil = [];
                foreach ($menuChild as $menuC) {
                    $listChil[] = [
                        'label' => $menuC->title,
                        'url' => $menuC->link,
                    ];
                }
                $menuDetail['items'] = $listChil;
            }
            $itemMenu[] = $menuDetail;
        }

        echo Nav::widget([
            'options' => ['class' => 'navbar-nav header-menu-top'],
            'items' => $itemMenu,
        ]);
        ?>
    </div>
</div>