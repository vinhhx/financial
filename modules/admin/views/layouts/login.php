<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon" href="<?php echo Yii::$app->request->baseUrl; ?>/favicon.png" type="image/png" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<?php echo $this->render('@app/modules/admin/views/layouts/login/header'); ?>
<div class="container-login">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 col-xs-12 hidden-xs"></div>
            <div class="col-sm-8 col-xs-12">
               <?php  echo \app\extensions\widgets\Alert::widget(); ?>
                <?= $content ?>
            </div>
            <div class="col-sm-2 col-xs-12  hidden-xs"></div>
        </div>
    </div>

</div>
<?php echo $this->render('@app/modules/admin/views/layouts/login/footer'); ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
