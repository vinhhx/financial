<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */

$this->title = 'User information';
$this->params['pageTitle'] = $this->title;
$this->params['pageDescription'] = $this->title;
$this->params['breadcrumbs'] = [
    'Users' => ['index'],
    $this->title
];
$this->params["parentAction"]=\yii\helpers\Url::to(["admin/index"]);
?>
<div class="admin-view">

    <div class="box box-primary">
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?=
                    DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'username',
                            'email:email',
                            'full_name',
                            'phone',
                            [
                                'label' => $model->getAttributeLabel('status'),
                                'value' => \app\models\User::getStatusLabels()[$model->status],
                            ],
                            'created_at:datetime',
                        ],
                    ])
                    ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::a('<i class="fa fa-floppy-o"></i>&nbsp;'.Yii::t('customers','Save information'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary btn-sm']) ?>
                <?= Html::a('<i class="fa fa-users"></i>&nbsp;'.Yii::t('customers','Set permission'), ['permission', 'id' => $model->id], ['class' => 'btn btn-warning btn-sm btn-second-horizontal']) ?>
            </div>
        </div>
    </div>

</div>
