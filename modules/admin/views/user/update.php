<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */

$this->title = Yii::t('users','Update user #').'[' . $model->username . ']';
$this->params['pageTitle'] = $this->title;
$this->params['pageDescription'] = '';
$this->params['breadcrumbs'] = [
    Yii::t('users','Users') => ['index'],
    Yii::t('users','Update')
];
$this->params["parentAction"]=\yii\helpers\Url::to(["admin/index"]);
?>
<div class="admin-update">

    <?=
    $this->render('_form', [
        'model' => $model,
    ])
    ?>

</div>
