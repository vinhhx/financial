<?php

use yii\helpers\Html;
use app\extensions\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\AdminSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('users', 'User manager');
$this->params['pageTitle'] = Yii::t('users', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-index box box-info">

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box-body table-responsive no-pad?ing">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'username',
                [
                    'label' => '',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return
                            '<table class="table table-bordered no-margin">
                                <tbody>
                                    <tr>
                                        <td width="80">' . $data->getAttributeLabel('username') . '</td>
                                        <td>' . Html::encode($data->username) . '</td>
                                    </tr>
                                    <tr>
                                        <td>' . $data->getAttributeLabel('phone') . '</td>
                                        <td>' . Html::encode($data->phone) . '</td>
                                    </tr>
                                    <tr>
                                        <td>' . $data->getAttributeLabel('email') . '</td>
                                        <td>' . Html::encode($data->email) . '</td>
                                    </tr>
                                </tbody>
                            </table>';
                    }
                ],
                [
                    'attribute' => 'status',
                    'content' => function ($data) {
                        /* @var $data app\models\User */
                        $statusLabels = $data->getStatusLabels();
                        return isset($statusLabels[$data->status]) ? $statusLabels[$data->status] : '--';
                    }
                ],
                'created_at:datetime',
                [
                    'class' => 'yii\grid\ActionColumn',
                    'header' => 'Action',
                    'template' => '{view}<br>{update}<br>{permission}<br>{delete}',
                    'buttons' => [
                        'view' => function ($url, $model) {
                            return Html::a('<i class="fa fa-eye"></i>&nbsp;' . Yii::t('users', 'Detail'), ['view', 'id' => $model->id], ['class' => 'btn btn-xs btn-info', 'style' => 'margin-bottom: 3px;']);
                        },
                        'update' => function ($url, $model) {
                            return Html::a('<i class="fa fa-pencil-square-o"></i>&nbsp;' . Yii::t('users', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary', 'style' => 'margin-bottom: 3px;']);
                        },
                        'permission' => function ($url, $model) {
                            return Html::a('<i class="fa fa-users"></i>&nbsp;' . Yii::t('users', 'Set permission'), ['permission', 'id' => $model->id], ['class' => 'btn btn-xs btn-warning', 'style' => 'margin-bottom: 3px;']);
                        },
                        'delete' => function ($url, $model) {
                            return Html::a(Yii::t('users', 'Delete'), ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger btn-xs',
                                'style' => 'margin-bottom: 3px;',
                                'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method' => 'post',
                                'data-pjax' => '0',
                            ]);
                        }
                    ]
                ],
            ],
            'tools' => [
                Html::a('<i class="fa fa-plus-square-o"></i>&nbsp;' . Yii::t('users', 'Create new user'), ['create'], ['class' => 'btn btn-success btn-sm'])
            ]
        ]);
        ?>
    </div>
</div>