<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Admin */
/* @var $selected array */

$this->title = Yii::t('users','Set permission for user #').'['.$model->username.']';
$this->params['pageTitle'] = $this->title;
$this->params['pageDescription'] = '';
$this->params['breadcrumbs'] = [
    Yii::t('users','Users') => ['index'],
    Yii::t('users','Set permission')
];
$this->params["parentAction"]=\yii\helpers\Url::to(["admin/index"]);

// css
$css = <<< CSS
.drop-down-list > .items input{
    float: left;
    margin-right: 5px;
}
CSS;
$this->registerCss($css);
?>
<div class="admin-permission">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box box-primary">
        <div class="box-body">
            <?= Html::checkboxList('data', $selected, $items, [
                'class' => 'form-group drop-down-list',
                'item' => function ($index, $label, $name, $checked, $value){
                    return '<div class="items">' .
                    '<label>'.Html::checkbox($name, $checked, ['value' => $value, 'class' => 'check-item']).' '.$label.'</label>' .
                    '</div>';
                }
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('<i class="fa fa-floppy-o"></i>&nbsp;'.Yii::t('app','Save permission'), ['class' => 'btn btn-primary btn-sm']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$var = RBAC_FULL;
$js = <<< JS
    (function($){
        $('.check-item')
        .each(function(){
            var val = $(this).val();
            var tmp = val.split('/');
            if(tmp.length === 2){
                $(this).attr('data-parent', tmp[0]);
            }
        })
        .change(function(){
            var val = $(this).val();
            if(val === '{$var}'){
                $('.check-item[value!="'+val+'"]:visible').prop('checked', false);
            }else{
                $('.check-item[value="{$var}"]:checked:visible').prop('checked', false);
            }
            var parent = $(this).data('parent');
            if(parent){
                var childTotal = $('.check-item[data-parent="'+parent+'"]:visible').length;
                var childCheckedTotal = $('.check-item[data-parent="'+parent+'"]:checked:visible').length;
                var parentItem = $('.check-item[value="'+parent+'"]:visible');
                if(childTotal === childCheckedTotal){
                    parentItem.prop('checked', true);
                }else{
                    parentItem.prop('checked', false);
                }
            }else{
                $('.check-item[data-parent="'+val+'"]:visible').prop('checked', $(this).is(':checked'));
            }
        });
        $('.check-item:visible:checked').each(function(){
            $('.check-item[data-parent="'+$(this).val()+'"]:visible').prop('checked', true);
        });
    })(jQuery);
JS;
$this->registerJs($js);
?>