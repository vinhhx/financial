<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 30-Aug-16
 * Time: 4:19 AM
 */

$this->title = 'Require member' . $model->username . ' PH';
$this->params['breadcrumbs']['Customers'] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/admin/customer/index';
use yii\widgets\DetailView;

if ($model):
    ?>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'username',
        'status',
        'full_name',
        'phone',
        'email:email',
        'created_at:datetime',
    ],
]) ?>
    <?php
    if ($model->is_active == \app\models\Customer::IS_ACTIVE_COMPLETED && $package && $package->conditionPH()):
        $form = \yii\widgets\ActiveForm::begin();
        ?>
        <p>
            <?= \yii\helpers\Html::submitButton('Yêu cầu chuyển BTC', ['name' => 'require', 'value' => 'PH', 'class' => 'btn btn-sm btn-primary']) ?>
        </p>
        <?php
        \yii\widgets\ActiveForm::end();
    endif;
    ?>

<?php endif; ?>