<?php

use app\components\ExcelGrid;
use app\models\Customer;
use app\models\Package;
ExcelGrid::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'extension' => 'xlsx',
    'filename' => 'danhsachkhachhang_' . date('d-m-Y', time()),
    'properties' => [
        'creator' => 'Lincolnmgt',
        'title' => 'Danh sach khach hang',
        'subject' => 'Danh sach khach hang',
        'category' => 'Newsletter',
        'keywords' => '',
        'manager' => '',
        'description' => 'Danh sach khach hang',
        'company' => 'Lincolnmgt',
    ],
    'columns' => [
        'id',
        'username',
        'full_name',
        'phone',
        [
            'attribute' => 'sex',
            'content' => function ($data) {
                $arr = Customer::getSexLabels();
                return (isset($arr[$data->sex]) ? $arr[$data->sex] : '');
            }
        ],
        [
            'attribute' => 'package_id',
            'content' => function ($data) {
                return Package::getName($data->package_id);
            }
        ],
        [
            'attribute' => 'parent_id',
            'content' => function ($data) {
                return Customer::getName($data->parent_id);
            }
        ],
        [
            'attribute' => 'amount',
            'content' => function ($data) {
                return Yii::$app->formatter->asDecimal($data->amount, '1');
            }
        ],
        [
            'attribute' => 'status',
            'content' => function ($data) {
                return $data->getStatusLabel();
            }
        ],
        [
            'attribute' => 'date_join',
            'content' => function ($data) {
                return Yii::$app->formatter->asDate($data->date_join);
            }
        ],
    ],
]);
