<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 21-Dec-15
 * Time: 6:13 AM
 */

/* @var $this \yii\web\View*/
$this->title = Yii::t('customers', 'Customers Tree');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$urlTree= \yii\helpers\Url::toRoute('ajaxloadcustomertree') ;
$js=<<<EOD

var top_id=0;
var current_top=0;
var currentLv=0;
function showPrev(){
        $('.tree .top-parent').removeClass('hidden');
        if(currentLv > 2){
        $('.tree .prev-parent').removeClass('hidden');
        }else{
        $('.tree .prev-parent').addClass('hidden');
        }
    };
function hidePrev(){
        $('.tree .top-parent').addClass('hidden');
        $('.tree .prev-parent').addClass('hidden');
    };
function showtree(id,type) {
        current_top=id;
       // if (parseInt(id)) {
            $.ajax({
                url: '{$urlTree}',
                type: 'post',
                data: {'id': id,'type':type},
                dataType: "json",
                async: false,
                success: function (response) {
                    if (response.action) {
                        $('.tree .middle').html(response.html);
                        currentLv=parseInt(response.currentLv);
                        current_top=parseInt(response.id);
                    }
                }
            });

//        }else{
//        document.location.reload(true);
//        }
        showPrev();
    }
EOD;
$this->registerJs($js, \yii\web\View::POS_HEAD);
?>
<div class="box box-warning">
    <div class="box-body ">
        <div class="box-header">
            <h3 class="text-center"><?= Yii::t('customers', ' customer model') ?></h3>
        </div>
        <div class="tree">
            <div class="head-action">
                <p class="top-parent text-center hidden" > <a class=" fa fa-arrow-circle-up prev-top btn btn-sm btn-danger" href="javascript:void(0);" style="padding:2px 25px; margin-bottom:20px; font-size:20px;"></a></p>
                <p class="prev-parent text-center hidden"> <a class="fa fa-arrow-up prev-one-model  btn btn-sm btn-primary" href="javascript:void(0);" style="padding:4px 20px; font-size:16px;" ></a></p>
            </div>
            <div class="middle">
                <?php echo $model; ?>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript">

</script>
<?php
$js2=<<<EOD

 $('.prev-top').click(function(){
    showtree(0,0);
    hidePrev();
 });
 $('.prev-one-model').click(function(){
  showtree(current_top,1);
 });
EOD;
$this->registerJs($js2);

?>

<style>
    ul li a .view-more{
        display: none;
    }
    ul ul ul>li>a .view-more{
        display:block;
    }

    
    .i_user {
        display: inline-block;
        max-width: 40px;
    }
    .tree {
        overflow: auto;
    }
    .tree * {margin: 0; padding: 0;}
    .tree ul {
        padding-top: 20px; position: relative;
        display: flex;
        transition: all 0.5s;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
    }

    .tree li {
        float: left; text-align: center;
        list-style-type: none;
        position: relative;
        padding: 20px 5px 0 5px;

        transition: all 0.5s;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
    }

    /*We will use ::before and ::after to draw the connectors*/

    .tree li::before, .tree li::after{
        content: '';
        position: absolute; top: 0; right: 50%;
        border-top: 1px solid #ccc;
        width: 50%; height: 20px;
    }
    .tree li::after{
        right: auto; left: 50%;
        border-left: 1px solid #ccc;
    }

    /*We need to remove left-right connectors from elements without 
    any siblings*/
    .tree li:only-child::after, .tree li:only-child::before {
        display: none;
    }

    /*Remove space from the top of single children*/
    .tree li:only-child{ padding-top: 0;}

    /*Remove left connector from first child and 
    right connector from last child*/
    .tree li:first-child::before, .tree li:last-child::after{
        border: 0 none;
    }
    /*Adding back the vertical connector to the last nodes*/
    .tree li:last-child::before{
        border-right: 1px solid #ccc;
        border-radius: 0 5px 0 0;
        -webkit-border-radius: 0 5px 0 0;
        -moz-border-radius: 0 5px 0 0;
    }
    .tree li:first-child::after{
        border-radius: 5px 0 0 0;
        -webkit-border-radius: 5px 0 0 0;
        -moz-border-radius: 5px 0 0 0;
    }

    /*Time to add downward connectors from parents*/
    .tree ul ul::before{
        content: '';
        position: absolute; top: 0; left: 50%;
        border-left: 1px solid #ccc;
        width: 0; height: 20px;
    }

    .tree li a{
        border: 1px solid #ccc;
        padding: 5px 10px;
        text-decoration: none;
        color: #666;
        font-family: arial, verdana, tahoma;
        font-size: 13px;
        display: inline-block;

        border-radius: 5px;
        -webkit-border-radius: 5px;
        -moz-border-radius: 5px;

        transition: all 0.5s;
        -webkit-transition: all 0.5s;
        -moz-transition: all 0.5s;
    }

    /*Time for some hover effects*/
    /*We will apply the hover effect the the lineage of the element also*/
    .tree li a:hover, .tree li a:hover+ul li a {
        background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
    }
    /*Connector styles on hover*/
    .tree li a:hover+ul li::after, 
    .tree li a:hover+ul li::before, 
    .tree li a:hover+ul::before, 
    .tree li a:hover+ul ul::before{
        border-color:  #94a0b4;
    }
    .tree ul ul ul li>ul::before{
        border-left: none !important;
    }


</style>
