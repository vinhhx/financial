<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = Yii::t('customers', 'Update Bank Address');
$this->params['breadcrumbs']['Customers'] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/admin/customer/index';

?>
<div class="box box-widget">
    <div class="box-body">
        <?php $form = \yii\widgets\ActiveForm::begin() ?>
        <?= $form->field($model,'bank_address')->textInput()->hint('Nhập địa chỉ ví muốn chuyển thành') ?>
        <?= Html::submitButton('<span class="fa fa-floppy-o"></span>&nbsp; Lưu  ',['class'=>' btn btn-sm btn-primary']) ?>
        <?php \yii\widgets\ActiveForm::end(); ?>
    </div>
</div>
