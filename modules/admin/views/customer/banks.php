<?php

use yii\helpers\Html;
use app\extensions\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\search\News */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customer Bank Address');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/admin/customer/index';
?>
<div class="news-index">

    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'header' => 'bank_address',
                'format' => 'raw',
                'value' => function ($model) {
                    return Html::a($model->bank_address, ['customer/update-bank-address', 'id' => $model->id,'cusId'=>$model->customer_id], []);
                }
            ],
            'created_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'template' => '{update}',
                'buttons' => [

                    'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-pencil-square-o"></i>&nbsp;' . Yii::t('users', 'Update Bank Address'), ['update-bank-address','id' => $model->id,'cusId'=>$model->customer_id], ['class' => 'btn btn-xs btn-primary ']);
                    }
                ]
            ],

        ],
    ]); ?>
</div>
