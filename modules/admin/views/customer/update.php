<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = Yii::t('customers', 'Update {modelClass}: ', [
        'modelClass' => 'Customer',
    ]) . ' #' . $model->id;
$this->params['breadcrumbs']['Customers'] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/admin/customer/index';

?>
<div class="customer-update">

    <?= $this->render('_form', [
        'model' => $model,
        'isUpdated' => true
    ]) ?>

</div>
