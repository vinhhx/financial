<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\forms\search\CustomerRequireCashoutSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="form-search">
    <?php
    $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]);
    ?>
    <ul>
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'id', array('placeholder' => $model->getAttributeLabel('id'), 'style' => 'width:100px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'fullname', array('placeholder' =>  Yii::t('customers','Full Name'), 'style' => 'width:150px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'customer_username', array('placeholder' => $model->getAttributeLabel('customer_username'), 'style' => 'width:150px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'phone', array('placeholder' => Yii::t('customers','Phone'), 'style' => 'width:150px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeDropDownList($model, 'status', \app\models\CustomerRequireCashout::getStatusLabels(), array('prompt' => Yii::t('app', '-- Choice status --'), 'style' => 'width:150px')) ?>
        </li>
        <li class="form-item">
            <div class="form-group form-group-date" style="width: 200px; margin-bottom: -25px;">
                <label><?= $model->getAttributeLabel('created_at') ?></label>
                <div style="margin-top: -15px;">
                    <?=
                    \app\components\WGroupDatePicker::widget([
                        'model' => $model,
                        'attributes' => ['start_date', 'end_date'],
                        'formInline' => true,
                        'dateFormat' => 'dd-MM-yyyy',
                    ]);
                    ?>
                </div>
            </div>
        </li>
        <li class="form-item">
            <?= Html::submitButton('<i class="fa fa-search"></i>&nbsp;' . Yii::t('app', 'Search'), ['class' => 'btn btn-primary btn-sm']) ?>
            <?= Html::resetButton('<i class="fa fa-refresh"></i>&nbsp;' . Yii::t('app', 'reset'), ['class' => 'btn btn-default btn-sm btn-second-horizontal']) ?>
        </li>
    </ul>
    <?php ActiveForm::end(); ?>
</div>
