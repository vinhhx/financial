<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Video */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Video',
]) . $model->title;
$this->params['breadcrumbs'][Yii::t('app', 'Videos')] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][] = Yii::t('app', 'Videos');
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/admin/video/index';
?>
<div class="video-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
