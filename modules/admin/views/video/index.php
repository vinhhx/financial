<?php

use yii\helpers\Html;
use app\extensions\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\search\Video */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Videos');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="video-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'image',
                'content' => function ($model) {
                    if (!$model->getImage()) {
                        return '';
                    }
                    return Html::img($model->getImage(), ['width' => 80]);
                }
            ],
            'title',
            'video_url:url',
            // 'use_thumb',
            // 'video_thumb',
            // 'image',
            // 'status',
            // 'deleted',
            [
                'attribute' => 'status',
                'content' => function ($model) {
                    return $model->status ? '<i class="fa fa-check-square-o text-success"></i>' :
                        '<i class="fa fa-square-o text-danger"></i>';
                },
                'contentOptions' => [
                    'class' => 'text-center'
                ]
            ],
            'created_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'view' => function() {},
                    'update' => function($url, $model, $key) {
                        return Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-xs'
                        ]);
                    },
                    'delete' => function($url, $model, $key) {
                        return Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger btn-xs',
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post',
                            'data-pjax' => '0',
                        ]);
                    }
                ]
            ],
        ],
        'tools' => [
            Html::a('<i class="fa fa-plus-square-o"></i>&nbsp;' . Yii::t('app', 'Create news'), ['create'], ['class' => 'btn btn-success btn-sm'])
        ]
    ]); ?>
</div>
