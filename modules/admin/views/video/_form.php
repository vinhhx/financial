<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Video */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="video-form">

    <div class="box box-primary">
        <div class="box-body">

            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-lg-8">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'video_url')->textInput(['maxlength' => true]) ?>

                    <div class="preview-video" style="background-color: #e0e0e0; min-height: 300px;">

                    </div>
                </div>

                <div class="col-lg-4">

                    <?= $form->field($model, 'use_thumb')->checkbox() ?>

                    <?= $form->field($model, 'file')->fileInput() ?>
                    <p>
                        <?= $model->getImage() ? Html::img($model->getImage(), ['width' => '100%']) : '' ?>
                    </p>

                    <?= $form->field($model, 'status')->checkbox() ?>

                    <div class="form-group">
                        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                    </div>

                </div>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>

</div>
<?php
$youtubeID = $model->video_id ? $model->video_id : '';
$js = <<< JS
$(function() {
    var youtubeID = '{$youtubeID}';
    var u = $('#video-video_url'),
        p = $('.preview-video');
    u.on('paste', function(){
        setTimeout(function(){
            var url = u.val();
            var videoID = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
            if(videoID != null){
                var w = p.width(),
                    h = (315 / 560) * w;
                p.html('<iframe width="'+w+'" height="'+h+'" src="https://www.youtube.com/embed/'+videoID[1]+'" frameborder="0" allowfullscreen></iframe>');
            }
        }, 200);
    });

    if(youtubeID) {
        var w = p.width(),
            h = (315 / 560) * w;
        p.html('<iframe width="'+w+'" height="'+h+'" src="https://www.youtube.com/embed/'+youtubeID+'" frameborder="0" allowfullscreen></iframe>');
    }
});
JS;
$this->registerJs($js);
