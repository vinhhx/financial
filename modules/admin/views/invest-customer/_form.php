<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use app\models\Package;
use yii\helpers\ArrayHelper;
use app\models\Customer;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */


?>
<?php $form = ActiveForm::begin([]); ?>
<div class="row">
    <div class="col-sm-6">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('customers', 'Account') ?></h3>
            </div>
            <div class="box-body">

                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                <?php
                if (Yii::$app->request->get('ref', '')):
                else:
                    echo $form->field($model, 'customer_parent_id')->dropDownList(ArrayHelper::map($listCustomer, 'id', 'fullName'), ['prompt' => Yii::t('customers', ' -- Select account invited --'), 'id' => 'select-parent-customer']);
                endif;
                ?>
                <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            </div>

        </div>
    </div>
    <div class="col-sm-6">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('customers', 'Profile') ?></h3>
            </div>
            <div class="box-body">
                <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

                <?=
                $form->field($model, 'dob')->widget(DatePicker::className(), [
                    'options' => [
                        'class' => 'form-control',
                        'placeholder' => 'dd-mm-yyyy'
                    ],
                    'language' => 'vi',
                    'clientOptions' => [
                        'changeMonth' => true,
                        'changeYear' => true,
                        'yearRange' => '1960:2015'
                    ]
                ])
                ?>
                <?= $form->field($model, 'identity_card')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'nation')->dropDownList(\app\models\investments\InvestCustomer::getNationLabels()) ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="pull-right">
            <div class="form-group">
                <label class="control-label"></label>
                <?= Html::submitButton('<span class=" fa fa-floppy-o"></span>&nbsp;' . Yii::t('customer', 'Save customer'), ['class' => 'btn btn-primary btn-sm']) ?>
            </div>
        </div>
    </div>
</div>

<?php ActiveForm::end(); ?>
<?php
\app\assets\plugins\Select2PluginAsset::register($this);
$js = <<<EOD
if($("#select-parent-customer").length >0){
    $("#select-parent-customer").select2();
 }
EOD;
$this->registerJs($js);
?>
