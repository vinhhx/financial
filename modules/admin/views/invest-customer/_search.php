<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\forms\search\CustomerSearch */
/* @var $form yii\widgets\ActiveForm */

$package = \yii\helpers\ArrayHelper::map(\app\models\Package::getPackages(), 'id', 'package_name');
?>

<div class="customer-search form-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <ul style="margin-left:15px">
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'id', array('placeholder' => $model->getAttributeLabel('id'), 'style' => 'width:100px')) ?>
        </li>

        <li class="form-item">
            <?= Html::activeInput('text', $model, 'full_name', array('placeholder' => $model->getAttributeLabel('full_name'), 'style' => 'width:120px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'username', array('placeholder' => $model->getAttributeLabel('username'), 'style' => 'width:120px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'phone', array('placeholder' => $model->getAttributeLabel('phone'), 'style' => 'width:150px')) ?>
        </li>

        <li class="form-item">
            <?= Html::activeDropDownList($model, 'status', \app\models\investments\InvestCustomer::getStatusLabels(), array('prompt' => $model->getAttributeLabel('status'), 'style' => 'width:150px')) ?>
        </li>
        <li class="form-item">
            <div class="form-group form-group-date" style="width: 200px; margin-bottom: -25px;">
                <label>Date</label>

                <div style="margin-top: -15px;">
                    <?=
                    \app\components\WGroupDatePicker::widget([
                        'model' => $model,
                        'attributes' => ['start_date', 'end_date'],
                        'formInline' => true,
                        'dateFormat' => 'dd-MM-yyyy',
                    ]);
                    ?>
                </div>
            </div>
        </li>

        <li class="form-item">
            <?= Html::submitButton('<i class="fa fa-search"></i>&nbsp;Tìm kiếm', ['class' => 'btn btn-primary btn-sm']) ?>
            <?= Html::resetButton('<i class="fa fa-refresh"></i>&nbsp;Reset', ['class' => 'btn btn-default btn-sm btn-second-horizontal']) ?>
        </li>
    </ul>

    <?php ActiveForm::end(); ?>

</div>
