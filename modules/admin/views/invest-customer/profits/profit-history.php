<?php
use yii\helpers\Html;
use yii\grid\GridView;
use app\models\investments\InvestCustomerPackageHistory;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\invest\forms\InvestCustomerPackageHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = \Yii::t('investment', 'Lịch sử giao dịch của gói');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<?php echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
    'options' => ['class' => 'grid-view grid-view-customize'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

//        'bill_id',
        [
            'attribute' => 'type',
            'format' => 'html',
            'value' => function ($model) {
                return $model->getTypeLabel();
            }
        ],
        [
            'label' => 'Username',
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 130px;text-align:center; font-weight:bold', 'class' => 'text-primary'], // <-- set chiều rông
            'value' => function ($model) {
                return $model->investment->username;
            }
        ],
        [
            'attribute' => 'amount',
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 130px;text-align:right;'], // <-- set chiều rông
            'value' => function ($model) {
//                        $valueStr='';
                if ($model->sign == InvestCustomerPackageHistory::SIGN_SUB) {
                    return '<b class="text-green"> - </b><b class="text-red">' . \Yii::$app->formatter->asDecimal($model->amount, 8) . '</b>';
                } elseif ($model->sign == InvestCustomerPackageHistory::SIGN_ADD) {
                    return '<b class="text-red"> + </b><b class="text-red">' . \Yii::$app->formatter->asDecimal($model->amount, 8) . '</b>';
                } else {
                    return '<b class="text-red">' . \Yii::$app->formatter->asDecimal($model->amount, 8) . '</b>';
                }
            }
        ],
        [
            'label' => 'Package amount',
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 130px;text-align:right; font-weight:bold', 'class' => 'text-red'], // <-- set chiều rông
            'value' => function ($model) {
                return \Yii::$app->formatter->asDecimal($model->investPackage->amount, 8);
            }
        ],

        'created_at:datetime',

    ],
]); ?>

