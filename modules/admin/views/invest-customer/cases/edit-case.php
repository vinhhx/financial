<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 16-Oct-16
 * Time: 9:09 AM
 */
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = \Yii::t('investment', 'Sửa lại vị trí case');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$form = \yii\widgets\ActiveForm::begin();

?>
    <div class="box box-widget">
        <div class="box box-body">
            <?= $form->field($model, 'children_id')->dropDownList(ArrayHelper::map($listCustomer, 'id', 'fullName'), ['prompt' => 'Chọn tài khoản muốn chuyển đi']) ?>
            <?= $form->field($model, 'parent_id')->dropDownList(ArrayHelper::map($listCustomer, 'id', 'fullName'), ['prompt' => 'Chọn tài khoản muốn thêm chân']) ?>
            <div class="form-group">
                <label class="control-label"></label>
                <?= Html::submitButton('<span class=" fa fa-floppy-o"></span>&nbsp;' . Yii::t('customer', 'Change position'), ['class' => 'btn btn-primary btn-sm']) ?>
            </div>
        </div>
    </div>
<?php \yii\widgets\ActiveForm::end() ?>