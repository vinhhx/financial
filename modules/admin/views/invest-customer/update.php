<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = Yii::t('customers', 'Update Invest Customer');
$this->params['breadcrumbs']['Invest Customers'] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/admin/invest-customer/index';
?>
<div class="customer-create">

    <?= $this->render('_formUpdate', [
        'model' => $model,
        'listCustomer' => $listCustomer,
        'isUpdated' => true
    ]) ?>

</div>
