<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 10-Jun-16
 * Time: 9:13 AM
 */
use yii\widgets\DetailView;

if ($model):
    ?>
    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'username',
        'status',
        'full_name',
        'sex',
        'phone',
        'email:email',
        'nation',
        'address',
        'created_at:datetime',
    ],
]) ?>
    <?php
    $form = \yii\widgets\ActiveForm::begin();
    ?>
    <p>
        <?= \yii\helpers\Html::submitButton('Kích hoạt tài khoản', ['name' => 'active', 'value' => 'active', 'class' => 'btn btn-sm btn-warning']) ?>
    </p>
    <?php
    \yii\widgets\ActiveForm::end();
    ?>

<?php endif; ?>