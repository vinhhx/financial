<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\modules\admin\forms\ChangePasswordForm */

$this->title = Yii::t('app', 'Change Password');
$this->params['breadcrumbs'][] = $this->title;
$this->params["pageTitle"] = $this->title;
$this->params['parentAction'] = '/admin/customer/index';
?>

<div class="box box-warning">

    <div class="box-body">
        <?php $form = ActiveForm::begin(['id' => 'form-change-password']); ?>

        <?= $form->field($user, 'username')->textInput(['disabled' => true]); ?>
        <?= $form->field($model, 'newPassword')->passwordInput(); ?>
        <?= $form->field($model, 'newPasswordConfirmation')->passwordInput(); ?>
        <?php /*echo $form->field($model, 'captcha', [
            'template' => '{label} <div class="captcha-form">{input}</div><br/><div class="captcha-error col-md-12">{error}</div>',
        ])->widget(Captcha::className(), [
            'captchaAction'=>\yii\helpers\Url::to(['default/captcha']),
            'template' => '<div class="captcha-input col-md-4" style="padding-left:0px">{input}</div><div class="captcha-image col-md-4">{image}</div>']);*/ ?>
        <?= $form->field($model, 'captcha')->widget(
            \himiklab\yii2\recaptcha\ReCaptcha::className(),
            ['siteKey' => Yii::$app->params["GOOGLE_CAPTCHA_SITE_KEY"]]
        ) ?>
        <div class="form-group">
            <label class="control-label"></label>
            <?= Html::submitButton('<span class="fa fa-floppy-o"></span>&nbsp;' . Yii::t('customers', 'Save new password'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
