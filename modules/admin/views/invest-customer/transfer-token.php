<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 15-Jun-16
 * Time: 8:15 PM
 */
$this->title = 'Chuyển hoặc tạo token cho người chơi';
$this->params['pageTitle'] = $this->title;
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<?php $form = ActiveForm::begin([]); ?>
<div class="row">
    <div class="col-sm-6">
        <div class="box box-danger">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('customers', 'Add token form customer paid') ?></h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label class="label-control">
                        <?= $transferForm->getAttributeLabel('customer_receiver_id') ?>
                    </label>
                    <span class="form-control" disabled="disabled">
                        <?= $model->username ?>
                    </span>
                </div>
                <?= $form->field($transferForm, 'quantity')->textInput(['maxlength' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('<span class="fa fa-square-plus-o"></span>&nbsp; Tạo token', ['class' => 'btn btn-sm btn-success']) ?>
                </div>

            </div>
            <?php if (!empty($listToken)): ?>
                <div class="box-body">
                    <table class="table table-condensed">
                        <thead>
                        <th>#</th>
                        <th>Mã token</th>
                        </thead>
                        <tbody>
                        <?php $i = 1;
                        foreach ($listToken as $token): ?>
                            <tr>
                                <td width="30px" class="text-bold">
                                    <?= $i . '.' ?>

                                </td>
                                <td>
                                    <?= $token; ?>
                                </td>
                            </tr>
                            <?php $i++; endforeach; ?>
                        </tbody>
                    </table>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
<?php ActiveForm::end() ?>

