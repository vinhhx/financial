<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use \app\models\investments\InvestCustomer;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\InvestCustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('customers', 'Investor Customer');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="box box-primary">
    <div class="box-body">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
</div>
<div class="tools">
    <div class="pull-left">
        <?=
        Html::a('Export Excel', ['export-excel'] + Yii::$app->request->queryParams, [
            'class' => 'btn btn-info',
            'data-method' => 'post',
        ]);
        ?>
    </div>
    <div class="pull-right" style="margin-bottom: 20px ">
        <?= Html::a('<span class="fa fa-square-plus-o"></span>&nbsp;' . Yii::t('customers', 'Create Customer'), ['create'], ['class' => 'btn btn-success btn-sm']) ?>
    </div>
</div>


<div class="table-content" style="float: left; width: 100%;">

    <?php
    $gridColumns = [
//        ['class' => 'kartik\grid\SerialColumn',
//            'contentOptions' => ['class' => 'kartik-sheet-style'],
//            'width' => '36px',
//            'header' => '',
//            'headerOptions' => ['class' => 'kartik-sheet-style']
//        ],
        'id',
        [
            'header' => 'Message',
            'format' => 'raw',
            'value' => function ($model) {
                return Html::a('<i class="glyphicon glyphicon-envelope" style="font-size: 20px;"></i><span></span>', ['/admin/note/index'], [
                    'class' => 'add-note',
                    'data-table' => InvestCustomer::tableName(),
                    'data-id' => $model->id,
                    'data-toggle' => 'modal',
                    'data-target' => '#myModal',
                    'title' => 'Ghi chú',
                ]);
            }
        ],
        'username',
        'ref_code',
        'full_name',
        'email',
        'phone',
//        [
//            'attribute' => 'sex',
//            'format' => 'text', //raw, html
////                'filter' => Customer::getSexLabels(),
//            'content' => function ($data) {
//                $arr = Customer::getSexLabels();
//                return (isset($arr[$data->sex]) ? $arr[$data->sex] : '');
//            }
//        ],
//        [
//            'attribute' => 'package_id',
//            'format' => 'text', //raw, html
////                'filter' => ArrayHelper::map(Package::getPackages(), 'id', 'package_name'),
//            'content' => function ($data) {
//                return Package::getName($data->package_id);
//            }
//        ],
//        [
//            'attribute' => 'parent_id',
//            'format' => 'text', //raw, html
////                'filter' => ArrayHelper::map(Customer::getAllCustomer(), 'id', 'fullName'),
//            'content' => function ($data) {
//                return Customer::getName($data->parent_id);
//            }
//        ],
//        [
//            'attribute' => 'amount',
//            'content' => function ($data) {
//                return Yii::$app->formatter->asDecimal($data->amount, '1');
//            }
//        ],
        [
            'attribute' => 'is_active',
            'content' => function ($data) {
                /**
                 * @var $data InvestCustomer
                 */
                return (isset($data->getIsActiveLabels()[$data->is_active]) ? $data->getIsActiveLabels()[$data->is_active] : '--');
            }
        ],
        /*[
            'label' => 'Invest Bank Address',
            'content' => function ($data) {
                if ($data->bankAddress) {
                    return Html::a('<span class="fa fa-pencil-square-o"></span>&nbsp' . $data->bankAddress->bank_address,
                        ['update-bank-address', 'id' => $data->id],
                        [
                            'title' => 'Sửa địa chỉ nhận',
                            'class' => 'text-primary',
                        ]);
                }
                return Html::a('<span class="fa fa-plus-square-o"></span>&nbsp Add bank address ',
                    ['add-bank-address', 'id' => $data->id],
                    ['title' => 'Tạo mới địa chỉ nhận',
                        'class' => 'btn btn-success btn-flat btn-xs'
                    ]);
            }
        ],*/
        [
            'attribute' => 'created_at',
            'format' => 'text', //raw, html
            'filter' => false,
            'content' => function ($data) {
                return (isset($data->created_at) && $data->created_at > 0) ? Yii::$app->formatter->asDate($data->created_at) : '--';
            }
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'hAlign' => 'left',
            'headerOptions' => ['width' => '150px;', 'style' => 'width: 150px;'],
            'width' => '100px',
            'contentOptions' => ['style' => 'width: 150px;'],
            'template' => '{groups}',
            'buttons' => [
                'groups' => function ($url, $data) {
                    $html = '';
                    $html .= '<div class="btn-group">';
                    $html .= '<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Xử lý ... <span class="caret"></span>';
                    $html .= '</button>';
                    $html .= '<ul class="dropdown-menu " style="right: 0; left:auto">';
                    $html .= '<li class="bg-blue" >' . Html::a('<span class="fa fa-pencil-square-o"></span>&nbsp;' . Yii::t('app', 'Edit'), \yii\helpers\Url::to(['invest-customer/update', 'id' => $data->id]), ['class' => '', 'title' => Yii::t('app', 'Edit')]) . '</li>';
                    $html .= '<li class="bg-aqua">';
                    if ($data->is_active == InvestCustomer::IS_ACTIVE_PENDING):
                        $html .= Html::a('<span class="fa fa-check-square-o"></span>&nbsp;' . Yii::t('app', 'Active'), \yii\helpers\Url::to(['invest-customer/active', 'id' => $data->id]), ['class' => '', 'title' => Yii::t('app', 'Active')]);
                    elseif ($data->is_active == InvestCustomer::IS_ACTIVE_COMPLETED):
                        $html .= Html::a('<span class="fa fa-minus-square-o"></span>&nbsp;' . Yii::t('app', 'Unactive'), \yii\helpers\Url::to(['invest-customer/unactive', 'id' => $data->id]), ['class' => '', 'title' => Yii::t('app', 'Unactive')]);
                    endif;
                    $html .= '</li>';
                    $html .= '<li class="bg-yellow">';
                    $html .= Html::a('<span class="fa fa-key"></span>&nbsp;' . Yii::t('app', 'Change Password'), \yii\helpers\Url::to(['invest-customer/change-password', 'id' => $data->id]), ['class' => '', 'title' => Yii::t('app', 'Change Password')]);
                    $html .= '</li>';
                    $html .= '<li class="bg-green">';
                    $html .= Html::a('<span class="fa fa-plus-square-o"></span>&nbsp;' . Yii::t('app', 'Add token'), \yii\helpers\Url::to(['invest-customer/transfer-token', 'id' => $data->id]), ['class' => '', 'title' => Yii::t('app', 'Add token')]);
                    $html .= '</li>';
                    $html .= '<li class="bg-blue">';
                    if ($data->status == InvestCustomer::STATUS_ACTIVE):
                        $html .= Html::a('<span class="fa fa-key"></span>&nbsp;' . Yii::t('app', 'Lock User'), \yii\helpers\Url::to(['invest-customer/lock-user', 'id' => $data->id]), ['class' => '', 'title' => Yii::t('app', 'Lock user')]);
                    elseif ($data->status == InvestCustomer::STATUS_INACTIVE):
                        $html .= Html::a('<span class="fa fa-key"></span>&nbsp;' . Yii::t('app', 'Unlock User'), \yii\helpers\Url::to(['invest-customer/lock-user', 'id' => $data->id]), ['class' => '', 'title' => Yii::t('app', 'Unlock user')]);
                    endif;
                    $html .= '</li>';

                    if ($data->bankAddress) {
                        $urlBankAddress = \yii\helpers\Url::to(['update-bank-address', 'id' => $data->id]);
                    } else {
                        $urlBankAddress = \yii\helpers\Url::to(['add-bank-address', 'id' => $data->id]);
                    }
                    $html .= '<li class="bg-maroon">';
                    $html .= Html::a('<span class="fa fa-btc"></span>&nbsp;' . Yii::t('app', 'Bank Address'), $urlBankAddress, [
                        'class' => '',
                    ]);
                    $html .= '</li>';

                    $html .= '</ul>';
                    $html .= '</div>';
                    return $html;
                },


            ]
        ],
    ];

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
        'headerRowOptions' => ['class' => 'kartik-sheet-style'],
        //    'beforeHeader' => [
        //        [
        //            'columns' => [
        //                ['content' => 'Header Before 1', 'options' => ['colspan' => 4, 'class' => 'text-center warning']],
        //                ['content' => 'Header Before 2', 'options' => ['colspan' => 4, 'class' => 'text-center warning']],
        //                ['content' => 'Header Before 3', 'options' => ['colspan' => 3, 'class' => 'text-center warning']],
        //            ],
        //            'options' => ['class' => 'skip-export'] // remove this row from export
        //        ]
        //    ],
        'toolbar' => [
            '{export}',
            '{toggleData}'
        ],
        'exportConfig' => [
            GridView::CSV => ['label' => 'Save as CSV', 'filename' => Yii::t('customers', 'customerList') . date('Y-m-d', time()),],
            GridView::EXCEL => ['label' => 'Save as Excel', 'filename' => Yii::t('customers', 'customerList') . date('Y-m-d', time())],
            GridView::PDF => ['label' => 'Save as PDF', 'filename' => Yii::t('customers', 'customerList') . date('Y-m-d', time())],

        ],
        'export' => [
            'target' => GridView::TARGET_SELF,
        ],

        //    'target'=> ExportMenu::TARGET_SELF,

        'pjax' => true,
        'bordered' => true,
        'striped' => false,
        'condensed' => false,
        'responsive' => false,
        'responsiveWrap' => false,
        'persistResize' => false,
        'hover' => true,
        'floatHeader' => true,
        'floatHeaderOptions' => [],
        //    'floatHeaderOptions' => ['scrollingTop' => '50px'],
//            'showPageSummary' => true,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY
        ],
    ]);

    ?>
</div>

<div class="modal fade" id="pageModal" tabindex="-1" role="dialog" aria-labelledby="pageModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
            </div>

        </div>
    </div>
</div>

<style>
    .add-note span {
        top: -15px !important;
        right: -10px !important;
    }

    .col-md-2.col-sm-3.col-xs-6 {
        width: 20% !important;
    }

    .dropdown-menu li a {
        color: #fff;
    }

    .dropdown-menu li a:hover {
        background: inherit;
    }
</style>


