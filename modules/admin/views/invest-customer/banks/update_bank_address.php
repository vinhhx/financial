<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 15-Oct-16
 * Time: 11:48 AM
 */
$this->title = 'Cập nhật địa chỉ bitcoin cho người chơi #' . $customer->username;
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = ['customer/index'];

use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>
<?php
$form = ActiveForm::begin();
?>
<div class="box box-widget">
    <div class="box-body">
        <?= $form->field($model, 'bank_address')->textInput(['maxlength' => true]) ?>
        <div class="form-group">
            <?= Html::submitButton('<span class="fa fa-pencil-square-o"></span>&nbsp; Cập nhật địa chỉ bitcoin', ['class' => 'btn btn-sm btn-success']) ?>
        </div>

    </div>
</div>
<?php ActiveForm::end(); ?>
