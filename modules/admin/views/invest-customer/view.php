<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\investments\InvestCustomer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Invest Customers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invest-customer-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'ref_code',
            'auth_key',
            'password_hash',
            'password_reset_token',
            'status',
            'is_active',
            'is_deleted',
            'parent_id',
            'package_id',
            'amount',
            'full_name',
            'email:email',
            'phone',
            'sex',
            'dob',
            'identity_card',
            'nation',
            'address',
            'date_join',
            'last_auto_increase',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
