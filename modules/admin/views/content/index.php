<?php

use yii\helpers\Html;
use app\extensions\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\search\Content */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Contents');
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'name',
            [
                'attribute' => 'type_id',
                'content' => function ($model) {
                    return isset($model->typeLabels[$model->type_id]) ? $model->typeLabels[$model->type_id] : '--';
                }
            ],
            [
                'attribute' => 'status',
                'content' => function ($model) {
                    return $model->status ? '<i class="fa fa-check-square-o text-success"></i>' :
                        '<i class="fa fa-square-o text-danger"></i>';
                },
                'contentOptions' => [
                    'class' => 'text-center'
                ]
            ],
            'created_at:datetime',

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'tools' => [
            Html::a('<i class="fa fa-plus-square-o"></i>&nbsp;' . Yii::t('users', 'Create content'), ['create'], ['class' => 'btn btn-success btn-sm'])
        ]
    ]); ?>
</div>
