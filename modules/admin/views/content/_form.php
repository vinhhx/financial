<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\widgets\SymbolPicker;
use app\modules\admin\widgets\Editor;

/* @var $this yii\web\View */
/* @var $model app\models\Content */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="content-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-success">
        <div class="box-body">
            <div class="row">
                <div class="col-lg-8">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'content')->widget(Editor::className(), [
                        'options' => ['height' => 600]
                    ]) ?>

                </div>

                <div class="col-lg-4">
                    <?= $form->field($model, 'type_id')->dropDownList($model->getTypeLabels()) ?>

                    <?= $form->field($model, 'config')->widget(SymbolPicker::className()) ?>

                    <?= $form->field($model, 'status')->checkbox() ?>

                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
