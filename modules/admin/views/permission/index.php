<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
/* @var array $items */
/* @var array $roles */
/* @var array $selected */

$this->title = Yii::t('users', 'Permission Manager');
$this->params['pageTitle'] = $this->title;
$this->params['pageDescription'] = '';
$this->params['breadcrumbs'] = [
    $this->title

];
if (isset($items[$model->name])) {
    unset($items[$model->name]);
}
// css
$css = <<< CSS
.drop-down-list > .items input{
    float: left;
    margin-right: 5px;
}
CSS;
$this->registerCss($css);
?>
    <div class="auth-item-index">

        <?php $form = ActiveForm::begin([
            'id' => 'form',
        ]); ?>

        <div class="box box-primary">
            <div class="box-body">

                <div class="row">
                    <div class="col-md-4">
                        <?php if (!empty($roles)): ?>
                            <div class="form-group">
                                <label for=""><?= Yii::t('users', 'Update permission') ?></label>
                                <?= Html::dropDownList('', $model->name, $roles, ['class' => 'form-control', 'id' => 'goto-role', 'prompt' => Yii::t('users', 'Create new permission')]) ?>
                            </div>
                        <?php endif; ?>

                        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => $model->created_at ? true : false]) ?>

                        <?= $form->field($model, 'description')->textarea(['rows' => 1, 'disabled' => $model->created_at ? true : false]) ?>

                        <div class="form-group">
                            <?= Html::submitButton(!$model->created_at ? '<i class="fa fa-plus-square-o"></i>&nbsp;' . Yii::t('users', 'Create permission') : '<i class="fa fa-floppy-o"></i>&nbsp;' . Yii::t('users', 'Save permission'), ['class' => !$model->created_at ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <?= Html::checkboxList('data', $selected, $items, [
                            'class' => 'form-group drop-down-list',
                            'item' => function ($index, $label, $name, $checked, $value) {
                                return '<div class="items">' .
                                '<label>' . Html::checkbox($name, $checked, ['value' => $value, 'class' => 'check-item']) . ' ' . $label . '</label>' .
                                '</div>';
                            }
                        ]) ?>
                        <div class="from-group">
                            <?= Html::a('Update Permission System', ['update'], ['data-method' => 'post', 'class' => 'btn btn-success btn-update']) ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
<?php
$var = RBAC_FULL;
$js = <<< JS
    (function($){
        $('#goto-role').change(function(){
            var url = $(this.form).attr('action').split('?');
            location.href = url[0] + ($(this).val() ? '?item=' +$(this).val() : '');
        });

        $('.check-item')
        .each(function(){
            var val = $(this).val();
            var tmp = val.split('/');
            if(tmp.length === 2){
                $(this).attr('data-parent', tmp[0]);
            }
        })
        .change(function(){
            var val = $(this).val();
            if(val === '{$var}'){
                $('.check-item[value!="'+val+'"]:visible').prop('checked', false);
            }else{
                $('.check-item[value="{$var}"]:checked:visible').prop('checked', false);
            }
            var parent = $(this).data('parent');
            if(parent){
                var childTotal = $('.check-item[data-parent="'+parent+'"]:visible').length;
                var childCheckedTotal = $('.check-item[data-parent="'+parent+'"]:checked:visible').length;
                var parentItem = $('.check-item[value="'+parent+'"]:visible');
                if(childTotal === childCheckedTotal){
                    parentItem.prop('checked', true);
                }else{
                    parentItem.prop('checked', false);
                }
            }else{
                $('.check-item[data-parent="'+val+'"]:visible').prop('checked', $(this).is(':checked'));
            }
        });
        $('.check-item:visible:checked').each(function(){
            $('.check-item[data-parent="'+$(this).val()+'"]:visible').prop('checked', true);
        });

        $('.btn-update').click(function(){
            $(this).addClass('disabled');
            $.post($(this).attr('href'), {}, function(){
                location.reload();
            });
        });
    })(jQuery);
JS;
$this->registerJs($js);
?>