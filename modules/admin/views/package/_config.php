<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 27-Jul-16
 * Time: 10:27 AM
 */
use yii\helpers\Html;

if ($model->config):
    $config = $model->config;
    $invest = isset($config["investBonus"])?$config["investBonus"]:[];
    $investF1 = (isset($invest) && isset($invest["F1"])) ? $invest["F1"] : 0;
    $investF2 = (isset($invest) && isset($invest["F2"])) ? $invest["F2"] : 0;
    $investF3 = (isset($invest) && isset($invest["F3"])) ? $invest["F3"] : 0;
    $investF4 = (isset($invest) && isset($invest["F4"])) ? $invest["F4"] : 0;
    $investF5 = (isset($invest) && isset($invest["F5"])) ? $invest["F5"] : 0;

    $reInvest = isset($config["reinvestmentBonus"])?$config["reinvestmentBonus"]:[];
    $reInvestF1 = (isset($reInvest) && isset($reInvest["F1"])) ? $reInvest["F1"] : 0;
    $reInvestF2 = (isset($reInvest) && isset($reInvest["F2"])) ? $reInvest["F2"] : 0;
    $reInvestF3 = (isset($reInvest) && isset($reInvest["F3"])) ? $reInvest["F3"] : 0;
    $reInvestF4 = (isset($reInvest) && isset($reInvest["F4"])) ? $reInvest["F4"] : 0;
    $reInvestF5 = (isset($reInvest) && isset($reInvest["F5"])) ? $reInvest["F5"] : 0;
    ?>
    <div class="row">
        <div class="col-sm-6">
            <div class="box box-widget">
                <div class="box-header  with-border">
                    <h4 class="box-title">Cài đặt lãi đầu tư</h4>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">F1</span>
                            <?= Html::input('text', 'Package[config][investBonus][F1]', $investF1, ['class' => 'form-control']) ?>
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">F2</span>
                            <?= Html::input('text', 'Package[config][investBonus][F2]', $investF2, ['class' => 'form-control']) ?>
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">F3</span>
                            <?= Html::input('text', 'Package[config][investBonus][F3]', $investF3, ['class' => 'form-control']) ?>
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">F4</span>
                            <?= Html::input('text', 'Package[config][investBonus][F4]', $investF4, ['class' => 'form-control']) ?>
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">F5</span>
                            <?= Html::input('text', 'Package[config][investBonus][F5]', $investF5, ['class' => 'form-control']) ?>
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="box box-widget">
                <div class="box-header  with-border">
                    <h4 class="box-title">Cài đặt lãi tái đầu tư</h4>
                </div>
                <div class="box-body">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">F1</span>
                            <?= Html::input('text', 'Package[config][reinvestmentBonus][F1]',$reInvestF1, ['class' => 'form-control']) ?>
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">F2</span>
                            <?= Html::input('text', 'Package[config][reinvestmentBonus][F2]', $reInvestF2, ['class' => 'form-control']) ?>
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">F3</span>
                            <?= Html::input('text', 'Package[config][reinvestmentBonus][F3]', $reInvestF3, ['class' => 'form-control']) ?>
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">F4</span>
                            <?= Html::input('text', 'Package[config][reinvestmentBonus][F4]', $reInvestF4, ['class' => 'form-control']) ?>
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon">F5</span>
                            <?= Html::input('text', 'Package[config][reinvestmentBonus][F5]', $reInvestF5, ['class' => 'form-control']) ?>
                            <span class="input-group-addon">%</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
