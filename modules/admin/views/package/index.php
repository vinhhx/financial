<?php

use yii\helpers\Html;
use app\extensions\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\PackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('packages', 'Packages');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="box box-primary">
    <div class="box-body">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
</div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        'package_name',
        [
            'attribute' => 'amount',
            'contentOptions' => [
                'class' => 'text-red text-bold',
            ],
            'content' => function ($data) {
                return $data->amount ? Yii::$app->formatter->asDecimal($data->amount) : '0';
            }
        ],
        [
            'attribute' => 'increase_percent',
            'contentOptions' => [
                'class' => 'text-red text-bold',
            ],
            'content' => function ($data) {
                return $data->increase_percent ? Yii::$app->formatter->asDecimal($data->increase_percent,2) : '0';
            }
        ],
        [
            'attribute' => 'status',
            'content' => function ($data) {
                /* @var $data app\models\Package */
                $statusLabels = $data->getStatusLabels();
                return isset($statusLabels[$data->status]) ? $statusLabels[$data->status] : '--';
            }
        ],
        'created_at:datetime',
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Action',
            'template' => '{view}<br>{update}<br>{permission}',
            'buttons' => [
                'view' => function ($url, $model) {
                    return Html::a('<i class="fa fa-eye"></i>&nbsp;' . Yii::t('users', 'Detail'), ['view', 'id' => $model->id], ['class' => 'btn btn-xs btn-info']);
                },
                'update' => function ($url, $model) {
                    return Html::a('<i class="fa fa-pencil-square-o"></i>&nbsp;' . Yii::t('users', 'Edit'), ['update', 'id' => $model->id], ['class' => 'btn btn-xs btn-primary btn-second-vertical']);
                }
            ]
        ],
    ],
    'tools' => [
        Html::a('<i class="fa fa-plus-square-o"></i>&nbsp;' . Yii::t('users', 'Create new package'), ['create'], ['class' => 'btn btn-success btn-sm'])
    ]
]); ?>
