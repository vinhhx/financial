<?php

use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Package */

$this->title = Yii::t('packages', 'Create Package');
$this->params['breadcrumbs'][Yii::t('packages', 'Packages')] = Url::to(['package/index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="package-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
