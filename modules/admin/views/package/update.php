<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Package */

$this->title = Yii::t('packages', 'Update {modelClass}: ', [
        'modelClass' => 'Package',
    ]) . ' ' . $model->package_name;
$this->params['breadcrumbs'][Yii::t('packages', 'Packages')] = Url::to(['package/index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="package-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
