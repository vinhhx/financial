<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this \yii\web\View */

$this->title = Yii::t('app','Admin Page');
$this->params['pageTitle'] = $this->title;
?>
<div class="row default-admin">
    <div class="col-md-4">
        <div class="info-box">
            <?= Html::a('<span class="info-box-icon bg-aqua"><i class="fa fa-sitemap"></i></span>', ['customer/index'])?>
            <div class="info-box-content">
                <span class="info-box-text">
                    <?= Html::a(Yii::t('app','Customer manager'), ['customer/index'])?>
                </span>
                <!--<span class="info-box-number">93,139</span>-->
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="info-box">
            <?= Html::a('<span class="info-box-icon bg-red"><i class="fa fa-usd"></i></span>', ['withdrawal/index'])?>
            <div class="info-box-content">
                <span class="info-box-text">
                    <?= Html::a(Yii::t('app','Request withdrawal'), ['withdrawal/index'])?>
                </span>
                <!--<span class="info-box-number">3,139</span>-->
            </div>
        </div>
    </div>


    <div class="col-md-4">
        <div class="info-box">
            <?= Html::a('<span class="info-box-icon bg-yellow"><i class="fa fa-exchange"></i></span>', ['transaction/index'])?>
            <div class="info-box-content">
                <span class="info-box-text">
                    <?= Html::a(Yii::t('app',' Transactions history'), ['transaction/index'])?>
                </span>
                <!--<span class="info-box-number">3,139</span>-->
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="info-box">
            <?= Html::a('<span class="info-box-icon bg-green"><i class="fa fa-pie-chart"></i></span>', ['report/index'])?>
            <div class="info-box-content">
                <span class="info-box-text">
                    <?= Html::a(Yii::t('app','Statistics'), ['statistic/statistic-total-cashout'])?>
                </span>
                <!--<span class="info-box-number">3,139</span>-->
            </div>
        </div>
    </div>

    <div class="col-md-4">
        <div class="info-box">
            <?= Html::a('<span class="info-box-icon bg-maroon"><i class="fa fa-gears"></i></span>', ['user/index'])?>
            <div class="info-box-content">
                <span class="info-box-text">
                    <?= Html::a(Yii::t('app',' System Manager'), ['user/index'])?>
                </span>
                <!--<span class="info-box-number">3,139</span>-->
            </div>
        </div>
    </div>


</div>