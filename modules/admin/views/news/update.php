<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\News */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'News',
]) . $model->title;
$this->params['breadcrumbs'][Yii::t('app', 'News')] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][] = Yii::t('app', 'News');
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/admin/news/index';
?>
<div class="news-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
