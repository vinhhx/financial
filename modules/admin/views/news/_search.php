<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\forms\search\News */
/* @var $form yii\widgets\ActiveForm */
$package = \yii\helpers\ArrayHelper::map(\app\models\Package::getPackages(), 'id', 'package_name');
?>

<div class="customer-search form-search">

    <div class="box-primary box box-body">
        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <ul style="margin: 0; padding: 0;">
            <li class="form-item">
                <?= Html::activeInput('text', $model, 'id', array('placeholder' => $model->getAttributeLabel('id'), 'style' => 'width:100px')) ?>
            </li>

            <li class="form-item">
                <?= Html::activeInput('text', $model, 'title', array('placeholder' => $model->getAttributeLabel('title'), 'style' => 'width:120px')) ?>
            </li>
            <li class="form-item">
                <?= Html::activeDropDownList($model, 'news_category_id', $model->getCategoryOptions(), array('prompt' => '-- All categories --', 'style' => 'width:220px')) ?>
            </li>

            <li class="form-item">
                <?= Html::activeDropDownList($model, 'status', \app\models\Customer::getStatusLabels(), array('prompt' => $model->getAttributeLabel('status'), 'style' => 'width:150px')) ?>
            </li>

            <li class="form-item">
                <?= Html::submitButton('<i class="fa fa-search"></i>&nbsp;Tìm kiếm', ['class' => 'btn btn-primary btn-sm']) ?>
                <?= Html::resetButton('<i class="fa fa-refresh"></i>&nbsp;Reset', ['class' => 'btn btn-default btn-sm btn-second-horizontal']) ?>
            </li>
        </ul>

        <?php ActiveForm::end(); ?>
    </div>

</div>
