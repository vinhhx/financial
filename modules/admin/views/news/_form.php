<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\admin\widgets\Editor;

/* @var $this yii\web\View */
/* @var $model app\models\News */
/* @var $form yii\widgets\ActiveForm */

$this->registerCss('a[href="https://froala.com/wysiwyg-editor"]{ display: none !important; }')
?>

<div class="news-form">

    <?php $form = ActiveForm::begin([
        'options' => ['enctype' => 'multipart/form-data']
    ]); ?>

    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-body">
                    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'intro')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'content')->widget(Editor::className(), [
                        'options' => ['height' => 600]
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="box box-success">
                <div class="box-body">

                    <?= $form->field($model, 'news_category_id')->dropDownList($model->getCategoryOptions(), [
                        'prompt' => '-- Select one --'
                    ]) ?>

                    <?= $form->field($model, 'file')->fileInput() ?>

                    <p>
                        <?= $model->image ? Html::img($model->getImage(), ['width' => '100%']) : '' ?>
                    </p>

                    <?= $form->field($model, 'status')->checkbox() ?>

                    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
