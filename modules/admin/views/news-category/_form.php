<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\NewsCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="news-category-form">

    <div class="box box-success">
        <div class="box-body">
            <?php $form = ActiveForm::begin([
                'action' => $model->isNewRecord ? ['create'] : ['update', 'id' => $model->id]
            ]); ?>

            <?= $form->field($model, 'parent_id')->dropDownList($model->getItemOptionByLevel()) ?>

            <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'intro')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'status')->checkbox() ?>

            <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
