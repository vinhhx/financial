<?php

use yii\helpers\Html;

$type = isset($type) ? $type : 'cashout'
?>
<div class="pull-right" style=" padding:15px;">
    <?php switch ($type) { ?>
<?php case"request-cashout":
        default: ?>
            <p>
                <label><?= Yii::t('statistics', 'Total request cashout') ?> &nbsp;</label>
            <span
                class="text-danger"><b><?= Yii::$app->formatter->asInteger($dataTotal["total_request"]) ?></b></span>
            </p>
            <p>
                <label><?= Yii::t('statistics', 'Total amount request cashout') ?> &nbsp;</label>
            <span
                class="text-danger"><b><?= Yii::$app->formatter->asInteger($dataTotal["total_amount"]) ?></b></span>
            </p>
            <?php break; ?>

            <?php case"cashout":
        default: ?>
            <p>
                <label><?= Yii::t('statistics', 'Total User') ?> &nbsp;</label>
            <span
                class="text-danger"><b><?= Yii::$app->formatter->asInteger($dataTotal["total_customer"]) ?></b></span>
            </p>
            <p>
                <label><?= Yii::t('statistics', 'Total request cashout') ?> &nbsp;</label>
            <span
                class="text-danger"><b><?= Yii::$app->formatter->asInteger($dataTotal["total_required"]) ?></b></span>
            </p>
            <p>
                <label><?= Yii::t('statistics', 'Total amount request cashout') ?> &nbsp;</label>
            <span
                class="text-danger"><b><?= Yii::$app->formatter->asInteger($dataTotal["total_amount"]) ?></b></span>
            </p>
            <?php break; ?>

        <?php } ?>
</div>

