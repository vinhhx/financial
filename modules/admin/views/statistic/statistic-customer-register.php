<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 21-Dec-15
 * Time: 3:25 AM
 */

$this->title = 'Thống kê khách hàng đăng ký tài khoản';
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'] = [
    $this->title
];
$title = "";
if ($modelSearch->start_date && $modelSearch->end_date) {
    $title = Yii::t('statistics', 'Statistic Customer join to System from ') . date('d-m-Y', strtotime($modelSearch->start_date)) . Yii::t('statistics', ' to ') . date('d-m-Y', strtotime($modelSearch->end_date));

} elseif ($modelSearch->start_date) {
    $title = Yii::t('statistics', 'Statistic Customer join to System from ') . date('d-m-Y', strtotime($modelSearch->start_date));
} elseif ($modelSearch->end_date) {
    $title = Yii::t('statistics', 'Statistic Customer join to System last 30 days') . date('d-m-Y', strtotime($modelSearch->end_date));

}
?>
<div class="box box-primary">
    <div class="box-body">
        <?= $this->render('_search', ['model' => $modelSearch]) ?>
    </div>
</div>

<div class="box box-warning">


    <div class="box-body table-responsive no-padđing">
        <table id="table-statistic" class="table table-bordered table-hover">
            <thead>
            <tr>
                <td colspan="2" class="text-center">
                    <b> <?= $title ?></b>
                </td>
            </tr>
            <tr>
                <td class="text-left" rowspan="2" style="width:150px;">
                    <b><?= Yii::t('statistics', 'Date') ?></b>
                </td>
                <td class="text-left"><b><?= Yii::t('statistics', 'Customer') ?></b></td>
            </tr>
            <tr>
                <td><?= Yii::t('statistics', 'Quantity') ?></td>

            </tr>
            </thead>
            <tbody>

            <?php
            $totalCustomer = 0;

            foreach ($data as $key => $row):
                $userDay = 0;

                ?>
                <tr>
                    <td><?= $dateShow = date('d-m-Y', strtotime($row["date_join"])); ?>
                    </td>
                    <td class="text-left">
                        <?php

                        echo Yii::$app->formatter->asInteger($row["quantity"]);
                        $totalCustomer += $row["quantity"];
                        ?>
                    </td>

                </tr>
            <?php endforeach; ?>
            <tr class="total-heading text-danger">
                <td><b><?= Yii::t('statistics', 'Total') ?></b></td>
                <td class="text-left"><b><?= Yii::$app->formatter->asInteger($totalCustomer) ?></b></td>

            </tr>
            </tbody>
        </table>
    </div>
</div>