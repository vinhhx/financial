<?php

use app\models\Customer;
use app\models\Package;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use  \kartik\grid\GridView;
use kartik\export\ExportMenu;

$gridColumns = [
    ['class' => 'kartik\grid\SerialColumn'],
    'id',
    'username',
    'full_name',
    [
        'attribute' => 'package_id',
        'format' => 'text', //raw, html
        'filter' => ArrayHelper::map(Package::getPackages(), 'id', 'package_name'),
        'content' => function ($data) {
            return Package::getName($data["package_id"]);
        },
        'pageSummary'=>'Total',
    ],
    [
        'attribute' => 'amount',
        'content' => function ($data) {
            return Yii::$app->formatter->asDecimal($data["amount"], '0');
        },
        'pageSummary'=>true,
        'format'=>['decimal', 0],
    ],
    [
        'label' => Yii::t('app','Amount (VNĐ)'),
        'content' => function ($data) {
            $exchange=$data["amount"]*Yii::$app->params['exchange']['out'];
            return Yii::$app->formatter->asDecimal($exchange, '0');
        },
        'pageSummary'=>true,
        'format'=>['decimal', 0],
    ],



    [
        'attribute' => 'date_join',
        'format' => 'text', //raw, html
        'filter' => false,
        'content' => function ($data) {
            return Yii::$app->formatter->asDate($data["date_join"]);
        }
    ],
    [
        'label' => Yii::t('customers', 'Total Required'),
        'format' => 'text', //raw, html
        'filter' => false,
        'content' => function ($data) {
            return Yii::$app->formatter->asInteger($data["total_required"], 0);
        }
    ],
    [
        'label' => Yii::t('customers', 'Total Amount'),
        'format' => 'text', //raw, html
        'filter' => false,
        'content' => function ($data) {
            return Yii::$app->formatter->asDecimal($data["total_amount"], 0);
        }
    ],
];

echo GridView::widget([
    'dataProvider' => $dataProvider,
    //'filterModel' => $searchModel,
    'columns' => $gridColumns,
    'containerOptions' => ['style' => 'overflow: auto'], // only set when $responsive = false
//    'beforeHeader' => [
//        [
//            'columns' => [
//                ['content' => 'Header Before 1', 'options' => ['colspan' => 4, 'class' => 'text-center warning']],
//                ['content' => 'Header Before 2', 'options' => ['colspan' => 4, 'class' => 'text-center warning']],
//                ['content' => 'Header Before 3', 'options' => ['colspan' => 3, 'class' => 'text-center warning']],
//            ],
//            'options' => ['class' => 'skip-export'] // remove this row from export
//        ]
//    ],
    'toolbar' => [
        '{export}',
        '{toggleData}'
    ],
    'exportConfig' => [
        GridView::CSV => ['label' => 'Save as CSV','filename' => Yii::t('customers', 'customerCashout') . date('Y-m-d', time())],
        GridView::EXCEL => ['label' => 'Save as Excel','filename' => Yii::t('customers', 'customerCashout') . date('Y-m-d', time())],
        GridView::PDF => ['label' => 'Save as PDF','filename' => Yii::t('customers', 'customerCashout') . date('Y-m-d', time())],

    ],
    'export'=>[
        'target'=>GridView::TARGET_SELF,
    ],

//    'target'=> ExportMenu::TARGET_SELF,

    'pjax' => true,
    'bordered' => true,
    'striped' => false,
    'condensed' => false,
    'responsive' => false,
    'responsiveWrap' => false,
    'hover' => true,
    'floatHeader' => true,
    'floatHeaderOptions'=>[],
//    'floatHeaderOptions' => ['scrollingTop' => '50px'],
    'showPageSummary' => true,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY
    ],

]);
