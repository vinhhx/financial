<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Package;
use yii\helpers\ArrayHelper;
use app\models\Customer;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\search\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('statistics', 'List total customer require cashout');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="box box-primary">
    <div class="box-body">
        <?= $this->render('_search', ['model' => $modelSearch]) ?>
    </div>
</div>

<div class="box box-warning">
    <div class="box-body table-responsive no-padđing">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'summary' => $this->render('_statistic_header', ['dataTotal' => $dataTotal, 'type' => 'request-cashout']),
            'summaryOptions' => [
                'class' => 'pull-right',
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'customer_username',
                [
                    'attribute' => 'fullname',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return isset($data->user->full_name) ? $data->user->full_name : "--";
                    }
                ],
                [
                    'attribute' => 'phone',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return isset($data->user->phone) ? $data->user->phone : "--";
                    }
                ],

                [
                    'attribute' => 'amount',
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'width: 130px;text-align:right;', 'class' => 'text-red'], // <-- set chi?u r�ng
                    'value' => function ($data) {
                        return Yii::$app->formatter->asInteger($data->amount);
                    }

                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return isset(\app\models\CustomerRequireCashout::getStatusLabels()[$data->status]) ? \app\models\CustomerRequireCashout::getStatusLabels()[$data->status] : "--";
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Yii::$app->formatter->asDatetime($data->created_at);
                    }
                ],
                // 'approved_by',
                // 'updated_at',
            ],
        ]); ?>

    </div>
</div>