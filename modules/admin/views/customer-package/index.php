<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searchs\CustomerOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Statistic Customer request');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<?php echo $this->render('_search', ['model' => $searchModel]); ?>
<p>
    <?=
    Html::a('Export Excel', ['export-excel'] + Yii::$app->request->queryParams, [
        'class' => 'btn btn-info',
        'data-method' => 'post',
    ]);
    ?>
</p>
<div class="customer-statistic-index">
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'summary'=>'',
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Tên đăng nhập',
                'format' => 'raw',
                'value' => function ($model) {
                    return (isset($model["customer"])) ? $model["customer"]["username"] : "";
                }
            ],
            [
                'label' => 'Gói/Hoa hồng',
                'format' => 'raw',
                'value' => function ($model) {
                    return (isset($model["package"]) && !empty($model["package"])) ? $model["package"]["package_name"] : "hoa hồng";
                }
            ],
            [
                'label' => 'Yêu cầu',
                'format' => 'raw',
                'value' => function ($model) {
                    return '[' . \app\models\CustomerRequest::getRequestLabel($model["request"]["type"]) . ']&nbsp;' . Yii::$app->formatter->asDecimal($model["request"]["amount"], 8) . Yii::$app->params["unit"];
                }
            ],
            [
                'label' => 'Yêu cầu lúc',
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDatetime($model["request"]["created_at"]);
                }
            ],
            [
                'label' => 'Chi tiết',
                'format' => 'raw',
                'value' => function ($model) {
                    $html = '';
                    if (\app\models\CustomerRequest::getRequestLabel($model["request"]["type"]) == 'PH') {
                        if (!empty($model["sent"])) {
                            $html .= '<table class="table table-striped table-bordered">';
                            $html .= '<thead>';
                            $html .= '<th>chuyển tiền lần</th>';
                            $html .= '<th>Số tiền</th>';
                            $html .= '<th>Thời gian</th>';
                            $html .= '<th>Trạng thái</th>';
                            $html .= '</thead>';
                            $html .= '<tbody>';
                            foreach ($model["sent"] as $key => $value) {
                                $html .= '<tr>';
                                $html .= '<td>';
                                $html .= $key + 1;
                                $html .= '</td>';
                                $html .= '<td>';
                                $html .= Yii::$app->formatter->asDecimal($value["amount"], 8);
                                $html .= '</td>';
                                $html .= '<td>';
                                $html .= Yii::$app->formatter->asDatetime($value["created_at"]);
                                $html .= '</td>';
                                $html .= '<td>';
                                $html .= \app\models\CustomerTransaction::getStatusLabels($value["status"]);
                                $html .= '</td>';
                                $html .= '</tr>';

                            }
                            $html .= '</tbody>';
                            $html .= '</table>';

                        }

                    } elseif (\app\models\CustomerRequest::getRequestLabel($model["request"]["type"]) == 'GH') {
                        if (!empty($model["receiver"])) {
                            $html .= '<table class="table table-striped table-bordered">';
                            $html .= '<thead>';
                            $html .= '<th>Nhận tiền lần</th>';
                            $html .= '<th>Số tiền</th>';
                            $html .= '<th>Thời gian</th>';
                            $html .= '<th>Trạng thái</th>';
                            $html .= '</thead>';
                            $html .= '<tbody>';
                            foreach ($model["receiver"] as $key => $value) {
                                $html .= '<tr>';
                                $html .= '<td>';
                                $html .= $key + 1;
                                $html .= '</td>';
                                $html .= '<td>';
                                $html .= Yii::$app->formatter->asDecimal($value["amount"], 8);
                                $html .= '</td>';
                                $html .= '<td>';
                                $html .= Yii::$app->formatter->asDatetime($value["created_at"]);
                                $html .= '</td>';
                                $html .= '<td>';
                                $html .= \app\models\CustomerTransaction::getStatusLabels($value["status"]);
                                $html .= '</td>';
                                $html .= '</tr>';

                            }
                            $html .= '</tbody>';
                            $html .= '</table>';

                        }
                    }
                    return $html;

                }
            ]
        ]
    ]); ?>
 <?= \yii\widgets\LinkPager::widget(
        [
            'pagination'=>$pagination
        ]
    ) ?>
</div>
