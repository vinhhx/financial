<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Config */

$this->title = Yii::t('app', 'Create Config');
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][Yii::t('app', 'Configs')] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="config-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
