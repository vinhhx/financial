<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\investments\InvestCustomerPackageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Invest Customer Packages');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="invest-customer-package-index">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?= GridView::widget(['dataProvider' => $dataProvider,
        //        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'label' => 'Username',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-bold'],
                'value' => function ($model) {
                    return $model->customer->username;
                }
            ],
            [
                'label' => 'Số tiền đầu tư',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-bold text-red text-right'],
                'value' => function ($model) {
                    if (isset($model->package)) {
                        if ($model->package->amount) {
                            return Yii::$app->formatter->asDecimal($model->package->amount, 8);
                        }
                    } else {
                        return 'Chưa có gói đầu tư';
                    }

                }
            ],
            ['value' => 'Trạng thái',
                'format' => 'raw',
                'value' => function ($model) {
                    switch ($model->status) {
                        case \app\models\investments\InvestCustomerWallet::STATUS_INACTIVE:
                            return '<span class="text-red" >Chưa kích hoạt tài khoản</span>';
                            break;
                        case \app\models\investments\InvestCustomerWallet::STATUS_ACTIVE:
                        default:
                            return '<span class="text-green" >Đã kích hoạt tài khoản</span>';
                            break;
                    }
                }
            ],

            [
                'label' => 'Package Status',
                'format' => 'raw',
                'value' => function ($model) {
                    if (isset($model->package)) {
                        return $model->package->getStatusLabel();
                    } else {
                        return 'Chưa có gói đầu tư';
                    }

                }
            ],
            [
                'label' => 'Thời gian bắt đầu đầu tư',
                'format' => 'raw',
                'value' => function ($model) {
                    if (isset($model->package)) {
                        Yii::$app->formatter->asDate($model->package->created_at);
                    } else {
                        return 'Chưa có gói đầu tư';
                    }

                }
            ],

//            'created_at: datetime',
        ],
    ]); ?>
</div>
