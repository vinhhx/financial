<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\forms\investments\InvestCustomerPackageSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invest-customer-package-search form-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <ul style="margin: 0; padding: 0;">
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'id', array('placeholder' => $model->getAttributeLabel('id'), 'style' => 'width:100px')) ?>
        </li>
        `
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'customer_username', array('placeholder' => 'Customer Username', 'style' => 'width:120px')) ?>
        </li>

        <li class="form-item">
            <?= Html::activeDropDownList($model, 'status', \app\models\investments\InvestCustomerPackage::getStatusLabels(), array('prompt' => 'All status', 'style' => 'width:150px')) ?>
        </li>
        <li class="form-item">
            <?= Html::submitButton('<i class="fa fa-search"></i>&nbsp;Tìm kiếm', ['class' => 'btn btn-primary btn-sm']) ?>
        </li>
    </ul>

    <?php ActiveForm::end(); ?>

</div>
