<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\investments\InvestCustomerPackage */

$this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Invest Customer Package',
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Invest Customer Packages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="invest-customer-package-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
