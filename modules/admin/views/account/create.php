<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\investments\InvestCustomerPackage */

$this->title = Yii::t('app', 'Create Invest Customer Package');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Invest Customer Packages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invest-customer-package-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
