<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomerTransactionQueue */

$this->title = Yii::t('app', 'Create Customer Transaction Queue');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customer Transaction Queues'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-transaction-queue-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
