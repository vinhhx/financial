<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\search\CustomerTransactionQueueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customer Transaction Waiting Provider help');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-transaction-queue-index">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <div style="margin-bottom: 10px;">
        <?=
        Html::a('Export Excel', ['export-excel', 'type' => 'index-provider-help'] + Yii::$app->request->queryParams, [
            'class' => 'btn btn-info',
            'data-method' => 'post',
        ]);
        ?>
    </div>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'customer_id',
            'customer_username',
//            'customer_request_id',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getStatusLabel();
                }
            ],
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getTypeStepLabel();
                }
            ],
            'start_at:datetime',
            'amount',
            'created_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{add}<br/>{finish}',
                'buttons' => [
                    'add' => function ($url, $model, $key) {
                        if ($model->type == \app\models\CustomerTransaction::TYPE_OPEN_PACKAGE):
                            return Html::a('<span class="fa fa-puzzle-piece"></span>&nbsp' . Yii::t('app', 'Get help'), '', [
                                'class' => ' cus-btn btn btn-success btn-xs btn-add-receiver', 'data-queue-id' => $model->id
                            ]);
                        elseif ($model->type == \app\models\CustomerTransaction::TYPE_CASH_OUT_PACKAGE || $model->type == \app\models\CustomerTransaction::TYPE_CASH_OUT_POUNDAGE):
                            return Html::a('<span class="fa fa-puzzle-piece"></span>&nbsp' . Yii::t('app', 'Provider help'), '', [
                                'class' => ' cus-btn btn btn-danger btn-xs btn-add-sent', 'data-queue-id' => $model->id
                            ]);
                        endif;
                    },
                    'finish' => function ($url, $model) {
                        return Html::a('<span class="fa fa-check"></span>&nbsp;' . Yii::t('app', 'Completed'), \yii\helpers\Url::to(['completed-gh', 'id' => $model->id]), [
                            'class' => 'cus-btn btn btn-xs btn-success',
                            'title' => Yii::t('app', 'Completed'),
                            'data-confirm' => Yii::t('app', 'Are you sure, completed this request GH ?'),
                            'data-method' => 'post',
                            'data-pjax' => 0,
                            'aria-label' => Yii::t('app', 'Completed')
                        ]);
                    }

                ]
            ],
        ],
    ]); ?>
</div>
<div class="modal fade" id="pageModal" tabindex="-1" role="dialog" aria-labelledby="pageModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
            </div>

        </div>
    </div>
</div>

<?php
$js = <<<EOD
 $('body').on('click','a.btn-add-sent',function(e){
   e.preventDefault();
    var queueId=$(this).attr('data-queue-id');
    if(queueId){
        $.post($(this).attr('href'),{type:'add-queue',id:queueId},function(data){
            $('#pageModal .modal-body').html(data);
            $('#pageModal').modal('show');
        })
    }
 return false;
 }).on('change','#addreceiverform-receiver_id',function(e){
    var cid=$(this).val();
    if(cid){
    $.post($(this).attr('href'),{type:'get-bank-address',cid:cid},function(data){
            if(data){
                $('#addreceiverform-receiver_bank_address').html(data);
            }
        })
    }
 }).on('beforeSubmit','#form-add-receiver',function(e){
 //pass validate
    $(this).find('button[type="submit"]').attr('disabled','disabled');
    $(this).find('button[type="submit"]').html('Processing...');
    e.preventDefault();
        e.stopImmediatePropagation();
        var formData = new FormData(this);
        formData.append('type', 'submit-add-sent');
        var _this = jQuery(this);
        jQuery.ajax({
            url: jQuery(this).attr('action'),
            type: 'POST',
            dataType: 'JSON',
            async: false,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                if (data) {
                    if (data.status == 'success') {
                      alert('Add account get help completed!');
                        location.reload();
                    } else if (data.status == "error") {
                       alert('Error:  not add account get help!');
                    }
                }
                modal.modal('hide');
            }
        });
 });
EOD;
$this->registerJs($js);
?>
<style>
    .cus-btn {
        margin: 5px;
        display: inline-block;
    }

</style>