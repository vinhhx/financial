<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 23-Jun-16
 * Time: 12:00 AM
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>
<div class="page-view-transaction">
    <h2 class="page-title bg-green">Add transaction to request investion!</h2>

    <div class="page-content">
        <?php
        $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'id' => 'form-add-receiver',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-2',
                    'offset' => 'col-sm-offset-0',
                    'wrapper' => 'col-sm-6',
                    'error' => 'col-sm-6',
                    'hint' => 'col-sm-2',
                ],
            ]
        ]);

        ?>
        <?= $form->field($model, 'customer_receiver_username')->textInput(['disabled' => 'disabled']) ?>
        <?= $form->field($model, 'amount_request')->textInput(['disabled' => 'disabled']) ?>
        <?= $form->field($model, 'sent_id')->dropDownList($customers, ['prompt' => '-- Chọn tài khoản nhận  --']) ?>
        <?= $form->field($model, 'amount')->textInput(['value' => $model->amount]) ?>
        <?= $form->field($model, 'customer_receiver_queue_id')->hiddenInput()->label(false) ?>
        <div class="form-group">
            <label class="control-label col-sm-2" for="button-submit"></label>

            <div class="col-sm-6">
                <?= Html::submitButton('<span class="fa fa-floppy-o"></span>&nbsp; Save', ['class' => 'btn btn-sm btn-primary']) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
<style>
    .field-addreceiverform-customer_sent_queue_id {
        margin: 0px;
        height: 1px;
    }

    .field-addreceiverform-receiver_bank_address {
        margin-bottom: 0px;
    }
</style>
