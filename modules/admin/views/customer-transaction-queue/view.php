<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerTransactionQueue */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Customer Transaction Queue'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-transaction-queue-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'customer_id',
            'customer_username',
            'customer_request_id',
            'type',
            'type_step',
            'status',
            'start_at:datetime',
            'amount',
            'created_at:datetime',
            'updated_at:datetime',
            'min_amount',
            'max_amount',
        ],
    ]) ?>

</div>
