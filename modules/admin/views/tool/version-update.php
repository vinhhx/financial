<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 29-Feb-16
 * Time: 11:42 AM
 */
?>
<div class="box">
    <div class="box-header">
        <h3> Các nâng cấp hiển thị theo ngày:</h3>
    </div>
    <div class="box-body">
        <div class="col-md-12">
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Cập nhật ngày 29-02-2016</h3>

                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.box-tools -->
                </div>
                <!-- /.box-header -->
                <div class="box-body" style="display: block;">
                    <h4>*) admin.lincolmgt.com </h4>

                    <p> &nbsp;&nbsp;-Sửa giao diện view sơ đồ nhánh khách hàng.</p>

                    <p> &nbsp;&nbsp;-Tạm ứng tiền cho khách hàng:<br/>

                        &nbsp;&nbsp;&nbsp;&nbsp;+Danh sách các khách hàng tạm ứng và số tiền tạm ứng<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;+Thanh toán tạm ứng( nhận tiền của khach hàng tạm ứng)-> chuyển trạng thái tài khoản của khách
                        và active các tài khoản của khách đã tạo khi được tạm ứng tiền<br/>
                    </p>

                    <p> &nbsp;&nbsp;-Hủy tạm ứng(Xóa các tài khoản được tạo khi tài khoản cha đang trọng trạng thái tạm ứng tiền)<br/>
                        &nbsp;&nbsp;-Hiển thị lại danh sách giao dịch cho dễ hiểu. Thêm tiền tiền Việt Nam theo tỉ giá<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;+ Vào 1USD=24.000<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;+ Ra 1USD=21.000<br/>
                    </p>
                    <br/>
                    <h4>*) account.lincolmgt.com</h4>

                    <p> &nbsp;&nbsp;-Mở tính năng cho khách hàng tự đăng ký tài khoản con<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;+ Số tiền đăng ký tính theo tỷ giá<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;+ Cho tài khoản cha chọn vị trí cho tài khoản con trong nhánh<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;+ Trạng thái chờ nếu tài khoản cha đang xin tạm ứng.<br/>
                    </p>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
</div>
