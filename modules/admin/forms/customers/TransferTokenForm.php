<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 16-Aug-16
 * Time: 9:43 AM
 */
namespace app\modules\admin\forms\customers;

use app\models\Customer;
use app\models\CustomerOrder;
use app\models\CustomerToken;
use app\models\CustomerTokenTransaction;
use app\models\investments\InvestCustomer;
use app\modules\admin\forms\AddTokenForm;
use yii\base\Exception;
use yii\base\Model;

class TransferTokenForm extends Model
{
    public $customer_send_id;
    public $customer_receiver_id;
    public $quantity;
    public $module_type;
    protected $customerSent;
    protected $customerReceiver;

    public function rules()
    {
        return [
            [['customer_receiver_id', 'module_type', 'quantity'], 'required'],
            [['customer_receiver_id', 'customer_send_id'], 'integer'],
            [['quantity'], 'integer', 'min' => 1],

        ];
    }

    public function attributeLabels()
    {
        return [
            'customer_receiver_id' => 'Receiver Username',
            'customer_send_id' => 'Tranfer Username',
            'quantity' => 'Quantity transfer',
            'module_type' => 'Module type',
        ];
    }

    public function transfer()
    {
        $result = [];
        if ($this->validate()) {
            $customerSent = $this->getCustomerSent();
            if ($customerSent) {
                $customerReceiver = $this->getCustomerReceiver();
                if (!$customerReceiver) {
                    $result = [
                        'status' => 'error',
                        'code' => 404,
                        'message' => 'User receiver token not existed!',
                    ];
                    return $result;
                }
                //Transfer từ tk admin sang tài khoản nhận
                $transaction = \Yii::$app->db->beginTransaction();
                //create customer request
                $tokens = [];
                try {
                    $cusOrder = new CustomerOrder();
                    $cusOrder->customer_id = $this->customer_receiver_id;
                    $cusOrder->customer_username = $this->customerReceiver->username;
                    $cusOrder->quantity = $this->quantity;
                    $cusOrder->status = CustomerOrder::STATUS_PENDING;
                    $cusOrder->type = CustomerOrder::TYPE_EXCHANGE_TOKEN;
                    $cusOrder->attachment = $this->customerSent->username;
                    if ($this->module_type == CustomerToken::TYPE_CUSTOMER) {
                        $cusOrder->type_module = CustomerOrder::TYPE_MODULE_CUSTOMER;
                    } elseif ($this->module_type == CustomerToken::TYPE_INVEST) {
                        $cusOrder->type_module = CustomerOrder::TYPE_MODULE_INVEST;
                    }
                    if ($cusOrder->save()) {
                        //Tạo bản ghi xong rồi add token cho người chơi
                        $addToken = $this->addToken();
                        if (!isset($addToken["status"]) || $addToken["status"] != "success" || !isset($addToken["token"]) || empty($addToken["token"])) {
                            throw new Exception('Không tạo được token cho người chơi.');
                        }
                        $tokens = $addToken["token"];
                        $typeModule = 0;
                        if ($cusOrder->type_module == CustomerOrder::TYPE_MODULE_CUSTOMER) {
                            $typeModule = CustomerTokenTransaction::TYPE_MODULE_CUSTOMER;
                        } elseif ($cusOrder->type_module == CustomerOrder::TYPE_MODULE_CUSTOMER) {
                            $typeModule = CustomerTokenTransaction::TYPE_MODULE_INVEST;
                        }
                        //Luưu transaction
                        $cusTokenTransaction = [];
                        $cusTokenTransaction[] = [
                            $this->customerSent->id,
                            $cusOrder->id,
                            $typeModule,
                            $this->quantity,
                            CustomerTokenTransaction::SIGN_SUB,
                            $this->customerReceiver->username,
                            json_encode($tokens),
                            CustomerTokenTransaction::TYPE_SENT,
                            time(),
                            time()
                        ];
                        $cusTokenTransaction[] = [
                            $this->customer_receiver_id,
                            $cusOrder->id,
                            $typeModule,
                            $this->quantity,
                            CustomerTokenTransaction::SIGN_ADD,
                            $this->customerSent->username,
                            json_encode($tokens),
                            CustomerTokenTransaction::TYPE_RECEIVED,
                            time(),
                            time()
                        ];
                        $batchInsert = \Yii::$app->db->createCommand()->batchInsert(
                            CustomerTokenTransaction::tableName(),
                            [
                                'customer_id',
                                'bill_id',
                                'type_module',
                                'quantity',
                                'sign',
                                'receiver',
                                'content',
                                'type',
                                'created_at',
                                'updated_at',
                            ], $cusTokenTransaction)->execute();
                        if ($batchInsert) {
                            $cusOrder->status = CustomerOrder::STATUS_COMPLETED;
                            if (!$cusOrder->save(true, ['status', 'updated_at'])) {
                                throw new Exception('Require sent token not completed');
                            }
                            $transaction->commit();
                            $result = [
                                'status' => 'success',
                                'code' => 200,
                                'message' => 'Completed',
                            ];


                        }
                    } else {
                        $result = [
                            'status' => 'error',
                            'code' => 400,
                            'message' => 'Not create Order transfer token',
                        ];
                    }

                } catch (\Exception $ex) {
                    $transaction->rollBack();
                    if ($tokens) {
                        \Yii::$app->db->createCommand()->delete(CustomerToken::tableName(), ['token' => $tokens])->execute();
                    }
                    $result = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => $ex->getMessage(),
                    ];
                }

                return $result;
            }
            $result = $this->addToken();
            //Tạo mới token cho người chơi
        }

        return $result;
    }

    public function addToken()
    {
        $addTokenForm = new AddTokenForm();
        $addTokenForm->type = $this->module_type;
        $addTokenForm->customer_id = $this->customer_receiver_id;
        $addTokenForm->quantity = $this->quantity;
        $result = $addTokenForm->addToken();
        return $result;
    }

    public function getCustomerSent()
    {
        if ($this->customer_send_id && $this->module_type == CustomerToken::TYPE_CUSTOMER) {
            $this->customerSent = Customer::findOne(['id' => $this->customer_send_id, 'created_type' => Customer::TYPE_ADMIN]);
        }
        return $this->customer_send_id;
    }

    public function getCustomerReceiver()
    {
        if ($this->module_type == CustomerToken::TYPE_CUSTOMER) {
            $this->customerReceiver = Customer::findOne(['id' => $this->customer_receiver_id]);
        } elseif ($this->module_type == CustomerToken::TYPE_CUSTOMER) {
            $this->customerReceiver = InvestCustomer::findOne(['id' => $this->customer_receiver_id]);
        }
        return $this->customerReceiver;
    }
}