<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 14-Dec-15
 * Time: 11:09 PM
 */

namespace app\modules\admin\forms;

use app\models\Customer;
use app\models\CustomerLoan;
use app\models\CustomerRequireCashout;
use app\models\CustomerTransaction;
use app\models\LogAction;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;
use yii\web\NotFoundHttpException;

class CustomerLoanForm extends Model
{
    public $amount;
    public $customer_id;


    public function rules()
    {
        return [
            [['customer_id'], 'integer'],
            [['amount'], 'number'],
            [['amount', 'customer_id'], 'required', 'skipOnEmpty' => false, 'skipOnError' => false],
//            [['amount'], 'validateAmount'],
//            [['captcha'], 'captcha','captchaAction' => '/customer/default/captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'amount' => Yii::t('customers', 'Amount Money Add'),
            'captcha' => Yii::t('customers', 'Captcha'),
        ];
    }

//    public function validateAmount($attribute, $params)
//    {
//        if ((int)$this->$attribute < (int)25) {
//            $this->addError($attribute, Yii::t('customers', 'Number input is false.Please input number than 25 USD'));
//        } else if ($this->$attribute > (int)Yii::$app->user->identity->amount) {
//            $this->addError($attribute, Yii::t('customers', 'Number input is false.Please input number less than') . Yii::$app->formatter->asInteger(Yii::$app->user->identity->amount) . ' USD');
//        }
//    }


    public function saveCustomerLoan()
    {
        if ($this->validate()) {
            $transaction = Yii::$app->db->beginTransaction();
            $customer = Customer::findOne($this->customer_id);
            if (!$customer) {
                throw new NotFoundHttpException(Yii::t('customers', 'This customer not existed'));
            }
            try {
                $loan=New CustomerLoan();
                $loan->customer_id=(int)$customer->id;
                $loan->amount_loan=(float)$this->amount;
                $loan->amount_loan_vnd=(float)($loan->amount_loan * Yii::$app->params['exchange']['in']);
                $loan->status=CustomerLoan::STATUS_PENDING;
                $loan->created_by=Yii::$app->user->id;
                if($loan->save()){
                    //C?p nh?t customer sang tr?ng th�i ?ang n?

                    //T?o transaction
                    $custran = new CustomerTransaction();
                    $custran->customer_id = (int)$customer->id;
                    $custran->customer_username = $customer->username;
                    $custran->amount = (float)$this->amount;
                    $custran->bill_id = $loan->id;
                    $custran->type = CustomerTransaction::TYPE_CUSTOMER_LOAN_ADMIN;
                    $custran->status = 1;
                    $custran->sign = CustomerTransaction::SIGN_ADD;
                    $custran->balance_before = (float)$customer->amount;
                    $custran->balance_after = (float)$customer->amount + round($this->amount, 1, PHP_ROUND_HALF_DOWN);
                    if ($custran->save()) {
                        $logType = LogAction::TYPE_TRANSACTION_CUSTOMER_LOAN_ADMIN;
                        LogAction::logAdmin($custran->id, $custran::tableName(), $logType, ArrayHelper::toArray($custran));
                        $customer->amount = $custran->balance_after;
                        $customer->is_loan_admin=Customer::IS_LOAN;
                        if ($customer->save(true, ['amount','is_loan_admin', 'updated_at'])) {
                            $transaction->commit();
                            return true;
                        }
                    }
                }

            } catch (Exception $ex) {
                $transaction->rollBack();
                return false;
            }

        }
        return false;
    }


}
