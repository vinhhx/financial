<?php

namespace app\modules\admin\forms;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\logs\AdminLogAction;

/**
 * AdminLogActionSearch represents the model behind the search form about `app\models\logs\AdminLogAction`.
 */
class AdminLogActionSearch extends AdminLogAction
{
    public $start_date;
    public $end_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'action_type', 'created_at', 'updated_at'], 'integer'],
            [['user_name', 'ip_access', 'controller_id', 'controller_action_id', 'action_content', 'start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdminLogAction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'action_type' => $this->action_type,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        if ($this->end_date) {
            $query->andFilterWhere(['<=', 'created_at', strtotime(Yii::$app->formatter->asDate($this->end_date, 'php:Y-m-d') . '23:59')]);
        }
        if ($this->start_date) {
            $query->andFilterWhere(['>=', 'created_at', strtotime(Yii::$app->formatter->asDate($this->start_date, 'php:Y-m-d') . '00:00')]);
        }
        $query->andFilterWhere(['like', 'user_name', $this->user_name])
            ->andFilterWhere(['like', 'ip_access', $this->ip_access])
            ->andFilterWhere(['like', 'controller_id', $this->controller_id])
            ->andFilterWhere(['like', 'controller_action_id', $this->controller_action_id])
            ->andFilterWhere(['like', 'action_content', $this->action_content]);

        return $dataProvider;
    }
}
