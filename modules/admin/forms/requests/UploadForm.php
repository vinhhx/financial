<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 25-Jun-16
 * Time: 5:18 AM
 */
namespace app\modules\admin\forms\requests;

use yii\base\Exception;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\Html;
use Yii;

class UploadForm extends Model
{
    public $file;
    public $request_id;

    public function rules()
    {
        return [
            [['request_id', 'file'], 'required'],
            [['request_id'], 'integer'],
            [['file'], 'file', 'extensions' => 'jpg, png, gif', 'mimeTypes' => 'image/jpeg, image/png, image/gif', 'maxSize' => 8 * 1024 * 1024 /* 1 Mb */],
            [['file'], 'safe'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => Yii::t('customer', 'File'),
        ];
    }

}