<?php
namespace app\modules\admin\forms;

use app\models\Customer;
use app\models\User;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;

class ChangePasswordForm extends Model
{
    public $newPassword;
    public $newPasswordConfirmation;
    public $captcha;
    public $customerId;

    public function rules()
    {
        return [
            [['newPassword', 'newPasswordConfirmation'], 'required'],
            ['newPassword', 'string', 'min' => 6],
            ['newPasswordConfirmation', 'compare', 'compareAttribute' => 'newPassword'],
            //['captcha', 'captcha', 'captchaAction' => '/admin/default/captcha'],
            [['captcha'], ReCaptchaValidator::className(),'secret' => Yii::$app->params["GOOGLE_CAPTCHA_SECRET_KEY"]]
        ];
    }

    public function attributeLabels()
    {
        return [
            'newPassword' => Yii::t('customers', 'New Password'),
            'newPasswordConfirmation' => Yii::t('customers', 'Repeate new Password'),
            'captcha' => Yii::t('customers', 'Captcha'),
        ];
    }

}