<?php

namespace app\modules\admin\forms\investments;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\investments\InvestCustomerPackage;

/**
 * InvestCustomerPackageSearch represents the model behind the search form about `app\models\investments\InvestCustomerPackage`.
 */
class InvestCustomerPackageSearch extends InvestCustomerPackage
{
    public $customer_username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'package_id', 'status', 'is_active', 'type', 'last_cash_out_at', 'joined_at', 'created_at', 'updated_at'], 'integer'],
            [['amount'], 'number'],
            [['customer_username'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvestCustomerPackage::find();
        $query->joinWith('customer');
        $query->with('package');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'invest_customer_package.id' => $this->id,
            'invest_customer_package.customer_id' => $this->customer_id,
            'invest_customer_package.package_id' => $this->package_id,
            'invest_customer_package.amount' => $this->amount,
            'invest_customer_package.status' => $this->status,
            'invest_customer_package.is_active' => $this->is_active,
            'invest_customer_package.type' => $this->type,
            'invest_customer_package.last_cash_out_at' => $this->last_cash_out_at,
            'invest_customer_package.joined_at' => $this->joined_at,
            'invest_customer_package.created_at' => $this->created_at,
            'invest_customer_package.updated_at' => $this->updated_at,
            'customer.username' => $this->customer_username,
        ]);

        return $dataProvider;
    }
}
