<?php

namespace app\modules\admin\forms\investments;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\investments\InvestCustomerWallet;

/**
 * InvestCustomerWalletSearch represents the model behind the search form about `app\models\investments\InvestCustomerWallet`.
 */
class InvestCustomerWalletSearch extends InvestCustomerWallet
{
    public $customer_username;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['balance', 'poundage'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvestCustomerWallet::find();
        $query->with('package');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'invest_customer_wallet.id' => $this->id,
            'invest_customer_wallet.customer_id' => $this->customer_id,
            'invest_customer_wallet.balance' => $this->balance,
            'invest_customer_wallet.poundage' => $this->poundage,
            'invest_customer_wallet.type' => $this->type,
            'invest_customer_wallet.status' => $this->status,
            'invest_customer_wallet.created_at' => $this->created_at,
            'invest_customer_wallet.updated_at' => $this->updated_at,
            'customer.username' => $this->customer_username,
        ]);

        return $dataProvider;
    }
}
