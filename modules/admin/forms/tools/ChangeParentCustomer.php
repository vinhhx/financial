<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 16-Oct-16
 * Time: 7:57 AM
 */
namespace app\modules\admin\forms\tools;

use app\models\Customer;
use app\models\investments\InvestCustomer;
use app\models\investments\InvestCustomerCase;
use app\models\investments\InvestCustomerPackage;
use app\models\logs\AdminLogAction;
use yii\base\Exception;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class ChangeParentCustomer extends Model
{
    public $children_id;
    public $parent_id;

    public function rules()
    {
        return [
            [['children_id', 'parent_id'], 'required'],
            [['children_id', 'parent_id'], 'integer'],
            ['parent_id', 'compare', 'compareAttribute' => 'children_id', 'operator' => '!='],
            ['children_id', 'validateChildren'],
            ['parent_id', 'validateParent'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'children_id' => \Yii::t('customers', 'Username cần chuyển'),
            'parent_id' => \Yii::t('customers', 'Username sẽ được làm cấp cha'),
        ];
    }

    public function validateChildren($attribute, $params)
    {

        $childrenCustomer = InvestCustomer::findOne($this->$attribute);
        /* Check tồn tại*/
        if (!$childrenCustomer) {
            $this->addError($attribute, 'Không tồn tại tài khoản này');
        } else {
            /* Check tài khoản này đã tham gia gói chưa*/
            if (InvestCustomerPackage::find()->where(['customer_id' => $childrenCustomer->id])->count() > 0) {
                $this->addError($attribute, 'Tài khoản này đã có gói đầu tư');
            } else {
                /* Check tài khoản này có cấp con không*/
                if (InvestCustomerCase::find()->where(['parent_id' => $childrenCustomer->id])->count() > 0) {
                    $this->addError($attribute, 'Tài khoản này đã có nhánh con');
                }
            }
        }
    }

    public function validateParent($attribute, $params)
    {

        $ParentCustomer = InvestCustomer::findOne($this->$attribute);
        /* Check tồn tại*/
        if (!$ParentCustomer) {
            $this->addError($attribute, 'Không tồn tại tài khoản này');
        } else {
            /* Check tài khoản này có cấp con không*/
            if (InvestCustomerCase::find()->where(['parent_id' => $ParentCustomer->id, 'level' => 1])->count() > 1) {
                $this->addError($attribute, 'Tài khoản này đã có đủ nhánh con');
            }
        }
    }

    public function change()
    {
        if ($this->validate()) {
            $customer = InvestCustomer::findOne($this->children_id);
            $transaction = \Yii::$app->db->beginTransaction();
            $oldParent = $customer->parent_id;
            try {
                $customer->deleteCase();
                $customer->parent_id = $this->parent_id;
                if ($customer->save(true, ['parent_id', 'updated_at'])) {
                    $result = $customer->addInvestCustomerCase();
                    if (isset($result["code"]) && (int)$result["code"] == 200) {
                        AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_CHANGE_CASE, ArrayHelper::merge($customer->attributes, ['old_parent_id' => $oldParent]));
                        $transaction->commit();
                    } else {
                        throw new Exception('Không repair được tài khoản này. ');
                    }
                } else {
                    throw new Exception('Không cập nhật được tài khoản này. ');
                }
            } catch (\Exception $ex) {
                $transaction->rollBack();
                $this->addError('children_id', $ex->getMessage());
            }
        }
    }

}