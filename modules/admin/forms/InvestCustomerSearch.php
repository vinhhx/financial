<?php

namespace app\modules\admin\forms;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\investments\InvestCustomer;

/**
 * InvestCustomerSearch represents the model behind the search form about `app\models\investments\InvestCustomer`.
 */
class InvestCustomerSearch extends InvestCustomer
{

    public $start_date;
    public $end_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'is_active', 'is_deleted', 'parent_id', 'package_id', 'sex', 'date_join', 'created_at', 'updated_at'], 'integer'],
            [['username', 'ref_code', 'auth_key', 'password_hash', 'password_reset_token', 'full_name', 'email', 'phone', 'dob', 'identity_card', 'nation', 'address', 'last_auto_increase'], 'safe'],
            [['amount'], 'number'],
            [['start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvestCustomer::find();
        $query->with('bankAddress');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->start_date) {
            $query->andFilterWhere(['>=', 'created_at', strtotime(Yii::$app->formatter->asDate($this->start_date, 'php:Y-m-d' . ' 00:00'))]);
        }
        if ($this->end_date) {
            $query->andFilterWhere(['<=', 'created_at', strtotime(Yii::$app->formatter->asDate($this->end_date, 'php:Y-m-d' . ' 23:59'))]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'is_active' => $this->is_active,
            'is_deleted' => $this->is_deleted,
            'parent_id' => $this->parent_id,
            'package_id' => $this->package_id,
            'amount' => $this->amount,
            'sex' => $this->sex,
            'dob' => $this->dob,
            'date_join' => $this->date_join,
            'last_auto_increase' => $this->last_auto_increase,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'ref_code', $this->ref_code])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'identity_card', $this->identity_card])
            ->andFilterWhere(['like', 'nation', $this->nation])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
