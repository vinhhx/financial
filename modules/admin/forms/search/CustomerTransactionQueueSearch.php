<?php

namespace app\modules\admin\forms\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerTransactionQueue;

/**
 * CustomerTransactionQueueSearch represents the model behind the search form about `app\models\CustomerTransactionQueue`.
 */
class CustomerTransactionQueueSearch extends CustomerTransactionQueue
{

    public $start_date;
    public $end_date;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'customer_request_id', 'type_step', 'status', 'start_at', 'created_at', 'updated_at'], 'integer'],
            [['customer_username'], 'safe'],
            [['amount', 'min_amount', 'max_amount'], 'number'],
            [['type'], 'safe'],
            [['start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerTransactionQueue::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->start_date) {
            $query->andFilterWhere(['>=', 'created_at', strtotime(Yii::$app->formatter->asDate($this->start_date, 'php:Y-m-d' . ' 00:00'))]);
        }
        if ($this->end_date) {
            $query->andFilterWhere(['<=', 'created_at', strtotime(Yii::$app->formatter->asDate($this->end_date, 'php:Y-m-d' . ' 23:59'))]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'customer_request_id' => $this->customer_request_id,
            'type' => $this->type,
            'type_step' => $this->type_step,
            'status' => $this->status,
            'start_at' => $this->start_at,
            'amount' => $this->amount,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'min_amount' => $this->min_amount,
            'max_amount' => $this->max_amount,
        ]);

        $query->andFilterWhere(['like', 'customer_username', $this->customer_username]);

        return $dataProvider;
    }
}
