<?php

namespace app\modules\admin\forms\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerLoan;

/**
 * CustomerLoanSearch represents the model behind the search form about `app\models\CustomerLoan`.
 */
class CustomerLoanSearch extends CustomerLoan
{
    public $view_all;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'status', 'type', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['amount_loan', 'amount_loan_vnd'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerLoan::find();
        $query->where(['customer_loan.status' => CustomerLoan::STATUS_PENDING]);
        $query->joinWith('customer');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'customer_loan.id' => $this->id,
            'customer_loan.customer_id' => $this->customer_id,
            'customer_loan.amount_loan' => $this->amount_loan,
            'customer_loan.amount_loan_vnd' => $this->amount_loan_vnd,
            'customer_loan.status' => $this->status,
            'customer_loan.type' => $this->type,
            'customer_loan.created_by' => $this->created_by,
            'customer_loan.created_at' => $this->created_at,
            'customer_loan.updated_at' => $this->updated_at,
        ]);

        return $dataProvider;
    }
}
