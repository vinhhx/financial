<?php

namespace app\modules\admin\forms\search;

use app\models\Customer;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerRequireCashout;

/**
 * CustomerRequireCashoutSearch represents the model behind the search form about `app\models\CustomerRequireCashout`.
 */
class CustomerRequireCashoutSearch extends CustomerRequireCashout
{

    public $phone;
    public $fullname;
    public $start_date;
    public $end_date;
    private $dataProvider;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'amount', 'status', 'created_at', 'updated_at'], 'integer'],
            [['customer_username', 'approved_by', 'phone', 'fullname', 'start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerRequireCashout::find();
        $query->joinWith('user');
        $isSystemAdmin = Yii::$app->authManager->getAssignment(RBAC_FULL, Yii::$app->user->id);
        if (!$isSystemAdmin) {
            $query->andWhere(['customer.user_id_first_branch' => Customer::ID_ONLY_VIEW]);
        }
        $this->dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $this->dataProvider;
        }
        if ($this->start_date) {
            $query->andFilterWhere(['>=', 'customer_require_cashout.created_at', strtotime(Yii::$app->formatter->asDate($this->start_date, 'php:Y-m-d' . ' 00:00'))]);
        }
        if ($this->end_date) {
            $query->andFilterWhere(['<=', 'customer_require_cashout.created_at', strtotime(Yii::$app->formatter->asDate($this->end_date, 'php:Y-m-d' . ' 23:59'))]);
        }
        $query->andFilterWhere([
            'customer_require_cashout.id' => $this->id,
            'customer_require_cashout.customer_id' => $this->customer_id,
            'customer_require_cashout.amount' => $this->amount,
            'customer_require_cashout.status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'customer_require_cashout.customer_username', $this->customer_username])
            ->andFilterWhere(['like', 'user.full_name', $this->fullname])
            ->andFilterWhere(['like', 'user.phone', $this->phone])
            ->andFilterWhere(['like', 'customer_require_cashout.approved_by', $this->approved_by]);

        return $this->dataProvider;
    }

    public function statisticCutomerPending($params)
    {
        $query = CustomerRequireCashout::find();
        $query->joinWith('user');
        $isSystemAdmin = Yii::$app->authManager->getAssignment(RBAC_FULL, Yii::$app->user->id);
        if (!$isSystemAdmin) {
            $query->andWhere(['customer.user_id_first_branch' => Customer::ID_ONLY_VIEW]);
        }
        $this->dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
        ]);

        $this->load($params);
        if ($this->start_date && !$this->end_date) {
            $this->start_date = date('Y-m-d', strtotime($this->start_date));
            $this->end_date = date('Y-m-d');
        } elseif ($this->end_date && !$this->start_date) {
            $this->end_date = date('Y-m-d', strtotime($this->end_date));
            $this->start_date = date('Y-m-d', strtotime("-7 day", strtotime($this->end_date)));
        } elseif (!$this->start_date && !$this->end_date) {
            $this->end_date = date('Y-m-d');
            $this->start_date = date('Y-m-d', strtotime("-7 day", time()));
        } else {
            $this->end_date = date('Y-m-d', strtotime($this->end_date));
            $this->start_date = date('Y-m-d', strtotime($this->start_date));
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $this->dataProvider;
        }
        if ($this->start_date) {
            $query->andFilterWhere(['>=', 'customer_require_cashout.created_at', strtotime(Yii::$app->formatter->asDate($this->start_date, 'php:Y-m-d' . ' 00:00'))]);
        }
        if ($this->end_date) {
            $query->andFilterWhere(['<=', 'customer_require_cashout.created_at', strtotime(Yii::$app->formatter->asDate($this->end_date, 'php:Y-m-d' . ' 23:59'))]);
        }
        $query->andWhere(['customer_require_cashout.status'=> self::STATUS_PENDING]);

        return $this->dataProvider;
    }


    public function getCustomerRequestCashOut($type = null)
    {
        // Tính tổng tiền khách hàng có thể rút
        $cloneDataProvider = clone $this->dataProvider->query;
        $dataTotal = $cloneDataProvider
            ->select('customer_require_cashout.customer_id,COUNT(*) AS total_request, SUM(customer_require_cashout.amount) AS total_amount')->asArray()->one();
        return $dataTotal;

    }
}
