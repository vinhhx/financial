<?php

namespace app\modules\admin\forms\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Note;
use yii\db\Query;

/**
 * NoteSearch represents the model behind the search form about `app\models\Note`.
 */
class NoteSearch extends Note
{

    public $start_date;
    public $end_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id', 'type', 'created_at', 'updated_at'], 'integer'],
            [['id', 'object_table', 'content', 'created_id', 'created_by', 'start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Note::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['id' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id ? explode(',', $this->id) : $this->id,
            'object_id' => $this->object_id,
            'type' => $this->type,
            'created_id' => $this->created_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'object_table', $this->object_table])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'created_by', $this->created_by]);

        return $dataProvider;
    }

    public function statisticSearch($type, $params)
    {
        $this->load($params);
        //face 7 ngày tìm kiếm nếu không có ngày bắt đầu và ngày kết thúc
        if ($this->start_date) {
            $this->start_date = date('Y-m-d', strtotime($this->start_date));
        } else {
            $this->start_date = date('Y-m-d', strtotime("-7 day"));
        }

        if ($this->end_date) {
            $this->end_date = date('Y-m-d', strtotime($this->end_date));
        } else {
            $this->end_date = date('Y-m-d');
        }

        $query = new Query();
        $query->select([
            'COUNT(id) AS total_created_note,created_by'
        ]);
        $query->from(Note::tableName());
        $query->from(Note::tableName());
        if ($this->created_by) {
            $query->andWhere(['created_by' => explode(',', $this->created_by)]);
        }
        $query->andWhere(["BETWEEN", 'created_at', strtotime($this->start_date ." 00:00:00"), strtotime($this->end_date ." 23:59:59")]);
        $query->groupBy('created_by');
        $data = $query->all();
        //dk lọc order paid
        return $data;

    }

    public function statisticAdminNote($params)
    {
        return $this->statisticSearch(self::TYPE_ADMIN, $params);

    }

}
