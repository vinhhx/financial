<?php

namespace app\modules\admin\forms\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Customer;

/**
 * CustomerSearch represents the model behind the search form about `app\models\Customer`.
 */
class CustomerSearch extends Customer
{

    public $start_date;
    public $end_date;
    private $dataProvider;

//    /**
//     * @inheritdoc
//     */
    public function rules()
    {
        return [
            [['id', 'package_id', 'amount', 'status', 'sex', 'created_at', 'updated_at', 'is_active'], 'integer'],
            [['username', 'auth_key', 'password_hash', 'password_reset', 'full_name', 'phone', 'email', 'nation', 'address', 'last_auto_increase', 'start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $pagination = true)
    {
        if ($pagination) {
            $pagination = [
                'pageSize' => 30,
            ];
        }
        $query = Customer::find();
        $query->with('packages');
        $query->where(['<>','is_deleted',Customer::IS_DELETED]);
//        $isSystemAdmin = Yii::$app->authManager->getAssignment(RBAC_FULL, Yii::$app->user->id);
//        if (!$isSystemAdmin) {
//            $query->andWhere(['user_id_first_branch' => Customer::ID_ONLY_VIEW]);
//        }
        //Nếu ko phải quyền admin thì chỉ cho xem các khách hàng nhanh 1

//        $query->with('package');
        $this->dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => $pagination,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
        ]);

        $this->load($params);

        $query->andWhere(["!=", "is_deleted", self::IS_DELETED]);

        if ($this->start_date) {
            $query->andFilterWhere(['>=', 'date_join', strtotime(Yii::$app->formatter->asDate($this->start_date, 'php:Y-m-d' . ' 00:00'))]);
        }
        if ($this->end_date) {
            $query->andFilterWhere(['<=', 'date_join', strtotime(Yii::$app->formatter->asDate($this->end_date, 'php:Y-m-d' . ' 23:59'))]);
        }
        $query->andFilterWhere([
            'id' => $this->id,
            'package_id' => $this->package_id,
            'parent_id' => $this->parent_id,
            'amount' => $this->amount,
            'status' => $this->status,
            'is_active' => $this->is_active,
            'sex' => $this->sex,
            'last_auto_increase' => $this->last_auto_increase,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset', $this->password_reset])
            ->andFilterWhere(['like', 'full_name', $this->full_name])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'nation', $this->nation])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $this->dataProvider;;
    }

    public function statisticCutomerJoin($params)
    {
        $this->load($params);
        if ($this->start_date && !$this->end_date) {
            $this->start_date = date('Y-m-d', strtotime($this->start_date));
            $this->end_date = date('Y-m-d');
        } elseif ($this->end_date && !$this->start_date) {
            $this->end_date = date('Y-m-d', strtotime($this->end_date));
            $this->start_date = date('Y-m-d', strtotime("-30 day", strtotime($this->end_date)));
        } elseif (!$this->start_date && !$this->end_date) {
            $this->end_date = date('Y-m-d');
            $this->start_date = date('Y-m-d', strtotime("-30 day", time()));
        } else {
            $this->end_date = date('Y-m-d', strtotime($this->end_date));
            $this->start_date = date('Y-m-d', strtotime($this->start_date));
        }

        $query = Customer::find();
        $query->select("date_join,
        COUNT(id) AS quantity");
        $query->groupBy('date_join');
        $query->having(["BETWEEN", 'date_join', $this->start_date, $this->end_date]);
        $query->asArray();
        $data = $query->all();
        //dk lọc order paid
        return $data;
    }

    public function getCustomerTotalCashOut($type = null)
    {
        // Tính tổng tiền khách hàng có thể rút
        $cloneDataProvider = clone $this->dataProvider->query;
        $dataTotal = $cloneDataProvider
            ->select('COUNT(id) AS total_customer, SUM(amount) AS total_amount')->asArray()->one();
        return $dataTotal;
    }

    public function statisticCutomerCustomerCashout()
    {
        $query = Customer::find();
        $query->where(["status" => Customer::STATUS_ACTIVE]);
        $query->andWhere([">=", "amount", 25]);

        $this->dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
        ]);

        return $this->dataProvider;
    }

}
