<?php

namespace app\modules\admin\forms\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerTransaction;

/**
 * CustomerTransactionSearch represents the model behind the search form about `app\models\CustomerTransaction`.
 */
class CustomerTransactionSearch extends CustomerTransaction
{
    public $type;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_sent_request_id', 'customer_sent_queue_id', 'customer_sent_id', 'customer_receiver_request_id', 'customer_receiver_queue_id', 'customer_receiver_id', 'status', 'customer_sent_type', 'customer_receiver_type', 'type_step', 'expired_sent', 'expired_approve', 'created_at', 'updated_at'], 'integer'],
            [['customer_sent_username', 'customer_sent_bank_address', 'customer_receiver_username', 'customer_receiver_bank_address', 'attachment', 'note'], 'safe'],
            [['amount'], 'number'],
            [['type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        if ($this->type) {
            $query->andFilterWhere([
                'or',
                ['customer_sent_type' => $this->type],
                ['customer_receiver_type' => $this->type],
            ]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_sent_request_id' => $this->customer_sent_request_id,
            'customer_sent_queue_id' => $this->customer_sent_queue_id,
            'customer_sent_id' => $this->customer_sent_id,
            'amount' => $this->amount,
            'customer_receiver_request_id' => $this->customer_receiver_request_id,
            'customer_receiver_queue_id' => $this->customer_receiver_queue_id,
            'customer_receiver_id' => $this->customer_receiver_id,
            'status' => $this->status,
            'customer_sent_type' => $this->customer_sent_type,
            'customer_receiver_type' => $this->customer_receiver_type,
            'type_step' => $this->type_step,
            'expired_sent' => $this->expired_sent,
            'expired_approve' => $this->expired_approve,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'customer_sent_username', $this->customer_sent_username])
            ->andFilterWhere(['like', 'customer_sent_bank_address', $this->customer_sent_bank_address])
            ->andFilterWhere(['like', 'customer_receiver_username', $this->customer_receiver_username])
            ->andFilterWhere(['like', 'customer_receiver_bank_address', $this->customer_receiver_bank_address])
            ->andFilterWhere(['like', 'attachment', $this->attachment])
            ->andFilterWhere(['like', 'note', $this->note]);

        return $dataProvider;
    }
}
