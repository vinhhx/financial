<?php

namespace app\modules\admin\forms\search;

use app\models\CustomerTransaction;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerPoundageTransaction;
use yii\data\ArrayDataProvider;

/**
 * CustomerPoundageTransactionSearch represents the model behind the search form about `app\models\CustomerPoundageTransaction`.
 */
class CustomerPoundageTransactionSearch extends CustomerPoundageTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'bill_id', 'sign', 'customer_id', 'from_id', 'level', 'type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['customer_username', 'from_username'], 'safe'],
            [['amount', 'balance_before', 'balance_after'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerPoundageTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'bill_id' => $this->bill_id,
            'sign' => $this->sign,
            'customer_id' => $this->customer_id,
            'from_id' => $this->from_id,
            'amount' => $this->amount,
            'balance_before' => $this->balance_before,
            'balance_after' => $this->balance_after,
            'level' => $this->level,
            'type' => $this->type,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'customer_username', $this->customer_username])
            ->andFilterWhere(['like', 'from_username', $this->from_username]);

        return $dataProvider;
    }

    public function searchStaticPoundageGroup($params)
    {
        $this->load($params);
        $this->sign = ($this->sign) ? $this->sign : self::SIGN_ADD;
        $this->status = ($this->status) ? $this->status : self::STATUS_COMPLETED;
        $query = CustomerPoundageTransaction::find();
        $query->joinWith('wallet', true, ' LEFT JOIN ');
        $query->select([
            'customer_poundage_transaction.id',
            'customer_poundage_transaction.sign',
            'customer_poundage_transaction.customer_id',
            'customer_poundage_transaction.customer_username',
            'SUM(customer_poundage_transaction.amount) as total_amount',
            'customer_poundage_transaction.type',
            'customer_poundage_transaction.status',
            'customer_wallet.poundage as commission_balance'
        ]);
        $query->andFilterWhere([
            'customer_poundage_transaction.id' => $this->id,
            'customer_poundage_transaction.sign' => $this->sign,
            'customer_poundage_transaction.customer_id' => $this->customer_id,
            'customer_poundage_transaction.amount' => $this->amount,
            'customer_poundage_transaction.type' => $this->type,
            'customer_poundage_transaction.status' => $this->status,
        ]);
        $query->andFilterWhere(['like', 'customer_poundage_transaction.customer_username', $this->customer_username])
            ->andFilterWhere(['like', 'customer_poundage_transaction.from_username', $this->from_username]);
        $query->groupBy('customer_poundage_transaction.customer_username');
        $query->orderBy(['customer_poundage_transaction.id' => SORT_DESC]);
        $query->asArray();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
        ]);
        return $dataProvider;
    }
}
