<?php

namespace app\modules\admin\forms\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerRequest;

/**
 * CustomerRequestSearch represents the model behind the search form about `app\models\CustomerRequest`.
 */
class CustomerRequestStatisticSearch extends CustomerRequest
{

    public $start_date;
    public $end_date;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'type', 'status', 'created_at', 'package_id', 'updated_at'], 'integer'],
            [['block_chain_wallet_id'], 'safe'],
            [['start_date', 'end_date'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerRequest::find();
        $query->with(['customer', 'customerPackage', 'sent', 'receive']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->start_date) {
            $query->andFilterWhere(['>=', 'created_at', strtotime(Yii::$app->formatter->asDate($this->start_date, 'php:Y-m-d' . ' 00:00'))]);
        }
        if ($this->end_date) {
            $query->andFilterWhere(['<=', 'created_at', strtotime(Yii::$app->formatter->asDate($this->end_date, 'php:Y-m-d' . ' 23:59'))]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'type' => $this->type,
            'status' => $this->status,
            'amount' => $this->amount,
            'created_at' => $this->created_at,
            'package_id' => $this->package_id,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'block_chain_wallet_id', $this->block_chain_wallet_id]);

        return $dataProvider;
    }

    public function searchFull($params)
    {
        $query = CustomerRequest::find();
        $query->with(['customer', 'customerPackage', 'sent', 'receive']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => false,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->start_date) {
            $query->andFilterWhere(['>=', 'created_at', strtotime(Yii::$app->formatter->asDate($this->start_date, 'php:Y-m-d' . ' 00:00'))]);
        }
        if ($this->end_date) {
            $query->andFilterWhere(['<=', 'created_at', strtotime(Yii::$app->formatter->asDate($this->end_date, 'php:Y-m-d' . ' 23:59'))]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'type' => $this->type,
            'status' => $this->status,
            'amount' => $this->amount,
            'created_at' => $this->created_at,
            'package_id' => $this->package_id,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'block_chain_wallet_id', $this->block_chain_wallet_id]);

        return $dataProvider;
    }
}
