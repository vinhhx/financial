<?php
/**
 * Created by PhpStorm.
 * User: quyet
 * Date: 6/13/2016
 * Time: 10:18 PM
 */

namespace app\modules\admin\forms;


use yii\base\Model;

class MediaForm extends Model
{
    public $file;

    public function rules()
    {
        return [
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }
}