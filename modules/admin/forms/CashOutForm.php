<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 14-Dec-15
 * Time: 11:09 PM
 */

namespace app\modules\admin\forms;

use app\models\Customer;
use app\models\CustomerRequireCashout;
use app\models\CustomerTransaction;
use app\models\LogAction;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\Html;
use Yii;

class CashOutForm extends Model
{
    public $amount;
//    public $captcha;
    public $customer_id;
    protected $currentCustomer;


    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id'], 'validateCustomer'],
            [['amount'], 'integer'],
            [['amount'], 'required', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['amount'], 'validateAmount'],
//            [['captcha'], 'captcha','captchaAction' => '/customer/default/captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'amount' => Yii::t('customers', 'Amount require cashout'),
            'captcha' => Yii::t('customers', 'Captcha'),
        ];
    }

    public function validateCustomer($attribute, $params){
        if(!$this->$attribute){
            $this->addError($attribute, Yii::t('customers', 'Customer not existed'));
        }else{
            $customer=Customer::findOne($this->$attribute);
            if(!$customer){
                $this->addError($attribute, Yii::t('customers', 'Customer not existed'));
            }else{
                $this->currentCustomer=$customer;
            }
        }
    }

    public function validateAmount($attribute, $params)
    {
        if ((int)$this->$attribute < (int)25) {
            $this->addError($attribute, Yii::t('customers', 'Number input is false.Please input number than 25 USD'));
        } else if ($this->$attribute > (int)$this->currentCustomer->amount) {
            $this->addError($attribute, Yii::t('customers', 'Number input is false.Please input number less than') . Yii::$app->formatter->asInteger($this->currentCustomer->amount) . ' USD');
        } else {
            //Lãi hàng tuần
            //Tổng tiền đã thực hiện yêu cầu rút tiền.
            $totalRequestCashout = $this->currentCustomer->getRequiredCashout();
            //Tiền lãi hàng tuần đc nhận
            $balanceAtLeast = $this->currentCustomer->getAmountIncrease();// Số dư tối thiểu bằng số tiền lãi hàng tuần nhận được
            //Số dư tối thiếu
//            $balanceAtLeast=$this->currentCustomer->getBalanceAtLeast();
            if (($this->currentCustomer->amount - floatval($this->amount) - floatval($totalRequestCashout)) < $balanceAtLeast) {
                $this->addError($attribute, Yii::t('customers', 'Number input is false.Your account balance is greater than ') . $balanceAtLeast . " USD");
            }
        }
    }


    public function saveCashOut()
    {

        if ($this->validate()) {
            $transaction = Yii::$app->db->beginTransaction();

            $model = new CustomerRequireCashout();
            $model->customer_id =$this->currentCustomer->id;
            $model->customer_username = $this->currentCustomer->username;
            $model->status = CustomerRequireCashout::STATUS_PENDING;
            $model->amount = $this->amount;
            $model->out_of_date=date('Y-m-d',strtotime("next Saturday"));
            $model->type=CustomerRequireCashout::TYPE_ADMIN;
            $model->admin_created_by= Yii::$app->user->id;
            try {
                if ($model->save()) {
                    LogAction::logUser($model->id, $model::tableName(), LogAction::TYPE_ADMIN_REQUIRE_CASHOUT, $model->getAttributes());
                    $transaction->commit();
                    //Trừ tiền người dùng
//                    $beforeTransaction = Yii::$app->user->identity->amount;
//                    Yii::$app->user->identity->amount -= $model->amount;
//
//                    if (Yii::$app->user->identity->save(true, ['amount', 'updated_at'])) {
//                        //Lưu bảng transaction
//                        if (CustomerTransaction::saveTransaction(
//                            Yii::$app->user->id,
//                            Yii::$app->user->identity->username,
//                            $model->amount,
//                            CustomerTransaction::TYPE_CUSTOMER_WIDRAWL,
//                            CustomerTransaction::SIGN_SUB,
//                            $beforeTransaction,
//                            Yii::$app->user->identity->amount
//                        )
//                        ) {
//                            LogAction::logUser($model->id, $model::tableName(), LogAction::TYPE_USER_REQUIRE_WITHDRAWL, $model->getAttributes());
//                            $transaction->commit();
//                            return true;
//                        }else{
//
//                        }
//                    }else{
//
//                    }
                } else {
                }

            } catch (Exception $ex) {
                $transaction->rollBack();
                $this->addError('amount', $ex);
//                return false;
            }

        }

        return $this;
    }


}
