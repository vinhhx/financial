<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 2:11 PM
 */
namespace app\modules\admin\forms\transactions;

use app\models\Customer;
use app\models\CustomerPoundageTransaction;
use app\models\CustomerRequireCashout;
use app\models\CustomerTransaction;
use app\models\LogAction;
use app\models\logs\AdminLogAction;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\Html;
use Yii;

class CompensateCommissionForm extends Model
{
    const TYPE_ADD = 1;
    const TYPE_SUB = 2;
    public $amount;
    public $type;
    public $customer_id;
    public $error;


    public function rules()
    {
        return [
            [['customer_id', 'type', 'amount'], 'required'],
            [['customer_id', 'type'], 'integer'],
            [['amount'], 'number', 'min' => 0],
            [['error'], 'safe']

        ];
    }

    public function attributeLabels()
    {
        return [
            'type' => Yii::t('customer', 'Loại hoa hồng bù'),
            'amount' => Yii::t('customer', 'Số tiền hoa hồng bù'),
        ];
    }

    public static function getTypeLabels()
    {
        return [
            static::TYPE_ADD => 'Cộng thêm hoa hồng',
            static::TYPE_SUB => 'Trừ hoa hồng',
        ];
    }

    public function saveCompensate()
    {
        //check nếu đang có 1 thao tác bù đang diễn ra chưa đc xác nhận thì sẽ phải
        if (CustomerPoundageTransaction::find()->where([
                'type' => CustomerPoundageTransaction::TYPE_POUNDAGE_COMPENSATE,
                'status' => CustomerPoundageTransaction::STATUS_PENDING,
                'customer_id' => $this->customer_id
            ])->limit(1)->one() !== null
        ) {
            $this->addError('error', 'Tài khoản này đang có tác vụ bù hoa hồng chưa được xác thực. Vui lòng xác thực trước khi tiếp tục.');
            return false;
        }
        $customer = Customer::findOne($this->customer_id);
        $signType = ($this->type == self::TYPE_SUB) ? CustomerPoundageTransaction::SIGN_SUB : CustomerPoundageTransaction::SIGN_ADD;
        if (!$customer->compensatePoundageSystem($this->amount,$signType)) {
            $this->addError('error', 'Người chơi này không tạo được hoa hồng bù');
            return false;
        } else {

            return true;
        }
        return false;
    }

}