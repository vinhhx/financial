<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 22-Jun-16
 * Time: 9:07 PM
 */
namespace app\modules\admin\forms\transactions;

use app\models\Customer;
use app\models\CustomerBankAddress;
use app\models\CustomerRequest;
use app\models\CustomerTransaction;
use app\models\CustomerTransactionQueue;
use app\models\SystemEmailQueue;
use yii\base\Model;

class AddReceiverForm extends Model
{
    public $receiver_id;
    public $receiver_bank_address;
    public $customer_sent_request_id;
    public $customer_sent_id;
    public $customer_sent_queue_id;
    public $customer_sent_username;
    public $amount;
    public $customer_sent_type;
    public $customer_sent_type_step;
    public $error;

    public function  rules()
    {
        return [
            [['receiver_id', 'receiver_bank_address', 'customer_sent_request_id', 'customer_sent_id',
                'customer_sent_queue_id', 'amount'], 'required', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['receiver_id', 'customer_sent_request_id', 'customer_sent_id', 'customer_sent_queue_id', 'customer_sent_type'], 'integer'],
            [['amount'], 'number'],
            [['receiver_bank_address', 'customer_sent_username'], 'string'],
            [['error'], 'safe'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'receiver_id' => 'Tài khoản nhận',
            'receiver_bank_address' => 'Địa chỉ ví',
            'customer_sent_request_id' => 'Phiếu yêu cầu nộp tiền',
            'customer_sent_sent_id' => 'Người chơi gửi',
            'customer_sent_queue_id' => 'Hàng chờ',
            'customer_sent_username' => 'Tk người gửi',
            'amount' => 'Số tiền',
            'customer_sent_type' => 'Loại yêu cầu',
            'error' => 'Lỗi',
        ];
    }

    public function createTransaction()
    {

        $receiver = Customer::findOne($this->receiver_id);
        $senderQueue = CustomerTransactionQueue::findOne($this->customer_sent_queue_id);
        $sender = Customer::findOne($this->customer_sent_id);
        if ($receiver) {
            //check bank address
            $bank = CustomerBankAddress::find()->where(['customer_id' => $receiver->id, 'id' => $this->receiver_bank_address])->limit(1)->one();
            if ($bank) {
                $this->receiver_bank_address = $bank->bank_address;
                if ($this->validate()) {
                    //Tạo queue nhận tiền để map với queue nộp tiền
                    $transaction = \Yii::$app->db->beginTransaction();
                    try {
                        //B1 create Request
                        $receiverRequest = new CustomerRequest();
                        $receiverRequest->customer_id = $receiver->id;
                        $receiverRequest->block_chain_wallet_id = $this->receiver_bank_address;
                        $receiverRequest->type = CustomerRequest::TYPE_RECEIVER_BY_SPECIAL;
                        $receiverRequest->status = CustomerRequest::STATUS_CREATED;
                        $receiverRequest->amount = $this->amount;
                        $receiverRequest->package_id = 0;
                        if ($receiverRequest->save()) {

                            //B2 tạo customer transaction
                            $customerTransactionQueue = new CustomerTransactionQueue();
                            $customerTransactionQueue->customer_id = $receiver->id;
                            $customerTransactionQueue->customer_username = $receiver->username;
                            $customerTransactionQueue->customer_request_id = $receiverRequest->id;
                            $customerTransactionQueue->type = CustomerTransaction::TYPE_RECEIVER_BY_SPECIAL;
                            $customerTransactionQueue->type_step = CustomerTransaction::TYPE_STEP_GET_HELP;
                            $customerTransactionQueue->status = CustomerTransactionQueue::STATUS_PENDING;
                            $customerTransactionQueue->amount = $this->amount;
                            $customerTransactionQueue->start_at = time();
                            if ($customerTransactionQueue->save()) {
                                //B3 Tạo Transaction
                                $customerTrancsaction = new CustomerTransaction();
                                $customerTrancsaction->customer_sent_queue_id = $senderQueue->id;
                                $customerTrancsaction->customer_sent_request_id = $senderQueue->customer_request_id;
                                $customerTrancsaction->customer_sent_id = $this->customer_sent_id;
                                $customerTrancsaction->customer_sent_username = $this->customer_sent_username;
                                $customerTrancsaction->customer_sent_type = $this->customer_sent_type;
                                $customerTrancsaction->customer_sent_ref_code = $sender->ref_code;
                                $customerTrancsaction->amount = $this->amount;
                                $customerTrancsaction->customer_receiver_request_id = $receiverRequest->id;
                                $customerTrancsaction->customer_receiver_queue_id = $customerTransactionQueue->id;
                                $customerTrancsaction->customer_receiver_id = $receiver->id;
                                $customerTrancsaction->customer_receiver_username = $receiver->username;
                                $customerTrancsaction->customer_receiver_ref_code = $receiver->ref_code;
                                $customerTrancsaction->customer_receiver_bank_address = $this->receiver_bank_address;
                                $customerTrancsaction->status = CustomerTransaction::STATUS_SEND_PENDING;
                                $customerTrancsaction->customer_receiver_type = CustomerTransaction::TYPE_RECEIVER_BY_SPECIAL;
                                $customerTrancsaction->type_step = $senderQueue->type_step;
                                $customerTrancsaction->expired_sent = time() + (int)\Yii::$app->params["TimeRequestLimit"];
                                $customerTrancsaction->expired_approve = time() + (int)\Yii::$app->params["TimeRequestLimit"];
                                if ($customerTrancsaction->save()) {
                                    //cap nhaatj laij trang thais queue
                                    $senderQueue->status = CustomerTransactionQueue::STATUS_ADDED;
                                    $senderQueue->save(true, ['status', 'updated_at']);

                                    $customerTransactionQueue->status = CustomerTransactionQueue::STATUS_ADDED;
                                    $customerTransactionQueue->save(true, ['status', 'updated_at']);
                                    $transaction->commit();
                                    //Gửi email thông báo yêu cầu chuyển tiền khi khách hàng đc map PH
                                    SystemEmailQueue::createEmailQueue(
                                        SystemEmailQueue::TYPE_CUSTOMER_PROVIDER_HELP,
                                        $sender->email,
                                        '[PH] You received PH at ' . \Yii::$app->formatter->asDatetime(time(), 'php:d-m-Y H:i:s'),
                                        'customer_require_ph',
                                        $customerTrancsaction->attributes
                                    );
                                    return true;
                                } else {
//                                    echo '<pre>';
//                                    var_dump($customerTrancsaction->getErrors());
//                                    die;
                                }


                            }
                        }

                    } catch (\Exception $ex) {
                        $transaction->rollBack();
//                        echo '<pre>';
//                        var_dump($ex->getMessage());
//                        die;
                        return false;
                    }
                }
            }
        }
    }

}