<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 2:11 PM
 */
namespace app\modules\admin\forms\transactions;

use app\models\CustomerRequireCashout;
use app\models\CustomerTransaction;
use app\models\LogAction;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\Html;
use Yii;

class CustomerActionForm extends Model
{
    public $file;
    public $transaction_id;
    public $type;
    public $customer_id;

    public function rules()
    {
        return [
            [['transaction_id','customer_id'], 'required'],
            [['transaction_id'], 'integer'],
            [['type'], 'string'],
            [['file'], 'file', 'extensions' => 'jpg, png, gif', 'mimeTypes' => 'image/jpeg, image/png, image/gif', 'maxSize' => 8 * 1024 * 1024 /* 1 Mb */],
            [['file'], 'safe'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => Yii::t('customer', 'File'),
        ];
    }

}


