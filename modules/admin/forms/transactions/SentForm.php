<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 22-Jun-16
 * Time: 9:07 PM
 */
namespace app\modules\admin\forms\transactions;

use app\models\Customer;
use app\models\CustomerBankAddress;
use app\models\CustomerRequest;
use app\models\CustomerTransaction;
use app\models\CustomerTransactionQueue;
use app\models\SystemEmailQueue;
use yii\base\Exception;
use yii\base\Model;

class SentForm extends Model
{
    public $sent_id;
    public $customer_receiver_request_id;
    public $customer_receiver_id;
    public $customer_receiver_queue_id;
    public $customer_receiver_username;
    public $amount_request;
    public $amount;
    public $customer_receiver_type;
    public $customer_receiver_type_step;
    public $error;

    public function  rules()
    {
        return [
            [['sent_id', 'customer_receiver_request_id', 'customer_receiver_id',
                'customer_receiver_queue_id', 'amount'], 'required', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['sent_id', 'customer_receiver_request_id', 'customer_receiver_id', 'customer_receiver_queue_id', 'customer_receiver_type'], 'integer'],
            [['amount', 'amount_request'], 'number'],
            ['amount', 'compare', 'compareAttribute' => 'amount_request', 'operator' => '<=', 'type' => 'number'],

            [['customer_receiver_username'], 'string'],
            [['error'], 'safe'],

        ];
    }

    function validatorAmountInput($attribute, $params)
    {
        if ($this->amount_request) {
            if ($this->amount_request > $this->$attribute) {
                $this->addError($attribute, 'Số tiền nhập vào phải nhở hơn số tiền yêu cầu rút');
            }
        }
    }

    public function attributeLabels()
    {
        return [
            'sent_id' => 'Tài khoản cho',
            'receiver_bank_address' => 'Địa chỉ ví',
            'customer_receiver_request_id' => 'Phiếu yêu cầu nộp tiền',
            'amount_request' => 'Số tiền người rút yêu cầu',
            'customer_receiver_id' => 'Người chơi gửi',
            'customer_receiver_queue_id' => 'Hàng chờ',
            'customer_receiver_username' => 'Tk người gửi',
            'amount' => 'Số tiền',
            'customer_receiver_type' => 'Loại yêu cầu',
            'error' => 'Lỗi',
        ];
    }

    public function createTransaction()
    {
        //Admin là người gửi
        $sent = Customer::findOne($this->sent_id); //Lấy thông tin người sẽ gửi
        $receiver = Customer::findOne($this->customer_receiver_id);
        $receiverQueue = CustomerTransactionQueue::findOne($this->customer_receiver_queue_id);
        //Nếu amount < amount yêu cầu rút thì cập nhật lại so tien queue dấy và tao 1 queue nữa để cân đối tiền rút
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ($sent) {
                if ($this->validate()) {
                    if ($receiverQueue->amount > $this->amount) {
                        $newAmount = $receiverQueue->amount - $this->amount;
                        $receiverQueue->amount = $this->amount;
                        if ($receiverQueue->save(true, ['amount', 'updated_at'])) {
                            $newReceiverQueue = new CustomerTransactionQueue();
                            $newReceiverQueue->customer_id = $receiverQueue->customer_id;
                            $newReceiverQueue->customer_username = $receiverQueue->customer_username;
                            $newReceiverQueue->customer_request_id = $receiverQueue->customer_request_id;
                            $newReceiverQueue->type = $receiverQueue->type;
                            $newReceiverQueue->type_step = $receiverQueue->type_step;
                            $newReceiverQueue->status = $receiverQueue->status;
                            $newReceiverQueue->start_at = $receiverQueue->start_at;
                            $newReceiverQueue->amount = $newAmount;
                            if (!$newReceiverQueue->save()) {
                                throw new Exception(' Not create new receiver queue');
                            }

                        } else {
                            throw new Exception(' Not update new amount to queue');
                        }
                    }
                    $receiverRequest = CustomerRequest::findOne(['id' => $receiverQueue->customer_request_id]);
                    //Tạo queue nhận tiền để map với queue nộp tiền
                    //B1 create Request
                    $sentRequest = new CustomerRequest();
                    $sentRequest->customer_id = $sent->id;
                    $sentRequest->block_chain_wallet_id = '';
                    $sentRequest->type = CustomerRequest::TYPE_SENT_BY_SPECIAL;
                    $sentRequest->status = CustomerRequest::STATUS_CREATED;
                    $sentRequest->amount = $this->amount;
                    $sentRequest->package_id = 0;
                    if ($sentRequest->save()) {

                        //B2 tạo customer transaction
                        $customerTransactionQueue = new CustomerTransactionQueue();
                        $customerTransactionQueue->customer_id = $sent->id;
                        $customerTransactionQueue->customer_username = $sent->username;
                        $customerTransactionQueue->customer_request_id = $sentRequest->id;
                        $customerTransactionQueue->type = CustomerTransaction::TYPE_SENT_BY_SPECIAL;
                        $customerTransactionQueue->type_step = CustomerTransaction::TYPE_STEP_PROVIDE_HELP;
                        $customerTransactionQueue->status = CustomerTransactionQueue::STATUS_PENDING;
                        $customerTransactionQueue->amount = $this->amount;
                        $customerTransactionQueue->start_at = time();
                        if ($customerTransactionQueue->save()) {
                            //B3 Tạo Transaction
                            $customerTransaction = new CustomerTransaction();
                            $customerTransaction->customer_sent_queue_id = $receiverQueue->id; //queue người gửi - tk admin
                            $customerTransaction->customer_sent_request_id = $sentRequest->id; // request người gửi  -tk admin
                            $customerTransaction->customer_sent_id = $sent->id; //user người nhận -- tk admin
                            $customerTransaction->customer_sent_username = $sent->username;   //user người gửi
                            $customerTransaction->customer_sent_type = $customerTransactionQueue->type; //loai
                            $customerTransaction->amount = $this->amount; //số tiền
                            $customerTransaction->customer_receiver_request_id = $this->customer_receiver_request_id;
                            $customerTransaction->customer_receiver_queue_id = $this->customer_receiver_queue_id;
                            $customerTransaction->customer_receiver_id = $this->customer_receiver_id;
                            $customerTransaction->customer_receiver_username = $this->customer_receiver_username;
                            $customerTransaction->customer_receiver_bank_address = $receiverRequest->block_chain_wallet_id;
                            $customerTransaction->status = CustomerTransaction::STATUS_SEND_PENDING;
                            $customerTransaction->customer_receiver_type = $receiverQueue->type;
                            $customerTransaction->type_step = $receiverQueue->type_step;
                            $customerTransaction->expired_sent = time() + (int)\Yii::$app->params["TimeRequestLimit"];
                            $customerTransaction->expired_approve = time() + (int)\Yii::$app->params["TimeRequestLimit"];
                            if ($customerTransaction->save()) {
                                //cap nhaatj laij trang thais queue
                                $receiverQueue->status = CustomerTransactionQueue::STATUS_ADDED;
                                $receiverQueue->save(true, ['status', 'updated_at']);

                                $customerTransactionQueue->status = CustomerTransactionQueue::STATUS_ADDED;
                                $customerTransactionQueue->save(true, ['status', 'updated_at']);

                                $transaction->commit();
                                SystemEmailQueue::createEmailQueue(
                                    SystemEmailQueue::TYPE_CUSTOMER_GET_HELP,
                                    $receiver->email,
                                    '[GH] You received GH at ' . \Yii::$app->formatter->asDatetime(time(), 'php:d-m-Y H:i:s'),
                                    'customer_received_gh',
                                    $customerTransaction->attributes
                                );
                                return true;
                            }

                        }
                    }


                }
            }
        } catch (\Exception $ex) {
//            $transaction->rollBack();
//            echo '<pre>';
//            var_dump($ex->getMessage());
//            die;
            return false;
        }
    }

}