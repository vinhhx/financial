<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 14-Dec-15
 * Time: 11:09 PM
 */

namespace app\modules\admin\forms;

use app\models\Customer;
use app\models\CustomerRequireCashout;
use app\models\CustomerToken;
use app\models\CustomerTransaction;
use app\models\investments\InvestCustomer;
use app\models\LogAction;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;
use yii\web\NotFoundHttpException;

class AddTokenForm extends Model
{
    public $quantity;
    public $customer_id;
    public $type;

    public function rules()
    {
        return [
            [['customer_id','type'], 'integer'],
            [['quantity'], 'integer', 'min' => 1],
            [['quantity', 'customer_id'], 'required', 'skipOnEmpty' => false, 'skipOnError' => false],
//            [['amount'], 'validateAmount'],
//            [['captcha'], 'captcha','captchaAction' => '/customer/default/captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'customer_id' => Yii::t('customers', 'Customer'),
            'quantity' => Yii::t('customers', 'Quantity'),
        ];
    }

//    public function validateAmount($attribute, $params)
//    {
//        if ((int)$this->$attribute < (int)25) {
//            $this->addError($attribute, Yii::t('customers', 'Number input is false.Please input number than 25 USD'));
//        } else if ($this->$attribute > (int)Yii::$app->user->identity->amount) {
//            $this->addError($attribute, Yii::t('customers', 'Number input is false.Please input number less than') . Yii::$app->formatter->asInteger(Yii::$app->user->identity->amount) . ' USD');
//        }
//    }


    public function addToken()
    {
        if ($this->validate()) {
            $customer = null;
            switch ($this->type) {
                case CustomerToken::TYPE_CUSTOMER:
                    $customer = Customer::findOne(['id' => $this->customer_id]);
                    break;
                case CustomerToken::TYPE_INVEST:
                    $customer = InvestCustomer::findOne(['id' => $this->customer_id]);
                    break;
                default:
                    $customer = null;
            }
            $result = [
                'status' => 'success',
                'message' => '',
                'token' => []
            ];
            if ($customer) {
                $i = 0;
                $maxExecute = $this->quantity + 100;
                $executed = 1;
                $resultData = [];
                do {
                    $token = $customer->createToken();
                    if ($token) {
                        $resultData[] = $token->id;
                        $result["token"][] = $token->token;
                        $i++;
                    }
                    $executed++;
                } while ($i < $this->quantity && $executed < $maxExecute);
                if ($i != $this->quantity) {
                    //delete các result đã tạo
                    Yii::$app->db->createCommand()->delete(CustomerToken::tableName(), ['customer_id' => $customer->id, 'id' => $resultData])->execute();
                    $result["status"] = "error";
                    $result["message"] = "Không tạo đủ số lượng token yêu cầu";

                }
                return $result;
            }else{
                return  $result = [
                    'status' => 'error',
                    'message' => 'Not find customer',
                    'token' => []
                ];
            }

        }
        return  $result = [
            'status' => 'error',
            'message' => 'Validate error',
            'token' => []
        ];
    }


}
