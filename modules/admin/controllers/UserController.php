<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\models\Customer;
use app\models\logs\AdminLogAction;
use app\models\permissions\AuthAssignment;
use Yii;
use yii\web\NotFoundHttpException;
use app\modules\admin\forms\UserSearch;
use app\models\User;
use app\models\permissions\AuthItem;

class UserController extends AdminBaseController
{

    public function actionCreate()
    {
        $model = new User();
        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->password_hash);
            $model->generateAuthKey();
//            $model->created_type=Customer::TYPE_ADMIN;
//            $model->created_by=Yii::$app->user->id;
            if ($model->save()) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_CREATE, $model->attributes);
                Yii::$app->session->setFlash('success', Yii::t('users', 'Create new user completed.'));
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            $attributeValidate = ['status', 'email', 'updated_at', 'full_name', 'phone'];
            if ($model->password_hash) {
                $model->setPassword($model->password_hash);
                $attributeValidate[] = 'password_hash';
                $attributeValidate[] = 'auth_key';

            }
            if ($model->save(true, $attributeValidate)) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->status = User::IS_DELETED;
        if ($model->save(true, ['status', 'updated_at'])) {
            AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_DELETE, $model->attributes);
            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the BannerSystem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BannerSystem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            if ($model->username == Yii::$app->params["HIDDEN_ADMIN"] && Yii::$app->user->identity->username != Yii::$app->params["HIDDEN_ADMIN"]) {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Phân quyền cho quản trị
     * @param $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionPermission($id)
    {
        $model = $this->findModel($id);

        $auth = Yii::$app->authManager;

        // Lấy các quyền được gán cho user
        $assignments = $auth->getAssignments($model->id);
        $selected = array_keys($assignments);

        if (Yii::$app->request->isPost && ($data = Yii::$app->request->post('data')) !== null) {
            $authAssignmentModel = new AuthAssignment();
            $authAssignmentModel->set($data, $model->id);
            Yii::$app->cache->delete('rbac');
            AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
            Yii::$app->session->setFlash('success', Yii::t('users', 'Set permission for user completed'));
            return $this->refresh();
        }

        return $this->render('permission', [
            'model' => $model,
            'items' => AuthItem::getItems(),
            'selected' => $selected,
        ]);
    }

}
