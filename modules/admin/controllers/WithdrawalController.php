<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\models\Customer;
use app\models\CustomerTransaction;
use app\models\LogAction;
use app\models\logs\AdminLogAction;
use app\models\User;
use Yii;
use app\models\CustomerRequireCashout;
use app\modules\admin\forms\search\CustomerRequireCashoutSearch;
use yii\base\Exception;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * WithdrawlController implements the CRUD actions for CustomerRequireCashout model.
 */
class WithdrawalController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'accept-request' => ['post'],
                    'reject-request' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerRequireCashout models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerRequireCashoutSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomerRequireCashout model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }



    /**
     * Finds the CustomerRequireCashout model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerRequireCashout the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerRequireCashout::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAcceptRequest($id)
    {
        $model = $this->findModel($id);
        $customer = Customer::findOne($model->customer_id);
        if (!$customer) {
            throw new NotFoundHttpException(Yii::t('app', 'Customer not existed'));
        }
        $model->status = $model::STATUS_APPROVED;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($model->save(true, ['status', 'updated_at'])) {
                //Thực hiện giao dịch trừ tiền của khách hàng
                $beforeTransaction = $customer->amount;
                $customer->amount -= $model->amount;

                if ($customer->save(true, ['amount', 'updated_at'])) {
                    //Lưu bảng transaction
                    if (CustomerTransaction::saveTransaction(
                        $customer->id,
                        $customer->username,
                        $model->amount,
                        CustomerTransaction::TYPE_CUSTOMER_WIDRAWL,
                        CustomerTransaction::SIGN_SUB,
                        $beforeTransaction,
                        $customer->amount
                    )
                    ) {
                        AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_CREATE, $model->attributes);
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('customers', 'Accept request cashout money. '));
                        return $this->redirect(['index']);
                    }
                }

            }
        } catch (Exception $ex) {
//            $transaction->rollBack();
            Yii::$app->session->setFlash('error', Yii::t('customers', $ex->getMessage()));
            return $this->redirect(['index']);
        }


        return $this->redirect(['index']);
    }

    public function actionRejectRequest($id)
    {
        $model = $this->findModel($id);
        $customer = Customer::findOne($model->customer_id);
        if (!$customer) {
            throw new NotFoundHttpException(Yii::t('app', 'Customer not existed'));
        }
        $model->status = $model::STATUS_DENIED;
//        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model->save(true, ['status', 'updated_at']);
            //Ho�n tr? ti?n cho kh�ch
//            $beforeTransaction = $customer->amount;
//            $customer->amount += $model->amount;
//
//            if ($customer->save(true, ['amount', 'updated_at'])) {
//                //L?u b?ng transaction
//                if (CustomerTransaction::saveTransaction(
//                    $customer->id,
//                    $customer->username,
//                    $model->amount,
//                    CustomerTransaction::TYPE_CUSTOMER_WIDRAWL_DELETED,
//                    CustomerTransaction::SIGN_ADD,
//                    $beforeTransaction,
//                    $customer->amount
//                )
//                ) {
            AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
//                    $transaction->commit();
            Yii::$app->session->setFlash('warning', Yii::t('customers', 'Reject request cashout money. '));

            return $this->redirect(['index']);
//                }
//            }

        } catch (Exception $ex) {
//            $transaction->rollBack();
            Yii::$app->session->setFlash('error', Yii::t('customers', $ex->getMessage()));
            return $this->redirect(['index']);
        }
    }
}
