<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\models\Customer;
use app\modules\admin\forms\transactions\CompensateCommissionForm;
use Yii;
use app\models\CustomerPoundageTransaction;
use app\modules\admin\forms\search\CustomerPoundageTransactionSearch;
use yii\base\Exception;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PoundageTransactionController implements the CRUD actions for CustomerPoundageTransaction model.
 */
class PoundageTransactionController extends AdminBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerPoundageTransaction models.
     * @return mixed
     */
    public function actionGroupCommission()
    {
        $searchModel = new CustomerPoundageTransactionSearch();
        $dataProvider = $searchModel->searchStaticPoundageGroup(Yii::$app->request->queryParams);

        return $this->render('group-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all CustomerPoundageTransaction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerPoundageTransactionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCompensateCommission($id)
    {
        $customer = Customer::findOne($id);
        if (!$customer) {
            throw new BadRequestHttpException('Hệ thống không tồn tại người chơi này');
        }
        if ($customer->is_deleted == Customer::IS_DELETED) {
            throw new MethodNotAllowedHttpException('Tài khoản này trong trạng thái xóa bạn không thể thao tác được tác vụ này');
        }
        if ($customer->is_active == Customer::IS_ACTIVE_PENDING) {
            throw new MethodNotAllowedHttpException('Tài khoản này chưa được kích hoạt bạn không thể thao tác được tác vụ này');
        }
        if ($customer->status !== Customer::STATUS_ACTIVE) {
            throw new MethodNotAllowedHttpException('Tài khoản này đang bị khóa bạn không thể thao tác được tác vụ này');
        }
        $wallet = $customer->findWallet();
        $model = new CompensateCommissionForm();
        $model->customer_id = $customer->id;
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            try {
                if ($model->type == $model::TYPE_SUB && ($model->amount > $wallet->poundage)) {
                    $model->addError('error', 'Giá trị bù trừ hoa hồng không được vượt giá giá trị hoa hồng');
                }
                if ($model->saveCompensate()) {
                    Yii::$app->session->setFlash('success', 'Bạn đã thực hiện thành công thao tác bù tiền hoa hồng');
                    return $this->redirect(['group-commission']);
                } else {
                    if (!empty($model->getErrors())) {
                        Yii::$app->session->setFlash('error', reset(array_shift($model->getErrors())));
                    }
                }
            } catch (\Exception $ex) {
                Yii::$app->session->setFlash('error', $ex->getMessage());
            }

        }
        return $this->render('compensate', [
            'model' => $model,
            'customer' => $customer,
            'wallet' => $wallet,
        ]);
    }

//    /**
//     * Displays a single CustomerPoundageTransaction model.
//     * @param integer $id
//     * @return mixed
//     */
//    public function actionView($id)
//    {
//        return $this->render('view', [
//            'model' => $this->findModel($id),
//        ]);
//    }

//    /**
//     * Creates a new CustomerPoundageTransaction model.
//     * If creation is successful, the browser will be redirected to the 'view' page.
//     * @return mixed
//     */
//    public function actionCreate()
//    {
//        $model = new CustomerPoundageTransaction();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
//    }

//    /**
//     * Updates an existing CustomerPoundageTransaction model.
//     * If update is successful, the browser will be redirected to the 'view' page.
//     * @param integer $id
//     * @return mixed
//     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
//    }

//    /**
//     * Deletes an existing CustomerPoundageTransaction model.
//     * If deletion is successful, the browser will be redirected to the 'index' page.
//     * @param integer $id
//     * @return mixed
//     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
//    }

    /**
     * Finds the CustomerPoundageTransaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerPoundageTransaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerPoundageTransaction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionApproveCompensate($id)
    {
        //
        if (!Yii::$app->request->isPost) {
            throw new BadRequestHttpException('Yêu cầu không hợp lệ');
        }
        $model = $this->findModel($id);
        if ($model->type !== CustomerPoundageTransaction::TYPE_POUNDAGE_COMPENSATE) {
            throw new BadRequestHttpException('Giao dịch không chính xác');
        }
        if ($model->status !== CustomerPoundageTransaction::STATUS_PENDING) {
            throw new BadRequestHttpException('Giao dịch không chính xác');
        }
        if ($model->approvedCompensate()) {
            Yii::$app->session->setFlash('success', 'Bù hoa hồng thành công !');
        } else {
            Yii::$app->session->setFlash('error', 'Bù hoa hồng xảy ra lỗi !');
        }
        return $this->redirect(['compensate']);

    }

    public function actionCompensate()
    {
        $searchModel = new CustomerPoundageTransactionSearch();
        $params = Yii::$app->request->queryParams;
        $params['CustomerPoundageTransactionSearch']['type'] = CustomerPoundageTransaction::TYPE_POUNDAGE_COMPENSATE;
        $params['CustomerPoundageTransactionSearch']['status'] = CustomerPoundageTransaction::STATUS_PENDING;
        $dataProvider = $searchModel->search($params);

        return $this->render('index-compensate', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }
}
