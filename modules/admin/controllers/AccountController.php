<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\models\investments\InvestCustomer;
use app\models\investments\InvestCustomerPackageHistory;
use app\models\investments\InvestCustomerWallet;
use app\modules\admin\forms\investments\InvestCustomerWalletSearch;
use Yii;
use app\models\investments\InvestCustomerPackage;
use app\modules\admin\forms\investments\InvestCustomerPackageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AccountController implements the CRUD actions for InvestCustomerPackage model.
 */
class AccountController extends AdminBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InvestCustomerPackage models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvestCustomerWalletSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InvestCustomerPackage model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the InvestCustomerPackage model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InvestCustomerPackage the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InvestCustomerPackage::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

//    public function actionRegister($id)
//    {
//        $customer = InvestCustomer::findOne($id);
//        if ($customer) {
//            $wallet = $customer->getCurrentWallet();
//            $customerPackage = new InvestCustomerPackage();
//            $customerPackage->customer_id = $customer->id;
//            $customerPackage->package_id = 1000000;
//            $customerPackage->amount = 1;
//            $customerPackage->status = InvestCustomerPackage::STATUS_ACTIVE;
//            $customerPackage->is_active = InvestCustomerPackage::IS_ACTIVE_SUCCESS;
//            $customerPackage->current_profit = 0;
//            $customerPackage->total_current_profit = 0;
//            $customerPackage->joined_at = time();
//            $customerPackage->last_profit_receiver = time();
//            $customerPackage->last_cash_out_at = time();
//
//            $transaction = Yii::$app->db->beginTransaction();
//            try {
//                $customer->addInvestCustomerCase();
//                //Tao Pakage
//                if ($customerPackage->save()) {
//                    //Tao Package History
//                    $packHis = new InvestCustomerPackageHistory();
//                    $packHis->customer_package_id = $customerPackage->id;
//                    $packHis->sign = InvestCustomerPackageHistory::SIGN_ADD;
//                    $packHis->amount = 0;
//                    $packHis->type = InvestCustomerPackageHistory::TYPE_OPEN_PACKAGE;
//                    if (!$packHis->save()) {
//                        throw new Exception("Not insert to pack history.");
//                    }
//                    //Tao request nop tien
//                    $transaction->commit();
//                } else {
//                    throw new \Exception('Not create new package');
//                }
//            } catch (\Exception $ex) {
//                $transaction->rollBack();
//                return [
//                    "status" => "success",
//                    "code" => 400,
//                    "message" => $ex->getMessage()
//                ];
//            }
//        }
//    }
}
