<?php
/**
 * Created by PhpStorm.
 * User: quyet
 * Date: 4/10/2016
 * Time: 1:16 PM
 */

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\components\helpers\UploadHelpers;
use app\modules\admin\forms\MediaForm;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\BaseFileHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\web\UploadedFile;

class FroalaController extends AdminBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'upload' => ['POST'],
                ],
            ],
        ];
    }

    public function actionUpload()
    {
        $model = new MediaForm();

        Yii::$app->response->format = Response::FORMAT_JSON;

        $model->file = UploadedFile::getInstance($model, 'file');

        if ($model->validate(['file'])) {

            $fileName = md5(uniqid() . $model->file->name) . '.' . $model->file->extension;

            // Uploaded dir
            $folderId = strtotime(date('Y-m-01 00:00:00'));

            $uploadDir = UploadHelpers::getUploadDirById($folderId, 'news');

            // Create folder upload
            BaseFileHelper::createDirectory($uploadDir);

            if ($model->file->saveAs($uploadDir . $fileName)) {
                return [
                    'link' => UploadHelpers::getImageById($folderId, $fileName, 'news'),
                ];
            }
        }

        return $model->getErrors();
    }

    public function actionManager()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $result = [];

        return $result;
    }

    protected function getNamespaceFromString($string, $target = '-')
    {
        return str_replace($target, '\\', $string);
    }
}