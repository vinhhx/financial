<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\models\logs\AdminLogAction;
use Yii;
use app\models\Package;
use app\modules\admin\forms\PackageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PackageController implements the CRUD actions for Package model.
 */
class PackageController extends AdminBaseController
{
    public function behaviors()
    {
        $this->layout = 'admin';

        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Package models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PackageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Package model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model=$this->findModel($id);
        $model->config = $model->getConfigDefault();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Package model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Package();
        $model->config = $model->getConfigDefault();
        if ($model->load(Yii::$app->request->post())) {
            $config = $model->config;
            $model->config = json_encode($model->config);
            if ($model->save()) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_CREATE, $model->attributes);

                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        return $this->render('create', [
            'model' => $model,
        ]);

    }

    /**
     * Updates an existing Package model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->config = $model->getConfigDefault();
        if ($model->load(Yii::$app->request->post())) {
            $config = $model->config;
            $model->config = json_encode($config);
            if ($model->save()) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                return $this->redirect(['view', 'id' => $model->id]);
            }

        }
        return $this->render('update', [
            'model' => $model,
        ]);

    }


    /**
     * Finds the Package model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Package the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Package::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
