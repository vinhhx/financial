<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\models\Customer;
use app\models\CustomerTransaction;
use app\models\CustomerTransactionQueue;
use app\models\LogAction;
use app\modules\admin\forms\LoginForm;
use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class ToolController extends AdminBaseController
{

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionAddmountbyweek()
    {
        ini_set('max_execution_time', 600);
        $query = (new \yii\db\Query())
            ->select('id,username,package_id,amount,last_auto_increase')
            ->from('customer');
        $query->andFilterWhere(['status' => 1]);
        $query->andFilterWhere(["!=", "is_deleted", \app\models\Customer::IS_DELETED]);
        $query->andFilterWhere(['>=', 'DATEDIFF(NOW(),DATE(last_auto_increase))', 7]);
        $data = $query->all();
        if ($data) {
            foreach ($data as $item) {
                if (strtotime(date('Y-m-d')) >= strtotime(date('Y-m-d', strtotime($item['last_auto_increase'] . "+7 days")))) {
                    $package = \app\models\Package::findOne($item['package_id']);
                    if ($package) {
                        $interest = ((float)$package->amount * (float)$package->increase_percent / 100) / 4;
                        $amount_new = (float)$item['amount'] + $interest;
                        \Yii::$app->db->createCommand()
                            ->update('customer', [
                                'amount' => $amount_new,
                                'last_auto_increase' => date('Y-m-d', strtotime($item['last_auto_increase'] . "+7 days")),
                            ], 'id=' . $item['id'])
                            ->execute();
                        \app\models\Customer::saveCustomerTransaction($item['id'], $item['username'], $interest, (int)$item['amount'], $amount_new, \app\models\CustomerTransaction::TYPE_ADD_INTEREST_WEEK, \app\models\CustomerTransaction::SIGN_ADD);
                    }
                }
            }
        }
        exit('Done');
    }

    public function actionResetCustomerLastIncrease($id = null)
    {
        $query = (new Query())
            ->select('id,username,package_id,amount,last_auto_increase')
            ->from('customer');
        $query->andFilterWhere(['status' => 1]);
        $query->andFilterWhere(["!=", "is_deleted", \app\models\Customer::IS_DELETED]);
        $query->andFilterWhere(['>=', 'DATEDIFF(NOW(),DATE(last_auto_increase))', 7]);
        $data = $query->all();
        if ($data) {
            foreach ($data as $item) {
                if (strtotime(date('Y-m-d')) >= strtotime(date('Y-m-d', strtotime($item['last_auto_increase'] . "+7 days")))) {
//                    $days=strtotime(date('Y-m-d'))- strtotime($item['last_auto_increase']);
//                    var_dump($days/(24*60*60));
//                    echo'<br>';

                    $dateNow = date_create(date('Y-m-d'));
                    $dateCreate = date_create($item['last_auto_increase']);
                    $dDiff = $dateNow->diff($dateCreate);
                    $days = $dDiff->days;
                    if ($days % 7 == 0) {
                        $dayIncrease = date('Y-m-d');

                    } else {
                        $dayIncrease = date('Y-m-d', strtotime(date('Y-m-d') . "-" . ($days % 7) . " days"));
                    }
                    \Yii::$app->db->createCommand()
                        ->update('customer', [
                            'last_auto_increase' => $dayIncrease,
                        ], 'id=' . $item['id'])
                        ->execute();
                    echo $dayIncrease;
                    echo '<br>';

                }
            }
        }
    }

    public function actionResetMoney()
    {
        $customers = Customer::find()->where(['status' => Customer::STATUS_ACTIVE])
            ->all();
        foreach ($customers as $cus) {
            if ($cus->amount > 0) {
                $balance_before = $cus->amount;
                $cus->amount = 0;
                if ($cus->save(true, ['amount'])) {
                    echo $cus->id;
                    //T?o b?n ghi transaction reset
                    $custran = new CustomerTransaction();
                    $custran->customer_id = (int)$cus->id;
                    $custran->customer_username = $cus->username;
                    $custran->amount = (float)$balance_before;
                    $custran->bill_id = 0;
                    $custran->type = CustomerTransaction::TYPE_SYSTEM_RESET;
                    $custran->status = 1;
                    $custran->sign = CustomerTransaction::SIGN_SUB;
                    $custran->balance_after = (float)0;
                    $custran->balance_before = (float)$balance_before;
                    if ($custran->save()) {
                        $logType = LogAction::TYPE_SYSTEM_RESET_AMOUNT;
                        LogAction::logAdmin($custran->id, $custran::tableName(), $logType, ArrayHelper::toArray($custran));
                    }
                }

            }

        }

    }

    public function actionVersionUpdate()
    {
        return $this->render('version-update');
    }

    public function actionTestSendMail()
    {

        $message = \Yii::$app->mailer->compose('register-completed', [
            "id" => "1000000",
            "username" => "hoangxuanvinh",
            "phone" => "0986118464",
            "email" => "hoangxuanvinh88@gmai.com"
        ]);

        $result_code = $message->
        setFrom([Yii::$app->params["emailSend"] => 'LINCOLNMGT.COM'])
            ->setTo('hoangxuanvinh88@gmail.com')
            ->setSubject('Test send email ' . date('d/m/Y H:i:s'))
            ->send();

        var_dump($result_code);
    }

    public function actionRepairTransactionType()
    {
        $transaction = CustomerTransaction::find()->all();
        foreach ($transaction as $tran) {
            $queue = CustomerTransactionQueue::find()->where(['id' => [$tran->customer_sent_queue_id, $tran->customer_receiver_queue_id]])->indexBy('id')->all();
            $tran->customer_sent_type = $queue[$tran->customer_sent_queue_id]->type;
            $tran->customer_receiver_type = $queue[$tran->customer_receiver_queue_id]->type;
            if($tran->save(true, ['customer_sent_type', 'customer_receiver_type'])){
                echo 'Cập nhật thành công lại type cho nhận của transaction ' . $tran->id."<br/>";

            }else{
                echo 'Không cập nhật đc ' . $tran->id."<br/>";
            }
        }
    }

}
