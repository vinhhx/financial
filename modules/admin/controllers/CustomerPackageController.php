<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\models\CustomerPackage;
use app\models\CustomerRequest;
use app\models\CustomerTransaction;
use app\models\logs\AdminLogAction;
use app\models\Package;
use app\models\Customer;
use app\modules\admin\forms\search\CustomerRequestStatisticSearch;
use app\modules\admin\forms\search\CustomerSearch;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomerPackageController extends AdminBaseController
{

    public function actionIndex()
    {
        $searchModel = new CustomerRequestStatisticSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $models = $dataProvider->getModels();
        $modelKeys = $dataProvider->getKeys();
        $totalRecord = $dataProvider->getTotalCount();
        $pagination = $dataProvider->getPagination();
        $datas = [];
        if ($models) {
            foreach ($models as $model) {
                $datas[$model->id]["customer"] = ArrayHelper::toArray($model->customer);
                $datas[$model->id]["request"] = ArrayHelper::toArray($model);
                $datas[$model->id]["sent"] = [];
                $datas[$model->id]["receiver"] = [];
                if ($model->sent) {
                    foreach ($model->sent as $transaction) {
                        $datas[$model->id]["sent"][] = ArrayHelper::toArray($transaction);
                    }
                }
                if ($model->receive) {
                    foreach ($model->receive as $transaction) {
                        $datas[$model->id]["receiver"][] = ArrayHelper::toArray($transaction);

                    }
                }

                if ($model->customerPackage) {
                    $datas[$model->id]["package"] = ArrayHelper::toArray($model->customerPackage);
                } else {
                    $datas[$model->id]["package"] = [];
                }
            }
        }
        $arrayDataProvider = new ArrayDataProvider([
            'allModels' => $datas,
        ]);
//        $arrayDataProvider->setPagination($pagination);
//        $arrayDataProvider->pagination->totalCount=$totalRecord;
//        $arrayDataProvider->set=$totalRecord;
        return $this->render('index', [
            'dataProvider' => $arrayDataProvider,
            'pagination' => $dataProvider->pagination,
            'searchModel' => $searchModel
        ]);
    }

    public function actionExportExcel()
    {
        set_time_limit(1000);
        if (!\Yii::$app->request->isPost) {
            throw new \yii\web\MethodNotAllowedHttpException('Truy cập không hợp lệ.');
        }

        /**
         * Lay du lieu model Index
         */
        $searchModel = new CustomerRequestStatisticSearch();
        $dataProvider = $searchModel->searchFull(\Yii::$app->request->queryParams);
        $models = $dataProvider->getModels();
        $modelKeys = $dataProvider->getKeys();
        $totalRecord = $dataProvider->getTotalCount();
        $pagination = $dataProvider->getPagination();
        $datas = [];
        if ($models) {
            foreach ($models as $key => $model) {
                $datas[$key]["customer"] = ArrayHelper::toArray($model->customer);
                $datas[$key]["request"] = ArrayHelper::toArray($model);
                $datas[$key]["sent"] = [];
                $datas[$key]["receiver"] = [];
                if ($model->sent) {
                    foreach ($model->sent as $transaction) {
                        $datas[$key]["sent"][] = ArrayHelper::toArray($transaction);
                    }
                }
                if ($model->receive) {
                    foreach ($model->receive as $transaction) {
                        $datas[$key]["receiver"][] = ArrayHelper::toArray($transaction);

                    }
                }

                if ($model->customerPackage) {
                    $datas[$key]["package"] = ArrayHelper::toArray($model->customerPackage);
                } else {
                    $datas[$key]["package"] = [];
                }
            }
        }
        $arrayDataProvider = new ArrayDataProvider([
            'allModels' => $datas,
            'pagination' => [
                'pageSize' => false,
            ],
        ]);
//        $arrayDataProvider->setPagination($pagination);
//        $arrayDataProvider->setTotalCount($totalRecord);
        $page = Yii::$app->request->get('page', 1);
        $models = $arrayDataProvider->getModels();
        /**
         * Xuat file excel
         */
        // Load excel template
        $phpExcel = \PHPExcel_IOFactory::load(\Yii::getAlias('@app/web/template/customer-package.xlsx'));

        $phpExcel->setActiveSheetIndex(0);

        // Dòng bắt đầu fill data
        $baseRow = 6;

        $totalRows = count($models);
        $i = 1;
        foreach ($models as $n => $model) {
            $typeRequest = '';
            $modelRequestType = null;
            if (CustomerRequest::getRequestLabel($model["request"]["type"]) == 'PH') {
                if (!empty($model["sent"])) {
                    $typeRequest = 'Chuyển lần: ';
                    $modelRequestType = $model["sent"];
                }
            } elseif (CustomerRequest::getRequestLabel($model["request"]["type"]) == 'GH') {
                if (!empty($model["receiver"])) {
                    $typeRequest = 'Nhận lần: ';
                    $modelRequestType = $model["receiver"];
                }
            }
            if ($modelRequestType) {
                foreach ($modelRequestType as $key => $value) {
                    $row = $baseRow + $i;
                    $phpExcel->getActiveSheet()->insertNewRowBefore($row, 1);
                    $phpExcel->getActiveSheet()
                        ->setCellValue('A' . $row, $i + 1)
                        ->setCellValue('B' . $row, isset($model["customer"]) ? $model["customer"]["username"] : '')
                        ->setCellValue('C' . $row, (isset($model["package"]) && !empty($model["package"])) ? $model["package"]["package_name"] : "Hoa hồng")
                        ->setCellValue('D' . $row, '[' . CustomerRequest::getRequestLabel($model["request"]["type"]) . '] ' . Yii::$app->formatter->asDecimal($model["request"]["amount"], 8) . Yii::$app->params["unit"])
                        ->setCellValue('E' . $row, Yii::$app->formatter->asDatetime($model["request"]["created_at"]))
                        ->setCellValue('F' . $row, $typeRequest . ($key + 1))
                        ->setCellValue('G' . $row, Yii::$app->formatter->asDecimal($value["amount"], 8))
                        ->setCellValue('H' . $row, Yii::$app->formatter->asDatetime($value["created_at"]))
                        ->setCellValue('I' . $row, CustomerTransaction::getStatusLabels($value["status"]));
                    $i++;
                }
            } else {
                $row = $baseRow + $i;
                $phpExcel->getActiveSheet()->insertNewRowBefore($row, 1);
                $phpExcel->getActiveSheet()
                    ->setCellValue('A' . $row, $i + 1)
                    ->setCellValue('B' . $row, isset($model["customer"]) ? $model["customer"]["username"] : '')
                    ->setCellValue('C' . $row, (isset($model["package"]) && !empty($model["package"])) ? $model["package"]["package_name"] : "Hoa hồng")
                    ->setCellValue('D' . $row, '[' . CustomerRequest::getRequestLabel($model["request"]["type"]) . '] ' . Yii::$app->formatter->asDecimal($model["request"]["amount"], 8) . Yii::$app->params["unit"])
                    ->setCellValue('E' . $row, Yii::$app->formatter->asDatetime($model["request"]["created_at"]))
                    ->setCellValue('F' . $row, '')
                    ->setCellValue('G' . $row, '')
                    ->setCellValue('H' . $row, '')
                    ->setCellValue('I' . $row, '');
                $i++;
            }
        }

        $phpExcel->getActiveSheet()->removeRow($baseRow - 1, 1);

        $filename = 'Customer_Package_Request_page' . $page . \Yii::$app->formatter->asDate(time());

        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');

        ob_end_clean();
        header("Cache-Control: no-cache");
        header("Expires: 0");
        header("Pragma: no-cache");
        header("Content-Type: application/xlsx");
        header("Content-Disposition: attachment; filename={$filename}.xlsx");

        $objWriter->save('php://output');
        AdminLogAction::createLogAction(AdminLogAction::ACTION_EXPORT_EXCEL, []);
        Yii::$app->end();
    }

}
