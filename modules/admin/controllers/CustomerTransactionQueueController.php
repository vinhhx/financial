<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\models\Customer;
use app\models\CustomerBankAddress;
use app\models\CustomerPoundageTransaction;
use app\models\CustomerRequest;
use app\models\CustomerTransaction;
use app\models\logs\AdminLogAction;
use app\modules\admin\forms\transactions\AddReceiverForm;
use app\modules\admin\forms\transactions\SentForm;
use Yii;
use app\models\CustomerTransactionQueue;
use app\modules\admin\forms\search\CustomerTransactionQueueSearch;
use yii\base\Exception;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CustomerTransactionQueueController implements the CRUD actions for CustomerTransactionQueue model.
 */
class CustomerTransactionQueueController extends AdminBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'completed-gh' => ['POST']
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerTransactionQueue models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $type = Yii::$app->request->post('type', '');
            switch ($type) {
                case 'add-queue':
                    $id = Yii::$app->request->post('id', 0);
                    $customers = Customer::getAllCustomer(0, Customer::TYPE_ADMIN);
                    $customers = ArrayHelper::map($customers, 'id', 'fullName');
                    if ($id) {
                        $queue = CustomerTransactionQueue::findOne(['id' => $id]);
                        if ($queue) {
                            $model = null;
                            switch ($queue->type) {
                                case CustomerTransaction::TYPE_OPEN_PACKAGE:
                                    $model = new AddReceiverForm();
                                    $model->customer_sent_id = $queue->customer_id;
                                    $model->customer_sent_username = $queue->customer_username;
                                    $model->customer_sent_queue_id = $queue->id;
                                    $model->customer_sent_request_id = $queue->customer_request_id;
                                    $model->customer_sent_type = $queue->type;
                                    $model->customer_sent_type_step = $queue->type_step;
                                    $model->amount = $queue->amount;
                                    break;
                                default:
                                    break;
                            }
                            return $html = $this->renderAjax('forms/_request-add', ['model' => $model, 'customers' => $customers]);
                        }
                    }
                    break;
                case"get-bank-address":
                    $customerId = Yii::$app->request->post('cid', 0);
                    if ($customerId) {
                        $banks = CustomerBankAddress::find()->where(['customer_id' => $customerId])->all();
                        if ($banks) {
                            $banks = ArrayHelper::map($banks, 'id', 'bank_address');

                        }
                        $result = Html::tag('option', '-- Chọn địa chỉ ngân hàng --', ['value' => '']);
                        if ($banks) {
                            foreach ($banks as $k => $v) {
                                //  $selected = $selectedId == $k ? true : false;
                                $result .= Html::tag('option', $v, ['value' => $k, false]);
                            }
                        }
                        return $result;
                    }
                    break;
                case "submit-add-receiver":
                    $model = new AddReceiverForm();
                    if ($model->load(Yii::$app->request->post())) {
                        if ((int)$model->customer_sent_queue_id) {
                            $queueSent = CustomerTransactionQueue::findOne(['id' => $model->customer_sent_queue_id]);

                            if ($queueSent) {
                                $model->customer_sent_id = $queueSent->customer_id;
                                $model->customer_sent_username = $queueSent->customer_username;
                                $model->customer_sent_queue_id = $queueSent->id;
                                $model->customer_sent_request_id = $queueSent->customer_request_id;
                                $model->customer_sent_type = $queueSent->type;
                                $model->customer_sent_type_step = $queueSent->type_step;
                                $model->amount = $queueSent->amount;
                                $result = $model->createTransaction();
                                AdminLogAction::createLogAction(AdminLogAction::ACTION_CUSTOMER_TRANSACTION_QUEUE_ADD_RECEIVER, $model->attributes);
                                if ($result) {
                                    return ["status" => "success"];
                                } else {
                                    return ["status" => "error"];
                                }
                            }
                        }

                    }
                    break;
                default:
                    break;
            }
            return;
        }
        $searchModel = new CustomerTransactionQueueSearch();
        $queries = Yii::$app->request->queryParams;
        $queries["CustomerTransactionQueueSearch"]['status'] = CustomerTransactionQueue::STATUS_PENDING;
        $queries["CustomerTransactionQueueSearch"]['type'] = CustomerTransaction::TYPE_OPEN_PACKAGE;
        $dataProvider = $searchModel->search($queries);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all CustomerTransactionQueue models.
     * @return mixed
     */
    public function actionIndexProviderHelp()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $type = Yii::$app->request->post('type', '');
            switch ($type) {
                case 'add-queue':
                    $id = Yii::$app->request->post('id', 0);
                    $customers = Customer::getAllCustomer(0, Customer::TYPE_ADMIN);
                    $customers = ArrayHelper::map($customers, 'id', 'fullName');
                    if ($id) {
                        $queue = CustomerTransactionQueue::findOne(['id' => $id]);
                        if ($queue) {
                            $model = null;
                            switch ($queue->type) {
                                case CustomerTransaction::TYPE_CASH_OUT_PACKAGE:
                                case CustomerTransaction::TYPE_CASH_OUT_POUNDAGE:
                                    $model = new SentForm();
                                    //Thông tin người nhận
                                    $model->customer_receiver_id = $queue->customer_id;
                                    $model->customer_receiver_username = $queue->customer_username;
                                    $model->customer_receiver_queue_id = $queue->id;
                                    $model->customer_receiver_request_id = $queue->customer_request_id;
                                    $model->customer_receiver_type = $queue->type;
                                    $model->customer_receiver_type_step = $queue->type_step;
                                    $model->amount = $queue->amount;
                                    $model->amount_request = $queue->amount;

                                    break;
                                default:
                                    break;
                            }
                            return $html = $this->renderAjax('forms/_request-sent', ['model' => $model, 'customers' => $customers]);
                        }
                    }
                    break;
//                case"get-bank-address":
//                    $customerId = Yii::$app->request->post('cid', 0);
//                    if ($customerId) {
//                        $banks = CustomerBankAddress::find()->where(['customer_id' => $customerId])->all();
//                        if ($banks) {
//                            $banks = ArrayHelper::map($banks, 'id', 'bank_address');
//
//                        }
//                        $result = Html::tag('option', '-- Chọn địa chỉ ngân hàng --', ['value' => '']);
//                        if ($banks) {
//                            foreach ($banks as $k => $v) {
//                                //  $selected = $selectedId == $k ? true : false;
//                                $result .= Html::tag('option', $v, ['value' => $k, false]);
//                            }
//                        }
//                        return $result;
//                    }
//                    break;
                case "submit-add-sent":
                    $model = new SentForm();
                    if ($model->load(Yii::$app->request->post())) {
                        if ((int)$model->customer_receiver_queue_id) {
                            $queueReceiver = CustomerTransactionQueue::findOne(['id' => $model->customer_receiver_queue_id]);
                            if ($queueReceiver) {
                                $model->customer_receiver_id = $queueReceiver->customer_id;
                                $model->customer_receiver_username = $queueReceiver->customer_username;
                                $model->customer_receiver_queue_id = $queueReceiver->id;
                                $model->customer_receiver_request_id = $queueReceiver->customer_request_id;
                                $model->customer_receiver_type = $queueReceiver->type;
                                $model->customer_receiver_type_step = $queueReceiver->type_step;
                                $model->amount_request = $queueReceiver->amount;
                                $result = $model->createTransaction();
                                AdminLogAction::createLogAction(AdminLogAction::ACTION_CUSTOMER_TRANSACTION_QUEUE_ADD_SENDER, $model->attributes);
                                if ($result) {
                                    return ["status" => "success"];
                                } else {
                                    return ["status" => "error"];
                                }
                            }
                        }

                    }
                    break;
                default:
                    break;
            }
            return;
        }
        $searchModel = new CustomerTransactionQueueSearch();
        $queries = Yii::$app->request->queryParams;
        $queries["CustomerTransactionQueueSearch"]['status'] = CustomerTransactionQueue::STATUS_PENDING;
        $queries["CustomerTransactionQueueSearch"]['type'] = [CustomerTransaction::TYPE_CASH_OUT_PACKAGE, CustomerTransaction::TYPE_CASH_OUT_POUNDAGE];
        $dataProvider = $searchModel->search($queries);

        return $this->render('index-provider-help', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomerTransactionQueue model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionExportExcel()
    {
        set_time_limit(1000);

        if (!Yii::$app->request->isPost) {
            throw new \yii\web\MethodNotAllowedHttpException('Truy cập không hợp lệ.');
        }

        $searchModel = new CustomerTransactionQueueSearch();
        $type = Yii::$app->request->get('type');
        $queries = Yii::$app->request->queryParams;
        if ($type == 'index') {
            $queries["CustomerTransactionQueueSearch"]['status'] = CustomerTransactionQueue::STATUS_PENDING;
            $queries["CustomerTransactionQueueSearch"]['type'] = CustomerTransaction::TYPE_OPEN_PACKAGE;
        } elseif ($type == 'index-provider-help') {
            $queries["CustomerTransactionQueueSearch"]['status'] = CustomerTransactionQueue::STATUS_PENDING;
            $queries["CustomerTransactionQueueSearch"]['type'] = [CustomerTransaction::TYPE_CASH_OUT_PACKAGE, CustomerTransaction::TYPE_CASH_OUT_POUNDAGE];
        }
        $dataProvider = $searchModel->search($queries);
        $dataProvider->pagination = false;
        $models = $dataProvider->getModels();

        // Load excel template
        $phpExcel = \PHPExcel_IOFactory::load(\Yii::getAlias('@app/web/template/customer-transaction-queue.xlsx'));

        $phpExcel->setActiveSheetIndex(0);

        // Dòng bắt đầu fill data
        $baseRow = 6;

        $totalRows = count($models);
        //echo '<pre>';var_dump($models);die;
        /* @var $itemModel CustomerTransactionQueueSearch */
        foreach ($models as $n => $itemModel) {
            $row = $baseRow + $n;
            $phpExcel->getActiveSheet()->insertNewRowBefore($row, 1);
            $phpExcel->getActiveSheet()
                ->setCellValue('A' . $row, $n + 1)
                ->setCellValue('B' . $row, $itemModel->id)
                ->setCellValue('C' . $row, $itemModel->customer_id)
                ->setCellValue('D' . $row, $itemModel->customer_username)
                ->setCellValue('E' . $row, $itemModel->getStatusLabel())
                ->setCellValue('F' . $row, $itemModel->getTypeStepLabel())
                ->setCellValue('G' . $row, Yii::$app->formatter->asDate($itemModel->start_at))
                ->setCellValue('H' . $row, $itemModel->amount)
                ->setCellValue('I' . $row, Yii::$app->formatter->asDate($itemModel->created_at));
        }

        $phpExcel->getActiveSheet()->removeRow($baseRow - 1, 1);

        // Information

        $filename = 'Customer-transaction-queue_' . \Yii::$app->formatter->asDate(time());

        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');

        ob_end_clean();
        header("Cache-Control: no-cache");
        header("Expires: 0");
        header("Pragma: no-cache");
        header("Content-Type: application/xlsx");
        header("Content-Disposition: attachment; filename={$filename}.xlsx");

        $objWriter->save('php://output');
        AdminLogAction::createLogAction(AdminLogAction::ACTION_EXPORT_EXCEL, []);
        Yii::$app->end();
    }

    /**
     * Finds the CustomerTransactionQueue model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerTransactionQueue the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerTransactionQueue::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionCompletedGh($id)
    {
        $model = $this->findModel($id);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model->status = CustomerTransactionQueue::STATUS_COMPLETED;
            if ($model->save(true, ['status', 'updated_at'])) {
                $request = CustomerRequest::findOne($model->customer_request_id);
                if (!$request) {
                    throw new NotFoundHttpException('The requested page does not exist.');
                }
                if (!$request->checkRequieCompleted()) {
                    throw new Exception('Có giao dịch mapping chưa hoàn thành. Vui lòng chờ các giao dịch đã hoàn thành.');
                }
                $request->status = CustomerRequest::STATUS_COMPLETED;
                if ($request->save(true, ['status', 'updated_at'])) {
                    $transaction->commit();
                    AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_CREATE, $request->attributes);
                    Yii::$app->session->setFlash('success', 'Request GH is completed');

                }
            }
        } catch (\Exception $ex) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', 'Request GH is  not completed');
        }
        return $this->redirect(['index-provider-help']);
    }
}
