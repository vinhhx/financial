<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\forms\InvestRegisterForm;
use app\models\CustomerToken;
use app\models\investments\InvestCustomerRequest;
use app\models\LogAction;
use app\models\logs\AdminLogAction;
use app\models\searchs\InvestCustomerPackageHistorySearch;
use app\modules\admin\forms\ChangePasswordForm;
use app\modules\admin\forms\customers\TransferTokenForm;
use app\modules\admin\forms\tools\ChangeParentCustomer;
use Yii;
use app\models\investments\InvestCustomer;
use app\modules\admin\forms\InvestCustomerSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * InvestCustomerController implements the CRUD actions for InvestCustomer model.
 */
class InvestCustomerController extends AdminBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InvestCustomer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvestCustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InvestCustomer model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new InvestCustomer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($ref = 0)
    {
        $model = new InvestRegisterForm();
        $model->scenario = $model::SCENARIO_ADMIN_REGISTER;
        $listCustomer = null;
        if ($ref) {
            $customerInvited = InvestCustomer::findByRefCode($ref);
            if (!$customerInvited) {
                throw new NotFoundHttpException('Tài khoản giới thiệu không hợp lệ.');
            }
            $model->customer_parent_id = $customerInvited->id;
        } else {
            $listCustomer = InvestCustomer::getAllCustomer(0, InvestCustomer::TYPE_ADMIN);
        }

        if ($model->load(Yii::$app->request->post())) {
            $customer = $model->createCustomer(true);
            $errors = $customer->getErrors();
            if (empty($errors)) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_CREATE, $customer->attributes);
                //Add case
                if ($customer->addInvestCustomerCase()) {
//                    LogAction::logAdmin($customer->id, $customer::tableName(), LogAction::TYPE_USER_CREATE, $customer->getAttributes(['id', 'username', 'full_name', 'sex', 'phone', 'email', 'date_join', 'created_at']));
                } else {
                    //Nếu có lỗi xóa tài khoản đi
                    Yii::$app->session->setFlash('error', 'Không tạo được tài khoản invest');
                    $customer->delete();
                }
                return $this->redirect(['index']);
            } else {
                if (!empty($errors)) {
                    $error = '';
                    foreach ($customer->getErrors() as $val) {
                        $error .= isset($val[0]) ? $val[0] : '';
                    }
                    AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_ERROR, $customer->getErrors());
                    Yii::$app->session->setFlash('error', $error);
                    //print_r($customer->getErrors());
                }
            }

        }
        return $this->render('create', [
            'model' => $model,
            'listCustomer' => $listCustomer
        ]);
    }

    public function actionUpdate($id)
    {
        /**
         * @var $model InvestCustomer
         */
        $model = $this->findModel($id);
        $listCustomer = InvestCustomer::getAllCustomer(0, InvestCustomer::TYPE_ADMIN);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save(true, ["full_name", "dob", "phone", "email", "nation", "identity_card", "updated_at"])) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                return $this->redirect(['index']);
            }
        }
        return $this->render('update', [
            'model' => $model,
            'listCustomer' => $listCustomer
        ]);
    }

    public function actionActive($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->post()) {
            $active = Yii::$app->request->post('active', '');
            if ($active == 'active') {
                if ($model->setActive()) {
                    AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                    Yii::$app->session->setFlash('success', 'Active user "' . $model->username . '" Success.');
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->session->setFlash('error', 'Active user "' . $model->username . '" Error.');
                }
            }
        }
        return $this->render('active', ['model' => $model]);
    }

    public function actionUnactive($id)
    {
        $model = $this->findModel($id);
        if ($model->is_active == InvestCustomer::IS_ACTIVE_COMPLETED) {
            $model->is_active = InvestCustomer::IS_ACTIVE_PENDING;
            if ($model->save(true, ['is_active', 'updated_at'])) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                Yii::$app->session->setFlash('success', 'Unactive "' . $model->username . '" Successfull.');
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Unactive "' . $model->username . '" Error.');
            }
        }
        return false;
    }

    public function actionLockUser($id)
    {
        $model = $this->findModel($id);
        if ($model->status == InvestCustomer::STATUS_ACTIVE) {
            $model->status = InvestCustomer::STATUS_INACTIVE;
            if ($model->save(true, ['status', 'updated_at'])) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                Yii::$app->session->setFlash('success', 'Khóa tài khoản "' . $model->username . '" thành công.');
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Khóa tài khoản "' . $model->username . '" không thành công.');
            }
        }
        if ($model->status == InvestCustomer::STATUS_INACTIVE) {
            $model->status = InvestCustomer::STATUS_ACTIVE;
            if ($model->save(true, ['status', 'updated_at'])) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                Yii::$app->session->setFlash('success', 'Mở khóa tài khoản "' . $model->username . '" thành công.');
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Mở khóa tài khoản "' . $model->username . '" không thành công.');
            }
        }
        return $this->render('active', ['model' => $model]);
    }

    /**
     * Deletes an existing InvestCustomer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
//        $this->findModel($id)->delete();
        $model->is_deleted = $model::IS_DELETED;
        LogAction::logAdmin($model->id, $model::tableName(), LogAction::TYPE_USER_DELETE, $model->getAttributes(["id", "is_deleted", "updated_at"]));
        $model->save(true, ['is_deleted', 'updated_at']);
        AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_DELETE, $model->attributes);
//        InvestCustomer::saveCustomerCaseDelete($model->id, $model->parent_id);
        return $this->redirect(['index']);
    }

    public function actionExportExcel()
    {
        set_time_limit(1000);
        if (!Yii::$app->request->isPost) {
            throw new \yii\web\MethodNotAllowedHttpException('Truy cập không hợp lệ.');
        }

        $searchModel = new InvestCustomerSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $dataProvider->pagination = false;
        $models = $dataProvider->getModels();

        // Load excel template
        $phpExcel = \PHPExcel_IOFactory::load(\Yii::getAlias('@app/web/template/invest-customer-list.xlsx'));

        $phpExcel->setActiveSheetIndex(0);

        // Dòng bắt đầu fill data
        $baseRow = 7;

        $totalRows = count($models);

        /* @var $itemModel InvestCustomerSearch */
        foreach ($models as $n => $itemModel) {
            $row = $baseRow + $n;
            $phpExcel->getActiveSheet()->insertNewRowBefore($row, 1);
            //echo '<pre>';var_dump($itemModel);die;
            $phpExcel->getActiveSheet()
                ->setCellValue('A' . $row, $n + 1)
                ->setCellValue('B' . $row, $itemModel->username)
                ->setCellValue('C' . $row, $itemModel->full_name)
                ->setCellValue('D' . $row, $itemModel->email)
                ->setCellValue('E' . $row, $itemModel->phone)
                ->setCellValue('F' . $row, InvestCustomer::getName($itemModel->parent_id))
                ->setCellValue('G' . $row, Yii::$app->formatter->asDate($itemModel->created_at));
        }

        $phpExcel->getActiveSheet()->removeRow($baseRow - 1, 1);

        // Information
        // total

        $filename = 'Invest_' . \Yii::$app->formatter->asDate(time());

        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');

        ob_end_clean();
        header("Cache-Control: no-cache");
        header("Expires: 0");
        header("Pragma: no-cache");
        header("Content-Type: application/xlsx");
        header("Content-Disposition: attachment; filename={$filename}.xlsx");

        $objWriter->save('php://output');
        AdminLogAction::createLogAction(AdminLogAction::ACTION_EXPORT_EXCEL, []);
        Yii::$app->end();
    }

    /**
     * Finds the InvestCustomer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InvestCustomer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InvestCustomer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChangePassword($id)
    {
        $user = $this->findModel($id);
        $model = new ChangePasswordForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $user->setPassword($model->newPassword);
                $user->generateAuthKey();
                if ($user->save()) {
                    AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $user->attributes);
                    Yii::$app->session->setFlash('success', Yii::t('customers', 'Change password completed'));
                    $this->redirect(['index']);
                }
            }
        }
        return $this->render('change-password', [
            'model' => $model,
            'user' => $user,
        ]);
    }

    public function actionTransferToken($id)
    {
        $model = $this->findModel($id);
        $form = new TransferTokenForm();
        $form->customer_receiver_id = $model->id;
        $listToken = [];
        if ($form->load(Yii::$app->request->post())) {
            $form->module_type = CustomerToken::TYPE_INVEST;
            $result = $form->transfer();

            if (isset($result["status"]) && $result["status"] == "success") {
                $listToken = isset($result["token"]) ? $result["token"] : [];
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, ArrayHelper::merge($model->attributes, $result));

                Yii::$app->session->setFlash('success', ' Đã tạo thành công ' . $form->quantity . ' token cho tài khoản này.');
                $form = new TransferTokenForm();
            } else {
                Yii::$app->session->setFlash('error', ' Lỗi không tạo thành công ' . $form->quantity . ' token cho tài khoản này.');
            }
        }
        return $this->render('transfer-token', [
            'model' => $model,
            'transferForm' => $form,
            'listToken' => $listToken
        ]);
    }

    public function actionUpdateBankAddress($id)
    {
        $customer = $this->findModel($id);
        if (!$customer) {
            throw new NotFoundHttpException('Tài khoản này không tồn tại!');
        }
        $model = $customer->bankAddress;
        if (!$model) {
            throw new NotFoundHttpException('Không tồn tại địa chỉ bitcoin của tài khoản này!');
        }
        $currentAddress = $model->bank_address;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->bank_address == '') {
                $model->addError('bank_address', 'Địa chỉ bitcoin ko đc để trống');
            } elseif ($model->bank_address !== $currentAddress) {
                //cập nhật lại địa chỉ bank
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($model->save(true, ['bank_address', 'updated_at'])) {
                        //Cập nhật các request đang trong trạng thái pending
                        Yii::$app->db->createCommand()->update(
                            InvestCustomerRequest::tableName(),
                            ['bank_address' => $model->bank_address],
                            ['customer_id' => $customer->id, 'type' => [InvestCustomerRequest::TYPE_GET_HELP, InvestCustomerRequest::TYPE_CASH_OUT], 'status' => InvestCustomerRequest::STATUS_CREATED]
                        )->execute();
                        AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_CHANGE_BANK_INVEST, ArrayHelper::merge($model->attributes, ['old_bank_address' => $currentAddress]));
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', 'Cập nhật địa chỉ ví thành công');

                        return $this->refresh();


                    }
                } catch (\Exception $ex) {
                    $transaction->rollBack();
                    $model->bank_address->$currentAddress;
                    Yii::$app->session->setFlash('error', $ex->getMessage());
                }

            }

        }

        return $this->render('banks/update_bank_address',
            [
                'customer' => $customer,
                'model' => $model
            ]
        );
    }

    public function actionProfitHistory()
    {
        $searchModel = new InvestCustomerPackageHistorySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('profits/profit-history', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionEditCase()
    {
        $model = new ChangeParentCustomer();
        $listCustomer = InvestCustomer::getAllCustomer(0);
        if ($model->load(Yii::$app->request->post())) {
            $model->change();
            if (empty($model->getErrors())) {
                Yii::$app->session->setFlash('success', 'Thay đổi case thành công');
                return $this->redirect(['index']);
            }
        }
        return $this->render('cases/edit-case',
            [
                'model' => $model,
                'listCustomer' => $listCustomer
            ]
        );
    }
}
