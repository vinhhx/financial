<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\models\logs\AdminLogAction;
use app\modules\admin\forms\requests\UploadForm;
use Yii;
use app\models\investments\InvestCustomerRequest;
use app\modules\admin\forms\investments\InvestCustomerRequestSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * InvestCustomerRequestController implements the CRUD actions for InvestCustomerRequest model.
 */
class InvestCustomerRequestController extends AdminBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InvestCustomerRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->redirect(['join-index']);
    }

    /**
     * Lists all InvestCustomerRequest models.
     * @return mixed
     */
    public function actionJoinIndex()
    {
        $searchModel = new InvestCustomerRequestSearch();
        $params = Yii::$app->request->queryParams;
        $params["InvestCustomerRequestSearch"]["type"] = [InvestCustomerRequest::TYPE_PROVIDER_HELP, InvestCustomerRequest::TYPE_LOAN_OPEN_PACKAGE];
        $dataProvider = $searchModel->search($params);

        return $this->render('join-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all InvestCustomerRequest models.
     * @return mixed
     */
    public function actionCashOutIndex()
    {
        $searchModel = new InvestCustomerRequestSearch();
        $params = Yii::$app->request->queryParams;
        $params["InvestCustomerRequestSearch"]["type"] = [InvestCustomerRequest::TYPE_CASH_OUT, InvestCustomerRequest::TYPE_GET_HELP];
        $dataProvider = $searchModel->search($params);

        return $this->render('cash-out-index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InvestCustomerRequest model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $modelForm = new UploadForm();
        $modelForm->request_id = $id;
        if ($modelForm->load(Yii::$app->request->post())) {
            $modelForm->file = UploadedFile::getInstance($modelForm, 'file');
            $uploadDir = 'admin/requests/' . Yii::$app->user->id;
            $model->approved_by = Yii::$app->user->identity->username;
            $model->transaction_code = Yii::$app->image->upload(0, $uploadDir, $modelForm->file);
//            $model->upload_attachment_at = time();
            if ($model->transaction_code) {
                $model->status = InvestCustomerRequest::STATUS_COMPLETED;
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    if ($model->save(true, ['transaction_code', 'status', 'updated_at', 'approved_by'])) {
                        $model->finish();
                        AdminLogAction::createLogAction(AdminLogAction::ACTION_CUSTOMER_REQUEST_APPROVED, $model->attributes);
                        $transaction->commit();
                        if ($model->type == InvestCustomerRequest::TYPE_LOAN_OPEN_PACKAGE || $model->type == InvestCustomerRequest::TYPE_PROVIDER_HELP) {
                            return $this->redirect(['join-index']);
                        } else {
                            return $this->redirect(['cash-out-index']);
                        }


                    }
                } catch (\Exception $ex) {
                    $transaction->rollBack();
                    Yii::$app->session->setFlash('error', $ex->getMessage());
                }

            }
        }
        return $this->render('view', [
            'model' => $model,
            'modelForm' => $modelForm
        ]);
    }


    public function actionExportExcel()
    {
        set_time_limit(1000);
        if (!Yii::$app->request->isPost) {
            throw new \yii\web\MethodNotAllowedHttpException('Truy cập không hợp lệ.');
        }

        $searchModel = new InvestCustomerRequestSearch();
        $params = Yii::$app->request->queryParams;
        $type = Yii::$app->request->get('type');
        if ($type == 'cash-out-index') {
            $params["InvestCustomerRequestSearch"]["type"] = [InvestCustomerRequest::TYPE_CASH_OUT, InvestCustomerRequest::TYPE_GET_HELP];
        } elseif ($type == 'join-index') {
            $params["InvestCustomerRequestSearch"]["type"] = [InvestCustomerRequest::TYPE_PROVIDER_HELP, InvestCustomerRequest::TYPE_LOAN_OPEN_PACKAGE];
        }
        $dataProvider = $searchModel->search($params);
        $dataProvider->pagination = false;
        $models = $dataProvider->getModels();

        // Load excel template
        $phpExcel = \PHPExcel_IOFactory::load(\Yii::getAlias('@app/web/template/invest-customer-request.xlsx'));

        $phpExcel->setActiveSheetIndex(0);

        // Dòng bắt đầu fill data
        $baseRow = 7;

        $totalRows = count($models);

        /* @var $itemModel InvestCustomerRequest */
        foreach ($models as $n => $itemModel) {
            $row = $baseRow + $n;
            $phpExcel->getActiveSheet()->insertNewRowBefore($row, 1);
            //echo '<pre>';var_dump($itemModel);die;
            $phpExcel->getActiveSheet()
                ->setCellValue('A' . $row, $n + 1)
                ->setCellValue('B' . $row, $itemModel->customer_username)
                ->setCellValue('C' . $row, $itemModel->getTypeLabel())
                ->setCellValue('D' . $row, $itemModel->amount)
                ->setCellValue('E' . $row, $itemModel->bank_address)
                ->setCellValue('F' . $row, Yii::$app->formatter->asDate($itemModel->created_at))
                ->setCellValue('G' . $row, $itemModel->getStatusLabel());
        }

        $phpExcel->getActiveSheet()->removeRow($baseRow - 1, 1);

        // Information
        // total

        $filename = 'Invest-Customer-Request-' . \Yii::$app->formatter->asDate(time());

        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');

        ob_end_clean();
        header("Cache-Control: no-cache");
        header("Expires: 0");
        header("Pragma: no-cache");
        header("Content-Type: application/xlsx");
        header("Content-Disposition: attachment; filename={$filename}.xlsx");

        $objWriter->save('php://output');
        AdminLogAction::createLogAction(AdminLogAction::ACTION_EXPORT_EXCEL, []);
        Yii::$app->end();
    }


    /**
     * Finds the InvestCustomerRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InvestCustomerRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InvestCustomerRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionApproved($id)
    {
        $model = $this->findModel($id);
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model->finish();

            AdminLogAction::createLogAction(AdminLogAction::ACTION_CUSTOMER_REQUEST_APPROVED, $model->attributes);

            $transaction->commit();

            Yii::$app->session->setFlash('success', 'Approved request completed');

            return $this->redirect(['index']);

        } catch (\Exception $ex) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('error', $ex->getMessage());
        } finally {


        }

        return $this->redirect(['view', 'id' => $id]);

    }
}
