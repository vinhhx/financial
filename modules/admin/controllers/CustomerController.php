<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\forms\RegisterForm;
use app\models\CustomerBankAddress;
use app\models\CustomerCase;
use app\models\CustomerToken;
use app\models\LogAction;
use app\models\logs\AdminLogAction;
use app\models\Package;
use app\models\SystemEmailQueue;
use app\modules\admin\forms\AddMoneyForm;
use app\modules\admin\forms\AddTokenForm;
use app\modules\admin\forms\CashOutForm;
use app\modules\admin\forms\ChangePasswordForm;
use app\modules\admin\forms\CustomerLoan;
use app\modules\admin\forms\CustomerLoanForm;
use app\modules\admin\forms\customers\TransferTokenForm;
use app\modules\customer\forms\search\CustomerBankAddressSearch;
use Yii;
use app\models\Customer;
use app\modules\admin\forms\search\CustomerSearch;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\CustomerPackage;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomerController extends AdminBaseController
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $allPackage = Package::findAll(['status' => Package::STATUS_ACTIVE]);
        $packages = ArrayHelper::map($allPackage, 'id', 'package_name');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'packages' => $packages
        ]);
    }

    public function actionRequirePh($id)
    {
        $model = $this->findModel($id);
        $packages = $model->packages;
        $package = isset($packages[0]) ? $packages[0] : null;
        /*
         *  @var $package CustomerPackage
         */
        if (Yii::$app->request->post() && $package) {
            $required = Yii::$app->request->post('require', '');
            if ($required == "PH") {
                $transaction = Yii::$app->db->beginTransaction();
                try {
                    $package->reinvestment_date = time();
                    $package->is_reinvestment = CustomerPackage::PACKAGE_IS_REINVESTMENT;
                    $package->package_amount = $package->package_amount_root;
                    $package->status = CustomerPackage::STATUS_REQUEST_IN_STEP_ONE;
                    if ($package->save(true, ['status', 'package_amount', 'reinvestment_date', 'is_reinvestment', 'updated_at'])) {
                        $model->is_invest = Customer::INVESTED;
                        if ($model->save(true, ['is_invest', 'updated_at'])) {
                            //Tạo gói đầu tư rồi giờ tạo request
                            $result = $package->requestOpenPackage();
                            if ($result["status"] == "success") {
                                //sent email
                                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_CREATE, $model->attributes);
                                $transaction->commit();
                                $params = $package->getAttributes(['id', 'package_name', 'package_amount', 'increase_amount']);
                                $params["username"] = $model->username;
                                SystemEmailQueue::createEmailQueue(
                                    SystemEmailQueue::TYPE_CUSTOMER_RE_OPEN_PACKAGE,
                                    $model->email,
                                    'You receiver 1 request completed ' . $package->package_name . ' at' . Yii::$app->formatter->asDatetime(time()),
                                    're-open-package-completed',
                                    $params
                                );

                                Yii::$app->session->setFlash('success', 'Request member GH to system');
                                return $this->redirect(['customer-transaction-queue/index']);
                            } else {
                                Yii::$app->session->setFlash('error', 'Request member GH to system error');

                            }
                        }

                    }
                } catch (\Exception $ex) {
                    $transaction->rollBack();
                    AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_ERROR, $model->attributes);
                    Yii::$app->session->setFlash('error', 'Request member GH to system error');
                }


            }
        }
        return $this->render('require-ph', [
            'model' => $model,
            'package' => $package
        ]);
    }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($ref = null)
    {
        $model = new RegisterForm();
        $model->scenario = $model::SCENARIO_ADMIN_REGISTER;
        $listCustomer = null;
        if ($ref) {
            $customerInvited = Customer::findByRefCode($ref);
            if (!$customerInvited) {
                throw new NotFoundHttpException('Tài khoản giới thiệu không hợp lệ.');
            }
            $model->customer_parent_id = $customerInvited->id;
        } else {
            $listCustomer = Customer::getAllCustomer(0, Customer::TYPE_ADMIN);
        }

        if ($model->load(Yii::$app->request->post())) {
            $customer = $model->createCustomer(true);
            $errors = $customer->getErrors();
            if (empty($errors)) {
                //Add case
                if ($customer->addCustomerCase()) {
//                    LogAction::logAdmin($customer->id, $customer::tableName(), LogAction::TYPE_USER_CREATE, $customer->getAttributes(['id', 'username', 'full_name', 'sex', 'phone', 'email', 'date_join', 'created_at']));
                    AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_CREATE, $customer->attributes);
                } else {
                    //Nếu có lỗi xóa tài khoản đi
                    $customer->delete();
                }
                return $this->redirect(['index']);
            } else {
                if (!empty($errors)) {
                    print_r($customer->getErrors());
                }
            }

        }
        return $this->render('create', [
            'model' => $model,
            'listCustomer' => $listCustomer
        ]);
    }

    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $old_parent_id = $model->parent_id;
        if ($model->load(Yii::$app->request->post())) {
            $model->parent_id = $old_parent_id;
            if ($model->save(true, ["status", "date_join", "full_name", "sex", "dob", "phone", "email", "nation", "address", "updated_at"])) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                return $this->redirect(['index']);
            }
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if ($model->created_type == Customer::TYPE_ADMIN) {
            $model->is_deleted = $model::IS_DELETED;
            if ($model->save(true, ['is_deleted', 'updated_at'])) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_DELETE, $model->attributes);

                Yii::$app->session->setFlash('success', 'You are deleted user: "' . $model->username . '" successfull.');
            } else {

                Yii::$app->session->setFlash('error', 'You not deleted user: "' . $model->username . '".');
            }
        } else {
            Yii::$app->session->setFlash('error', 'You not deleted user: "' . $model->username . '".');
        }
//        Customer::saveCustomerCaseDelete($model->id, $model->parent_id);
        return $this->redirect(['index']);
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChangePassword($id)
    {
        $user = $this->findModel($id);
        $model = new ChangePasswordForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $user->setPassword($model->newPassword);
                $user->generateAuthKey();
                if ($user->save()) {
                    AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                    Yii::$app->session->setFlash('success', Yii::t('customers', 'Change password completed'));
                    $this->redirect(['index']);
                }
            }
        }
        return $this->render('change-password', [
            'model' => $model,
            'user' => $user,
        ]);
    }

    public function actionAjaxloadcustomertree()
    {
        $id = (int)Yii::$app->request->post('id');
        $type = (int)Yii::$app->request->post('type', 0);
        if ($type == 1) {
            $id = isset(Customer::getParents($id, 3)['id']) ? Customer::getParents($id, 3)['id'] : 0;
        }
        $reVal = [];
        $reVal['action'] = false;
//        if ($id) {
        $isSystemAdmin = Yii::$app->authManager->getAssignment(RBAC_FULL, Yii::$app->user->id);
        if (!$isSystemAdmin) {
            $filterAdmin = Customer::ID_ONLY_VIEW;
        } else {
            $filterAdmin = 0;
        }
        $model = Customer::showTree($id, 0, true, $filterAdmin);
        $reVal['action'] = true;
        $reVal['html'] = $model;
        $reVal['id'] = $id;
        $level = 0;
        if ($id > 0) {
            $level = (new Query())->select('MAX(level)')->from(CustomerCase::tableName())->where(['customer_id' => $id])->scalar();
        }
        $reVal['currentLv'] = (int)$level;

//        }
        echo json_encode($reVal);
        Yii::$app->end();
    }

    public function actionShowTreeCustomer()
    {
        $isSystemAdmin = Yii::$app->authManager->getAssignment(RBAC_FULL, Yii::$app->user->id);
        if (!$isSystemAdmin) {
            $filterAdmin = Customer::ID_ONLY_VIEW;
        } else {
            $filterAdmin = 0;
        }
        $model = Customer::showTree(0, 4, true, $filterAdmin);
        return $this->render('tree', ['model' => $model]);
    }

    public function actionExport()
    {
        ini_set('max_execution_time', 600);
        $this->layout = false;
        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, false);

        return $this->renderPartial('export', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionActive($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->post()) {
            $active = Yii::$app->request->post('active', '');
            if ($active == 'active') {
                if ($model->setActive()) {
                    AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                    Yii::$app->session->setFlash('success', 'Kích hoạt tài khoản "' . $model->username . '" thành công.');
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->session->setFlash('error', 'Kích hoạt tài khoản "' . $model->username . '" không thành công.');
                }
            }
        }
        return $this->render('active', ['model' => $model]);
    }

    public function actionUnactive($id)
    {
        $model = $this->findModel($id);
        if ($model->is_active == Customer::IS_ACTIVE_COMPLETED) {
            $model->is_active = Customer::IS_ACTIVE_PENDING;
            if ($model->save(true, ['is_active', 'updated_at'])) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                Yii::$app->session->setFlash('success', 'Unactive "' . $model->username . '" Successfull.');
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Unactive "' . $model->username . '" Error.');
            }
        }
        return false;
    }

    public function actionLockUser($id)
    {
        $model = $this->findModel($id);
        if ($model->status == Customer::STATUS_ACTIVE) {
            $model->status = Customer::STATUS_INACTIVE;
            if ($model->save(true, ['status', 'updated_at'])) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                Yii::$app->session->setFlash('success', 'Khóa tài khoản "' . $model->username . '" thành công.');
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Khóa tài khoản "' . $model->username . '" không thành công.');
            }
        }
        if ($model->status == Customer::STATUS_INACTIVE) {
            $model->status = Customer::STATUS_ACTIVE;
            if ($model->save(true, ['status', 'updated_at'])) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                Yii::$app->session->setFlash('success', 'Mở khóa tài khoản "' . $model->username . '" thành công.');
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Mở khóa tài khoản "' . $model->username . '" không thành công.');
            }
        }
        return $this->render('active', ['model' => $model]);
    }


    public function actionBuyToken($id)
    {
        $model = $this->findModel($id);
        $buyTokenForm = new AddTokenForm();
        $buyTokenForm->customer_id = $model->id;
        $listToken = [];
        if ($buyTokenForm->load(Yii::$app->request->post())) {
            $result = $buyTokenForm->addToken();
            if (isset($result["status"]) && $result["status"] == "success") {
                $listToken = $result["token"];
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_CREATE, ArrayHelper::merge($model->attributes, $listToken));
                Yii::$app->session->setFlash('success', ' Đã tạo thành công ' . $buyTokenForm->quantity . ' token cho tài khoản này.');
                $buyTokenForm = new AddTokenForm();
            } else {
                Yii::$app->session->setFlash('error', ' Lỗi không tạo thành công ' . $buyTokenForm->quantity . ' token cho tài khoản này.');
            }
        }
        return $this->render('add-token', [
            'model' => $model,
            'buyTokenForm' => $buyTokenForm,
            'listToken' => $listToken
        ]);
    }

    public function actionTransferToken($id)
    {
        $model = $this->findModel($id);
        $form = new TransferTokenForm();
        $form->customer_receiver_id = $model->id;
        $listToken = [];
        if ($form->load(Yii::$app->request->post())) {
            $form->module_type = CustomerToken::TYPE_CUSTOMER;
            $result = $form->transfer();

            if (isset($result["status"]) && $result["status"] == "success") {
                $listToken = isset($result["token"]) ? $result["token"] : [];
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_CREATE, ArrayHelper::merge($form->attributes, $result));
                Yii::$app->session->setFlash('success', ' Đã tạo thành công ' . $form->quantity . ' token cho tài khoản này.');
                $form = new TransferTokenForm();
            } else {
                Yii::$app->session->setFlash('error', ' Lỗi không tạo thành công ' . $form->quantity . ' token cho tài khoản này.');
            }
        }
        $customerAdmin = Customer::getAllCustomer(0, Customer::TYPE_ADMIN);
        $customerAdmin = ArrayHelper::map($customerAdmin, 'id', 'fullName');
        return $this->render('transfer-token', [
            'model' => $model,
            'transferForm' => $form,
            'customerAdmin' => $customerAdmin,
            'listToken' => $listToken
        ]);
    }

    public function actionExportExcel()
    {
        set_time_limit(1000);
        if (!Yii::$app->request->isPost) {
            throw new \yii\web\MethodNotAllowedHttpException('Truy cập không hợp lệ.');
        }

        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $dataProvider->pagination = false;
        $models = $dataProvider->getModels();

        // Load excel template
        $phpExcel = \PHPExcel_IOFactory::load(\Yii::getAlias('@app/web/template/customer-list.xlsx'));

        $phpExcel->setActiveSheetIndex(0);

        // Dòng bắt đầu fill data
        $baseRow = 7;

        $totalRows = count($models);

        /* @var $itemModel CustomerSearch */
        foreach ($models as $n => $itemModel) {
            $nameParent = Customer::getName($itemModel->parent_id);
            /*echo '<pre>';
            var_dump($itemModel);
            die;*/
            $row = $baseRow + $n;
            $phpExcel->getActiveSheet()->insertNewRowBefore($row, 1);
            //echo '<pre>';var_dump($itemModel);die;
            $phpExcel->getActiveSheet()
                ->setCellValue('A' . $row, $n + 1)
                ->setCellValue('B' . $row, $itemModel->username)
                ->setCellValue('C' . $row, $itemModel->full_name)
                ->setCellValue('D' . $row, ($itemModel->getTotalChildren()))
                ->setCellValue('E' . $row, $itemModel->email)
                ->setCellValue('F' . $row, $itemModel->phone)
                ->setCellValue('G' . $row, isset($nameParent) ? $nameParent : '--')
                ->setCellValue('H' . $row, Yii::$app->formatter->asDate($itemModel->date_join));
        }

        //$phpExcel->getActiveSheet()->removeRow($baseRow - 1, 1);

        // Information
        $filename = 'Customer_' . \Yii::$app->formatter->asDate(time());

        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');
        ob_end_clean();
        header("Cache-Control: no-cache");
        header("Expires: 0");
        header("Pragma: no-cache");
        header("Content-Type: application/xlsx");
        header("Content-Disposition: attachment; filename={$filename}.xlsx");

        $objWriter->save('php://output');
        AdminLogAction::createLogAction(AdminLogAction::ACTION_EXPORT_EXCEL, []);
        Yii::$app->end();
    }

    public function actionCustomerWallet($id)
    {
        $model = $this->findModel($id);
        $bankAddress = new CustomerBankAddressSearch();
        $dataProvider = $bankAddress->searchAdmin([
            "{$bankAddress->formName()}" => [
                'customer_id' => $id
            ]
        ]);
        return $this->render('banks', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionUpdateBankAddress($id, $cusId)
    {
        $model = CustomerBankAddress::find()->where([
            'id' => $id,
            'customer_id' => $cusId
        ])->limit(1)->one();
        if (!$model) {
            throw new NotFoundHttpException('Customer and bank address not exactly');
        }
        $currentBank = $model->bank_address;
        if (Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
            if ($currentBank != $model->bank_address) {
                if ($model->save(true, ['bank_address', 'updated_at'])) {
                    $customer = Customer::findOne($model->customer_id);
                    if ($customer) {
                        $customer->updateBankAddressToTransaction($model->bank_address);
                    }
                    Yii::$app->session->setFlash('success', 'Cập nhật địa chỉ ví mới của người chơi thành công.');
                    return $this->redirect(['customer-wallet', 'id' => $model->customer_id]);
                } else {
                    Yii::$app->session->setFlash('error', 'Không cập nhật được địa chỉ  ví người chơi.');
                }
            } else {
                Yii::$app->session->setFlash('warning', 'Địa chỉ ví cũ và mới phải có sự khác nhau.');
            }

        }
        return $this->render('update-bank-address', [
            'model' => $model
        ]);
    }


}
