<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\models\logs\AdminLogAction;
use Yii;
use app\models\permissions\AuthItem;
use yii\base\Exception;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PermissionController implements the CRUD actions for AuthItem model.
 */
class PermissionController extends AdminBaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthItem models
     * @param null|string $item role name
     * @return mixed
     */
    public function actionIndex($item = null)
    {
        $auth = Yii::$app->authManager;

        $roles = $auth->getRoles();
        unset($roles[RBAC_FULL]);

        $roleItems = [];
        foreach ($roles as $role) {
            $roleItems[$role->name] = $role->name;
        }

        $model = $this->findModel($item);

        $children = $auth->getChildren($model->name);
        $selected = array_keys($children);

        if (Yii::$app->request->isPost && ($data = Yii::$app->request->post('data')) !== null) {
            $data = AuthItem::filter($data);

            if (!$model->created_at && $model->load(Yii::$app->request->post())) {
                $name = AuthItem::rewrite($model->name);
                $role = $auth->createRole($name);
                $role->description = $model->description;
                if($auth->add($role)){
                    AdminLogAction::createLogAction(AdminLogAction::ACTION_PERMISSION_CREATE, $model->attributes);

                }
            } else {
                $role = $auth->getRole($model->name);
            }
            if ($role) {
                $auth->removeChildren($role);
                foreach ($data as $n) {
                    $child = $auth->getPermission($n);
                    $auth->addChild($role, $child);
                }
                Yii::$app->session->setFlash('success', 'Cập nhật thành công.');
                return $this->redirect(['index', 'item' => $role->name]);
            }
        }

        return $this->render('index', [
            'model' => $model,
            'items' => AuthItem::getItems(),
            'roles' => $roleItems,
            'selected' => $selected,
        ]);
    }

    public function actionUpdate()
    {
        if (!Yii::$app->request->isAjax) {
            throw new MethodNotAllowedHttpException('Method is not allowed.');
        }
        if (Yii::$app->request->isPost) {
            try {
                AuthItem::getAdminActions();

                // Delete Cache
                Yii::$app->cache->delete('rbac');

                $this->setFlash('success', 'Cập nhật thành công.');
            } catch (Exception $ex) {
                echo 'Caught exception: ', $ex->getMessage(), "\n";
            }
        }
        return true;
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $item
     * @return AuthItem the loaded model
     */
    protected function findModel($item)
    {
        if ($item === RBAC_FULL) {
            return new AuthItem();
        }

        $auth = Yii::$app->authManager;

        $role = $auth->getRole($item);

        if ($role) {
            return new AuthItem([
                'name' => $role->name,
                'type' => $role->type,
                'description' => $role->description,
                'rule_name' => $role->ruleName,
                'data' => $role->data,
                'created_at' => $role->createdAt,
                'updated_at' => $role->updatedAt,
            ]);
        }

        return new AuthItem();
    }

    public function actionRenderAction()
    {
        try {
            AuthItem::getAdminActions();
        } catch (Exception $ex) {
            echo 'Caught exception: ', $ex->getMessage(), "\n";
        }
    }

}
