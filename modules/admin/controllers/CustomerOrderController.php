<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\models\Customer;
use app\models\CustomerToken;
use app\models\investments\InvestCustomer;
use app\models\logs\AdminLogAction;
use app\models\SystemEmailQueue;
use app\modules\admin\forms\AddTokenForm;
use Yii;
use app\models\CustomerOrder;
use app\models\searchs\CustomerOrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CustomerOrderController implements the CRUD actions for CustomerOrder model.
 */
class CustomerOrderController extends AdminBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'approved' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomerOrder model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }


    public function actionExportExcel()
    {
        set_time_limit(1000);
        if (!Yii::$app->request->isPost) {
            throw new \yii\web\MethodNotAllowedHttpException('Truy cập không hợp lệ.');
        }

        $searchModel = new CustomerOrderSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $dataProvider->pagination = false;
        $models = $dataProvider->getModels();

        // Load excel template
        $phpExcel = \PHPExcel_IOFactory::load(\Yii::getAlias('@app/web/template/customer-order.xlsx'));

        $phpExcel->setActiveSheetIndex(0);

        // Dòng bắt đầu fill data
        $baseRow = 7;

        $totalRows = count($models);

        /* @var $itemModel CustomerOrderSearch */
        foreach ($models as $n => $itemModel) {
            $row = $baseRow + $n;
            $phpExcel->getActiveSheet()->insertNewRowBefore($row, 1);
            //echo '<pre>';var_dump($itemModel);die;
            $phpExcel->getActiveSheet()
                ->setCellValue('A' . $row, $n + 1)
                ->setCellValue('B' . $row, $itemModel->customer_username)
                ->setCellValue('C' . $row, $itemModel->id)
                ->setCellValue('D' . $row, $itemModel->getTypeLabel())
                ->setCellValue('E' . $row, $itemModel->getTypeModuleLabel())
                ->setCellValue('F' . $row, $itemModel->quantity)
                ->setCellValue('G' . $row, Yii::$app->formatter->asDate($itemModel->created_at))
                ->setCellValue('H' . $row, (isset($itemModel->upload_attachment_at) && $itemModel->upload_attachment_at > 0) ? Yii::$app->formatter->asDate($itemModel->upload_attachment_at) : '--')
                ->setCellValue('I' . $row, $itemModel->getStatusLabel());
        }

        $phpExcel->getActiveSheet()->removeRow($baseRow - 1, 1);

        // Information

        $filename = 'Customer-Order-' . \Yii::$app->formatter->asDate(time());

        $objWriter = \PHPExcel_IOFactory::createWriter($phpExcel, 'Excel2007');

        ob_end_clean();
        header("Cache-Control: no-cache");
        header("Expires: 0");
        header("Pragma: no-cache");
        header("Content-Type: application/xlsx");
        header("Content-Disposition: attachment; filename={$filename}.xlsx");

        $objWriter->save('php://output');

        AdminLogAction::createLogAction(AdminLogAction::ACTION_EXPORT_EXCEL, []);
        Yii::$app->end();
    }


    /**
     * Finds the CustomerOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionApproved($id)
    {
        $model = $this->findModel($id);
        $form = new AddTokenForm();
        $form->customer_id = $model->customer_id;
        $form->quantity = $model->quantity;
        $customer = null;
        if ($model->type_module == CustomerOrder::TYPE_MODULE_INVEST) {
            $form->type = CustomerToken::TYPE_INVEST;
            $customer = InvestCustomer::findOne($model->customer_id);
        } elseif ($model->type_module == CustomerOrder::TYPE_MODULE_CUSTOMER) {
            $form->type = CustomerToken::TYPE_CUSTOMER;
            $customer = Customer::findOne($model->customer_id);
        }
        $result = $form->addToken();
        if ($result["status"] == "success" && $customer) {
            $model->approved_at = time();
            $model->approved_by = Yii::$app->user->identity->id;
            $model->status = CustomerOrder::STATUS_COMPLETED;
            if ($model->save(true, ['approved_at', 'approved_by', 'status', 'updated_at'])) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $model->attributes);
                SystemEmailQueue::createEmailQueue(
                    SystemEmailQueue::TYPE_CUSTOMER_BUY_TICKET,
                    $customer->email,
                    'You received ' . $model->quantity . ' token. - ' . date('Y-m-d H:i:s'),
                    'receive-token',
                    [
                        'username' => $customer->username,
                        'quantity' => $model->quantity,
                        'tokens' => $result["token"]
                    ]
                );
                Yii::$app->session->setFlash('success', 'Add token for customer bought completed');
                return $this->redirect(['index']);
            } else {
                Yii::$app->session->setFlash('error', 'Not save customer order token');
            }
        } else {

            Yii::$app->session->setFlash('error', $result["message"]);

        }
        return $this->redirect(['view', 'id' => $model->id]);
    }
}
