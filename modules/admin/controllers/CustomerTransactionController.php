<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\models\Customer;
use app\models\logs\AdminLogAction;
use app\models\SystemEmailQueue;
use app\modules\admin\forms\transactions\CustomerActionForm;
use Yii;
use app\models\CustomerTransaction;
use app\modules\admin\forms\search\CustomerTransactionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * CustomerTransactionController implements the CRUD actions for CustomerTransaction model.
 */
class CustomerTransactionController extends AdminBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerTransaction models.
     * @return mixed
     */
    public function actionIndex()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = 'json';
            $type = Yii::$app->request->post('type', '');
            $id = Yii::$app->request->post('tid', 0);
            $customerTransaction = ($id) ? CustomerTransaction::findOne($id) : null;
            switch ($type) {
                case"sent": //admin chuyển tiền
                    if ($customerTransaction) {
                        $customerSentId = $customerTransaction->customer_sent_id;
                        $customerSent = Customer::findOne(['id' => $customerSentId, 'created_type' => Customer::TYPE_ADMIN]);
                        if ($customerSent) {
                            $modelForm = new CustomerActionForm();
                            $modelForm->type = $type;
                            $modelForm->customer_id = $customerSent->id;
                            $modelForm->transaction_id = $customerTransaction->id;
                            $ajaxHtml = $this->renderAjax('ajax/_ajax_view_transaction', ['model' => $customerTransaction, 'modelForm' => $modelForm, 'customer' => $customerSent]);
                            AdminLogAction::createLogAction(AdminLogAction::ACTION_CUSTOMER_TRANSACTION_SENT, $modelForm->attributes);

                            return $ajaxHtml;
                        }

                    }
                    return;
                    break;
                case"remove-attachment": //admin hủy chuyển tiền
                    if ($customerTransaction) {
                        $result = $customerTransaction->adminRemoveAttachment();
                        AdminLogAction::createLogAction(AdminLogAction::ACTION_CUSTOMER_TRANSACTION_REMOVE_ATTACHMENT, $result);
                        return $result;
                    }
                    return false;
                    break;
                case"receive":  //admin nhận tiền
                    if ($customerTransaction) {
                        $customerReceiverId = $customerTransaction->customer_receiver_id;
                        $customerReceiver = Customer::findOne(['id' => $customerReceiverId, 'created_type' => Customer::TYPE_ADMIN]);
                        if ($customerReceiver) {
                            $modelForm = new CustomerActionForm();
                            $modelForm->type = $type;
                            $modelForm->customer_id = $customerReceiver->id;
                            $modelForm->transaction_id = $customerTransaction->id;
                            $ajaxHtml = $this->renderAjax('ajax/_ajax_view_transaction', ['model' => $customerTransaction, 'modelForm' => $modelForm, 'customer' => $customerReceiver]);
                            AdminLogAction::createLogAction(AdminLogAction::ACTION_CUSTOMER_TRANSACTION_RECEIVE, $modelForm->attributes);
                            return $ajaxHtml;
                        }
                    }
                    return;
                    break;
                case 'upload':
                    $modelForm = new CustomerActionForm();
                    if ($modelForm->load(Yii::$app->request->post())) {
                        $query = CustomerTransaction::find();
                        $query->where(['id' => $modelForm->transaction_id]);
//                        $query->andFilterWhere([
//                            'or',
//                            ['customer_sent_id' => Yii::$app->user->identity->id],
//                            ['customer_receiver_id' => Yii::$app->user->identity->id]
//                        ]);
                        $query->andWhere(['customer_sent_id' => $modelForm->customer_id]);
                        $query->limit(1);
                        $customerTransaction = $query->one();
                        $result = [
                            "status" => "error",
                            "message" => ""
                        ];
                        if ($customerTransaction) {
                            $modelForm->file = UploadedFile::getInstance($modelForm, 'file');
                            if($modelForm->validate('file')){
                                $uploadDir = 'customer/' . $modelForm->customer_id;
                                $customerTransaction->attachment = Yii::$app->image->upload(0, $uploadDir, $modelForm->file);
                                if ($customerTransaction->attachment) {
                                    $customerTransaction->status = CustomerTransaction::STATUS_SEND_COMPLETED;
                                    if ($customerTransaction->save(true, ['attachment', 'status', 'updated_at'])) {
                                        AdminLogAction::createLogAction(AdminLogAction::ACTION_CUSTOMER_TRANSACTION_UPLOAD_ATTACHMENT, $customerTransaction->attributes);
                                        $params = [];
                                        $params["username"] = Yii::$app->user->identity->username;
                                        $params["amount"] = $customerTransaction->amount;
                                        $params["image"] = $customerTransaction->attachment;
                                        SystemEmailQueue::createEmailQueue(
                                            SystemEmailQueue::TYPE_CUSTOMER_WITHDRAW_POUNDAGE,
                                            Yii::$app->user->identity->email,
                                            'Your request withdraw is completed - ' . date('Y-m-d H:i:s'),
                                            'withdraw-completed',
                                            $params
                                        );
                                        $result["status"] = "success";
                                        $result["content"] = $this->renderAjax('ajax/messages/_add_attachment_completed');
                                    }
                                }else{
                                    $result["status"] = "error";
                                    $result["content"] = $this->renderAjax('ajax/messages/_add_attachment_completed');
                                }
                            }

                        }
                        return $result;
                    }
                    break;
                case 'approved':
                    if ($customerTransaction) {
                        $customerReceiverId = $customerTransaction->customer_receiver_id;
                        $customerReceiver = Customer::findOne(['id' => $customerReceiverId, 'created_type' => Customer::TYPE_ADMIN]);
                        if ($customerReceiver) {
                            if ($customerTransaction->status == CustomerTransaction::STATUS_SEND_COMPLETED) {
                                $message = $customerTransaction->finish(true);
                                AdminLogAction::createLogAction(AdminLogAction::ACTION_CUSTOMER_TRANSACTION_APPROVED, $customerTransaction->attributes);
                                if ($message) {
                                    $ajaxHtml = $this->renderAjax('ajax/messages/_approved_transaction_completed');
                                } else {
                                    $ajaxHtml = $this->renderAjax('ajax/messages/_approved_transaction_fail');

                                }
                                return $ajaxHtml;
                            }
                        }
                    }
                    return;
                default:
                    break;
            }
        }
        $searchModel = new CustomerTransactionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomerTransaction model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }



    /**
     * Finds the CustomerTransaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerTransaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerTransaction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
