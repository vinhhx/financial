<?php

namespace app\modules\admin\controllers;

use app\components\controllers\AdminBaseController;
use app\models\Customer;
use app\models\CustomerRequireCashout;
use app\modules\admin\forms\search\CustomerRequireCashoutSearch;
use app\modules\admin\forms\search\CustomerSearch;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\Query;

class StatisticController extends AdminBaseController
{

    public function actionStatisticCustomerJoin()
    {
        $modelSearch = new CustomerSearch();
        $data = $modelSearch->statisticCutomerJoin(Yii::$app->request->queryParams);
        return $this->render('statistic-customer-register', [
            'data' => $data,
            'modelSearch' => $modelSearch
        ]);
    }

    public function actionStatisticTotalCashout()
    {
        $query = (new Query());
        $query->select(['c.id', 'c.username', 'c.full_name', 'c.package_id', 'c.amount', 'c.status', 'c.date_join', 'c.last_auto_increase', 'COUNT(crs.id) as total_required', 'SUM(crs.amount) as total_amount']);
        $query->from(Customer::tableName() . ' c');
        $query->leftJoin(CustomerRequireCashout::tableName() . ' crs', 'crs.customer_id= c.id AND crs.status=' . CustomerRequireCashout::STATUS_PENDING);

        $query->where(['!=', 'c.is_deleted', Customer::IS_DELETED]);
        $isSystemAdmin = Yii::$app->authManager->getAssignment(RBAC_FULL, Yii::$app->user->id);
        if (!$isSystemAdmin) {
            $query->andWhere(['c.user_id_first_branch' => Customer::ID_ONLY_VIEW]);
        }
        $query->groupBy(['c.id']);
        $dataTotal = (new Query())
            ->select(['COUNT(id) as total_customer', 'SUM(total_required) as total_required', 'SUM(total_amount) as total_amount'])
            ->from(['tbl'=>$query])
            ->one();
        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
//            'sort' => [
//                'defaultOrder' => [
//                    'c.username' => SORT_ASC,
//                ]
//            ],
            'pagination' => [
                'pageSize' => 30
            ]

        ]);


        return $this->render('statistic-customer-cashout', [
            'dataProvider' => $dataProvider,
            'dataTotal' => $dataTotal
        ]);
    }

    public function  actionStatisticCustomerRequest()
    {
        $modelSearch = new CustomerRequireCashoutSearch();
        $dataProvider = $modelSearch->statisticCutomerPending(Yii::$app->request->queryParams);
        $dataTotal = $modelSearch->getCustomerRequestCashOut();
        return $this->render('statistic-require-cashout', [
            'dataProvider' => $dataProvider,
            'modelSearch' => $modelSearch,
            'dataTotal' => $dataTotal
        ]);
    }

}
