<?php

/**
 * Created by PhpStorm.
 * User: Quan
 * Date: 8/27/2015
 * Time: 2:22 PM
 */

namespace app\modules\admin\widgets;

use Yii;
use yii\base\Widget;
use yii\db\mysql\Schema;
use yii\helpers\Url;

class MenuWidget extends Widget
{

    public function run()
    {

        $config = [
            [
                'label' => '<i class="fa fa-dashboard"></i>&nbsp;' . Yii::t('app', 'Home'),
                'url' => ['default/index'],
            ],
            [
                'label' => '<i class="fa fa-users"></i>&nbsp;' . Yii::t('app', 'Customers'),
                'url' => null,
                'children' => [
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Customer List'),
                        'url' => ['customer/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Customer Orders'),
                        'url' => ['customer-order/index'],
                    ],

                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Customer Message'),
                        'url' => ['note/listing-note'],
                    ],

                ]
            ],
            [
                'label' => '<i class="fa fa-exchange"></i>&nbsp;' . Yii::t('app', 'Transactions'),
                'children' => [
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Customer waiting join '),
                        'url' => ['customer-transaction-queue/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Customer waiting cash out '),
                        'url' => ['customer-transaction-queue/index-provider-help'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Transaction mapping'),
                        'url' => ['customer-transaction/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Customer Package Request'),
                        'url' => ['customer-package/index'],
                    ],


                ]
            ],
              [
                'label' => '<i class="fa fa-balance-scale"></i>&nbsp;' . Yii::t('app', 'Commission'),
                'children' => [
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Customer Commission'),
                        'url' => ['poundage-transaction/group-commission'],
                    ],
[
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Compensate Require'),
                        'url' => ['poundage-transaction/compensate'],
                    ],

                ]
            ],

            [
                'label' => '<i class="fa fa-database"></i>&nbsp;' . Yii::t('app', 'Investment'),
                'children' => [
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Customer\'s Investor '),
                        'url' => ['invest-customer/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Customer request join '),
                        'url' => ['invest-customer-request/join-index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Customer request cashout'),
                        'url' => ['invest-customer-request/cash-out-index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Everyday profit history'),
                        'url' => ['invest-customer/profit-history'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Edit Invest customer case'),
                        'url' => ['invest-customer/edit-case'],
                    ],
//                    [
//                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Customer transactions'),
//                        'url' => ['invest-customer-transaction/cash-out-index'],
//                    ],

                ]
            ],

//            [
//                'label' => '<i class="fa fa-pie-chart"></i>&nbsp;' . Yii::t('app', 'Statistics'),
//                'children' => [
//                    [
//                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Statistic new customer'),
//                        'url' => ['statistic/statistic-customer-join'],
//                    ],
//                    [
//                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Statistic Total Cashout'),
//                        'url' => ['statistic/statistic-total-cashout'],
//                    ],
//                    [
//                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Statistic request Cashout'),
//                        'url' => ['statistic/statistic-customer-request'],
//                    ],
////                    [
////                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;'.Yii::t('app','Paid'),
////                        'url' => ['statistic/paid'],
////                    ],
//
//
//                ]
//            ],
//            [
//                'label' => '<i class="fa fa-comments-o"></i>&nbsp;'.Yii::t('app','Complain'),
//                'children' => [
//                    [
//                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;'.Yii::t('app','Complain List'),
//                        'url' => ['complain/index'],
//                    ],
//                ],
//            ],

            [
                'label' => '<i class="fa fa-th-large"></i>&nbsp;' . Yii::t('app', 'Website'),
                'children' => [
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'News'),
                        'url' => ['news/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Category News'),
                        'url' => ['news-category/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Video'),
                        'url' => ['video/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Content'),
                        'url' => ['content/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Banner'),
                        'url' => ['banner/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Config'),
                        'url' => ['config/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Menu'),
                        'url' => ['menu/index'],
                    ],
                ]
            ],
            [
                'label' => '<i class="fa fa-cogs"></i>&nbsp;' . Yii::t('app', 'System'),
                'children' => [
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Users'),
                        'url' => ['user/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Permission'),
                        'url' => ['permission/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Package'),
                        'url' => ['package/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('app', 'Package Rate'),
                        'url' => ['invest-package/index'],
                    ],
//                    [
//                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;',
//                        'url' => ['config/index'],
//                    ],
                ]
            ],
//            [
//                'label' => '<i class="fa fa-wrench"></i>&nbsp;'.Yii::t('app','Version Update *'),
//                'url' => ['tool/version-update'],
//            ],
            [
                'label' => '<i class="fa fa-sign-out"></i>&nbsp;' . Yii::t('customers', 'Log out'),
                'url' => ['default/logout'],
            ],
        ];
        if (!Yii::$app->user->isGuest) {
            if (Yii::$app->user->identity->username == Yii::$app->params['HIDDEN_ADMIN']) {
                $config[] = [
                    'label' => '<i class="fa fa-life-ring"></i>&nbsp;' . Yii::t('customers', 'Logs'),
                    'url' => ['admin-log-action/index'],
                ];
            }
        }

        // Current Module
//        $prefix = Yii::$app->controller->module->uniqueId;

        // current action
        $action = '/' . Yii::$app->controller->action->uniqueId;

        $parentAction = isset($this->getView()->params["parentAction"]) ? $this->getView()->params["parentAction"] : "";

        // Remove wallet tránh lỗi URL
//        if ($prefix == 'admin/') {
//            $prefix = 'admin';
//            $prefix = "/{$prefix}/";
//        } else {
//            $prefix = null;
//        }
        // Remove wallet tránh lỗi URL
        foreach ($config as $k => $cfg) {
            if (isset($cfg['children'])) {
                foreach ($cfg['children'] as $k1 => $child) {

                    if (isset($child['url']) && $child['url']) {
//                        $child['url'][0] = $prefix . $child['url'][0];
                        $prefix = '/admin/' . $child['url'][0];
                        $child['url'] = Url::to($child['url']);
                        // Menu đang được chọn
                        if ($prefix == $action || $prefix == $parentAction) {
                            $cfg['selected'] = true;
                            $child['selected'] = true;
                        }
                        //
                        $cfg['children'][$k1] = $child;
                    }
                }
            }
            if (isset($cfg['url']) && $cfg['url']) {
//                $cfg['url'][0] = $prefix . $cfg['url'][0];
                $cfg['url'] = Url::to($cfg['url']);
                if ($cfg['url'] == $action || $cfg['url'] == $parentAction) {
                    $cfg['selected'] = true;
                }
            }
            $config[$k] = $cfg;
        }

        return $this->render('menu-widget', [
            'currentController' => Yii::$app->controller->id,
            'config' => $config
        ]);
    }

}
