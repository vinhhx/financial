<?php
/**
 * Created by PhpStorm.
 * User: quyet
 * Date: 11/15/2015
 * Time: 8:56 AM
 */

namespace app\modules\admin\widgets;

use app\assets\FroalaEditorAsset;
use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\InputWidget;

class Editor extends InputWidget
{
    public $className;

    public $objectId;

    public $options;

    public function init()
    {
        parent::init();

        FroalaEditorAsset::register($this->view);

        $height = isset($this->options['height']) ? $this->options['height'] : 800;

        $this->view->registerJs("$('#" . $this->options['id'] . "').froalaEditor({
            height: {$height},
            imageManagerScrollOffset:2,
            pastePlain: true,
            toolbarSticky: false,
            key: 'dadddddsadsdas',
            imageUploadURL: '" . Url::to(['froala/upload']) . "',
            imageManagerLoadURL: '" . Url::to(['froala/manager']) . "',
            imageUploadParam: 'MediaForm[file]',
            imageUploadParams: {
                _csrf: $('meta[name=\"csrf-token\"]').attr('content'),
                className: '" . $this->className . "',
                objectId: '" . $this->objectId . "'
            },
            toolbarButtons: [
                'undo', 'redo' , '|',
                'bold', 'italic', 'underline', 'strikethrough', 'subscript', 'superscript', 'outdent', 'indent', 'clearFormatting', 'insertTable', 'html', '|',
                'insertImage', 'insertLink', 'insertVideo', '|',
                'color', 'emoticons', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', 'insertHR'
            ]
        })");
    }

    public function run()
    {
        echo Html::activeTextarea($this->model, $this->attribute, $this->options);
    }
}