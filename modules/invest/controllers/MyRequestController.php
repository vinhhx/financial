<?php

namespace app\modules\invest\controllers;

use app\components\controllers\InvestBaseController;
use app\models\SystemEmailQueue;
use app\modules\invest\forms\requests\UploadForm;
use Yii;
use app\models\investments\InvestCustomerRequest;
use app\modules\invest\forms\InvestCustomerRequestSearch;
use yii\web\Controller;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * MyRequestController implements the CRUD actions for InvestCustomerRequest model.
 */
class MyRequestController extends InvestBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all InvestCustomerRequest models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new InvestCustomerRequestSearch();
        $params = Yii::$app->request->queryParams;
        $params["InvestCustomerRequestSearch"]["customer_id"] = Yii::$app->user->id;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single InvestCustomerRequest model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if ($model->customer_id != Yii::$app->user->identity->id) {
            throw new MethodNotAllowedHttpException(Yii::t('messages', 'Bạn không có quyền truy cập chức năng này'));
        }
        if (Yii::$app->request->isAjax) {
            $type = Yii::$app->request->post('type', '');
            switch ($type) {
                case 'remove-image':
                    $result = $model->removeAttachment();
                    return $result;

                    break;
                default:
                    break;

            }
            return false;
        }
        $modelForm = new UploadForm();
        $modelForm->customer_id = Yii::$app->user->id;
        $modelForm->request_id = $id;
        if ($modelForm->load(Yii::$app->request->post())) {
            $modelForm->file = UploadedFile::getInstance($modelForm, 'file');
            $uploadDir = 'invest/requests/' . Yii::$app->user->id;
            $model->transaction_code = Yii::$app->image->upload(0, $uploadDir, $modelForm->file);
//            $model->upload_attachment_at = time();
            if ($model->transaction_code) {
                $model->status = InvestCustomerRequest::STATUS_SENT;
                if ($model->save(true, ['transaction_code', 'status', 'updated_at'])) {
                    $params = [];
                    $params['username'] = Yii::$app->user->identity->username;
                    $params['image'] = $model->transaction_code;

                    SystemEmailQueue::createEmailQueue(
                        SystemEmailQueue::TYPE_INVEST_ACTIVE_PACKAGE,
                        Yii::$app->user->identity->email,
                        '[INVESTOR] Active package',
                        'investor-active-package',
                        $params
                    );
                    Yii::$app->session->setFlash('success', Yii::t('messages', 'Bạn tải file xác thực thành công! '));
                    return $this->redirect(['index']);
                }
            }
        }
        return $this->render('view', [
            'model' => $model,
            'modelForm' => $modelForm
        ]);
    }

    /**
     * Creates a new InvestCustomerRequest model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new InvestCustomerRequest();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing InvestCustomerRequest model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing InvestCustomerRequest model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the InvestCustomerRequest model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return InvestCustomerRequest the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = InvestCustomerRequest::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
