<?php

namespace app\modules\invest\controllers;

use app\components\controllers\InvestBaseController;
use app\forms\InvestRegisterForm;
use app\models\Customer;
use app\models\investments\InvestCustomerPackage;
use app\models\investments\InvestPackage;
use app\models\LogAction;
use app\models\SystemEmailQueue;
use app\modules\invest\forms\LoginForm;
use app\modules\invest\forms\RegisterForm;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;

/**
 * Default controller for the `invest` module
 */
class DefaultController extends InvestBaseController
{
    /**
     * Renders the index view for the module
     * @return string
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['index', 'login', 'logout'],
                'rules' => [
                    [
                        'actions' => ['index', 'logout', 'error', 'captcha'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login', 'forget-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['register'],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ]
                ]
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        \Yii::$app->view->registerMetaTag([
            'http-equiv' => 'refresh',
            'content' => Yii::$app->params['pageRefresh']
        ]);
        $packageHistories = Yii::$app->user->identity->searchNewPackageHistory(4);
        $packageTransactions = Yii::$app->user->identity->searchNewTransaction(10);
        return $this->render('index', [
            'packageHistories' => $packageHistories,
            'packageTransactions' => $packageTransactions,
        ]);
    }

    public function actionLogin()
    {
        $this->layout = 'login';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
//            return $this->goBack();
            return $this->redirect(['default/index']);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout(false);

        return $this->redirect(Yii::$app->user->loginUrl);
    }

    public function actionRegister($ref = null)
    {
        $this->layout = 'login';
        $model = new RegisterForm();
        $model->ref_code = $ref;
        if (!$ref) {
            $ref = Yii::$app->session->get('invite_code');
            $model->ref_code = $ref;
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->checkRequireCreate()) {
                $customer = $model->createCustomer();
                $errors = $customer->getErrors();
                if (empty($errors)) {
                    //Add case
                    $result = $customer->addInvestCustomerCase();
                    if (isset($result["code"]) && (int)$result["code"] == 200) {
                        LogAction::logUser($customer->id, $customer::tableName(), LogAction::TYPE_USER_CREATE, $customer->getAttributes(['id', 'username', 'full_name', 'sex', 'phone', 'email', 'date_join', 'created_at']));
//                        if($redirect==1){
//                            return $this->redirect(['/invest/'])
//                        }
                        //Gửi Email
                        $params = [
                            'title' => 'Thank you for creating an account with bitoness.com',
                            'username' => $customer->username,

                        ];
                        SystemEmailQueue::createEmailQueue(
                            SystemEmailQueue::TYPE_INVEST_REGISTERED,
                            $customer->email,
                            '[INVESTOR] Register new account',
                            'register-completed',
                            $customer->getAttributes(['id', 'username', 'ref_code', 'email', 'phone', 'full_name'])
                        );
                        Yii::$app->session->removeAllFlashes();
                        Yii::$app->session->setFlash('success', Yii::t('messages', 'Tạo tài khoản thành công.'));
                        return $this->redirect(['index']);

                    } else {
                        //Nếu có lỗi xóa tài khoản đi
                        $customer->deleteCase();
                        $customer->delete();
                        Yii::$app->session->removeAllFlashes();
                        Yii::$app->session->setFlash('error', Yii::t('messages', 'Không tạo được tài khoản.'));
                    }

                } else {
                    Yii::$app->session->removeAllFlashes();
                    Yii::$app->session->setFlash('error', Yii::t('messages', 'Không tạo được tài khoản.'));
                }

            } else {
            }
        }

        return $this->render('register', ['model' => $model,]);
    }

}
