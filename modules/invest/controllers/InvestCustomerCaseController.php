<?php

namespace app\modules\invest\controllers;

use app\components\controllers\InvestBaseController;
use app\models\investments\InvestCustomerCase;
use app\models\investments\InvestPackage;
use Yii;
use yii\data\ActiveDataProvider;

class InvestCustomerCaseController extends InvestBaseController
{
    public function actionIndex()
    {
        $query = InvestCustomerCase::find()
            ->joinWith('investCustomerPackage')
            ->where(['invest_customer_case.parent_id' => Yii::$app->user->id])
            ->orderBy(['level' => SORT_ASC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
