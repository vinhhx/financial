<?php

namespace app\modules\invest\controllers;

use app\models\investments\InvestCustomerBankAddress;
use app\models\investments\InvestCustomerCase;
use app\models\searchs\InvestCustomerBankAddressSearch;
use app\modules\invest\forms\packages\RegisterForm;
use app\components\controllers\CustomerBaseController;
use app\components\controllers\InvestBaseController;
use app\models\CustomerActivity;
use app\models\CustomerBankAddress;
use app\models\CustomerOrder;
use app\models\CustomerPackage;
use app\models\CustomerPoundageTransaction;
use app\models\CustomerRequireCashout;
use app\models\CustomerToken;
use app\models\CustomerTransaction;
use app\models\investments\InvestCustomer;
use app\models\investments\InvestCustomerPackage;
use app\models\investments\InvestCustomerRequest;
use app\models\investments\InvestCustomerTransaction;
use app\models\investments\InvestCustomerWallet;
use app\models\investments\InvestPackage;
use app\models\LogAction;
use app\models\searchs\CustomerOrderSearch;
use app\models\searchs\CustomerTokenSearch;
use app\modules\invest\forms\packages\SetParentForm;
use app\modules\invest\forms\requests\ActiveWalletForm;
use \app\modules\invest\forms\requests\CashOutForm;
use app\modules\customer\forms\ChangePasswordForm;
use app\modules\customer\forms\CustomerActiveForm;
use \app\modules\invest\forms\requests\CustomerProviderHelpForm;
use app\modules\customer\forms\CustomerTransactionSearch;
use app\modules\invest\forms\Orders\BuyTokenForm;
use app\modules\invest\forms\Orders\PaymentOrderForm;
use app\modules\customer\forms\search\CustomerBankAddressSearch;
use app\modules\customer\forms\search\CustomerCaseSearch;
use app\modules\customer\forms\search\CustomerRequireCashoutSearch;
use app\modules\invest\forms\packages\UpgradePackageForm;
use Yii;
use app\models\Customer;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Package;
use yii\web\UploadedFile;


/**
 * MyAccountController implements the CRUD actions for Customer model.
 */
class MyAccountController extends InvestBaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
//                    'active-by-current-token' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {

        //return $this->redirect(['default/index']);
        $model = Yii::$app->user->identity;
        return $this->render('index', ['model' => $model]);
    }

    /**
     * Displays a single Customer model.
     * @param string $id
     * @return mixed
     */
    public function actionView()
    {
        return $this->render('view', [
            'model' => $this->findModel(Yii::$app->user->id),
        ]);
    }


    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate()
    {

        $model = $this->findModel(Yii::$app->user->id);
        $packageList = ArrayHelper::map(Package::getPackages(Yii::$app->user->identity->amount), 'id', 'package_name');
        $accountInvite = ArrayHelper::map(Customer::getAccountInBranch(Yii::$app->user->id), 'id', 'username');
        if ($model->load(Yii::$app->request->post()) && $model->save(true, [
                'full_name',
                'phone',
                'sex',
                'dob',
                'email',
                'address',
                'upated_at'
            ])
        ) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'packageList' => $packageList,
                'accountInvite' => $accountInvite
            ]);
        }
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public
    function actionDelete($id)
    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionChangePassword()
    {
//        $user= $this->findModel($id);
        $model = new \app\modules\invest\forms\ChangePasswordForm();
        $model->currentPassword = '';
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->changePassword()) {
                Yii::$app->session->setFlash('success', Yii::t('customers', 'Update new password completed.'));
                $this->redirect(['index']);
            } else {

            }
        }
        return $this->render('change-password', [
            'model' => $model
        ]);
    }


    public function actionMyBank()
    {
        $searchModel = new InvestCustomerBankAddressSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams['InvestCustomerBankAddressSearch']['invest_customer_id'] = Yii::$app->user->id;
        $dataProvider = $searchModel->search($queryParams);

        return $this->render('banks/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateAddressBank()
    {
        if (Yii::$app->user->identity->bankAddress) {
            throw new BadRequestHttpException ('The request not exactly!');
        }
        $model = new InvestCustomerBankAddress();

        if ($model->load(Yii::$app->request->post())) {
            $model->invest_customer_id = Yii::$app->user->id;
            if ($model->save()) {
                return $this->redirect(['my-bank', 'id' => $model->id]);
            }
        }
        return $this->render('banks/create', [
            'model' => $model,
        ]);
    }


    public function actionMember($next = 0)
    {
        $id = Yii::$app->user->id;
        if ($next) {// check có phải là con của bản than ko
            if ($next == $id) {
                $id = $next;
            } else {
                $chk = (new Query())->select('*')->from(InvestCustomerCase::tableName())->where([
                    'parent_id' => $id,
                    'customer_id' => $next
                ])->count();
                if (!$chk) {
                    throw new NotFoundHttpException(Yii::t('messages', 'Yêu cầu không tồn tại.'));
                }
                $id = $next;
            }

        }

        $model = InvestCustomer::showTree($id, 3, false);
        return $this->render('members/index-2', ['model' => $model]);
    }

    public function actionMember2()
    {
        $model = InvestCustomer::showTree(Yii::$app->user->id, 10, false);
        return $this->render('members/index-2', ['model' => $model]);
    }

    public function actionBuyToken()
    {
        $model = new BuyTokenForm();
        $model->customer_id = Yii::$app->user->identity->id;
        if ($model->load(Yii::$app->request->post())) {
            $order = $model->buy();
            if ($order && isset($order->id) && $order->id) {
                Yii::$app->session->setFlash('success', Yii::t('messages', 'Bạn đã mua ') . $order->quantity . ' token! ');
                return $this->redirect(['check-out', 'id' => $order->id]);
            } else {
                Yii::$app->session->setFlash('success', Yii::t('messages', 'Bạn không đặt mua được ') . $order->quantity . ' token! ');
            }
        }
        return $this->render('orders/buy-token', ['model' => $model]);
    }

    public function actionCheckOut($id)
    {
        $model = CustomerOrder::findOne([
            'id' => $id,
            'customer_id' => Yii::$app->user->identity->id,
            'type_module' => CustomerOrder::TYPE_MODULE_INVEST
        ]);
        if (!$model) {
            throw new NotFoundHttpException(Yii::t('messages', 'Yêu cầu không tồn tại.'));
        }
        if (Yii::$app->request->isAjax) {
            $type = Yii::$app->request->post('type', '');
            switch ($type) {
                case 'remove-image':
                    $result = $model->removeAttachment();
                    return $result;

                    break;
                default:
                    break;

            }
            return false;
        }
        $modelForm = new PaymentOrderForm();
        $modelForm->order_id = $model->id;
        if ($modelForm->load(Yii::$app->request->post())) {
            $modelForm->file = UploadedFile::getInstance($modelForm, 'file');

            if ($modelForm->validate('file')) {
                $uploadDir = 'orders/invest/' . Yii::$app->user->id;
                $model->attachment = Yii::$app->image->upload(0, $uploadDir, $modelForm->file);
                $model->upload_attachment_at = time();
                if ($model->attachment) {

                    $model->status = CustomerOrder::STATUS_PAID;
                    if ($model->save(true, ['attachment', 'upload_attachment_at', 'status', 'updated_at'])) {
                        Yii::$app->session->setFlash('success', Yii::t('messages', 'Bạn đã thanh toán cho yêu cầu mua token '));
                        return $this->redirect(['token']);
                    } else {
                    }
                }
            } else {
                $modelForm->addError('file', 'Your image exactly');
            }

        }
        return $this->render('orders/checkout', ['model' => $model, 'modelForm' => $modelForm]);
    }

    public function actionToken()
    {
        $modelSearch = new CustomerOrderSearch();
        $params = Yii::$app->request->queryParams;
        $params["CustomerOrderSearch"]["customer_id"] = Yii::$app->user->identity->id;
        $params["CustomerOrderSearch"]["type_module"] = CustomerOrder::TYPE_MODULE_INVEST;
        $dataProvider = $modelSearch->search($params);
        return $this->render('orders/index', ['modelSearch' => $modelSearch, 'dataProvider' => $dataProvider]);
    }

    public function actionUpgrade()
    {
        $model = $this->finMyPackage();
        $formModel = new UpgradePackageForm();

        if ($formModel->load(Yii::$app->request->post())) {
            $formModel->customer_id = Yii::$app->user->id;
            if ($formModel->validate()) {
                $result = $formModel->upgrade();
                if (isset($result["status"]) && $result["status"] == "success") {
                    Yii::$app->session->setFlash('success', Yii::t('messages', 'Nâng cấp gói đầu tư thành công'));
                    return $this->redirect(['default/index']);
                }
                $msg = isset($result["message"]) ? $result["message"] : Yii::t('messages', 'Không nâng cấp được gói đầu tư');
                Yii::$app->session->setFlash('error', $msg);
            } else {
                Yii::$app->session->setFlash('error', Yii::t('messages', 'Dữ liệu nhập vào không chính xác'));
            }
        }
        $packages = InvestPackage::find()->where([
            'status' => InvestPackage::STATUS_ACTIVE
        ])->all();
        return $this->render('upgrade', ['model' => $model, 'formModel' => $formModel, 'packages' => $packages]);

    }

    protected function finMyPackage()
    {
        if (($model = InvestCustomerPackage::findOne(['customer_id' => Yii::$app->user->id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('messages', 'Yêu cầu không tồn tại.'));
        }
    }

    public function actionCashOutProfit()
    {
        $model = $this->finMyPackage();
        if (!$model->canCashOutProfit()) {
            Yii::$app->session->setFlash('warning', Yii::t('messages', 'Hiện tại không thể rút tiền lợi nhuận về ví được'));

        } else {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                //cập nhật gói
                $amount = $model->current_profit;
                if ($model->hasBonusProfit()) {
                    $amount += round(($amount * 35 / 100), 8);
                }
                $model->current_profit = 0;
                $model->last_cash_out_at = time();
                $wallet = Yii::$app->user->identity->getCurrentWallet();
                if ($model->save(true, ['current_profit', 'last_cash_out_at', 'updated_at'])) {
                    //Tạo transaction
                    $balanceBefore = $wallet->balance;
                    $amount -= round((($amount * Yii::$app->params['feeProfit']) / 100), 8);
                    $wallet->balance += $amount;
                    if ($wallet->save(true, ['balance', 'updated_at'])) {
                        $customerTransaction = new InvestCustomerTransaction();
                        $customerTransaction->customer_id = Yii::$app->user->identity->id;
                        $customerTransaction->customer_username = Yii::$app->user->identity->username;
                        $customerTransaction->bill_id = $model->id;
                        $customerTransaction->amount = $amount;
                        $customerTransaction->sign = InvestCustomerTransaction::SIGN_ADD;
                        $customerTransaction->type = InvestCustomerTransaction::TYPE_CASH_OUT_PROFIT;
                        $customerTransaction->balance_before = $balanceBefore;
                        $customerTransaction->balance_after = $wallet->balance;
                        if (!$customerTransaction->save()) {
                            throw new Exception(Yii::t('messages', 'Không tạo được giao dịch'));
                        }
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('messages', 'Rút tiền lãi từ gói đầu tư về ví thành công.'));
                    }

                } else {
                    throw new Exception(Yii::t('messages', "Không rút lợi nhuận đầu tư về được."));
                }
            } catch (\Exception $ex) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $ex->getMessage());
            }
        }
        return $this->redirect(['default/index']);
    }

    public function actionCashOutPoundage()
    {
        $model = $this->finMyPackage();
        if (!$model->canCashOutPoundage()) {
            Yii::$app->session->setFlash('warning', Yii::t('messages', 'Hiện tại không thể rút đươc hoa hồng về ví.'));

        } else {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                //cập nhật gói
                $amount = $model->current_poundage;
                if ($model->hasBonusPoundage()) {
                    $amount += round(($amount * 25 / 100), 8);
                }
                $model->current_poundage = 0;
                $model->last_cash_out_poundage = time();
                $wallet = Yii::$app->user->identity->getCurrentWallet();
                if ($model->save(true, ['current_poundage', 'last_cash_out_poundage', 'updated_at'])) {
                    //Tạo transaction
                    $balanceBefore = $wallet->balance;
                    $amount -= round((($amount * Yii::$app->params['feePoundage']) / 100), 8);
                    $wallet->balance += $amount;
                    if ($wallet->save(true, ['balance', 'updated_at'])) {
                        $customerTransaction = new InvestCustomerTransaction();
                        $customerTransaction->customer_id = Yii::$app->user->identity->id;
                        $customerTransaction->customer_username = Yii::$app->user->identity->username;
                        $customerTransaction->bill_id = $model->id;
                        $customerTransaction->amount = $amount;
                        $customerTransaction->sign = InvestCustomerTransaction::SIGN_ADD;
                        $customerTransaction->type = InvestCustomerTransaction::TYPE_CASH_OUT_POUNDAGE;
                        $customerTransaction->balance_before = $balanceBefore;
                        $customerTransaction->balance_after = $wallet->balance;
                        if (!$customerTransaction->save()) {
                            throw new Exception(Yii::t('messages', "Không tạo đưuọc giao dịch"));
                        }
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('messages', 'Rút tiền hoa hồng từ gói đầu tư về ví thành công.'));
                    }

                } else {
                    throw new Exception(Yii::t('messages', "Không rút được hoa hồng"));
                }
            } catch (\Exception $ex) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $ex->getMessage());
            }
        }
        return $this->redirect(['default/index']);
    }

    public function actionCashOut()
    {
        $model = $this->finMyPackage();
        if ($model->canCashOutAllPackage()) {
            Yii::$app->session->setFlash('warning', Yii::t('messages', 'Hiện tại không thể rút toàn bộ gói về '));

        } else {
//            $transaction = Yii::$app->db->beginTransaction();
//            try {
//                //cập nhật gói
//                $amountProfit = $model->current_profit;
//                $amountPackage = $model->amount;
//                $model->current_profit = 0;
//                $model->amount = 0;
//                $model->status = InvestCustomerPackage::STATUS_EMPTY;
//                $model->type = InvestCustomerPackage::TYPE_CASHED_OUT;
//                $model->last_cash_out_at = time();
//                $wallet = Yii::$app->user->identity->getCurrentWallet();
//                if ($model->save(true, ['current_profit', 'last_cash_out_at', 'amount', 'status', 'type', 'updated_at'])) {
//                    //Tạo transaction
//                    //Rút tiền hoa hồng trc
//                    $balanceBefore = $wallet->balance;
//                    if ($model->hasBonusProfit()) {
//                        $amountProfit += round(($amountProfit * 35 / 100), 8);
//                    }
//                    $wallet->balance += $amountProfit;
//                    if ($wallet->save(true, ['balance', 'updated_at'])) {
//                        $customerTransaction = new InvestCustomerTransaction();
//                        $customerTransaction->customer_id = Yii::$app->user->identity->id;
//                        $customerTransaction->customer_username = Yii::$app->user->identity->username;
//                        $customerTransaction->bill_id = $model->id;
//                        $customerTransaction->amount = $amountProfit;
//                        $customerTransaction->sign = InvestCustomerTransaction::SIGN_ADD;
//                        $customerTransaction->type = InvestCustomerTransaction::TYPE_CASH_OUT_POUNDAGE;
//                        $customerTransaction->balance_before = $balanceBefore;
//                        $customerTransaction->balance_after = $wallet->balance;
//                        if (!$customerTransaction->save()) {
//                            throw new Exception("Not create transaction record.");
//                        }
//
//
//                    } else {
//                        throw new Exception("Not save new wallet balance.");
//                    }
//                    //Rút tiền gốc
//                    $balanceBefore = $wallet->balance;
//                    $wallet->balance += $amountPackage;
//                    if ($wallet->save(true, ['balance', 'updated_at'])) {
//                        $customerTransaction = new InvestCustomerTransaction();
//                        $customerTransaction->customer_id = Yii::$app->user->identity->id;
//                        $customerTransaction->customer_username = Yii::$app->user->identity->username;
//                        $customerTransaction->bill_id = $model->id;
//                        $customerTransaction->amount = $amountPackage;
//                        $customerTransaction->sign = InvestCustomerTransaction::SIGN_ADD;
//                        $customerTransaction->type = InvestCustomerTransaction::TYPE_CASH_OUT_CAPITAL;
//                        $customerTransaction->balance_before = $balanceBefore;
//                        $customerTransaction->balance_after = $wallet->balance;
//                        if (!$customerTransaction->save()) {
//                            throw new Exception("Not create transaction record.");
//                        }
//
//                    } else {
//                        throw new Exception("Not save new wallet balance.");
//                    }
//                    $transaction->commit();
//                    Yii::$app->session->setFlash('success', 'Rút tiền lãi và gốc từ gói đầu tư về ví thành công.');
//
//                } else {
//                    throw new Exception("Not cash out profit of package.");
//                }
//            } catch (\Exception $ex) {
//                $transaction->rollBack();
//                Yii::$app->session->setFlash('error', $ex->getMessage());
//            }
        }
        return $this->redirect(['default/index']);

    }

    public function actionProviderHelp()
    {
        $model = new CustomerProviderHelpForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->bank_address = Yii::$app->params["invest_bit_coin_address"];
            $model->customer_id = Yii::$app->user->id;
            //Tao request nop tien
            if ($model->validate()) {
                $customerRequest = new InvestCustomerRequest();
                $customerRequest->customer_id = Yii::$app->user->identity->id;
                $customerRequest->customer_username = Yii::$app->user->identity->username;
                $customerRequest->amount = $model->amount;
                $customerRequest->bank_address = Yii::$app->params["invest_bit_coin_address"];
                $customerRequest->status = InvestCustomerRequest::STATUS_CREATED;
                $customerRequest->type = InvestCustomerRequest::TYPE_PROVIDER_HELP;
                if ($customerRequest->save()) {
                    Yii::$app->session->setFlash('success', Yii::t('messages', 'Bạn vừa tạo yêu cầu chuyển BTC vào ví.'));
                    return $this->redirect(['my-request/view', 'id' => $customerRequest->id]);
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('messages', 'Lỗi thao tác chuyển BTC vào tài ví.'));
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('messages', 'Thông tin đầu vào không chính xác.'));
            }
        }

        return $this->render('provider-help', ['model' => $model]);
    }

    public function actionGetHelp()
    {
        $model = new CashOutForm();
        $customerBank = Yii::$app->user->identity->bankAddress;

        $model->bank_address = ($customerBank) ? $customerBank->bank_address : '';
        if ($model->load(Yii::$app->request->post())) {
//            $model->bank_address = Yii::$app->params["system_bit_coin_address"];
            $model->customer_id = Yii::$app->user->id;
            $model->bank_address = ($customerBank) ? $customerBank->bank_address : '';
            //Tao request nop tien
            if ($model->validate() && Yii::$app->user->identity->canWithDrawn()) {
                $customerRequest = new InvestCustomerRequest();
                $customerRequest->customer_id = Yii::$app->user->identity->id;
                $customerRequest->customer_username = Yii::$app->user->identity->username;
                $customerRequest->amount = $model->amount;
                $customerRequest->bank_address = $model->bank_address;
                $customerRequest->status = InvestCustomerRequest::STATUS_CREATED;
                $customerRequest->type = InvestCustomerRequest::TYPE_GET_HELP;
                if ($customerRequest->save()) {
                    Yii::$app->session->setFlash('success', Yii::t('messages', 'Bạn vừa tạo yêu cầu rút BTC thành công.'));
                    return $this->redirect(['my-request/view', 'id' => $customerRequest->id]);
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('messages', 'Lỗi thao tác tạo yêu cầu rút.'));
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('messages', 'Thông tin đầu vào không chính xác.'));
            }
        }

        return $this->render('get-help', ['model' => $model]);
    }


    public function actionActive()
    {
        $model = new ActiveWalletForm();
        $model->customer_id = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post())) {
            if ($model->active()) {
                Yii::$app->session->setFlash('success', Yii::t('messages', 'Tài khoản đã được kích hoạt thành công'));
                return $this->redirect(['default/index']);
            } else {
                Yii::$app->session->removeAllFlashes();
                Yii::$app->session->setFlash('error', Yii::t('messages', 'Mã token không chính xác'));
            }
        } else {
            Yii::$app->session->removeAllFlashes();
        }
//        $modelSearch=null;
//        $dataProvider=null;

        $modelSearch = new CustomerTokenSearch();
        $params = Yii::$app->request->queryParams;
        $params['CustomerTokenSearch']['customer_id'] = Yii::$app->user->id;
        $params['CustomerTokenSearch']['status'] = CustomerToken::STATUS_PENDING;
        $params['CustomerTokenSearch']['type'] = CustomerToken::TYPE_INVEST;
        $dataProvider = $modelSearch->search($params);
        return $this->render('_form-active', ['model' => $model, 'dataProvider' => $dataProvider]);

    }


    public function actionCreatePackage()
    {
        $this->layout = 'login';
        $currentPackage = Yii::$app->user->identity->getCurrentPackage();
        if ($currentPackage) {
            return $this->redirect(['default/index']);
        }
        $model = new RegisterForm();
        if ($model->load(Yii::$app->request->post())) {
            if (!Yii::$app->user->isGuest) {
                $model->customer_id = Yii::$app->user->identity->id;
            }
            if (!Yii::$app->params["INVEST_OPEN_CREATE_PACKAGE"]) {
                $msg = Yii::t('systems', 'Hiện tại hệ thống đang khóa chức năng tạo gói đầu tư. Vui lòng chờ trong thời gian tới.');
                Yii::$app->session->setFlash('warning', $msg);
            } else {
                if ($model->validate()) {
                    $result = $model->register();
                    if (isset($result["code"]) && (int)$result["code"] == 200) {
                        Yii::$app->session->removeAllFlashes();
                        Yii::$app->session->setFlash('success', Yii::t('systems', 'Đăng ký sân chơi lãi tĩnh thành công.'));
                        if (isset($result['requestId']) && $result["requestId"]) {
                            return $this->redirect(['my-request/view', 'id' => $result["requestId"]]);
                        }
                        return $this->redirect(['default/index']);
                    } else {
                        Yii::$app->session->removeAllFlashes();
                        $msg = isset($result["message"]) ? $result["message"] : Yii::t('systems', 'Lỗi không đăng ký được sân chơi lãi tĩnh.');
                        Yii::$app->session->setFlash('error', $msg);
                    }
                }
            }
        }

        $packages = InvestPackage::find()->where([
            'status' => InvestPackage::STATUS_ACTIVE
        ])->all();

        return $this->render('create-package', ['model' => $model, 'packages' => $packages]);
    }

    public function actionSetParent($ref = null)
    {
        $this->layout = 'login';
        $model = new \app\modules\invest\forms\SetParentForm();
        $model->ref_code = $ref;
        if ($model->load(Yii::$app->request->post())) {
            $model->customer_id = Yii::$app->user->id;
            $result = $model->setParent();
            if (isset($result["code"]) && $result["code"] == 200) {
                Yii::$app->session->setFlash('success', Yii::t('systems', 'Đăng ký người giới thiệu thành công.'));
                return $this->redirect(['default/index']);
            } else {
                $msg = isset($result["message"]) ? $result["message"] : Yii::t('messages', 'Không đăng ký được người giới thiệu');
                Yii::$app->session->setFlash('error', $msg);
            }
        }
        return $this->render('set-parent', ['model' => $model]);
    }


}
