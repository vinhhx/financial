<?php

namespace app\modules\invest;

/**
 * invest module definition class
 */
use yii\helpers\Url;

class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\invest\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here

        // Set Components
        \Yii::$app->setComponents([
//            'authManager' => [
//                'class' => '\yii\rbac\DbManager',
//                'cache' => 'cache'
//            ],
            'user' => [
                'class' => '\yii\web\User',
                'identityClass' => 'app\models\investments\InvestCustomer',
                'enableAutoLogin' => false,
                'loginUrl' => Url::to(['/invest/default/login']),
                'idParam' => '__invest',
                'authTimeoutParam' => '__investExpire',
                'absoluteAuthTimeoutParam' => '__investAbsoluteExpire',
                'returnUrlParam' => '__investReturnUrl',
            ],
//            'urlManager' => [
//                'class' => 'yii\web\urlManager',
//                'enablePrettyUrl' => true,
//                'showScriptName' => false,
//                'rules' => [
//
//                ],
//            ],

        ]);

        \Yii::$app->errorHandler->errorAction = 'invest/default/error';

        // Set default layout
        $this->layout = 'admin';
        \Yii::$app->language = 'en';
    }
}
