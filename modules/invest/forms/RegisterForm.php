<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 06-Jul-16
 * Time: 12:21 PM
 */
namespace app\modules\invest\forms;

use app\forms\InvestRegisterForm;
use app\models\Customer;
use app\models\CustomerActivity;
use app\models\CustomerBankAddress;
use app\models\investments\InvestCustomer;
use app\modules\customer\forms\CustomerInvestPackageForm;

class RegisterForm extends InvestRegisterForm
{


    public function createCustomer($isAdmin = false)
    {

        $customer = new InvestCustomer();
        $customer->username = $this->username;
        $customer->password_hash = $this->password;
        $customer->full_name = $this->full_name;
        $customer->date_join = time();
        $customer->identity_card = $this->identity_card;
        $customer->email = $this->email;
        $customer->dob = $this->dob;
        $customer->parent_id = ($this->customer_parent_id) ? $this->customer_parent_id : 0;
        $customer->phone = $this->phone;
        $customer->sex = ($this->sex) ? $this->sex : 0;
        $customer->address = ($this->address) ? $this->address : '';
        $customer->nation = ($this->nation) ? $this->nation : '';
        $customer->setPassword($customer->password_hash);
        $customer->generateAuthKey();
        $customer->createRefCode();
        $customer->status = InvestCustomer::STATUS_ACTIVE;
        $customer->is_active = InvestCustomer::IS_ACTIVE_PENDING;
//        $customer->checkParentRequired();
        if ($isAdmin) {
            $customer->created_type = InvestCustomer::TYPE_ADMIN;
            $customer->is_active = InvestCustomer::IS_ACTIVE_COMPLETED;

        } else {
            $customer->created_type = InvestCustomer::TYPE_CUSTOMER;
        }
        if (!\Yii::$app->user->isGuest) {
            $customer->created_by = \Yii::$app->user->identity->username;
        }

        if ($customer->save()) {
//            CustomerActivity::systemCreate($customer->id, $customer->username, CustomerActivity::TYPE_DANG_KY_TAI_KHOAN, 'Đăng ký thành công tài khoản', $customer->attributes);
            //Lưu bảng customer case
            $customer->createInvestWallet();
            if ($isAdmin && $this->bank_address) {
                $bankAddress = new CustomerBankAddress();
                $bankAddress->customer_id = $customer->id;
                $bankAddress->bank_address = $this->bank_address;
                $bankAddress->status = CustomerBankAddress::STATUS_ACTIVE;
                $bankAddress->save();
            }

        } else {
        }
        return $customer;
    }

    public function checkRequireCreate()
    {
        if ($this->validate()) {
            if ($this->customer_parent_id) {
                $customer = InvestCustomer::findOne($this->customer_parent_id);
                if (!$customer) {
                    $this->addError('ref_code', \Yii::t('systems', 'This sponsor code not existed'));
                    return false;
                }
                $totalChild = $customer->hasChildren();
                if ($totalChild >= 2) {
                    $this->addError('ref_code', \Yii::t('systems', 'This sponsor code is max 2 member'));
                    return false;
                }

            } else {
                $this->addError('ref_code', \Yii::t('systems', 'This sponsor code not existed'));
                return false;
            }
            return true;
        }
        return false;
    }


}