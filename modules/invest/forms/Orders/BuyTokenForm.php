<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 25-Jun-16
 * Time: 1:52 AM
 */
namespace app\modules\invest\forms\Orders;

use app\models\CustomerOrder;
use app\models\CustomerToken;
use yii\base\Model;

class BuyTokenForm extends Model
{

    public $quantity;
    public $customer_id;


    public function rules()
    {

        return [
            [['quantity'], 'integer', 'min' => 1, 'max' => 500],
            [['customer_id'], 'integer'],
            [['customer_id', 'quantity'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'quantity' => \Yii::t('customers','Số lượng token cần mua'),
            'customer_id' => \Yii::t('customers','Mã người chơi'),
        ];
    }

    public function buy()
    {
        $order = new CustomerOrder();
        $order->customer_id = \Yii::$app->user->identity->id;
        $order->customer_username = \Yii::$app->user->identity->username;
        $order->quantity = $this->quantity;
        $order->type_module=CustomerOrder::TYPE_MODULE_INVEST;
        $order->save();
        return $order;
    }


}