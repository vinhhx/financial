<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 2:11 PM
 */
namespace app\modules\invest\forms\requests;

use app\models\CustomerPackage;
use app\models\CustomerRequireCashout;
use app\models\CustomerTransaction;
use app\models\LogAction;
use app\models\Package;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\Html;
use Yii;

class CustomerProviderHelpForm extends Model
{

    public $customer_id;
    public $bank_address;
    public $amount;


    public function rules()
    {
        return [
            [['customer_id', 'bank_address', 'amount'], 'required'],
            [['customer_id'], 'integer'],
            [['amount'], 'number', 'min' => 0.1],
            [['bank_address'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'customer_id' => Yii::t('investment', 'Mã người chơi'),
            'bank_address' => Yii::t('investment', 'Địa chỉ ngân hàng thụ hưởng'),
            'amount' => Yii::t('investment', 'Số BTC'),
        ];
    }

    public function createPackage()
    {
        $customerPackage = new CustomerPackage();
        $package = Package::findOne(['id' => $this->package_id, 'status' => Package::STATUS_ACTIVE]);
        if (!$package) {
            return [
                "status" => 'error',
                "error_code" => '102',
                "message" => Yii::t('messages', 'Gói đầu tư không chính xác'),
            ];
        }
        $customerPackage->customer_id = $this->customer_id;
        $customerPackage->package_id = $package->id;
        $customerPackage->package_amount = $package->amount;
        $customerPackage->increase_amount = $package->increase_percent;
        $customerPackage->package_name = ($this->package_name) ? $this->package_name : $package->package_name . '-' . date('Y-m-d');
        $customerPackage->block_chain_wallet_id = $this->bank_address;
        $customerPackage->status = CustomerPackage::STATUS_REQUEST_IN_STEP_ONE;
        $customerPackage->is_reinvestment = (Yii::$app->user->identity->is_invest) ? 1 : 0;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($customerPackage->save()) {
                $customerPackage->reinvestment_date = $customerPackage->created_at;
                $customerPackage->save(true, ['reinvestment_date']);
                //Tạo gói đầu tư rồi giờ tạo request
                $result = $customerPackage->requestOpenPackage($this->sent_bit_coin_first);
                if ($result["status"] == "success") {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
                return $result;
            }
        } catch (\Exception $ex) {
            $transaction->rollBack();
            return [
                "status" => "error",
                "error_code" => $ex->getCode(),
                "message" => $ex->getMessage(),

            ];
        }


    }

}


