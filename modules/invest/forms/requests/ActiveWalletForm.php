<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 14-Jun-16
 * Time: 7:28 AM
 */
namespace app\modules\invest\forms\requests;

use app\models\Customer;
use app\models\CustomerActivity;
use app\models\CustomerToken;
use app\models\investments\InvestCustomer;
use app\models\investments\InvestCustomerWallet;
use yii\base\Model;
use Yii;

class ActiveWalletForm extends Model
{
    public $customer_id;
    public $token;

    public function rules()
    {
        return [
            [['customer_id', 'token'], 'required'],
            [['customer_id'], 'integer'],
            [['token'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'customer_id' => Yii::t('investment', 'Mã người chơi'),
            'token' => Yii::t('investment', 'Token'),
        ];
    }

    public function active()
    {
        $token = CustomerToken::find()
            ->where([
                'token' => $this->token,
                'status' => CustomerToken::STATUS_PENDING,
                'type' => CustomerToken::TYPE_INVEST
            ])
            ->andFilterWhere(['or',
                ['customer_id' => \Yii::$app->user->id],
                ['customer_id' => null]])
            ->limit(1)->one();
        if ($token) {
            \Yii::$app->user->identity->is_active = InvestCustomer::IS_ACTIVE_COMPLETED;
            if (\Yii::$app->user->identity->save(true, ['is_active', 'updated_at'])) {
                $token->status = CustomerToken::STATUS_USED;
                if (!$token->customer_id) {
                    $token->customer_id = \Yii::$app->user->identity->id;
                }
                if ($token->save(true, ['customer_id', 'status', 'updated_at'])) {
//                    CustomerActivity::customerCreate(CustomerActivity::TYPE_KICH_HOAT_TAI_KHOAN, 'Kích hoạt tài khoản thành công', $token->attributes);
                    return true;
                } else {
                    \Yii::$app->user->identity->is_active = InvestCustomer::IS_ACTIVE_PENDING;
                    \Yii::$app->user->identity->save(true, ['is_active', 'updated_at']);
                }
            }

        }
        return false;
    }

}