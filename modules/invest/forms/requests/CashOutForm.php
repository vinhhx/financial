<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 14-Dec-15
 * Time: 11:09 PM
 */

namespace app\modules\invest\forms\requests;

use app\models\Customer;
use app\models\CustomerPackage;
use app\models\CustomerPoundageTransaction;
use app\models\CustomerRequest;
use app\models\CustomerRequireCashout;
use app\models\CustomerToken;
use app\models\CustomerTransaction;
use app\models\CustomerTransactionQueue;
use app\models\LogAction;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\Html;
use Yii;

class CashOutForm extends Model
{
    public $amount;
    public $customer_id;
    public $bank_address;

    public function rules()
    {
        return [
            [['customer_id'], 'integer'],
            [['bank_address'], 'string'],
//            [['amount'], 'number',  'min' => Yii::$app->params["min_cash_out_amount"], 'max' =>  Yii::$app->params["max_cash_out_amount"]],
            [['amount'], 'number'],
            [['amount'], 'ValidateWithDraw'],
            [['amount', 'customer_id', 'bank_address'], 'required', 'skipOnEmpty' => false, 'skipOnError' => false],
        ];
    }

    public function attributeLabels()
    {
        return [
            'amount' => Yii::t('investment', 'Số BTC'),
            'bank_address' => Yii::t('investment', 'Tài khoản ngân hàng thụ hưởng'),
        ];
    }

    public function ValidateWithDraw($attribute, $param)
    {
        $wallet = Yii::$app->user->identity->getCurrentWallet();
        if ($wallet->balance < $this->$attribute) {
            $this->addError($attribute, Yii::t('messages', "Số tiền yêu cầu rút ra lớn hơn số dư tài khoản."));
        }
    }

}
