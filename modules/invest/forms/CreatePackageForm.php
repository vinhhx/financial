<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 29-Jun-16
 * Time: 7:19 AM
 */
namespace app\modules\invest\forms;

use app\models\CustomerPackage;
use app\models\CustomerRequest;
use app\models\investments\InvestCustomer;
use app\models\investments\InvestCustomerCase;
use app\models\investments\InvestCustomerPackage;
use app\models\investments\InvestCustomerPackageHistory;
use app\models\investments\InvestCustomerRequest;
use app\models\investments\InvestPackage;
use Yii;
use yii\base\Model;
use yii\db\Query;

class CreatePackageForm extends Model
{
    public $customer_id;
    public $parent_id;
    public $ref_code;
    public $amount;
    public $package_id;

    public function rules()
    {
        return [
            [['ref_code'], 'required'],
            [['ref_code'], 'string'],
            [['ref_code'], 'ValidatorCustomerInvite'],
            [['customer_id', 'parent_id', 'amount'], 'required'],
            [['amount'], 'number'],
            [['customer_id', 'parent_id', 'package_id'], 'integer'],
            [['amount'], 'ValidatorAmount'],

        ];
    }

    public function ValidatorAmount($attribute, $params)
    {
        $query = InvestPackage::find();
        $query->where(['status' => InvestPackage::STATUS_ACTIVE]);
        $query->andFilterWhere([
            'and',
            ['<=', 'amount_min', $this->$attribute],
            ['>=', 'amount_max', $this->$attribute]
        ]);
        $query->orderBy(['amount_min' => SORT_DESC]);
        $query->limit(1);
        $package = $query->one();
        if ($package) {
            $this->package_id = $package->id;
        } else {
            $this->addError($attribute, 'Number input not in range package');
        }
    }

    public function ValidatorCustomerInvite($attribute, $params)
    {
        if ($this->$attribute) {
            $customer = InvestCustomer::findOne(['ref_code' => $this->$attribute]);
            if (!$customer) {
                $this->addError($attribute, 'Referrer code not existed.');
            } elseif (!$customer->isRegister()) {
                $this->addError($attribute, 'Account invite not active.');
            } elseif ($customer->hasChildren() >= 2) {
                $this->addError($attribute, 'Account invite max children.');
            } else {
                $this->parent_id = $customer->id;
            }
        }
    }

    public function register()
    {
        $result = [];
        if ($this->validate()) {
            $wallet = Yii::$app->user->identity->getCurrentWallet();
            $customerPackage = new InvestCustomerPackage();
            $customerPackage->customer_id = Yii::$app->user->id;
            $customerPackage->package_id = $this->package_id;
            $customerPackage->amount = $this->amount;
            $customerPackage->status = InvestCustomerPackage::STATUS_ACTIVE;
            $customerPackage->is_active = InvestCustomerPackage::IS_ACTIVE_PENDING;
            $customerPackage->current_profit = 0;
            $customerPackage->total_current_profit = 0;
            $customerPackage->joined_at = time();
            $customerPackage->last_profit_receiver = time();

            $transaction = Yii::$app->db->beginTransaction();
            try {
                //Tao Pakage
                if ($customerPackage->save()) {
                    //Tao Package History
                    $packHis = new InvestCustomerPackageHistory();
                    $packHis->customer_package_id = $customerPackage->id;
                    $packHis->sign = InvestCustomerPackageHistory::SIGN_ADD;
                    $packHis->amount = 0;
                    $packHis->type = InvestCustomerPackageHistory::TYPE_OPEN_PACKAGE;
                    if (!$packHis->save()) {
                        throw new Exception("Not insert to pack history.");
                    }
                    //Tao request nop tien
                    $customerRequest = new InvestCustomerRequest();
                    $customerRequest->customer_id = Yii::$app->user->identity->id;
                    $customerRequest->customer_username = Yii::$app->user->identity->username;
                    $customerRequest->amount = $customerPackage->amount + Yii::$app->params["join_fee"];
                    $customerRequest->bank_address = Yii::$app->params["system_bit_coin_address"];
                    $customerRequest->status = InvestCustomerRequest::STATUS_CREATED;
                    $customerRequest->type = InvestCustomerRequest::TYPE_LOAN_OPEN_PACKAGE;
                    if ($customerRequest->save()) {
                        $result = $this->addInvestCustomerCase();
                        if (isset($result["code"]) && (int)$result["code"] == 200) {
                            $transaction->commit();
                            return $result;
                        }
                    } else {
                        throw new \Exception('Not create new request join');
                    }
                } else {
                    throw new \Exception('Not create new package');
                }
            } catch (\Exception $ex) {
                $transaction->rollBack();
                return [
                    "status" => "success",
                    "code" => 400,
                    "message" => $ex->getMessage()
                ];
            }
        }
        return $result;
    }

    public function addInvestCustomerCase()
    {
        $result = [];
        if ($this->customer_id) {
            //Nếu là nhánh cấp 1 thì nó chính là con của chính nó
            try {
                if ($this->parent_id == 0) {
                    $customerCases = new InvestCustomerCase();
                    $customerCases->customer_id = $this->customer_id;
                    $customerCases->parent_id = $this->parent_id;
                    $customerCases->level = 0;
//                    $customerCases->customer_username = $this->username;
                    $customerCases->status = InvestCustomerCase::STATUS_ACTIVE;
                    if ($customerCases->save()) {
                        $result["status"] = "success";
                        $result["code"] = 200;
                        $result["message"] = "Thêm tài khoản giới thiệu thành công";
                        return $result;
                    }
                } elseif ($this->parent_id > 0) {
                    //Lấy danh sách khách hàng là cha của người giới thiệu
                    $customerCases = (new Query())
                        ->select(['id', 'parent_id', 'position', 'status', 'level'])
                        ->from(InvestCustomerCase::tableName())
                        ->where(['customer_id' => $this->parent_id])
                        ->andFilterWhere(['>', 'level', 0])
                        ->all();
                    $data = [];
                    //Add cha nó là lv1
                    $currentTime = time();
                    //Tính xem nó là chân trái hay phải
                    $numberChildren = (new Query())
                        ->select(['count(customer_id)'])
                        ->from(InvestCustomerCase::tableName())
                        ->where(['parent_id' => $this->parent_id, 'level' => 1])
                        ->scalar();
                    $position = ($numberChildren) + 1;
                    $data[] = [
                        $this->customer_id,
                        $this->parent_id,
                        1,
                        $position,
                        InvestCustomerCase::STATUS_ACTIVE,
                        $currentTime,
                        $currentTime
                    ];
                    if ($customerCases) {
                        $dataActivity = [];//Sau làm thêm chức năng thông báo tài khoản con đc thêm mới
                        foreach ($customerCases as $item) {
                            $data[] = [
                                $this->customer_id,
                                $item["parent_id"],
                                $item["level"] + 1,
                                $item["position"],
                                $item["status"],
                                $currentTime,
                                $currentTime
                            ];
                        }

                    }
                    if (!empty($data)) {
                        Yii::$app->db->createCommand()->batchInsert(InvestCustomerCase::tableName(), [
                            'customer_id',
                            'parent_id',
                            'level',
                            'position',
                            'status',
                            'created_at',
                            'updated_at',
                        ], $data)->execute();


                        $result["status"] = "success";
                        $result["code"] = 200;
                        $result["message"] = "Thêm tài khoản giới thiệu thành công";
                        return $result;
                    } else {
                        $result["status"] = "error";
                        $result["code"] = 110;
                        $result["message"] = "Thêm tài khoản giới thiệu có lỗi";
                        return $result;
                    }
                } else {
                    $result["status"] = "error";
                    $result["code"] = 110;
                    $result["message"] = "Thêm tài khoản giới thiệu có lỗi";
                    return $result;
                }
            } catch (\Exception $ex) {
                $result["status"] = "error";
                $result["code"] = $ex->getCode();
                $result["message"] = $ex->getMessage();
                return $result;
            }

        }
        return $result;
    }

}