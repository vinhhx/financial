<?php

namespace app\modules\invest\forms;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\investments\InvestCustomerTransaction;

/**
 * InvestCustomerTransactionSearch represents the model behind the search form about `app\models\investments\InvestCustomerTransaction`.
 */
class InvestCustomerTransactionSearch extends InvestCustomerTransaction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'bill_id', 'sign', 'type', 'created_at', 'updated_at'], 'integer'],
            [['customer_username'], 'safe'],
            [['amount', 'balance_before', 'balance_after'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvestCustomerTransaction::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'bill_id' => $this->bill_id,
            'amount' => $this->amount,
            'sign' => $this->sign,
            'type' => $this->type,
            'balance_before' => $this->balance_before,
            'balance_after' => $this->balance_after,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'customer_username', $this->customer_username]);

        return $dataProvider;
    }
}
