<?php
namespace app\modules\invest\forms;

use app\models\Customer;
use app\models\User;
use Yii;
use yii\base\Model;

class ChangePasswordForm extends Model
{
    public $currentPassword;
    public $newPassword;
    public $newPasswordConfirmation;
    public $captcha;
    public $customerId;

    public function rules()
    {
        return [
            [['currentPassword', 'newPassword', 'newPasswordConfirmation'], 'required'],
            ['currentPassword', 'validateCurrentPassword'],

            ['newPassword', 'string', 'min' => 6],
            ['newPasswordConfirmation', 'compare', 'compareAttribute' => 'newPassword'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'currentPassword' => Yii::t('customers', 'Mật khẩu hiên tại'),
            'newPassword' => Yii::t('customers', 'Mật khẩu mới'),
            'newPasswordConfirmation' => Yii::t('customers', 'Nhập lại mật khẩu'),
        ];
    }

    public function validateCurrentPassword($attribute, $params)
    {
        $customer = Yii::$app->user->identity;
        if (!$customer->validatePassword($this->$attribute)) {
            $this->addError($attribute, Yii::t('customers', 'Mật khẩu không chính xác'));
        }


    }

    public function changePassword()
    {
        Yii::$app->user->identity->setPassword($this->newPassword);
        return Yii::$app->user->identity->save(true, ['password_hash', 'updated_at']);
    }

}