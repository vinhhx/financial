<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 29-Jun-16
 * Time: 7:19 AM
 */
namespace app\modules\invest\forms;

use app\models\CustomerPackage;
use app\models\CustomerRequest;
use app\models\investments\InvestCustomer;
use app\models\investments\InvestCustomerCase;
use app\models\investments\InvestCustomerPackage;
use app\models\investments\InvestCustomerPackageHistory;
use app\models\investments\InvestCustomerRequest;
use app\models\investments\InvestPackage;
use Yii;
use yii\base\Model;
use yii\db\Query;

class SetParentForm extends Model
{
    public $customer_id;
    public $parent_id;
    public $ref_code;
    public $amount;
    public $package_id;

    public function rules()
    {
        return [
            [['ref_code'], 'required'],
            [['ref_code'], 'string'],
            [['ref_code'], 'ValidatorCustomerInvite'],
            [['customer_id', 'parent_id'], 'required'],

            [['customer_id', 'parent_id',], 'integer'],


        ];
    }

    public function ValidatorCustomerInvite($attribute, $params)
    {
        if ($this->$attribute) {
            $customer = InvestCustomer::findOne(['ref_code' => $this->$attribute]);
            if (!$customer) {
                $this->addError($attribute, Yii::t('messages', 'Mã giới thiệu không tồn tại.'));
            } elseif ($customer->hasChildren() >= 2) {
                $this->addError($attribute, Yii::t('messages', 'Tài khoản giới thiệu đã đủ vị trí F1 trực tiếp'));
            } else {
                $this->parent_id = $customer->id;
            }
        }
    }

    public function setParent()
    {
        $result = [];
        if ($this->validate()) {
            $result = $this->addInvestCustomerCase();
        }
        return $result;
    }

    public function addInvestCustomerCase()
    {
        $result = [];
        if ($this->customer_id) {
            //Nếu là nhánh cấp 1 thì nó chính là con của chính nó
            try {
                if ($this->parent_id == 0) {
                    $customerCases = new InvestCustomerCase();
                    $customerCases->customer_id = $this->customer_id;
                    $customerCases->parent_id = $this->parent_id;
                    $customerCases->level = 0;
//                    $customerCases->customer_username = $this->username;
                    $customerCases->status = InvestCustomerCase::STATUS_ACTIVE;
                    if ($customerCases->save()) {
                        $result["status"] = "success";
                        $result["code"] = 200;
                        $result["message"] = Yii::t('messages', "Thêm tài khoản giới thiệu thành công");
                        return $result;
                    }
                } elseif ($this->parent_id > 0) {
                    //Lấy danh sách khách hàng là cha của người giới thiệu
                    $customerCases = (new Query())
                        ->select(['id', 'parent_id', 'position', 'status', 'level'])
                        ->from(InvestCustomerCase::tableName())
                        ->where(['customer_id' => $this->parent_id])
                        ->andFilterWhere(['>', 'level', 0])
                        ->all();
                    $data = [];
                    //Add cha nó là lv1
                    $currentTime = time();
                    //Tính xem nó là chân trái hay phải
                    $numberChildren = (new Query())
                        ->select(['count(customer_id)'])
                        ->from(InvestCustomerCase::tableName())
                        ->where(['parent_id' => $this->parent_id, 'level' => 1])
                        ->scalar();
                    $position = ($numberChildren) + 1;
                    $data[] = [
                        $this->customer_id,
                        $this->parent_id,
                        1,
                        $position,
                        InvestCustomerCase::STATUS_ACTIVE,
                        $currentTime,
                        $currentTime
                    ];
                    if ($customerCases) {
                        $dataActivity = [];//Sau làm thêm chức năng thông báo tài khoản con đc thêm mới
                        foreach ($customerCases as $item) {
                            $data[] = [
                                $this->customer_id,
                                $item["parent_id"],
                                $item["level"] + 1,
                                $item["position"],
                                $item["status"],
                                $currentTime,
                                $currentTime
                            ];
                        }

                    }
                    if (!empty($data)) {
                        Yii::$app->db->createCommand()->batchInsert(InvestCustomerCase::tableName(), [
                            'customer_id',
                            'parent_id',
                            'level',
                            'position',
                            'status',
                            'created_at',
                            'updated_at',
                        ], $data)->execute();


                        $result["status"] = "success";
                        $result["code"] = 200;
                        $result["message"] = Yii::t('messages', "Thêm tài khoản giới thiệu thành công");
                        return $result;
                    } else {
                        $result["status"] = "error";
                        $result["code"] = 110;
                        $result["message"] = Yii::t('messages', "Thêm tài khoản giới thiệu có lỗi");
                        return $result;
                    }
                } else {
                    $result["status"] = "error";
                    $result["code"] = 110;
                    $result["message"] = Yii::t('messages', "Thêm tài khoản giới thiệu có lỗi");
                    return $result;
                }
            } catch (\Exception $ex) {
                $result["status"] = "error";
                $result["code"] = $ex->getCode();
                $result["message"] = $ex->getMessage();
                return $result;
            }

        }
        return $result;
    }

}