<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 29-Jun-16
 * Time: 7:19 AM
 */
namespace app\modules\invest\forms\packages;

use app\models\CustomerPackage;
use app\models\CustomerRequest;
use app\models\CustomerToken;
use app\models\investments\InvestCustomer;
use app\models\investments\InvestCustomerCase;
use app\models\investments\InvestCustomerPackage;
use app\models\investments\InvestCustomerPackageHistory;
use app\models\investments\InvestCustomerRequest;
use app\models\investments\InvestCustomerTransaction;
use app\models\investments\InvestPackage;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Query;

class UpgradePackageForm extends Model
{
    public $customer_id;
    public $amount;
    public $package_id;

    public function rules()
    {
        return [
            [['amount'], 'ValidatorAmount'],
            [['customer_id', 'package_id', 'amount'], 'required'],
            [['amount'], 'number'],
            [['customer_id', 'package_id'], 'integer'],

        ];
    }

    public function ValidatorAmount($attribute, $params)
    {
        $wallet = Yii::$app->user->identity->getCurrentWallet();
        $totalAmount = $wallet->balance;
        $package = Yii::$app->user->identity->getCurrentPackage();
        $packageAmount = $package->amount;
        $amountDiff = $this->$attribute - $packageAmount;
        if ($amountDiff <= 0) {
            $this->addError($attribute, Yii::t('messages', 'Số BTC gói mới không được nhỏ hơn hoặc bằng gói hiện tại'));
        } elseif ($amountDiff > $totalAmount) {
            $this->addError($attribute, Yii::t('messages', 'Số dư tài khoản không đủ để nâng cấp'));
        } else {
            $query = InvestPackage::find();
            $query->where(['status' => InvestPackage::STATUS_ACTIVE]);
            $query->andFilterWhere([
                'and',
                ['<=', 'amount_min', $this->$attribute],
                ['>=', 'amount_max', $this->$attribute]
            ]);
            $query->orderBy(['amount_min' => SORT_DESC]);
            $query->limit(1);
            $package = $query->one();
            if ($package) {
                $this->package_id = $package->id;
            } else {
                $this->addError($attribute, Yii::t('messages', 'Giá trị nhập vào không đúng giá trị các gói đầu tư'));
            }
        }

    }


    public function upgrade()
    {
        $result = [];
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $wallet = Yii::$app->user->identity->getCurrentWallet();
            $package = Yii::$app->user->identity->getCurrentPackage();
            $token = CustomerToken::find()
                ->where([
                    'status' => CustomerToken::STATUS_PENDING,
                    'customer_id' => \Yii::$app->user->id
                ])->limit(1)->one();
            if (!$token) {
                throw new \Exception(Yii::t('messages', "Số token của bạn hiên tại không đủ để nâng cấp gói."));
            }
            if ($package){
                if ($package->status == InvestCustomerPackage::STATUS_ACTIVE) {
                    $amountDiff = $this->amount - $package->amount;
                    $package->amount = $this->amount;
                    $package->package_id = $this->package_id;
                    if ($package->save(true, ['amount', 'package_id', 'updated_at'])) {
                        $token->status = CustomerToken::STATUS_USED;
                        if (!$token->save(true, ['status', 'updated_at'])) {
                            throw new \Exception(Yii::t('messages', "Token bị lỗi không thực hiện được yêu cầu này."));
                        }
                        //Lưu history
                        $packageHistory = new InvestCustomerPackageHistory();
                        $packageHistory->customer_package_id = $package->id;
                        $packageHistory->sign = InvestCustomerPackageHistory::SIGN_ADD;
                        $packageHistory->amount = $amountDiff;
                        $packageHistory->type = InvestCustomerPackageHistory::TYPE_UPGRADE;
                        if (!$packageHistory->save()) {
                            throw new Exception(Yii::t('messages', "Không cập nhật được gói đầu tư."));
                        }
                        //Trừ tiền trong ví
                        $balanceBefore = $wallet->balance;
                        $wallet->balance -= $amountDiff;
                        if ($wallet->save(true, ['balance', 'updated_at'])) {
                            //Tạo transaction
                            $customerTransaction = new InvestCustomerTransaction();
                            $customerTransaction->customer_id = Yii::$app->user->identity->id;
                            $customerTransaction->customer_username = Yii::$app->user->identity->username;
                            $customerTransaction->bill_id = $package->id;
                            $customerTransaction->amount = $amountDiff;
                            $customerTransaction->sign = InvestCustomerTransaction::SIGN_SUB;
                            $customerTransaction->type = InvestCustomerTransaction::TYPE_UPGRADE_PACKAGE;
                            $customerTransaction->balance_before = $balanceBefore;
                            $customerTransaction->balance_after = $wallet->balance;
                            if (!$customerTransaction->save()) {
                                throw new Exception(Yii::t('messages', "Không tạo được giao dịch"));
                            }
                            //+tiền cho nhánh trên
                            $savePoundage = Yii::$app->user->identity->addPoundageParent($amountDiff);
                            if (isset($savePoundage["code"]) && $savePoundage["code"] == 200) {
                                $transaction->commit();
                                $result = [
                                    "status" => "success",
                                    "error_code" => 1,
                                    "message" => ''
                                ];
                            }
                        } else {
                            throw new \Exception(Yii::t('messages', "Không cập nhật được gói đầu tư hiên tại."));

                        }
                    } else {
                        throw new \Exception(Yii::t('messages', "Không cập nhật được gói đầu tư hiện tại"));
                    }
                } elseif ($package->status == InvestCustomerPackage::STATUS_EMPTY) {
                    //validate số tiền nhạp vào
                    $query = InvestPackage::find();
                    $query->where(['status' => InvestPackage::STATUS_ACTIVE]);
                    $query->andFilterWhere([
                        'and',
                        ['<=', 'amount_min', $this->amount],
                        ['>=', 'amount_max', $this->amount]
                    ]);
                    $query->orderBy(['amount_min' => SORT_DESC]);
                    $query->limit(1);
                    $pack = $query->one();
                    if ($pack) {

                        $package->package_id = $pack->id;
                        $package->amount = $this->amount;
                        $package->status = InvestCustomerPackage::STATUS_ACTIVE;
                        $package->current_profit = 0;
                        $package->total_current_profit = 0;
//                        $package->joined_at = time();
                        $package->last_profit_receiver = time();
                        $package->last_cash_out_at = time();
                        if ($package->save(true, ['package_id', 'amount', 'status', 'current_profit', 'total_current_profit', 'last_profit_receiver', 'last_cash_out_at'])) {

                            $token->status = CustomerToken::STATUS_USED;
                            if (!$token->save(true, ['status', 'updated_at'])) {
                                throw new \Exception(Yii::t('messages', "Token bị lỗi không thực hiện được yêu cầu này."));
                            }
                            //trừ tiên trong wallet:
                            $packageHistory = new InvestCustomerPackageHistory();
                            $packageHistory->customer_package_id = $package->id;
                            $packageHistory->sign = InvestCustomerPackageHistory::SIGN_ADD;
                            $packageHistory->amount = $package->amount;
                            $packageHistory->type = InvestCustomerPackageHistory::TYPE_UPGRADE;
                            if (!$packageHistory->save()) {
                                throw new Exception(Yii::t('messages', "Không cập nhật được gói đầu tư hiện tại."));
                            }
                            //Trừ tiền trong ví
                            $balanceBefore = $wallet->balance;
                            $wallet->balance -= $package->amount;
                            if ($wallet->save(true, ['balance', 'updated_at'])) {
                                //Tạo transaction
                                $customerTransaction = new InvestCustomerTransaction();
                                $customerTransaction->customer_id = Yii::$app->user->identity->id;
                                $customerTransaction->customer_username = Yii::$app->user->identity->username;
                                $customerTransaction->bill_id = $package->id;
                                $customerTransaction->amount = $package->amount;
                                $customerTransaction->sign = InvestCustomerTransaction::SIGN_SUB;
                                $customerTransaction->type = InvestCustomerTransaction::TYPE_UPGRADE_PACKAGE;
                                $customerTransaction->balance_before = $balanceBefore;
                                $customerTransaction->balance_after = $wallet->balance;
                                if (!$customerTransaction->save()) {
                                    throw new Exception(Yii::t('messages', "Không tạo được giao dịch"));
                                }
                                //+tiền cho nhánh trên
                                $savePoundage = Yii::$app->user->identity->addPoundageParent($package->amount);
                                if (isset($savePoundage["code"]) && $savePoundage["code"] == 200) {
                                    $transaction->commit();
                                    $result = [
                                        "status" => "success",
                                        "error_code" => 1,
                                        "message" => ''
                                    ];
                                }
                            } else {
                                throw new \Exception(Yii::t('messages', "Số dư ví không được cập nhật."));
                            }
                        } else {
                            throw new \Exception(Yii::t('messages', "Giá trị gói đầu tư không được cập nhật."));
                        }

                    } else {
                        throw new \Exception(Yii::t('messages', "Số tiền nhập vào ko phù hợp với các gói có sẵn."));
                    }

                }
            }
                //Cập nhật gói hiện tại

        } catch (\Exception $ex) {
            $transaction->rollBack();
            $result = [
                "status" => "error",
                "error_code" => 100,
                "message" => $ex->getMessage()
            ];
        }

        return $result;
    }


}