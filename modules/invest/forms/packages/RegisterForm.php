<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 29-Jun-16
 * Time: 7:19 AM
 */
namespace app\modules\invest\forms\packages;

use app\models\CustomerPackage;
use app\models\CustomerRequest;
use app\models\investments\InvestCustomer;
use app\models\investments\InvestCustomerCase;
use app\models\investments\InvestCustomerPackage;
use app\models\investments\InvestCustomerPackageHistory;
use app\models\investments\InvestCustomerRequest;
use app\models\investments\InvestPackage;
use app\models\SystemEmailQueue;
use Yii;
use yii\base\Model;
use yii\db\Query;

class RegisterForm extends Model
{
    public $customer_id;
//    public $parent_id;
    public $amount;
    public $package_id;

    public function rules()
    {
        return [
//            [['ref_code'], 'required'],
//            [['ref_code'], 'string'],
//            [['ref_code'], 'ValidatorCustomerInvite'],
            [['customer_id', 'amount'], 'required'],
            [['amount'], 'number'],
            [['customer_id', 'package_id'], 'integer'],
            [['amount'], 'ValidatorAmount'],

        ];
    }

    public function ValidatorAmount($attribute, $params)
    {
        $query = InvestPackage::find();
        $query->where(['status' => InvestPackage::STATUS_ACTIVE]);
        $query->andFilterWhere([
            'and',
            ['<=', 'amount_min', $this->$attribute],
            ['>=', 'amount_max', $this->$attribute]
        ]);
        $query->orderBy(['amount_min' => SORT_DESC]);
        $query->limit(1);
        $package = $query->one();
        if ($package) {
            $this->package_id = $package->id;
        } else {
            $this->addError($attribute, Yii::t('messages', 'Số BTC nhập vào không đúng giá trị các gói đầu tư'));
        }
    }

//    public function ValidatorCustomerInvite($attribute, $params)
//    {
//        if ($this->$attribute) {
//            $customerParent = Yii::$app->user->identity->getParent();
//            if (!$customerParent) {
//                $this->addError($attribute, Yii::t('messages', 'Tài khoản của bạn bị lỗi mã giới thiệu. Vui lòng liên hệ với quản trị viên.'));
//            } else {
//                $this->parent_id = $customerParent->id;
//            }
//        }
//    }

    public function register()
    {
        $result = [];
        if ($this->validate()) {
            $wallet = Yii::$app->user->identity->getCurrentWallet();
            $customerPackage = new InvestCustomerPackage();
            $customerPackage->customer_id = Yii::$app->user->id;
            $customerPackage->package_id = $this->package_id;
            $customerPackage->amount = $this->amount;
            $customerPackage->status = InvestCustomerPackage::STATUS_ACTIVE;
            $customerPackage->is_active = InvestCustomerPackage::IS_ACTIVE_PENDING;
            $customerPackage->current_profit = 0;
            $customerPackage->total_current_profit = 0;
            $customerPackage->joined_at = time();
            $customerPackage->last_profit_receiver = time();
            $customerPackage->last_cash_out_at = time();
            $customerPackage->last_cash_out_poundage = time();

            $transaction = Yii::$app->db->beginTransaction();
            try {
                //Tao Pakage
                if ($customerPackage->save()) {
                    //Tao Package History
                    $packHis = new InvestCustomerPackageHistory();
                    $packHis->customer_package_id = $customerPackage->id;
                    $packHis->sign = InvestCustomerPackageHistory::SIGN_ADD;
                    $packHis->amount = 0;
                    $packHis->type = InvestCustomerPackageHistory::TYPE_OPEN_PACKAGE;
                    if (!$packHis->save()) {
                        throw new \Exception(Yii::t('messages', "Không tạo được bản ghi lịch sử gói đầu tư."));
                    }
                    //Tao request nop tien
                    $customerRequest = new InvestCustomerRequest();
                    $customerRequest->customer_id = Yii::$app->user->identity->id;
                    $customerRequest->customer_username = Yii::$app->user->identity->username;
                    $customerRequest->amount = $customerPackage->amount;
                    $customerRequest->bank_address = Yii::$app->params["invest_bit_coin_address"];
                    $customerRequest->status = InvestCustomerRequest::STATUS_CREATED;
                    $customerRequest->type = InvestCustomerRequest::TYPE_LOAN_OPEN_PACKAGE;
                    if ($customerRequest->save()) {
                        $transaction->commit();
                        $params = $customerPackage->getAttributes(['id', 'amount']);
                        $params["username"] = Yii::$app->user->identity->username;
                        SystemEmailQueue::createEmailQueue(
                            SystemEmailQueue::TYPE_INVEST_OPEN_PACKAGE,
                            Yii::$app->user->identity->email,
                            '[INVESTOR] Open package completed',
                            'investor-open-package-completed',
                            $params
                        );
                        return [
                            "status" => "success",
                            "code" => 200,
                            "message" => '',
                            "requestId" => $customerRequest->id
                        ];
//                        $result = $this->addInvestCustomerCase();
//                        if (isset($result["code"]) && (int)$result["code"] == 200) {

//                            $message = \Yii::$app->mailer->compose('test');
//                            $result_code = $message->setFrom([Yii::$app->params["emailSend"] => 'BITONESS.COM'])
//                                ->setTo(Yii::$app->user->identity->email)
//                                ->setSubject('bitoness.com - Your online account register has been successfull')
//                                ->send();
//                            return $result;
//                        }
                    } else {
                        throw new \Exception(Yii::t('messages', 'Không tạo được bản ghi đăng ký'));
                    }
                } else {
                    throw new \Exception(Yii::t('messages', 'Không tạo được gói đầu tư'));
                }
            } catch (\Exception $ex) {
                $transaction->rollBack();
                return [
                    "status" => "error",
                    "code" => 400,
                    "message" => $ex->getMessage()
                ];
            }
        }
        return $result;
    }



}