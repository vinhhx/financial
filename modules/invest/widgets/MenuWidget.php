<?php

/**
 * Created by PhpStorm.
 * User: Quan
 * Date: 8/27/2015
 * Time: 2:22 PM
 */

namespace app\modules\invest\widgets;

use Yii;
use yii\base\Widget;
use yii\db\mysql\Schema;
use yii\helpers\Url;

class MenuWidget extends Widget
{

    public function run()
    {

        $config = [
            [
                'label' => '<i class="bit bit-dashboard"></i>&nbsp;' . Yii::t('systems', 'Trang chủ'),
                'url' => ['default/index'],
            ],

            [

                'label' => '<i class="bit bit-account"></i>&nbsp;' . Yii::t('systems', 'Tài khoản'),
                'url' => null,
                'children' => [
//                    [
//                        'label' => '<i class="bit bit-wallet"></i>&nbsp;' . Yii::t('customers', 'My Bank Account'),
//                        'url' => ['my-account/my-bank'],
//                    ],

                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('systems', 'My bitcoin wallet address'),
                        'url' => ['my-account/my-bank'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('systems', 'Danh sách Token'),
                        'url' => ['my-account/token'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('systems', 'Yêu cầu'),
                        'url' => ['my-request/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('systems', 'Lịch sử gói đầu tư'),
                        'url' => ['my-package/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('systems', 'Giao dịch'),
                        'url' => ['my-transaction/index'],
                    ],
                ]
            ],
            [
                'label' => '<i class="bit bit-balance"></i>&nbsp;' . Yii::t('customers', 'Token'),
                'url' => null,
                'children' => [
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('customers', 'Token List'),
                        'url' => ['customer-token/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('customers', 'Transaction History'),
                        'url' => ['customer-token/transaction-history'],
                    ],


                ]
            ],

            [
                'label' => '<i class="bit bit-member"></i>&nbsp;' . Yii::t('systems', 'Genealogy'),
                'url' => null,
                'children' => [
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('systems', 'My member'),
                        'url' => ['my-account/member'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('systems', 'Member list'),
                        'url' => ['invest-customer-case/index'],
                    ],
                ]
            ],
            [
                'label' => '<i class="bit bit-sign-out"></i>&nbsp;' . Yii::t('systems', 'Đăng xuất'),
                'url' => ['default/logout'],
            ],
            /*[
                'label' => '<i class="bit bit-support"></i>&nbsp;' . Yii::t('systems', 'Hỗ trợ'),
                'url' => ['default/index'],
            ],*/

        ];

        // Current Module
//        $prefix = Yii::$app->controller->module->uniqueId;

        // current action
        $action = '/' . Yii::$app->controller->action->uniqueId;

        $parentAction = isset($this->getView()->params["parentAction"]) ? $this->getView()->params["parentAction"] : "";

        // Remove wallet tránh lỗi URL
        foreach ($config as $k => $cfg) {
            if (isset($cfg['children'])) {
                foreach ($cfg['children'] as $k1 => $child) {
                    if (isset($child['url']) && $child['url']) {
//                        $child['url'][0] = $prefix . $child['url'][0];
                        $prefix = '/customer/' . $child['url'][0];
                        $child['url'] = Url::to($child['url']);
                        // Menu đang được chọn
                        if ($prefix == $action || $prefix == $parentAction) {
                            $cfg['selected'] = true;
                            $child['selected'] = true;
                        }
                        //
                        $cfg['children'][$k1] = $child;
                    }
                }
            }
            if (isset($cfg['url']) && $cfg['url']) {
//                $cfg['url'][0] = $prefix . $cfg['url'][0];
                $cfg['url'] = Url::to($cfg['url']);
                if ($cfg['url'] == $action || $cfg['url'] == $parentAction) {
                    $cfg['selected'] = true;
                }
            }
            $config[$k] = $cfg;
        }

        return $this->render('menu-widget', [
            'currentController' => Yii::$app->controller->id,
            'config' => $config
        ]);
    }

}
