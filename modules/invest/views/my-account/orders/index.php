<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\forms\search\CustomerBankAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My order list');
$this->params['breadcrumbs'][$this->title] = \yii\helpers\Url::to(['my-account/order-index']);
$this->params['pageTitle'] = $this->title;
?>
<div class="customer-bank-address-index">

    <!--    --><?php //echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box box-customer">
        <div class="box-header">
            <?= Html::a('<i class="fa fa-plus-square-o"></i>&nbsp;' . Yii::t('app', 'Buy Token'), ['buy-token'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'grid-view grid-view-customize'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'type',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getTypeLabel();
                }

            ],
            'quantity',
            'created_at:datetime',
            'upload_attachment_at:datetime',
            [
                'attribute' => 'upload_attachment_at',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->type == \app\models\CustomerOrder::TYPE_BUY_TOKEN) {
                        return Yii::$app->formatter->asDatetime($model->upload_attachment_at);
                    } else {
                        return Yii::$app->formatter->asDatetime($model->created_at);
                    }
                }

            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getStatusLabel();
                }

            ],
            [
                'attribute' => 'attachment',
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->attachment && $model->type == \app\models\CustomerOrder::TYPE_BUY_TOKEN):
                        return Html::a(Html::img(Yii::$app->image->getImg($model->attachment, '100x100'), ['class' => 'image image-responsive']), Yii::$app->image->getImg($model->attachment), ['target' => '_blank']);
                    else:
                        return $model->attachment;
                    endif;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{checkout}',
                'buttons' => [
                    'checkout' => function ($url, $model) {
                        if ($model->status == \app\models\CustomerOrder::STATUS_PENDING && $model->type==\app\models\CustomerOrder::TYPE_BUY_TOKEN):
                            return Html::a('<span class="fa fa-shopping-cart"></span>&nbsp;' . Yii::t('app', 'Check out'), ['check-out', 'id' => $model->id], [
                                'class' => 'btn btn-warning btn-xs',
                            ]);
                        else:
                            return Html::a('<span class="fa fa-eye"></span>&nbsp;' . Yii::t('app', 'Detail'), ['check-out', 'id' => $model->id], [
                                'class' => 'btn btn-primary btn-xs',
                            ]);
                        endif;
                    },
//                    'delete' => function ($url, $model, $key) {
//                        return Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
//                            'class' => 'btn btn-danger btn-xs',
//                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
//                            'data-method' => 'post',
//                            'data-pjax' => '0',
//                        ]);
//                    }
                ]
            ],
        ],

    ]); ?>
</div>
