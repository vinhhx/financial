<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\forms\search\CustomerBankAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My Members');
$this->params['breadcrumbs']["Account"] = \yii\helpers\Url::to(['my-account/index']);
$this->params['pageTitle'] = $this->title;
?>
<div class="list-members">
    <div class="tree">

        <div class="middle">
            <table id="btree" class="btree" cellspacing="0" cellpadding="0">
                <tbody>
                <?php if (isset($model[1]["id"]) && $model[1]["id"] !== Yii::$app->user->identity->id): ?>
                    <tr>
                        <td colspan="4" width="100%" style="text-align:center; padding-bottom: 10px;">
                            <a href="<?= Url::to(['my-account/member']) ?>"><img src="/images/icon-member/arrow-t-d.png"
                                                                                 border="0"></a>
                            <a href="<?= Url::to(['my-account/member', 'next' => $model[1]["parent_id"]]) ?>"><img
                                    src="/images/icon-member/arrow-u-d.png" border="0"
                                    style="margin-top: 11px; margin-left: 5px;"></a>
                        </td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <?php
                    $lvl1 = isset($model[1]) ? $model[1] : [];
                    $name1 = isset($lvl1["username"]) ? $lvl1["username"] : '';
                    $amount1 = isset($lvl1["amount"]) ? Yii::$app->formatter->asDecimal($lvl1["amount"], 8) : '';
                    $childrenNumber1=isset($lvl1["total_children"]) ? Yii::$app->formatter->asInteger($lvl1["total_children"]) :0;
                    ?>
                    <td colspan="2" width="100%" style="text-align:center;">
                        <a class="tree_user" href="javascript:void(0)"><img alt="test1" class="i_user"
                                                                            src="/images/flags/i_user.png"><br><?= $name1.Html::tag('span',' ('.$childrenNumber1.')',['class'=>'number-children text-blue text-bold']) ?>
                            <br>
                            <span class="text-red">[<?= $amount1 ?>]</span>
                        </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" width="100%">
                        <table id="xbtree" class="btree" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                                <td width="25%">
                                    <div class="btree_bnum_left">&nbsp;</div>
                                </td>
                                <td width="25%" style="border-right:1px solid #C7C8C9;">
                                    <?php
                                    if(!empty($model[2][1])):
                                        $totalChildren=isset($model[2][1]["total_children"])?((int)$model[2][1]["total_children"]+1):1;
                                     echo  Html::tag('span',$totalChildren,['class'=>'leg-left']);
                                    endif;
                                    ?>
                                </td>
                                <td width="25%">
                                    <?php
                                    if(!empty($model[2][2])):
                                        $totalChildren=isset($model[2][2]["total_children"])?((int)$model[2][2]["total_children"]+1):1;
                                        echo  Html::tag('span',$totalChildren,['class'=>'leg-right']);
                                    endif;
                                    ?>

                                </td>
                                <td width="25%">
                                    <div class="btree_bnum_right">&nbsp;</div>
                                </td>
                            </tr>
                            <tr>
                                <td width="25%">&nbsp;</td>
                                <td width="25%" style="border-top:1px solid #C7C8C9; border-left:1px solid #C7C8C9;">
                                    &nbsp;</td>
                                <td width="25%" style="border-top:1px solid #C7C8C9; border-right:1px solid #C7C8C9;">
                                    &nbsp;</td>
                                <td width="25%">&nbsp;</td>
                            </tr>
                            <tr>
                                <?php
                                $lvl21 = isset($model[2][1]) ? $model[2][1] : [];
                                $name21 = isset($lvl21["username"]) ? $lvl21["username"] : '';
                                $amount21 = isset($lvl21["amount"]) ? Yii::$app->formatter->asDecimal($lvl21["amount"], 8) : '';
                                $childrenNumber21=isset($lvl21["total_children"]) ? Yii::$app->formatter->asInteger($lvl21["total_children"]) :0;
                                $lvl22 = isset($model[2][2]) ? $model[2][2] : [];
                                $name22 = isset($lvl22["username"]) ? $lvl22["username"] : '';
                                $amount22 = isset($lvl22["amount"]) ? Yii::$app->formatter->asDecimal($lvl22["amount"], 8) : '';
                                $childrenNumber22=isset($lvl22["total_children"]) ? Yii::$app->formatter->asInteger($lvl22["total_children"]) :0;
                                ?>
                                <td width="50%" colspan="2" style="text-align:center;" valign="top">
                                    <a class="tree_user" href="javascript:void(0)">

                                        <?php if ($lvl21): ?>
                                            <img alt="test1"
                                                 class="i_user"
                                                 src="/images/flags/i_user.png"><br><?= $name21.Html::tag('span',' ('.$childrenNumber21.')',['class'=>'number-children text-blue text-bold']) ?>
                                            <br>
                                            <span class="text-red">[<?= $amount21 ?>]</span>
                                        <?php endif; ?>
                                    </a>
                                </td>
                                <td width="50%" colspan="2" style="text-align:center;" valign="top">
                                    <a class="tree_user" href="javascript:void(0)">
                                        <?php if ($lvl22): ?>
                                            <img alt="test1"
                                                 class="i_user"
                                                 src="/images/flags/i_user.png"><br><?= $name22.Html::tag('span',' ('.$childrenNumber22.')',['class'=>'number-children text-blue text-bold']) ?>
                                            <br>
                                            <span class="text-red">[<?= $amount22 ?>]</span>
                                        <?php endif; ?>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td width="50%" colspan="2" valign="top">
                                    <table id="xbtree" class="btree" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td width="25%">
                                                <div class="btree_bnum_left">&nbsp;</div>
                                            </td>
                                            <td width="25%" style="border-right:1px solid #C7C8C9;">
                                                <?php
                                                if(!empty($model[3][11])):
                                                    $totalChildren=isset($model[3][11]["total_children"])?((int)$model[3][11]["total_children"]+1):1;
                                                    echo  Html::tag('span',$totalChildren,['class'=>'leg-left']);
                                                endif;
                                                ?>

                                            </td>
                                            <td width="25%">
                                                <?php
                                                if(!empty($model[3][12])):
                                                    $totalChildren=isset($model[3][12]["total_children"])?((int)$model[3][12]["total_children"]+1):1;
                                                    echo  Html::tag('span',$totalChildren,['class'=>'leg-right']);
                                                endif;
                                                ?>
                                            </td>
                                            <td width="25%">
                                                <div class="btree_bnum_right">&nbsp;</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="25%">&nbsp;</td>
                                            <td width="25%"
                                                style="border-top:1px solid #C7C8C9; border-left:1px solid #C7C8C9;">
                                                &nbsp;</td>
                                            <td width="25%"
                                                style="border-top:1px solid #C7C8C9; border-right:1px solid #C7C8C9;">
                                                &nbsp;</td>
                                            <td width="25%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <?php
                                            $lvl311 = isset($model[3][11]) ? $model[3][11] : [];
                                            $name311 = isset($lvl311["username"]) ? $lvl311["username"] : '';
                                            $amount311 = isset($lvl311["amount"]) ? Yii::$app->formatter->asDecimal($lvl311["amount"], 8) : '';
                                            $childrenNumber311=isset($lvl311["total_children"]) ? Yii::$app->formatter->asInteger($lvl311["total_children"]) :0;
                                            $lvl312 = isset($model[3][12]) ? $model[3][12] : [];
                                            $name312 = isset($lvl312["username"]) ? $lvl312["username"] : '';
                                            $amount312 = isset($lvl312["amount"]) ? Yii::$app->formatter->asDecimal($lvl312["amount"], 8) : '';
                                            $childrenNumber312=isset($lvl312["total_children"]) ? Yii::$app->formatter->asInteger($lvl312["total_children"]) :0;
                                            ?>
                                            <td width="50%" colspan="2" style="text-align:center;" valign="top">
                                                <a class="tree_user" href="javascript:void(0)">
                                                    <?php if ($lvl311): ?>
                                                        <img alt="test1"
                                                             class="i_user"
                                                             src="/images/flags/i_user.png"><br><?= $name311.Html::tag('span',' ('.$childrenNumber311.')',['class'=>'number-children text-blue text-bold']) ?>
                                                        <br>
                                                        <span class="text-red">[<?= $amount311 ?>]</span>
                                                    <?php endif; ?>
                                                </a>
                                            </td>
                                            <td width="50%" colspan="2" style="text-align:center;" valign="top">
                                                <a class="tree_user" href="javascript:void(0)">
                                                    <?php if ($lvl312): ?>
                                                        <img alt="test1"
                                                             class="i_user"
                                                             src="/images/flags/i_user.png"><br><?= $name312.Html::tag('span',' ('.$childrenNumber312.')',['class'=>'number-children text-blue text-bold']) ?>
                                                        <br>
                                                        <span class="text-red">[<?= $amount312 ?>]</span>
                                                    <?php endif; ?>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="50%" colspan="2" valign="top">
                                                <table id="xbtree" class="btree" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                    <tr>
                                                        <td width="25%">
                                                            <div class="btree_bnum_left">&nbsp;</div>
                                                        </td>
                                                        <td width="25%" style="border-right:1px solid #C7C8C9;">
                                                            <?php
                                                            if(!empty($model[4][111])):
                                                                $totalChildren=isset($model[4][111]["total_children"])?((int)$model[4][111]["total_children"]+1):1;
                                                                echo  Html::tag('span',$totalChildren,['class'=>'leg-left']);
                                                            endif;
                                                            ?>
                                                        </td>
                                                        <td width="25%">
                                                            <?php
                                                            if(!empty($model[4][112])):
                                                                $totalChildren=isset($model[4][112]["total_children"])?((int)$model[4][112]["total_children"]+1):1;
                                                                echo  Html::tag('span',$totalChildren,['class'=>'leg-right']);
                                                            endif;
                                                            ?>
                                                        </td>
                                                        <td width="25%">
                                                            <div class="btree_bnum_right">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%">&nbsp;</td>
                                                        <td width="25%"
                                                            style="border-top:1px solid #C7C8C9; border-left:1px solid #C7C8C9;">
                                                            &nbsp;</td>
                                                        <td width="25%"
                                                            style="border-top:1px solid #C7C8C9; border-right:1px solid #C7C8C9;">
                                                            &nbsp;</td>
                                                        <td width="25%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <?php
                                                        $lvl4111 = isset($model[4][111]) ? $model[4][111] : [];
                                                        $name4111 = isset($lvl4111["username"]) ? $lvl4111["username"] : '';
                                                        $amount4111 = isset($lvl4111["amount"]) ? Yii::$app->formatter->asDecimal($lvl4111["amount"], 8) : '';
                                                        $childrenNumber4111=isset($lvl4111["total_children"]) ? Yii::$app->formatter->asInteger($lvl4111["total_children"]) :0;
                                                        $lvl4112 = isset($model[4][112]) ? $model[4][112] : [];
                                                        $name4112 = isset($lvl4112["username"]) ? $lvl4112["username"] : '';
                                                        $amount4112 = isset($lvl4112["amount"]) ? Yii::$app->formatter->asDecimal($lvl4112["amount"], 8) : '';
                                                        $childrenNumber4112=isset($lvl4112["total_children"]) ? Yii::$app->formatter->asInteger($lvl4112["total_children"]) :0;
                                                        ?>
                                                        <td width="50%" colspan="2" style="text-align:center;"
                                                            valign="top">
                                                            <a class="tree_user" href="javascript:void(0)">
                                                                <?php if ($lvl4111): ?>
                                                                    <img alt="test1"
                                                                         class="i_user"
                                                                         src="/images/flags/i_user.png">
                                                                    <br><?= $name4111.Html::tag('span',' ('.$childrenNumber4111.')',['class'=>'number-children text-blue text-bold']) ?>
                                                                    <br>
                                                                    <span class="text-red">[<?= $amount4111 ?>]</span>
                                                                <?php endif; ?>
                                                            </a>
                                                            <?php if (isset($lvl4111["childrens"])): ?>
                                                                <br/>
                                                                <a class="next-member"
                                                                   href="<?= Url::to(['my-account/member', 'next' => $lvl21["id"]]) ?>">
                                                                    <img title="Next"
                                                                         src="/images/icon-member/arrow-down-active.png"
                                                                         border="0" style="margin-top: -10px;">
                                                                </a>
                                                            <?php endif ?>
                                                        </td>
                                                        <td width="50%" colspan="2" style="text-align:center;"
                                                            valign="top">
                                                            <a class="tree_user" href="javascript:void(0)">

                                                                <?php if ($lvl4112): ?>
                                                                    <img alt="test1"
                                                                         class="i_user"
                                                                         src="/images/flags/i_user.png">
                                                                    <br><?= $name4112.Html::tag('span',' ('.$childrenNumber4112.')',['class'=>'number-children text-blue text-bold']) ?>
                                                                    <br>
                                                                    <span class="text-red">[<?= $amount4112 ?>]</span>
                                                                <?php endif; ?>
                                                            </a>
                                                            <?php if (isset($lvl4112["childrens"])): ?>
                                                                <br/>
                                                                <a class="next-member"
                                                                   href="<?= Url::to(['my-account/member', 'next' => $lvl21["id"]]) ?>">
                                                                    <img title="Next"
                                                                         src="/images/icon-member/arrow-down-active.png"
                                                                         border="0" style="margin-top: -10px;">
                                                                </a>
                                                            <?php endif ?>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="50%" colspan="2" valign="top">
                                                <table id="xbtree" class="btree" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                    <tr>
                                                        <td width="25%">
                                                            <div class="btree_bnum_left">&nbsp;</div>
                                                        </td>
                                                        <td width="25%" style="border-right:1px solid #C7C8C9;">
                                                            <?php
                                                            if(!empty($model[4][121])):
                                                                $totalChildren=isset($model[4][121]["total_children"])?((int)$model[4][121]["total_children"]+1):1;
                                                                echo  Html::tag('span',$totalChildren,['class'=>'leg-left']);
                                                            endif;
                                                            ?>
                                                        </td>
                                                        <td width="25%">
                                                            <?php
                                                            if(!empty($model[4][122])):
                                                                $totalChildren=isset($model[4][122]["total_children"])?((int)$model[4][122]["total_children"]+1):1;
                                                                echo  Html::tag('span',$totalChildren,['class'=>'leg-right']);
                                                            endif;
                                                            ?>

                                                        </td>
                                                        <td width="25%">
                                                            <div class="btree_bnum_right">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%">&nbsp;</td>
                                                        <td width="25%"
                                                            style="border-top:1px solid #C7C8C9; border-left:1px solid #C7C8C9;">
                                                            &nbsp;</td>
                                                        <td width="25%"
                                                            style="border-top:1px solid #C7C8C9; border-right:1px solid #C7C8C9;">
                                                            &nbsp;</td>
                                                        <td width="25%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <?php
                                                        $lvl4121 = isset($model[4][121]) ? $model[4][121] : [];
                                                        $name4121 = isset($lvl4121["username"]) ? $lvl4121["username"] : '';
                                                        $amount4121 = isset($lvl4121["amount"]) ? Yii::$app->formatter->asDecimal($lvl4121["amount"], 8) : '';
                                                        $childrenNumber4121=isset($lvl4121["total_children"]) ? Yii::$app->formatter->asInteger($lvl4121["total_children"]) :0;
                                                        $lvl4122 = isset($model[4][122]) ? $model[4][122] : [];
                                                        $name4122 = isset($lvl4122["username"]) ? $lvl4122["username"] : '';
                                                        $amount4122 = isset($lvl4122["amount"]) ? Yii::$app->formatter->asDecimal($lvl4122["amount"], 8) : '';
                                                        $childrenNumber4122=isset($lvl4122["total_children"]) ? Yii::$app->formatter->asInteger($lvl4122["total_children"]) :0;
                                                        ?>
                                                        <td width="50%" colspan="2" style="text-align:center;"
                                                            valign="top">
                                                            <a class="tree_user" href="javascript:void(0)">
                                                                <?php if ($lvl4121): ?>
                                                                    <img alt="test1"
                                                                         class="i_user"
                                                                         src="/images/flags/i_user.png">
                                                                    <br><?= $name4121.Html::tag('span',' ('.$childrenNumber4121.')',['class'=>'number-children text-blue text-bold']) ?>
                                                                    <br>
                                                                    <span class="text-red">[<?= $amount4121 ?>]</span>
                                                                <?php endif; ?>
                                                            </a>
                                                            <?php if (isset($lvl4121["childrens"])): ?>
                                                                <br/>
                                                                <a class="next-member"
                                                                   href="<?= Url::to(['my-account/member', 'next' => $lvl21["id"]]) ?>">
                                                                    <img title="Next"
                                                                         src="/images/icon-member/arrow-down-active.png"
                                                                         border="0" style="margin-top: -10px;">
                                                                </a>
                                                            <?php endif ?>
                                                        </td>
                                                        <td width="50%" colspan="2" style="text-align:center;"
                                                            valign="top">
                                                            <a class="tree_user" href="javascript:void(0)">
                                                                <?php if ($lvl4122): ?>
                                                                    <img alt="test1"
                                                                         class="i_user"
                                                                         src="/images/flags/i_user.png">
                                                                    <br><?= $name4122.Html::tag('span',' ('.$childrenNumber4122.')',['class'=>'number-children text-blue text-bold']) ?>
                                                                    <br>
                                                                    <span class="text-red">[<?= $amount4122 ?>]</span>
                                                                <?php endif; ?>
                                                            </a>
                                                            <?php if (isset($lvl4122["childrens"])): ?>
                                                                <br/>
                                                                <a class="next-member"
                                                                   href="<?= Url::to(['my-account/member', 'next' => $lvl21["id"]]) ?>">
                                                                    <img title="Next"
                                                                         src="/images/icon-member/arrow-down-active.png"
                                                                         border="0" style="margin-top: -10px;">
                                                                </a>
                                                            <?php endif ?>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="50%" colspan="2" valign="top">
                                    <table id="xbtree" class="btree" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td width="25%">
                                                <div class="btree_bnum_left">&nbsp;</div>
                                            </td>
                                            <td width="25%" style="border-right:1px solid #C7C8C9;">
                                                <?php
                                                if(!empty($model[3][21])):
                                                    $totalChildren=isset($model[3][21]["total_children"])?((int)$model[3][21]["total_children"]+1):1;
                                                    echo  Html::tag('span',$totalChildren,['class'=>'leg-left']);
                                                endif;
                                                ?>
                                            </td>
                                            <td width="25%">
                                                <?php
                                                if(!empty($model[3][22])):
                                                    $totalChildren=isset($model[3][22]["total_children"])?((int)$model[3][22]["total_children"]+1):1;
                                                    echo  Html::tag('span',$totalChildren,['class'=>'leg-right']);
                                                endif;
                                                ?>
                                            </td>
                                            <td width="25%">
                                                <div class="btree_bnum_right">&nbsp;</div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="25%">&nbsp;</td>
                                            <td width="25%"
                                                style="border-top:1px solid #C7C8C9; border-left:1px solid #C7C8C9;">
                                                &nbsp;</td>
                                            <td width="25%"
                                                style="border-top:1px solid #C7C8C9; border-right:1px solid #C7C8C9;">
                                                &nbsp;</td>
                                            <td width="25%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <?php
                                            $lvl321 = isset($model[3][21]) ? $model[3][21] : [];
                                            $name321 = isset($lvl321["username"]) ? $lvl321["username"] : '';
                                            $amount321 = isset($lvl321["amount"]) ? Yii::$app->formatter->asDecimal($lvl321["amount"], 8) : '';
                                            $childrenNumber321=isset($lvl321["total_children"]) ? Yii::$app->formatter->asInteger($lvl321["total_children"]) :0;
                                            $lvl322 = isset($model[3][22]) ? $model[3][22] : [];
                                            $name322 = isset($lvl322["username"]) ? $lvl322["username"] : '';
                                            $amount322 = isset($lvl322["amount"]) ? Yii::$app->formatter->asDecimal($lvl322["amount"], 8) : '';
                                            $childrenNumber322=isset($lvl322["total_children"]) ? Yii::$app->formatter->asInteger($lvl322["total_children"]) :0;
                                            ?>
                                            <td width="50%" colspan="2" style="text-align:center;"
                                                valign="top">
                                                <a class="tree_user" href="javascript:void(0)">
                                                    <?php if ($lvl321): ?>
                                                        <img alt="test1"
                                                             class="i_user"
                                                             src="/images/flags/i_user.png">
                                                        <br><?= $name321.Html::tag('span',' ('.$childrenNumber321.')',['class'=>'number-children text-blue text-bold']) ?>
                                                        <br>
                                                        <span class="text-red">[<?= $amount321 ?>]</span>
                                                    <?php endif; ?>
                                                </a>
                                            </td>
                                            <td width="50%" colspan="2" style="text-align:center;"
                                                valign="top">
                                                <a class="tree_user" href="javascript:void(0)">
                                                    <?php if ($lvl322): ?>
                                                        <img alt="test1"
                                                             class="i_user"
                                                             src="/images/flags/i_user.png">
                                                        <br><?= $name322.Html::tag('span',' ('.$childrenNumber322.')',['class'=>'number-children text-blue text-bold']) ?>
                                                        <br>
                                                        <span class="text-red">[<?= $amount322 ?>]</span>
                                                    <?php endif; ?>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="50%" colspan="2" valign="top">
                                                <table id="xbtree" class="btree" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                    <tr>
                                                        <td width="25%">
                                                            <div class="btree_bnum_left">&nbsp;</div>
                                                        </td>
                                                        <td width="25%" style="border-right:1px solid #C7C8C9;">
                                                            <?php
                                                            if(!empty($model[4][211])):
                                                                $totalChildren=isset($model[4][211]["total_children"])?((int)$model[4][211]["total_children"]+1):1;
                                                                echo  Html::tag('span',$totalChildren,['class'=>'leg-left']);
                                                            endif;
                                                            ?>
                                                        </td>
                                                        <td width="25%">
                                                            <?php
                                                            if(!empty($model[4][212])):
                                                                $totalChildren=isset($model[4][212]["total_children"])?((int)$model[4][212]["total_children"]+1):1;
                                                                echo  Html::tag('span',$totalChildren,['class'=>'leg-right']);
                                                            endif;
                                                            ?>
                                                        </td>
                                                        <td width="25%">
                                                            <div class="btree_bnum_right">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%">&nbsp;</td>
                                                        <td width="25%"
                                                            style="border-top:1px solid #C7C8C9; border-left:1px solid #C7C8C9;">
                                                            &nbsp;</td>
                                                        <td width="25%"
                                                            style="border-top:1px solid #C7C8C9; border-right:1px solid #C7C8C9;">
                                                            &nbsp;</td>
                                                        <td width="25%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <?php
                                                        $lvl4211 = isset($model[4][211]) ? $model[4][211] : [];
                                                        $name4211 = isset($lvl4211["username"]) ? $lvl4211["username"] : '';
                                                        $amount4211 = isset($lvl4211["amount"]) ? Yii::$app->formatter->asDecimal($lvl4211["amount"], 8) : '';
                                                        $childrenNumber4211=isset($lvl4211["total_children"]) ? Yii::$app->formatter->asInteger($lvl4211["total_children"]) :0;
                                                        $lvl4212 = isset($model[4][212]) ? $model[4][212] : [];
                                                        $name4212 = isset($lvl4212["username"]) ? $lvl4212["username"] : '';
                                                        $amount4212 = isset($lvl4212["amount"]) ? Yii::$app->formatter->asDecimal($lvl4212["amount"], 8) : '';
                                                        $childrenNumber4212=isset($lvl4212["total_children"]) ? Yii::$app->formatter->asInteger($lvl4212["total_children"]) :0;
                                                        ?>
                                                        <td width="50%" colspan="2" style="text-align:center;"
                                                            valign="top">
                                                            <a class="tree_user" href="javascript:void(0)">
                                                                <?php if ($lvl4211): ?>
                                                                    <img alt="test1"
                                                                         class="i_user"
                                                                         src="/images/flags/i_user.png">
                                                                    <br><?= $name4211.Html::tag('span',' ('.$childrenNumber4211.')',['class'=>'number-children text-blue text-bold']) ?>
                                                                    <br>
                                                                    <span class="text-red">[<?= $amount4211 ?>]</span>
                                                                <?php endif; ?>
                                                            </a>
                                                            <?php if (isset($lvl4211["childrens"])): ?>
                                                                <br/>
                                                                <a class="next-member"
                                                                   href="<?= Url::to(['my-account/member', 'next' => $lvl22["id"]]) ?>">
                                                                    <img title="Next"
                                                                         src="/images/icon-member/arrow-down-active.png"
                                                                         border="0" style="margin-top: -10px;">
                                                                </a>
                                                            <?php endif ?>
                                                        </td>
                                                        <td width="50%" colspan="2" style="text-align:center;"
                                                            valign="top">
                                                            <a class="tree_user" href="javascript:void(0)">
                                                                <?php if ($lvl4212): ?>
                                                                    <img alt="test1"
                                                                         class="i_user"
                                                                         src="/images/flags/i_user.png">
                                                                    <br><?= $name4212 .Html::tag('span',' ('.$childrenNumber4212.')',['class'=>'number-children text-blue text-bold'])?>
                                                                    <br>
                                                                    <span class="text-red">[<?= $amount4212 ?>]</span>
                                                                <?php endif; ?>
                                                            </a>
                                                            <?php if (isset($lvl4212["childrens"])): ?>
                                                                <br/>
                                                                <a class="next-member"
                                                                   href="<?= Url::to(['my-account/member', 'next' => $lvl22["id"]]) ?>">
                                                                    <img title="Next"
                                                                         src="/images/icon-member/arrow-down-active.png"
                                                                         border="0" style="margin-top: -10px;">
                                                                </a>
                                                            <?php endif ?>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="50%" colspan="2" valign="top">
                                                <table id="xbtree" class="btree" cellspacing="0" cellpadding="0">
                                                    <tbody>
                                                    <tr>
                                                        <td width="25%">
                                                            <div class="btree_bnum_left">&nbsp;</div>
                                                        </td>
                                                        <td width="25%" style="border-right:1px solid #C7C8C9;">
                                                            <?php
                                                            if(!empty($model[4][221])):
                                                                $totalChildren=isset($model[4][221]["total_children"])?((int)$model[4][221]["total_children"]+1):1;
                                                                echo  Html::tag('span',$totalChildren,['class'=>'leg-left']);
                                                            endif;
                                                            ?>
                                                        </td>
                                                        <td width="25%">
                                                            <?php
                                                            if(!empty($model[4][222])):
                                                                $totalChildren=isset($model[4][222]["total_children"])?((int)$model[4][222]["total_children"]+1):1;
                                                                echo  Html::tag('span',$totalChildren,['class'=>'leg-right']);
                                                            endif;
                                                            ?>
                                                        </td>
                                                        <td width="25%">
                                                            <div class="btree_bnum_right">&nbsp;</div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td width="25%">&nbsp;</td>
                                                        <td width="25%"
                                                            style="border-top:1px solid #C7C8C9; border-left:1px solid #C7C8C9;">
                                                            &nbsp;</td>
                                                        <td width="25%"
                                                            style="border-top:1px solid #C7C8C9; border-right:1px solid #C7C8C9;">
                                                            &nbsp;</td>
                                                        <td width="25%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <?php
                                                        $lvl4221 = isset($model[4][221]) ? $model[4][221] : [];
                                                        $name4221 = isset($lvl4221["username"]) ? $lvl4221["username"] : '';
                                                        $amount4221 = isset($lvl4221["amount"]) ? Yii::$app->formatter->asDecimal($lvl4221["amount"], 8) : '';
                                                        $childrenNumber4221=isset($lvl4221["total_children"]) ? Yii::$app->formatter->asInteger($lvl4221["total_children"]) :0;
                                                        $lvl4222 = isset($model[4][222]) ? $model[4][222] : [];
                                                        $name4222 = isset($lvl4222["username"]) ? $lvl4222["username"] : '';
                                                        $amount4222 = isset($lvl4222["amount"]) ? Yii::$app->formatter->asDecimal($lvl4222["amount"], 8) : '';
                                                        $childrenNumber4222=isset($lvl4222["total_children"]) ? Yii::$app->formatter->asInteger($lvl4222["total_children"]) :0;
                                                        ?>
                                                        <td width="50%" colspan="2" style="text-align:center;"
                                                            valign="top">
                                                            <a class="tree_user" href="javascript:void(0)">
                                                                <?php if ($lvl4221): ?>
                                                                    <img alt="test1"
                                                                         class="i_user"
                                                                         src="/images/flags/i_user.png">
                                                                    <br><?= $name4221.Html::tag('span',' ('.$childrenNumber4221.')',['class'=>'number-children text-blue text-bold']) ?>
                                                                    <br>
                                                                    <span class="text-red">[<?= $amount4221 ?>]</span>
                                                                <?php endif; ?>

                                                            </a>
                                                            <?php if (isset($lvl4221["childrens"])): ?>
                                                                <a class="next-member"
                                                                   href="<?= Url::to(['my-account/member', 'next' => $lvl22["id"]]) ?>">
                                                                    <img title="Next"
                                                                         src="/images/icon-member/arrow-down-active.png"
                                                                         border="0" style="margin-top: -10px;">
                                                                </a>
                                                            <?php endif ?>
                                                        </td>
                                                        <td width="50%" colspan="2" style="text-align:center;"
                                                            valign="top">
                                                            <a class="tree_user" href="javascript:void(0)">
                                                                <?php if ($lvl4222): ?>
                                                                    <img alt="test1"
                                                                         class="i_user"
                                                                         src="/images/flags/i_user.png">
                                                                    <br><?= $name4222.Html::tag('span',' ('.$childrenNumber4222.')',['class'=>'number-children text-blue text-bold']) ?>
                                                                    <br>
                                                                    <span class="text-red">[<?= $amount4222 ?>]</span>
                                                                <?php endif; ?>
                                                            </a>
                                                            <?php if (isset($lvl4222["childrens"])): ?>
                                                                <br/>
                                                                <a class="next-member"
                                                                   href="<?= Url::to(['my-account/member', 'next' => $lvl22["id"]]) ?>">
                                                                    <img title="Next"
                                                                         src="/images/icon-member/arrow-down-active.png"
                                                                         border="0" style="margin-top: -10px;">
                                                                </a>
                                                            <?php endif ?>

                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
            <table width="100%">
                <tbody>
                <!--                <tr>-->
                <!--                    <td width="50%" style="text-align:left;">-->
                <!--                        <a href="ag_btree.php?id=28989">-->
                <!--                            <img title="Bottom Left"-->
                <!--                                 src="/images/icon-member/arrow-mostdown-active.png"-->
                <!--                                 border="0">-->
                <!--                        </a>&nbsp;-->
                <!--                        <a href="ag_btree.php?id=18772">-->
                <!--                            <img title="Next" src="/images/icon-member/arrow-down-active.png"-->
                <!--                                 border="0" style="margin-top: -10px;">-->
                <!--                        </a>-->
                <!--                    </td>-->
                <!--                    <td width="50%" style="text-align:right;">-->
                <!--                        <a href="ag_btree.php?id=25400">-->
                <!--                            <img title="Next" src="/images/icon-member/arrow-down-active.png"-->
                <!--                                 border="0" style="margin-top: -10px;">-->
                <!--                        </a>-->
                <!--                        <a href="ag_btree.php?id=30701">-->
                <!--                            <img title="Bottom Right"-->
                <!--                                 src="/images/icon-member/arrow-mostdown-active.png"-->
                <!--                                 border="0">-->
                <!--                        </a>-->
                <!--                    </td>-->
                <!--                </tr>-->
                </tbody>
            </table>
        </div>
    </div>
</div>
<style>
    .btree {
        width: 100%;
    }

    .next-member {
        margin-top: 10px;
        display: inline-block;
    }
    .leg-left{
        padding-right:20px;
        float: right;
        font-weight: bold;
        color: #CC0000;
    }
    .leg-right{
        padding-left:20px;
        float: left;
        font-weight: bold;
        color: #44b5f6;
    }
</style>