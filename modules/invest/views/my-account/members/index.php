<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\forms\search\CustomerBankAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('investment', 'Thành viên của nhánh');
$this->params['breadcrumbs']['Genealogy'] = \yii\helpers\Url::to(['my-account/member']);
$this->params['pageTitle'] = $this->title;
$statisticMember = Yii::$app->user->identity->statisticBranch();
?>
<div class="statistic-branch" style="text-align:center;">
    <p>
        <label><?= Yii::t('investment', 'Số thành viên chân mạnh') ?></label>:&nbsp;<b class="text-blue"><?= $statisticMember["member_left"] ?></b>&nbsp;<b>member</b>
    </p>

    <p>
        <label><?= Yii::t('investment', 'Tổng lợi nhuận chân mạnh') ?></label>:&nbsp;<b class="text-red"><?= Yii::$app->formatter->asDecimal($statisticMember["poundage_left"],8)  ?>&nbsp;<?= Yii::$app->params["unit"] ?></b>
    </p>

    <p>
        <label><?= Yii::t('investment', 'Số thành viên chân yếu') ?></label>:&nbsp;<b class="text-blue"><?= $statisticMember["member_right"] ?></b><b>member</b>
    </p>

    <p>
        <label><?= Yii::t('investment', 'Tổng lợi nhuận chân mạnh') ?></label>:&nbsp;<b class="text-red"><?= Yii::$app->formatter->asDecimal($statisticMember["poundage_right"],8)  ?>&nbsp;<?= Yii::$app->params["unit"] ?></b>
    </p>

</div>
<div class="list-members">
    <div class="tree">
        <div class="head-action">
            <p class="top-parent text-center hidden"><a class=" fa fa-arrow-circle-up prev-top btn btn-sm btn-danger"
                                                        href="javascript:void(0);"
                                                        style="padding:2px 25px; margin-bottom:20px; font-size:20px;"></a>
            </p>

            <p class="prev-parent text-center hidden"><a class="fa fa-arrow-up prev-one-model  btn btn-sm btn-primary"
                                                         href="javascript:void(0);"
                                                         style="padding:4px 20px; font-size:16px;"></a></p>
        </div>
        <div class="middle">
            <?php echo $model; ?>
        </div>
    </div>
</div>
