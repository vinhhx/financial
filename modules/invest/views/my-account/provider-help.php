<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 01-Jul-16
 * Time: 2:16 PM
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('investment', 'Yêu cầu cho');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$wallet = Yii::$app->user->identity->getCurrentWallet();
?>

<div class="box box-customer">
    <div class="box-body">
        <?php $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-3',
                    'offset' => 'col-sm-offset-0',
                    'wrapper' => 'col-sm-6',
                    'error' => 'col-sm-6',
                    'hint' => 'col-sm-6',
                ],

            ]
        ]); ?>
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label col-sm-3"
                       for="button-submit"><?= Yii::t('investment', 'Số dư tài khoản') ?></label>

                <div class="col-sm-6">
                <span class="text-bold text-primary form-control"
                      style="border:none"><?= Yii::$app->formatter->asDecimal($wallet->balance, 8) ?>
                    &nbsp;<?= Yii::$app->params["unit"] ?></span>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3"
                       for="button-submit"><?= Yii::t('investment', 'Địa chỉ ngân hàng thụ hưởng') ?></label>

                <div class="col-sm-6">
                <span class="text-bold text-primary form-control"
                      style="border:none"><?= Yii::$app->params["invest_bit_coin_address"] ?></span>
                </div>
            </div>
            <?= $form->field($model, 'amount')->textInput(['placeholder' => Yii::t('investment', 'Số BTC'), 'class' => 'form-control', 'style' => 'width:250px;', 'maxlength' => true]); ?>
            <div class="form-group">
                <label class="control-label col-sm-3" for="button-submit"></label>

                <div class="col-sm-6">
                    <?= \yii\helpers\Html::submitButton('<span class="fa fa-money"></span>&nbsp;' . Yii::t('investment', 'Gửi'), ['class' => 'btn btn-sm btn-success']) ?>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="col-sm-6">
                <h4>QR CODE:</h4>
                <img src="/images/QR_INVEST.png?v=<?= Yii::$app->params["IMAGE_VERSION"] ?>" style="width: 150px;"/>
            </div>
        </div>


        <?php ActiveForm::end(); ?>
    </div>
</div>

