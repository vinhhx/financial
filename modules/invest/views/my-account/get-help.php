<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 01-Jul-16
 * Time: 2:16 PM
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('investment', 'Yêu cầu nhận');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$wallet = Yii::$app->user->identity->getCurrentWallet();
$disabled = false;
?>

<div class="box box-customer">
    <div class="box-body">
        <?php $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-3',
                    'offset' => 'col-sm-offset-0',
                    'wrapper' => 'col-sm-9',
                    'error' => 'col-sm-9',
                    'hint' => 'col-sm-9',
                ],

            ]
        ]); ?>
        <?php if (!Yii::$app->user->identity->canWithDrawn() || $model->bank_address==''):
            $disabled = true;
            ?>
            <div class="form-group">
                <label class="control-label col-sm-3 text-red"
                       for="button-submit"><?= Yii::t('customers', 'System Notice') ?></label>

                <div class="col-sm-6">
                    <label class="form-control color-red"
                           style="border: none;"><?= Yii::t('customers', 'Account has request withdraw BTC or time last withdrawn least than 3 hour'); ?></label>
                </div>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <label class="control-label col-sm-3"
                   for="button-submit"><?= Yii::t('investment', 'Số dư hiện tại') ?></label>

            <div class="col-sm-6">
                <span class="text-bold text-primary form-control"
                      style="border:none"><?= Yii::$app->formatter->asDecimal($wallet->balance, 8) ?>
                    &nbsp;<?= Yii::$app->params["unit"] ?></span>
            </div>
        </div>
        <?= $form->field($model, 'amount')->textInput(['placeholder' => Yii::t('investment', 'Số BTC'), 'class' => 'form-control', 'style' => 'width:250px;', 'maxlength' => true, 'disabled' => $disabled]); ?>
        <?php if ($model->bank_address): ?>
            <?= $form->field($model, 'bank_address')->textInput(['placeholder' => Yii::t('investment', 'Địa chỉ ngân hàng thụ hưởng'), 'class' => 'form-control', 'style' => 'width:500px;', 'maxlength' => true, 'disabled' => true]); ?>
        <?php else:?>
            <div class="form-group">
                <label class="control-label col-sm-3" for="cashoutform-bank_address">Receive bank account</label>

                <div class="col-sm-9">
                    <span class="text-red"> Your account does not have bitcoin bank address. Please add bitcoin bank address </span><?= Html::a('HERE',['create-address-bank'],['style'=>'font-weight:bold; text-decoration:underline','class'=>'text-bold text-red']) ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="form-group">
            <label class="control-label col-sm-3" for="button-submit"></label>

            <div class="col-sm-6">
                <?= \yii\helpers\Html::submitButton('<span class="fa fa-money"></span>&nbsp;' . Yii::t('investment', 'Gửi'), ['class' => 'btn btn-sm btn-success', 'disabled' => $disabled]) ?>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

