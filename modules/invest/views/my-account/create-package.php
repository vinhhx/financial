<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 29-Jun-16
 * Time: 9:04 AM
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('investment', 'Tạo gói đầu tư.');
//$refCode = Yii::$app->request->get('ref', '');
$packageDisable = (!Yii::$app->params["INVEST_OPEN_CREATE_PACKAGE"])?true :false;
?>
<?= \app\extensions\widgets\Alert::widget(); ?>
<h1 class="title-login text-center"><?= Html::encode($this->title) ?></h1>

<div class="login-form-border"">
<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-3',
            'offset' => 'col-sm-offset-0',
            'wrapper' => 'col-sm-9',
            'error' => 'col-sm-9',
            'hint' => 'col-sm-9',
        ],
    ]
]); ?>
    <div class="col-lg-6 col-sm-6 col-xs-12">
        <?php if (!Yii::$app->user->isGuest): ?>
            <h3 style="margin-top: 0px;"><?php echo Yii::t('systems', 'Xin chào,') ?> <?= Yii::$app->user->identity->username ?></h3>
        <?php endif; ?>
        <p style="text-align: left; padding: 0px 0px 0px 15px;"><?php echo Yii::t('systems', 'Để đăng ký chơi tại sân chơi này, bạn vui lòng điền đầy đủ các thông tin trong mẫu dưới đây.') ?></p>
        <?= $form->field($model, 'amount')->textInput(['placeholder' => Yii::t('systems', 'Số tiền tham gia.'), 'maxlength' => true]); ?>
        <div class="form-group">
            <label class="control-label col-sm-2" for="button-submit"></label>

            <div class="col-sm-6">
                <?= \yii\helpers\Html::submitButton('<span class="fa fa-floppy-o"></span>&nbsp;' . Yii::t('systems', 'Đăng ký'), ['class' => 'btn btn-sm btn-danger','disabled'=>$packageDisable]) ?>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-sm-6 col-xs-12 hidden-xs">
        <?php if ($packages): ?>
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <th style="width: 10px">#</th>
                    <th><?php echo Yii::t('systems', 'Tên gói') ?></th>
                    <th><?php echo Yii::t('systems', 'Giá trị ') ?></th>
                    <th style="width: 60px"><?php echo Yii::t('systems', 'Lãi hằng ngày') ?></th>
                </tr>
                <?php $i = 1;
                foreach ($packages as $pack): ?>
                    <tr>
                        <td><?= $i ?></td>
                        <td><?= $pack->package_name ?></td>
                        <td>
                            <?php
                            $text = Yii::$app->formatter->asDecimal($pack->amount_min, 1) . Yii::$app->params["unit"];
                            if ($pack->amount_min !== $pack->amount_max) {
                                $text = Yii::$app->formatter->asDecimal($pack->amount_min, 1) . Yii::$app->params["unit"] . '&nbsp; → &nbsp; ' . Yii::$app->formatter->asDecimal($pack->amount_max, 1) . Yii::$app->params["unit"];
                            }
                            ?>
                            <span class="badge <?=isset(Yii::$app->params["bg-color-index"][$i])?Yii::$app->params["bg-color-index"][$i]:'bg-teal' ?>"><?= $text ?></span>
                        </td>
                        <td><span
                                class="badge bg-teal"><?= Yii::$app->formatter->asDecimal($pack->increase_day, 1) . '%' ?></span>
                        </td>
                    </tr>
                    <?php $i++; ?>
                <?php endforeach; ?>

                </tbody>
            </table>
        <?php endif; ?>
    </div>

<?php ActiveForm::end(); ?>
</div>
