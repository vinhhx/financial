<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerBankAddress */

$this->title = Yii::t('app', 'Detail Bank Address') . ': #' . $model->id;
$this->params['breadcrumbs'][Yii::t('app', 'Bank Address')] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/customer/my-account/index';
?>
<div class="customer-bank-address-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('customers', 'Bạn chắc chắn xóa dữ liệu này ?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-body box-primary">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'bank_address',
                        'status',
                        'created_at:datetime',
                        'updated_at:datetime',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>
