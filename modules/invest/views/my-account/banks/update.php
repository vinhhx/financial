<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerBankAddress */

$this->title = Yii::t('customers', 'Cập nhật địa chỉ ngân hàng') . ': ' . $model->id;
$this->params['breadcrumbs'][Yii::t('app', 'Bank Address')] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/customer/customer-bank-address/index';
?>
<div class="customer-bank-address-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
