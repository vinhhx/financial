<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 29-Jun-16
 * Time: 9:04 AM
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('investment', 'Tạo gói đầu tư.');
//$refCode = Yii::$app->request->get('ref', '');
?>
<?= \app\extensions\widgets\Alert::widget(); ?>
<h1 class="title-login text-center"><?= Html::encode($this->title) ?></h1>

<div class="login-form-border"">
<?php $form = ActiveForm::begin([
    'layout' => 'horizontal',
    'fieldConfig' => [
        'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
        'horizontalCssClasses' => [
            'label' => 'col-sm-3',
            'offset' => 'col-sm-offset-0',
            'wrapper' => 'col-sm-9',
            'error' => 'col-sm-9',
            'hint' => 'col-sm-9',
        ],
    ]
]); ?>
<div class="col-lg-12 col-sm-12 col-xs-12">
    <?php if (!Yii::$app->user->isGuest): ?>
        <h3 style="margin-top: 0px;"><?php echo Yii::t('systems', 'Xin chào,') ?> <?= Yii::$app->user->identity->username ?></h3>
    <?php endif; ?>
    <p style="text-align: left; padding: 0px 0px 0px 15px;"><?php echo Yii::t('systems', 'Để đăng ký chơi tại sân chơi này, bạn vui lòng điền đầy đủ các thông tin trong mẫu dưới đây.') ?></p>
    <?= $form->field($model, 'ref_code')->textInput(['placeholder' => Yii::t('systems', 'Mã giới thiệu.'), 'maxlength' => true]); ?>
    <div class="form-group">
        <label class="control-label col-sm-2" for="button-submit"></label>

        <div class="col-sm-6">
            <?= \yii\helpers\Html::submitButton('<span class="fa fa-floppy-o"></span>&nbsp;' . Yii::t('systems', 'Đăng ký'), ['class' => 'btn btn-sm btn-danger']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
</div>
