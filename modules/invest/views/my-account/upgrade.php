<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 01-Jul-16
 * Time: 7:35 AM
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('investment', 'Cập nhật gói đầu tư');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>

<div class="box box-customer">
    <div class="box-body">
        <?php $form = ActiveForm::begin([
            'layout' => 'horizontal',
            'fieldConfig' => [
                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                'horizontalCssClasses' => [
                    'label' => 'col-sm-3',
                    'offset' => 'col-sm-offset-0',
                    'wrapper' => 'col-sm-9',
                    'error' => 'col-sm-9',
                    'hint' => 'col-sm-9',
                ],
            ]
        ]); ?>
        <div class="col-lg-6 col-sm-6 col-xs-12">

            <p style="text-align: left; padding: 0px 0px 0px 15px;"><?php echo Yii::t('investment', 'Để nâp cấp gói chơi hiện tại, bạn vui lòng điền đầy đủ các thông tin trong mẫu dưới đây.') ?></p>

            <div class="form-group">
                <label class="control-label col-sm-3"
                       for="button-submit"><?= Yii::t('investment', 'Giá trị gói hiện tại') ?></label>

                <div class="col-sm-6">
                    <span class="text-bold text-primary form-control"
                          style="border:none"><?= Yii::$app->formatter->asDecimal($model->amount, 8) ?></span>
                </div>
            </div>
            <?= $form->field($formModel, 'amount')->textInput(['placeholder' => Yii::t('investment', 'Số BTC.'), ['class' => 'form-control col-sm-6'], 'maxlength' => true]); ?>
            <div class="form-group">
                <label class="control-label col-sm-3" for="button-submit"></label>

                <div class="col-sm-6">
                    <?php $currentToken = Yii::$app->user->identity->countCurrentToken();
                    $disabled = ($currentToken < 1) ? true : false;
                    ?>
                    <?= \yii\helpers\Html::submitButton('<span class="fa fa-level-up"></span>&nbsp;' . Yii::t('investment', 'Nâng cấp'), ['class' => 'btn btn-sm btn-success', 'disabled' => $disabled]) ?>
                    <?php if ($disabled):
                        echo \yii\helpers\Html::a(Yii::t('customers', 'Mua token'), ['buy-token'], ['class' => 'btn btn-sm btn-warning']);
                    endif ?>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12 hidden-xs">
            <?php if ($packages): ?>
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th><?php echo Yii::t('investment', 'Tên gói') ?></th>
                        <th><?php echo Yii::t('investment', 'Giá trị ') ?></th>
                        <th style="width: 60px"><?php echo Yii::t('investment', 'Lãi hằng ngày') ?></th>
                    </tr>
                    <?php $i = 1;
                    foreach ($packages as $pack): ?>
                        <tr>
                            <td><?= $i ?></td>
                            <td><?= $pack->package_name ?></td>
                            <td>
                                <?php
                                $text = Yii::$app->formatter->asDecimal($pack->amount_min, 1) . Yii::$app->params["unit"];
                                if ($pack->amount_min !== $pack->amount_max) {
                                    $text = Yii::$app->formatter->asDecimal($pack->amount_min, 1) . Yii::$app->params["unit"] . '&nbsp; → &nbsp; ' . Yii::$app->formatter->asDecimal($pack->amount_max, 1) . Yii::$app->params["unit"];
                                }
                                ?>
                                <span class="badge label-success"><?= $text ?></span>
                            </td>
                            <td><span
                                    class="badge label-danger"><?= Yii::$app->formatter->asDecimal($pack->increase_day, 1) . '%' ?></span>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            <?php endif; ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
