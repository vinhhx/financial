<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\modules\admin\forms\ChangePasswordForm */

$this->title = Yii::t('app', 'Change Password');
$this->params['breadcrumbs'][] = $this->title;
$this->params["pageTitle"] = $this->title;
$user = Yii::$app->user->identity;
?>

<div class="box box-warning">

    <div class="box-body">
        <?php $form = ActiveForm::begin(['id' => 'form-change-password']); ?>

        <?= $form->field($user, 'username')->textInput(['disabled' => true]); ?>
        <?= $form->field($model, 'currentPassword')->passwordInput(); ?>
        <?= $form->field($model, 'newPassword')->passwordInput(); ?>
        <?= $form->field($model, 'newPasswordConfirmation')->passwordInput(); ?>
        <div class="form-group">
            <label class="control-label"></label>
            <?= Html::submitButton(Yii::t('customers', 'Đổi mật khẩu'), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
