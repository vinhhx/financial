<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\investments\InvestCustomerPackageHistory */

$this->title = Yii::t('app', 'Create Invest Customer Package History');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Invest Customer Package Histories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invest-customer-package-history-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
