<?php

use yii\grid\GridView;
use app\models\investments\InvestCustomerCase;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('investment', 'Member list');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<?=
GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view grid-view-customize'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'Username',
            'value' => function ($model) {
                return $model->investCustomer->username;
            }
        ],
        [
            'label' => 'Level',
            'value' => function ($model) {
                return $model->level;
            }
        ],
        [
            'label' => 'Leg position',
            'format' => 'raw',
            'value' => function ($model) {
                $class = ($model->position == InvestCustomerCase::LEFT_LEG) ? 'text-green' : 'text-red';
                return \yii\helpers\Html::tag('label', InvestCustomerCase::getPosition($model->position), ['class' => $class]);
            }
        ],

        [
            'label' => 'Package',
            'value' => function ($model) {
                //$listPackage = $model->getInvestPackage();
                //return isset($listPackage[$model->investCustomerPackage->package_id]->package_name) ? $listPackage[$model->investCustomerPackage->package_id]->package_name : '--';
                return isset($model->investCustomerPackage->amount) ? Yii::$app->formatter->asDecimal($model->investCustomerPackage->amount, 8) : '--';
            }
        ],
    ],
]);
?>

