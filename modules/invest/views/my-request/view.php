<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\investments\InvestCustomerRequest */

$this->title = Yii::t('investment', 'Yêu cầu số #') . $model->id;
$this->params['breadcrumbs'][Yii::t('investment', 'Danh sách')] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/customer/my-request/index';
?>
<div class="box box-customer">
    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'customer_username',
                [
                    'label' => 'BTC value',
                    'value' => Yii::$app->formatter->asDecimal($model->amount, 8),
                ],
                'bank_address',
                [
                    'label' => 'Current Status request',
                    'value' => $model->getStatusLabel(),
                ],
                [
                    'label' => 'Current Type request',
                    'value' => $model->getTypeLabel(),
                ],
                'created_at:datetime',
            ],
        ]) ?>

        <?php if ($model->canUploadAttachment()): ?>
            <h4> <?= Yii::t('investment', 'Tải lên hình ảnh chụp giao dịch ngân hàng của bạn để xác thực.') ?></h4>
            <?php $form = \yii\widgets\ActiveForm::begin(['id' => 'upload-attachment',
                'options' => ['enctype' => 'multipart/form-data']]) ?>
            <div class="bg-file-upload">
                <?= $form->field($modelForm, 'file')->fileInput()->label(false) ?>
            </div>
            <?= Html::submitButton('Upload', ['class' => 'btn btn-danger btn-sm']) ?>

            <?php \yii\widgets\ActiveForm::end() ?>
        <?php else:
            if ($model->transaction_code):
                ?>
                <div class="attachment-file">
                    <a target="_blank" id="attachment-file"
                       href="<?= Yii::$app->image->getImg($model->transaction_code) ?>">
                        <?= Html::img(Yii::$app->image->getImg($model->transaction_code, '100x100'), ['class' => 'img-thumbnail img-item']) ?>

                    </a>
                    <?php if ($model->canDeleteAttachment()): ?>
                        <span id="action-delete-image" class="glyphicon glyphicon-remove" data-toggle="tooltip"
                              data-placement="top" title="remove this attachment"></span>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>

<?php
$js = <<<Js
    $('.attachment-file').removeImage();
Js;
$this->registerJs($js);
?>
