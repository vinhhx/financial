<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Note;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\NoteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Messages';
$this->params['pageTitle'] = $this->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="note-index">
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box box-body box-primary">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                'object_id',
                'object_table',
                'content:ntext',
                [
                    'attribute' => 'type',
                    'content' => function ($model) {
                        return isset(Note::getTypeLabels()[$model->type]) ? Note::getTypeLabels()[$model->type] : '--';
                    },
                ],
                [
                    'attribute' => 'created_by',
                    'content' => function ($model) {
                        return Html::encode($model->created_by);
                    },
                ],
                'created_at:datetime',
            //['class' => 'yii\grid\ActionColumn'],
            ],
        ]);
        ?>
    </div>
</div>
