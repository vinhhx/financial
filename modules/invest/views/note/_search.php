<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Note;

/* @var $this yii\web\View */
/* @var $model app\models\Note */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-body box-primary">
    <div class="note-search form-search">
        <?php
        $form = ActiveForm::begin([
            'method' => 'get',
        ]);
        ?>
        <ul>
            <li class="form-label"><b>Search</b></li>
            <li class="form-item">
                <?= Html::activeInput('text', $model, 'id', array('placeholder' => $model->getAttributeLabel('id'), 'style' => 'width:150px')) ?>
            </li>
            <li class="form-item">
                <?= Html::activeInput('text', $model, 'object_id', array('placeholder' => $model->getAttributeLabel('object_id'), 'style' => 'width:150px')) ?>
            </li>
            <li class="form-item">
                <?= Html::activeInput('text', $model, 'object_table', array('placeholder' => $model->getAttributeLabel('object_table'), 'style' => 'width:150px')) ?>
            </li>
            <li class="form-item">
                <?= Html::activeDropDownList($model, 'type', Note::getTypeLabels(), array('prompt' => '-- Loại --', 'style' => 'width:150px')) ?>
            </li>
            <li class="form-item">
                <?= Html::activeInput('text', $model, 'created_by', array('placeholder' => $model->getAttributeLabel('created_by'), 'style' => 'width:150px')) ?>
            </li>
            <li class="form-item">
                <?= Html::submitButton('<i class="fa fa-search"></i>&nbsp;Search', ['class' => 'btn btn-primary btn-sm']) ?>
            </li>
        </ul>
        <?php ActiveForm::end(); ?>
    </div>
</div>