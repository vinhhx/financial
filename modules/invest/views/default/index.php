<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\investments\InvestCustomerPackage;

$this->title = Yii::t('systems', "Trang chủ");
$this->params['pageTitle'] = $this->title;

$wallet = Yii::$app->user->identity->getCurrentWallet();
$package = Yii::$app->user->identity->getCurrentPackage();
$packageDisable = Yii::$app->params["INVEST_OPEN_CREATE_PACKAGE"];
?>

<div class="row default-admin">
    <?php if (!$package): ?>
        <div class="col-md-3 col-sm-4 col-xs-6">
            <div class="info-box bg-blue">
                <a href="<?= Url::to(['my-account/create-package']) ?>" id="open-package"
                   class="index-block <?= (!$packageDisable) ? 'disabled' : '' ?>">
                    <span class="info-box-icon"><img src="/images/ico-wallet-v2.png"/></span>

                    <div class="info-box-content">
                        <h4 class="info-box-text-header" style="text-align: center;"><?= Yii::t('investment', 'Order investment package') ?></h4>

                    </div>
                </a>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
    <?php endif; ?>

    <div class="col-md-3 col-sm-4 col-xs-6">
        <div class="info-box bg-aqua">
            <a href="javascript:void(0);" id="my-account-balance" class="index-block" ?>
                <span class="info-box-icon"><img src="/images/ico-wallet.png"/></span>

                <div class="info-box-content">
                    <p class="info-box-text-header" style="font-size: 18px; margin-top: 8px; margin-bottom: -5px;"><?= Yii::t('investment', 'Số dư') ?></p>

                    <p class="info-box-text-content"><?= Yii::$app->formatter->asDecimal($wallet->balance, 8) ?>
                        BTC</p>
                </div>
            </a>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>

    <div class="col-md-3 col-sm-4 col-xs-6">
        <div class="info-box bg-red">
            <a href="<?= Url::to(['my-account/provider-help']) ?>" id="provider-helper" class="index-block">
                <span class="info-box-icon"><img src="/images/ico-user-up.png"/></span>

                <div class="info-box-content">
                    <p class="info-box-text-header" style="font-size: 18px; margin-top: 12px;">
                        <?php echo Yii::t('investment', 'Make deposit') ?></p>
                </div>
            </a>
        </div>
    </div>
    <div class="col-md-3 col-sm-4 col-xs-6">
        <div class="info-box bg-green">
            <a href="<?= Url::to(['my-account/get-help']) ?>" id="widthdraw-package" class="index-block">
                <span class="info-box-icon"><img src="/images/ico-user-down.png"/></span>

                <div class="info-box-content">
                    <p class="info-box-text-header" style="font-size: 18px; margin-top: 12px;">
                        <?php echo Yii::t('investment', 'Withdraw') ?></p>
                </div>
            </a>
        </div>
    </div>


</div>
<div class="row-link-refferal">
    <p>Your refferal link
        is: <?= Html::a(Yii::$app->user->identity->getUrlRegisterReferal(), Yii::$app->user->identity->getUrlRegisterReferal()) ?></p>
</div>
<div class="clearfix"></div>
<div class="row">
    <?php if ($package): ?>
        <?php
        $disableUpgrade = ($package->canUpgrade()) ? false : true;
        $disableCashOutProfit = ($package->canCashOutProfit()) ? false : true;
        $disableCashOutAll = ($package->canCashOutAllPackage()) ? false : true;
        $disableCashOutPound = ($package->canCashOutPoundage()) ? false : true;

        ?>
        <div class="col-sm-6">
            <div class="box box-customer">
                <div class="box-header">
                    <h3 class="box-title"><?= Yii::t('investment', "Thông tin gói đầu tư") ?></h3>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th><?php echo Yii::t('investment', 'Nhãn') ?></th>
                            <th> <?= Yii::t('investment', 'Giá trị') ?></th>
                            <th style="max-width: 40px"></th>
                        </tr>
                        <tr>
                            <td>1.</td>
                            <td><span class="text-bold"><?= Yii::t('investment', 'Số BTC') ?></span></td>
                            <td class="text-center"><span
                                    class="text-center text-bold <?= ($package->status == InvestCustomerPackage::STATUS_ACTIVE ? 'text-green' : 'text-red') ?>"><?= Yii::$app->formatter->asDecimal($package->amount, 8) ?>
                                    <?= Yii::$app->params["unit"] ?></span></td>
                            <td><?= Html::a('<span class="fa fa-level-up"></span>&nbsp;' . Yii::t('system', 'Nâng cấp'),
                                    ['my-account/upgrade'],
                                    ['class' => 'btn btn-sm btn-warning', 'style' => 'margin-left:10px;', 'disabled' => $disableUpgrade,

                                    ]) ?></td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td><span class="text-bold"><?= Yii::t('investment', 'Lợi nhuận') ?></span></td>
                            <td class="text-center"><span
                                    class="text-blue text-center text-bold"><?= Yii::$app->formatter->asDecimal($package->current_profit, 8) ?>
                                    <?= Yii::$app->params["unit"] ?></span>
                                <?php if ($package->hasBonusProfit()): ?>
                                    <span class="btn btn-flat bg-aqua">+35%</span>
                                <?php endif; ?>
                            </td>
                            <td><?= Html::a('<span class="fa fa-money"></span>&nbsp;' . Yii::t('investment', 'Rút lợi nhuận'),
                                    ['my-account/cash-out-profit'],
                                    ['class' => 'btn btn-sm btn-primary',
                                        'style' => 'margin-left:10px;',
                                        'data' => [
                                            'confirm' => Yii::t('investment', 'Bạn có chắc muốn rút toàn bộ số lợi nhuân không ?') . "\n" .
                                                Yii::t('investment', 'Phí rút một lần là ') . Yii::$app->params["feeProfit"] . '%',
                                            'method' => 'post',
                                        ],
                                        'disabled' => $disableCashOutProfit
                                    ]) ?></td>
                        </tr>
                        <tr>
                            <td>3.</td>
                            <td><span class="text-bold"><?= Yii::t('investment', 'Hoa hồng giới thiệu') ?></span></td>
                            <td class="text-center"><span
                                    class="text-maroon text-center text-bold"><?= Yii::$app->formatter->asDecimal($package->current_poundage, 8) ?>
                                    &nbsp; <?= Yii::$app->params["unit"] ?></span>
                                <?php if ($package->hasBonusPoundage()): ?>
                                    <span class="btn btn-flat bg-aqua">+25%</span>
                                <?php endif; ?>
                            </td>
                            <td><?= Html::a('<span class="fa fa-money"></span>&nbsp;' . Yii::t('investment', 'Rút hoa hồng'),
                                    ['my-account/cash-out-poundage'],
                                    ['class' => 'btn btn-sm bg-maroon',
                                        'style' => 'margin-left:10px;',
                                        'data' => [
                                            'confirm' => Yii::t('investment', 'Bạn có chắc muốn rút toàn bộ số lợi nhuận hoa hồng không ? ') . "\n" .
                                                Yii::t('investment', 'Phí rút một lần là ') . Yii::$app->params['feePoundage'] . '%',
                                            'method' => 'post',
                                        ],
                                        'disabled' => $disableCashOutPound
                                    ]) ?></td>
                        </tr>
                        <tr>
                            <td>4.</td>
                            <td><span class="text-bold"><?= Yii::t('investment', 'Ngày tham gia') ?></span></td>
                            <td class="text-center text-bold">
                                <span><?= Yii::$app->formatter->asDatetime($package->created_at); ?></span></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>5.</td>
                            <td><span class="text-bold"><?= Yii::t('investment', 'Trạng thái hiện tại') ?></span></td>
                            <td class="text-center">
                                <span
                                    class="badge <?= ($package->is_active == InvestCustomerPackage::IS_ACTIVE_PENDING) ? 'bg-orange' : 'bg-green' ?> ?>"><?= ($package->is_active == InvestCustomerPackage::IS_ACTIVE_PENDING) ? Yii::t('investment', 'Đang chờ kích hoạt') : $package->getStatusLabel() ?></span>
                            </td>
                            <td></td>
                        </tr>

                        </tbody>
                        <tfoot>
                        <tr>
                            <td></td>
                            <td colspan=>
                                <div class="pull-left"><span
                                        class="text-bold"><?= Yii::t('investment', 'Tổng:') ?></span></div>

                            </td>
                            <td class="text-center">
                                <?php $total = $package->amount + $package->current_profit + $package->current_poundage; ?>
                                <span
                                    class="text-bold text-center text-red"><?= Yii::$app->formatter->asDecimal($total, 8) ?><?= Yii::$app->params["unit"] ?></span>
                            </td>
                            <td>
                                <?php
                                //                                Html::a('<span class="fa fa-money"></span>&nbsp;' . Yii::t('system', 'CASH OUT PACKAGE'),
                                //                                    ['my-account/cash-out'],
                                //                                    ['class' => 'btn btn-sm btn-success',
                                //                                        'style' => 'margin-left:10px;',
                                //                                        'data' => [
                                //                                            'confirm' => Yii::t('app', 'Are you sure cash out of this package ?'),
                                //                                            'method' => 'post',
                                //                                        ],
                                //                                        'disabled' => $disableCashOutAll
                                //                                    ])
                                ?>
                            </td>
                        </tr>
                        </tfoot>
                    </table>


                </div>

            </div>


        </div>
        <div class="col-sm-6">
            <div class="box box-customer">
                <div class="box-header">
                    <h3 class="box-title"><?= Yii::t('investment', 'Lịch sử hoạt động mới nhất của gói đầu tư :') ?></h3>
                </div>
                <div class="box-body">
                    <?php if ($packageHistories): ?>
                        <ul class="products-list product-list-in-box">

                            <?php foreach ($packageHistories as $item): ?>
                                <li class="item">
                                    <div class="product-img"
                                         style="background:#fafafa; width: 40px; height:40px;padding:10px; font-size:20px;line-height:20px">
                                        <?php switch ($item->type) {
                                            case \app\models\investments\InvestCustomerPackageHistory::TYPE_OPEN_PACKAGE:
                                                echo '<span class="fa fa-play-circle text-success"></i></span>';
                                                break;
                                            case \app\models\investments\InvestCustomerPackageHistory::TYPE_UPGRADE:
                                                echo '<span class="fa fa-level-up text-warning"></i></span>';
                                                break;
                                            case \app\models\investments\InvestCustomerPackageHistory::TYPE_ACTIVE_PACKAGE:
                                                echo '<span class="fa fa-check-circle text-success"></i></span>';
                                                break;
                                            case \app\models\investments\InvestCustomerPackageHistory::TYPE_CAPITAL_PACKAGE:
                                                echo '<span class="fa fa-long-arrow-right text-success"></i></span>';
                                                break;
                                            case \app\models\investments\InvestCustomerPackageHistory::TYPE_WITHDRAW:
                                                echo '<span class="fa fa-long-arrow-left text-danger"></i></span>';
                                                break;
                                            case \app\models\investments\InvestCustomerPackageHistory::TYPE_POUNDAGE_PACKAGE:
                                            case \app\models\investments\InvestCustomerPackageHistory::TYPE_PROFIT_PACKAGE:
                                                echo '<span class="fa fa-plus-circle text-danger"></i></span>';
                                                break;

                                            default:
                                                break;
                                        } ?>
                                    </div>
                                    <div class="product-info">
                                        <span class="product-description text-bold pull-left text-aqua"
                                              style="line-height: 40px; min-width:20%; padding-right:15px ">
                                          <?= Yii::$app->formatter->asDatetime($item->created_at) ?>
                                        </span>
                                        <span class="product-description text-bold pull-left"
                                              style="line-height: 40px;">
                                          <?= $item->getTypeLabel() ?>
                                        </span>
                                        <span class="product-description text-bold pull-right text-red"
                                              style="line-height: 40px;">
                                          <?= $item->getSignLabel() . Yii::$app->formatter->asDecimal($item->amount, 8) . Yii::$app->params["unit"]; ?>
                                        </span>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </div>
                <?php if ($packageHistories): ?>
                    <div class="box-footer text-center">
                        <a href="<?= Url::to(['my-package/index']) ?>"
                           class="uppercase text-bold"><?= Yii::t('investment', 'Xem tất cả') ?> </a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
<div class="row">
    <div class="col-xs-12">
        <hr/>
        <?php if ($packageTransactions): ?>
            <div class="box box-customer">
                <div class="box-header">
                    <h3 class="box-title"><a
                            href="<?= Url::to(['my-transaction/index']) ?>"><?= Yii::t('investment', 'Giao dịch thực hiện gần nhất:') ?></a>
                    </h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive grid-view-customize">
                        <table class="table no-margin">
                            <thead>
                            <tr>
                                <th style="width:15px">#</th>
                                <th><?= Yii::t('investment', 'Tạo lúc') ?></th>
                                <th><?= Yii::t('investment', 'Loại giao dịch') ?></th>
                                <th><?= Yii::t('investment', 'Số dư trước giao dịch') ?></th>
                                <th><?= Yii::t('investment', 'Số BTC') ?></th>
                                <th><?= Yii::t('investment', 'Số dư sau giao dịch') ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $index = 1;
                            foreach ($packageTransactions as $tran): ?>
                                <tr>
                                    <td><?= $index ?></td>
                                    <td><?= Yii::$app->formatter->asDatetime($tran->created_at) ?></td>
                                    <td><span class="text-bold"><?= $tran->getTypeLabel(); ?></span></td>
                                    <td class="text-right">
                                        <span
                                            class="text-red text-bold"><?= Yii::$app->formatter->asDecimal($tran->balance_before, 8) ?></span>
                                    </td>
                                    <td class="text-right">
                                        <?php
                                        if ($tran->sign == \app\models\investments\InvestCustomerTransaction::SIGN_SUB) {
                                            echo '<span class="text-green text-bold"> - </span><b class="text-red">' . Yii::$app->formatter->asDecimal($tran->amount, 8) . '</b>';
                                        } elseif ($tran->sign == \app\models\investments\InvestCustomerTransaction::SIGN_ADD) {
                                            echo '<span class="text-red text-bold"> + </span><b class="text-red">' . Yii::$app->formatter->asDecimal($tran->amount, 8) . '</b>';
                                        } else {
                                            echo '<span class="text-red text-bold">' . Yii::$app->formatter->asDecimal($tran->amount, 8) . '</span>';
                                        }
                                        ?>
                                    </td>
                                    <td class="text-right">
                                        <span
                                            class="text-red text-bold"><?= Yii::$app->formatter->asDecimal($tran->balance_after, 8) ?></span>
                                    </td>
                                </tr>
                                <?php $index++;
                            endforeach;
                            ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.box-body -->
                <div class="box-footer clearfix">
                    <a href="<?= Url::to(['my-transaction/index']) ?>" class="btn btn-sm btn-info btn-flat pull-left"><i
                            class="fa fa-eye"></i>&nbsp;<?= Yii::t('investment', 'Xem toàn bộ giao dịch') ?></a>
                </div>
                <!-- /.box-footer -->
            </div>
        <?php endif; ?>
    </div>
</div>
<div class="page-modal">
    <div class="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Default Modal</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-success">Submit</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
</div>
<style>
    .product-list-in-box .item:first-child {
        padding-top: 0px;
    }

    a.disabled {
        opacity: .3;
        cursor: default !important;
        pointer-events: none;
        display: block;
    }
</style>


