<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Customer;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

$this->title = 'Registration';
$refCode = Yii::$app->request->get('ref', '');
?>
<?= \app\extensions\widgets\Alert::widget(); ?>
<h1 class="title-login text-center"><?= Html::encode($this->title) ?></h1>
<div class="row">
    <div class="login-form-border">
        <?php $form = ActiveForm::begin([
            'id' => 'register-form',
//        'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "<div>{input}</div>{error}",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>
        <div class="col-sm-6 col-xs-12">
            <?= $form->field($model, 'full_name')->textInput(['placeholder' => $model->getAttributeLabel('full_name')]) ?>
            <?= $form->field($model, 'username')->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>
            <?= $form->field($model, 'password')->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>
            <?= $form->field($model, 'passwordConfirmation')->passwordInput(['placeholder' => $model->getAttributeLabel('passwordConfirmation')]) ?>

        </div>
        <div class="col-sm-6 col-xs-12">
            <?= $form->field($model, 'nation')->dropDownList(Customer::getNationLabels()) ?>
            <?= $form->field($model, 'identity_card')->textInput(['placeholder' => $model->getAttributeLabel('identity_card')]) ?>
            <?= $form->field($model, 'phone')->textInput(['placeholder' => $model->getAttributeLabel('phone')]) ?>
            <?= $form->field($model, 'email')->textInput(['placeholder' => $model->getAttributeLabel('email')]) ?>

        </div>
        <div class="col-xs-12">
            <h3 style="color: #e1474d; text-transform: uppercase; font-size: 14px; font-weight: bold; text-align: left;">
                <?= Yii::t('system', 'Sponsor details') ?></h3>
        </div>
        <div class="col-xs-12">
            <?= $form->field($model, 'ref_code')->textInput(['placeholder' => $model->getAttributeLabel('ref_code')]) ?>
        </div>

        <div class="col-xs-12">
            <div style="padding: 0 15px 0 40px;">
                <div class="form-group" style="text-align: left; margin-top: 20px;">
                    <?= Html::submitButton('Submit', ['class' => 'btn', 'name' => 'login-button', 'style' => 'background-color:#f44336; color:#fff; margin-right:20px;']) ?>
                    <?= Html::resetButton('Clear', ['class' => 'btn', 'name' => 'login-button', 'style' => 'background-color:#959595; color:#fff']) ?>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>

