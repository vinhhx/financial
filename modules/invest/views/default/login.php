<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\modules\admin\forms\LoginForm */

$this->title = Yii::t('investment', 'Đăng nhập hệ thống')

?>
<div class="site-login" style="text-align: center">
    <h1 class="title-login"><?= Html::encode($this->title) ?></h1>

    <div class="row">
        <div class="col-sm-2"></div>
        <div class="login-form-border col-sm-8 col-xs-12" style="padding-bottom: 10px;">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "<div>{input}</div>",
                    'labelOptions' => ['class' => 'col-lg-1 control-label'],
                ],
            ]); ?>

            <?= $form->field($model, 'username')->textInput(['placeholder' => 'Username / Email']) ?>

            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password']) ?>

            <?= $form->field($model, 'rememberMe')->checkbox([
                'template' => "<div class=\"text-left\">{input} {label}</div>",
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn', 'name' => 'login-button', 'style' => 'background-color:#f44336; color:#fff']) ?>
                <?= Html::resetButton('Clear', ['class' => 'btn', 'name' => 'login-button', 'style' => 'background-color:#959595; color:#fff']) ?>

            </div>


            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-sm-2"></div>

    </div>
    <div class="row">
        <div class="register-button">
            <?= Html::a('Register', ['register'], ['class' => 'btn btn-xs btn-register']) ?>
            <?= Html::a('Forget Password', ['forget-password'], ['class' => 'btn btn-xs btn-register']) ?>
        </div>

    </div>
</div>

