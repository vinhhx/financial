<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 16-Jul-16
 * Time: 2:00 AM
 */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Yii::t('app', 'Transfer Token');
$this->params['breadcrumbs'][Yii::t('app', 'Tokens')] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/customer/my-account/index';

?>
<div class="box-body">
    <?php $form = ActiveForm::begin([
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-3',
                'offset' => 'col-sm-offset-0',
                'wrapper' => 'col-sm-9',
                'error' => 'col-sm-9',
                'hint' => 'col-sm-9',
            ],

        ]
    ]); ?>
    <?php
    $countToken = Yii::$app->user->identity->countCurrentToken();
    if($countToken<1):
    ?>
    <div class="form-group">
        <label class="control-label col-sm-3 text-red"
               for="button-submit"><?= Yii::t('customers', 'System Notice') ?></label>

        <div class="col-sm-6">
            <label class="form-control color-red"
                   style="border: none;"><?= Yii::t('customers', 'Number token  not enough to exchange'); ?></label>
        </div>
    </div>
    <?php endif; ?>
    <div class="form-group">
        <label class="control-label col-sm-3"
               for="button-submit"><?= Yii::t('customers', 'Rút tối thiểu') ?></label>

        <div class="col-sm-6">
            <label class="form-control"
                   style="border: none;"><?= 1 ?></label>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-3"
               for="button-submit"><?= Yii::t('investment', 'Number current token') ?></label>

        <div class="col-sm-6">
                <span class="text-bold text-primary form-control"
                      style="border:none"><?= Yii::$app->formatter->asInteger($countToken) ?>
                    &nbsp;Token</span>
        </div>
    </div>
    <?= $form->field($model, 'customer_username')->textInput(['placeholder' => Yii::t('investment', 'Customer username'), 'class' => 'form-control', 'style' => 'width:500px;', 'maxlength' => true]); ?>

    <?= $form->field($model, 'quantity')->textInput(['placeholder' => Yii::t('investment', 'Number token'), 'class' => 'form-control', 'style' => 'width:250px;', 'maxlength' => true]); ?>
    <?= $form->field($model, 'password')->passwordInput(['placeholder' => Yii::t('investment', 'Current Password'), 'class' => 'form-control', 'style' => 'width:250px;', 'maxlength' => true]); ?>
    <div class="form-group">
        <label class="control-label col-sm-3" for="button-submit"></label>

        <div class="col-sm-6">
            <?php $disabled= ($countToken)?false:true; ?>
            <?= \yii\helpers\Html::submitButton('<span class="fa fa-money"></span>&nbsp;' . Yii::t('investment', 'Gửi'), ['class' => 'btn btn-sm btn-success','disabled'=>$disabled]) ?>
            <?php if($disabled): ?>
                <?= Html::a('<i class="fa fa-plus-square-o"></i>&nbsp;' . Yii::t('app', 'Buy Token'), ['my-account/buy-token'], ['class' => 'btn btn-primary btn sm']) ?>
            <?php endif; ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>