<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\forms\CustomerTokenSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customer Tokens');
$this->params['breadcrumbs']["Account"] = \yii\helpers\Url::to(['my-account/index']);
$this->params['pageTitle'] = $this->title;
?>
<div class="customer-token-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>

        <?= Html::a('<i class="fa fa-exchange"></i>&nbsp;' .Yii::t('app', 'Transfer token'), ['exchange'], ['class' => 'btn btn-warning']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'grid-view grid-view-customize'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'token',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return '<span class="'.(($model->status==\app\models\CustomerToken::STATUS_USED)? 'text-red':'text-green').'">'. $model->getStatusLabel().'</span>';
                }

            ],
             'created_at:datetime',
            // 'updated_at',

//            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
