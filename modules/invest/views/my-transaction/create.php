<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\investments\InvestCustomerTransaction */

$this->title = Yii::t('app', 'Create Invest Customer Transaction');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Invest Customer Transactions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invest-customer-transaction-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
