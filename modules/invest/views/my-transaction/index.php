<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\invest\forms\InvestCustomerTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = Yii::t('investment', 'Customer Transactions');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<?php echo $this->render('_search', ['model' => $searchModel]); ?>

<p style="font-size: 14px">
    Account balance: <b class="text-danger" style="font-size: 20px;"><?= Yii::$app->formatter->asDecimal(Yii::$app->user->identity->getCurrentWallet()->balance, 8) ?> BTC</b>
</p>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
    'options' => ['class' => 'grid-view grid-view-customize'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

//        'bill_id',
        'created_at:datetime',


        ['attribute' => 'balance_before',
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 130px;text-align:right;'],
            'value' => function ($model) {
                return '<b class="text-red">' . Yii::$app->formatter->asDecimal($model->balance_before, 8) . '</b>';
            }
        ],
        [
            'attribute' => 'amount',
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 130px;text-align:right;'], // <-- set chiều rông
            'value' => function ($model) {
//                        $valueStr='';
                if ($model->sign == \app\models\investments\InvestCustomerTransaction::SIGN_SUB) {
                    return '<b class="text-green"> - </b><b class="text-red">' . Yii::$app->formatter->asDecimal($model->amount, 8) . '</b>';
                } elseif ($model->sign == \app\models\investments\InvestCustomerTransaction::SIGN_ADD) {
                    return '<b class="text-red"> + </b><b class="text-red">' . Yii::$app->formatter->asDecimal($model->amount, 8) . '</b>';
                } else {
                    return '<b class="text-red">' . Yii::$app->formatter->asDecimal($model->amount, 8) . '</b>';
                }
            }
        ],
        ['attribute' => 'balance_after',
            'format' => 'raw',
            'contentOptions' => ['style' => 'width: 130px;text-align:right;'],
            'value' => function ($model) {
                return '<b class="text-red">' . Yii::$app->formatter->asDecimal($model->balance_after, 8) . '</b>';
            }
        ],
        [
            'attribute' => 'type',
            'format' => 'html',
            'value' => function ($model) {
                return $model->getTypeLabel();
            }
        ],
        // 'sign',
        // 'type',
        // 'balance_before',
        // 'balance_after',
        // 'created_at',
        // 'updated_at',

        ['class' => 'yii\grid\ActionColumn',
            'template' => '{view}',
            'buttons' => [
                'view' => function ($url, $data) {
                    return Html::a('<span class="fa fa-eye"></span>&nbsp;' . Yii::t('investment', 'Chi tiết'), $url, ['class' => 'btn btn-xs btn-info']);
                }
            ]
        ],
    ],
]); ?>

