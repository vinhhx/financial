<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\investments\InvestCustomerTransaction */

$this->title = Yii::t('investment','Giao dịch số #') . $model->id;
$this->params['breadcrumbs'][Yii::t('investment', 'Danh sách')] = Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="box box-customer">
    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'label' => 'Type of transaction',
                    'value' => $model->getTypeLabel(),
                ],
                'balance_before:decimal',
                [
                    'label' => 'Sign of transaction',
                    'value' => $model->getSignLabel(),
                ],
                'amount:decimal',
                'balance_after:decimal',
                'created_at:datetime',
            ],
        ]) ?>
    </div>
</div>
