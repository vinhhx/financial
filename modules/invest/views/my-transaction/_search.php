<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\invest\forms\InvestCustomerTransactionSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="invest-customer-transaction-search form-search">

    <?php
    $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]);
    ?>
    <ul>
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'id', array('placeholder' => $model->getAttributeLabel('id'), 'style' => 'width:100px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeInput('text', $model, 'customer_username', array('placeholder' => $model->getAttributeLabel('customer_username'), 'style' => 'width:150px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeDropDownList($model, 'type', \app\models\investments\InvestCustomerTransaction::getTypeLabels(), array('prompt' => Yii::t('app', '-- All Type of transaction --'), 'style' => 'width:150px')) ?>
        </li>
        <li class="form-item">
            <?= Html::activeDropDownList($model, 'sign', \app\models\investments\InvestCustomerTransaction::getSignLabels(), array('prompt' => Yii::t('app', '-- All Sign  --'), 'style' => 'width:150px')) ?>
        </li>
<!--        <li class="form-item">-->
<!--            <div class="form-group form-group-date" style="width: 200px; margin-bottom: -25px;">-->
<!--                <label>--><?php //echo $model->getAttributeLabel('created_at') ?><!--</label>-->
<!---->
<!--                <div style="margin-top: -15px;">-->
<!--                    --><?php //echo
//                    \app\components\WGroupDatePicker::widget([
//                        'model' => $model,
//                        'attributes' => ['start_date', 'end_date'],
//                        'formInline' => true,
//                        'dateFormat' => 'dd-MM-yyyy',
//                    ]);
//                    ?>
<!--                </div>-->
<!--            </div>-->
<!--        </li>-->
        <li class="form-item">
            <?= Html::submitButton('<i class="fa fa-search"></i>&nbsp;' . Yii::t('app', 'Search'), ['class' => 'btn btn-primary btn-sm']) ?>
        </li>
    </ul>
    <?php ActiveForm::end(); ?>

</div>
