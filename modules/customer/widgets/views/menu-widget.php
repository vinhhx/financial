<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

?>
    <div class="user-panel">
        <div class="pull-left image">
            <img src="/images/ico-user2.png" class="" alt="User Image">
        </div>
        <div class="pull-left info" style="padding-left: 5px;">
            <div
                style="margin-top: -5px; text-transform: uppercase; margin-bottom: 10px; font-size: 14px;"><?= Yii::$app->user->identity->full_name ?></div>
            <div style="font-size: 10px;">
                <div style="padding-bottom: 5px;">
                    <span>ID: <?= Yii::$app->user->identity->username ?></span>
                </div>
                <div>
                    <span><?= Yii::$app->user->identity->email ?></span>
                </div>
            </div>
        </div>
    </div>
    <ul class="sidebar-menu">
        <?php
        $currentLanguage = Yii::$app->language;
        //        $languages = Yii::$app->params['languages'];
        //        if (in_array($currentLanguage, array_keys($languages))):
        //            list($route, $parmas) = Yii::$app->getUrlManager()->parseRequest(Yii::$app->getRequest());
        //            $parmas = ArrayHelper::merge($_GET, $parmas);
        //            $url = isset($parmas["route"]) ? $parmas["route"] : $route;
        //            foreach ($languages as $key => $lang):
        //                if ($key != $currentLanguage):
        //                    $url_lang = Yii::$app->urlManager->createUrl(ArrayHelper::merge($parmas, [$url, 'lang' => $key]));
        //                    $url_image = Url::to('@web/images/flags/' . $key . '.png');
        //                    ?>
        <!---->
        <!--                    <li>-->
        <!--                        <a href="--><? //= $url_lang ?><!--">-->
        <!--                            <img alt="--><? //= $lang ?><!--" class="lang-flag" src="-->
        <? //= $url_image ?><!--" title="--><? //= $lang ?><!--"-->
        <!--                                 width="20px"> --><? //= $lang ?>
        <!--                        </a>-->
        <!--                    </li>-->
        <!--                    --><?php
        //                endif;
        //            endforeach;
        //        endif; ?>
        <?php if ($config): ?>
            <?php foreach ($config as $menu):
                $cssClass = isset($menu['cssClass']) ? $menu['cssClass'] : '';
                if (isset($menu['children']) && is_array($menu['children'])) {
                    $cssClass .= 'treeview';
                }
                if (isset($menu['selected']) && $menu['selected']) {
                    $cssClass .= ' active';
                }

                ?>
                <li<?php if ($cssClass) echo " class=\"{$cssClass}\"" ?>>
                    <?php if (isset($menu['url']) && $menu['url']): ?>
                        <?= Html::a($menu['label'], $menu['url']); ?>
                    <?php else: ?>
                        <?= Html::a($menu['label'], 'javascript:void(0);'); ?>
                    <?php endif; ?>
                    <?php if (isset($menu['children']) && is_array($menu['children'])): ?>
                        <ul class="treeview-menu">
                            <?php foreach ($menu['children'] as $subMenu):
                                $cssClassSub = '';
                                if (isset($subMenu['selected']) && $subMenu['selected']) {
                                    $cssClassSub = ' active';
                                }
                                ?>
                                <li<?php if ($cssClassSub) echo " class=\"{$cssClassSub}\"" ?>>
                                    <?php if (isset($subMenu['url']) && $subMenu['url']): ?>
                                        <?= Html::a($subMenu['label'], $subMenu['url']); ?>
                                    <?php else: ?>
                                        <?= Html::a($subMenu['label'], 'javascript:void(0);'); ?>
                                    <?php endif; ?>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>

        <?php endif; ?>
        <li>
            <?= Html::a('<i class="bit bit-support"></i>&nbsp;Support', ['/customer/note/index'], [
                'class' => 'add-note',
                'data-table' => \app\models\Customer::tableName(),
                'data-id' => Yii::$app->user->id,
                'data-toggle' => 'modal',
                'data-target' => '#myModal',
                'title' => 'Message',
            ]) ?>
        </li>
    </ul>

<?php /*
<ul class="sidebar-menu">
    <li class="header">Menu</li>

    <li class="treeview <?php echo ($currentController == 'user' || $currentController == 'profile') ? ' active' : '' ?>">
        <a href="#"><i class="fa fa-newspaper-o"></i> <span>Khách hàng</span> <i
                class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li><a href="<?= Url::to(['user/index']); ?>"><i class="fa fa-user"></i> <span>Người dùng</span></a>
            </li>
            <li><a href="<?= Url::to(['profile/index']); ?>"><i class="fa fa-user"></i> <span>Hồ sơ người dùng</span></a>
            </li>
        </ul>
    </li>

    <li><a href="<?= Url::to(['shop/index']); ?>"><i class="fa fa-briefcase"></i> <span>Gian hàng</span></a>
    </li>

    <li class="treeview <?php echo ($currentController == 'product') ? ' active' : '' ?>">
        <a href="#"><i class="fa fa-newspaper-o"></i> <span>Sản phẩm</span> <i
                class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li><a href="<?= Url::to(['product/category-index']); ?>"><i class="fa fa-list-alt"></i> <span>Danh mục sản phẩm</span></a>
            </li>
        </ul>
    </li>

    <li class="treeview <?php echo ($currentController == 'page' || $currentController == 'menu') ? ' active' : '' ?>">
        <a href="#"><i class="fa fa-newspaper-o"></i> <span>Trang nội dung</span> <i
                class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li><a href="<?= Url::to(['page/category']); ?>"><i class="fa fa-angle-double-right"></i> Danh
                    mục</a></li>
            <li><a href="<?= Url::to(['page/index']); ?>"><i class="fa fa-angle-double-right"></i> Bài viết</a>
            </li>
        </ul>
    </li>

    <li class="treeview <?php echo ($currentController == 'banner-system' || $currentController == 'build-layout') ? ' active' : '' ?>">
        <a href="#"><i class="fa fa-newspaper-o"></i> <span>Giao diện</span> <i
                class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li><a href="<?= Url::to(['banner-system/index']); ?>"><i class="fa fa-file-image-o"></i> <span>Banner</span></a>
            </li>
            <li><a href="<?= Url::to(['build-layout/index']); ?>"><i class="fa fa-angle-double-right"></i>Box
                    sản phẩm trang chủ</a></li>
            <li><a href="<?= Url::to(['menu/index']); ?>"><i class="fa fa-angle-double-right"></i> Menu</a>
            </li>
        </ul>
    </li>

    <li><a href="<?= Url::to(['wallet/receipt-note/index']); ?>"><i class="fa fa-hand-o-right"></i> <span>Phiếu thu tiền</span></a>
    </li>

    <li><a href="<?= Url::to(['order/index']); ?>"><i class="fa fa-hand-o-right"></i> <span>Đơn hàng</span></a>
    </li>

    <li><a href="<?= Url::to(['express-logistic-company/index']); ?>"><i class="fa fa-hand-o-right"></i>
            <span>Hãng vận chuyển</span></a></li>

    <li class="treeview <?php echo ($currentController == 'system-stock-receipt-note') ? ' active' : '' ?>">
        <a href="#"><i class='fa fa-hand-o-right'></i> <span>Kho hàng</span> <i
                class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li><a href="<?= Url::to(['system-stock/index']); ?>"><i class="fa fa-angle-double-right"></i>Quản
                    lý kho hàng</a></li>
            <li><a href="<?= Url::to(['system-stock-receipt-note/index']); ?>"><i
                        class="fa fa-angle-double-right"></i>Phiếu nhận hàng</a></li>
        </ul>
    </li>


    <li class="treeview <?php echo ($currentController == 'censor') ? ' active' : '' ?>">
        <a href="#"><i class='fa fa-flag'></i> <span>Kiểm duyệt</span> <i
                class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li><a href="<?= Url::to(['censor/censor-product']); ?>"><i class="fa fa-ticket"></i>Sản phẩm
                    chờ kiểm duyệt</a></li>
            <li><a href="<?= Url::to(['censor/censor-required']); ?>"><i class="fa fa-bars"></i>Danh sách
                    yêu cầu kiểm duyệt</a></li>
            <li><a href="<?= Url::to(['censor/censor-checked']); ?>"><i class="fa fa-check"></i>Danh sách SP
                    đã qua kiểm duyệt</a></li>
            <li><a href="<?= Url::to(['censor/view-log']); ?>"><i class="fa fa-users"></i>Log nhân viên
                    chỉnh sủa</a></li>

        </ul>
    </li>
    <li class="treeview <?php echo ($currentController == 'merchant') ? ' active' : '' ?>">
        <a href="#"><i class='fa fa-google-wallet'></i> <span>Merchant</span> <i
                class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li><a href="<?= Url::to(['/admin/wallet/merchant/index']); ?>"><i
                        class="fa fa-angle-double-right"></i>
                    Quản lý merchant</a></li>
            <li><a href="<?= Url::to(['/admin/wallet/merchant/create']); ?>"><i
                        class="fa fa-angle-double-right"></i>
                    Thêm mới merchant</a></li>
        </ul>
    </li>
    <li class="treeview <?php echo ($currentController == 'bad-word') ? ' active' : '' ?>">
        <a href="#"><i class='fa fa-exclamation-triangle'></i> <span>Lọc từ đen</span> <i
                class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li><a href="<?= Url::to(['bad-word/index']); ?>"><i class="fa fa-angle-double-right"></i>
                    Quản lý từ đen</a></li>
            <li><a href="<?= Url::to(['bad-word/create']); ?>"><i class="fa fa-angle-double-right"></i>
                    Thêm từ đen</a></li>
        </ul>
    </li>

    <li class="treeview <?php echo ($currentController == 'admin' || $currentController == 'permission') ? ' active' : '' ?>">

        <a href="#"><i class='fa fa-gears'></i> <span>Hệ thống</span> <i
                class="fa fa-angle-left pull-right"></i></a>
        <ul class="treeview-menu">
            <li><a href="<?= Url::to(['admin/index']); ?>"><i class="fa fa-angle-double-right"></i> Quản trị
                    viên</a></li>
            <li><a href="<?= Url::to(['permission/index']); ?>"><i class="fa fa-angle-double-right"></i>
                    Quản lý phân quyền</a></li>
            <li><a href="<?= Url::to(['bank/index']); ?>"><i class="fa fa-angle-double-right"></i> Ngân hàng</a>
            </li>
            <li><a href="<?= Url::to(['config/']); ?>"><i class="fa fa-gear"></i>Cấu hình</a></li>
        </ul>
    </li>


</ul>

 */ ?>