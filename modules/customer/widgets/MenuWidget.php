<?php

/**
 * Created by PhpStorm.
 * User: Quan
 * Date: 8/27/2015
 * Time: 2:22 PM
 */

namespace app\modules\customer\widgets;

use Yii;
use yii\base\Widget;
use yii\db\mysql\Schema;
use yii\helpers\Url;

class MenuWidget extends Widget
{

    public function run()
    {

        $config = [
            [
                'label' => '<i class="bit bit-dashboard"></i>&nbsp;' . Yii::t('customers', 'Dashboard'),
                'url' => ['default/index'],
            ],
            [
                'label' => '<i class="bit bit-account"></i>&nbsp;' . Yii::t('customers', 'Account'),
                'url' => null,
                'children' => [
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('customers', 'My bitcoin wallet address'),
                        'url' => ['my-account/my-bank'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('customers', 'Account balance'),
                        'url' => ['my-account/balance'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('customers', 'Account request histories'),
                        'url' => ['my-account/request-history'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('customers', 'Transaction'),
                        'url' => ['my-account/transaction'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('customers', 'Orders'),
                        'url' => ['my-account/order-index'],
                    ],

                ]
            ],
            [
                'label' => '<i class="bit bit-balance"></i>&nbsp;' . Yii::t('customers', 'Token'),
                'url' => null,
                'children' => [
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('customers', 'Token List'),
                        'url' => ['customer-token/index'],
                    ],
                    [
                        'label' => '<i class="fa fa-circle-o"></i>&nbsp;' . Yii::t('customers', 'Transaction History'),
                        'url' => ['customer-token/transaction-history'],
                    ],
                ]
            ],
            [
                'label' => '<i class="bit bit-member"></i>&nbsp;' . Yii::t('customers', 'Genealogy'),
                'url' => ['my-account/member'],
            ],
//            [
//                'label' => '<i class="fa fa-university"></i>&nbsp;' . Yii::t('customers', 'Bank Address'),
//                'url' => ['customer-bank-address/index'],
//            ],

//            [
//                'label' => '<i class="fa fa-comments-o"></i>&nbsp;' . Yii::t('customers', 'Complains'),
//                'url' => ['complain/index'],
//            ],
            [
                'label' => '<i class="bit bit-sign-out"></i>&nbsp;' . Yii::t('customers', 'Logout'),
                'url' => ['default/logout'],
            ],
            /*[
                'label' => '<i class="bit bit-support"></i>&nbsp;' . Yii::t('customers', 'Support'),
                'url' => ['default/index'],
            ],*/

        ];

        // Current Module
//        $prefix = Yii::$app->controller->module->uniqueId;

        // current action
        $action = '/' . Yii::$app->controller->action->uniqueId;

        $parentAction = isset($this->getView()->params["parentAction"]) ? $this->getView()->params["parentAction"] : "";

        // Remove wallet tránh lỗi URL
        foreach ($config as $k => $cfg) {
            if (isset($cfg['children'])) {
                foreach ($cfg['children'] as $k1 => $child) {
                    if (isset($child['url']) && $child['url']) {
//                        $child['url'][0] = $prefix . $child['url'][0];
                        $prefix = '/customer/' . $child['url'][0];
                        $child['url'] = Url::to($child['url']);
                        // Menu đang được chọn
                        if ($prefix == $action || $prefix == $parentAction) {
                            $cfg['selected'] = true;
                            $child['selected'] = true;
                        }
                        //
                        $cfg['children'][$k1] = $child;
                    }
                }
            }
            if (isset($cfg['url']) && $cfg['url']) {
//                $cfg['url'][0] = $prefix . $cfg['url'][0];
                $cfg['url'] = Url::to($cfg['url']);
                if ($cfg['url'] == $action || $cfg['url'] == $parentAction) {
                    $cfg['selected'] = true;
                }
            }
            $config[$k] = $cfg;
        }

        return $this->render('menu-widget', [
            'currentController' => Yii::$app->controller->id,
            'config' => $config
        ]);
    }

}
