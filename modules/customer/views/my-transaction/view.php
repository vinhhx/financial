<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerTransaction */

$this->title = Yii::t('transactions', 'Transaction id: #') . $model->id;
$this->params['breadcrumbs'][Yii::t('transactions', 'Customer Transactions')] = \yii\helpers\Url::to(['my-transaction/index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="box box-warning">
    <div class="box-body">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'balance_before:integer',

                [
                    'attribute' => 'amount',
                    'format' => 'raw',
                    'value' => ($model->sign == $model::SIGN_ADD) ? '<b class="text-red"> + </b>' . Yii::$app->formatter->asDecimal($model->amount, 8) : (($model->sign == $model::SIGN_SUB) ? '<b class="text-green"> - </b>' . Yii::$app->formatter->asInteger($model->amount) : Yii::$app->formatter->asDecimal($model->amount, 8)),
                ],
                [
                    'attribute' => 'amount',
                    'format' => 'html',
                    'value' => isset(\app\models\CustomerTransaction::getTypeUserTransaction()[$model->type]) ? \app\models\CustomerTransaction::getTypeUserTransaction()[$model->type] : "--",
                ],
                [
                    'label' => Yii::t('transactions', 'reason'),
                    'format' => 'html',
                    'value' => $model->getReason(),
                ],
                'balance_after:integer',
                'created_at:datetime',
            ],
        ]) ?>
    </div>


</div>
