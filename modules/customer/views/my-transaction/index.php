<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\forms\CustomerTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('customers', 'Customer Transactions');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="box box-primary">
    <div class="box-body">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
</div>
<div class="box box-warning">
    <div class="box-body table-responsive no-padđing">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
//            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'customer_username',
                [
                    'attribute' => 'type',
                    'format' => 'html',
                    'value' => function ($data) {
                        return \app\models\CustomerTransaction::getResonTransaction($data);
                    }
                ],
                ['attribute' => 'balance_before',
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'width: 130px;text-align:right;'],
                    'value' => function ($data) {
                        return Yii::$app->formatter->asDecimal($data->balance_before, 8);
                    }
                ],

                [
                    'attribute' => 'amount',
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'width: 130px;text-align:right;'], // <-- set chiều rông
                    'value' => function ($data) {
//                        $valueStr='';
                        if ($data->sign == \app\models\CustomerTransaction::SIGN_SUB) {
                            return '<b class="text-green">-</b>' . Yii::$app->formatter->asDecimal($data->amount, 8);
                        } elseif ($data->sign == \app\models\CustomerTransaction::SIGN_ADD) {
                            return '<b class="text-red">+</b>' . Yii::$app->formatter->asDecimal($data->amount, 8);
                        } else {
                            return Yii::$app->formatter->asDecimal($data->amount, 8);
                        }
                    }

                ],

                // 'status',

                ['attribute' => 'balance_after',
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'width: 130px;text-align:right;'],
                    'value' => function ($data) {
                        return Yii::$app->formatter->asDecimal($data->balance_after, 8);
                    }
                ],
                'created_at:datetime',

                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                    'buttons' => [
                        'view' => function ($url, $data) {
                            return Html::a('<span class="fa fa-eye"></span>&nbsp;' . Yii::t('transactions', 'View Detail'), $url, ['class' => 'btn btn-xs btn-info']);
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>


</div>
