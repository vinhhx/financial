<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\forms\CustomerTokenTransactionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Customer Token Transactions');
$this->params['pageTitle'] = $this->title;
?>
<div class="customer-token-transaction-index">


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'grid-view grid-view-customize'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//            'bill_id',
            [
                'attribute' => 'quantity',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width: 130px;text-align:right;'], // <-- set chiều rông
                'value' => function ($model) {
//                        $valueStr='';
                    if ($model->sign == \app\models\CustomerTokenTransaction::SIGN_SUB) {
                        return '<b class="text-green"> - </b><b class="text-red">' . Yii::$app->formatter->asInteger($model->quantity) . '</b>';
                    } elseif ($model->sign == \app\models\CustomerTokenTransaction::SIGN_ADD) {
                        return '<b class="text-red"> + </b><b class="text-red">' . Yii::$app->formatter->asInteger($model->quantity) . '</b>';
                    } else {
                        return '<b class="text-red">' . Yii::$app->formatter->asInteger($model->quantity) . '</b>';
                    }
                }
            ],
            'receiver',
            [
                'attribute' => 'content',
                'format' => 'raw',
                'contentOptions' => ['style' => 'width: 130px;text-align:left;'], // <-- set chiều rông
                'value' => function ($model) {
//                        $valueStr='';
                    $data = json_decode($model->content, true);
                    $html = implode($data, '<br/>');
                    return $html;
                }
            ],
            // 'type',
             'created_at:datetime',
            // 'updated_at',
            // 'type_module',

        ],
    ]); ?>
</div>
