<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('customers', 'Information');
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $model->full_name;
?>
<div class="box box-info">
    <div class="box-body table-responsive no-padding">
        <?= \yii\widgets\DetailView::widget([
            'model' => $model,
            'attributes' => [
                /*'id',*/
                'username',
                ['attribute' => 'password_hash',
                    'format' => 'raw',
                    'value' => '****** &nbsp;&nbsp' . Html::a('<span class="fa fa-key"></span>&nbsp;' . Yii::t('customers', 'Change password'), ['change-password'], ['class' => 'btn btn-xs btn-primary']),
                ],
                [
                    'attribute' => 'package_id',
                    'format' => 'raw',
                    'value' => isset($model->package->package_name) ? $model->package->package_name : "--",
                ],
                [
                    'attribute' => 'amount',
                    'format' => 'raw',
                    'value' => Yii::$app->formatter->asDecimal($model->amount, 8),
                ],
                [
                    'attribute' => 'date_join',
                    'format' => 'raw',
                    'value' => Yii::$app->formatter->asDate($model->date_join, 'php:m-d-Y'),
                ],
                'full_name',
                [
                    'attribute' => 'Passport',
                    'format' => 'raw',
                    'value' => $model->identity_card,
                ],
//                [
//                    'attribute' => 'dob',
//                    'format' => 'raw',
//                    'value' => Yii::$app->formatter->asDate($model->dob, 'php:m-d-Y'),
//                ],
                'phone',
                'email',
                'address',
            ],
        ]) ?>
    </div>
</div>
