<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\forms\search\CustomerRequireCashoutSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('customers', 'Customer Require Cashouts');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box box-warning">
    <div class="box-header">
        <div class="box-tools pull-right">
            <?php if (Yii::$app->user->identity->requireShowCashout()):
                echo Html::a('<span class="fa fa-square-plus-o"></span>&nbsp;' . Yii::t('customers', 'Require cash out'), ['require-cash-out'], ['class' => 'btn btn-success btn-sm']);
            endif;
            ?>

        </div>

    </div>
    <div class="box-body table-responsive no-padđing">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                [
                    'attribute' => 'amount',
                    'format' => 'raw',
                    'contentOptions' => ['style' => 'width: 130px;text-align:right;', 'class' => 'text-red'], // <-- set chiều rông
                    'value' => function ($data) {
                        return Yii::$app->formatter->asInteger($data->amount);
                    }

                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return isset(\app\models\CustomerRequireCashout::getStatusLabels()[$data->status]) ? \app\models\CustomerRequireCashout::getStatusLabels()[$data->status] : "--";
                    }
                ],
                // 'approved_by',
                [
                    'attribute' => 'created_at',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Yii::$app->formatter->asDatetime($data->created_at, 'php:Y-m-d');
                    }
                ],
                [
                    'attribute' => 'out_of_date',
                    'format' => 'raw',
                    'value' => function ($data) {
                        return Yii::$app->formatter->asDatetime(strtotime($data->out_of_date), 'php:Y-m-d');
                    }
                ],
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn',
                    'template' => '{delete-cash-out}',
                    'buttons' => [
                        'delete-cash-out' => function ($url, $model) {
                            if ($model->status == \app\models\CustomerRequireCashout::STATUS_PENDING) {
                                return Html::a('<span class="fa fa-ban"></span>&nbsp;' . Yii::t('app', 'Cancel'), \yii\helpers\Url::to(['my-account/delete-cash-out', 'id' => $model->id]), [
                                    'class' => 'btn btn-xs btn-warning btn-action',
                                    'title' => Yii::t('app', 'Cancel'),
                                    'data-confirm' => Yii::t('app', 'Are you sure, you want to cancel this item?'),
                                    'data-method' => 'post',
                                    'data-pjax' => 0,
                                    'aria-label' => Yii::t('app', 'Cancel'),
                                ]);
                            }
                        }
                    ]
                ],
            ],
        ]); ?>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


</div>
