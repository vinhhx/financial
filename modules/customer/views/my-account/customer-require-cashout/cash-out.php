<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 14-Dec-15
 * Time: 11:34 PM
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

$this->title = Yii::t('customers', 'Cash out');
$this->params['breadcrumbs']['My account'] = Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$totalRequestCashout = Yii::$app->user->identity->getRequiredCashout();
$amountAtleast = Yii::$app->user->identity->getBalanceAtLeast();
$suggestAmount = Yii::$app->user->identity->amount - floatval($amountAtleast) - floatval($totalRequestCashout);
$suggestAmount = ($suggestAmount > 0) ? (int)$suggestAmount : 0;

?>
<div class="box box-warning">
    <div class=" box-body">
        <?php $form = ActiveForm::begin([]); ?>
        <div class="form-group field-customer-amount has-success">
            <label class="control-label"
                   for="customer-amount"><?= Yii::t('customers', 'Greatest amount can withdrawn') ?></label>
            <?= Html::input('text', 'Customer[amount_max]', Yii::$app->formatter->asDecimal(Yii::$app->user->identity->amount, 1), ['class' => 'text-red text-right form-control', 'style' => 'font-weight:bold;width:250px', 'disabled' => 'disabled']) ?>
        </div>
        <?php if ($totalRequestCashout > 1): ?>
            <div class="form-group field-customer-amount has-success">
                <label class="control-label"
                       for="customer-amount"><?= Yii::t('customers', 'Account balance at least') ?></label>
                <?= Html::input('text', 'Customer[amount_cashedout]', Yii::$app->formatter->asDecimal($totalRequestCashout, 1), ['class' => 'text-red text-right form-control', 'style' => 'font-weight:bold;width:250px', 'disabled' => 'disabled']) ?>
            </div>
        <?php endif; ?>
        <div class="form-group field-customer-amount has-success">
            <label class="control-label"
                   for="customer-amount"><?= Yii::t('customers', 'Account balance at least') ?></label>
            <?= Html::input('text', 'Customer[amount_min]', Yii::$app->formatter->asDecimal($amountAtleast, 1), ['class' => 'text-red text-right form-control', 'style' => 'font-weight:bold;width:250px', 'disabled' => 'disabled']) ?>
        </div>

        <?= $form->field($model, 'amount')->textInput(['disabled' => ($suggestAmount < 25) ? true : false, 'style' => 'width:250px;font-weight:bold', 'class' => 'text-red text-right form-control ', 'value' => $suggestAmount]); ?>
        <?php if (Yii::$app->user->identity->requireShowCashout()): ?>
            <div class="form-group">
                <label class="control-label"></label>
                <?= Html::submitButton('<span class="fa fa-floppy-o"></span>&nbsp;' . Yii::t('customers', 'Cash Out'), ['class' => 'btn btn-sm btn-primary']) ?>
            </div>
        <?php endif; ?>
        <?php ActiveForm::end() ?>
    </div>
</div>
