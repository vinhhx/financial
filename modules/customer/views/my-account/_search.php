<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\forms\search\CustomerSearch */
/* @var $form yii\widgets\ActiveForm */

$package=\yii\helpers\ArrayHelper::map(\app\models\Package::getPackages(),'id','package_name');
?>

<div class="customer-search form-search">

    <?php $form = ActiveForm::begin([
        'action' => ['view-children-account'],
        'method' => 'get',
    ]); ?>

    <ul style="margin-left:15px">

            <li class="form-item">
                <?= Html::activeInput('text', $model, 'customer_username', array('placeholder' =>  Yii::t('customer','User Name'), 'style' => 'width:120px')) ?>
            </li>

        <li class="form-item">
                <?= Html::activeDropDownList($model, 'package_id', $package, array('prompt' =>  Yii::t('customer','--Select package--'), 'style' => 'width:150px')) ?>
            </li>
            <li class="form-item">
                <div class="form-group form-group-date" style="width: 200px; margin-bottom: -25px;">
                    <label><?= Yii::t('customer','Date Join') ?></label>
                    <div style="margin-top: -15px;">
                        <?=
                        \app\components\WGroupDatePicker::widget([
                            'model' => $model,
                            'attributes' => ['start_date', 'end_date'],
                            'formInline' => true,
                            'dateFormat' => 'dd-MM-yyyy',
                        ]);
                        ?>
                    </div>
                </div>
            </li>

        <li class="form-item">
            <?= Html::submitButton('<i class="fa fa-search"></i>&nbsp;'. Yii::t('app','Search'), ['class' => 'btn btn-primary btn-sm']) ?>
            <?= Html::resetButton('<i class="fa fa-refresh"></i>&nbsp;'. Yii::t('app','Reset'), ['class' => 'btn btn-default btn-sm btn-second-horizontal']) ?>
        </li>
    </ul>

    <?php ActiveForm::end(); ?>

</div>
