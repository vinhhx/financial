<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\forms\search\CustomerBankAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My Members');
$this->params['breadcrumbs'][$this->title] = '';
$this->params['pageTitle'] = $this->title;
$this->registerCssFile('/tree/css/folder-tree-static.css?v=1.0.0', ['depends' => \app\assets\AdminLteAsset::className()]);
$this->registerCssFile('/tree/css/context-menu.css?v=1.0.0', ['depends' => \app\assets\AdminLteAsset::className()]);
$this->registerJsFile('/tree/js/folder-tree-static.js', ['depends' => \app\assets\AdminLteAsset::className()]);
$this->registerJsFile('/tree/js/context-menu.js', ['depends' => \app\assets\AdminLteAsset::className()]);


?>
    <ul id="dhtmlgoodies_tree" class="dhtmlgoodies_tree">
        <li><a href="#" id="node_1">Europe</a>
            <ul>
                <li parentId="1"><a href="#" id="node_2">Loading...</a></li>
            </ul>
        </li>
        <li><a href="#" id="node_3">Asia</a>
            <ul>
                <li parentId="2"><a href="#" id="node_4">Loading...</a></li>
            </ul>
        </li>
        <li><a href="#" id="node_5">Africa</a>
            <ul>
                <li parentId="3"><a href="#" id="node_6">Loading...</a></li>
            </ul>
        </li>
        <li><a href="#" id="node_7">America</a>
            <ul>
                <li parentId="4"><a href="#" id="node_8">Loading...</a></li>
            </ul>
        </li>
    </ul>
    <a href="#" onclick="expandAll('dhtmlgoodies_tree');return false">Expand all</a>
    <a href="#" onclick="collapseAll('dhtmlgoodies_tree');return false">Collapse all</a>


    <p>Static tree</p>
    <ul id="dhtmlgoodies_tree2" class="dhtmlgoodies_tree">
        <li><a href="#" id="node_100">Europe</a>
            <ul>
                <li><a href="#" id="node_101">Norway</a>
                    <ul>
                        <li><a href="#" id="node_102">Rogaland</a>
                            <ul>
                                <li class="dhtmlgoodies_sheet.gif"><a href="#" id="node_103">Stavanger</a></li>
                                <li class="dhtmlgoodies_sheet.gif"><a href="#" id="node_104">Haugesund</a></li>
                            </ul>
                        </li>
                        <li class="dhtmlgoodies_sheet.gif"><a href="#" id="node_105">Hordaland</a></li>
                        <li class="dhtmlgoodies_sheet.gif"><a href="#" id="node_106">Oslo</a></li>
                    </ul>
                </li>
                <li><a href="#" id="node_107">United Kingdom</a>
                    <ul>
                        <li><a href="#" id="node_108">London</a></li>
                        <li><a href="#" id="node_109">Manchester</a></li>
                        <li><a href="#" id="node_110">Oxford</a></li>
                    </ul>
                </li>
                <li><a href="#" id="node_111">Sweden</a></li>
                <li><a href="#" id="node_112">Denmark</a></li>
                <li><a href="#" id="node_113">Germany</a></li>
            </ul>
        </li>
        <li><a href="#" id="node_114">Asia</a></li>
        <li><a href="#" id="node_115">Africa</a>
            <ul>
                <li><a href="#" id="node_116">Tanzania</a></li>
                <li><a href="#" id="node_117">Kenya</a></li>
            </ul>
        </li>
        <li><a href="#" id="node_118">America</a>
            <ul>
                <li class="dhtmlgoodies_sheet.gif"><a href="#" id="node_119">Canada</a></li>
                <li><a href="#" id="node_120">United States</a></li>
                <li class="dhtmlgoodies_sheet.gif"><a href="#" id="node_121">Mexico</a></li>
                <li><a href="#" id="node_122">Argentina</a></li>
            </ul>
        </li>
    </ul>
    <a href="#" onclick="expandAll('dhtmlgoodies_tree2');return false">Expand all</a>
    <a href="#" onclick="collapseAll('dhtmlgoodies_tree2');return false">Collapse all</a>

    <ul id="contextMenu">
        <li><a href="#" onclick="addNewNode(event);return false">Add new node</a></li>
        <li><a href="#" onclick="deleteNode(event);return false">Delete node</a></li>
    </ul>
<?php
$js = <<<EOD
initContextMenu();
EOD;
$this->registerJs($js);
?>