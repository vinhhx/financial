<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\forms\search\CustomerBankAddressSearch */
/* @var $model \app\models\Customer */

$this->title = Yii::t('app', 'My Members');
$this->params['breadcrumbs'][$this->title] = '';
$this->params['pageTitle'] = $this->title;
$this->registerCssFile('css/tree-customer.css?v=1.0.2', ['depends' => \app\assets\AdminLteAsset::className()]);

?>
    <div>
        <a href="javascript:;" id="expand-all">
            Expand all
        </a>
        <span style="padding: 0 5px">|</span>
        <a href="javascript:;" id="collapse-all">
            Collapse all
        </a>
    </div>
    <div class="list-members">
        <div class="middle customer-tree">
            <?php echo $model; ?>
        </div>
    </div>
<?php
$js = <<<EOD
    $('body').on('click', '.children-active > a', function(e){
        var _this = $(this).parent(); 
        if(_this.children('ul').is(':hidden')){  
            _this.children('ul').show();
            _this.css('background-image', 'url(/images/minus.gif)');
        }else{
            _this.children('ul').hide();
            _this.css('background-image', 'url(/images/plus.gif)');
        }
    }).on('click', '#expand-all', function(e){
        $('.customer-tree > ul > li ul').show();
        $('li.children-active').css('background-image', 'url(/images/minus.gif)');
    }).on('click', '#collapse-all', function(e){
        $('.customer-tree > ul > li > ul > li ul').hide();
        $('li.children-active').css('background-image', 'url(/images/plus.gif)');
    });
EOD;
$this->registerJs($js);
?>