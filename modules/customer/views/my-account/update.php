<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = Yii::t('customers', 'Update {modelClass}: ', [
        'modelClass' => 'Customer',
    ]) . ' ' . $model->id;
$this->params['breadcrumbs'][Yii::t('customers', 'My account')] = \yii\helpers\Url::to(['my-account/index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<?= $this->render('_form', [
    'model' => $model,
    'packageList' => $packageList,
    'accountInvite' => $accountInvite,
]) ?>

