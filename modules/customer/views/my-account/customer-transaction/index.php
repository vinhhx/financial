<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\forms\search\CustomerBankAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My Transaction');
$this->params['breadcrumbs']["Account"] = \yii\helpers\Url::to(['my-account/index']);
$this->params['pageTitle'] = $this->title;
?>
<div class="customer-bank-address-index">

    <!--    --><?php //echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box box-customer">
    </div>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'grid-view grid-view-customize table-responsive no-padding'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            /*[
                'label' => Yii::t('customers', 'Mã phiếu'),
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->customer_sent_id == Yii::$app->user->id) {
                        return $model->customer_sent_request_id;
                    } else {
                        return $model->customer_receiver_request_id;
                    }
                }
            ],*/
            [
                'label' => Yii::t('customers', 'Loại phiếu'),
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->customer_sent_id == Yii::$app->user->id) {
                        return Html::label('Transfer money', '', ['class' => 'label label-danger']);
                    } else {
                        return Html::label('Withdraw', '', ['class' => 'label label-success']);
                    }
                }

            ],
            /*[
                'label' => Yii::t('customers', 'Người Nhận/Chuyển'),
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->customer_sent_id == Yii::$app->user->id) {
                        return $model->customer_receiver_username;
                    } else {
                        return $model->customer_sent_username;
                    }
                }

            ],*/
            [
                'label' => Yii::t('customers', 'Địa chỉ Nhận/Chuyển'),
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->customer_sent_id == Yii::$app->user->id) {
                        return $model->customer_receiver_bank_address;
                    } else {
                        return $model->customer_sent_bank_address;
                    }
                }

            ],
            [
                'label' => Yii::t('customers', 'Trạng thái'),
                'format' => 'raw',
                'value' => function ($model) {
                    return \app\models\CustomerTransaction::getStatusLabels($model->status);
                }

            ],
            [
                'label' => Yii::t('customers', 'Lý do'),
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->customer_sent_id == Yii::$app->user->id) {
                        return \app\models\CustomerTransaction::getTypeLabels($model->customer_sent_type);
                    } else {
                        return \app\models\CustomerTransaction::getTypeLabels($model->customer_receiver_type);
                    }

                }
            ],
            [
                'label' => Yii::t('customers', 'Số tiền'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDecimal($model->amount, 8);
                }
            ],
            [
                'label' => Yii::t('customers', 'File đính kèm'),
                'format' => 'raw',
                'value' => function ($model) {
                    if ($model->attachment):
                        return Html::a(Html::img(Yii::$app->image->getImg($model->attachment, '100x100'), ['class' => 'image image-responsive']), Yii::$app->image->getImg($model->attachment), ['target' => '_blank']);
                    endif;
                }
            ],

            [
                'label' => Yii::t('customers', 'Tạo lúc'),
                'format' => 'raw',
                'value' => function ($model) {
                    return Yii::$app->formatter->asDatetime($model->created_at);
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="fa fa-eye"></span>&nbsp;&nbsp;' . Yii::t('app', 'Chi tiết'), ['transaction-view', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-xs'
                        ]);
                    },
//                    'update' => function ($url, $model, $key) {
//                        return Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], [
//                            'class' => 'btn btn-primary btn-xs'
//                        ]);
//                    },
//                    'delete' => function ($url, $model, $key) {
//                        return Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
//                            'class' => 'btn btn-danger btn-xs',
//                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
//                            'data-method' => 'post',
//                            'data-pjax' => '0',
//                        ]);
//                    }
                ]
            ],
        ],

    ]); ?>
</div>
