<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\forms\search\CustomerBankAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Account balance');
$this->params['breadcrumbs']["Account"] = \yii\helpers\Url::to(['my-account/index']);
$this->params['pageTitle'] = $this->title;
?>
<div class="customer-bank-address-index">

    <!--    --><?php //echo $this->render('_search', ['model' => $searchModel]); ?>
    <p style="font-size: 14px">
        Account balance: <b class="text-danger"
                            style="font-size: 20px;"><?= Yii::$app->formatter->asDecimal(Yii::$app->user->identity->getCurrentWallet()->poundage, 8) ?>
            BTC</b>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'grid-view grid-view-customize table-responsive no-padding'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'from_username',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center text-bold'],
                'value' => function ($model) {
                    return $model->from_username;
                }
            ],
            [
                'label' => 'Transaction type',
                'format' => 'raw',
                'contentOptions' => ['class' => 'text-center text-bold'],
                'value' => function ($model) {
                    if ($model->sign == \app\models\CustomerPoundageTransaction::SIGN_ADD) {
                        return '<b class="btn btn-xs btn-success btn-type">+</b>';
                    } else {
                        return '<b class="btn btn-xs btn-danger btn-type">-</b>';
                    }
                }

            ],
            [
                'attribute' => 'balance_before',
                'format' => 'raw',
                'contentOptions' => ['class' => ' text-right text-red'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asDecimal($model->balance_before, 8);
                }

            ], [
                'attribute' => 'amount',
                'format' => 'raw',
                'contentOptions' => ['class' => ' text-right text-red'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asDecimal($model->amount, 8);
                }

            ],
            [
                'attribute' => 'balance_after',
                'format' => 'raw',
                'contentOptions' => ['class' => ' text-right text-red'],
                'value' => function ($model) {
                    return Yii::$app->formatter->asDecimal($model->balance_after, 8);
                }

            ],
            [
                'attribute' => 'level',
                'format' => 'raw',
                'contentOptions' => ['class' => ' text-center text-blue text-bold'],
                'value' => function ($model) {
                    return 'F' . $model->level;
                }

            ],
            [
                'label' => 'Reason',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getTypeLabel();
                }
            ],
            [
                'attribute' => 'status',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->getStatusLabel();
                }
            ],


        ],

    ]); ?>
</div>
<style>
    .btn.btn-type {
        width: 20px;
        height: 20px;
        line-height: 16px;
        text-align: center !important;
        font-weight: bold;
        font-size: 20px;
        border-radius: 99px;
        padding: 0 0 0 2px !important;
    }
</style>