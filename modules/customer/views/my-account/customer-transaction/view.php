<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomerBankAddress */

$this->title = Yii::t('customers', 'Chi tiết giao dịch') . ' #' . $model->id;
$this->params['breadcrumbs'][Yii::t('app', 'Account')] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][Yii::t('app', 'My transaction')] = \yii\helpers\Url::to(['transaction']);
$this->params['breadcrumbs'][] = Yii::t('customers', 'Chi tiết');
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/customer/my-account/transaction';
?>
<div class="page-view-transaction">
    <h2 class="page-title"><?= Yii::t('customers', 'Chi tiết giao dịch') ?></h2>

    <div class="page-content">
        <p class="order-code"><?= Yii::t('customers', 'Giao dịch') ?>: <?= $model->id ?></p>
        <?= Yii::t('customers', 'Thành viên của hệ thống') ?>
        <?= ($model->customer_sent_id == Yii::$app->user->id) ? ' request assistance ' : ' receiver ' ?>
        <?= Yii::t('customers', 'với số tiền') ?>
        <?= Yii::$app->formatter->asDecimal($model->amount, 8) . Yii::$app->params["unit"] ?> BTC
        <table class="table table-bordered table-hover dataTable">
            <tbody>
            <tr>
                <td><?= Yii::t('customers', 'Bitcoin wallet address') ?></td>
                <td><?= $model->customer_receiver_bank_address ?></td>
            </tr>
            </tbody>
        </table>
        <div class="notice">
            <?= Yii::t('customers', 'Sau khi bạn nhận được tiền bạn hãy xác nhận bằng cách click nút xác nhận') ?>
            <p class="text-danger">
                <?= Yii::t('customers', 'Không xác nhận khi tiền chưa về tài khoản của bạn, hãy xác nhận khi tiền chắc chắn về tài khoản của bạn') ?>
            </p>
        </div>

        <table class="table table-bordered table-hover dataTable">
            <tbody>
            <tr>
                <td><?= Yii::t('customers', 'Transfer user code') ?></td>
                <td><?= $model->customer_sent_ref_code ?></td>
            </tr>
            </tbody>
        </table>
        <?php if ($model->canUpload()): ?>
            <?php $form = \yii\widgets\ActiveForm::begin([
                'id' => 'upload-transaction-bill'
            ]) ?>
            <div class="bg-file-upload">
                <?= $form->field($modelForm, 'file')->fileInput()->label(false) ?>
            </div>
            <?= Html::submitButton('Upload', ['class' => 'btn btn-danger btn-sm']) ?>
            <?= $form->field($modelForm, 'transaction_id')->hiddenInput()->label(false) ?>

            <?php \yii\widgets\ActiveForm::end() ?>
        <?php else:
            if ($model->attachment):
                ?>
                <?= Html::a(Html::img(Yii::$app->image->getImg($model->attachment, '100x100')), Yii::$app->image->getImg($model->attachment), ['target' => '_blank']) ?>
            <?php endif; ?>
        <?php endif; ?>

        <?php
        if ($model->canApproved()): ?>
            <div class="clearfix"></div>
            <?php echo Html::a(Yii::t('customers', 'Xác nhận'), 'javacript:void(0)', ['class' => 'btn btn-sm btn-success btn-approved-transaction', 'data-order-id' => $model->id]); ?>
        <?php endif;
        ?>
    </div>
</div>
