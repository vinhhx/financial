<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box box-warning">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box-header with-border">
        <h3 class="box-title"><?= Yii::t('customers', 'Information Account') ?></h3>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'username')->textInput(['maxlength' => true, 'disabled' => ($model->isNewRecord) ? false : true]) ?>
        <?php if ($model->isNewRecord): ?>
            <?= $form->field($model, 'password_hash')->passwordInput(['maxlength' => true]) ?>
        <?php endif; ?>
        <?= $form->field($model, 'package_id')->dropDownList($packageList, ['prompt' => Yii::t('customers', '-- Select package --'), 'disabled' => ($model->isNewRecord) ? false : true]) ?>
        <?= $form->field($model, 'parent_id')->dropDownList($accountInvite,['disabled' => ($model->isNewRecord) ? false : true]) ?>

    </div>
    <div class="box-header">
        <h3 class="box-title"><?= Yii::t('customers', 'Contact of Customer') ?></h3>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'sex')->dropDownList(\app\models\Customer::getSexLabels(), ['prompt' => Yii::t('customers', '-- Select sex -- ')]) ?>
        <?= $form->field($model, 'dob')->widget(DatePicker::className(), [
            'options' => [
                'class' => 'form-control'
            ],
            'clientOptions' => [
                'changeMonth' => true,
                'changeYear' => true,
                'yearRange' => '1960:2015'
            ]
        ]) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="box-footer">
        <div class="form-group">
            <label class="control-label"></label>
            <?= Html::submitButton($model->isNewRecord ? ('<span class=" fa fa-plus-square-o"></span>&nbsp;' . Yii::t('customers', 'Create New Customer')) : ('<span class=" fa fa-floppy-o"></span>&nbsp;' . Yii::t('customers', 'Save Customer Information')), ['class' => $model->isNewRecord ? 'btn btn-success btn-sm' : 'btn btn-primary btn-sm']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

