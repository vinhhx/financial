<?php

use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = Yii::t('customers', 'Register new account');
$this->params['breadcrumbs']['My account'] = Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<?= $this->render('_form', [
    'model' => $model,
    'packageList' => $packageList,
    'accountInvite' => $accountInvite,
]) ?>
