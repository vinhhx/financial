<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 14-Jun-16
 * Time: 12:27 PM
 */
$this->title = Yii::t('customers', 'Thông báo yêu cầu kích hoạt tài khoản');
$this->params["pageTitle"] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <h5 class="text-red">
            <?= Yii::t('customers', 'Tài khoản đang chờ kích hoạt. Để kích hoạt nhanh vui lòng liên hệ với ban quản trị website') ?>
        </h5>
    </div>
    <div class="col-md-6 col-sm-12">
        <div class=" box box-primary">
            <div class="box-header">
                <h3 class="box-title text-centrer"> <?= Yii::t('customers', 'Kích hoạt tài khoản') ?></h3>
            </div>
            <div class="box-body">
                <?php $form = \yii\widgets\ActiveForm::begin() ?>
                <?php echo $form->field($model, 'token')->textInput(); ?>
                <div class="form-group">
                    <label class="label-control">
                        <?= \yii\helpers\Html::submitButton('Active', ['class' => 'btn btn-primary btn-sm']) ?>


                    </label>
                </div>
                <?php \yii\widgets\ActiveForm::end() ?>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-12" >
        <div class=" box box-primary" style="min-height: 193px;">
            <div class="box-header">
                <h3 class="box-title text-centrer"> <?= Yii::t('customers', 'Danh sách token chưa sử dụng') ?></h3>
            </div>
            <div class="box-body">
                <?php $token = Yii::$app->user->identity->countCurrentToken();
                if ($token > 0): ?>
                    <?= \yii\grid\GridView::widget([
                        'dataProvider' => $dataProvider,
                        'options' => ['class' => 'grid-view grid-view-customize'],
                        'summary' => '',
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            ['attribute' => 'token',
                                'format' => 'raw',
                                'value' => function ($model) {
                                    return '<b class="text-success">' . $model->token . '</b>';
                                }
                            ],
                        ],
                    ]); ?>
                <?php else: ?>
                    <?= \yii\helpers\Html::a(Yii::t('customers', 'Mua token'), ['buy-token'], ['class' => 'btn btn-sm btn-warning']); ?>
                <?php endif; ?>

            </div>
        </div>
    </div>
</div>
