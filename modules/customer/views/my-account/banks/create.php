<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomerBankAddress */

$this->title = Yii::t('customers', 'Tạo mới địa chỉ ngân hàng');
$this->params['breadcrumbs'][Yii::t('app', 'Account')] = \yii\helpers\Url::to(['index']);
$this->params['breadcrumbs'][Yii::t('app', 'My bank')] = \yii\helpers\Url::to(['my-bank']);
$this->params['breadcrumbs'][] = 'Create';
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/customer/my-account/my-bank';
?>
<div class="customer-bank-address-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
