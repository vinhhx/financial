<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\customer\forms\search\CustomerBankAddressSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-bank-address-search form-search">
    <div class="box-primary box box-body">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
        ]); ?>

        <ul style=" margin: 0; padding: 0;">
            <li class="form-item">
                <?= Html::activeInput('text', $model, 'id', array('placeholder' => $model->getAttributeLabel('id'), 'style' => 'width:100px')) ?>
            </li>

            <li class="form-item">
                <?= Html::activeInput('text', $model, 'bank_address', array('placeholder' => $model->getAttributeLabel('bank_address'), 'style' => 'width:120px')) ?>
            </li>

            <li class="form-item">
                <?= Html::submitButton('<i class="fa fa-search"></i>&nbsp;' . Yii::t('customers', 'Tìm kiếm'), ['class' => 'btn btn-primary btn-sm']) ?>
            </li>
        </ul>

        <?php ActiveForm::end(); ?>

    </div>
</div>
