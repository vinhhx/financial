<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerBankAddress */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-bank-address-form row">

    <div class="col-lg-8 col-sm-10">

        <div class="box box-customer">
        <div clas="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'bank_address')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
        </div>

    </div>

</div>
