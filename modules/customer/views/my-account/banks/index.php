<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\forms\search\CustomerBankAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My bitcoin wallet address');
$this->params['breadcrumbs'][$this->title] = '';
$this->params['pageTitle'] = $this->title;
?>
<div class="customer-bank-address-index">

    <!--    --><?php //echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php if ($totalBank < 1): ?>
        <div class="box box-customer">
            <div class="box-header">
                <?= Html::a('<i class="fa fa-plus-square-o"></i>&nbsp;' . Yii::t('app', 'ADD NEW'), ['create-address-bank'], ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'options' => ['class' => 'grid-view grid-view-customize'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'bank_address',
            'created_at:datetime',
//            [
//                'class' => 'yii\grid\ActionColumn',
//                'template' => '{view}',
//                'buttons' => [
//                    'view' => function () {
//                    },
//                    /*'update' => function ($url, $model, $key) {
//                        return Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], [
//                            'class' => 'btn btn-primary btn-xs'
//                        ]);
//                    },*/
//                    /*'delete' => function ($url, $model, $key) {
//                        return Html::a(Yii::t('app', 'Delete'), ['delete-bank', 'id' => $model->id], [
//                            'class' => 'btn btn-danger btn-xs',
//                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
//                            'data-method' => 'post',
//                            'data-pjax' => '0',
//                        ]);
//                    }*/
//                ]
//            ],
        ],

    ]); ?>
</div>
