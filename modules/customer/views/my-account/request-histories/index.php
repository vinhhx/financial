<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\forms\search\CustomerBankAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'My request histories');
$this->params['breadcrumbs'][$this->title] = '';
$this->params['pageTitle'] = $this->title;
?>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'grid-view grid-view-customize'],
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'attribute' => 'type',
            'format' => 'raw',
            'value' => function ($model) {
                return $model::getRequestLabel($model->type);
            }
        ],
        [
            'attribute' => 'amount_exactly',
            'format' => 'raw',
            'value' => function ($model) {
                return Yii::$app->formatter->asDecimal($model->amount_exactly, 8) . Yii::$app->params['unit'];
            }
        ],
        [
            'attribute' => 'status',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->getStatusLabel();
            }
        ],
        [
            'attribute' => 'block_chain_wallet_id',
            'format' => 'raw',
            'value' => function ($model) {
                return $model->block_chain_wallet_id;
            }
        ],
        [
            'attribute' => 'created_at',
            'format' => 'raw',
            'value' => function ($model) {
                return Yii::$app->formatter->asDatetime($model->created_at);
            }
        ],
        [
            'label' => 'Last updated at',
            'format' => 'raw',
            'value' => function ($model) {
                return Yii::$app->formatter->asDatetime($model->updated_at);
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{history}',
            'buttons' => [
                'history' => function ($url, $model) {
                    if (in_array($model->type, [\app\models\CustomerRequest::TYPE_PAY_WHEN_OPEN_PACKAGE, \app\models\CustomerRequest::TYPE_SENT_BY_SPECIAL])):
                        return Html::a('<span class="fa fa-history"></span>&nbsp;' . Yii::t('app', 'History'), ['transaction', 'CustomerTransactionSearch[customer_sent_request_id]' => $model->id], [
                            'class' => 'btn btn-warning btn-xs',
                        ]);
                    endif;
                    return Html::a('<span class="fa fa-history"></span>&nbsp;' . Yii::t('app', 'History'), ['transaction', 'CustomerTransactionSearch[customer_receiver_request_id]' => $model->id], [
                        'class' => 'btn btn-warning btn-xs',
                    ]);
                }
            ]
        ]
    ],

]); ?>
</div>
