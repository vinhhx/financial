<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 10-Jun-16
 * Time: 2:54 AM
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::t('customers', 'Đăng ký mới gói đầu tư');
$this->params['breadcrumbs']['My account'] = Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <?php $form = ActiveForm::begin() ?>
        <div class="col-sm-12 col-lg-8">
            <?= $form->field($model, 'package_id')->dropDownList($packages, []); ?>
            <?= $form->field($model, 'block_chain_wallet_id')->textInput(['max-length' => true, 'placeholder' => $model->getAttributeLabel('block_chain_wallet_id')]); ?>
            <div class="form-group">
                <?= Html::submitButton('<span class="fa fa-floppy-o"></span>&nbsp;' . Yii::t('customers', 'Tạo gói đầu tư'), ['class' => 'btn btn-sm btn-primary']); ?>
            </div>
        </div>
        <?php ActiveForm::end() ?>

    </div>
</div>
