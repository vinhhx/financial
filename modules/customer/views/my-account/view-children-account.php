<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Package;
use yii\helpers\ArrayHelper;
use app\models\Customer;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\admin\forms\search\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('customers', 'View children account ');
$this->params['breadcrumbs'][Yii::t('customers', 'My account')] = Url::to(['index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
?>
<div class="box box-primary">
    <div class="box-body">
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    </div>
</div>
<div class="box box-info">

    <div class="box-body table-responsive no-pad?ing">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'headerOptions' => ['width' => '60'],
                ],
                [
                    'label' => Yii::t('customers', 'username'),
                    'format' => 'text',
                    'content' => function ($data) {
                        return $data->customer->username;
                    }
                ],
                [
                    'label' => Yii::t('customers', 'full_name'),
                    'format' => 'text',
                    'content' => function ($data) {
                        return $data->customer->full_name;
                    }
                ],
                'level',
                [
                    'label' => Yii::t('customers', 'parent name'),
                    'format' => 'text',
                    'content' => function ($data) {
                        return Customer::getName($data->customer->parent_id);
                    }
                ],
                [
                    'label' => Yii::t('customers', 'package_id'),
                    'format' => 'text',
                    'content' => function ($data) {
                        return Package::getName($data->customer->package_id);

                    }
                ],
                [
                    'label' => Yii::t('customers', 'date_join'),
                    'format' => 'text',
                    'content' => function ($data) {
                        return Yii::$app->formatter->asDate($data->customer->date_join);

                    }
                ],
            ],
        ]);
        ?>
    </div>
</div>
