<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\customer\forms\search\CustomerBankAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Buy Token');
$this->params['breadcrumbs']["Account"] = \yii\helpers\Url::to(['my-account/index']);
$this->params['pageTitle'] = $this->title;
?>
<div class="row">
    <div class="col-sm-12">
        <div class="box box-danger">
            <div class="box-header">
                <h3 class="box-title">Fill this form to buy token!</h3>
            </div>
            <div class="box-body">
                <div class="col-sm-6">
                    <?php
                    $form = ActiveForm::begin([
                        'layout' => 'horizontal',
                        'fieldConfig' => [
                            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                            'horizontalCssClasses' => [
                                'label' => 'col-sm-4',
                                'offset' => 'col-sm-offset-0',
                                'wrapper' => 'col-sm-8',
                                'error' => 'col-sm-8',
                                'hint' => 'col-sm-12',
                            ],
                        ]
                    ]);
                    ?>
                    <div class="form-group">
                        <label class="text-red control-label col-sm-4">Token
                            Price:</label>

                        <div class="col-sm-8">
                            <label class="form-control text-red"
                                   style="border:none;"><?= Yii::$app->params["join_fee"] ?> <?= Yii::$app->params["unit"] ?>
                                / 1
                                Token</label></div>
                    </div>
                    <div class="form-group">
                        <label class="text-success control-label col-sm-4">Bank
                            address :</label>

                        <div class="col-sm-8"><label class="form-control text-green"
                                                     style="border:none;"><?= Yii::$app->params["system_bit_coin_address"] ?></label>
                        </div>
                    </div>
                    <?= $form->field($model, 'quantity')->textInput(['placeholder' => 'Input quantity token which you want to buy...', 'maxlength' => true]); ?>
                    <div class="form-group">
                        <label class="control-label col-sm-4" for="button-submit"></label>

                        <div class="col-sm-8">
                            <?= \yii\helpers\Html::submitButton('<span class="fa fa-floppy-o"></span>&nbsp; Buy', ['class' => 'btn btn-sm btn-primary']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
                <div class="col-sm-6">
                    <h4>QR CODE:</h4>
                    <img src="/images/QR.jpg?v=<?= Yii::$app->params["IMAGE_VERSION"] ?>" style="width: 150px;"/>
                </div>
            </div>
        </div>
    </div>
</div>
