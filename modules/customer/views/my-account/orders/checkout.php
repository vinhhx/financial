<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerBankAddress */

$this->title = Yii::t('app', 'Payment for order');
$this->params['breadcrumbs'][Yii::t('app', 'Orders')] = \yii\helpers\Url::to(['order-index']);
$this->params['breadcrumbs'][] = $this->title;
$this->params['pageTitle'] = $this->title;
$this->params['parentAction'] = '/customer/my-account/index';
?>
<?php if (Yii::$app->request->get('type') == 'buy-token-success'): ?>
    <div class="alert alert-warning">
        <div style="font-size: 16px;">Please upload bitcoin transaction image.</div>
    </div>
<?php endif; ?>
<div class="customer-bank-address-view">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-body box-primary">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //'id',
                        'customer_username',
                        [
                            'label' => 'Quantity',
                            'value' => $model->quantity . ' Token (' . ($model->quantity * 0.03) . ' BTC)',
                        ],
                        [
                            'label' => 'Receiver bank address',
                            'value' => Yii::$app->params["system_bit_coin_address"],
                        ],
                        'created_at:datetime',
                    ],
                ]) ?>
                <?php if ($model->canUploadAttachment()): ?>
                    <div style="margin: 10px 0;">
                        Upload image screen transaction in blockchain wallet to completed order:
                    </div>
                    <?php $form = \yii\widgets\ActiveForm::begin(['id' => 'upload-order-bill',
                        'options' => ['enctype' => 'multipart/form-data']]) ?>
                    <div class="bg-file-upload">
                        <?= $form->field($modelForm, 'file')->fileInput()->label(false) ?>
                    </div>
                    <?= Html::submitButton('Upload', ['class' => 'btn btn-danger btn-sm']) ?>
                    <?= $form->field($modelForm, 'order_id')->hiddenInput()->label(false) ?>

                    <?php \yii\widgets\ActiveForm::end() ?>
                <?php else:
                    if ($model->attachment):
                        ?>
                        <div class="attachment-file">
                            <a target="_blank" id="attachment-file"
                               href="<?= Yii::$app->image->getImg($model->attachment) ?>">
                                <?= Html::img(Yii::$app->image->getImg($model->attachment, '100x100'), ['class' => 'img-thumbnail img-item']) ?>

                            </a>
                            <?php if ($model->canDeleteImage()): ?>
                                <span id="action-delete-image" class="glyphicon glyphicon-remove" data-toggle="tooltip"
                                      data-placement="top" title="remove this image"></span>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<?php
$js = <<<Js
    $('.attachment-file').removeImage();
Js;
$this->registerJs($js);
?>
