<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 10-Jun-16
 * Time: 2:54 AM
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div class="page-view-transaction">
    <h2 class="page-title"><?= Yii::t('customers', 'Gói đầu tư') ?></h2>

    <div class="page-content">
        <?php $form = ActiveForm::begin(['id' => 'create-invest-package']) ?>
        <div>
            <?= $form->field($model, 'customer_package_id')->hiddenInput()->label(false); ?>
            <h4 class="box-title" style="margin-top: 0px;"><?= Yii::t('customers', 'Mã số gói đầu tư') ?>
                : <?= $customerPackage->id ?></h4>
            <?= $form->field($model, 'package_id')->dropDownList($packages, ['prompt' => '-- ' . Yii::t('customers', 'Chọn gói đầu tư') . ' --']); ?>
            <?= $form->field($model, 'package_name')->textInput([]); ?>
            <div class="form-group">
                <?= Html::submitButton('<span class="fa fa-floppy-o"></span>&nbsp;' . Yii::t('customers', 'Tái đầu tư'), ['class' => 'btn btn-sm btn-primary', 'style' => 'margin-right:10px']); ?>
                <?= Html::a('Clear', 'javascript:void(0)', ['class' => 'btn btn-default btn-submit-cancel']) ?>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>
<style>
    .field-customerinvestpackageform-customer_package_id {
        margin-bottom: 0px;
        height: 1px;
    }
</style>
