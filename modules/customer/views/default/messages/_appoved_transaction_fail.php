<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 11:58 PM
 */
?>
<div class="page-view-transaction">
    <h2 class="page-title"><?= Yii::t('customers', 'Xác thực bị lỗi') ?></h2>

    <div class="page-content">
        <p><span class="icon-success"></span><?= Yii::t('customers', 'Lỗi khi bạn duyệt phiều gửi tiền') ?></p>
    </div>
</div>
