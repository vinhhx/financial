<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 29-Aug-16
 * Time: 4:18 PM
 */
$message = isset($message) ? $message : [];
$view = '';
switch ($message["status"]) {
    case"success":
        $view = '_create-package-success';
        break;
    case"error":
        $view = '_create-package-fail';
    default:
        break;
}
echo $this->render($view, ['message' => $message]);