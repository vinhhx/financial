<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 29-Aug-16
 * Time: 4:18 PM
 */
$message = isset($message) ? $message : [];
$view = '';
switch ($message["status"]) {
    case"success":
        $view = '_add_attachment_completed';
        break;
    case"error":
        $view = '_add_attachment_fail';
    default:
        break;
}
echo $this->render($view, $message);