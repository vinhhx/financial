<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 11:58 PM
 */
?>
<div class="page-view-transaction">
    <h2 class="page-title"><?= Yii::t('customers', 'Mở gói đầu tư') ?></h2>

    <div class="page-content">
        <p>
            <span class="icon-success"></span>
            <?= (isset($message["message"])) ? $message["message"] : Yii::t('customers', 'Lỗi không tạo được gói đầu tư') ?>
        </p>
    </div>
</div>
