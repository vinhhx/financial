<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 11:58 PM
 */
$messageText= isset($message) ? $message :Yii::t('customers', 'Upload Fail')
?>
<div class="page-view-transaction">
    <h2 class="page-title"><?= Yii::t('customers', 'Infomation') ?></h2>

    <div class="page-content">
        <p><span class="icon-success"></span> <?= $messageText ?></p>
    </div>
</div>
