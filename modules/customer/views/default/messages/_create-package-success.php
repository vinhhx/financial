<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 11:58 PM
 */
?>
<div class="page-view-transaction">
    <h2 class="page-title bg-green"><?= Yii::t('customers', 'Mở gói đầu tư thành công') ?></h2>

    <div class="page-content">
        <p><span class="icon-success"></span><?= Yii::t('customers', 'Bạn đã tạo gói đầu tư thành công') ?></p>
    </div>
</div>
