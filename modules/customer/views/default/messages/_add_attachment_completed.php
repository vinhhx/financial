<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 11:58 PM
 */
?>
<div class="page-view-transaction">
    <h2 class="page-title bg-green"><?= Yii::t('customers', 'Infomation') ?></h2>

    <div class="page-content">
        <p><span class="icon-success"></span> <?= Yii::t('customers', 'Upload successful') ?></p>
    </div>
</div>
