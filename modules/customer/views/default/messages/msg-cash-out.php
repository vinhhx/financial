<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 30-Aug-16
 * Time: 12:58 AM
 */
$message = isset($message) ? $message : [];
$view = '';
switch ($message["status"]) {
    case"success":
        $view = '_cash-out-success';
        break;
    case"error":
        $view = '_cash-out-fail';
    default:
        break;
}
echo $this->render($view, ['message' => $message]);