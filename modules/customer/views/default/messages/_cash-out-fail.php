<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 11:58 PM
 */
$msg=(isset($result) && isset($result["message"]) && $result["message"])? $result["message"]:Yii::t('customers', 'Lênh rút tiền của bạn đã có lỗi');
?>
<div class="page-view-transaction">
    <h2 class="page-title"><?= Yii::t('customers', 'Rút tiền lỗi') ?></h2>

    <div class="page-content">
        <p><span class="icon-success"></span><?=$msg ?></p>
    </div>
</div>
