<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 29-Aug-16
 * Time: 4:18 PM
 */
$message = isset($message) ? $message : [];
$view = '';
switch ($message["status"]) {
    case"success":
        $view = '_cash-out-success';
        break;
    case"error":
        $view = '../_cash-out-commission-form';
    default:
        break;
}
echo $this->render($view, $message);