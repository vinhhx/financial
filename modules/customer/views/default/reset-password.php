<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\customer\forms\ResetPasswordForm */

$this->title = Yii::t('customers', 'Change Password');
?>
<?= \app\extensions\widgets\Alert::widget(); ?>
<h1 class="title-login text-center"><?= Html::encode($this->title) ?></h1>
<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
        <div class="login-form-border" style="display: block; padding-bottom: 20px;">
            <?php $form = ActiveForm::begin(['id' => 'form-reset-password']); ?>
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <?= $form->field($model, 'newPassword')->passwordInput() ?>
                    <?= $form->field($model, 'newPasswordConfirmation')->passwordInput(); ?>
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
    <div class="col-sm-3"></div>
</div>