<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 12:13 PM
 */
use yii\helpers\Html;

?>
<div class="page-view-transaction">
    <h2 class="page-title"><?= Yii::t('customers', 'Chi tiết giao dịch') ?></h2>

    <div class="page-content">
        <!--<p class="order-code"><?/*= Yii::t('customers', 'Giao dịch') */?>: <?/*= $model->id */?></p>-->
        <?= Yii::t('customers', 'Tổng số BTC') ?> <?= Yii::$app->formatter->asDecimal($model->amount, 8) ?>
        <table class="table table-bordered table-hover dataTable">
            <tbody>

            <tr>
                <td><?= Yii::t('customers', 'Địa chỉ ngân hàng thụ hưởng') ?></td>
                <td><?= $model->customer_receiver_bank_address ?></td>
            </tr>
            </tbody>
        </table>
        <div class="notice">
            <?= Yii::t('customers', 'Sau khi bạn nhận được tiền bạn cần xác nhận bằng cách nhấn buton thích hợp!') ?>
            <p class="text-danger">
                <?= Yii::t('customers', 'Chưa xác nhận thanh toán trước khi chưa nhận tiền, xác nhận khi tài khoản của bạn đã chắc chắn nhận được tiền') ?>
            </p>
        </div>

        <table class="table table-bordered table-hover dataTable">
            <tbody>

            <tr>
                <td><?= Yii::t('customers', 'Địa chỉ nhận tiền') ?></td>
                <td><?= $model->customer_receiver_bank_address ?></td>
            </tr>
            <tr>
                <td><?= Yii::t('customers', 'Tài khoản chuyển tiền') ?></td>
                <td><?= $model->customer_sent_ref_code ?></td>
            </tr>
            </tbody>
        </table>
        <?php if ($model->canUpload()): ?>
            <?php $form = \yii\widgets\ActiveForm::begin([
                'id' => 'upload-transaction-bill',
                'options' => [
                    'enctype' => 'multipart/form-data',
                    'data-pjax'=>'#upload-attachment'
                ]
            ]) ?>
            <div class="bg-file-upload">
                <?= $form->field($modelForm, 'file')->fileInput()->label(false) ?>
            </div>
            <?= Html::submitButton('Upload', ['class' => 'btn btn-danger btn-sm', 'id' => 'upload-bill']) ?>
            <?= $form->field($modelForm, 'transaction_id')->hiddenInput()->label(false) ?>
            <?= Html::input('hidden','type', 'upload') ?>

            <?php \yii\widgets\ActiveForm::end() ?>
        <?php else:
            if ($model->attachment):

                ?>
                <div class="attachment-file">
                    <a target="_blank" id="attachment-file"
                       href="<?= Yii::$app->image->getImg($model->attachment) ?>">
                        <?= Html::img(Yii::$app->image->getImg($model->attachment, '100x100'), ['class' => 'img-thumbnail img-item']) ?>

                    </a>
                    <?php if ($model->canDeleteAttachment()): ?>
                        <span id="action-delete-image" class="glyphicon glyphicon-remove transaction-remove-attachment"
                              data-toggle="tooltip"
                              data-placement="top" title="remove this attachment"
                              data-transaction-id="<?= $model->id ?>"></span>
                    <?php endif; ?>
                </div>

            <?php endif; ?>
        <?php endif; ?>

        <?php
        if ($model->canApproved()): ?>
            <div class="clearfix"></div>
            <?php echo Html::a(Yii::t('customers', 'Xác nhận'), 'javacript:void(0)', ['class' => 'btn btn-sm btn-success btn-approved-transaction', 'data-order-id' => $model->id]); ?>
        <?php endif;
        ?>
    </div>
</div>
