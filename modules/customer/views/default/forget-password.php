<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\modules\customer\forms\RequestPasswordResetForm */

$this->title = Yii::t('customers', 'Forget Password');
?>
<?= \app\extensions\widgets\Alert::widget(); ?>
<h1 class="title-login text-center"><?= Html::encode($this->title) ?></h1>
<div class="row">
    <div class="col-sm-3"></div>
    <div class="col-sm-6">
        <div class="login-form-border" style="display: block;">
            <?php if (Yii::$app->request->get('act') == 'success'): ?>
                <div class="alert alert-success"
                     role="alert"><?= Yii::t('customers', 'Yêu cầu lấy lại mất khẩu đã được gửi đi. Vui lòng mở mail và làm theo hướng dẫn !') ?></div>
            <?php else: ?>
                <?php $form = ActiveForm::begin([
                    'id' => 'form-request-password-reset',
                    'options' => ['class' => 'form-horizontal'],
                ]); ?>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <?= $form->field($model, 'username') ?>
                        <?= $form->field($model, 'captcha')->widget(
                            \himiklab\yii2\recaptcha\ReCaptcha::className(),
                            ['siteKey' => Yii::$app->params["GOOGLE_CAPTCHA_SITE_KEY"]]
                        ) ?>
                        <div class="form-group">
                            <label class="control-label"></label>
                            <?= Html::submitButton('Gửi đi', ['class' => 'btn btn-primary']) ?>
                        </div>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-sm-3"></div>
</div>