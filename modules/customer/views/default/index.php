<?php

use yii\helpers\Html;

/* @var $this \yii\web\View */

$this->registerJsFile('/js/customer-process.js?v=' . Yii::$app->params["VERSION_JS"], ['depends' => \app\assets\AppAsset::className()]);
$this->registerJsFile('/js/countdown.js', ['depends' => \app\assets\AppAsset::className()]);

$this->title = Yii::t('customers', 'Dashboard');
$this->params['pageTitle'] = 'Dashboard';
$user = Yii::$app->user->identity;
?>
<div id="customer">
    <?php \yii\widgets\Pjax::begin() ?>
    <div id="current-time">
        <p class="current-time"> <?= Yii::t('customers', 'Location current time') ?> <?= Yii::$app->formatter->asDatetime(time(), 'php:Y-m-d H:i:s') ?></p>
    </div>
    <!--    <p class="current-time"> --><? //= Yii::t('customers', 'Location current time') ?><!-- -->
    <? //= date('d-m-Y H:i:s A') ?><!--</p>-->
    <div id="user-toolbar">
        <?php echo $this->render('containers/_customer_toolbar', []) ?>

    </div>
    <div class="row-link-refferal">
        <p>
            <?= Yii::t('customers', 'Đường dẫn liên kết giới thiệu của bạn') . ':' ?>
            <?= Html::a(Yii::$app->user->identity->getUrlRegisterReferal(), Yii::$app->user->identity->getUrlRegisterReferal()) ?></p>
    </div>
    <div class="row">
        <div id="current-package">
            <?= $this->render('containers/_customer_current_package') ?>
        </div>

    </div>
    <div id="current-transactions" class="row row-transactions">
        <?= $this->render('containers/_customer_current_transactions', ["transactions" => $transactions]) ?>
    </div>
    <?php \yii\widgets\Pjax::end() ?>
    <div class="modal fade" id="customerModal" tabindex="-1" role="dialog" aria-labelledby="pageModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <button type="button"
                        style="position: absolute; right: 0; background: #FFF; z-index: 999; width: 40px; height: 40px;"
                        class="close" data-dismiss="modal" aria-label="Close" data-toggle="tooltip" data-placement="top"
                        title="Close"
                "><span aria-hidden="true">×</span></button>
                <div class="modal-body" id="modalContent">
                </div>

            </div>
        </div>
    </div>
</div>
<?php
$rootIndex = \yii\helpers\Url::to(['/customer/default/index']);
$withDrawnCommissionFee = Yii::$app->params['CustomerPoundageCashOutFee'];
$js = <<<EOD
$('#customer').customerProcess({
    rootUrl:'{$rootIndex}',
    modalId:'#customerModal',
    withDrawnCommissionFee:'{$withDrawnCommissionFee}'
});
new Countdown();

$('.time-countdown').each(function(){
    var countdown = new Countdown({
        selector: '.'+$(this).attr('class-selector'),
        msgBefore: "Before time start",
        msgAfter: "0 hour",
        msgPattern: "{days} day {hours}:{minutes}:{seconds}",
        dateStart: new Date($(this).attr('date-start')),
        dateEnd: new Date($(this).attr('date-end'))
    });
});
EOD;
$this->registerJs($js);
?>
<style>
    a.disabled {
        opacity: .3;
        cursor: default !important;
        pointer-events: none;
        display: block;
    }
</style>
