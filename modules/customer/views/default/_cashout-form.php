<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 13-Jun-16
 * Time: 5:25 AM
 */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$requireCashOut = $package->requireCashOut();
?>
<div class="page-view-transaction">
    <h2 class="page-title bg-green"><?= Yii::t('customers', 'Rút tiền') ?></h2>

    <div class="page-content">
        <?php $form = ActiveForm::begin([
            'id' => 'customer-cash-out'
        ]) ?>
        <p><?= Yii::t('customers', 'Đơn vị rút tiền') ?>: BTC</p>

        <p><?= Yii::t('customers', 'Rút tối thiểu') ?>: <?= Yii::$app->params["min_cash_out_amount"] ?>
            &nbsp; <?= Yii::$app->params["unit"] ?></p>

        <p><?= Yii::t('customers', 'Số token trong tài khoản') ?>
            : <?= Yii::$app->formatter->asInteger($totalToken) ?> </p>

        <p><?= Yii::t('customers', 'Package name') ?>:
            :<b class="text-green"><?= isset($package->package_name) ? $package->package_name : '' ?></b></p>

        <?= $form->field($model, 'wallet_block_chain')->dropDownList($listBanks, ['placeholder' => '-- ' . Yii::t('customers', 'Chọn') . $model->getAttributeLabel('wallet_block_chain') . ' --']); ?>
        <?php
        if (empty($listBanks)): ?>
            <p class="text-danger">
                <?= Yii::t('customers', 'Your bitcoin wallet address is blank, pls add your bitcoin wallet address.') ?>
                <?= Html::a(Yii::t('customers', 'Click here'), Url::to(['my-account/create-address-bank']), ['class' => 'text-danger text-bold']) ?></p>
        <?php endif ?>
        <div class="form-group" style="margin-bottom: 0px;">
            <label> <?= Yii::t('customers', 'Số dư') ?>: </label>&nbsp;&nbsp;<span class="text-bold text-warning "
                                                                                   id="total-package-balance"><?= isset($package->balance) ? Yii::$app->formatter->asDecimal($package->balance, 8) : 0 ?></span>
        </div>
        <div class="form-group" style="margin-bottom: 0px;" id="show-warning">
            <ul class="warning">
                <?php if ($requireCashOut["status"] == "error"):
                    $messages = $requireCashOut["message"];
                    if (!empty($messages)):
                        foreach ($messages as $msg):
                            echo Html::tag('li', $msg, ['class' => 'text-red']);
                        endforeach;
                    endif;
                    ?>

                <?php endif; ?>

                <?php if ($totalToken == 0): ?>
                    <li class="text-red"><?= Yii::t('messages', 'Your token number had not enough to cash out this package') ?></li>
                <?php endif; ?>
            </ul>
        </div>

        <?= $form->field($model, 'amount')->hiddenInput()->label(false); ?>

        <?= Html::button('Submit', ['id' => 'btn-submit-cash-out', 'class' => 'btn btn-danger btn-sm', 'style' => 'margin-right:10px;', 'disabled' => (($totalToken == 0 || !($package->canCashOut()) || empty($listBanks)) ? true : false)]) ?>
        <?php if ($totalToken == 0): ?>
            <?= Html::a(Yii::t('customers', 'Mua token'), ['my-account/buy-token'], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
        <?= Html::a(Yii::t('customers', 'Xóa'), 'javascript:void(0)', ['class' => 'btn btn-default btn-submit-cancel']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
