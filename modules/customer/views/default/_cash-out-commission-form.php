<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 13-Jun-16
 * Time: 5:25 AM
 */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

$requireWidthdraw = Yii::$app->user->identity->requireWidthDrawnPoundage();
?>
<div class="page-view-transaction">
    <h2 class="page-title bg-green"><?= Yii::t('customers', 'Rút tiền hoa hồng') ?></h2>

    <div class="page-content">
        <?php $form = ActiveForm::begin([
            'id' => 'customer-cash-out-commission'
        ]) ?>
        <?php if ($requireWidthdraw["status"] == "error"):
            $messages = $requireWidthdraw["message"];
            if (!empty($messages)): ?>
                <ul class="warning" style="padding-left:15px;font-size: 14px;font-weight: 600; ">
                    <?php foreach ($messages as $msg):
                        echo Html::tag('li', $msg, ['class' => 'text-red']);
                    endforeach;
                    ?>
                </ul>
            <?php endif; ?>
        <?php endif; ?>
        <p><?= Yii::t('customers', 'Đơn vị rút tiền') ?>: BTC</p>

        <p><?= Yii::t('customers', 'Rút tối thiểu') ?>: <?= Yii::$app->params["min_cash_out_amount"] ?>
            &nbsp; <?= Yii::$app->params["unit"] ?></p>

        <p><?= Yii::t('customers', 'Rút tối đa') ?>: <?= Yii::$app->params["max_cash_out_amount"] ?>
            &nbsp; <?= Yii::$app->params["unit"] ?></p>

        <p><?= Yii::t('customers', 'Số Token') ?>:<?= Yii::$app->formatter->asInteger($totalToken) ?> </p>
        <?= $form->field($model, 'bank_address')->dropDownList($listBanks); ?>
        <?php
        if (empty($listBanks)): ?>
            <p class="text-danger">
                <?= Yii::t('customers', 'Your bitcoin wallet address is blank, pls add your bitcoin wallet address.') ?>
                <?= Html::a(Yii::t('customers', 'Click here'), Url::to(['my-account/create-address-bank']), ['class' => 'text-danger text-bold']) ?>
            </p>
        <?php endif ?>
        <?php $disabled = (($wallet->poundage < Yii::$app->params['min_cash_out_amount']) || $requireWidthdraw["status"] == "error") ? true : false ?>
        <?= $form->field($model, 'amount')->textInput(['disabled' => $disabled, 'data-result' => '#exactlyAmount'])
            ->label('Poundage Balance(/' . Yii::$app->formatter->asDecimal(Yii::$app->user->identity->getCurrentWallet()->poundage, 8) . ')', ['id' => 'label-balance'])
            ->hint('NOTE: Each drawing you lose 2.5% fee from the amount withdrawn', ['class' => 'text-yellow']); ?>
        <div class="form-group">
            <label class="control-label">Exactly Amount Receive:</label> &nbsp;
            <label id="exactlyAmount"
                   style="text-decoration: underline;"
                   class="text-red">0</label>&nbsp;<span><?= Yii::$app->params['unit'] ?> </span>
        </div>
        <?php if ($wallet->poundage < Yii::$app->params['min_cash_out_amount']): ?>
            <p id="show-error" class="text-red"> <?= Yii::t('customers', 'Số tiền không đủ để rút ra') ?></p>
        <?php endif ?>
        <?= Html::input('hidden', 'type', 'create-commission-cash-out') ?>
        <?= Html::submitButton(Yii::t('customers', 'Rút tiền'), ['id' => 'commission-submit-cash-out', 'class' => 'btn btn-danger btn-sm', 'style' => 'margin-right:10px;', 'disabled' => $disabled]) ?>
        <?= Html::a(Yii::t('customers', 'Xóa'), 'javascript:void(0)', ['class' => 'btn btn-default btn-submit-cancel']) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
