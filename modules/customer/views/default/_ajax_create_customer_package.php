<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 13-Jun-16
 * Time: 5:25 AM
 */
use yii\widgets\ActiveForm;
use yii\helpers\Html;
$lockOpenPackage = (!Yii::$app->params["CUSTOMER_OPEN_CREATE_PACKAGE"]) ? true : false;

?>
<div class="page-view-transaction">
    <h2 class="page-title"><?= Yii::t('customers', 'Chú ý') ?></h2>

    <div class="page-content">
        <?php $form = ActiveForm::begin() ?>
        <?php if ($lockOpenPackage): ?>
            <h4 class="text-danger">Website is locking function open package ...</h4>
        <?php endif; ?>
        <?= $form->field($model, 'amount')->textInput(['disabled'=>$lockOpenPackage]); ?>
        <!--        <p>100% Growth on amount x to x USD and 120% Growth on 810 to 4000 USD</p>-->
        <!---->
        <!--        <p>Select Growth <span>100% per month or 120% per month</span></p>-->
        <?= Html::submitButton('Submit', ['class' => 'btn btn-danger btn-sm', 'style' => 'margin-right:10px;']) ?>
        <?= Html::a('Clear', 'javascript:void(0)', ['class' => 'btn btn-default btn-submit-cancel','disabled'=>$lockOpenPackage]) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>
