<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 10-Jun-16
 * Time: 2:54 AM
 */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$lockOpenPackage = (!Yii::$app->params["CUSTOMER_OPEN_CREATE_PACKAGE"]) ? true : false;
?>
<div class="page-view-transaction">
    <h2 class="page-title"><?= Yii::t('customers', 'Chú ý') ?></h2>

    <div class="page-content">
        <?php $form = ActiveForm::begin(['id' => 'create-new-package']) ?>
        <div>
            <?php if ($lockOpenPackage): ?>
                <h4 class="text-danger">Website is locking function open package ...</h4>
            <?php endif; ?>
            <?= $form->field($model, 'package_id')->dropDownList($packageInfo, ['prompt' => '-- Select investment package --', 'id' => 'select-package-when-create', 'data-info' => json_encode($packageInfo),'disabled'=>$lockOpenPackage]); ?>
            <?= $form->field($model, 'package_name')->textInput(['disabled'=>$lockOpenPackage]); ?>
            <div class="form-group">
                <?= Html::button('<span class="fa fa-floppy-o"></span>&nbsp;' . Yii::t('customers', 'Tạo gói đầu tư'), ['id'=>'btn-submit-create-package', 'class' => 'btn btn-sm btn-primary', 'style' => 'margin-right:10px','disabled'=>$lockOpenPackage]); ?>
                <?= Html::a('Clear', 'javascript:void(0)', ['class' => 'btn btn-default btn-submit-cancel']) ?>
            </div>
        </div>
        <?php ActiveForm::end() ?>
    </div>

