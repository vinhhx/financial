<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 27-Aug-16
 * Time: 1:27 AM
 */
?>
<?php
$currentPackage = Yii::$app->user->identity->getCurrentPackages();
if (!empty($currentPackage)) :
    ?>
    <div class=" col-sm-10 col-lg-8 row-show-customer-packages">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t('customers', 'Your package') ?></h3>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <th><?= Yii::t('customers', 'Tên gói') ?></th>
                            <th><?= Yii::t('customers', 'Giá trị') ?>(BTC)</th>
                            <th><?= Yii::t('customers', 'Trạng thái') ?></th>
                            <th><?= Yii::t('customers', 'Số dư') ?>(BTC)</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        foreach ($currentPackage as $package):
                            ?>
                            <tr>
                                <td><?= $package["package_name"] ?></td>
                                <td><span
                                        class="label label-danger"><?= Yii::$app->formatter->asDecimal($package["package_amount"]) ?></span>
                                </td>
                                <td><span
                                        class="label label-info "><?= \app\models\CustomerPackage::getPackageLabels($package["status"]) ?></span>
                                </td>
                                <td><span
                                        class="label label-success "><?= Yii::$app->formatter->asDecimal($package["balance"]) ?></span>
                                </td>

                            </tr>
                            <?php
                        endforeach;

                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!--            <div class="box-footer clearfix">-->
            <!--                <a href="--><? //= Url::to(['my-account/register-package'])
            ?><!--"-->
            <!--                   class="btn btn-sm btn-success btn-flat pull-right">Tạo thêm gói đầu tư</a>-->
            <!--            </div>-->
        </div>
    </div>
    <?php
endif;
?>
