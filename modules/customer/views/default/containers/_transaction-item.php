<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 8:46 AM
 */
use yii\helpers\Html;
use app\models\CustomerTransaction;

if (!empty($item)):
    if ($type == 'sent'):
        switch ($item["status"]) {
            case CustomerTransaction::STATUS_SEND_PENDING:
                $classBackground = 'bg-red';
                $classTransactionIcon = 'pending';
                break;
            case CustomerTransaction::STATUS_SEND_COMPLETED:
                $classBackground = 'bg-yellow';
                $classTransactionIcon = 'send-completed';
                break;
            case CustomerTransaction::STATUS_RECEIVER_APPROVE:
            case CustomerTransaction::STATUS_FINISH:
                $classBackground = 'bg-green-2';
                $classTransactionIcon = 'success';
                break;
            default:
                $classBackground = 'bg-red';
                $classTransactionIcon = 'denied';
                break;
        }
        ?>
        <div
            class="info-box <?= $classBackground ?>  transaction-item-box">
            <div class="box-left">
        <span class="transaction-status">
        <i class="transaction-icon icon-<?= $classTransactionIcon ?>"></i>
        </span>

                <div class="clearfix"></div>
                <!--<span class="transaction-id"> <? /*= $item["id"] */
                ?>
        </span>-->
            </div>
            <div class="box-middle">
                <div class="line">
                    <?php
                    $msg = '';
                    if ($item["status"] == CustomerTransaction::STATUS_SEND_PENDING):
                        $msg = Yii::t('customers', 'You have a request move');
                    elseif ($item["status"] == CustomerTransaction::STATUS_SEND_COMPLETED):
                        $msg = Yii::t('customers', 'Move command is being processed');
                    elseif ($item["status"] == CustomerTransaction::STATUS_FINISH):
                        $msg = Yii::t('customers', 'Completed transactions');
                    endif;
                    ?>
                    <span class="transaction-title"><?= $msg ?></span>

                    <div class="progress-group">
                        <div>
                            <span class="progress-text"><?= Yii::t('customers', 'Thời gian còn lại') ?></span>
                        </div>
                        <?php
                        $percent = 100;
                        $time = 0;
                        ?>
                        <?php if ($item["status"] == CustomerTransaction::STATUS_SEND_PENDING || $item["status"] == CustomerTransaction::STATUS_RECEIVER_APPROVE):
                            ?>
                            <?php
                            $timeDiff = $item["expired_sent"] - time();

                            if ($timeDiff > 0) :
                                if (Yii::$app->params["TIME_REMAINING_TYPE"] == 'h'):
                                    $hours = floor($timeDiff / 3600);
                                else:
                                    $hours = floor($timeDiff / 60);
                                endif;
                                if ($hours < 1) :
                                    $time = 0;
                                else:
                                    $time = $hours;
                                    if ($timeDiff >= Yii::$app->params["TimeRequestLimit"]):
                                        $percent = 100;
                                    else:
                                        $percent = floor(($timeDiff / Yii::$app->params["TimeRequestLimit"]) * 100);
                                    endif;
                                endif;
                            endif;

                        elseif ($item["status"] == CustomerTransaction::STATUS_FINISH):
                            $time = 0;
                        endif;
                        ?>
                        <div class="progress sm">
                            <div class="progress-bar progress-bar-green"
                                 style="width: <?= $percent ?>%; <?= $percent == 100 ? ' opacity:0.5; ' : '' ?>"></div>
                        </div>
                        <span class="progress-time">
                            <?php if ($time == 0):
                                echo $time . ' hour';
                            else: ?>
                                <span class="time-countdown <?= 'number-' . $item["id"] ?>"
                                      class-selector="<?= 'number-' . $item["id"] ?>"
                                      date-start="<?= date('Y-m-d H:i:s', $item["created_at"]) ?>"
                                      date-end="<?= date('Y-m-d H:i:s', $item["expired_sent"]) ?>"></span>
                            <?php endif;
                            ?>

                        </span>

                    </div>
                </div>
                <div class="line">
                    <div class="transaction-create-time">
                        <span class="text-time-create"><?= Yii::t('customers', 'Thời gian tạo') ?></span>
                        <div class="clearfix hidden-xs">
                        </div>
                        <span><?= Yii::$app->formatter->asDate($item["created_at"], 'php:Y/m/d H:i') ?> </span>
                    </div>
                    <div class="transaction-user-step">
                    <span>
                        <?= $item["customer_sent_ref_code"] ?>
                        <span style="padding: 0 5px;"> > </span><?= $item["amount"] ?><?= Yii::$app->params["unit"] ?>
                        <span style="padding: 0 5px;"> > </span><?= $item["customer_receiver_ref_code"] ?></span>
                    </div>
                </div>
            </div>
            <div class="box-right">
                <?= Html::a('<span></span>', ['/customer/note/index'], [
                    'class' => 'add-note transaction-action action-message',
                    'data-table' => CustomerTransaction::tableName(),
                    'data-id' => $item['id'],
                    'data-toggle' => 'modal',
                    'data-target' => '#myModal',
                    'title' => 'Message',
                ]) ?>

                <!--<div class="clearfix"></div>
                <a href="#" class="transaction-action  action-print"></a>-->

                <div class="clearfix"></div>
                <a href="#" class="transaction-action  action-view open-modal-sent-view"
                   data-order-id="<?= $item['id'] ?>"></a>
            </div>
        </div>
    <?php elseif ($type == 'receiver'):
        switch ($item["status"]) {
            case CustomerTransaction::STATUS_SEND_PENDING:
                $classBackgroundReceiver = 'bg-red';
                $classTransactionIcon = 'pending';
                break;
            case CustomerTransaction::STATUS_SEND_COMPLETED:
                $classBackgroundReceiver = 'bg-yellow';
                $classTransactionIcon = 'send-completed';
                break;
            case CustomerTransaction::STATUS_RECEIVER_APPROVE:
            case CustomerTransaction::STATUS_FINISH:
                $classBackgroundReceiver = 'bg-green';
                $classTransactionIcon = 'success';
                break;
            default:
                $classBackgroundReceiver = 'bg-red';
                $classTransactionIcon = 'denied';
                break;
        }
        ?>
        <div class="info-box <?= $classBackgroundReceiver ?> transaction-item-box" ?>
            <h4 class="title"><?= Yii::t('customers', 'Yêu cầu nhận') ?></h4>

            <!--<h4 class="transaction-id"> <? /*= $item["id"] */
            ?></h4>-->

            <!--<p class="customer-receiver"><? /*= Yii::t('customers', 'Tham gia') */
            ?>
                : --><? /*= $item["customer_receiver_ref_code"] */
            ?>

            <p class="customer-receiver-amount"><?= Yii::t('customers', 'Số tiền') ?>
                : <?= $item["amount"] ?><?= Yii::$app->params["unit"] ?>

            <p class="customer-receiver-date">
                Date: <?= Yii::$app->formatter->asDate($item["created_at"], 'php:Y/m/d H:i') ?>

            <p class="customer-receiver-status">
                <?= Yii::t('customers', 'Trạng thái') ?>
                : <?= ($item["status"] == CustomerTransaction::STATUS_SEND_COMPLETED) ? Yii::t('customers', 'Chờ xử lý') : (($item["status"] == CustomerTransaction::STATUS_FINISH) ? Yii::t('customers', 'Đã hoàn thành') : (($item["status"] == CustomerTransaction::STATUS_SEND_PENDING) ? Yii::t('customers', 'Chờ chuyển tiền') : Yii::t('customers', 'Từ chối'))) ?>

            <div class="box-right" style="min-height: 100%;">
                <!--<a href="#" class="transaction-action action-open"></a>-->

                <?= Html::a('<span></span>', ['/customer/note/index'], [
                    'class' => 'add-note transaction-action action-message',
                    'data-table' => CustomerTransaction::tableName(),
                    'data-id' => $item['id'],
                    'data-toggle' => 'modal',
                    'data-target' => '#myModal',
                    'title' => 'Message',
                ]) ?>

                <div class="clearfix"></div>
                <a href="#" class="transaction-action  action-view open-modal-sent-view"
                   data-order-id="<?= $item['id'] ?>"></a>
            </div>

        </div>
    <?php endif; ?>
<?php endif ?>