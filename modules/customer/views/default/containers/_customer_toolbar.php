<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 27-Aug-16
 * Time: 1:20 AM
 */
?>
<div class="row default-admin">
    <div class="col-md-2 col-sm-3 col-xs-6">
        <div class="info-box bg-maroon">
            <a href="javascript:void(0);" id="my-poundage-balance" class="index-block" ?>
                <span class="info-box-icon"><img src="/images/ico-wallet-2-commission.png"/></span>

                <div class="info-box-content">
                    <p class="info-box-text-header"><?= Yii::t('customers', 'Số dư hoa hồng') ?></p>

                    <p class="info-box-text-content"><?= Yii::$app->formatter->asDecimal(Yii::$app->user->identity->getCurrentWallet()->poundage, 8) ?>
                        BTC</p>
                </div>
            </a>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <?php $pakageBalanceInfo = Yii::$app->user->identity->calcBalance() ?>
    <div class="col-md-2 col-sm-3 col-xs-6">
        <div class="info-box bg-aqua">
            <a href="javascript:void(0);" id="my-account-balance" class="index-block" ?>
                <span class="info-box-icon"><img src="/images/ico-wallet-3-package-balance.png"/></span>

                <div class="info-box-content">
                    <p class="info-box-text-header"><?= Yii::t('customers', 'Số dư các gói') ?></p>

                    <p class="info-box-text-content"><?= Yii::$app->formatter->asDecimal($pakageBalanceInfo["balance"], 8) ?>
                        BTC</p>
                </div>
            </a>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-md-2 col-sm-3 col-xs-6">
        <div class="info-box bg-yellow">
            <a href="javascript:void(0);" id="my-account-balance" class="index-block" ?>
                <span class="info-box-icon"><img src="/images/ico-wallet-2-customer-supended-balance.png"/></span>

                <div class="info-box-content">
                    <p class="info-box-text-header"><?= Yii::t('customers', 'Số dư tạm giữ') ?></p>

                    <p class="info-box-text-content"><?= Yii::$app->formatter->asDecimal($pakageBalanceInfo["holding"], 8) ?>
                        BTC</p>
                </div>
            </a>
            <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-md-2 col-sm-3 col-xs-6">
        <div class="info-box bg-red">
            <?php
            $lockOpenPackage = 'disabled';
            if (Yii::$app->user->identity->canPH()):
                $lockOpenPackage = '';
                ?>
                <div id="has-ph" class="hidden">
                    <div class="page-view-transaction">
                        <h2 class="page-title">ADD REQUEST</h2>

                        <div class="page-content">
                            <p>I have read the THE WARNING and I fully undstand at the risks. I make desision to
                                participate in this
                                site being of the sound mind and memory.</p>

                            <div class="clearfix"></div>
                            <div class="checkbox"><label><input type="checkbox" class="check-request"
                                                                name="check-read-request">
                                    Accept</label>
                            </div>
                            <div class="clearfix"></div>
                            <a class="btn btn-sm btn-danger btn-flat btn-submit-question-add-request"
                               href="javascript:void(0)"
                               style="margin-right:10px">Submit</a>
                            <a class="btn btn-sm btn-default btn-flat  btn-submit-cancel"
                               href="javascript:void(0)">Clear</a>
                        </div>
                    </div>
                </div>
                <?php
            endif;
            ?>
            <a href="javascript:void(0);" id="create-package" class="index-block <?= $lockOpenPackage ?>">
                <span class="info-box-icon"><img src="/images/ico-user-up.png"/></span>

                <div class="info-box-content">
                    <p class="info-box-text-header"><?= Yii::t('customers', 'Bid PipCoin') ?></p>

                    <p class="info-box-text-content"><?= Yii::t('customers', 'Yêu cầu cho') ?></p>
                </div>
                <!-- /.info-box-content -->
        </div>
        <!-- /.info-box -->
    </div>
    <div class="col-md-2 col-sm-3 col-xs-6">

        <div class="info-box bg-green">
            <a href="javascript:void(0);" id="widthdraw-package" class="index-block" ?>
                <span class="info-box-icon"><img src="/images/ico-user-down.png"/></span>

                <div class="info-box-content">
                    <p class="info-box-text-header"><?= Yii::t('customers', 'Ask PipCoin') ?></p>

                    <p class="info-box-text-content"><?= Yii::t('customers', 'Yêu cầu nhận') ?></p>
                </div>
                <!-- /.info-box-content -->
            </a>
        </div>
        <!-- /.info-box -->
    </div>
</div>