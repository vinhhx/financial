<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 27-Aug-16
 * Time: 1:30 AM
 */
use yii\helpers\Html;

if (!empty($transactions)):
    ?>
    <div class="col-sm-8">
        <div style="padding-bottom: 10px;">
            <?= Html::a(Yii::t('customers', 'Ẩn/Hiện'), 'javascript:void(0);', ['style' => 'color: #555;', 'class' => 'show-hide-transaction-success-sent']) ?>
        </div>
        <div class="transaction-sent">
            <?php
            if (!empty($transactions["sent"])):
                foreach ($transactions["sent"] as $item):
                    echo $this->render('_transaction-item', ['item' => $item, 'type' => 'sent']);
                endforeach;
            endif; ?>
        </div>
    </div>
    <div class="col-sm-4">
        <div style="padding-bottom: 10px;">
            <?= Html::a('Show/hide', 'javascript:void(0);', ['style' => 'color: #555;', 'class' => 'show-hide-transaction-success-receiver']) ?>
        </div>
        <div class="transaction-receiver">
            <?php
            if (!empty($transactions["receiver"])):
                foreach ($transactions["receiver"] as $item):
                    echo $this->render('_transaction-item', ['item' => $item, 'type' => 'receiver']);
                endforeach;
            endif; ?>
        </div>
    </div>
    <div class="col-sm-4"></div>
<?php endif; ?>