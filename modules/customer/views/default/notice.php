<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 10-Jun-16
 * Time: 3:21 AM
 */
$this->title = 'Thông báo';
$this->params['pageTitle'] = $this->title;
?>
<div class="callout callout-warning">
    <h4><?= Yii::t('customers', 'Thông báo') ?>!</h4>

    <p><?= Yii::t('customers', 'Tài khoản đang chờ kích hoạt. Để kích hoạt nhanh vui lòng liên hệ với ban quản trị website') ?> </p>

    <p><?= Yii::t('customers', 'Hotline: 098 xxx xxxx') ?></p>
</div>
