<?php

namespace app\modules\customer;
use yii\helpers\Url;
use Yii;
class Module extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\customer\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here


        // Set Components
        \Yii::$app->setComponents([
//            'authManager' => [
//                'class' => '\yii\rbac\DbManager',
//                'cache' => 'cache'
//            ],
            'user' => [
                'class' => '\yii\web\User',
                'identityClass' => 'app\models\Customer',
                'enableAutoLogin' => false,
                'loginUrl' => Url::to(['/customer/default/login']),
                'idParam' => '__customer',
                'authTimeoutParam' => '__customerExpire',
                'absoluteAuthTimeoutParam' => '__customerAbsoluteExpire',
                'returnUrlParam' => '__customerReturnUrl',
            ],
//            'urlManager' => [
//                'class' => 'yii\web\urlManager',
//                'enablePrettyUrl' => true,
//                'showScriptName' => false,
//                'rules' => [
//
//                ],
//            ],

        ]);

        // Customer Error Page
        \Yii::$app->errorHandler->errorAction = 'customer/default/error';

        // Set default layout
        $this->layout = 'admin';
        \Yii::$app->language = 'en';
    }
}
