<?php

namespace app\modules\customer\controllers;

use app\components\controllers\CustomerBaseController;
use app\models\CustomerActivity;
use app\models\CustomerBankAddress;
use app\models\CustomerOrder;
use app\models\CustomerPackage;
use app\models\CustomerPoundageTransaction;
use app\models\CustomerRequest;
use app\models\CustomerRequireCashout;
use app\models\CustomerToken;
use app\models\CustomerTransaction;
use app\models\LogAction;
use app\models\searchs\CustomerOrderSearch;
use app\models\searchs\CustomerTokenSearch;
use app\models\SystemEmailQueue;
use app\modules\customer\forms\CashOutForm;
use app\modules\customer\forms\ChangePasswordForm;
use app\modules\customer\forms\CustomerActiveForm;
use app\modules\customer\forms\search\CustomerRequestSearch;
use app\modules\customer\forms\CustomerTransactionSearch;
use app\modules\customer\forms\Orders\BuyTokenForm;
use app\modules\customer\forms\Orders\PaymentOrderForm;
use app\modules\customer\forms\search\CustomerBankAddressSearch;
use app\modules\customer\forms\search\CustomerCaseSearch;
use app\modules\customer\forms\search\CustomerRequireCashoutSearch;
use app\modules\invest\forms\requests\CustomerProviderHelpForm;
use Yii;
use app\models\Customer;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Package;
use yii\web\UploadedFile;


/**
 * MyAccountController implements the CRUD actions for Customer model.
 */
class MyAccountController extends CustomerBaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'delete-cash-out' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = Yii::$app->user->identity;
        return $this->render('index', ['model' => $model]);
    }

    /**
     * Displays a single Customer model.
     * @param string $id
     * @return mixed
     */
    public function actionView()
    {
        return $this->render('view', [
            'model' => $this->findModel(Yii::$app->user->id),
        ]);
    }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRegister()
    {
        //Tắt không được đăng ký tài khoản từ trong customer .
//        Yii::$app->session->setFlash('warning', Yii::t('customer', 'Register module is locked, please contact with office  to create new account.'));
//        return $this->redirect(['my-account/index']);
        $model = new Customer();
        $model->scenario = 'create';
        $model->date_join = time();
        $packageList = ArrayHelper::map(Package::getPackages(Yii::$app->user->identity->amount), 'id', 'package_name');
        $accountInvite = ArrayHelper::map(Customer::getAccountInBranch(Yii::$app->user->id), 'id', 'username');
        $model->parent_id = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post())) {
            $model->setPassword($model->password_hash);
            $model->generateAuthKey();
            if (!in_array($model->parent_id, $accountInvite)) {
                $model->parent_id = Yii::$app->user->id;
            }
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($model->validate()) {
                    $model->last_auto_increase = date('Y-m-d');
                    $model->created_by = Yii::$app->user->id;
                    $model->created_type = Customer::TYPE_CUSTOMER;
                    $model->amount = 0;
//                    $model->parent_id = Yii::$app->user->id;

                    if ($model->save()) {
                        $package = $model->package;
                        //Trừ tiền user
                        //Số tiền thực tế gói
                        $exchange = round(((int)Yii::$app->params["exchange"]["in"] / (int)Yii::$app->params["exchange"]["out"]), 3);

                        $packageAmount = $model->package->amount * $exchange;
                        $balanceBefore = Yii::$app->user->identity->amount;
                        Yii::$app->user->identity->amount -= $packageAmount;
                        Yii::$app->user->identity->save();

                        $balanceAfter = $balanceBefore - $packageAmount;
                        //Lưu transaction
                        CustomerTransaction::saveTransaction(
                            Yii::$app->user->id,
                            Yii::$app->user->identity->username,
                            $packageAmount,
                            CustomerTransaction::TYPE_CUSTOMER_REGISTER,
                            CustomerTransaction::SIGN_SUB,
                            $balanceBefore,
                            $balanceAfter
                        );
                        if (!Yii::$app->user->identity->is_loan_admin) {
                            Customer::saveCustomerCase($model->id, $model->parent_id);
                            Customer::addAmountByParentid($model->id, $model->package_id);
                            LogAction::logUser($model->id, $model::tableName(), LogAction::TYPE_USER_CREATE, $model->getAttributes());
                            $transaction->commit();
                        } else {
                            //Nếu là tiền mượn thì tài khoản đấy chỉ đc tạo để lấy ngày.
                            $model->status = Customer::STATUS_CREATED_BY_MONEY_LOAN;
                            $model->save(true, ['status', 'updated_at']);
                            Customer::saveCustomerCase($model->id, $model->parent_id);
                            LogAction::logUser($model->id, $model::tableName(), LogAction::TYPE_USER_CREATE, $model->getAttributes());
                            $transaction->commit();
                        }
//                        Customer::addAmountByPackage($model->id, $model->package_id, 'register');// không cần lưu tiền gốc vì tiền gốc không đc cho rút

                        Yii::$app->session->setFlash('success', Yii::t('messages', 'Đăng ký tài khoản thành công!'));
                        return $this->redirect(['index']);
                    }
                }
            } catch (Exception $ex) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', $ex->getMessage());
            }
        }

        return $this->render('register', [
            'model' => $model,
            'packageList' => $packageList,
            'accountInvite' => $accountInvite,
        ]);
    }

    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate()
    {

        $model = $this->findModel(Yii::$app->user->id);
        $packageList = ArrayHelper::map(Package::getPackages(Yii::$app->user->identity->amount), 'id', 'package_name');
        $accountInvite = ArrayHelper::map(Customer::getAccountInBranch(Yii::$app->user->id), 'id', 'username');
        if ($model->load(Yii::$app->request->post()) && $model->save(true, [
                'full_name',
                'phone',
                'sex',
                'dob',
                'email',
                'address',
                'upated_at'
            ])
        ) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
                'packageList' => $packageList,
                'accountInvite' => $accountInvite
            ]);
        }
    }

    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
    }

    public function actionDeleteBank($id)
    {
//        if ($model = CustomerBankAddress::findOne($id)) {
//            if ($model->delete()) {
//                return $this->redirect(['my-bank']);
//            }
//        }
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('messages', 'Nội dung yêu cầu không tồn tại.'));
        }
    }

    public function actionChangePassword()
    {
//        $user= $this->findModel($id);
        $model = new ChangePasswordForm();
        $model->currentPassword = '';
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->changePassword()) {
                Yii::$app->session->setFlash('success', Yii::t('messages', 'Cập nhật password mới thành công.'));
                $this->redirect(['index']);
            }
        }
        return $this->render('change-password', [
            'model' => $model
        ]);
    }

    public function actionRequireCashOut()
    {
        $model = new CashOutForm();
        if (Yii::$app->request->post() && $model->load(Yii::$app->request->post())) {
            if (in_array(date('l'), \app\models\CustomerRequireCashout::ALLOW_CASH_OUT)) {
                if ($model->validate() && $model->saveCashOut()) {
                    Yii::$app->session->setFlash('success', Yii::t('messages', 'Yêu cầu nhận thành công.'));
                    return $this->redirect(['cash-out']);
                } else {
//                    echo'<pre>';var_dump($model->getErrors());die;
                    Yii::$app->session->setFlash('error', Yii::t('messages', 'Lỗi không yêu cầu nhận được.'));
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('customers', Yii::t('messages', 'Thời gian yêu cầu nhận không chính xác')));
            }


        }
        return $this->render('customer-require-cashout/cash-out', ['model' => $model]);
    }

    public function actionCashOut()
    {
        $searchModel = new CustomerRequireCashoutSearch();
        $params = Yii::$app->request->queryParams;
        $params["CustomerRequireCashoutSearch"]["customer_id"] = Yii::$app->user->id;
        $dataProvider = $searchModel->search($params);
        return $this->render('customer-require-cashout/index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }

    public function actionDeleteCashOut($id)
    {
        if ($model = CustomerRequireCashout::find()->where([
            'id' => $id,
            'customer_id' => Yii::$app->user->id,
            'status' => CustomerRequireCashout::STATUS_PENDING,

        ])->one()
        ) {
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $model->status = CustomerRequireCashout::STATUS_DELETED;
                if ($model->save(true, ['status', 'updated_at'])) {
                    //Cộng tiền người dùng
                    $beforeTransaction = Yii::$app->user->identity->amount;
                    if (Yii::$app->user->identity->save(true, ['amount', 'updated_at'])) {
                        //Lưu bảng transaction
                        if (CustomerTransaction::saveTransaction(
                            Yii::$app->user->id,
                            Yii::$app->user->identity->username,
                            $model->amount,
                            CustomerTransaction::TYPE_CUSTOMER_WIDRAWL_DELETED,
                            CustomerTransaction::SIGN_ADD,
                            $beforeTransaction,
                            Yii::$app->user->identity->amount
                        )
                        ) {
                            LogAction::logUser($model->id, $model::tableName(), LogAction::TYPE_USER_REQUIRE_WITHDRAWL_DELETED, $model->getAttributes());
                            $transaction->commit();
                            Yii::$app->session->setFlash('success', Yii::t('customers', 'Cancel require withdrawl'));
                            return $this->redirect(['cash-out']);
                        } else {

                        }
                    } else {

                    }
                }

            } catch (Exception $ex) {
                $transaction->rollBack();
                Yii::$app->session->setFlash('error', Yii::t('customers', $ex->getMessage()));
                return $this->redirect(['cash-out']);
            }

        } else {
            Yii::$app->session->setFlash('error', Yii::t('customers', 'Require not existed'));
            return $this->redirect(['cash-out']);
        }
    }

    public function actionViewChildrenAccount()
    {
        $searchModel = new CustomerCaseSearch();
        $queryParams = Yii::$app->request->queryParams;
        $queryParams["CustomerCaseSearch"]['parent_id'] = Yii::$app->user->id;
        $dataProvider = $searchModel->searchChildrenAccount($queryParams);
        return $this->render('view-children-account', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionRegisterPackage()
    {
        $packageLimit = Yii::$app->params['packageLimit'];
        if (count(Yii::$app->user->identity->getCurrentPackages()) > $packageLimit) {
            throw new NotFoundHttpException(Yii::t('messages', 'Số lượng gói đầu tư đã đến giới hạn'));
        }
        $model = new CustomerPackage();
        $model->customer_id = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
            $package = Package::findOne(['id' => $model->package_id, 'status' => Package::STATUS_ACTIVE]);
            if (!$package) {
                $model->addError('package_id', Yii::t('messages', 'Gói đầu tư không chính xác'));
            } else {
                $model->package_amount = $package->amount;
                $model->status = CustomerPackage::STATUS_REQUEST_IN_STEP_ONE;
                if ($model->save()) {
                    $model->requestOpenPackage();
                    $activityMessage = 'customer đã đăng ký gói đầu tư ' . $package->package_name . ' trị giá ' . $model->package_amount . ' bitcoin';
                    CustomerActivity::customerCreate(CustomerActivity::TYPE_CUSTOMER_CREATE_PACKAGE, $activityMessage, $model->getAttributes());
                    LogAction::logUser($model->id, $model->tableName(), LogAction::TYPE_CUSTOMER_CREATE_PACKAGE, $model->getAttributes());
                    Yii::$app->session->setFlash('success', Yii::t('messages', 'Bạn đã đăng ký gói đầu tư mói thành công'));
                    return $this->redirect(['default/index']);
                } else {
                    Yii::$app->session->setFlash('error', Yii::t('messages', 'Chưa đăng ký thành công gói đầu tư mới'));
                }
            }
        }
        $packages = Package::getPackages();
        $packages = ArrayHelper::map($packages, 'id', 'package_name');
        return $this->render('register-package', [
            'model' => $model,
            'packages' => $packages
        ]);
    }

    public function actionActive()
    {
        $model = new CustomerActiveForm();
        $model->customer_id = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->active()) {
                SystemEmailQueue::createEmailQueue(
                    SystemEmailQueue::TYPE_CUSTOMER_ACTIVE_COMPLETED,
                    Yii::$app->user->identity->email,
                    'You actived account ' . date('Y-m-d H:i:s'),
                    'active-completed',
                    [
                        'username' => Yii::$app->user->identity->username,
                    ]
                );
                Yii::$app->session->setFlash('success', Yii::t('messages', 'Tài khoản đã được kích hoạt thành công'));
                return $this->redirect(['default/index']);
            } else {
                Yii::$app->session->removeAllFlashes();
                Yii::$app->session->setFlash('error', Yii::t('messages', 'Mã token không chính xác'));
            }
        } else {
            Yii::$app->session->removeAllFlashes();
        }
        $modelSearch = new CustomerTokenSearch();
        $params = Yii::$app->request->queryParams;
        $params['CustomerTokenSearch']['customer_id'] = Yii::$app->user->id;
        $params['CustomerTokenSearch']['status'] = CustomerToken::STATUS_PENDING;
        $params['CustomerTokenSearch']['type'] = CustomerToken::TYPE_CUSTOMER;
        $dataProvider = $modelSearch->search($params);
        return $this->render('_form-active', ['model' => $model, 'dataProvider' => $dataProvider]);
    }

    public function actionMyBank()
    {
        $searchModel = new CustomerBankAddressSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $totalBank = count($dataProvider->getModels());

        return $this->render('banks/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'totalBank' => $totalBank,
        ]);
    }

    public function actionCreateAddressBank()
    {
        if ($modelCheck = CustomerBankAddress::findOne(['customer_id' => Yii::$app->user->id])) {
            return $this->redirect('my-bank');
        }
        $model = new CustomerBankAddress();

        if ($model->load(Yii::$app->request->post())) {
            $model->customer_id = Yii::$app->user->id;
            if ($model->save()) {
                return $this->redirect(['my-bank', 'id' => $model->id]);
            }
        }
        return $this->render('banks/create', [
            'model' => $model,
        ]);
    }

    public function actionBalance()
    {
        $query = CustomerPoundageTransaction::find();
        $query->where(['customer_id' => Yii::$app->user->id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
        ]);
        return $this->render('customer-transaction/balance-index', [
            'dataProvider' => $dataProvider,
        ]);
//        return $this->redirect(Url::to('/customer/default/index'));
    }

    public function actionTransaction()
    {
        $searchModel = new CustomerTransactionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('customer-transaction/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    public function actionTransactionView($id)
    {
        $model = $this->findTransaction($id);
        return $this->render('customer-transaction/view', ['model' => $model]);
    }

    protected function findTransaction($id)
    {
        if (($model = CustomerTransaction::find()
                ->where(['id' => $id])
                ->andFilterWhere([
                    'or',
                    ['customer_sent_id' => Yii::$app->user->id],
                    ['customer_receiver_id' => Yii::$app->user->id],])
                ->limit(1)
                ->one()) !== null
        ) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('messages', 'Nội dung yêu cầu không tồn tại.'));
        }
    }

    public function actionMember()
    {
        $model = Customer::showTree(Yii::$app->user->id, 20, false);
        return $this->render('members/index', ['model' => $model]);
    }

    public function actionMember2()
    {
        $model = Customer::showTree(Yii::$app->user->id, 10, false);
        return $this->render('members/index2', ['model' => $model]);
    }

    public function actionBuyToken()
    {
        $model = new BuyTokenForm();
        $model->customer_id = Yii::$app->user->identity->id;
        if ($model->load(Yii::$app->request->post())) {
            $order = $model->buy();
            if ($order && isset($order->id) && $order->id) {
                return $this->redirect(['check-out', 'id' => $order->id, 'type' => 'buy-token-success']);
            } else {
                Yii::$app->session->setFlash('success', Yii::t('messages', 'Không đặt mua ') . $order->quantity . Yii::t('messages', ' token') . '!');
            }
        }
        return $this->render('orders/buy-token', ['model' => $model]);
    }

    public function actionCheckOut($id)
    {
        $model = CustomerOrder::findOne([
            'id' => $id,
            'customer_id' => Yii::$app->user->identity->id,
            'type_module' => CustomerOrder::TYPE_MODULE_CUSTOMER
        ]);
        if (!$model) {
            throw new NotFoundHttpException(Yii::t('messages', 'Nội dung yêu cầu không tồn tại'));
        }
        if (Yii::$app->request->isAjax) {
            $type = Yii::$app->request->post('type', '');
            switch ($type) {
                case 'remove-image':
                    $result = $model->removeAttachment();
                    return $result;

                    break;
                default:
                    break;

            }
            return false;
        }
        $modelForm = new PaymentOrderForm();
        $modelForm->order_id = $model->id;
        if ($modelForm->load(Yii::$app->request->post())) {
            $modelForm->file = UploadedFile::getInstance($modelForm, 'file');
            if ($modelForm->validate('file')) {
                $uploadDir = 'orders/' . Yii::$app->user->id;
                $model->attachment = Yii::$app->image->upload(0, $uploadDir, $modelForm->file);
                $model->upload_attachment_at = time();
                if ($model->attachment) {
                    $model->status = CustomerOrder::STATUS_PAID;
                    if ($model->save(true, ['attachment', 'upload_attachment_at', 'status', 'updated_at'])) {
                        //Yii::$app->session->setFlash('success', Yii::t('messages', 'Bạn đã thanh toán cho ') . $model->quantity . Yii::t('messages', ' yêu cầu mua token !'));
                        return $this->redirect(['order-index', 'type' => 'checkout-success', 'token' => $model->quantity]);
                    }
                }
            } else {
                $modelForm->addError('file', 'Your image not exactly');
            }
        }
        return $this->render('orders/checkout', ['model' => $model, 'modelForm' => $modelForm]);
    }

    public function actionOrderIndex()
    {
        $modelSearch = new CustomerOrderSearch();
        $params = Yii::$app->request->queryParams;
        $params["CustomerOrderSearch"]["customer_id"] = Yii::$app->user->identity->id;
        $params["CustomerOrderSearch"]["type_module"] = CustomerOrder::TYPE_MODULE_CUSTOMER;
        $dataProvider = $modelSearch->search($params);
        return $this->render('orders/index', ['modelSearch' => $modelSearch, 'dataProvider' => $dataProvider]);
    }

    public function actionRequestHistory()
    {
        $modelSearch = new CustomerRequestSearch();
        $params = Yii::$app->request->queryParams;
        $dataProvider = $modelSearch->search($params);
        return $this->render('request-histories/index', ['modelSearch' => $modelSearch, 'dataProvider' => $dataProvider]);

    }

}
