<?php

namespace app\modules\customer\controllers;

use app\components\controllers\CustomerBaseController;
use app\forms\RegisterForm;
use app\models\Customer;
use app\models\CustomerActivity;
use app\models\CustomerCase;
use app\models\CustomerPackage;
use app\models\CustomerTransaction;
use app\models\LogAction;
use app\models\Package;
use app\models\SystemEmailQueue;
use app\modules\customer\forms\CashOutCommissionForm;
use app\modules\customer\forms\CashOutForm;
use app\modules\customer\forms\CustomerActionForm;
use app\modules\customer\forms\CustomerCreatePackageForm;
use app\modules\customer\forms\CustomerInvestPackageForm;
use app\modules\customer\forms\LoginForm;
use app\modules\customer\forms\RequestPasswordResetForm;
use app\modules\customer\forms\ResetPasswordForm;
use yii\db\Query;
use yii\db\Transaction;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class DefaultController extends CustomerBaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
//                'only' => ['index', 'login', 'logout'],
                'rules' => [
                    [
                        'actions' => ['index', 'logout', 'error', 'ajaxloadcustomertree', 'notice', 'buy-token'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['captcha'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['login', 'forget-password', 'reset-password'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['register'],
                        'allow' => true,
                        'roles' => ['@', '?'],
                    ]
                ]
            ]
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
//                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
//        $modelTree = Customer::getShowCustomer(Yii::$app->user->id);
//        $model = Customer::showTree(Yii::$app->user->id,4);
        //Lấy những transaction gửi và chuyển
        $modelForm = new CustomerActionForm();
        if (Yii::$app->request->isPjax) {
//            Yii::$app->response->format = 'json';
            $type = Yii::$app->request->post('type', '');
            switch ($type) {
                case 'view':
                    $transactionId = Yii::$app->request->post('tid', 0);
                    if ($transactionId) {
                        $query = CustomerTransaction::find();
                        $query->where(['id' => $transactionId]);
                        $query->andFilterWhere([
                            'or',
                            ['customer_sent_id' => Yii::$app->user->identity->id],
                            ['customer_receiver_id' => Yii::$app->user->identity->id]
                        ]);
                        $query->limit(1);
                        $customerTransaction = $query->one();
                        if ($customerTransaction) {
                            $modelForm->transaction_id = $customerTransaction->id;
                            $ajaxHtml = $this->renderAjax('_ajax_view_transaction', ['model' => $customerTransaction, 'modelForm' => $modelForm]);
                            return $ajaxHtml;
                        }
                    }
                    return;
                    break;
                case 'upload':
                    if ($modelForm->load(Yii::$app->request->post())) {
                        $modelForm->file = UploadedFile::getInstance($modelForm, 'file');

                        if ($modelForm->validate('file')) {
                            $result = $modelForm->uploadAttachment();
                        } else {
                            $result["status"] = "error";
                            $result["message"] = "Image not exactly!";
                        }
                        return $this->renderAjax('messages/msg-upload-attachment', ['message' => $result]);
                    }
                    return;
                    break;
                case 'remove-attachment':
                    $transactionId = Yii::$app->request->post('tid', 0);
                    if ($transactionId) {
                        $query = CustomerTransaction::find();
                        $query->where(['id' => $transactionId]);
                        $query->andWhere(['customer_sent_id' => Yii::$app->user->id]);
                        $query->limit(1);
                        $customerTransaction = $query->one();
                        if ($customerTransaction) {
                            $customerTransaction->removeAttachment();

                        }
                    }
                    $transactions = CustomerTransaction::getTransactionCustomers();
                    return $this->renderAjax('containers/_customer_current_transactions', ['transactions' => $transactions]);
                    break;
                case 'approved':
                    $transactionId = Yii::$app->request->post('tid', 0);
                    if ($transactionId) {
                        $query = CustomerTransaction::find();
                        $query->where(['id' => $transactionId]);
                        $query->andWhere(['customer_receiver_id' => Yii::$app->user->id]);
                        $query->limit(1);
                        $customerTransaction = $query->one();
                        if ($customerTransaction && $customerTransaction->canApproved()) {
                            $customerTransaction->finish();
                        }
                        $transactions = CustomerTransaction::getTransactionCustomers();
                        return $this->renderAjax('containers/_customer_current_transactions', ['transactions' => $transactions]);
                    }
                    return;
                case'open-package':
                    $result = $this->openPackage();
                    return $this->renderAjax('register-package', $result);
                    break;

                case 'create-package':
                    $result = $this->createPackage(Yii::$app->request->post());
                    return $this->renderAjax('messages/msg-create-package', ['message' => $result]);
                    break;
                case 'open-request-widthdraw':
                    $result = $this->requestCashOut();
                    $ajaxHtml = $this->renderAjax('_cashout-form', $result);
                    return $ajaxHtml;
                    break;
                case 'open-commission-cash-out':
                    $result = $this->requestCommissionCashOut();
                    $ajaxHtml = $this->renderAjax('_cash-out-commission-form', $result);
                    return $ajaxHtml;
                    break;
                case 'create-commission-cash-out':
                    $model = new CashOutCommissionForm();
                    $results = [];
                    $results["status"] = "error";
                    $totalToken = Yii::$app->user->identity->countCurrentToken();
                    $results['totalToken'] = $totalToken;
                    $wallet = Yii::$app->user->identity->getCurrentWallet();
                    $results['wallet'] = $wallet;
                    $myBanks = Yii::$app->user->identity->getCurrentBankAddress();
                    $listBanks = ArrayHelper::map($myBanks, 'id', 'bank_address');
                    $results['listBanks'] = $listBanks;
                    $results["model"] = $model;
                    $model->customer_id = Yii::$app->user->identity->id;
                    if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                        if (isset($listBanks[$model->bank_address])) {
                            $model->bank_address = $listBanks[$model->bank_address];
                            $result = $model->createCashOut();
                            $results["model"] = $model;
                            if (isset($result["status"]) && $result["status"] == 'success') {
                                $results["status"] = "success";
                            }

                        } else {
                            $results["status"] = "error";
                            $results["model"] = $model;
                        }

                    } else {
                        $results["status"] = "error";
                        $results["model"] = $model;
                    }
                    $ajaxHtml = $this->renderAjax('messages/msg-cash-out-commission', ['message' => $results]);
                    return $ajaxHtml;
                    break;
                case'cash-out':
                    $result = [];
                    $result["status"] = "error";
                    $model = new CashOutForm();
                    if ($model->load(Yii::$app->request->post())) {
                        $packages = Yii::$app->user->identity->getPackages()->all();
                        $model->customer_package_id = isset($packages[0]) ? $packages[0]->id : null;
                        $myBanks = Yii::$app->user->identity->getCurrentBankAddress();
                        $listBanks = ArrayHelper::map($myBanks, 'id', 'bank_address');
                        if (isset($listBanks[$model->wallet_block_chain])) {
                            $model->wallet_block_chain = $listBanks[$model->wallet_block_chain];
                            if ($model->createCashOut()) {
                                $result["status"] = "success";

                            } else {
                                $result["status"] = "error";
                                $result["message"] = Yii::t('messages', "Your package cash out error.");
                            }

                        } else {
                            $result["status"] = "error";
                            $result["message"] = Yii::t('messages', "Địa chỉ ngân hàng thụ hưởng không chính xác.");

                        }
                    }
                    $ajaxHtml = $this->renderAjax('messages/msg-cash-out', ['message' => $result]);
                    return $ajaxHtml;
                    break;
                default:
                    break;

            }
            return;
            Yii::$app->end();
        }

        $transactions = CustomerTransaction::getTransactionCustomers();
        return $this->render('index', [
            'transactions' => $transactions
        ]);
    }

    public function actionError()
    {
        return $this->render('error');
    }

    public function actionLogin()
    {
        $this->layout = 'login';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            //check url redirect
            $url = Yii::$app->session->get('referrer_url');
            /*if ($url) {
                return $this->redirect($url);
            }*/
            return $this->redirect(['default/index']);
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout(false);

        return $this->redirect(Yii::$app->user->loginUrl);
    }

    public function actionAjaxloadcustomertree()
    {
        $id = (int)Yii::$app->request->post('id');
        $type = (int)Yii::$app->request->post('type', 0);
        if ($type == 1) {
            $id = isset(Customer::getParents($id, 3, Yii::$app->user->id)['id']) ? Customer::getParents($id, 3, Yii::$app->user->id)['id'] : 0;
        }
        $reVal = [];
        $reVal['action'] = false;
//        if ($id) {
//        $isSystemAdmin = Yii::$app->authManager->getAssignment(RBAC_FULL, Yii::$app->user->id);
//        if (!$isSystemAdmin) {
//            $filterAdmin=Customer::ID_ONLY_VIEW;
//        }else{
//            $filterAdmin=0;
//        }
        $model = Customer::showTree($id, 0, true);
        $reVal['action'] = true;
        $reVal['html'] = $model;
        $reVal['id'] = $id;
        $level = 0;
        if ($id > 0) {
            $level = (new Query())->select('MAX(level)')->from(CustomerCase::tableName())->where(['customer_id' => $id])->scalar();
        }
        $reVal['currentLv'] = (int)$level;

//        }
        echo json_encode($reVal);
        Yii::$app->end();
    }

    public function actionNotice()
    {
        if (Yii::$app->user->identity->is_active == Customer::IS_ACTIVE_COMPLETED) {
            return $this->redirect(['index']);
        }
        return $this->render('notice');
    }

    public function actionRegister($ref = null)
    {
        $this->layout = 'login';
        $model = new RegisterForm();
        $model->ref_code = $ref;
        if (!$ref) {
            $ref = Yii::$app->session->get('invite_code');
            $model->ref_code = $ref;
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $customer = $model->createCustomer();
                $errors = $customer->getErrors();
                if (empty($errors)) {
                    //Add case
                    $result = $customer->addCustomerCase();
                    if (isset($result["code"]) && (int)$result["code"] == 200) {
                        LogAction::logUser($customer->id, $customer::tableName(), LogAction::TYPE_USER_CREATE, $customer->getAttributes(['id', 'username', 'full_name', 'sex', 'phone', 'email', 'date_join', 'created_at']));
//                        if($redirect==1){
//                            return $this->redirect(['/invest/'])
//                        }
                        //Gửi Email
                        $params = [
                            'title' => Yii::t('customers', 'Thank you for creating an account with bitoness.com'),
                            'username' => $customer->username,

                        ];
                        SystemEmailQueue::createEmailQueue(
                            SystemEmailQueue::TYPE_CUSTOMER_REGISTERED,
                            $customer->email,
                            '[MEMBER] Register new account',
                            'register-completed',
                            $customer->getAttributes(['id', 'username', 'ref_code', 'email', 'phone', 'full_name'])
                        );

                        Yii::$app->session->removeAllFlashes();
                        Yii::$app->session->setFlash('success', Yii::t('messages', 'Tạo tài khoản thành công.'));
                        return $this->redirect(['index']);
                    } else {
                        //Nếu có lỗi xóa tài khoản đi

                        $customer->delete();
                        Yii::$app->session->removeAllFlashes();
                        Yii::$app->session->setFlash('error', Yii::t('messages', 'Không tạo được tài khoản.'));
                    }

                } else {
                    Yii::$app->session->removeAllFlashes();
                    Yii::$app->session->setFlash('error', Yii::t('messages', 'Không tạo được tài khoản.'));
                }

            } else {
            }
        }

        return $this->render('register', ['model' => $model,]);
    }

    public function actionForgetPassword()
    {
        $model = new RequestPasswordResetForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                /**
                 * @var $customer Customer
                 */
                if ($customer = Customer::findOne(['username' => $model->username])) {
                    $customer->generatePasswordResetToken();
                    if ($customer->save(true, ['updated_at', 'password_reset'])) {
                        $params = [
                            'title' => Yii::t('customers', 'Request Password Reset'),
                            'username' => $customer->username,
                            'token' => $customer->password_reset,
                        ];
//                        $message = \Yii::$app->mailer->compose('forget-password', $params);
//                        $result_code = $message->setFrom([Yii::$app->params["emailSend"] => 'BITONESS.COM'])
//                            ->setTo($customer->email)
//                            ->setSubject(Yii::t('customers', 'Bitoness.com - You have received a password reset request.'))
//                            ->send();

                        SystemEmailQueue::createEmailQueue(
                            SystemEmailQueue::TYPE_CUSTOMER_FORGET_PASSWORD,
                            $customer->email,
                            '[MEMBER] Forget password',
                            'forget-password',
                            [
                                'title' => Yii::t('customers', 'Request Password Reset'),
                                'username' => $customer->username,
                                'token' => $customer->password_reset,
                            ]
                        );

                        Yii::$app->session->removeAllFlashes();
                        Yii::$app->session->setFlash('success', Yii::t('messages', 'Send request successful password recovery.'));
                        return $this->redirect(['index']);
                    }
                }
            }
        }
        $this->layout = 'login';
        return $this->render('forget-password', [
            'model' => $model
        ]);
    }

    public function actionResetPassword($token)
    {
        /* @var $model Customer */
        if (!$token) {
            throw new NotFoundHttpException(Yii::t('customers', 'Found no link request.'));
        }
        $customer = Customer::findByPasswordResetToken($token);
        if (!$customer) {
            throw new NotFoundHttpException(Yii::t('customers', 'Account does not exist'));
        }

        $model = new ResetPasswordForm();
        /**
         * @var $model ResetPasswordForm
         */
        $model->username = $customer->username;
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->resetPassword()) {
                Yii::$app->session->removeAllFlashes();
                Yii::$app->session->setFlash('success', Yii::t('messages', 'You have successfully changed your password.'));
                return $this->redirect(['reset-password', 'token' => $token]);
            }
        }

        $this->layout = 'login';
        return $this->render('reset-password', [
            'model' => $model,
        ]);
    }

    public function createPackage($paramsPost)
    {
        $result = [
            "status" => "error",
            "message" => Yii::t('messages', 'Không tạo được gói đầu tư.'),
        ];
        $model = new CustomerCreatePackageForm();
        $model->customer_id = Yii::$app->user->id;
        if ($model->load($paramsPost) && Yii::$app->params["CUSTOMER_OPEN_CREATE_PACKAGE"]) {
            $result = $model->createPackage();

        } else {
            if (!Yii::$app->params["CUSTOMER_OPEN_CREATE_PACKAGE"]) {
                $result["message"] = Yii::t('messages', 'Website is locking function open new package.');
            }
        }
        return $result;

    }

    public function openPackage()
    {
        $result = [];
        $requestForm = new CustomerCreatePackageForm();
        $packages = Package::getPackages();
        $packageInfo = ArrayHelper::map($packages, 'id', 'package_name');
        $requestForm->package_name = 'Package number ' . (count(Yii::$app->user->identity->getCurrentPackages()) + 1);
        $requestForm->customer_id = Yii::$app->user->id;
        $result['model'] = $requestForm;
        $result['packages'] = $packages;
        $result['packageInfo'] = $packageInfo;
        return $result;
    }

    public function requestCashOut()
    {
        $result = [];
        $model = new CashOutForm();
        $packages = Yii::$app->user->identity->getPackages()->all();
        $package = isset($packages[0]) ? $packages[0] : null;
        $myBanks = Yii::$app->user->identity->getCurrentBankAddress();
        $listBanks = ArrayHelper::map($myBanks, 'id', 'bank_address');
        $totalToken = Yii::$app->user->identity->countCurrentToken();
        $result = [
            'model' => $model,
            'package' => $package,
            'listBanks' => $listBanks,
            'totalToken' => $totalToken,
            'result' => []
        ];
        return $result;
    }

    public function requestCommissionCashOut()
    {
        $result = [];
        $model = new CashOutCommissionForm();
        $result["model"] = $model;
        $myBanks = Yii::$app->user->identity->getCurrentBankAddress();
        $listBanks = ArrayHelper::map($myBanks, 'id', 'bank_address');
        $result['listBanks'] = $listBanks;
        $totalToken = Yii::$app->user->identity->countCurrentToken();
        $result['totalToken'] = $totalToken;
        $wallet = Yii::$app->user->identity->getCurrentWallet();
        $result['wallet'] = $wallet;
        return $result;
    }


}
