<?php

namespace app\modules\customer\controllers;

use app\components\controllers\CustomerBaseController;
use Yii;
use yii\web\ForbiddenHttpException;
use yii\web\MethodNotAllowedHttpException;

/**
 * CustomerTokenController implements the CRUD actions for CustomerToken model.
 */
class PackageController extends CustomerBaseController
{


    public function actionCreatePackage()
    {
        if (!Yii::$app->user->id) {
            throw new MethodNotAllowedHttpException('You not have permission to access this action');
        }
        if (!Yii::$app->request->isPjax) {
            throw new  ForbiddenHttpException('This access not exactly!');
        }
        $type = Yii::$app->request->post('type', '');
        switch ($type) {

        }
    }
}