<?php

namespace app\modules\customer\controllers;

use app\components\controllers\CustomerBaseController;
use app\models\CustomerCase;
use app\modules\customer\forms\search\CustomerCaseSearch;
use Yii;
use app\models\CustomerTransaction;
use app\modules\customer\forms\CustomerTransactionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MyTransactionController implements the CRUD actions for CustomerTransaction model.
 */
class MyTransactionController extends CustomerBaseController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerTransaction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerTransactionSearch();
        $params=Yii::$app->request->queryParams;
        $params["CustomerTransactionSearch"]["customer_id"]=Yii::$app->user->id;
        $dataProvider = $searchModel->search($params);
        $allModels = $dataProvider->getModels();
        $bills = [];
        foreach ($allModels as $model) {
            if ($model->bill_id > 0) {
                $bills[] = $model->bill_id;
            }
        }
        $billData=[];
        if(!empty($bills)){
            $billData=CustomerCase::find()->where(['id'=>$bills])->indexBy('id')->all();
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'billData'=>$billData
        ]);

    }

    /**
     * Displays a single CustomerTransaction model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomerTransaction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomerTransaction();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CustomerTransaction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CustomerTransaction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomerTransaction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerTransaction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerTransaction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
