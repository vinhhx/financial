<?php

namespace app\modules\customer\controllers;

use app\components\controllers\CustomerBaseController;
use app\forms\CustomerTokenTransactionSearch;
use app\forms\ExchangeTokenForm;
use app\models\CustomerTokenTransaction;
use Yii;
use app\models\CustomerToken;
use app\forms\CustomerTokenSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CustomerTokenController implements the CRUD actions for CustomerToken model.
 */
class CustomerTokenController extends CustomerBaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerToken models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerTokenSearch();
        $params = Yii::$app->request->queryParams;
        $params['CustomerTokenSearch']['customer_id'] = Yii::$app->user->identity->id;
        $dataProvider = $searchModel->search($params);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CustomerToken model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomerToken model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
//        $model = new CustomerToken();
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
    }

    /**
     * Updates an existing CustomerToken model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
//        $model = $this->findModel($id);
//
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->id]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
    }

    /**
     * Deletes an existing CustomerToken model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
//        $this->findModel($id)->delete();
//
//        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomerToken model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerToken the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerToken::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionExchange()
    {
        $model = new ExchangeTokenForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->type=CustomerToken::TYPE_CUSTOMER;
            if ($model->validate()) {
                $result = $model->process();
                if (isset($result["status"]) && $result["status"] == "success") {
                    Yii::$app->session->setFlash('success', 'You sent ' . $model->quantity . ' token to customer code ' . $model->customer_username);
                    return $this->redirect(['index']);
                } else {
                    Yii::$app->session->setFlash('error', $result["message"]);
                }
            }
        }
        return $this->render('exchange', ['model' => $model]);
    }

    public function actionTransactionHistory(){
        $searchModel = new CustomerTokenTransactionSearch();
        $params = Yii::$app->request->queryParams;
        $params['CustomerTokenTransactionSearch']['customer_id'] = Yii::$app->user->identity->id;
        $params['CustomerTokenTransactionSearch']['type_module'] = CustomerTokenTransaction::TYPE_MODULE_CUSTOMER;
        $dataProvider = $searchModel->search($params);

        return $this->render('transaction/index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


}
