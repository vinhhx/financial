<?php

namespace app\modules\customer\controllers;

use app\components\controllers\CustomerBaseController;
use Yii;
use app\models\Note;
use app\modules\customer\forms\search\NoteSearch;

class NoteController extends CustomerBaseController
{

    public function beforeAction($action)
    {
        switch ($action->id) {
            case 'ajax-get-all-note':
                $this->enableCsrfValidation = false;
                break;
        }
        return parent::beforeAction($action);
    }

    public function actionListing()
    {
        $searchModel = new NoteSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        return $this->render('listing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIndex()
    {
        $objectId = Yii::$app->request->post('id');
        $objectTable = Yii::$app->request->post('table');
        $model = new Note();

        if ($model->load(Yii::$app->request->post())) {
            $model->setScenario('addnote');
            $model->created_by = Yii::$app->user->identity->username;
            $model->created_id = Yii::$app->user->identity->id;
            $model->type = Note::TYPE_CUSTOMER;
            if ($model->save()) {
                Yii::$app->response->format = 'json';
                return ['status' => 'pass', 'id' => $model->object_id];
            } else {
                Yii::$app->response->format = 'json';
                return $model->getErrors();
            }
        }

        if ($objectId && $objectTable) {
            $listNote = Note::find()
                ->where(['object_table' => $objectTable, 'object_id' => $objectId, 'type' => Note::TYPE_CUSTOMER])
                ->orderBy(['id' => SORT_DESC])
                ->all();
            return $this->renderPartial('_form', [
                'model' => $model,
                'objectId' => $objectId,
                'objectTable' => $objectTable,
                'listNote' => $listNote,
            ]);
        }
    }

    public function actionAjaxGetAllNote()
    {
        $request = Yii::$app->request->post();
        $tables = isset($request['tables']) ? $request['tables'] : [];
        $ids = isset($request['ids']) ? $request['ids'] : [];
        $result = [];
        if (!empty($tables) && !empty($ids)) {
            $total = Note::find()
                ->where([
                    'object_table' => $tables,
                    'object_id' => $ids,
                    'type' => Note::TYPE_CUSTOMER,
                ])
                ->orderBy('id')
                ->all();

            $notes = [];
            if ($total) {
                foreach ($total as $item) {
                    $notes[$item->object_table . '|' . $item->object_id][] = $item;
                }
                if (!empty($notes)) {
                    foreach ($notes as $key => $note) {
                        $result[$key] = count($note);
                    }
                }
            }
        }
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $result;
    }

}
