<?php

namespace app\modules\customer\forms;

use app\models\Customer;
use Yii;
use yii\base\Model;

class ResetPasswordForm extends Model
{
    public $username;
    public $newPassword;
    public $newPasswordConfirmation;

    public function rules()
    {
        return [
            [['newPassword', 'newPasswordConfirmation'], 'required'],
            ['newPassword', 'string', 'min' => 6],
            ['newPasswordConfirmation', 'compare', 'compareAttribute' => 'newPassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'newPassword' => Yii::t('customers', 'Password'),
            'newPasswordConfirmation' => Yii::t('customers', 'Confirm Password'),
        ];
    }

    public function resetPassword()
    {
        $model = Customer::findByUsername($this->username);
        if ($model) {
            /**
             * @var $model Customer
             */
            $model->setPassword($this->newPassword);
            return $model->save(true, ['password_hash', 'updated_at']);
        }
        return false;
    }

}