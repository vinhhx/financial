<?php

namespace app\modules\customer\forms\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerRequest;
use Yii;


/**
 * CustomerRequestSearch represents the model behind the search form about `app\models\CustomerRequest`.
 */
class CustomerRequestSearch extends CustomerRequest
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'type', 'status', 'created_at', 'package_id', 'updated_at'], 'integer'],
            [['block_chain_wallet_id'], 'safe'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerRequest::find();
        $query->where(['customer_id' => Yii::$app->user->id]);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
//            'customer_id' => $this->customer_id,
            'type' => $this->type,
            'status' => $this->status,
            'amount' => $this->amount,
            'created_at' => $this->created_at,
            'package_id' => $this->package_id,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'block_chain_wallet_id', $this->block_chain_wallet_id]);

        return $dataProvider;
    }
}
