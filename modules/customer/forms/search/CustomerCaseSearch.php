<?php

namespace app\modules\customer\forms\search;

use app\models\CustomerCase;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Customer;

/**
 * CustomerSearch represents the model behind the search form about `app\models\Customer`.
 */
class CustomerCaseSearch extends CustomerCase
{

    public $start_date;
    public $end_date;
    public $package_id;
    public $username;
    public $full_name;
    public $phone;
    private $dataProvider;

//    /**
//     * @inheritdoc
//     */
    public function rules()
    {
        return [
            [['customer_id', 'parent_id', 'status', 'created_at', 'updated_at', 'level','package_id'], 'integer'],
            [['customer_username','start_date','end_date','username','full_name','phone'], 'string', 'max' => 150],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $pagination = true)
    {
        if ($pagination) {
            $pagination = [
                'pageSize' => 30,
            ];
        }
        $query = CustomerCase::find();
        $query->joinWith('customerParent', true, 'RIGHT JOIN');
        $this->dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => $pagination,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
        ]);

        $this->load($params);

        $query->andWhere(['customer_case.status' => CustomerCase::STATUS_ACTIVE]);
//        $query->andWhere(["!=", "is_deleted", self::IS_DELETED]);

        if ($this->start_date) {
            $query->andFilterWhere(['>=', 'customer.date_join', Yii::$app->formatter->asDate($this->start_date, 'php:Y-m-d')]);
        }
        if ($this->end_date) {
            $query->andFilterWhere(['<=', 'customer.date_join', Yii::$app->formatter->asDate($this->end_date, 'php:Y-m-d')]);
        }
        $query->andFilterWhere([
            'customer_case.id' => $this->id,
            'customer_case.package_id' => $this->package_id,
            'customer_case.parent_id' => $this->parent_id,
//            'amount' => $this->amount,
//            'status' => $this->status,
//            'sex' => $this->sex,
//            'last_auto_increase' => $this->last_auto_increase,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'customer.username', $this->username])
//            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
//            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
//            ->andFilterWhere(['like', 'password_reset', $this->password_reset])
            ->andFilterWhere(['like', 'customer.full_name', $this->full_name])
            ->andFilterWhere(['like', 'customer.phone', $this->phone]);
//            ->andFilterWhere(['like', 'email', $this->email])
//            ->andFilterWhere(['like', 'nation', $this->nation])
//            ->andFilterWhere(['like', 'address', $this->address]);

        return $this->dataProvider;;
    }

    public function searchChildrenAccount($params, $pagination = true)
    {
        if ($pagination) {
            $pagination = [
                'pageSize' => 30,
            ];
        }
        $query = CustomerCase::find();
        $query->joinWith('customer', true, 'LEFT JOIN');
        $query->where(['!=', 'customer_id', Yii::$app->user->id]);
        $this->dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => $pagination,
            'sort' => [
                'defaultOrder' => [
                    'level' => SORT_ASC,
                ]
            ]
        ]);

        $this->load($params);

        $query->andWhere(['customer_case.status' => CustomerCase::STATUS_ACTIVE]);
//        $query->andWhere(["!=", "is_deleted", self::IS_DELETED]);

        if ($this->start_date) {
            $query->andFilterWhere(['>=', 'customer.date_join', Yii::$app->formatter->asDate($this->start_date, 'php:Y-m-d')]);
        }
        if ($this->end_date) {
            $query->andFilterWhere(['<=', 'customer.date_join', Yii::$app->formatter->asDate($this->end_date, 'php:Y-m-d')]);
        }

        $query->andFilterWhere([
            'customer_case.id' => $this->id,
            'customer.package_id' => $this->package_id,
            'customer_case.parent_id' => $this->parent_id,
//            'amount' => $this->amount,
//            'status' => $this->status,
//            'sex' => $this->sex,
//            'last_auto_increase' => $this->last_auto_increase,
//            'created_at' => $this->created_at,
//            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'customer_case.customer_username', $this->customer_username])
//            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
//            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
//            ->andFilterWhere(['like', 'password_reset', $this->password_reset])
//            ->andFilterWhere(['like', 'customer.full_name', $this->full_name])
            ->andFilterWhere(['like', 'customer.phone', $this->phone]);
//            ->andFilterWhere(['like', 'email', $this->email])
//            ->andFilterWhere(['like', 'nation', $this->nation])
//            ->andFilterWhere(['like', 'address', $this->address]);
//        echo'<pre>';var_dump($query);die;

        return $this->dataProvider;
    }

    public function statisticCutomerJoin($params)
    {
        $this->load($params);
        if ($this->start_date && !$this->end_date) {
            $this->start_date = date('Y-m-d', strtotime($this->start_date));
            $this->end_date = date('Y-m-d');
        } elseif ($this->end_date && !$this->start_date) {
            $this->end_date = date('Y-m-d', strtotime($this->end_date));
            $this->start_date = date('Y-m-d', strtotime("-30 day", strtotime($this->end_date)));
        } elseif (!$this->start_date && !$this->end_date) {
            $this->end_date = date('Y-m-d');
            $this->start_date = date('Y-m-d', strtotime("-30 day", time()));
        } else {
            $this->end_date = date('Y-m-d', strtotime($this->end_date));
            $this->start_date = date('Y-m-d', strtotime($this->start_date));
        }

        $query = Customer::find();
        $query->select("date_join,
        COUNT(id) AS quantity");
        $query->groupBy('date_join');
        $query->having(["BETWEEN", 'date_join', $this->start_date, $this->end_date]);
        $query->asArray();
        $data = $query->all();
        //dk lọc order paid
        return $data;
    }

    public function getCustomerTotalCashOut($type = null)
    {
        // Tính tổng tiền khách hàng có thể rút
        $cloneDataProvider = clone $this->dataProvider->query;
        $dataTotal = $cloneDataProvider
            ->select('COUNT(id) AS total_customer, SUM(amount) AS total_amount')->asArray()->one();
        return $dataTotal;
    }

    public function statisticCutomerCustomerCashout()
    {
        $query = Customer::find();
        $query->where(["status" => Customer::STATUS_ACTIVE]);
        $query->andWhere([">=", "amount", 25]);

        $this->dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
        ]);

        return $this->dataProvider;
    }

}
