<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 14-Dec-15
 * Time: 11:09 PM
 */

namespace app\modules\customer\forms;

use app\models\Customer;
use app\models\CustomerPackage;
use app\models\CustomerPoundageTransaction;
use app\models\CustomerRequest;
use app\models\CustomerRequireCashout;
use app\models\CustomerToken;
use app\models\CustomerTransaction;
use app\models\CustomerTransactionQueue;
use app\models\LogAction;
use app\models\SystemEmailQueue;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\Html;
use Yii;

class CashOutCommissionForm extends Model
{
    public $amount;
    public $customer_id;
    public $bank_address;
    public $real_amount;

    public function rules()
    {
        return [
            [['customer_id'], 'integer'],
            [['amount', 'customer_id', 'bank_address'], 'required', 'skipOnEmpty' => false, 'skipOnError' => false],
            [['amount'], 'number', 'min' => Yii::$app->params["min_cash_out_amount"], 'max' => Yii::$app->params["max_cash_out_amount"]],
            ['amount', 'validateAmount'],
            ['real_amount', 'safe'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'amount' => Yii::t('customers', 'Số BTC yêu cầu nhận'),
            'wallet_block_chain' => Yii::t('customers', 'Địa chỉ ngân hàng thụ hưởng'),
            'bank_address' => Yii::t('customers', 'Your bitcoin wallet address'),
        ];
    }

    public function validateAmount($attribute, $params)
    {
        if ($this->$attribute < Yii::$app->params["min_cash_out_amount"] || $this->$attribute > Yii::$app->params["max_cash_out_amount"]) {
            $this->addError($attribute, 'Number BTC require cash out at between ' . Yii::$app->params["min_cash_out_amount"] . ' BTC and ' . Yii::$app->params["max_cash_out_amount"] . ' BTC');
        }
        $wallet = Yii::$app->user->identity->getCurrentWallet();

        if ($wallet->poundage < $this->$attribute) {
            $this->addError($attribute, 'Number BTC require cash out must be less than BTC you have!');
        }

    }

    public function createCashOut()
    {
        // Số dư tk
        $result = [
            "status" => 'error',
            "error_code" => 300,
            'message' => '',
        ];
        $wallet = Yii::$app->user->identity->getCurrentWallet();

        if ($wallet->poundage < $this->amount) {
            $result["status"] = "error";
            $result["error_code"] = 301;
            $result["message"] = \Yii::t('customers', 'Số dư không đủ để tạo yêu cầu nhận') . Yii::$app->formatter->asDecimal($this->amount, 8);
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $customerRequest = new CustomerRequest();
            $customerRequest->customer_id = Yii::$app->user->id;
            $customerRequest->type = CustomerRequest::TYPE_RECEIVE_WHEN_CASHOUT_POUNDAGE;
            $customerRequest->package_id = 0;
            $customerRequest->status = CustomerRequest::STATUS_CREATED;
            $customerRequest->amount = $this->amount;
            $customerRequest->amount_exactly = round((($this->amount / 100) * (100 - Yii::$app->params["CustomerPoundageCashOutFee"])), 8);
            $customerRequest->block_chain_wallet_id = $this->bank_address;
            if ($customerRequest->save()) {
                $customerTransactionQueue = new CustomerTransactionQueue();
                $customerTransactionQueue->customer_request_id = $customerRequest->id;
                $customerTransactionQueue->customer_id = Yii::$app->user->identity->id;
                $customerTransactionQueue->customer_username = Yii::$app->user->identity->username;
                $customerTransactionQueue->type = CustomerTransaction::TYPE_CASH_OUT_POUNDAGE;
                $customerTransactionQueue->type_step = CustomerTransaction::TYPE_STEP_CASH_OUT_POUNDAGE_1;
                $customerTransactionQueue->status = CustomerTransactionQueue::STATUS_PENDING;
                $customerTransactionQueue->start_at = time();
                $customerTransactionQueue->amount = $customerRequest->amount_exactly;
                if ($customerTransactionQueue->save()) {
//                    $token = CustomerToken::find()->where(['customer_id' => Yii::$app->user->id, 'status' => CustomerToken::STATUS_PENDING])->orderBy(['id' => SORT_ASC])->limit(1)->one();
//                    if (!$token) {
//                        throw new Exception('Not enought Token!');
//                    }
//                    $token->status = CustomerToken::STATUS_USED;
//                    if (!$token->save(true, ['status', 'updated_at'])) {
//                        throw new Exception(\Yii::t('customers', 'Hệ thống trừ token có lỗi'));
//                    }
                    $poundageBefore = $wallet->poundage;
                    $wallet->poundage -= $this->amount;
                    if ($wallet->save(true, ['poundage', 'updated_at'])) {
//                        Cập nhật bảng poundadge transaction
                        $poundage = new CustomerPoundageTransaction();
                        $poundage->bill_id = $customerRequest->id;
                        $poundage->customer_id = Yii::$app->user->identity->id;
                        $poundage->sign = CustomerPoundageTransaction::SIGN_SUB;
                        $poundage->customer_username = Yii::$app->user->identity->username;
                        $poundage->from_id = 0;
                        $poundage->from_username = 'system';
                        $poundage->status = CustomerPoundageTransaction::STATUS_HOLDING;
                        $poundage->amount = $this->amount;
                        $poundage->balance_before = $poundageBefore;
                        $poundage->balance_after = $wallet->poundage;
                        $poundage->type = CustomerPoundageTransaction::TYPE_CASH_OUT_POUND;
                        if (!$poundage->save()) {
                            throw new Exception(\Yii::t('customers', 'Not save poundage transaction'));
                        }
                        $transaction->commit();
                        $result["status"] = "success";
                        $result["error_code"] = 0;
                        $result["message"] = '';
                        //Gửi email thông báo đặt lệnh rút tiền hoa hồng thành công\
                        $params = $customerRequest->getAttributes(["id", "customer_id", "block_chain_wallet_id", "type", "status", "amount"]);
                        $params["username"] = Yii::$app->user->identity->username;
                        SystemEmailQueue::createEmailQueue(
                            SystemEmailQueue::TYPE_CUSTOMER_WITHDRAW_POUNDAGE,
                            Yii::$app->user->identity->email,
                            'You have request withdraw commission - ' . date('Y-m-d H:i:s'),
                            'withdraw-commission',
                            $params
                        );

                    }
                } else {
                    throw new Exception('Not completed require cash out commission!');
                }
            }

        } catch (\Exception $ex) {
            $transaction->rollBack();
            $result["status"] = "error";
            $result["error_code"] = 400;
            $result["message"] = $ex->getMessage();

        }

        return $result;

    }


}
