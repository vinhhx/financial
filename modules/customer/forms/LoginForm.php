<?php

namespace app\modules\customer\forms;

use app\models\Admin;
use app\models\Customer;
use Yii;
use yii\base\Model;

/**
 * Class LoginForm
 * @package app\modules\admin
 * @author Quan
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = false;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'Username'),
            'password' => Yii::t('app', 'Pasword'),
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            if ($user = Customer::findOne(['username' => $this->username])) {
                if ($user->status == Customer::STATUS_ACTIVE) {
                    if (!$user->validatePassword($this->password)) {
                        $this->addError($attribute, Yii::t('messages', 'The password you entered is incorrect. Please try again.'));
                    }
                } else {
                    $this->addError('username', Yii::t('messages', 'Your account has been locked, please contact website administrator or your sponsor'));
                }
            } else {
                $this->addError('username', Yii::t('messages', 'The username you entered is incorrect. Please try again.'));
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            if ($user->is_deleted != Customer::IS_DELETED) {
                return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
            }
        }
        $this->addError('username', Yii::t('messages', 'The username you entered is incorrect. Please try again.'));
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = Customer::findByUsername($this->username);
        }

        return $this->_user;
    }
}