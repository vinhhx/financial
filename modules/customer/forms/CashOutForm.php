<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 14-Dec-15
 * Time: 11:09 PM
 */

namespace app\modules\customer\forms;

use app\models\Customer;
use app\models\CustomerPackage;
use app\models\CustomerRequest;
use app\models\CustomerRequireCashout;
use app\models\CustomerToken;
use app\models\CustomerTransaction;
use app\models\CustomerTransactionQueue;
use app\models\LogAction;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\Html;
use Yii;

class CashOutForm extends Model
{
    public $amount;
    public $wallet_block_chain;
    public $customer_package_id;

    public function rules()
    {
        return [
            [['customer_package_id'], 'integer'],
            [['amount'], 'number', 'min' => Yii::$app->params["min_cash_out_amount"]],
            [['wallet_block_chain'], 'string'],
            [['amount', 'wallet_block_chain', 'customer_package_id'], 'required', 'skipOnEmpty' => false, 'skipOnError' => false],
//            [['amount'], 'validateAmount'],
//            [['captcha'], 'captcha','captchaAction' => '/customer/default/captcha'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'amount' => Yii::t('customers', 'Số BTC yêu cầu nhận'),
            'wallet_block_chain' => Yii::t('customers', 'Địa chỉ ngân hàng thụ hưởng'),
        ];
    }


    public function createCashOut()
    {
        $package = CustomerPackage::findOne(['customer_id' => Yii::$app->user->id, 'id' => $this->customer_package_id]);
        if (!$package) {
            return false;
        }
        $this->amount = $package->balance;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $customerRequest = new CustomerRequest();
            $customerRequest->customer_id = Yii::$app->user->id;
            $customerRequest->type = CustomerRequest::TYPE_RECEIVE_WHEN_CASHOUT_PACKAGE;
            $customerRequest->package_id = $this->customer_package_id;
            $customerRequest->status = CustomerRequest::STATUS_CREATED;
            $customerRequest->amount = $this->amount;
            $customerRequest->amount_exactly = $this->amount;
            $customerRequest->block_chain_wallet_id = $this->wallet_block_chain;
            if ($customerRequest->save()) {
                $customerTransactionQueue = new CustomerTransactionQueue();
                $customerTransactionQueue->customer_request_id = $customerRequest->id;
                $customerTransactionQueue->customer_id = Yii::$app->user->identity->id;
                $customerTransactionQueue->customer_username = Yii::$app->user->identity->username;
                $customerTransactionQueue->type = CustomerTransaction::TYPE_CASH_OUT_PACKAGE;
                $customerTransactionQueue->type_step = CustomerTransaction::TYPE_STEP_CASH_OUT_PACKAGE_1;
                $customerTransactionQueue->status = CustomerTransactionQueue::STATUS_PENDING;
                $customerTransactionQueue->start_at = time();
                $customerTransactionQueue->amount = $this->amount;
                if ($customerTransactionQueue->save()) {
                    //Trừ token
                    $token = CustomerToken::find()->where(['customer_id' => Yii::$app->user->id, 'status' => CustomerToken::STATUS_PENDING])->orderBy(['id' => SORT_ASC])->limit(1)->one();
                    if (!$token) {
                        throw new Exception('Not enought Token!');
                    }
                    $token->status = CustomerToken::STATUS_USED;
                    if (!$token->save(true, ['status', 'updated_at'])) {
                        throw new Exception(\Yii::t('customers', 'Không cập nhật được trạng thái token'));
                    }
                    $package->balance -= $this->amount;
                    if ($package->save(true, ['balance', 'updated_at'])) {
                        $transaction->commit();
                        return true;
                    }
                }
            }

        } catch (\Exception $ex) {
            $transaction->rollBack();
            return false;
        }

        return false;

    }


}
