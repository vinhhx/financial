<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 2:11 PM
 */
namespace app\modules\customer\forms;

use app\models\CustomerPackage;
use app\models\CustomerRequireCashout;
use app\models\CustomerTransaction;
use app\models\LogAction;
use app\models\Package;
use app\models\SystemEmailQueue;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\Html;
use Yii;

class CustomerCreatePackageForm extends Model
{

    public $customer_id;
    public $bank_address;
    public $sent_bit_coin_first;
    public $package_id;
    public $package_name;


    public function rules()
    {
        return [
            [['customer_id', 'package_id'], 'required'],
            [['sent_bit_coin_first'], 'number'],
            [['customer_id', 'package_id'], 'integer'],
            [['bank_address', 'package_name'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'customer_id' => Yii::t('customers', 'Mã người chơi'),
            'sent_bit_coin_first' => Yii::t('customers', 'Cho lần đầu'),
            'bank_address' => Yii::t('customers', 'Địa chỉ ngân hàng thụ hưởng'),
            'package_name' => Yii::t('customers', 'Đặt tên cho gói đầu tư'),
        ];
    }

    public function createPackage()
    {
        $customerPackage = new CustomerPackage();
        $package = Package::findOne(['id' => $this->package_id, 'status' => Package::STATUS_ACTIVE]);
        if (!$package) {
            return [
                "status" => 'error',
                "error_code" => '102',
                "message" => Yii::t('messages', 'Gói đầu tư không chính xác'),
            ];
        }
        $customerPackage->customer_id = $this->customer_id;
        $customerPackage->package_id = $package->id;
        $customerPackage->package_amount = $package->amount;
        $customerPackage->package_amount_root = $package->amount;
        $customerPackage->increase_amount = $package->increase_percent;
        $customerPackage->package_name = ($this->package_name) ? $this->package_name : $package->package_name . '-' . date('Y-m-d');
        $customerPackage->block_chain_wallet_id = $this->bank_address;
        $customerPackage->status = CustomerPackage::STATUS_REQUEST_IN_STEP_ONE;
        $customerPackage->is_reinvestment = (Yii::$app->user->identity->is_invest) ? 1 : 0;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($customerPackage->save()) {
                $customerPackage->reinvestment_date = $customerPackage->created_at;
                $customerPackage->save(true, ['reinvestment_date']);
                //Tạo gói đầu tư rồi giờ tạo request
                $result = $customerPackage->requestOpenPackage($this->sent_bit_coin_first);
                if ($result["status"] == "success") {
                    //sent email

                    $transaction->commit();
                    $params = $customerPackage->getAttributes(['id', 'package_name', 'package_amount', 'increase_amount']);
                    $params["username"] = Yii::$app->user->identity->username;
                    SystemEmailQueue::createEmailQueue(
                        SystemEmailQueue::TYPE_CUSTOMER_OPEN_PACKAGE,
                        Yii::$app->user->identity->email,
                        'Open package ' . $customerPackage->package_name . ' completed',
                        'open-package-completed',
                        $params
                    );
                } else {
                    $transaction->rollBack();
                }
                return $result;
            }
        } catch (\Exception $ex) {
            $transaction->rollBack();
            return [
                "status" => "error",
                "error_code" => $ex->getCode(),
                "message" => $ex->getMessage(),

            ];
        }


    }

}


