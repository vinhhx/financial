<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 2:11 PM
 */
namespace app\modules\customer\forms;

use app\models\CustomerPackage;
use app\models\CustomerRequireCashout;
use app\models\CustomerTransaction;
use app\models\LogAction;
use app\models\Package;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\Html;
use Yii;

class CustomerInvestPackageForm extends Model
{

    public $customer_id;
    public $package_id;
    public $customer_package_id;
    public $package_name;


    public function rules()
    {
        return [
            [['customer_id', 'package_id', 'customer_package_id'], 'required'],
            [['customer_id', 'package_id', 'customer_package_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'customer_id' => Yii::t('customers', 'Mã người chơi'),
            'package_name' => Yii::t('customers', 'Đặt tên cho gói đầu tư'),
            'package_id' => Yii::t('customers', 'Mã gói đầu tư'),
            'customer_package_id' => Yii::t('customers', 'Mã gói đầu tư của người chơi'),
        ];
    }

    public function investPackage()
    {
        $customerPackage = CustomerPackage::findOne(['id' => $this->customer_package_id, 'customer_id' => Yii::$app->user->id]);
        $package = Package::findOne(['id' => $this->package_id, 'status' => Package::STATUS_ACTIVE]);
        if (!$customerPackage || !$customerPackage->canReinvestment() || !$package) {
            return [
                "status" => 'error',
                "error_code" => 104,
                "message" => Yii::t('messages','Gói đầu tư không chính xác'),
            ];
        }
        //ktra gói đầu tư mới
        $customerPackage->package_id = $package->id;
        $customerPackage->package_amount = $package->amount;
        $customerPackage->increase_amount = $package->increase_percent;
        $customerPackage->package_name = ($this->package_name) ? $this->package_name : $customerPackage->package_name;
        $customerPackage->status = CustomerPackage::STATUS_REQUEST_IN_STEP_ONE;
        $customerPackage->is_reinvestment=1;
        $customerPackage->reinvestment_date=time();
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($customerPackage->save(true,['package_id','package_amount','increase_amount','package_name','status','is_reinvestment','reinvestment_date','updated_at'])) {
                //Tạo gói đầu tư rồi giờ tạo request
                $result = $customerPackage->requestOpenPackage();
                if ($result["status"] == "success") {
                    $transaction->commit();
                } else {
                    $transaction->rollBack();
                }
                return $result;
            }
        } catch (\Exception $ex) {
            $transaction->rollBack();
            return [
                "status" => "error",
                "error_code" => $ex->getCode(),
                "message" => $ex->getMessage(),

            ];
        }


    }

}


