<?php
namespace app\modules\customer\forms;

use app\models\Customer;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;

class RequestPasswordResetForm extends Model
{
    public $email;
    public $username;
    public $captcha;

    public function rules()
    {
        return [
//            ['email', 'email'],
            ['username', 'string'],
//            ['email', 'validateEmail'],
            ['username', 'validateUsername'],
            [['captcha'], ReCaptchaValidator::className(), 'secret' => Yii::$app->params["GOOGLE_CAPTCHA_SECRET_KEY"]]
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => Yii::t('customers', 'Username'),
            'phone' => Yii::t('customers', 'Phone'),
            'captcha' => Yii::t('customers', 'Captcha'),
        ];
    }

    public function validateEmail($attribute, $params)
    {
        $model = Customer::findByEmail($this->email);
        if (!$model) {
            $this->addError('email', Yii::t('customers', 'Email does not exist'));
        } else {
            if ($model->status == Customer::STATUS_INACTIVE) {
                $this->addError('email', Yii::t('customers', 'Account not activated'));
            }
        }
    }

    public function validateUsername($attribute, $params)
    {
        $model = Customer::findByUsername($this->username);
        if (!$model) {
            $this->addError('username', Yii::t('customers', 'Username does not exist'));
        } else {
            if ($model->status == Customer::STATUS_INACTIVE) {
                $this->addError('username', Yii::t('customers', 'Account not activated'));
            }
        }
    }

}