<?php
namespace app\modules\customer\forms;

use app\models\Customer;
use app\models\User;
use himiklab\yii2\recaptcha\ReCaptcha;
use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;

class ChangePasswordForm extends Model
{
    public $currentPassword;
    public $newPassword;
    public $newPasswordConfirmation;
    public $captcha;
    public $customerId;

    public function rules()
    {
        return [
            [['currentPassword', 'newPassword', 'newPasswordConfirmation'], 'required'],
            ['currentPassword', 'validateCurrentPassword'],

            ['newPassword', 'string', 'min' => 6],
            ['newPasswordConfirmation', 'compare', 'compareAttribute' => 'newPassword'],
            [['captcha'], ReCaptchaValidator::className(),'secret' => Yii::$app->params["GOOGLE_CAPTCHA_SECRET_KEY"]]
        ];
    }

    public function attributeLabels()
    {
        return [
            'currentPassword' => Yii::t('customers', 'Mật khẩu hiên tại'),
            'newPassword' => Yii::t('customers', 'Mật khẩu mới'),
            'newPasswordConfirmation' => Yii::t('customers', 'Nhập lại mật khẩu'),
            'captcha' => Yii::t('customers', 'Mã bảo mật'),
        ];
    }

    public function validateCurrentPassword($attribute, $params)
    {
        $customer = Yii::$app->user->identity;
        if (!$customer->validatePassword($this->$attribute)) {
            $this->addError($attribute, Yii::t('customers', 'Mật khẩu không chính xác'));
        }


    }

    public function changePassword()
    {
        Yii::$app->user->identity->setPassword($this->newPassword);
        return Yii::$app->user->identity->save(true, ['password_hash', 'updated_at']);
    }

}