<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 12-Jun-16
 * Time: 2:11 PM
 */
namespace app\modules\customer\forms;

use app\models\CustomerRequireCashout;
use app\models\CustomerTransaction;
use app\models\LogAction;
use app\models\SystemEmailQueue;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Transaction;
use yii\helpers\Html;
use Yii;
use yii\web\UploadedFile;

class CustomerActionForm extends Model
{
    public $file;
    public $transaction_id;

    public function rules()
    {
        return [
            [['transaction_id'], 'required'],
            [['transaction_id'], 'integer'],
            [['file'], 'file', 'extensions' => 'jpg, png, gif', 'mimeTypes' => 'image/jpeg, image/png, image/gif', 'maxSize' => 8 * 1024 * 1024 /* 1 Mb */],
            [['file'], 'safe'],

        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => Yii::t('customer', 'File'),
        ];
    }

    public function uploadAttachment()
    {
        $query = CustomerTransaction::find();
        $query->where(['id' => $this->transaction_id]);
        $query->andWhere(['customer_sent_id' => Yii::$app->user->id]);
        $query->limit(1);
        $customerTransaction = $query->one();
        $result = [
            "status" => "error",
            "message" => ""
        ];
        if ($customerTransaction) {
            $result['model'] = $customerTransaction;
            $result['modelForm'] = $this;
            if ($this->file) {
                $uploadDir = 'customer/' . Yii::$app->user->id;
                $customerTransaction->attachment = Yii::$app->image->upload(0, $uploadDir, $this->file);
                if ($customerTransaction->attachment) {
                    $customerTransaction->status = CustomerTransaction::STATUS_SEND_COMPLETED;
                    if ($customerTransaction->save(true, ['attachment', 'status', 'updated_at'])) {
                        $params = [];
                        $params["username"] = Yii::$app->user->identity->username;
                        $params["amount"] = $customerTransaction->amount;
                        $params["image"] = $customerTransaction->attachment;
                        SystemEmailQueue::createEmailQueue(
                            SystemEmailQueue::TYPE_CUSTOMER_PROVIDER_HELP,
                            Yii::$app->user->identity->email,
                            'Provider help sent btc - ' . date('Y-m-d H:i:s'),
                            'provider-help-sent',
                            $params
                        );
                        $result["status"] = "success";
                    } else {
                        $this->addError('file', 'Error');
                        $result["status"] = "error";
                        $result["message"] = "Update status transaction error";
                        $result['model'] = $customerTransaction;
                        $result['modelForm'] = $this;
                    }
                } else {
                    $result["status"] = "error";
                    $result["message"] = "Not upload attachment";
                    $result['model'] = $customerTransaction;
                    $result['modelForm'] = $this;
                }
            } else {
                $result["status"] = "error";
                $result["message"] = "Not found attachment";
                $result['model'] = $customerTransaction;
                $result['modelForm'] = $this;
            }

        } else {
            $result["status"] = "error";
            $result["message"] = "This transaction not existed";
            $result['model'] = $customerTransaction;
            $result['modelForm'] = $this;
        }
        return $result;
    }

}


