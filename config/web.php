<?php

$params = require(__DIR__ . '/params.php');
use \yii\web\Request;

$baseUrl = str_replace('/web', '', (new Request)->getBaseUrl());
$config = [
    'id' => 'lincol-app',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'admin' => [
            'class' => 'app\modules\admin\AdminModule',
            // ... other configurations for the module ...
        ],
        'customer' => [
            'class' => 'app\modules\customer\Module',
            // ... other configurations for the module ...
        ],
        'invest' => [
            'class' => 'app\modules\invest\Module',
            // ... other configurations for the module ...
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ]
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'YNEEmxsYIsNToQcpENgbTFSf4jkqtTFG',
            'baseUrl' => $baseUrl,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\Customer',
            'enableAutoLogin' => true,
            'loginUrl' => ['site/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@app/mail',
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.fastmail.com',
                'username' => 'support@bitoness.com',
                'password' => '7pqm46cj8wdnveuu',
                'port' => 587,
                'encryption' => 'tls',
            ],
        ],
        'sentEmail' => [
            'class' => 'app\components\SentEmail'
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
        'urlManager' => require(__DIR__ . '/routes.php'),
        /*
         * Config language
         * default BasePath @app/messages
         * echo Yii::t('file', 'message translate');
         * @app/messages/<LanguageCode>/file.php
         */
        'language' => 'en',
        'sourceLanguage' => 'en_GB',
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource'
                ],
            ],
        ],
//        'timeZone' => 'Asia/Ho_Chi_Minh',
        'formatter' => [
            'dateFormat' => 'yyyy-MM-dd',
            'datetimeFormat' => 'yyyy-MM-dd H:mm',
            'decimalSeparator' => ',',
            'thousandSeparator' => '.',
            'timeZone' => 'Asia/Ho_Chi_Minh',
//            'currencyCode' => 'EUR',
        ],
        'image' => [
            'class' => 'app\components\images\ImageUpload',
        ],
//        'session' => [
//            'name' => 'LTHZSESSID',
//            'cookieParams' => [
//                'path' => '/',
//                'domain' => '.lincolnmgt.com'
//            ],
//        ],
    ],
    'params' => $params,

];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}
$config = yii\helpers\ArrayHelper::merge(
    $config,
    require(__DIR__ . '/config-local.php')
);

return $config;

