<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 06-Oct-15
 * Time: 12:50 AM
 */
// Define constants phân quyền
defined('LINCOL_IS_CUSTOMER') or define('LINCOL_IS_CUSTOMER',1);
return [
    'id' => 'lincol-customer',
    'defaultRoute' => 'customer/default/index',
    'controllerNamespace' => 'app\modules\customer\controllers',
    'viewPath' => '@app/modules/customer/views',
    'components' => [
        'session' => [
            'name' => 'HZPSESSINFO'
        ],
        'user' => [
            'class' => '\yii\web\User',
            'identityClass' => 'app\models\Customer',
            'enableAutoLogin' => false,
            'loginUrl' => ['/customer/default/login'],
            'idParam' => '__customer',
            'authTimeoutParam' => '__customerExpire',
            'absoluteAuthTimeoutParam' => '__customerAbsoluteExpire',
            'returnUrlParam' => '__customerReturnUrl',
        ],
        'urlManager' => [
//            'class' => 'yii\web\urlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                '/'=>'customer/default/index',
                'login'=>'customer/default/login',
                'logout'=>'customer/default/logout',
                'register'=>'customer/my-account/register',
                'info'=>'customer/my-account/index',
                'update'=>'customer/my-account/update',
                'change-password'=>'customer/my-account/change-password',
                'create-cashout'=>'customer/my-account/require-cash-out',
                'cashout'=>'customer/my-account/cash-out',
                'transaction'=>'customer/my-transaction/index',
                'detail-transaction'=>'customer/my-transaction/view',

            ],
        ],
        'errorHandler' => [
            'errorAction' => 'customer/default/error',
        ],

    ],

    'params' => [],
];