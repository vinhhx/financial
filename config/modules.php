<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 30-Sep-15
 * Time: 12:51 AM
 */
return [
    //module admin
    'admin' => [
        'class' => '\app\modules\admin\AdminModule',
        // ... other configurations for the module ...
    ],
    'customer' => [
        'class' => '\app\modules\customer\Module',
        // ... other configurations for the module ...
    ]
];