<?php

$params = [
    'HOTLINE' => '09X XXXX XXX',
    'adminEmail' => 'admin@bitoness.com',
    'emailSend' => 'admin@bitoness.com',
    'customer.passwordResetTokenExpire' => '86400',
    'languages' => [
        'en' => 'English',
        'vi' => 'Vietnamese'
    ],

    'packageLimit' => 10,
    'imageResizes' => [
        '100x100',
    ],
    'CDNServer' => '/uploads/',
    'TimeRequestFirst' => 86400, //1 ngày
    'TimeRequestLast' => 172800, //2 ngày
    'TimeRequestLimit' => 86400, //24h
    'TimeCompletePackage' => 10800, //3h

    'HH_TT_F1' => 10,
    'HH_TT_F2' => 3,
    'HH_TT_F3' => 2,
    'HH_TT_F4' => 1,
    'HH_TT_F5' => 1,

    'HH_GT_F1' => 15,
    'HH_GT_F2' => 3,
    'HH_GT_F3' => 2,
    'HH_GT_F4' => 1,
    'HH_GT_F5' => 1,

    'tokenAmount' => 0.03,
    'min_cash_out_amount' => 0.3,
    'max_cash_out_amount' => 1.5,
    'HH_GT' => 16,
    'TOKEN_PRICE' => 15,
    'mapTransaction' => 'off',
//    'system_bit_coin_address' => '3GY6vfoUijwUpEtDQ4GmbeaG6dcMewFd8j',
    'system_bit_coin_address' => '3CBxSj3tHCWBJWybCSreLNTzWD2gr6VzC6',
    'invest_bit_coin_address' => '3EaDiQMwythPzsvVyDFXHcM3n9mAri5uTG',
    'join_fee' => 0.03,

    'unit' => 'BTC',
    'weekBonus' => 4,
    'CashOutBonus' => 35,
    'pageRefresh' => 60,
    'feeProfit' => 2.5,
    'feePoundage' => 5,
    'DayCashOut' => ["Monday"],
    'InvestCashOutPoundageTime' => 1209600, //2 tuần
    'InvestCashOutBonus' => 2419200, //4 tuần
    'bg-color-index' => [
        1 => 'bg-gray',
        2 => 'bg-blue',
        3 => 'bg-green',
        4 => 'bg-orange',
        5 => 'bg-maroon',
        6 => 'bg-danger',
    ],
    'INVEST_OPEN_CREATE_PACKAGE' => true, //true : cho mở gới false: không cho mở gói
    'CUSTOMER_OPEN_CREATE_PACKAGE' => true, //true : cho mở gới false: không cho mở gói
    'POUNDAGE_TIME_DELAY_CASH_OUT' => 10800, //3h
    'TIME_REMAINING_TYPE' => 'm',
    'HIDDEN_ADMIN' => 'hideadmin',
    'IMAGE_VERSION' => '1.4.0',
    'VERSION_CSS' => '1.4.2',
    'VERSION_JS' => '1.4.1',
    'GOOGLE_CAPTCHA_SITE_KEY' => '6Lc40QYUAAAAAIpdsgxXd5ZCDqDhVClOjEUh9vPd',
    'GOOGLE_CAPTCHA_SECRET_KEY' => '6Lc40QYUAAAAAFt9MoaRSoouT49sTU1tG1eFXvxS',
    'CustomerPoundageCashOutFee' => 2.5,
];

return yii\helpers\ArrayHelper::merge(
    $params, require(__DIR__ . '/params-local.php')
);
