<?php

namespace app\models;

use app\models\logs\AdminLogAction;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customer_poundage_transaction".
 *
 * @property integer $id
 * @property integer $bill_id
 * @property integer $sign
 * @property string $customer_id
 * @property string $customer_username
 * @property integer $from_id
 * @property string $from_username
 * @property string $amount
 * @property integer $balance_before
 * @property integer $balance_after
 * @property integer $level
 * @property integer $type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class CustomerPoundageTransaction extends \yii\db\ActiveRecord
{
    const  F1 = 1;
    const  F2 = 2;
    const  F3 = 3;
    const  F4 = 4;
    const  F5 = 5;
    const  F6 = 6;
    const  F7 = 7;
    const  F8 = 8;
    const  F9 = 9;
    const  F10 = 10;

    const TYPE_POUNDAGE_FIRST = 1; //hoa hồng tạo gói đầu tư
    const TYPE_POUNDAGE_SECOND = 2;// hoa hồng tái dầu tư
    const TYPE_POUNDAGE_SYSTEM = 3;// hoa hồng hệ thống thêm
    const TYPE_CASH_OUT_POUND = 10;// rút tiền hoa hồng
    const TYPE_POUNDAGE_COMPENSATE = 20;//Hệ thống + thêm hoa hồng

    const SIGN_ADD = 1;
    const SIGN_SUB = 2;

    const STATUS_COMPLETED = 1;
    const STATUS_HOLDING = 10;
    const STATUS_REFUND = 99;
    const STATUS_PENDING = 2;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_poundage_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bill_id', 'sign', 'customer_id', 'from_id', 'level', 'type', 'created_at', 'updated_at'], 'integer'],
            [['customer_id', 'customer_username'], 'required'],
            [['amount', 'balance_before', 'balance_after'], 'number'],
            [['customer_username', 'from_username'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'bill_id' => Yii::t('customers', 'Mã phiếu '),
            'sign' => Yii::t('customers', 'Dấu'),
            'customer_id' => Yii::t('customers', 'Mã người chơi'),
            'customer_username' => Yii::t('customers', 'Người chơi'),
            'from_id' => Yii::t('customers', 'Từ mã người chơi'),
            'from_username' => Yii::t('customers', 'Từ người chơi'),
            'amount' => Yii::t('customers', 'Số BTC'),
            'balance_before' => Yii::t('customers', 'Trước giao dịch'),
            'balance_after' => Yii::t('customers', 'Sau giao dịch'),
            'level' => Yii::t('customers', 'Cấp độ'),
            'type' => Yii::t('customers', 'Loại'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getWallet()
    {
        return $this->hasOne(CustomerWallet::className(), ['customer_id' => 'customer_id']);
    }

    public static function getStatusLabels()
    {
        return [
            self::STATUS_COMPLETED => Yii::t('customers', 'Completed'),
            self::STATUS_HOLDING => Yii::t('customers', 'Processing'),
            self::STATUS_REFUND => Yii::t('customers', 'Hoàn trả'),
            self::STATUS_PENDING => Yii::t('customers', 'Pending'),
        ];
    }

    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        if (isset($labels[$this->status])) {
            return $labels[$this->status];
        }
        return '';
    }

    public static function getTypeLabels()
    {
        return [
            self::TYPE_CASH_OUT_POUND => Yii::t('customers', 'PH Request Widthdrawn'),
            self::TYPE_POUNDAGE_FIRST => Yii::t('customers', 'Direct Commission'),
            self::TYPE_POUNDAGE_SECOND => Yii::t('customers', 'Reinvestment Commission'),
            self::TYPE_POUNDAGE_SYSTEM => 'System Bonus Commission',
            self::TYPE_POUNDAGE_COMPENSATE => 'System Compensatory',
        ];
    }

    public static function getSignLabels()
    {
        return [
            self::SIGN_ADD => Yii::t('customers', 'ADD(+)'),
            self::SIGN_SUB => Yii::t('customers', 'SUB(-)'),

        ];
    }

    public function getTypeLabel()
    {
        $labels = static::getTypeLabels();
        if (isset($labels[$this->type])) {
            return $labels[$this->type];
        }
        return '';
    }


    public function approvedCompensate()
    {
        if ($this->type !== self::TYPE_POUNDAGE_COMPENSATE && $this->status !== self::STATUS_PENDING) {
            return false;
        }
        $customer = Customer::findOne($this->customer_id);
        if (!$customer || !$customer->isProcess()) {
            return false;
        }
        $wallet = $customer->findWallet();
        //Xử lý
        if ($this->sign == self::SIGN_SUB) {
            if ($this->amount > $wallet->poundage) {
                return false;
            }
        }
        $beforeBalance = $wallet->poundage;
        $this->balance_before = $beforeBalance;
        switch ($this->sign) {
            case self::SIGN_ADD:
                $this->balance_after = $beforeBalance + $this->amount;
                $this->status = self::STATUS_COMPLETED;
                break;
            case self::SIGN_SUB:
                $this->balance_after = $beforeBalance - $this->amount;
                $this->status = self::STATUS_COMPLETED;
                break;
            default:
                break;
        }
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($this->save(true, ['balance_before', 'balance_after', 'status', 'updated_at'])) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $this->attributes);
                $wallet->poundage = $this->balance_after;
                if ($wallet->save(true, ['poundage', 'updated_at'])) {
                    AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_UPDATE, $wallet->attributes);
                    $transaction->commit();
                    return true;
                }
            }
        } catch (Exception $ex) {
            $transaction->rollBack();
            return false;
        }
        return false;
    }
}
