<?php

namespace app\models;

use app\components\helpers\StringHelper;
use app\components\helpers\UploadHelpers;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property string $url
 * @property integer $type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class Banner extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const TYPE_SLIDE_HOME = 1;

    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['title', 'image', 'url'], 'string', 'max' => 255],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','Mã'),
            'title' => Yii::t('app','Tiêu đề'),
            'image' => Yii::t('app','File ảnh'),
            'file' => Yii::t('app', 'File ảnh'),
            'url' => Yii::t('app','Đường dẫn'),
            'type' =>Yii::t('app','Loại'),
            'status' => Yii::t('app','Trạng thái'),
            'created_at' => Yii::t('app','Tạo lúc'),
            'updated_at' => Yii::t('app','Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    public function beforeSave($insert)
    {
        // Upload file
        $this->upload();

        return parent::beforeSave($insert);
    }


    public function upload()
    {
        $this->file = UploadedFile::getInstance($this, 'file');

        if ($this->file && $this->validate(['file'])) {

            $fileName = md5(uniqid() . $this->file->name) . '.' . $this->file->extension;

            // Uploaded dir
            $folderId = strtotime(date('Y-m-01 00:00:00'));

            $uploadDir = UploadHelpers::getUploadDirById($folderId, 'banner');

            // Create folder upload
            BaseFileHelper::createDirectory($uploadDir);

            if ($this->file->saveAs($uploadDir . $fileName)) {
                $this->image = UploadHelpers::getImageById($folderId, $fileName, 'banner');
                $this->file = null;
            }
        }
    }

    public function getImage()
    {
        return $this->image;
    }

    public static function getStatusLabels(){
        return[
            self::STATUS_ACTIVE => Yii::t('app','Đã kích hoạt'),
            self::STATUS_INACTIVE => Yii::t('app','Đã tạm dừng'),
        ];
    }

    public static function getTypeLabels(){
        return[
            self::TYPE_SLIDE_HOME => Yii::t('app','Slide trang chủ'),
        ];
    }
}
