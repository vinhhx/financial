<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "re_calculate_poundage_queue".
 *
 * @property integer $id
 * @property string $params
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class ReCalculatePoundageQueue extends \yii\db\ActiveRecord
{
    const  STATUS_PENDING = 1;
    const STATUS_COMPLETED = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 're_calculate_poundage_queue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['params'], 'string'],
            [['status', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'params' => Yii::t('customers', 'Tham số'),
            'status' => Yii::t('customers', 'Trạng thái'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
}
