<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "email".
 *
 * @property integer $id
 * @property string $to_email
 * @property string $subject
 * @property string $template
 * @property string $params
 * @property integer $result_code
 * @property string $result_message
 * @property integer $type
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Email extends \yii\db\ActiveRecord
{
    const STATUS_PENDING = 1;
    const STATUS_SENT_COMPLETE = 2;
    const STATUS_SENT_ERROR = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['params'], 'string'],
            [['result_code', 'type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['to_email', 'subject', 'template', 'result_message'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'to_email' => Yii::t('customers', 'Email nhận'),
            'subject' => Yii::t('customers', 'Tiêu đề'),
            'template' => Yii::t('customers', 'Giao diện gửi email'),
            'params' => Yii::t('customers', 'Tham số'),
            'result_code' => Yii::t('customers', 'Mã kết quả gửi'),
            'result_message' => Yii::t('customers', 'Nội dung kết quả gửi'),
            'type' => Yii::t('customers', 'Loại'),
            'status' => Yii::t('customers', 'Trạng thái'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
}
