<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;

/**
 * This is the model class for table "customer_package".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $package_id
 * @property integer $status
 * @property integer $is_active
 * @property integer $type
 * @property integer $time_sent_completed
 * @property string $created_at
 * @property string $updated_at
 * @property string $package_amount
 * @property string $increase_amount
 * @property string $total
 * @property string $balance
 * @property integer $is_reinvestment
 * @property string $reinvestment_date
 * @property string $block_chain_wallet_id
 * @property string $package_name
 * @property string $package_amount_root
 *
 */
class CustomerPackage extends \yii\db\ActiveRecord
{
    const STATUS_REQUEST_IN_STEP_ONE = 1; //Trạng thái nộp tiền hoàn thành lần 1
    const STATUS_REQUEST_IN_STEP_TWO = 2; //Trạng thái nộp tiền hoàn thành lần 2
    const STATUS_RUNNING = 10; //Trạng thái dang chạy
    const STATUS_EMPTY = 20;//Trạng thái trống đáo hạn 1 vòng

    const STATUS_REQUEST_OUT_STEP_ONE = 20;
    const STATUS_REQUEST_OUT_STEP_TWO = 21;

    const  STATUS_PENDING_REINVESTMENT = 30;

    const  PACKAGE_IS_REINVESTMENT = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'package_id'], 'required'],
            [['package_amount', 'increase_amount', 'total', 'balance', 'package_amount_root'], 'number'],
            [['customer_id', 'package_id', 'status', 'is_active', 'type', 'created_at', 'updated_at', 'is_reinvestment', 'reinvestment_date', 'time_sent_completed'], 'integer'],
            [['package_name', 'block_chain_wallet_id'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'customer_id' => Yii::t('customers', 'Mã người chơi'),
            'package_id' => Yii::t('customers', 'Mã gói đầu tư'),
            'status' => Yii::t('customers', 'Trạng thái'),
            'is_active' => Yii::t('customers', 'Trạng thái kích hoạt'),
            'type' => Yii::t('customers', 'Loại'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
            'block_chain_wallet_id' => Yii::t('customers', 'Địa chỉ ngân hàng thụ hưởng'),
            'package_amount_root' => Yii::t('customers', 'Package Amount Root'),

        ];
    }

    public function getPackage()
    {
        return $this->hasOne(Package::className(), ['id' => 'package_id']);
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function requestOpenPackage($amount = 0)
    {
        $customer = Customer::findOne(['id' => $this->customer_id]);
        $customerRequest = new CustomerRequest();
        $customerRequest->customer_id = $this->customer_id;
        $customerRequest->type = CustomerRequest::TYPE_PAY_WHEN_OPEN_PACKAGE;
        $customerRequest->package_id = $this->id;
        $customerRequest->status = CustomerRequest::STATUS_CREATED;
        $customerRequest->amount = $this->package_amount;
        $customerRequest->amount_exactly = $this->package_amount;
        $customerRequest->block_chain_wallet_id = $this->block_chain_wallet_id;
        try {
            if ($customerRequest->save()) {
                $amount1 = round(($customerRequest->amount / 2), 8);
                $amount2 = $customerRequest->amount - $amount1;
                $minAmount = 0;
                $maxAmount = 0;
                if (!$amount) {
                    $minAmount = round(($customerRequest->amount / 100) * 20, 8);
                    $maxAmount = round(($customerRequest->amount / 100) * 80, 8);
//                    $amount=$amount1;
                    $amount2 = $customerRequest->amount - $amount1;
                }

                //Thêm vào queue 2 bản ghi chờ giao dịch
                //bản ghi yêu cầu chuyển tiề lần t1
                $timeNext = (int)rand(Yii::$app->params["TimeRequestLast"], Yii::$app->params["TimeRequestLimit"]) + $customerRequest->created_at;
                $data = [];
                $data [] = [
                    $customerRequest->customer_id,
                    $customer->username,
                    $customerRequest->id,
                    $minAmount,
                    $maxAmount,
                    CustomerTransaction::TYPE_OPEN_PACKAGE,
                    CustomerTransaction::TYPE_STEP_OPEN_PACKAGE_1,
                    CustomerTransactionQueue::STATUS_PENDING,
                    $customerRequest->created_at,
                    $amount1,
                    time(),
                    time(),
                ];
                $data[] = [
                    $customerRequest->customer_id,
                    $customer->username,
                    $customerRequest->id,
                    0,
                    0,
                    CustomerTransaction::TYPE_OPEN_PACKAGE,
                    CustomerTransaction::TYPE_STEP_OPEN_PACKAGE_2,
                    CustomerTransactionQueue::STATUS_REQUEST_COMPLETED_FIRST,
                    $timeNext,
                    $amount2,
                    time(),
                    time(),
                ];

                Yii::$app->db->createCommand()->batchInsert(CustomerTransactionQueue::tableName(), [
                    'customer_id',
                    'customer_username',
                    'customer_request_id',
                    'min_amount',
                    'max_amount',
                    'type',
                    'type_step',
                    'status',
                    'start_at',
                    'amount',
                    'created_at',
                    'updated_at'
                ], $data)->execute();
                $activityMessage = 'customer đã đăng ký gói đầu tư ' . $this->package_name . ' trị giá ' . $this->package_amount . ' bitcoin';
                CustomerActivity::customerCreate(CustomerActivity::TYPE_MO_GOI_DAU_TU, $activityMessage, $this->getAttributes());
                LogAction::logUser($this->id, $this->tableName(), LogAction::TYPE_CUSTOMER_CREATE_PACKAGE, $this->getAttributes());
                return [
                    "status" => 'success',
                    "error_code" => 0,
                    "message" => ''
                ];
            }

        } catch (\Exception $ex) {
            $customerRequest->delete();
            return ["status" => 'error',
                "error_code" => $ex->getCode(),
                "message" => $ex->getMessage()];
        }

    }


    public function requireCashOut()
    {
        $result = [
            "status" => "error",
            "message" => [],
        ];
        if ($this->balance <= 0) {
            $result["status"] = "error";
            $result["message"][] = "Package balance not enought to cashout";
        }
        if ($this->status !== self::STATUS_EMPTY) {
            //Nếu gói đang hoạt động hoặc đang cho thì trạng thái này ko đc rút
            if ($this->is_reinvestment != self::PACKAGE_IS_REINVESTMENT) {
                $result["status"] = "error";
                $result["message"][] = "Package status is PH, please completed step to cash out";
            }
        }

        $timeCreated = ($this->is_reinvestment) ? $this->reinvestment_date : $this->created_at;
//        $timeExisted = time() - $timeCreated;
        $timeCondition = time() - $this->time_sent_completed;
        if ($timeCondition > Yii::$app->params["TimeCompletePackage"]) {
            //Sau 7 ngày và 48h chuyển lần thứ 2
            //Nếu gói đã tồn tại đc 1 ngày và thời gian hoàn thành lớn hơn thời gian giới hạn
            if (empty($result["message"])) {
                $result["status"] = "success";
                $result["message"] = [];
            }
        } else {
            $result["status"] = "error";
            $result["message"] = ['Your package had not qualified to cash out'];
        }
        return $result;
    }

    public
    function canCashOut()
    {
        $result = $this->requireCashOut();
        if ($result["status"] == "success") {
            return true;
        }

        return false;
    }

    public function conditionPH()
    {
        if ($this->status != self::STATUS_EMPTY) {
            return false;
        }
        return true;
    }

    public function canReinvestment()
    {
        if ($this->status == self::STATUS_EMPTY) {
            return true;
        }
        return false;
    }

    public
    static function getPackageLabels($status = '')
    {
        $labels = [
            self::STATUS_EMPTY => Yii::t('customers', 'Gói đầu tư đang trống'),
            self::STATUS_RUNNING => Yii::t('customers', 'Hoàn thành chuyển tiền'),
            self::STATUS_REQUEST_IN_STEP_ONE => Yii::t('customers', 'Yêu cầu chuyển tiền lần đầu'),
            self::STATUS_REQUEST_IN_STEP_TWO => Yii::t('customers', 'Yêu cầu chuyển tiền lần cuối'),

        ];
        if ($status) {
            if (isset($labels[$status])) {
                return $labels[$status];
            } else {
                return '';
            }
        }
        return $labels;
    }

    public function caculatePoundage()
    {
        //Lấy danh sách tài khoản cho đến level 5
//        $customers=new Query()

        $customerIds = CustomerCase::find()
            ->where([
                'customer_id' => $this->customer_id,
                'level' => [1, 2, 3, 4, 5]
            ])->all();
        $params = [];
        if ($customerIds) {
            //Nếu là gói tái đầu tư
            $packageAll = Package::find()->where(['status' => Package::STATUS_ACTIVE])->indexBy('id')->all();
            foreach ($customerIds as $customer) {
                $customers = Customer::find()->where(['id' => [$this->customer_id, $customer->parent_id]])->indexBy('id')->all();
                $customerCurrent = $customers[$this->customer_id];
                $packageCurrent = isset($packageAll[$this->package_id]) ? $packageAll[$this->package_id] : null;
                $customerParent = $customers[$customer->parent_id];
//                $type = ($this->is_reinvestment) ? CustomerPoundageTransaction::TYPE_POUNDAGE_SECOND : CustomerPoundageTransaction::TYPE_POUNDAGE_FIRST;
                $config = $packageCurrent->getConfigDefault();
                switch ($customer->level) {
                    case CustomerPoundageTransaction::F1:
                        //F1
                        $percent = 0;
                        if ($this->is_reinvestment) {
                            $percent = (isset($config["reinvestmentBonus"]) && isset($config["reinvestmentBonus"]["F1"])) ? (int)$config["reinvestmentBonus"]["F1"] : 0;
                        } else {
                            $percent = (isset($config["investBonus"]) && isset($config["investBonus"]["F1"])) ? (int)$config["investBonus"]["F1"] : 0;
                        }
                        $amount = round((($this->package_amount / 100) * $percent), 8); //Lam tron 4 so
                        $level = CustomerPoundageTransaction::F1;
                        break;
                    case CustomerPoundageTransaction::F2:
                        //F1
                        $percent = 0;
                        if ($this->is_reinvestment) {
                            $percent = (isset($config["reinvestmentBonus"]) && isset($config["reinvestmentBonus"]["F2"])) ? (int)$config["reinvestmentBonus"]["F2"] : 0;
                        } else {
                            $percent = (isset($config["investBonus"]) && isset($config["investBonus"]["F2"])) ? (int)$config["investBonus"]["F2"] : 0;
                        }
                        $amount = round((($this->package_amount / 100) * $percent), 8); //Lam tron 4 so
                        $level = CustomerPoundageTransaction::F2;
                        break;
                    case CustomerPoundageTransaction::F3:
                        //F1
                        $percent = 0;
                        if ($this->is_reinvestment) {
                            $percent = (isset($config["reinvestmentBonus"]) && isset($config["reinvestmentBonus"]["F3"])) ? (int)$config["reinvestmentBonus"]["F3"] : 0;
                        } else {
                            $percent = (isset($config["investBonus"]) && isset($config["investBonus"]["F3"])) ? (int)$config["investBonus"]["F3"] : 0;
                        }
                        $amount = round((($this->package_amount / 100) * $percent), 8); //Lam tron 4 so
                        $level = CustomerPoundageTransaction::F3;
                        break;
                    case CustomerPoundageTransaction::F4:
                        //F1
                        $percent = 0;
                        if ($this->is_reinvestment) {
                            $percent = (isset($config["reinvestmentBonus"]) && isset($config["reinvestmentBonus"]["F4"])) ? (int)$config["reinvestmentBonus"]["F4"] : 0;
                        } else {
                            $percent = (isset($config["investBonus"]) && isset($config["investBonus"]["F4"])) ? (int)$config["investBonus"]["F4"] : 0;
                        }
                        $amount = round((($this->package_amount / 100) * $percent), 8); //Lam tron 4 so
                        $level = CustomerPoundageTransaction::F4;
                        break;
                    case CustomerPoundageTransaction::F5:
                        //F1
                        $percent = 0;
                        if ($this->is_reinvestment) {
                            $percent = (isset($config["reinvestmentBonus"]) && isset($config["reinvestmentBonus"]["F5"])) ? (int)$config["reinvestmentBonus"]["F5"] : 0;
                        } else {
                            $percent = (isset($config["investBonus"]) && isset($config["investBonus"]["F5"])) ? (int)$config["investBonus"]["F5"] : 0;
                        }
                        $amount = round((($this->package_amount / 100) * $percent), 8); //Lam tron 4 so
                        $level = CustomerPoundageTransaction::F5;
                        break;
                    default:
                        break;
                }
                $customerParentWallet = $customerParent->getCurrentWallet();
                $balance_before = $customerParentWallet->poundage;
                //Add vào ví
                if ($amount > 0) {
                    if ($customerParentWallet->add($amount)) {
                        //Tạo  transaction
                        $customerPoundage = new CustomerPoundageTransaction();
                        $customerPoundage->amount = $amount;
                        $customerPoundage->balance_before = ($balance_before) ? $balance_before : 0;
                        $customerPoundage->balance_after = $balance_before + $amount;
                        $customerPoundage->level = $level;
                        $customerPoundage->type = ($this->is_reinvestment) ? CustomerPoundageTransaction::TYPE_POUNDAGE_SECOND : CustomerPoundageTransaction::TYPE_POUNDAGE_FIRST;
                        $customerPoundage->sign = CustomerPoundageTransaction::SIGN_ADD;
                        $customerPoundage->customer_id = $customerParent->id;
                        $customerPoundage->customer_username = $customerParent->username;
                        $customerPoundage->from_id = $customerCurrent->id;
                        $customerPoundage->from_username = $customerCurrent->username;
                        if ($customerPoundage->save()) {
                            $params[] = $customerParent->id;
                        } else {
                        }
                        unset($customerPoundage);
                    }
                }
                unset($customerParent);
                unset($customerParentWallet);

            }
            if (!empty($params)) {
                $poundageQueue = new ReCalculatePoundageQueue();
                $poundageQueue->status = ReCalculatePoundageQueue::STATUS_PENDING;
                $poundageQueue->params = json_encode($params);
                $poundageQueue->save();
            }
        }

    }

}
