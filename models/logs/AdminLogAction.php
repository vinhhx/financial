<?php

namespace app\models\logs;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\MethodNotAllowedHttpException;
use yii\web\NotAcceptableHttpException;

/**
 * This is the model class for table "admin_log_action".
 *
 * @property integer $id
 * @property string $user_id
 * @property string $user_name
 * @property string $ip_access
 * @property string $controller_id
 * @property string $controller_action_id
 * @property integer $action_type
 * @property string $action_content
 * @property string $created_at
 * @property string $updated_at
 */
class AdminLogAction extends \yii\db\ActiveRecord
{
    const ACTION_TYPE_CREATE = 1;
    const ACTION_TYPE_UPDATE = 2;
    const ACTION_TYPE_DELETE = 3;
    const ACTION_EXPORT_EXCEL = 4;

    const ACTION_CUSTOMER_TRANSACTION_SENT = 10;
    const ACTION_CUSTOMER_TRANSACTION_REMOVE_ATTACHMENT = 11;
    const ACTION_CUSTOMER_TRANSACTION_RECEIVE = 12;
    const ACTION_CUSTOMER_TRANSACTION_UPLOAD_ATTACHMENT = 13;
    const ACTION_CUSTOMER_TRANSACTION_APPROVED = 14;

    const ACTION_CUSTOMER_TRANSACTION_QUEUE_ADD_RECEIVER = 20;
    const ACTION_CUSTOMER_TRANSACTION_QUEUE_ADD_SENDER = 21;

    const ACTION_CUSTOMER_REQUEST_APPROVED = 30;
    const ACTION_PERMISSION_CREATE = 40;

    const ACTION_TYPE_CHANGE_CASE = 50;
    const ACTION_TYPE_CHANGE_BANK_CUSTOMER = 60;
    const ACTION_TYPE_CHANGE_BANK_INVEST = 61;


    const ACTION_TYPE_ERROR = 99;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'admin_log_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'user_name', 'ip_access', 'controller_id', 'controller_action_id', 'action_type'], 'required'],
            [['user_id', 'action_type', 'created_at', 'updated_at'], 'integer'],
            [['action_content'], 'string'],
            [['user_name', 'ip_access', 'controller_id', 'controller_action_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'user_name' => Yii::t('app', 'User Name'),
            'ip_access' => Yii::t('app', 'Ip Access'),
            'controller_id' => Yii::t('app', 'Controller ID'),
            'controller_action_id' => Yii::t('app', 'Controller Action ID'),
            'action_type' => Yii::t('app', 'Action Type'),
            'action_content' => Yii::t('app', 'Action Content'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function createLogAction($actionType, $actionContent)
    {
        if (Yii::$app->user->isGuest) {
            throw new NotAcceptableHttpException('You not permission access this action');
        }
        $logs = new AdminLogAction();
        $logs->user_id = Yii::$app->user->identity->id;
        $logs->user_name = Yii::$app->user->identity->username;
        $logs->ip_access = Yii::$app->request->userIP;
        $logs->controller_id = Yii::$app->controller->id;
        $logs->controller_action_id = Yii::$app->controller->action->id;
        $logs->action_type = $actionType;
        $logs->action_content = json_encode($actionContent);
        if ($logs->save()) {
            return true;
        }
        throw new MethodNotAllowedHttpException('This action Not create log');

    }

    public static function getActionTypeLabels($type = '')
    {
        $actionTypes = [
            self::ACTION_TYPE_CREATE => 'Tạo mới',
            self::ACTION_TYPE_UPDATE => 'Cập nhật',
            self::ACTION_TYPE_DELETE => 'Xóa',
            self::ACTION_TYPE_ERROR => 'Lỗi',
            self::ACTION_EXPORT_EXCEL => 'Xuất Excel',
            self::ACTION_PERMISSION_CREATE => 'Tạo quyền',
            self::ACTION_CUSTOMER_REQUEST_APPROVED => 'Xác nhận yêu cầu',
            self::ACTION_CUSTOMER_TRANSACTION_QUEUE_ADD_SENDER => 'Map tài khoản chuyển',
            self::ACTION_CUSTOMER_TRANSACTION_QUEUE_ADD_RECEIVER => 'Map tài khoản nhận',
            self::ACTION_CUSTOMER_TRANSACTION_SENT => 'Giao dịch chuyển',
            self::ACTION_CUSTOMER_TRANSACTION_RECEIVE => 'Giao dịch nhận',
            self::ACTION_CUSTOMER_TRANSACTION_UPLOAD_ATTACHMENT => 'Upload attachment',
            self::ACTION_CUSTOMER_TRANSACTION_REMOVE_ATTACHMENT => 'Remove attachment',
            self::ACTION_CUSTOMER_TRANSACTION_APPROVED => 'Xác nhận giao dịch',
            self::ACTION_TYPE_CHANGE_BANK_CUSTOMER => 'Đổi địa chỉ ví customer',
            self::ACTION_TYPE_CHANGE_BANK_INVEST => 'Đổi địa chỉ ví Invest customer',
            self::ACTION_TYPE_CHANGE_CASE => 'Đổi vị trí thành viên case invest',
        ];
        if ($type) {
            $typeLabel = isset($actionTypes[$type]) ? $actionTypes[$type] : '----';
            return $typeLabel;
        }
        return $actionTypes;

    }

}
