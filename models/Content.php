<?php

namespace app\models;

use app\components\helpers\StringHelper;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "content".
 *
 * @property integer $id
 * @property integer $type_id
 * @property string $name
 * @property string $alias
 * @property string $content
 * @property integer $status
 * @property integer $deleted
 * @property string $config
 * @property integer $created_at
 * @property integer $updated_at
 */
class Content extends \yii\db\ActiveRecord
{
    const TYPE_ID_WHY_CHOICE_US = 1;
    const TYPE_ID_INTRO = 2;
    const TYPE_ID_TESTIMONIAL = 3;
    const TYPE_ID_FAQ = 4;
    const TYPE_ID_ABOUT_US = 5;
    const TYPE_ID_HELPFUL_LINK = 6;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type_id', 'name'], 'required'],
            [['type_id', 'status', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['content', 'config'], 'string'],
            [['name', 'alias'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Mã'),
            'type_id' => Yii::t('app', 'Mã loại'),
            'name' => Yii::t('app', 'Tên'),
            'alias' => Yii::t('app', 'Tên gọi khác'),
            'content' => Yii::t('app', 'Nội dung'),
            'status' => Yii::t('app', 'Trạng thái'),
            'deleted' => Yii::t('app', 'Đã xóa'),
            'config' => Yii::t('app', 'Cài đặt'),
            'created_at' => Yii::t('app', 'Tạo lúc'),
            'updated_at' => Yii::t('app', 'Cập nhật lúc'),
        ];
    }

    public function getTypeLabels()
    {
        return [
            self::TYPE_ID_WHY_CHOICE_US => 'Block Why choice us',
            self::TYPE_ID_INTRO => 'Block Intro',
            self::TYPE_ID_TESTIMONIAL => 'Block Testimonial',
            self::TYPE_ID_FAQ => 'Block FAQ',
            self::TYPE_ID_ABOUT_US => 'Block About us',
            self::TYPE_ID_HELPFUL_LINK => 'Block Helpful links'
        ];
    }

    public function beforeSave($insert)
    {
        $this->alias = StringHelper::rewrite($this->name);

        return parent::beforeSave($insert);
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }
}
