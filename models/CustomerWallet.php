<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customer_wallet".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $balance
 * @property string $poundage
 * @property integer $type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class CustomerWallet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_wallet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id', 'type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['balance', 'poundage'], 'number'],
            [['customer_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'customer_id' => Yii::t('customers', 'Mã người chơi'),
            'balance' => Yii::t('customers', 'Số dư'),
            'poundage' => Yii::t('customers', 'Hoa hồng'),
            'type' => Yii::t('customers', 'Loại'),
            'status' => Yii::t('customers', 'Trạng thái'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function getWallet($customer_id)
    {
        $wallet = static::findOne(['customer_id' => $customer_id]);
        if (!$wallet) {
            $wallet = new CustomerWallet();
            $wallet->customer_id = $customer_id;
            $wallet->save();
        }
        return $wallet;
    }

    public function add($amount)
    {
        $this->balance += $amount;
        $this->poundage += $amount;
        return $this->save(true, ['balance', 'poundage', 'updated_at']);
    }
}
