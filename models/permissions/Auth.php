<?php

namespace app\models\permissions;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "auth".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $source
 * @property string $source_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class Auth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'source', 'source_id'], 'required'],
            [['user_id'], 'integer'],
            [['source', 'source_id'], 'string', 'max' => 255]
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('systems','Mã'),
            'user_id' => Yii::t('systems','Mã quản trị viên'),
            'source' => Yii::t('systems','Tài nguyên'),
            'source_id' => Yii::t('systems','Mã Tài nguyên'),
            'created_at' => Yii::t('systems','Tạo lúc'),
            'updated_at' => Yii::t('systems','Cập nhật lúc'),
        ];
    }

}
