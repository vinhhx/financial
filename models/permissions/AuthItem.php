<?php

namespace app\models\permissions;

use Yii;
use yii\base\Exception;

/**
 * This is the model class for table "auth_item".
 *
 * @property string $name
 * @property integer $type
 * @property string $description
 * @property string $rule_name
 * @property string $data
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthRule $ruleName
 * @property AuthItemChild[] $authItemChildren
 * @property AuthItemChild[] $authItemChildren0
 */
class AuthItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('systems','Tên'),
            'type' => Yii::t('systems','Loại'),
            'description' =>Yii::t('systems','Mô tả'),
            'rule_name' =>Yii::t('systems','Tên qui tắc'),
            'data' => Yii::t('systems','Dữ liệu'),
            'created_at' => Yii::t('systems','Tạo lúc'),
            'updated_at' => Yii::t('systems','Cập nhật lúc'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::className(), ['item_name' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(AuthRule::className(), ['name' => 'rule_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren()
    {
        return $this->hasMany(AuthItemChild::className(), ['parent' => 'name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthItemChildren0()
    {
        return $this->hasMany(AuthItemChild::className(), ['child' => 'name']);
    }

    /**
     * Scan tất cả controller và action trong thư mục được chỉ định
     * và insert vào các bảng auth dùng để phân quyền theo action
     *
     * @param string $module
     */
    public static function getAdminActions($module = '')
    {
        $adminDir = Yii::getAlias('@app/modules/admin/controllers');
        $prefixNamespace = '\app\modules\admin\controllers\\';
        if ($module) {
            $adminDir = Yii::getAlias("@app/modules/admin/modules/{$module}/controllers");
            $prefixNamespace = '\app\modules\admin\modules\\' . $module . '\controllers\\';
        }
        $auth = Yii::$app->authManager;

        // @var $controllers List tất cả controller file trong thư mục backend
        $controllers = array_diff(scandir($adminDir), ['.', '..']);

        // Tạo quyền system admin
        try{
            $root = $auth->createRole(RBAC_FULL);
            $root->description = Yii::t('systems','Quyền thực thi tất cả tác vụ của hệ thống');
            if($auth->add($root)){
                echo "Created Controller: {$root->name}\n";
            }
        }catch (Exception $ex){

        }

        if(is_array($controllers) && !empty($controllers)){
            $prefixAuthItem = ($module) ? $module . '-' : '';
            foreach($controllers as $file){
                $item = $adminDir . '/' .$file;
                require_once($item);
                $itemNameSpace = $prefixNamespace . strstr($file,'.',true);

                $itemObject = null;

                try{
                    $itemObject = new $itemNameSpace(null, null, null);
                    $controllerName = $prefixAuthItem . self::rewrite($file);
                    $parent = $auth->createPermission($controllerName);
                    $parent->description = $prefixAuthItem . self::splitStringAtUppercase($file);
                    if($auth->add($parent)){
                        echo 'Created Controller: '.$controllerName."\n";
                    }
                }catch (\Exception $ex){
                    echo $ex->getMessage();
//                    return;
                }

                // Get tất cả action có trong Object Controller
                $methods = get_class_methods($itemObject);
                if(is_array($methods) && !empty($methods)){
                    foreach($methods as $method){
                        if(strpos($method, 'action') === 0 && $method !== 'actions'){
                            try{
                                $actionName = $parent->name . '/' . self::rewrite($method);
                                $child = $auth->createPermission($actionName);
                                $child->description = $parent->description . ' ' . self::splitStringAtUppercase($method);
                                if($auth->add($child)){
                                    $auth->addChild($parent, $child);
                                    echo 'Created Action: '.$actionName."\n";
                                }
                            }catch (Exception $ex){
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $string
     * @return string
     */
    private static function splitStringAtUppercase($string)
    {
        $string = str_replace('.php', '', $string);

        $array = preg_split('/(?=[A-Z])/', $string);

        if(is_array($array) && $array[0] == ''){
            unset($array[key($array)]);
        }

        $array = array_map('ucfirst', $array);

        return implode(' ', $array);
    }
    /**
     * @param string $string
     * @return string
     */
    public static function rewrite($string)
    {
        $string = str_replace('.php', '', $string);

        // Kiểm tra và loại bỏ 'Controller' ở cuối cùng ra khỏi chuỗi
        if(preg_match('/Controller$/', $string) === 1){
            $string = preg_replace('/Controller$/', '', $string);
        }

        // Kiểm tra và loại bỏ 'action' ở đầu tiên ra khỏi chuỗi
        if(preg_match('/^action/', $string) === 1){
            $string = preg_replace('/^action/', '', $string);
        }
        $string = ucfirst($string);

        // Cắt chuỗi theo ký tự viết hoa và chuyển tất cả item trong mảng thành chữ thường
        $array = array_map('strtolower', preg_split('/(?=[A-Z])/', $string));

        if(is_array($array) && $array[0] == ''){
            unset($array[key($array)]);
        }

        return implode('-', $array);
    }

    /**
     * Data filter
     * Nếu chọn các permission cha thì loại bỏ hết các permission con
     * @param $data
     * @return mixed
     */
    public static function filter($data)
    {
        $auth = Yii::$app->authManager;
        foreach($data as $name){
            $children = $auth->getChildren($name);
            if(!empty($children)){
                foreach($children as $child){
                    if(($key = array_search($child->name, $data)) !== false) {
                        unset($data[$key]);
                    }
                }
            }
        }
        return $data;
    }

    /**
     * Lấy danh sách cách roles và permissions trên hệ thống
     * @return array
     */
    public static function getItems()
    {
        $auth = Yii::$app->authManager;

        $roles = $auth->getRoles();
        $permissions = $auth->getPermissions();

        $items = [];
        if(!empty($roles)){
            foreach($roles as $role){
                $items[$role->name] = "{$role->name} ({$role->description})";
            }
        }
        $p = [];
        if(!empty($permissions)){
            foreach($permissions as $permission){
                $ex = explode('/', $permission->name);
                $p[$ex[0]][] = $permission;
            }
            foreach($p as $n => $t){
                foreach($t as $k){
                    $prefix = $k->name == $n ? '-- --': '-- -- -- --';
                    $items[$k->name] = "{$prefix} {$k->name} ({$k->description})";
                }
            }
        }

        return $items;
    }
}
