<?php

namespace app\models\permissions;

use Yii;

/**
 * This is the model class for table "auth_assignment".
 *
 * @property string $item_name
 * @property string $user_id
 * @property integer $created_at
 *
 * @property AuthItem $itemName
 */
class AuthAssignment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'auth_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_name', 'user_id'], 'required'],
            [['created_at'], 'integer'],
            [['item_name', 'user_id'], 'string', 'max' => 64]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_name' => Yii::t('systems','Tên quyền hệ thống'),
            'user_id' =>Yii::t('systems','Mã quản trị viên'),
            'created_at' =>Yii::t('systems','Tạo lúc'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemName()
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'item_name']);
    }

    /**
     * @param $data
     * @param $userId
     * @return mixed
     */
    public function set($data, $userId)
    {
        $result = [];
        $auth = Yii::$app->authManager;

        // Remove quy?n c? c?a user
        AuthAssignment::deleteAll(['user_id' => $userId]);

        // Check n?u ???c ch?n quy?n system-admin th� b? assign c�c quy?n c�n l?i
        if(in_array(RBAC_FULL, $data) && ($role = $auth->getRole(RBAC_FULL)) !== null){
            if($auth->assign($role, $userId)){
                $result[] = RBAC_FULL;
            }
            return $result;
        }

        $data = AuthItem::filter($data);
        if(!empty($data)){
            foreach($data as $item){
                $permission = $auth->getPermission($item);
                if(!$permission){
                    $permission = $auth->getRole($item);
                }
                if($permission !== null && $auth->assign($permission, $userId)){
                    $result[] = $item;
                }
            }
        }
        return $result;
    }
}
