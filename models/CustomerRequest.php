<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customer_request".
 *
 * @property integer $id
 * @property string $customer_id
 * @property integer $type
 * @property integer $status
 * @property string $amount
 * @property string $amount_exactly
 * @property string $block_chain_wallet_id
 * @property integer $created_at
 * @property integer $package_id
 * @property integer $updated_at
 */
class CustomerRequest extends \yii\db\ActiveRecord
{
    const TYPE_PAY_WHEN_OPEN_PACKAGE = 10; // mở gói
    const TYPE_RECEIVE_WHEN_CASHOUT_PACKAGE = 20; // Rrut tien
    const TYPE_RECEIVE_WHEN_CASHOUT_POUNDAGE = 30; //rut
    const TYPE_RECEIVER_BY_SPECIAL = 40;
    const TYPE_SENT_BY_SPECIAL = 50;

    const STATUS_CREATED = 1;
    const STATUS_COMPLETE_FIRST = 10;
    const STATUS_COMPLETE_SECOND = 20;
    const STATUS_COMPLETE_THIRD = 30;
    const STATUS_COMPLETED = 99;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'type', 'status'], 'required'],
            [['customer_id', 'type', 'status', 'created_at', 'package_id', 'updated_at'], 'integer'],
            [['amount', 'amount_exactly'], 'number'],
            [['block_chain_wallet_id'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'amount' => Yii::t('app', 'Amount'),
            'created_at' => Yii::t('app', 'Created At'),
            'package_id' => Yii::t('app', 'Package ID'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'amount_exactly' => Yii::t('app', 'Amount GH || PH'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getSent()
    {
        return $this->hasMany(CustomerTransaction::className(), ['customer_sent_request_id' => 'id']);

    }

    public function getReceive()
    {
        return $this->hasMany(CustomerTransaction::className(), ['customer_receiver_request_id' => 'id']);

    }

//    public function getTransactions()
//    {
//        return $this->hasMany(CustomerTransaction::className(), ['status' => 1])->orOnCondition(['customer_receiver_request_ids' => 'id'])->orOnCondition(['customer_sent_request_id' => 'id']);
//
//    }

//    public function getTransactions()
//    {
//        return
//            CustomerTransaction::find()->orOnCondition([
//                'customer_transaction.customer_sent_request_id' => 'customer_request.ids',
//            ])->orOnCondition([
//                'customer_transaction.customer_receiver_request_id' => 'customer_request.id',
//            ]);
//    }

    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    public function getCustomerPackage()
    {
        return $this->hasOne(CustomerPackage::className(), ['id' => 'package_id']);
    }

    public static function getTypeLabels()
    {
        return [
            static::TYPE_PAY_WHEN_OPEN_PACKAGE => 'Mở gói đầu tư',
            static::TYPE_RECEIVE_WHEN_CASHOUT_PACKAGE => 'Rút tiền từ gói đầu tư',
            static::TYPE_RECEIVE_WHEN_CASHOUT_POUNDAGE => 'Rút tiền hoa hồng'
        ];
    }

    public function getTypeLabel()
    {
        $labsels = static::getTypeLabels();
        if (isset($labsels[$this->type])) {
            return $labsels[$this->type];
        }
        return '';
    }

    public static function getRequestLabel($type)
    {
        switch ($type) {
            case static::TYPE_PAY_WHEN_OPEN_PACKAGE:
            case static::TYPE_SENT_BY_SPECIAL:
                return 'PH';
                break;
            case static::TYPE_RECEIVE_WHEN_CASHOUT_PACKAGE:
            case static::TYPE_RECEIVER_BY_SPECIAL:
                return 'GH';
                break;
            case static::TYPE_RECEIVE_WHEN_CASHOUT_POUNDAGE:
                return 'GH Commission';
                break;
            default:
                return '';
                break;

        }
    }

    public static function getStatusLabels()
    {
        return [
            static::STATUS_CREATED => 'Created',
            static::STATUS_COMPLETE_FIRST => 'First step completed',
            static::STATUS_COMPLETE_SECOND => 'Last step completed',
            static::STATUS_COMPLETE_THIRD => 'Last step completed',
            static::STATUS_COMPLETED => 'Finished',
        ];
    }

    public function getStatusLabel()
    {
        $labsels = static::getStatusLabels();
        if (isset($labsels[$this->status])) {
            return $labsels[$this->status];
        }
        return '';
    }

    public function countRequestQueuePending()
    {
        return CustomerTransactionQueue::find()
            ->where([
                'customer_request_id' => $this->id,
                'status' => [
                    CustomerTransactionQueue::STATUS_PENDING,
                    CustomerTransactionQueue::STATUS_ADDED
                ]
            ])->count();
    }

    public function checkRequieCompleted()
    {
        $currentTransaction = $this->getReceive()->where(['!=', 'customer_transaction.status', CustomerTransaction::STATUS_FINISH])->count();
        if ($currentTransaction) {
            return false;
        }
        return true;
    }
}
