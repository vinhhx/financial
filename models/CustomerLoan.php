<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customer_loan".
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $amount_loan
 * @property string $amount_loan_vnd
 * @property integer $status
 * @property integer $type
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class CustomerLoan extends \yii\db\ActiveRecord
{
    const STATUS_PENDING = 1; //pending
    const STATUS_CANCEL = 0; //cancel
    const STATUS_COMPLETED = 2; //complete

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_loan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'amount_loan'], 'required'],
            [['customer_id', 'status', 'type', 'created_at', 'updated_at','created_by'], 'integer'],
            [['amount_loan', 'amount_loan_vnd'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'customer_id' => Yii::t('customers', 'Mã người chơi'),
            'amount_loan' => Yii::t('customers', 'Số BTC tạm ứngs'),
            'amount_loan_vnd' => Yii::t('customers', 'Số tiền tạm ứng'),
            'status' => Yii::t('customers', 'Trạng thái'),
            'type' => Yii::t('customers', 'Loại'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
    public function getUser(){
        return $this->hasOne(User::className(),['id'=>'created_by']);
    }
}
