<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "package".
 *
 * @property integer $id
 * @property string $package_name
 * @property string $amount
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $increase_percent
 * @property string $config
 */
class Package extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const CACHE_PACKAGE_KEY = "PACKAGE";

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['amount', 'increase_percent'], 'number'],
            [['package_name'], 'string', 'max' => 45],
            [['package_name'], 'unique'],
            [['config'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'package_name' => Yii::t('customers', 'Tên gói'),
            'amount' => Yii::t('customers', 'Sô BTC'),
            'status' => Yii::t('customers', 'Trạng thái'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
            'increase_percent' => Yii::t('customers', 'Phần trăm lợi nhuận'),
        ];
    }

    public function behaviors()
    {
        return [

            TimestampBehavior::className()
        ];
    }

    public static function getPackages($maxAmount = null)
    {
        $query = self::find();
        $query->select(['id', 'CONCAT(`package_name`," (",`amount`," BTC )") AS `package_name`']);
        $query->where(['status' => self::STATUS_ACTIVE]);
        if ($maxAmount) {
            $amountExchange = (int)$maxAmount * Yii::$app->params["exchange"]["out"];
            $amountMaxPackage = (int)($amountExchange / (int)Yii::$app->params["exchange"]["in"]);
            $query->andWhere(["<=", "amount", $amountMaxPackage]);
        }
        $query->orderBy(['amount' => SORT_ASC]);
        return $query->all();
    }

    public static function getPackagesById($id)
    {
        $query = self::find();
        $query->where(['status' => self::STATUS_ACTIVE]);
        if ($id) {
            $package = self::findOne($id);
            if ($package) {
                $query->andWhere([">=", "amount", $package->amount]);
            }
        }
        $query->orderBy(['amount' => SORT_ASC]);
        return $query->all();
    }

    public static function getName($id)
    {
        if ($id) {
            $model = static::findOne($id);
            return $model ? $model->package_name : '';
        }
        return '';
    }

    public static function getMinPackage()
    {
        $query = self::find();
        $query->where([
            'status' => self::STATUS_ACTIVE,
        ]);
        $query->orderBy(['amount' => SORT_ASC]);
        return $query->one();
    }

    /**
     * getStatusLabel
     * @return array
     */
    static function getStatusLabels()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Đang hoạt động'),
            self::STATUS_INACTIVE => Yii::t('app', 'Dừng hoạt động'),
        ];
    }

    public function getConfigDefault(){
        $config=json_decode($this->config,true);
        if(!$config){
            return [
                "investBonus"=>[
                    "F1"=>0,
                    "F2"=>0,
                    "F3"=>0,
                    "F4"=>0,
                    "F5"=>0,
                ],
                "reinvestmentBonus"=>[
                    "F1"=>0,
                    "F2"=>0,
                    "F3"=>0,
                    "F4"=>0,
                    "F5"=>0,
                ]
            ];
        }
        return $config;


    }

}
