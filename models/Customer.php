<?php

namespace app\models;

use app\models\logs\AdminLogAction;
use app\modules\admin\controllers\CustomerController;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\base\NotSupportedException;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "customer".
 *
 * @property string $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset
 * @property integer $package_id
 * @property integer $parent_id
 * @property integer $user_id_first_branch
 * @property string $amount
 * @property integer $status
 * @property integer $is_deleted
 * @property integer $is_loan_admin
 * @property integer $created_type
 * @property string $created_by
 * @property string $full_name
 * @property integer $sex
 * @property string $dob
 * @property integer $date_join
 * @property string $phone
 * @property string $ref_code
 * @property string $email
 * @property string $nation
 * @property string $address
 * @property string $last_auto_increase
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $is_active
 * @property integer $poundage_level
 * @property integer $is_invest
 *
 * @property CustomerCase[] $customerCases
 */
class Customer extends \yii\db\ActiveRecord implements IdentityInterface
{

    const STATUS_DELETED = 2;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_LOCK = 3;
    const STATUS_CREATED_BY_MONEY_LOAN = 4;
    const STATUS_REQUIRE_ACTIVE = 5;

    const INVESTED = 1; //Đã tái đầu tư

    const TYPE_ADMIN = 1;
    const TYPE_CUSTOMER = 2;

    const IS_ACTIVE_PENDING = 0;
    const IS_ACTIVE_COMPLETED = 1;

    const SEX_MALE = 1;
    const SEX_FEMALE = 2;
    const IS_DELETED = 1;
    const ID_ONLY_VIEW = 1;
    const IS_LOAN = 1;

    const NATION_UNITED_KINGDOM = 1;
    const NATION_UNITED_STATES = 2;
    const NATION_CANADA = 3;
    const NATION_GERMANY = 4;
    const NATION_NETHERLANDS = 5;
    const NATION_ISRAEL = 6;
    const NATION_FRANCE = 7;
    const NATION_ITALY = 8;
    const NATION_SWITZERLAND = 9;
    const NATION_HONGKONG = 10;
    const NATION_CHINA = 11;
    const NATION_SINGAPORE = 12;
    const NATION_KOREA = 13;
    const NATION_JAPAN = 14;
    const NATION_SOUTH_AFRICA = 15;
    const NATION_VIETNAM = 16;

//    const POUND_LEVEL

    private $_currentWallet;
    private $_currentPackages;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'full_name', 'identity_card', 'ref_code', 'email'], 'required'],
            [['password_hash'], 'required', 'on' => ['create']],
            [['amount'], 'number'],
            [['package_id', 'status', 'sex', 'created_at', 'updated_at', 'created_type', 'parent_id', 'is_deleted', 'user_id_first_branch', 'is_loan_admin', 'is_active',
                'poundage_level', 'is_invest', 'date_join'], 'integer'],
            [['last_auto_increase', 'parent_id'], 'safe'],
            [['username', 'email', 'nation', 'address'], 'string', 'max' => 45],
            [['dob'], 'app\components\validators\DateValidator'],
            [['phone'], 'app\components\validators\PhoneNumberValidator'],
            [['auth_key', 'password_hash', 'password_reset', 'full_name', 'created_by'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 11],
            [['username'], 'unique'],
//            [['username', 'email', 'phone', 'identity_card'], 'unique'],
//            [['date_join'], 'validatorDateNow', 'on' => ['create', 'update']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'username' => Yii::t('customers', 'Tên đăng nhập'),
            'auth_key' => Yii::t('customers', 'Mã bảo mật'),
            'password_hash' => Yii::t('customers', 'Mật khẩu'),
            'password_reset' => Yii::t('customers', 'Mật khẩu cấp lại'),
            'package_id' => Yii::t('customers', 'Gói'),
            'parent_id' => Yii::t('customers', 'Tài khoản giới thiệu'),
            'amount' => Yii::t('customers', 'Tổng số dư'),
            'status' => Yii::t('customers', 'Trạng thái'),
            'created_type' => Yii::t('customers', 'Dạng đăng ký'),
            'created_by' => Yii::t('customers', 'Người tạo'),
            'dob' => Yii::t('customers', 'Ngày sinh'),
            'date_join' => Yii::t('customers', 'Ngày tham gia'),
            'full_name' => Yii::t('customers', 'Họ tên'),
            'sex' => Yii::t('customers', 'Giới tính'),
            'phone' => Yii::t('customers', 'Số điện thoại'),
            'email' => Yii::t('customers', 'Địa chỉ Email'),
            'nation' => Yii::t('customers', 'Quốc gia'),
            'address' => Yii::t('customers', 'Địa chỉ'),
            'last_auto_increase' => Yii::t('customers', 'Lần nhận lợi nhuận cuối cùng'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
            'is_deleted' => Yii::t('customers', 'Đã xóa'),
            'user_id_first_branch' => Yii::t('customers', 'Tài khoản đầu nhánh')
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerCases()
    {
        return $this->hasMany(CustomerCase::className(), ['customer_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|\app\models\User
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function findByRefCode($ref)
    {
        return static::findOne(['ref_code' => $ref, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['customer.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset = null;
    }

    /**
     * Get list of status labels.
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
//            self:: STATUS_DELETED => 'Đã xóa',
            self:: STATUS_INACTIVE => Yii::t('customers', 'locked'),
            self:: STATUS_ACTIVE => Yii::t('customers', 'active'),
        ];
    }

    /**
     * @return int|string label of current user's status.
     */
    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();

        if (isset($labels[$this->status])) {
            return $labels[$this->status];
        }

        return $this->status;
    }

    public static function getIsActiveLabels()
    {
        return [
            self:: IS_ACTIVE_PENDING => Yii::t('customers', 'Pending'),
            self:: IS_ACTIVE_COMPLETED => Yii::t('customers', 'Completed'),
        ];
    }

    public function getIsActiveLabel()
    {
        $labels = static::getIsActiveLabels();

        if (isset($labels[$this->is_active])) {
            return $labels[$this->is_active];
        }

        return '--';
    }

    public static function getStatusTypes()
    {
        return [
            self::TYPE_ADMIN => Yii::t('customers', 'Quản trị viên'),
            self::TYPE_CUSTOMER => Yii::t('customers', 'Người chơi'),
        ];
    }

    public static function getAllCustomer($id = 0, $type = '')
    {
        $query = (new Query())
            ->select('id,CONCAT(`username`,"(",`full_name`,")") AS `fullName`')
            ->from(self::tableName());
        if ((int)$id) {
            $query->andFilterWhere(['<>', 'id', $id]);
        }
        if ($type == Customer::TYPE_ADMIN) {
            $query->andWhere(['created_type' => Customer::TYPE_ADMIN]);
        }
        $query->groupBy('id');
        return $customers = $query->all();
    }

    public static function getName($id)
    {
        if ($id) {
            $model = static::findOne($id);
            return $model ? $model->username : '';
        }
        return '';
    }

    public static function getSexLabels()
    {
        return [
            self::SEX_MALE => Yii::t('customers', 'Nam'),
            self::SEX_FEMALE => Yii::t('customers', 'Nữ'),
        ];
    }

    public function getPackage()
    {
        return $this->hasOne(Package::className(), ["id" => "package_id"]);
    }

    public function addCustomerCase()
    {
        if ($this->id) {
            $result = [];
            //Nếu là nhánh cấp 1 thì nó chính là con của chính nó
            $transaction = Yii::$app->db->beginTransaction();
            try {
                if ($this->parent_id == 0) {
                    $customerCases = new CustomerCase();
                    $customerCases->customer_id = $this->id;
                    $customerCases->parent_id = $this->id;
                    $customerCases->level = 0;
                    $customerCases->customer_username = $this->username;
                    $customerCases->status = 1;
                    if ($customerCases->save()) {
                        $transaction->commit();
                        $result["status"] = "success";
                        $result["code"] = 200;
                        $result["message"] = Yii::t('app', 'Thêm tài khoản giới thiệu thành công');
                        return $result;
                    }
                } elseif ($this->parent_id > 0) {
                    //Lấy danh sách khách hàng là cha của người giới thiệu
                    $customerCases = (new Query())
                        ->select(['id', 'parent_id', 'customer_username', 'status', 'level'])
                        ->from(CustomerCase::tableName())
                        ->where(['customer_id' => $this->parent_id])
                        ->andFilterWhere(['>', 'level', 0])
                        ->all();
                    $data = [];
                    //Add cha nó là lv1
                    $currentTime = time();

                    $data[] = [
                        $this->id,
                        $this->parent_id,
                        1,
                        CustomerCase::STATUS_ACTIVE,
                        $currentTime,
                        $currentTime
                    ];
                    if ($customerCases) {
                        $dataActivity = [];//Sau làm thêm chức năng thông báo tài khoản con đc thêm mới
                        foreach ($customerCases as $item) {
                            $data[] = [
                                $this->id,
                                $item["parent_id"],
                                $item["level"] + 1,
                                $item["status"],
                                $currentTime,
                                $currentTime
                            ];
                        }

                    }
                    if (!empty($data)) {
                        Yii::$app->db->createCommand()->batchInsert(CustomerCase::tableName(), [
                            'customer_id',
                            'parent_id',
                            'level',
                            'status',
                            'created_at',
                            'updated_at',
                        ], $data)->execute();


                        $transaction->commit();
                        $result["status"] = "success";
                        $result["code"] = 200;
                        $result["message"] = Yii::t('app', 'Thêm tài khoản giới thiệu thành công');
                        return $result;
                    } else {
                        $result["status"] = "error";
                        $result["code"] = 110;
                        $result["message"] = Yii::t('app', 'Thêm tài khoản giới thiệu có lỗi');
                        return $result;
                    }
                } else {
                    $result["status"] = "error";
                    $result["code"] = 110;
                    $result["message"] = Yii::t('app', 'Thêm tài khoản giới thiệu có lỗi');
                    return $result;
                }
            } catch (\Exception $ex) {
                $transaction->rollBack();
                $result["status"] = "error";
                $result["code"] = $ex->getCode();
                $result["message"] = $ex->getMessage();
                return $result;
            }

        }
        return $result;
    }


    /* Customer fix tạm sơ đồ chiều ngang */

    private static function getAllCustomers($id, $level = 0, $filterAdmin = 0)
    {
        $data = null;
        if ($id) {
            $customer = Customer::find();
            $customer->select('t.id,t.username,t.parent_id,t.package_id');
            $customer->from('customer t');
            $customer->rightJoin('customer_case c', 'c.customer_id=t.id AND (c.parent_id=' . (int)$id . ' OR t.id=' . (int)$id . ')');
            if ($filterAdmin) {
                $customer->andWhere(['t.user_id_first_branch' => $filterAdmin]);
            }
//            $customer->andFilterWhere([
//                't.status' => self::STATUS_ACTIVE,
//                'c.status' => self::STATUS_ACTIVE,
//            ]);
            if ((int)$level) {
                $customer->andWhere(["<", "c.level", $level]);
            }
            $customer->andWhere(["!=", "t.is_deleted", self::IS_DELETED]);
            $customer->orderBy('t.parent_id');
            $customer->indexBy('id');
            $data = $customer->asArray()->all();
        } else {
            if ((int)$level > 0) {
                $customer = Customer::find();
                $customer->select('t.id,t.username,t.parent_id,t.package_id');
                $customer->from('customer t');
                $customer->rightJoin('(SELECT customer_id, MAX(level) AS level FROM `customer_case` WHERE STATUS=1 GROUP BY customer_id  ORDER BY MAX(level) DESC ) AS c', 'c.customer_id=t.id');
                if ($filterAdmin) {
                    $customer->andWhere(['t.user_id_first_branch' => $filterAdmin]);
                }
                $customer->andFilterWhere([
                    't.status' => self::STATUS_ACTIVE,
                ]);
                $customer->andWhere(["<", "c.level", $level]);
                $customer->andWhere(["!=", "t.is_deleted", self::IS_DELETED]);
                $customer->orderBy('t.parent_id');
                $data = $customer->asArray()->all();
            } else {
                $query = Customer::find();
                $query->select('id,username,parent_id,package_id');
                $query->where(['status' => self::STATUS_ACTIVE]);
                $query->andWhere(["!=", "is_deleted", self::IS_DELETED]);
                if ($filterAdmin) {
                    $query->andWhere(['user_id_first_branch' => $filterAdmin]);
                }
                $query->orderBy(['parent_id' => SORT_ASC])->asArray();
                $query->indexBy('id');
                $data = $query->all();
            }
        }

        return $data;
    }

    public static function getTreeCustomer($id = 0, $level = 0, $filterAdmin = 0)
    {

        $customers = static::getAllCustomers($id, $level, $filterAdmin);
        $data = array();
        //Tính thành phần cấp con
        $query = (new Query());
        $query->select('COUNT(id) as children, parent_id');
        $query->from('customer_case');
        $query->where(['parent_id' => array_keys($customers)]);
        $query->andFilterWhere(['<>', 'parent_id', 0]);
//        $query->andFilterWhere(['<>', 'parent_id', $id]);
        $query->groupBy('parent_id');
        $query->indexBy('parent_id');
        $childrens = $query->all();
        foreach ($customers as $cus) {
            if (isset($childrens[$cus["id"]]["children"])) {
                $cus['total_member'] = $childrens[$cus["id"]]["children"];
            } else {
                $cus['total_member'] = 0;
            }
            if (!isset($data[$cus['id']])) {
                $data[$cus['id']] = $cus;
            }
            $data[(int)$cus["parent_id"]]["childrens"][] = $cus["id"];
        }

        return $data;
    }


    public static function showTree($id = 0, $level = 0, $click = true, $filterAdmin = 0)
    {
        $html = '';
        $data = self::getTreeCustomer($id, $level, $filterAdmin);
        if (!empty($data)) {
            $package = Package::find();
            $package->select('id,package_name');
            $package->andFilterWhere([
                'status' => self::STATUS_ACTIVE
            ]);
            $packages = $package->asArray()->all();
            if ($packages) {
                foreach ($packages as $item) {
                    $data['package'][$item['id']] = $item['package_name'];
                }
            }

            $html = self::renderTree($data, $id, $level, $click);
        }
        return $html;
    }

    public static function getShowCustomer($id)
    {
        $html = '';
        $data = self::getTreeCustomer($id);
        if (!empty($data)) {
            $package = Package::find();
            $package->select('id,package_name');
            $package->andFilterWhere([
                'status' => self::STATUS_ACTIVE
            ]);
            $packages = $package->asArray()->all();
            if ($packages) {
                foreach ($packages as $item) {
                    $data['package'][$item['id']] = $item['package_name'];
                }
            }
            $html = self::renderTree($data, $id);
        }
        return $html;
    }

    public static function renderHtml($data, $id, $click = true, $level = 20)
    {
        $level -= 1;
        $html = '';
        if ($level > 0) {
            if (isset($data[$id])) {
                $c = $data[$id];
                $hasChildren = (isset($c['childrens']) && count($c['childrens'])) ? true : false;
                $name = $c['username'];
                if (isset($data['package'][$c['package_id']])) {
                    $name .= '<br/>[' . substr($data['package'][$c['package_id']], 0, 3) . ']';
                }
                $onclick = '';
                if ($click && $hasChildren) {
                    $onclick = 'onclick="showtree(' . $c['id'] . ',0)"';
                }

//                $html .= '<li> <a href="javascript:void(0)" class="tree_user" ' . $onclick . ' ><img src="' . \Yii::getAlias('@web') . "/images/flags/i_user.png" . '" class="i_user" alt="' . $c['username'] . '"/><br/>' . $name . '<br>' . (($hasChildren) ? '<span class="fa fa-arrow-down text center view-more"> </span>' : '') . '</a>';
                $html .= '<li class="' . (($hasChildren && $level < 19) ? 'children-active' : '') . '"> <a href="javascript:void(0)" class="tree_user" ' . $onclick . ' ><img src="' . \Yii::getAlias('@web') . "/images/folder.gif" . '" class="i_user" alt="' . $c['username'] . '"/> ' . $name . '</a> ' . ((isset($c['total_member']) && $c['total_member'] > 0) ? '(' . $c['total_member'] . ')' : '');
                if ($hasChildren) {
                    $html .= '<ul>';
                    foreach ($c['childrens'] as $cid) {
                        $html .= self::renderHtml($data, $cid, $click, $level);
                    }
                    $html .= '</ul>';
                }
                $html .= '</li>';
            }
        }

        return $html;
    }

    public static function renderTree($data, $id, $level = 0, $click = true)
    {
        $html = '';
        if ($id) {
            $html .= '<ul>';
            $html .= self::renderHtml($data, $id, $click);
            $html .= '</ul>';
        } else {
            if (isset($data[$id]) && $data[$id]) {
                $c = $data[$id];
                if (isset($c['childrens']) && count($c['childrens'])) {
                    $html .= '<ul>';
                    foreach ($c['childrens'] as $cid) {
                        $html .= self::renderHtml($data, $cid, $click);
                    }
                    $html .= '</ul>';
                }
            }
        }
        return $html;
    }


    public static function getParents($id, $count = 0, $accountId = 0)
    {
        $i = 0;
        $data = ['id' => 0];
        for ($i = 0; $i < $count; $i++) {
            $dt = (new Query())->select(['id', 'parent_id'])->from(Customer::tableName())->where(['id' => $id])->one();

            if (empty($dt) || (($dt["parent_id"]) == 0) || $dt["id"] == $accountId) {
                $data['id'] = $accountId;
//                $data['level'] = 0;
                break;
            } else {
                $data['id'] = $dt['id'];
                $id = $dt['parent_id'];

            }
            unset($dt);

        }
        return $data;
    }


    /**
     * Function tạo mã code giới thiệu đăng ký
     */
    public function createRefCode()
    {
        $this->ref_code = 'CUS' . uniqid();
    }

    /**
     * @return \yii\db\ActiveQuery
     * Function relation Customer và package
     */
    public function getPackages()
    {
        return $this->hasMany(CustomerPackage::className(), ['customer_id' => 'id']);
    }

    /**
     * Hàm khởi tạo wallet | 1 customer chỉ có duy nhất 1 wallet
     */
    public function createWallet()
    {
        if ($this->id) {
            $wallet = new CustomerWallet();
            $wallet->customer_id = $this->id;
            $wallet->save();
        }
    }

    public function getWallet()
    {
        return $this->hasOne(CustomerWallet::className(), ['customer_id' => 'id']);
    }

    public function findWallet()
    {
        return CustomerWallet::getWallet($this->id);
    }

    /**
     * Cache thông tin ví hiện tại vào property $currentWallet.
     */
    public function getCurrentWallet()
    {
        if (!$this->_currentWallet) {
            $this->_currentWallet = $this->findWallet();
        }
        return $this->_currentWallet;
    }

    public function getCurrentPackages()
    {
        if (!$this->_currentPackages) {
            $query = (new Query())
                ->select(['customer_package.*'])
                ->from(CustomerPackage::tableName())
                ->where(['customer_package.customer_id' => $this->id])
                ->indexBy('id');

            $this->_currentPackages = $query->all();
        }
        return $this->_currentPackages;
    }


    public function canPH()
    {
        if (!Yii::$app->params['CUSTOMER_OPEN_CREATE_PACKAGE']) {
            return false;
        }
        if (!empty($this->getCurrentPackages())) {
            return false;
        }
        return true;
    }

    public function getUrlRegisterReferal()
    {
        if ($this->ref_code) {
            return Yii::$app->urlManager->createAbsoluteUrl(['/customer/default/register', 'ref' => $this->ref_code]);
        }
        return '';
    }

    public static function getNationLabels()
    {
        return [
            self::NATION_UNITED_KINGDOM => 'United Kingdom',
            self::NATION_UNITED_STATES => 'United States',
            self::NATION_CANADA => 'Canada',
            self::NATION_GERMANY => 'Germany',
            self::NATION_NETHERLANDS => 'Netherlands',
            self::NATION_ISRAEL => 'Israel',
            self::NATION_FRANCE => 'France',
            self::NATION_ITALY => 'Italy',
            self::NATION_SWITZERLAND => 'Switzerland',
            self::NATION_HONGKONG => 'HongKong',
            self::NATION_CHINA => 'China',
            self::NATION_SINGAPORE => 'Singapore',
            self::NATION_KOREA => 'Korea',
            self::NATION_JAPAN => 'Japan',
            self::NATION_SOUTH_AFRICA => 'South Africa',
            self::NATION_VIETNAM => 'Vietnam',
        ];
    }

    public function setActive()
    {
        $token = $this->createToken();
        if ($token) {
            $this->is_active = self::IS_ACTIVE_COMPLETED;
            if ($this->save()) {
                $token->status = $token::STATUS_USED;
                if ($token->save(true, ['is_active', 'updated_at'])) {
                    return true;
                }
            }
        }
        return false;
    }

    public function createToken()
    {
        $token = new CustomerToken();
        $token->customer_id = $this->id;
        $token->token = $token::generateToken();
        $token->type = CustomerToken::TYPE_CUSTOMER;
        if ($token->save()) {
            return $token;
        }
        return null;
    }

    public function getCurrentToken()
    {
        return CustomerToken::findCurrentToken($this->id);
    }

    public function countCurrentToken()
    {
        return CustomerToken::countCurrentToken($this->id);
    }

    public function getBankAddress()
    {
        return $this->hasMany(CustomerBankAddress::className(), ['customer_id' => 'id']);
    }

    public function getCurrentBankAddress()
    {
        return $this->getBankAddress()->where(['status' => CustomerBankAddress::STATUS_ACTIVE])->all();
    }

    public function checkInvest()
    {
        if (!$this->is_invest) {
            $packageCount = CustomerPackage::find()->where(["status" => [CustomerPackage::STATUS_RUNNING, CustomerPackage::STATUS_EMPTY]])->count();
            if ($packageCount > 0) {
                $this->is_invest = 1;
                if ($this->save(true, ['is_invest', 'updated_at'])) {
                    $this->is_invest = 1;
                } else {
                    $this->is_invest = 0;
                }
            }
        }
//        return $this;
    }

    public function compensatePoundageSystem($amount, $sign = 0)
    {
        $sign = ($sign) ? $sign : CustomerPoundageTransaction::SIGN_ADD;
        $transaction = Yii::$app->db->beginTransaction();
        try {
            if ($amount <= 0) {
                throw new Exception('Amount required !');
            }
            $poundage = new CustomerPoundageTransaction();
            $poundage->sign = $sign;
            $poundage->customer_id = $this->id;
            $poundage->customer_username = $this->username;
            $poundage->from_id = 0;
            $poundage->amount = $amount;
            $poundage->balance_before = 0;
            $poundage->balance_after = 0;
            $poundage->level = 0;
            $poundage->from_username = 'system';
            $poundage->type = CustomerPoundageTransaction::TYPE_POUNDAGE_COMPENSATE;
            $poundage->status = CustomerPoundageTransaction::STATUS_PENDING;
            if ($poundage->save()) {
                AdminLogAction::createLogAction(AdminLogAction::ACTION_TYPE_CREATE, $poundage->attributes);
                $transaction->commit();
                return true;
            }

        } catch (\Exception $ex) {
            $transaction->rollBack();
            return false;
        }
        return false;
    }

    public function addPoundageSystem($lv)
    {
        if ($lv > $this->poundage_level) {
            $wallet = $this->getCurrentWallet();
            $beforeBalance = $wallet->poundage;
            $poundage = new CustomerPoundageTransaction();
            $poundage->sign = CustomerPoundageTransaction::SIGN_ADD;
            $poundage->customer_id = $this->id;
            $poundage->customer_username = $this->username;
            $poundage->from_id = 0;
            $poundage->amount = $lv;
            $poundage->balance_before = $beforeBalance;
            $wallet->poundage += $poundage->amount;
            $poundage->balance_after = $wallet->poundage;
            $poundage->level = 0;
            $poundage->type = CustomerPoundageTransaction::TYPE_POUNDAGE_SYSTEM;
            $this->poundage_level = $lv;
            if ($poundage->save()) {
                if ($wallet->save(true, ['poundage'])) {
                    $this->save(true, ['poundage_level']);
                    return true;
                }

            }
        }
        return false;
    }

    public
    function calcBalance()
    {
        $packages = $this->getCurrentPackages();
        $packageBalance = 0;
        $pakageHolding = 0;
        if ($packages) {
            foreach ($packages as $pack) {
                $packageBalance += $pack["balance"];
                $package = Package::findOne($pack["package_id"]);
                if (!$package) {
                    return [
                        'balance' => $packageBalance,
                        'holding' => 0
                    ];
                }
                if ($pack["status"] == CustomerPackage::STATUS_RUNNING) {
                    $increasePackage = round((($pack["package_amount"] / 100) * $package->increase_percent), 8);
                    $pakageHolding += $pack["package_amount"] + $increasePackage;
                }
            }
        }
        return [
            'balance' => $packageBalance,
            'holding' => $pakageHolding
        ];
    }

    public
    function requireWidthDrawnPoundage()
    {
        $result = [
            "status" => "error",
            "message" => [],
        ];
        $currentPackage = $this->getCurrentPackages();
        if (!$currentPackage) {
            $result["status"] = "error";
            $result["message"][] = 'You have not package. Please open package first';
        } else {
            $packageActive = array_shift($currentPackage);
            if (!$packageActive || empty($packageActive)) {
                $result["status"] = "error";
                $result["message"][] = 'You have not package. Please open package first';
            } else {
                if (!in_array($packageActive["status"], [CustomerPackage::STATUS_EMPTY, CustomerPackage::STATUS_RUNNING]) && $packageActive["is_reinvestment"] != CustomerPackage::PACKAGE_IS_REINVESTMENT) {
                    $result["status"] = "error";
                    $result["message"][] = 'You are not completed first PH, please completed.';
                }
            }

        }
        //lay ban ghi rút tiền mới nhất
        $model = CustomerRequest::find()->
        where([
            'type' => CustomerRequest::TYPE_RECEIVE_WHEN_CASHOUT_POUNDAGE,
            'customer_id' => $this->id,
        ])->orderBy(['id' => SORT_DESC])->limit(1)->one();
        if (!$model && empty($result["message"])) {
            $result["status"] = "success";
        } else {
            if ($model->status == CustomerRequest::STATUS_COMPLETED && empty($result["message"])) {
                $timeDiff = (int)(time() - $model->updated_at);
                if ($timeDiff > Yii::$app->params["POUNDAGE_TIME_DELAY_CASH_OUT"]) {
                    //Nếu lớn hơn 3h thì đc rút
                    $result["status"] = "success";
                } else {
                    $result["status"] = "error";
                    $result["message"][] = 'Last withdrawn commission least than 3 hour.';
                }
            } else {
                $result["status"] = "error";
                $result["message"][] = 'Please wait for completion of the current withdrawal order.';
            }
        }
        return $result;
    }

    public
    function canWithDrawnPoundage()
    {
        //Check xem nó đã vào gói nào chưa
        $result = $this->requireWidthDrawnPoundage();
        if ($result["status"] == "success") {
            return true;
        }
        return false;
    }

    public
    function getTotalChildren()
    {
        $query = (new Query());
        $query->select('COUNT(id) as totalChildren');
        $query->from('customer_case');
        $query->where(['parent_id' => $this->id]);
        $query->andFilterWhere(['>', 'level', 0]);
        $childrens = $query->one();
        //$customers = static::getAllCustomers($this->id);
        //echo '<pre>';var_dump($childrens);die;
        ////select * from customer_case where parent_id ={user_id} and level>0
        return ($childrens['totalChildren']);
    }

    public
    function updateBankAddressToTransaction($bankAddress)
    {
        //ktra tồn tại địa chỉ đấy thuộc tài khoản user ko
        $currentBankAddress = CustomerBankAddress::findOne(['customer_id' => $this->id, 'bank_address' => $bankAddress]);
        if ($currentBankAddress) {
            //Cập nhật địa chỉ ví tại request
            Yii::$app->db->createCommand()->update(CustomerRequest::tableName(),
                [
                    'block_chain_wallet_id' => $bankAddress,
                ],
                [
                    'customer_id' => $this->id,
                    'type' => [CustomerRequest::TYPE_RECEIVE_WHEN_CASHOUT_PACKAGE, CustomerRequest::TYPE_RECEIVE_WHEN_CASHOUT_POUNDAGE, CustomerRequest::TYPE_RECEIVE_WHEN_CASHOUT_POUNDAGE],
                    'status' => [CustomerRequest::STATUS_CREATED]
                ])->execute();

            //Cập nhật địa chỉ tại transaction
            Yii::$app->db->createCommand()->update(CustomerTransaction::tableName(),
                ['customer_receiver_bank_address' => $bankAddress],
                [
                    'customer_receiver_id' => $this->id,
                    'status' => CustomerTransaction::STATUS_SEND_PENDING,
                ])->execute();
        }
    }

    public function isProcess()
    {
        if ($this->is_deleted == self::IS_DELETED || $this->is_active == self::IS_ACTIVE_PENDING || $this->status !== self::STATUS_ACTIVE) {
            return false;
        }

        return true;

    }

}

