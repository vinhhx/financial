<?php

namespace app\models;

use app\components\helpers\StringHelper;
use app\components\helpers\UploadHelpers;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "video".
 *
 * @property integer $id
 * @property string $title
 * @property string $alias
 * @property string $video_url
 * @property string $video_id
 * @property integer $use_thumb
 * @property string $video_thumb
 * @property string $image
 * @property integer $status
 * @property integer $deleted
 * @property integer $created_at
 * @property integer $updated_at
 */
class Video extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'video_url'], 'required'],
            [['use_thumb', 'status', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['title', 'alias', 'video_url', 'video_thumb', 'image'], 'string', 'max' => 255],
            [['video_id'], 'string', 'max' => 25],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'alias' => Yii::t('app', 'Alias'),
            'video_url' => Yii::t('app', 'Video Url'),
            'video_id' => Yii::t('app', 'Video ID'),
            'use_thumb' => Yii::t('app', 'Use Thumb'),
            'video_thumb' => Yii::t('app', 'Video Thumb'),
            'file' => Yii::t('app', 'Video Thumb'),
            'image' => Yii::t('app', 'Image'),
            'status' => Yii::t('app', 'Status'),
            'deleted' => Yii::t('app', 'Deleted'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->alias = StringHelper::rewrite($this->title);

        if ($this->video_url) {
            preg_match('/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/', $this->video_url, $matches);

            if ($matches && isset($matches[1])) {
                $this->video_id = $matches[1];
                $this->video_thumb = "http://img.youtube.com/vi/{$this->video_id}/0.jpg";
            }
        }

        // Upload file
        $this->upload();

        return parent::beforeSave($insert);
    }


    public function upload()
    {
        $this->file = UploadedFile::getInstance($this, 'file');

        if ($this->file && $this->validate(['file'])) {

            $fileName = md5(uniqid() . $this->file->name) . '.' . $this->file->extension;

            // Uploaded dir
            $folderId = strtotime(date('Y-m-01 00:00:00'));

            $uploadDir = UploadHelpers::getUploadDirById($folderId, 'video');

            // Create folder upload
            BaseFileHelper::createDirectory($uploadDir);

            if ($this->file->saveAs($uploadDir . $fileName)) {
                $this->image = UploadHelpers::getImageById($folderId, $fileName, 'video');
                $this->file = null;
            }
        }
    }

    public function getImage()
    {
        if ($this->use_thumb) {
            return $this->video_thumb;
        }
        return $this->image;
    }
}
