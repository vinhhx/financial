<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customer_transaction_queue".
 *
 * @property integer $id
 * @property interger $customer_id
 * @property string $customer_username
 * @property string $customer_request_id
 * @property integer $type
 * @property integer $type_step
 * @property integer $status
 * @property string $amount
 * @property integer $start_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $min_amount
 * @property integer $max_amount
 *
 */
class CustomerTransactionQueue extends \yii\db\ActiveRecord
{
//    const TYPE_OPEN = 1; // Mở gói đầu tư
//    const TYPE_OPEN_PACKAGE_FINISH = 2; // Mở gói đầu tư
//    const TYPE_CASHOUT_PACKAGE = 10; //type rút gói đầu tư
//    const TYPE_CASHOUT_POUNDAGE = 20; //type rút hoa hồng

    const STATUS_PENDING = 1;
    const STATUS_ADDED = 9;
    const STATUS_REQUEST_COMPLETED_FIRST = 2;
    const STATUS_COMPLETED = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_transaction_queue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'start_at', 'customer_username'], 'required'],
            [['customer_id', 'customer_request_id', 'type', 'status', 'created_at', 'updated_at', 'start_at', 'type_step'], 'integer'],
            [['amount', 'min_amount', 'max_amount'], 'number'],
            [['customer_username'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'customer_id' => Yii::t('customers', 'Mã người chơi'),
            'customer_request_id' => Yii::t('customers', 'Mã người chơi yêu cầu'),
            'type' => Yii::t('customers', 'loại'),
            'status' => Yii::t('customers', 'Trạng thái'),
            'amount' => Yii::t('customers', 'Số BTC'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function createTransaction()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $tkNhan = null;
            $tkCho = null;
            switch ($this->type) {
                case CustomerTransaction::TYPE_OPEN_PACKAGE: //Nếu loại queue là mở gói đầu tư
                    $tkNhan = $this->mappingAccount('add'); // Tìm 1 tài khoản cho đúng yêu cầu
                    if (!$tkNhan) {
                        //Không tìm đc tài khoản cho nào hợp lý dừng mapping
                        throw new \Exception('no account required cash out ...');
                    }
                    //Tài khoản cho chính là queue hiện tại
                    $tkCho = $this;
                    break;
                case CustomerTransaction::TYPE_CASH_OUT_PACKAGE://Trường hợp nhận thì sẽ tìm tk cho đi
                case CustomerTransaction::TYPE_CASH_OUT_POUNDAGE:
                    $tkCho = $this->mappingAccount('sub');
                    if (!$tkCho) {
                        //Nếu ko tìm đc tk cho thì dừng lại tại đây
                        throw new \Exception(Yii::t('messages', 'Không có tài khoản nào đang chờ cho'));
                    }
                    $tkNhan = $this;
                    break;
                default:
                    throw new \Exception(Yii::t('messages', 'Giao dịch không tồn tại'));
                    break;
            }
            if ($tkCho && $tkNhan) {
                //Cập nhật tài khoản cho số tiền
                //Nếu nhận và cho không cân đối tách nhận thành 2 gói nhận
                if ($tkNhan->amount > $tkCho->amount) {
                    $newAmount = $tkNhan->amount - $tkCho->amount;
                    //Cập nhật tk nhận thành số tiền mới
                    $tkNhan->amount = $tkCho->amount;
                    if ($tkNhan->save(true, ['amount', 'updated_at'])) {
                        //Khởi tạo 1 Queue tk Nhận với Amount mới
                        $newReceiverQueue = new CustomerTransactionQueue();
                        $newReceiverQueue->customer_id = $tkNhan->customer_id;
                        $newReceiverQueue->customer_username = $tkNhan->customer_username;
                        $newReceiverQueue->customer_request_id = $tkNhan->customer_request_id;
                        $newReceiverQueue->type = $tkNhan->type;
                        $newReceiverQueue->type_step = $tkNhan->type_step;
                        $newReceiverQueue->status = $tkNhan->status;
                        $newReceiverQueue->start_at = $tkNhan->start_at;
                        $newReceiverQueue->amount = $newAmount;
                        if (!$newReceiverQueue->save()) {
                            throw new Exception(Yii::t('messages', ' Không đẩy số dư người nhận vào hàng chờ được'));
                        }
                    } else {
                        throw new Exception(Yii::t('messages', ' Không cập nhật trạng thái người cho'));
                    }
                }

                $requests = CustomerRequest::find()->where([
                    'id' => [$tkCho->customer_request_id, $tkNhan->customer_request_id],
                ])->indexBy('id')->all();

                $sender = (isset($requests[$tkCho->customer_request_id]) && ($requests[$tkCho->customer_request_id]["status"] != CustomerRequest::STATUS_COMPLETED)) ? $requests[$tkCho->customer_request_id] : null;

                $receiver = (isset($requests[$tkNhan->customer_request_id]) && ($requests[$tkNhan->customer_request_id]["status"] != CustomerRequest::STATUS_COMPLETED)) ? $requests[$tkNhan->customer_request_id] : null;
                //Lấy và gán tài khoản cho và tk nhận
                if (!$sender) {
                    throw new \Exception(Yii::t('messages', 'Yêu cầu cho không xác định'));
                }
                if (!$receiver) {
                    throw new \Exception(Yii::t('messages', 'Yêu cầu nhận không xác định'));
                }
                $tkCho->amount = $tkNhan->amount;
                if ($tkCho->type_step == CustomerTransaction::TYPE_STEP_OPEN_PACKAGE_1) {
                    if ($tkCho->save(true, ['amount'])) {
                        //Cập nhật lại số tiền của tài khoản cho đi thứ 2
                        $amountDiff = $sender->amount - $tkCho->amount;
                        Yii::$app->db->createCommand()->update(self::tableName(), [
                            'amount' => $amountDiff,
                            'min_amount' => $amountDiff,
                            'max_amount' => $amountDiff,
                        ], [
                                'customer_request_id' => $sender->id,
                                'type_step' => CustomerTransaction::TYPE_STEP_OPEN_PACKAGE_2,
                            ]
                        )->execute();
                    }
                }
                $customerTransaction = new CustomerTransaction();

                $customerTransaction->customer_sent_queue_id = $tkCho->id;

                $customerTransaction->customer_sent_request_id = $sender->id;

                $customerTransaction->customer_sent_id = $sender->customer_id;

                $customerTransaction->customer_sent_username = $tkCho->customer_username;

                $customerTransaction->customer_sent_bank_address = $sender->block_chain_wallet_id;

                $customerTransaction->customer_receiver_queue_id = $tkNhan->id;

                $customerTransaction->customer_receiver_request_id = $receiver->id;

                $customerTransaction->customer_receiver_id = $receiver->customer_id;

                $customerTransaction->customer_receiver_username = $tkNhan->customer_username;

                $customerTransaction->customer_receiver_bank_address = $receiver->block_chain_wallet_id; //khi là người gửi

                $customerTransaction->amount = $this->amount;

                $customerTransaction->status = CustomerTransaction::STATUS_SEND_PENDING;

                $customerTransaction->customer_sent_type = $tkCho->type;
                $customerTransaction->customer_receiver_type = $tkNhan->type;

                $customerTransaction->type_step = $this->type_step;

                $customerTransaction->expired_sent = time() + Yii::$app->params['TimeRequestLast'];

                $customerTransaction->expired_approve = ($customerTransaction->expired_sent + Yii::$app->params['TimeRequestLast']);

                if (!$customerTransaction->save()) {

                    throw new \Exception(Yii::t('messages', 'Không tạo được giao dịch cho yêu cầu') . $this->id);
                } else {

                }
                $tkCho->status = self::STATUS_COMPLETED;
                if (!$tkCho->save(true, ['status', 'updated_at'])) {
                    throw new \Exception(Yii::t('messages', 'Không cập nhật được trạng thái cho của ') . $this->id);

                }
                $tkNhan->status = self::STATUS_COMPLETED;
                if (!$tkNhan->save(true, ['status', 'updated_at'])) {
                    throw new \Exception(Yii::t('messages', 'Không cập nhật được trạng thái nhận của ') . $this->id);

                }
                $transaction->commit();
//                echo 'tao transaction thành công';
                return true;

            }


        } catch (\Exception $ex) {
            $transaction->rollBack();
            echo $ex->getMessage();
            return false;

        }

    }

    public function mappingAccount($type)
    {
        $query = CustomerTransactionQueue::find();
//        $query->leftJoin(Customer::tableName(), 'customer.id=customer_transaction_queue.customer_id');
        $query->where(['status' => self::STATUS_PENDING]);
        $query->andFilterWhere(['<=', 'start_at', time()]);
        $query->andFilterWhere(['<>', 'customer_id', $this->customer_id]);
        switch ($type) {
            case 'add':
//                if ($this->type_step == CustomerTransaction::TYPE_STEP_OPEN_PACKAGE_1) {
//                    $query->andFilterWhere(['between', 'amount', $this->min_amount, $this->max_amount]);
//                } else {
//                    $query->andWhere(['amount' => $this->amount]);
//                }
                $query->andFilterWhere(['>=', 'amount' => $this->amount]);
                $query->andFilterWhere(['not in', 'type', [CustomerTransaction::TYPE_OPEN_PACKAGE]]);
                $customerSupport = $query->limit(1)->one();
                return $customerSupport;
                break;
            case'sub':
                //Tìm những tài khoản cho nếu
                $query2 = $query;
//                $query2->andFilterWhere(['>=', 'min_amount', $this->amount]);
                $query2->andFilterWhere(['<=', 'amount', $this->amount]);
                $query2->andFilterWhere(['in', 'type', [CustomerTransaction::TYPE_OPEN_PACKAGE]]);
                $query2->andFilterWhere(['in', 'type_step', [CustomerTransaction::TYPE_STEP_OPEN_PACKAGE_1]]);
                $query2->orderBy('start_at');
                $customerSupport = $query2->limit(1)->one();
                if (!$customerSupport) {
                    $query->andWhere(['amount' => $this->amount]);
                    $query->andFilterWhere(['in', 'type', [CustomerTransaction::TYPE_OPEN_PACKAGE]]);
                    $query->andFilterWhere(['in', 'type_step', [CustomerTransaction::TYPE_STEP_OPEN_PACKAGE_2]]);
                    $customerSupport = $query->limit(1)->one();
                }
                return $customerSupport;
                break;
            default:
                break;
        }
        return null;

//        if($customer){
//            //lấy tk admin
////            Customer::findOne(['id'=>Yii::$app->params['customerAdminWalletId']['id']]);
//        }
    }

    public static function getStatusLabels()
    {
        return [
            self::STATUS_PENDING => Yii::t('customers', 'Đang chờ'),
            self::STATUS_ADDED => Yii::t('customers', 'Đã đặt chuyển'),
            self::STATUS_REQUEST_COMPLETED_FIRST => Yii::t('customers', 'Hoàn thành yêu cầu chuyển lần thứ nhất'),
            self::STATUS_COMPLETED =>Yii::t('customers', 'Hoàn thành'),
        ];
    }

    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        if (isset($labels[$this->status])) {
            return $labels[$this->status];
        }
        return '';
    }

    public function getTypeLabel()
    {
        $labels = CustomerTransaction::getTypeLabels($this->type);
    }

    public function getTypeStepLabel()
    {
        $labels = CustomerTransaction::getTypeStepLabels();
        if (isset($labels[$this->type_step])) {
            return $labels[$this->type_step];
        }
        return '';
    }

}
