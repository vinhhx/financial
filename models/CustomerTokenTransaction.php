<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customer_token_transaction".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $bill_id
 * @property string $quantity
 * @property integer $sign
 * @property string $receiver
 * @property string $content
 * @property integer $type
 * @property string $created_at
 * @property string $updated_at
 * @property integer $type_module
 */
class CustomerTokenTransaction extends \yii\db\ActiveRecord
{
    const TYPE_BOUGHT = 1;
    const TYPE_USED_ACTIVE = 2;
    const TYPE_USED_WITHDRAW = 3;
    const TYPE_USED_UPGRADE = 4;
    const TYPE_SENT = 10;
    const TYPE_RECEIVED = 11;

    const SIGN_ADD = 1;
    const SIGN_SUB = 2;

    const TYPE_MODULE_CUSTOMER = 1;
    const TYPE_MODULE_INVEST = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_token_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'quantity'], 'required'],
            [['customer_id', 'bill_id', 'quantity', 'sign', 'type', 'created_at', 'updated_at', 'type_module'], 'integer'],
            [['receiver', 'content'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'bill_id' => Yii::t('app', 'Bill ID'),
            'quantity' => Yii::t('app', 'Quantity'),
            'sign' => Yii::t('app', 'Sign'),
            'receiver' => Yii::t('app', 'User'),
            'content' => Yii::t('app', 'Content'),
            'type' => Yii::t('app', 'Type'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    public static function getSignLabels()
    {
        return [
            static::SIGN_ADD => ' + ',
            static::SIGN_SUB => ' - ',
        ];
    }

    public function getSignLabel()
    {
        $labels = static::getSignLabels();
        if (isset($labels[$this->sign])) {
            return $labels[$this->sign];
        }
        return '';
    }
}
