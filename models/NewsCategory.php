<?php

namespace app\models;

use app\components\helpers\StringHelper;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "news_category".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $title
 * @property string $alias
 * @property string $intro
 * @property string $image
 * @property integer $status
 * @property integer $deleted
 * @property integer $created_at
 * @property integer $updated_at
 */
class NewsCategory extends \yii\db\ActiveRecord
{
    public $level;

    /**
     * @var $item self
     */
    private static $item;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'status', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'required'],
            [['intro'], 'string'],
            [['title', 'alias', 'image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Mã'),
            'parent_id' => Yii::t('app', 'Cấp cha'),
            'title' => Yii::t('app', 'Tiêu đề'),
            'alias' => Yii::t('app', 'Tên khác'),
            'intro' => Yii::t('app', 'Giới thiệu'),
            'image' => Yii::t('app', 'Hình ảnh'),
            'status' => Yii::t('app', 'Trạng thái'),
            'deleted' => Yii::t('app', 'Đã xóa'),
            'created_at' => Yii::t('app', 'Tạo lúc'),
            'updated_at' => Yii::t('app', 'Cập nhật lúc'),
        ];
    }

    public function getItems()
    {
        if (self::$item === null) {
            self::$item = self::findAll(['deleted' => 0]);
        }

        return self::$item;
    }


    /**
     * @param int $parentId
     * @param int $level
     * @return NewsCategory[]
     */
    public function getItemByLevel($parentId = 0, $level = 1)
    {
        $items = $this->getItems();

        $result = [];

        if ($items) {
            foreach ($items as $item) {
                if ($item->parent_id == $parentId) {
                    $item->level = $level;
                    $result[] = $item;

                    $result = ArrayHelper::merge($result, $this->getItemByLevel($item->id, $level + 1));
                }
            }
        }

        return $result;
    }

    public function getItemOptionByLevel($includeRoot = true)
    {
        $items = $this->getItemByLevel();

        $result = [];

        if ($includeRoot) {
            $result[0] = '*** ROOT ***';
        }

        foreach ($items as $item) {
            $result[$item->id] = str_repeat('-- ', $item->level) . $item->title;
        }

        return $result;
    }

    public function beforeSave($insert)
    {
        $this->alias = StringHelper::rewrite($this->title);

        return parent::beforeSave($insert);
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }
}
