<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customer_case".
 *
 * @property string $id
 * @property string $customer_id
 * @property integer $parent_id
 * @property string $customer_username
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Customer $customer
 */
class CustomerCase extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_case';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id', 'parent_id', 'status', 'created_at', 'updated_at', 'level'], 'integer'],
            [['customer_username'], 'string', 'max' => 150],
            [['customer_id', 'parent_id'], 'unique', 'targetAttribute' => ['customer_id', 'parent_id'], 'message' => 'The combination of Customer ID and Parent ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'customer_id' => Yii::t('customers', 'Mã người chơi'),
            'parent_id' => Yii::t('customers', 'Mã người giới thiệu'),
            'level' => Yii::t('customers', 'Cấp độ'),
            'status' => Yii::t('customers', 'Trạng thái'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    public function getCustomerParent()
    {
        return $this->hasOne(Customer::className(), ['id' => 'parent_id']);
    }

    public function behaviors()
    {
        return [TimestampBehavior::className(),];
    }
}
