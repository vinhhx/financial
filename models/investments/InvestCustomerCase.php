<?php

namespace app\models\investments;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "invest_customer_case".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $parent_id
 * @property integer $level
 * @property integer $position
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class InvestCustomerCase extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const LEFT_LEG = 1;
    const RIGHT_LEG = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invest_customer_case';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'parent_id', 'level'], 'required'],
            [['customer_id', 'parent_id', 'level', 'position', 'status', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('investment', 'Mã'),
            'customer_id' => Yii::t('investment', 'Mã người chơi'),
            'parent_id' => Yii::t('investment', 'Mã người giới thiệu'),
            'level' => Yii::t('investment', 'Cấp độ'),
            'position' => Yii::t('investment', 'Vị trí'),
            'status' => Yii::t('investment', 'Trạng thái'),
            'created_at' => Yii::t('investment', 'Thời gian tham gia'),
            'updated_at' => Yii::t('investment', 'Thời gian tham gia'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getParent()
    {
        return $this->hasOne(InvestCustomer::className(), ["id" => "parent_id"]);
    }

    public function getPackage()
    {
        return $this->hasOne(InvestCustomerPackage::className(), ['customer_id' => 'id'])->via('parent');
    }

    /**
     * Lien ket 1-1 voi bang invest_customer
     */
    public function getInvestCustomer()
    {
        return $this->hasOne(InvestCustomer::className(), ['id' => 'customer_id']);
    }

    /**
     * Lien ket 1-1 voi bang invest_customer_package thong qua bang invest_customer
     */
    public function getInvestCustomerPackage()
    {
        return $this->hasOne(InvestCustomerPackage::className(), ['customer_id' => 'id'])->via('investCustomer');
    }

    public function getInvestPackage()
    {
        if ($models = InvestPackage::find()->where(['status' => InvestPackage::STATUS_ACTIVE])->indexBy('id')->all()) {
            return $models;
        }
    }

    public static function positionLabels()
    {
        return [
            self::LEFT_LEG => 'LEFT',
            self::RIGHT_LEG => 'RIGHT',
        ];
    }

    public static function getPosition($position = 0)
    {
        $result = null;
        if (!$position) {
            return static::positionLabels();
        }
        $result = static::positionLabels();
        if ($position && isset($result[$position])) {
            return $result[$position];
        }
        return null;
    }


}
