<?php

namespace app\models\investments;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "invest_customer_wallet".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $balance
 * @property string $poundage
 * @property integer $type
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class InvestCustomerWallet extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 10;
    const STATUS_INACTIVE = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invest_customer_wallet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id', 'type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['balance', 'poundage'], 'number'],
            [['customer_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('investment', 'Mã'),
            'customer_id' => Yii::t('investment', 'Mã người chơi'),
            'balance' => Yii::t('investment', 'Số dư'),
            'poundage' => Yii::t('investment', 'Hoa hồng'),
            'type' => Yii::t('investment', 'Loại'),
            'status' => Yii::t('investment', 'Trạng thái'),
            'created_at' => Yii::t('investment', 'Tạo lúc'),
            'updated_at' => Yii::t('investment', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function getInvestWallet($customer_id)
    {
        //Nếu tài khoản chưa có ví -> tức là chưa đc đăng ký chơi bên invest hiện form đăng ký
        $wallet = static::findOne(['customer_id' => $customer_id]);
        if (!$wallet) {
            $wallet = new InvestCustomerWallet();
            $wallet->customer_id = $customer_id;
            $wallet->save();
        }
        return $wallet;
    }

    public function getCustomer()
    {
        return $this->hasOne(InvestCustomer::className(), ['id' => 'customer_id']);

    }

    public function getPackage()
    {
        return $this->hasOne(InvestCustomerPackage::className(), ['customer_id' => 'id'])->via('customer');
    }
}
