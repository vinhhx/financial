<?php

namespace app\models\investments;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "invest_package".
 *
 * @property integer $id
 * @property string $package_name
 * @property string $amount_min
 * @property string $amount_max
 * @property string $increase_day
 * @property string $increase_month
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $branch_full
 * @property string $branch_short
 * @property string $matching
 */
class InvestPackage extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    public static $_packages;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invest_package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['package_name', 'branch_full', 'branch_short', 'matching', 'increase_day'], 'required'],
            [['amount_min', 'amount_max', 'increase_day', 'increase_month', 'branch_full', 'branch_short', 'matching'], 'number'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['package_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('investment', 'Mã'),
            'package_name' => Yii::t('investment', 'Tên gói đầu tư'),
            'amount_min' => Yii::t('investment', 'Số BTC tối thiểu'),
            'amount_max' => Yii::t('investment', 'Số BTC tối đa'),
            'increase_day' => Yii::t('investment', 'Lãi hàng ngày'),
            'increase_month' => Yii::t('investment', 'Lãi hàng tháng'),
            'status' => Yii::t('investment', 'Trạng thái'),
            'created_at' => Yii::t('investment', 'Tạo lúc'),
            'updated_at' => Yii::t('investment', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function getStatusLabels()
    {
        return [
            static::STATUS_ACTIVE => Yii::t('investment', 'Đang hoạt động'),
            static::STATUS_INACTIVE => Yii::t('investment', 'Dừng hoạt động'),
        ];
    }

    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        if (isset($labels[$this->status])) {
            return $labels[$this->status];
        }
        return '--';
    }

    public static function getAllPackages()
    {
        if (!self::$_packages || is_null(self::$_packages)) {
            self::$_packages = self::find()->where(['status' => self::STATUS_ACTIVE])->indexBy('id')->asArray()->all();
        }
        return self::$_packages;
    }
}
