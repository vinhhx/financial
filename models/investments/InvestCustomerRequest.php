<?php

namespace app\models\investments;

use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "invest_customer_request".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $customer_username
 * @property string $amount
 * @property string $bank_address
 * @property integer $status
 * @property integer $type
 * @property string $transaction_code
 * @property string $approved_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class InvestCustomerRequest extends \yii\db\ActiveRecord
{
    const STATUS_CREATED = 1;
    const STATUS_SENT = 10;
    const STATUS_COMPLETED = 99;

    const TYPE_GET_HELP = 1; //RUT
    const TYPE_PROVIDER_HELP = 2;//NỘP
    const TYPE_LOAN_OPEN_PACKAGE = 20;//NỘP
    const TYPE_CASH_OUT = 3;//NỘP

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invest_customer_request';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'status', 'type', 'created_at', 'updated_at'], 'integer'],
            [['amount'], 'number'],
            [['bank_address'], 'required'],
            [['customer_username', 'bank_address', 'transaction_code', 'approved_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('investment', 'Mã'),
            'customer_id' => Yii::t('investment', 'Mã người chơi'),
            'customer_username' => Yii::t('investment', 'Tên đăng nhập'),
            'amount' => Yii::t('investment', 'Số BTC'),
            'bank_address' => Yii::t('investment', 'Địa chỉ ngân hàng thụ hưởng'),
            'status' => Yii::t('investment', 'Trạng thái'),
            'type' => Yii::t('investment', 'Loại'),
            'transaction_code' => Yii::t('investment', 'Phiếu xác nhận'),
            'approved_by' => Yii::t('investment', 'QTV đã xác nhận'),
            'created_at' => Yii::t('investment', 'Tạo lúc'),
            'updated_at' => Yii::t('investment', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function getStatusLabels()
    {
        return [
            static::STATUS_CREATED => Yii::t('investment', 'Chờ bổ xung phiếu xác nhận'),
            static::STATUS_SENT => Yii::t('investment', 'Đã bổ xung phiếu xác nhận'),
            static::STATUS_COMPLETED => Yii::t('investment', 'Hoàn thành'),
        ];

    }

    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        if (isset($labels[$this->status])) {
            return $labels[$this->status];
        }
        return '';

    }

    public static function getTypeLabels()
    {
        return [
            static::TYPE_PROVIDER_HELP => Yii::t('investment', 'Yêu cầu chuyển BTC'),
            static::TYPE_GET_HELP => Yii::t('investment', 'Yêu cầu nhận BTC'),
            static::TYPE_CASH_OUT => Yii::t('investment', 'Yêu cầu rút BTC'),
            static::TYPE_LOAN_OPEN_PACKAGE => Yii::t('ivestment', 'Require sent BTC open package')
        ];

    }

    public function getTypeLabel()
    {
        $labels = static::getTypeLabels();
        if (isset($labels[$this->type])) {
            return $labels[$this->type];
        }
        return '';

    }

    public function canDeleteAttachment()
    {
        if ($this->status == self::STATUS_SENT && $this->transaction_code) {
            return true;

        }
        return false;
    }

    public function removeAttachment()
    {
        if ($this->status !== self::STATUS_SENT || !$this->transaction_code) {
            return false;
        }
        $attachment = $this->transaction_code;
        $this->transaction_code = '';
        $this->status = self::STATUS_CREATED;
        if ($this->save(true, ['transaction_code', 'status', 'updated_at'])) {
            return Yii::$app->image->deleteImage($attachment);
        }

    }

    public function canUploadAttachment()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        if (Yii::$app->user->id == $this->customer_id &&
            $this->status == self::STATUS_CREATED &&
            in_array($this->type, [self::TYPE_PROVIDER_HELP, self::TYPE_LOAN_OPEN_PACKAGE])
        ) {
            return true;
        }
        return false;
    }

    public function finish()
    {
        $customer = InvestCustomer::findOne($this->customer_id);
        if (!$customer) {
            throw new NotFoundHttpException(Yii::t('messages', 'Yêu cầu không tồn tại, vui lòng kiểm tra lại.'));
        }
        switch ($this->type) {
            case self::TYPE_PROVIDER_HELP://chuyển tiên cho hệ thống
            case self::TYPE_LOAN_OPEN_PACKAGE://chuyển tiên cho hệ thống
                return $customer->finishProviderHelper($this);
                break;
            case self::TYPE_GET_HELP:
            case self::TYPE_CASH_OUT:
                return $customer->finishGetHelp($this);
                break;
        }
        return true;

    }

}
