<?php

namespace app\models\investments;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "invest_customer_transaction".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $customer_username
 * @property string $amount
 * @property integer $sign
 * @property integer $bill_id
 * @property integer $type
 * @property string $balance_before
 * @property string $balance_after
 * @property integer $created_at
 * @property integer $updated_at
 */
class InvestCustomerTransaction extends \yii\db\ActiveRecord
{

    const SIGN_ADD = 1;
    const SIGN_SUB = 2;

    const TYPE_ACTIVE_PACKAGE = 1;
    const TYPE_PH_TO_WALLET = 20;
    const TYPE_ADD_POUNDAGE = 2;
    const TYPE_UPGRADE_PACKAGE = 3;
    const TYPE_GET_HELP = 10;
    const TYPE_CASH_OUT_POUNDAGE = 11;
    const TYPE_CASH_OUT_CAPITAL = 12;
    const TYPE_CASH_OUT_PROFIT = 13;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invest_customer_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'sign', 'type', 'created_at', 'updated_at', 'bill_id'], 'integer'],
            [['amount', 'balance_before', 'balance_after'], 'number'],
            [['customer_username'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('investment', 'Mã'),
            'customer_id' => Yii::t('investment', 'Mã người chơi'),
            'customer_username' => Yii::t('investment', 'Tên đăng nhập'),
            'amount' => Yii::t('investment', 'Số BTC'),
            'sign' => Yii::t('investment', 'Giao dịch'),
            'type' => Yii::t('investment', 'Loại'),
            'balance_before' => Yii::t('investment', 'Số dư trước'),
            'balance_after' => Yii::t('app', 'Số dư sau'),
            'created_at' => Yii::t('app', 'Giao dịch lúc'),
            'updated_at' => Yii::t('app', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function getTypeLabels()
    {
        return [
            static::TYPE_GET_HELP => Yii::t('investment', 'Yêu cầu nhận BTC'),
            static::TYPE_PH_TO_WALLET => Yii::t('investment', 'Kích hoạt gói đàu tư'),
            static::TYPE_CASH_OUT_POUNDAGE => Yii::t('investment', 'Rút BTC hoa hồng'),
            static::TYPE_CASH_OUT_PROFIT => Yii::t('investment', 'Rút BTC lợi nhuận'),
            static::TYPE_ACTIVE_PACKAGE => Yii::t('investment', 'Bắt đầu tham gia đầu tư'),
            static::TYPE_ADD_POUNDAGE => Yii::t('investment', 'Nhận hoa hồng từ người chơi'),
            static::TYPE_UPGRADE_PACKAGE => Yii::t('investment', 'Nấp cấp gói đầu tư'),
            static::TYPE_CASH_OUT_CAPITAL => Yii::t('investment', 'Rút toàn bộ'),
        ];
    }

    public function getTypeLabel()
    {
        $label = static::getTypeLabels();
        if (isset($label[$this->type])) {
            return $label[$this->type];
        }
        return '';
    }

    public static function getSignLabels()
    {
        return [
            static::SIGN_ADD => ' + ',
            static::SIGN_SUB => ' - ',
        ];
    }

    public function getSignLabel()
    {
        $labels = static::getSignLabels();
        if (isset($labels[$this->sign])) {
            return $labels[$this->sign];
        }
        return '';
    }
}
