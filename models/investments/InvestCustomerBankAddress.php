<?php

namespace app\models\investments;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "invest_customer_bank_address".
 *
 * @property integer $id
 * @property string $invest_customer_id
 * @property string $bank_address
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class InvestCustomerBankAddress extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invest_customer_bank_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invest_customer_id', 'bank_address'], 'required'],
            [['invest_customer_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['bank_address'], 'string', 'max' => 255],
            [['invest_customer_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'invest_customer_id' => Yii::t('app', 'Invest Customer ID'),
            'bank_address' => Yii::t('app', 'Bank Address'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }
}
