<?php

namespace app\models\investments;

use app\models\Customer;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;

/**
 * This is the model class for table "invest_customer_package".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $package_id
 * @property string $amount
 * @property integer $status
 * @property integer $is_active
 * @property integer $type
 * @property string $last_cash_out_at
 * @property string $joined_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $last_profit_receiver
 * @property string $current_profit
 * @property string $total_current_profit
 * @property string $current_poundage
 * @property string $last_cash_out_poundage
 * @property string $strong_leg_balance;
 */
class InvestCustomerPackage extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_EMPTY = 10;

    const IS_ACTIVE_SUCCESS = 10;
    const IS_ACTIVE_PENDING = 1;

    const TYPE_RUNNING = 0;
    const TYPE_CASHED_OUT = 1;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invest_customer_package';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'package_id'], 'required'],
            [['customer_id', 'package_id', 'status', 'is_active', 'type', 'last_cash_out_at', 'joined_at', 'created_at', 'updated_at', 'last_profit_receiver', 'last_cash_out_poundage'], 'integer'],
            [['amount', 'current_profit', 'total_current_profit', 'current_poundage', 'strong_leg_balance'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('investment', 'Mã'),
            'customer_id' => Yii::t('investment', 'Mã người chơi'),
            'package_id' => Yii::t('investment', 'Mã gói đầu tư'),
            'amount' => Yii::t('investment', 'Số BTC '),
            'current_profit' => Yii::t('investment', 'Số BTC lợi nhuận '),
            'current_poundage' => Yii::t('investment', 'Số BTC hoa hồng '),
            'total_current_profit' => Yii::t('investment', 'Tổng số lợi nhuận đã kiếm được '),
            'status' => Yii::t('investment', 'Trạng thái'),
            'is_active' => Yii::t('investment', 'Tình trạng xác thực'),
            'type' => Yii::t('investment', 'Loại'),
            'last_cash_out_at' => Yii::t('investment', 'Lần cuối bạn đã rút lợi nhuận'),
            'last_profit_receiver' => Yii::t('investment', 'Lần cuối bạn đã rút lợi nhuận'),
            'last_cash_out_poundage' => Yii::t('investment', 'Lần cuối bạn đã rút hoa hồng'),
            'joined_at' => Yii::t('investment', 'Thời gian bắt đầu đầu tư'),
            'created_at' => Yii::t('investment', 'Tham gia lúc'),
            'updated_at' => Yii::t('investment', 'Cập nhật lúc'),
            'strong_leg_balance' => Yii::t('investment', 'Strong_leg_balance'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    public function getPackage()
    {
        return $this->hasOne(InvestPackage::className(), ['id' => 'package_id']);
    }

    public static function getStatusLabels()
    {
        return [
            static::STATUS_ACTIVE => Yii::t('investment', 'Đang hoạt động'),
            static::STATUS_INACTIVE => Yii::t('investment', 'Dừng hoạt động'),
        ];
    }

    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        if (isset($labels[$this->status])) {
            return $labels[$this->status];
        }
        return '';
    }


    public function canCashOutProfit()
    {
        //Chỉ đc t\rút vào thứ 2 hang tuần:
        $dayText = Yii::$app->formatter->asDate(time(), 'php:l');
        if (in_array($dayText, Yii::$app->params["DayCashOut"])) {
            if ($this->current_profit > 0 && $this->status == self::STATUS_ACTIVE && $this->is_active == self::IS_ACTIVE_SUCCESS) {
                return true;
            }
            return false;
        }
        //fix chỉ hiện thị sau  10'
    }

    public function canCashOutAllPackage()
    {
        //fix chỉ hiện thị sau  15'
        $dayText = Yii::$app->formatter->asDate(time(), 'php:l');
        if (in_array($dayText, Yii::$app->params["DayCashOut"])) {
            if ($this->status == self::STATUS_ACTIVE && $this->is_active == self::IS_ACTIVE_SUCCESS) {
                return true;
            }
            return false;
        }
    }

    public function canUpgrade()
    {
        //fix chỉ hiện thị sau  15'
        $wallet = Yii::$app->user->identity->getCurrentWallet();
        $balance = $wallet->balance;
        if (($balance > 0.1) && in_array($this->status, [self::STATUS_ACTIVE, self::STATUS_EMPTY]) && $this->is_active == self::IS_ACTIVE_SUCCESS) {
            return true;
        }
        return false;
    }

    public function canCashOutPoundage()
    {
        $dayText = Yii::$app->formatter->asDate(time(), 'php:l');
        if (in_array($dayText, Yii::$app->params["DayCashOut"])) {
            $timeDiff = time() - $this->last_cash_out_poundage; //2 ruần rút 1 lần
            if ($this->current_poundage > 0 && $this->status == self::STATUS_ACTIVE && $this->is_active == self::IS_ACTIVE_SUCCESS && $timeDiff > Yii::$app->params["InvestCashOutPoundageTime"]) {
                return true;
            }
            return false;
        }

    }

    public function hasBonusProfit()
    {
        //fix chỉ hiện thị sau  10'
        $dayText = Yii::$app->formatter->asDate($this->last_cash_out_at, 'php:l');
        if (in_array($dayText, Yii::$app->params["DayCashOut"])) {
            $timeDiff = time() - $this->last_cash_out_at;
        } else {
            $timeLastCashOut = strtotime('next monday', $this->last_cash_out_at);
            $timeDiff = time() - $timeLastCashOut;
        }
        if ($timeDiff >= Yii::$app->params["InvestCashOutBonus"] && $this->canCashOutProfit()) {
            return true;
        }
        return false;
    }

    public function hasBonusPoundage()
    {
        //fix chỉ hiện thị sau  10'
        $dayText = Yii::$app->formatter->asDate($this->last_cash_out_poundage, 'php:l');
        if (in_array($dayText, Yii::$app->params["DayCashOut"])) {
            $timeDiff = time() - $this->last_cash_out_poundage;
        } else {
            $timeLastCashOut = strtotime('next monday', $this->last_cash_out_poundage);
            $timeDiff = time() - $timeLastCashOut;
        }
        if ($timeDiff >= Yii::$app->params["InvestCashOutBonus"] && $this->canCashOutPoundage()) {
            return true;
        }
        return false;
    }


    /**
     * @param $childrenPackage //gói vào hệ thống
     * @param $params //vị trí gói đó đối với người chơi
     * @param $amount //Giá trị gói vào hệ thống
     * @param int $profit //Lợi nhuận người chơi thu đc
     * @return float|int|void
     * @throws \yii\db\Exception
     */

    public function calculateProfit($params)
    {
        $currentProfit = 0;
        //Hoa hồng trực tiếp nếu có

        if ($params['children_level'] == 1) {
            $currentProfit = $this->calcDirectCommission($params);

        }
        //hoa hồng chân yếu nếu có
        $currentProfit += $this->calcWeakBranchCommission($params);
        //Hoa hồng matching
        if ($params['children_level'] > 1) {
            $params["children_profit"] += $currentProfit;
            $currentProfit += $this->calcMatchingCommission($params);
        }
        $this->current_poundage += $currentProfit;
        if (!$this->save(true, ['current_poundage', 'updated_at'])) {
            throw new Exception('Not Update customer poundage');
        }

        return $currentProfit;
    }

    /**
     * @param $amount //Số tiền đầu tư
     * @param $billId //Gói ng chơi mới vào hệ thống
     * @param $sourceId //id người chơi mơi vào hệ thống
     * @return float|int //Lợi nhuận trực tiếp danh cho cấp cha F1
     */
    public function calcDirectCommission($params)
    {
        $profit = 0;
        //Hoa hồng trực tiếp
        $allPackages = InvestPackage::getAllPackages();
        if (isset($allPackages[$this->package_id]) && $params["children_package_amount"] > 0) {
            $percent = $allPackages[$this->package_id]["branch_full"];
            $profit = round(($params["children_package_amount"] * ($percent / 100)), 8);
            try {
                InvestCustomerPackageHistory::addAmountTransactionHistory($this->id, $profit, InvestCustomerPackageHistory::TYPE_EARNING_FROM_F1, $params["children_package_id"], $params["children_package_id"]);

            } catch (\Exception $ex) {
                throw new Exception($ex->getMessage());
            }

        }
        return $profit;

    }

    /**
     * @param $params
     * @return float|int
     * @throws \Exception
     */
    public function calcWeakBranchCommission($params)
    {
        $position = $params['children_position'];
        $amount = $params["children_package_amount"];
        $strongLegBalance = $this->strong_leg_balance;
        $allPackages = InvestPackage::getAllPackages();
        if (isset($allPackages[$this->package_id])) {
            $percent = $allPackages[$this->package_id]['branch_short'];
            $profit = 0;
            $LL = $this->getTotalPackageAmount(1, $params["children_id"]);
            $LR = $this->getTotalPackageAmount(2, $params["children_id"]);

            if ($LL == $LR || ($position == 1 && $LL > $LR) || ($position == 2 && $LL < $LR)) {
                $this->strong_leg_balance = $strongLegBalance + $amount;
            }
            if ($position == 1 && $LL < $LR || $position == 2 && $LL > $LR) {
                $FMinBefore = ($LL < $LR) ? $LL : $LR;
                $FMax = ($LL < $LR) ? $LR : $LL;
                $FMinAfter = $FMinBefore = $amount;
                $FBalance = ($LL > $LR) ? ($LL - $LR) : ($LR - $LL);
                if ($FMinAfter == $FMax) {
                    $profit = $FBalance * $percent / 100;
                    $this->strong_leg_balance = 0;
                }
                if ($FMinAfter < $FMax) {
                    $profit = $amount * $percent / 100;
                    $this->strong_leg_balance = $FMax - $FMinAfter;
                }
                if ($FMinAfter > $FMax) {
                    $profit = $FBalance * $percent / 100;
                    $this->strong_leg_balance = $FMinAfter - $FMax;
                }
            }
        }
        //Cập nhật lại giá trị bảo lưu chân mạnh
        if (!$this->save(true, ['strong_leg_balance', 'updated_at'])) {
            throw new \Exception('Not update strong_leg_balance');
        }
        InvestCustomerPackageHistory::addAmountTransactionHistory($this->package_id, $profit, InvestCustomerPackageHistory::TYPE_EARNING_FROM_PAID_LEG, $params["children_package_id"], $params["children_id"]);
        return $profit;
    }

    /**
     * Tính hoa hồng matching từ số tiền
     */
    public function calcMatchingCommission($params)
    {
        $profit = 0;
        $allPackages = InvestPackage::getAllPackages();
        if (isset($allPackages[$this->package_id])) {
            $percent = $allPackages[$this->package_id]['matching'];
            $profit = $params["children_profit"] * $percent / 100;
            InvestCustomerPackageHistory::addAmountTransactionHistory($this->id, $profit, InvestCustomerPackageHistory::TYPE_EARNING_FROM_MATCHING_F1, $params["children_package_id"], $params["children_id"]);
        }
        return $profit;

    }

    public function  getTotalPackageAmount($leg, $childrenId)
    {
        $query = new Query();
        $query->select(['SUM(icp.amount)']);
        $query->from(InvestCustomerPackage::tableName() . ' as icp');
        $query->leftJoin(InvestCustomerCase::tableName() . ' as icc', 'icp.customer_id=icc.parent_id');
        $query->where([
                'AND',
                ['!=', 'icp.status', InvestCustomerPackage::STATUS_INACTIVE],
                ['icc.parent_id' => $this->customer_id],
                ['icc.position' => $leg],
                ['not in', 'icc.customer_id', [$childrenId, (int)$this->customer_id]]
            ]
        );

        return round($query->scalar(), 8);
    }


}
