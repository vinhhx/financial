<?php

namespace app\models\investments;

use app\models\CustomerToken;
use Yii;
use yii\base\ErrorException;
use yii\base\Exception;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "invest_customer".
 *
 * @property integer $id
 * @property string $username
 * @property string $ref_code
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property integer $status
 * @property integer $is_active
 * @property integer $is_deleted
 * @property integer $parent_id
 * @property integer $package_id
 * @property string $amount
 * @property string $full_name
 * @property string $email
 * @property string $phone
 * @property integer $sex
 * @property string $dob
 * @property string $identity_card
 * @property string $nation
 * @property string $address
 * @property string $date_join
 * @property string $last_auto_increase
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_type
 * @property string $created_by
 */
class InvestCustomer extends \yii\db\ActiveRecord implements IdentityInterface
{

    const STATUS_DELETED = 2;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_LOCK = 3;
    const STATUS_CREATED_BY_MONEY_LOAN = 4;
    const STATUS_REQUIRE_ACTIVE = 5;

    const TYPE_ADMIN = 1;
    const TYPE_CUSTOMER = 2;

    const IS_ACTIVE_PENDING = 0;
    const IS_ACTIVE_COMPLETED = 1;

    const SEX_MALE = 1;
    const SEX_FEMALE = 2;
    const IS_DELETED = 1;
    const ID_ONLY_VIEW = 1;
    const IS_LOAN = 1;

    const NATION_UNITED_KINGDOM = 1;
    const NATION_UNITED_STATES = 2;
    const NATION_CANADA = 3;
    const NATION_GERMANY = 4;
    const NATION_NETHERLANDS = 5;
    const NATION_ISRAEL = 6;
    const NATION_FRANCE = 7;
    const NATION_ITALY = 8;
    const NATION_SWITZERLAND = 9;
    const NATION_HONGKONG = 10;
    const NATION_CHINA = 11;
    const NATION_SINGAPORE = 12;
    const NATION_KOREA = 13;
    const NATION_JAPAN = 14;
    const NATION_SOUTH_AFRICA = 15;
    const NATION_VIETNAM = 16;


    private $_currentWallet;
    private $_currentPackage;
    private $_isActive;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invest_customer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'ref_code', 'auth_key', 'password_hash', 'parent_id', 'full_name', 'email', 'phone'], 'required'],
            [['status', 'is_active', 'is_deleted', 'parent_id', 'package_id', 'sex', 'date_join', 'created_at', 'updated_at', 'created_type'], 'integer'],
            [['amount'], 'number'],
            [['dob', 'last_auto_increase', 'created_by'], 'safe'],
            [['username', 'password_hash', 'password_reset_token', 'full_name', 'address'], 'string', 'max' => 255],
            [['ref_code', 'auth_key'], 'string', 'max' => 32],
            [['email'], 'string', 'max' => 50],
            [['phone', 'identity_card'], 'string', 'max' => 20],
            [['nation'], 'string', 'max' => 45],
            [['date_join', 'dob'], 'app\components\validators\DateValidator'],
            [['phone'], 'app\components\validators\PhoneNumberValidator'],
            [['username'], 'unique'],
            [['ref_code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'username' => Yii::t('app', 'Username'),
            'ref_code' => Yii::t('app', 'Ref Code'),
            'auth_key' => Yii::t('app', 'Auth Key'),
            'password_hash' => Yii::t('app', 'Password Hash'),
            'password_reset_token' => Yii::t('app', 'Password Reset Token'),
            'status' => Yii::t('app', 'Status'),
            'is_active' => Yii::t('app', 'Is Active'),
            'is_deleted' => Yii::t('app', 'Is Deleted'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'package_id' => Yii::t('app', 'Package ID'),
            'amount' => Yii::t('app', 'Amount'),
            'full_name' => Yii::t('app', 'Full Name'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'sex' => Yii::t('app', 'Sex'),
            'dob' => Yii::t('app', 'Dob'),
            'identity_card' => Yii::t('app', 'Identity Card'),
            'nation' => Yii::t('app', 'Nation'),
            'address' => Yii::t('app', 'Address'),
            'date_join' => Yii::t('app', 'Date Join'),
            'last_auto_increase' => Yii::t('app', 'Last Auto Increase'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return array
     * Tự động add thời gian sau khi save
     */
    public function  behaviors()
    {

        return [
            TimestampBehavior::className(),
        ];
    }

    public function getBankAddress()
    {
        return $this->hasOne(InvestCustomerBankAddress::className(), ['invest_customer_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerCases()
    {
        return $this->hasMany(InvestCustomerCase::className(), ['customer_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|\app\models\User
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function findByRefCode($ref)
    {
        return static::findOne(['ref_code' => $ref, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['customer.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);
        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset = null;
    }

    /**
     * Get list of status labels.
     * @return array
     */
    public static function getStatusLabels()
    {
        return [
//            self:: STATUS_DELETED => 'Đã xóa',
            self:: STATUS_INACTIVE => Yii::t('customers', 'locked'),
            self:: STATUS_ACTIVE => Yii::t('customers', 'active'),
        ];
    }

    /**
     * @return int|string label of current user's status.
     */
    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();

        if (isset($labels[$this->status])) {
            return $labels[$this->status];
        }

        return $this->status;
    }

    public static function getIsActiveLabels()
    {
        return [
            self:: IS_ACTIVE_PENDING => Yii::t('customers', 'Pending'),
            self:: IS_ACTIVE_COMPLETED => Yii::t('customers', 'Completed'),
        ];
    }

    public function getIsActiveLabel()
    {
        $labels = static::getIsActiveLabels();

        if (isset($labels[$this->is_active])) {
            return $labels[$this->is_active];
        }

        return '--';
    }

    public static function getStatusTypes()
    {
        return [
            self::TYPE_ADMIN => Yii::t('customers', 'Quản trị viên'),
            self::TYPE_CUSTOMER => Yii::t('customers', 'Người chơi'),
        ];
    }

    public static function getAllCustomer($id = 0, $type = '')
    {
        $query = (new Query())
            ->select('id,CONCAT(`username`,"(",`full_name`,")") AS `fullName`')
            ->from(self::tableName());
        if ((int)$id) {
            $query->andFilterWhere(['<>', 'id', $id]);
        }
        if ($type == InvestCustomer::TYPE_ADMIN) {
            $query->andWhere(['created_type' => InvestCustomer::TYPE_ADMIN]);
        }
        $query->groupBy('id');
        return $customers = $query->all();
    }

    public static function getSexLabels()
    {
        return [
            self::SEX_MALE => Yii::t('customers', 'Nam'),
            self::SEX_FEMALE => Yii::t('customers', 'Nữ'),
        ];
    }


    /**
     * @return array|null|\yii\db\ActiveRecord
     * Lấy gói hiện tại của customer
     */
    public function getCurrentPackage()
    {
        if (!$this->_currentPackage) {
            $this->_currentPackage = $this->findPackage();
        }
        return $this->_currentPackage;
    }

    public function findPackage()
    {
        return InvestCustomerPackage::find()->where(['customer_id' => $this->id])->limit(1)->one();
    }


    public function isRegister()
    {
        $customerParent = InvestCustomerCase::find()->where(['customer_id' => $this->id])->count();
        if ($customerParent > 0) {
            return true;
        }
        return false;
    }

    public function isActive()
    {
//        $wallet = $this->getCurrentWallet();
        if ($this->is_active == self::IS_ACTIVE_COMPLETED) {
            return true;
        }
        return false;
    }


    /**
     * Function tạo mã code giới thiệu đăng ký
     */
    public function createRefCode()
    {
        $this->ref_code = 'INV' . uniqid();
    }

    public function getCurrentWallet()
    {
        if (!$this->_currentWallet) {
            $this->_currentWallet = $this->findWallet();
        }
        return $this->_currentWallet;
    }


    public function findWallet()
    {
        return InvestCustomerWallet::getInvestWallet($this->id);
    }

    public function getInvestPackage()
    {
        return $this->hasOne(InvestCustomerPackage::className(), ['customer_id' => 'id']);
    }

    public function getUrlRegisterReferal()
    {
        if ($this->ref_code) {
            return Yii::$app->urlManager->createAbsoluteUrl(['/invest/default/register', 'ref' => $this->ref_code]);
        }
        return '';
    }

    public function hasChildren()
    {
        return InvestCustomerCase::find()->where(['parent_id' => $this->id, 'level' => 1])->count();
    }

    public function getCurrentToken()
    {
        return CustomerToken::findCurrentToken($this->id, CustomerToken::TYPE_INVEST);
    }

    public function countCurrentToken()
    {
        return CustomerToken::countCurrentToken($this->id, CustomerToken::TYPE_INVEST);
    }


    /**
     * Hàm khởi tạo wallet | 1 customer chỉ có duy nhất 1 wallet
     */
    public function createInvestWallet()
    {
        if ($this->id) {
            $wallet = new InvestCustomerWallet();
            $wallet->customer_id = $this->id;
            $wallet->save();
        }
    }

    public static function getNationLabels()
    {
        return [
            self::NATION_UNITED_KINGDOM => 'United Kingdom',
            self::NATION_UNITED_STATES => 'United States',
            self::NATION_CANADA => 'Canada',
            self::NATION_GERMANY => 'Germany',
            self::NATION_NETHERLANDS => 'Netherlands',
            self::NATION_ISRAEL => 'Israel',
            self::NATION_FRANCE => 'France',
            self::NATION_ITALY => 'Italy',
            self::NATION_SWITZERLAND => 'Switzerland',
            self::NATION_HONGKONG => 'HongKong',
            self::NATION_CHINA => 'China',
            self::NATION_SINGAPORE => 'Singapore',
            self::NATION_KOREA => 'Korea',
            self::NATION_JAPAN => 'Japan',
            self::NATION_SOUTH_AFRICA => 'South Africa',
            self::NATION_VIETNAM => 'Vietnam',
        ];
    }

    public function setActive()
    {
        $token = $this->createToken();
        if ($token) {
            $this->is_active = self::IS_ACTIVE_COMPLETED;
            if ($this->save()) {
                $token->status = $token::STATUS_USED;
                if ($token->save(true, ['is_active', 'updated_at'])) {
                    return true;
                }
            }
        }
        return false;
    }

    public function createToken()
    {
        $token = new CustomerToken();
        $token->customer_id = $this->id;
        $token->token = $token::generateToken();
        $token->type = CustomerToken::TYPE_INVEST;
        if ($token->save()) {
            return $token;
        }
        return null;
    }

    public function addPoundageParent($amount, $bill_id = 0)
    {
        $customerCases = InvestCustomerCase::find()->joinWith('parent')->where(['customer_id' => $this->id])->andWhere(['<>', 'level', 0])->indexBy('parent_id')->orderBy('level')->all();
        $result = [];
        $result["status"] = "success";
        $result["code"] = 200;
        $result["message"] = "";
        $source_id = $this->id;
        if ($customerCases) {
            $packages = InvestPackage::find()->indexBy('id')->all();
//            $query = new Query();
//            $query->select(['parent_id', 'COUNT(customer_id) as member']);
//            $query->from(InvestCustomerCase::tableName());
//            $query->where([
//                'parent_id' => array_keys($customerCases),
//                'level' => 1
//            ]);
//            $query->groupBy('parent_id');
//            $query->indexBy('parent_id');
//            $childrenNumbers = $query->all();
            $level = 1;
//            $amountIncrease = $amount;
            $totalAmount = $amount;
            try {
                $percent = 0;
                foreach ($customerCases as $case) {
                    if ($level == 1 && $case->level !== 1) {
                        throw new \Exception(Yii::t('messages', 'Lỗi không đúng cấp độ con F1'));
                    }
                    $cusPack = $case->package;
                    $earningTotal = 0;
                    $cus = $case->parent;
                    $cusWallet = $cus->findWallet();
                    $pack = ($cusPack && isset($packages[$cusPack->package_id])) ? $packages[$cusPack->package_id] : null;
                    if ($cusPack && $cus && $pack && $cusWallet) {

                        $balanceBefore = $cusWallet->balance;
                        $percent = $pack->matching;
                        $percentType = InvestCustomerPackageHistory::TYPE_EARNING_FROM_MATCHING_F1;
                        $percentShort = 0;
                        if ($case->level == 1) {
                            $percentType = InvestCustomerPackageHistory::TYPE_EARNING_FROM_F1;
                            if ($case->position == InvestCustomerCase::LEFT_LEG) {
                                $percent = $pack->branch_full; // Chan đủ chỉ đc hh trực tiếp
                            } elseif ($case->position == InvestCustomerCase::RIGHT_LEG) {
                                $percent = $pack->branch_full; //chân thiếu = HHTT+ HH chân yếu
                                $percentShort = $pack->branch_short;
                            } else {
                                throw new \Exception(Yii::t('messages', 'Lỗi không chính xác số lượng đời F1'));
                            }
                        }
                        $amountIncrease = round(($totalAmount * ($percent / 100)), 8);
                        //Không lưu transaction mà lưu vao package
                        $beforeBalance = $cusPack->current_poundage;
                        $cusPack->current_poundage += $amountIncrease;

                        if ($cusPack->save(true, ['current_poundage', 'updated_at'])) {
                            $earningTotal = $amountIncrease;
                            //package Transaction:
                            $packageHistory = new InvestCustomerPackageHistory();
                            $packageHistory->customer_package_id = $cusPack->id;
                            $packageHistory->sign = InvestCustomerPackageHistory::SIGN_ADD;
                            $packageHistory->amount = $amountIncrease;
                            $packageHistory->type = $percentType;
                            $packageHistory->source_id = $source_id;
                            $packageHistory->bill_id = $bill_id;

                            if (!$packageHistory->save()) {
                                throw new \Exception(Yii::t('messages', 'Không thêm vào được lịch sử gói đầu tư'));
                            }
//                            $source_id = $cus->id;
//                            $bill_id = $packageHistory->id;
                        } else {
                        }
                        //Hoa hong chân yếu
                        if ($percentShort > 0) {
                            $amountIncrease2 = round(($totalAmount * ($percentShort / 100)), 8);
                            //Không lưu transaction mà lưu vao package
                            $beforeBalance = $cusPack->current_poundage;
                            $cusPack->current_poundage += $amountIncrease2;

                            if ($cusPack->save(true, ['current_poundage', 'updated_at'])) {
                                $earningTotal = $amountIncrease2;
                                //package Transaction:
                                $packageHistory = new InvestCustomerPackageHistory();
                                $packageHistory->customer_package_id = $cusPack->id;
                                $packageHistory->sign = InvestCustomerPackageHistory::SIGN_ADD;
                                $packageHistory->amount = $amountIncrease2;
                                $packageHistory->type = InvestCustomerPackageHistory::TYPE_EARNING_FROM_PAID_LEG;
                                $packageHistory->source_id = $source_id;
                                $packageHistory->bill_id = $bill_id;

                                if (!$packageHistory->save()) {
                                    throw new \Exception(Yii::t('messages', 'Không thêm vào được lịch sử gói đầu tư'));
                                }
                            }
                        }
                        $totalAmount = $earningTotal;
                        //Lưu thêm hoa hông chân yếu
//                        $cusWallet->balance += $amountIncrease;
//                        if ($cusWallet->save(true, ['balance', 'updated_at'])) {
//                            $cusTran = new InvestCustomerTransaction();
//                            $cusTran->customer_id = $cus->id;
//                            $cusTran->customer_username = $cus->username;
//                            $cusTran->bill_id = $case->id;
//                            $cusTran->amount = $amountIncrease;
//                            $cusTran->sign = InvestCustomerTransaction::SIGN_ADD;
//                            $cusTran->type = InvestCustomerTransaction::TYPE_ADD_POUNDAGE;
//                            $cusTran->balance_before = $balanceBefore;
//                            $cusTran->balance_after = $cusWallet->balance;
//                            if (!$cusTran->save()) {
//                                throw new \Exception("Not create transaction record.");
//                            }
//                        } else {
//                            throw new \Exception("Not save new balance to wallet.");
//                        }

                    } else {
                        $amountIncrease = 0;
                        break;
//                        throw new \Exception(Yii::t('messages', 'Lỗi dữ liệu không chính xác'));
                    }
                    $level++;
                }
                $result["status"] = "success";
                $result["code"] = 200;
                $result["message"] = "";
            } catch (\Exception $ex) {
                $result["status"] = "error";
                $result["code"] = 400;
                $result["message"] = $ex->getMessage();
            }

        }
        return $result;
    }


    /* Customer fix tạm sơ đồ chiều ngang */

    private static function getAllCustomers($id, $level = 0, $filterAdmin = 0)
    {
        $data = null;
        if ($id) {
            $query = new Query();
            $query->select('customer_id');
            $query->from('invest_customer_case');
            $query->where(['parent_id' => $id]);
//            $query->orWhere(['customer_id' => $id]);
            if ((int)$level) {
                $query->andWhere(["<=", "level", $level]);
            }
            $column = $query->column();
            $column = ArrayHelper::merge([$id], $column);
            $customer = new Query();
            $customer->select('t.id,t.username,c.parent_id,t.package_id,c.level,icp.amount');
            $customer->from('invest_customer t');
            $customer->leftJoin('invest_customer_package icp', 'icp.customer_id=t.id');
            $customer->rightJoin('invest_customer_case c', 'c.customer_id=t.id');
            $customer->where(['t.id' => $id]);
            $customer->orFilterWhere([
                    'and',
                    ['c.parent_id' => $column],
                    ['c.level' => 1]
                ]
            );
//            $customer->andFilterWhere([
//                't.status' => self::STATUS_ACTIVE,
//                'c.status' => self::STATUS_ACTIVE,
//            ]);
//            if ((int)$level) {
//                $customer->andWhere(["<", "c.level", $level]);
//            }
            $customer->andWhere(["!=", "t.is_deleted", self::IS_DELETED]);
            $customer->orderBy(['c.id' => SORT_ASC, 'c.parent_id' => SORT_ASC]);
//            $customer->indexBy('id');
            $data = $customer->all();
        } else {
//            if ((int)$level > 0) {
//                $customer = Customer::find();
//                $customer->select('t.id,t.username,c.parent_id,t.package_id');
//                $customer->from('customer t');
//                $customer->rightJoin('(SELECT customer_id, MAX(level) AS level FROM `invest_customer_case` WHERE STATUS=1 GROUP BY customer_id  ORDER BY MAX(level) DESC ) AS c', 'c.customer_id=t.id');
//                if ($filterAdmin) {
//                    $customer->andWhere(['t.user_id_first_branch' => $filterAdmin]);
//                }
//                $customer->andFilterWhere([
//                    't.status' => self::STATUS_ACTIVE,
//                ]);
//                $customer->andWhere(["<", "c.level", $level]);
//                $customer->andWhere(["!=", "t.is_deleted", self::IS_DELETED]);
//                $customer->orderBy('t.parent_id');
//                $data = $customer->asArray()->all();
//            } else {
//                $query = Customer::find();
//                $query->select('id,username,parent_id,package_id');
//                $query->where(['status' => self::STATUS_ACTIVE]);
//                $query->andWhere(["!=", "is_deleted", self::IS_DELETED]);
//                if ($filterAdmin) {
//                    $query->andWhere(['user_id_first_branch' => $filterAdmin]);
//                }
//                $query->orderBy(['parent_id' => SORT_ASC])->asArray();
//                $data = $query->all();
//            }
        }

        return $data;
    }

    public static function getTreeCustomer($id = 0, $level = 0, $filterAdmin = 0)
    {
        $data = [];
        $customers = static::getAllCustomers($id, $level, $filterAdmin);
        $data = [];
        foreach ($customers as $cus) {
            if (!isset($data[$cus['id']])) {
                $data[$cus['id']] = $cus;
            }
            if ($cus['parent_id'] >= 0) {

                $data[(int)$cus["parent_id"]]["childrens"][] = $cus["id"];
            }
        }
        return $data;
    }


    public static function showTree($id = 0, $level = 0, $click = true, $filterAdmin = 0)
    {
        $result = [];
        $data = static::getTreeCustomer($id, $level, $filterAdmin);
        if (!empty($data)) {

            $query = (new Query());
            $query->select('COUNT(id) as children, parent_id');
            $query->from('invest_customer_case');
            $query->where(['parent_id' => array_keys($data)]);
            $query->andFilterWhere(['<>', 'parent_id', 0]);
            $query->groupBy('parent_id');
            $query->indexBy('parent_id');
            $childrens = $query->all();
            if ($id > 0) {
                $result[1] = $data[$id];
                if (isset($childrens[$id])) {
                    $result[1]["total_children"] = (int)$childrens[$id]["children"];
                }
                $c1 = $data[$id];
                if (isset($c1["childrens"]) && count($c1["childrens"])) {
                    $child1 = $c1["childrens"];
                    $result[2][1] = (isset($child1[0]) && isset($data[$child1[0]])) ? $data[$child1[0]] : [];
                    if (isset($child1[0]) && isset($childrens[$child1[0]])) {
                        $result[2][1]["total_children"] = (int)$childrens[$child1[0]]["children"];
                    }
                    $result[2][2] = (isset($child1[1]) && isset($data[$child1[1]])) ? $data[$child1[1]] : [];
                    if (isset($child1[1]) && isset($childrens[$child1[1]])) {
                        $result[2][2]["total_children"] = (int)$childrens[$child1[1]]["children"];
                    }
                } else {
                    $result[2][11] = [];
                    $result[2][12] = [];
                }
                foreach ($result[2] as $key => $value) {
                    if (isset($value["childrens"]) && count($value["childrens"])) {
                        $child2 = $value["childrens"];
                        $result[3][($key * 10) + 1] = (isset($child2[0]) && isset($data[$child2[0]])) ? $data[$child2[0]] : [];
                        if (isset($child2[0]) && isset($childrens[$child2[0]])) {
                            $result[3][($key * 10) + 1]["total_children"] = (int)$childrens[$child2[0]]["children"];
                        }
                        $result[3][($key * 10) + 2] = (isset($child2[1]) && isset($data[$child2[1]])) ? $data[$child2[1]] : [];
                        if (isset($child2[1]) && isset($childrens[$child2[1]])) {
                            $result[3][($key * 10) + 2]["total_children"] = (int)$childrens[$child2[1]]["children"];
                        }

                    } else {
                        $result[3][($key * 10) + 1] = [];
                        $result[3][($key * 10) + 2] = [];

                    }
                }

                foreach ($result[3] as $key => $value) {
                    if (isset($value["childrens"]) && count($value["childrens"])) {
                        $child3 = $value["childrens"];
                        $result[4][($key * 10) + 1] = (isset($child3[0]) && isset($data[$child3[0]])) ? $data[$child3[0]] : [];
                        if (isset($child3[0]) && isset($childrens[$child3[0]])) {
                            $result[4][($key * 10) + 1]["total_children"] = (int)$childrens[$child3[0]]["children"];
                        }
                        $result[4][($key * 10) + 2] = (isset($child3[1]) && isset($data[$child3[1]])) ? $data[$child3[1]] : [];
                        if (isset($child3[1]) && isset($childrens[$child3[0]])) {
                            $result[4][($key * 10) + 2]["total_children"] = (int)$childrens[$child3[0]]["children"];
                        }

                    } else {
                        $result[4][($key * 10) + 1] = [];
                        $result[4][($key * 10) + 2] = [];

                    }
                }
//                foreach ($result[4] as $key => $value) {
//                    if (isset($value["childrens"]) && count($value["childrens"])) {
//                        $child4 = $value["childrens"];
//                        $result[5][($key * 10) + 1] = (isset($child4[0]) && isset($data[$child4[0]])) ? $data[$child4[0]] : [];
//                        if (isset($child4[0]) && isset($childrens[$child4[0]])) {
//                            $result[5][($key * 10) + 1]["total_children"] = (int)$childrens[$child4[0]]["children"];
//                        }
//                        $result[5][($key * 10) + 2] = (isset($child4[1]) && isset($data[$child4[1]])) ? $data[$child4[1]] : [];
//                        if (isset($child4[1]) && isset($childrens[$child4[1]])) {
//                            $result[5][($key * 10) + 1]["total_children"] = (int)$childrens[$child4[1]]["children"];
//                        }
//
//                    } else {
//                        $result[5][($key * 10) + 1] = [];
//                        $result[5][($key * 10) + 2] = [];
//
//                    }
//                }

            }
        }

        return $result;
    }


    public static function getShowCustomer($id)
    {
        $html = '';
        $data = self::getTreeCustomer($id);
        if (!empty($data)) {
            $package = InvestPackage::find();
            $package->select('id,package_name');
            $package->andFilterWhere([
                'status' => self::STATUS_ACTIVE
            ]);
            $packages = $package->asArray()->all();
            if ($packages) {
                foreach ($packages as $item) {
                    $data['package'][$item['id']] = $item['package_name'];
                }
            }
            $html = self::renderTree($data, $id);
        }
        return $html;
    }

    public static function renderHtml($data, $id, $click = true, $level = 4)
    {
        $html = '';
        if (isset($data[$id])) {
            $c = $data[$id];
            $hasChildren = (isset($c['childrens']) && count($c['childrens'])) ? true : false;
            $name = $c['username'];
            $amount = ($c['amount']) ? Yii::$app->formatter->asDecimal($c['amount'], 8) : '';
            //show cha
            $html .= '<tr>';
            $html .= '<td colspan="2" width="100%" style="text-align:center;">';
            $html .= '<a class="tree_user" href="javascript:void(0)"><img alt="test1" class="i_user" src="/images/flags/i_user.png"><br>' . $name . '<br>[' . $amount . ']</a>';
            $html .= '</td>';
            $html .= '</tr>';
            $html .= '<tr>';

            $html .= '</tr>';
        }


        return $html;
    }


    public static function renderTree($data, $id, $level = 0, $click = true)
    {
        $html = '';
        if ($id) {
            $html .= '<table id="btree" class="btree" cellspacing="0" cellpadding="0">';
            $html .= '<tbody>';
            if ($id != Yii::$app->user->identity->id) {
                $html .= '<tr>';

                $html .= '<td colspan="4" width="100%" style="text-align:center; padding-bottom: 10px;">';
                $html .= '<a href="javascript:;"><img src="/images/icon-member/arrow-t-d.png" border="0"></a>';
                $html .= '<a href="javascript:;"><img src="/images/icon-member/arrow-u-d.png" border="0" style="margin-top: 11px; margin-left: 5px;"></a>';
                $html .= '</td>';
                $html .= '</tr>';
            }
            $html .= static::renderHtml($data, $id, $click);
            $html .= '</tbody>';
            $html .= '</table>';
        } else {
            //dùng cho admin nếu có
            if (isset($data[$id]) && $data[$id]) {
                $c = $data[$id];
                if (isset($c['childrens']) && count($c['childrens'])) {
                    $html .= '<ul>';
                    foreach ($c['childrens'] as $cid) {
                        $html .= self::renderHtml($data, $cid, $click);
                    }
                    $html .= '</ul>';
                }
            }
        }
        return $html;
    }


    public function searchNewPackageHistory($limit = 10)
    {
        $customerPackage = $this->getCurrentPackage();
        if (!$customerPackage) {
            return null;
        }
        $packageHistory = InvestCustomerPackageHistory::find()
            ->where(['customer_package_id' => $customerPackage->id])
            ->orderBy(['id' => SORT_DESC])
            ->limit($limit)->all();
        return $packageHistory;
    }

    public function searchNewTransaction($limit = 10)
    {
        $myTransaction = InvestCustomerTransaction::find()
            ->where(['customer_id' => $this->id])
            ->orderBy(['id' => SORT_DESC])
            ->limit($limit)->all();
        return $myTransaction;
    }

    public function getParent()
    {
        $customerCase = InvestCustomerCase::find()->where([
            'customer_id' => Yii::$app->user->id,
            'level' => 1
        ])->limit(1)->one();
        if ($customerCase) {
            return InvestCustomer::findOne(['id' => $customerCase->parent_id]);
        }
        return null;
    }

    public function addInvestCustomerCase()
    {
        $result = [];
        if ($this->id) {
            //Nếu là nhánh cấp 1 thì nó chính là con của chính nó
            try {
                if ($this->parent_id == 0) {
                    $customerCases = new InvestCustomerCase();
                    $customerCases->customer_id = $this->id;
                    $customerCases->parent_id = $this->parent_id;
                    $customerCases->level = 0;
//                    $customerCases->customer_username = $this->username;
                    $customerCases->status = InvestCustomerCase::STATUS_ACTIVE;
                    if ($customerCases->save()) {
                        $result["status"] = "success";
                        $result["code"] = 200;
                        $result["message"] = Yii::t('messages', "Thêm tài khoản giới thiệu thành công");
                        return $result;
                    }
                } elseif ($this->parent_id > 0) {
                    //Lấy danh sách khách hàng là cha của người giới thiệu
                    $customerCases = (new Query())
                        ->select(['id', 'parent_id', 'position', 'status', 'level'])
                        ->from(InvestCustomerCase::tableName())
                        ->where(['customer_id' => $this->parent_id])
                        ->andFilterWhere(['>', 'level', 0])
                        ->all();
                    $data = [];
                    //Add cha nó là lv1
                    $currentTime = time();
                    //Tính xem nó là chân trái hay phải
                    $numberChildren = (new Query())
                        ->select(['count(customer_id)'])
                        ->from(InvestCustomerCase::tableName())
                        ->where(['parent_id' => $this->parent_id, 'level' => 1])
                        ->scalar();
                    $position = ($numberChildren) + 1;
                    $data[] = [
                        $this->id,
                        $this->parent_id,
                        1,
                        $position,
                        InvestCustomerCase::STATUS_ACTIVE,
                        $currentTime,
                        $currentTime
                    ];
                    if ($customerCases) {
                        $dataActivity = [];//Sau làm thêm chức năng thông báo tài khoản con đc thêm mới
                        foreach ($customerCases as $item) {
                            $data[] = [
                                $this->id,
                                $item["parent_id"],
                                $item["level"] + 1,
                                $item["position"],
                                $item["status"],
                                $currentTime,
                                $currentTime
                            ];
                        }

                    }
                    if (!empty($data)) {
                        Yii::$app->db->createCommand()->batchInsert(InvestCustomerCase::tableName(), [
                            'customer_id',
                            'parent_id',
                            'level',
                            'position',
                            'status',
                            'created_at',
                            'updated_at',
                        ], $data)->execute();


                        $result["status"] = "success";
                        $result["code"] = 200;
                        $result["message"] = Yii::t('messages', "Thêm tài khoản giới thiệu thành công");
                        return $result;
                    } else {
                        $result["status"] = "error";
                        $result["code"] = 110;
                        $result["message"] = Yii::t('messages', "Thêm tài khoản giới thiệu có lỗi");
                        return $result;
                    }
                } else {
                    $result["status"] = "error";
                    $result["code"] = 110;
                    $result["message"] = Yii::t('messages', "Thêm tài khoản giới thiệu có lỗi");
                    return $result;
                }
            } catch (\Exception $ex) {
                $result["status"] = "error";
                $result["code"] = $ex->getCode();
                $result["message"] = $ex->getMessage();
                return $result;
            }

        }
        return $result;
    }

    public function statisticBranch()
    {
        $cusPackage = $this->getCurrentPackage();
        //Lấy các nhánh F1
        $result = [
            "member_left" => 0,
            "member_right" => 0,
            "poundage_left" => 0,
            "poundage_right" => 0,
        ];
        $f1 = InvestCustomerCase::find()->where(['parent_id' => $this->id, 'level' => 1])->indexBy('customer_id')->all();
        if ($f1 && $cusPackage) {
            $queryMember = new Query();
            $queryMember->select(['parent_id', 'COUNT(customer_id) as total_member']);
            $queryMember->from(InvestCustomerCase::tableName());
            $queryMember->where(['parent_id' => array_keys($f1)]);
            $queryMember->groupBy('parent_id');
            $queryMember->indexBy('parent_id');
            $members = $queryMember->all();

            if ($cusPackage) {
                $queryPoundage = new Query();
                $queryPoundage->select(['source_id', 'SUM(amount) as total_poundage']);
                $queryPoundage->from(InvestCustomerPackageHistory::tableName());
                $queryPoundage->where([
                    'customer_package_id' => $cusPackage->id,
                    'source_id' => array_keys($f1),
                    'sign' => InvestCustomerPackageHistory::SIGN_ADD,
                    'type' => InvestCustomerPackageHistory::TYPE_POUNDAGE_PACKAGE,
                ]);
                $queryPoundage->groupBy('source_id');
                $queryPoundage->indexBy('source_id');
                $profits = $queryPoundage->all();
            }
            foreach ($f1 as $item) {
                if ($item->position == 1) {
                    $result["member_left"] = isset($members[$item->customer_id]["total_member"]) ? $members[$item->customer_id]["total_member"] + 1 : 1;
                    $result["poundage_left"] = isset($profits[$item->customer_id]["total_poundage"]) ? $profits[$item->customer_id]["total_poundage"] : 0;
                } elseif ($item->position == 2) {
                    $result["member_right"] = isset($members[$item->customer_id]["total_member"]) ? $members[$item->customer_id]["total_member"] + 1 : 1;
                    $result["poundage_right"] = isset($profits[$item->customer_id]["total_poundage"]) ? $profits[$item->customer_id]["total_poundage"] : 0;
                }
            }

        }
        return $result;
    }

    public function canWithDrawn()
    {
        $model = InvestCustomerRequest::find()->where([
            'type' => [InvestCustomerRequest::TYPE_GET_HELP, InvestCustomerRequest::TYPE_CASH_OUT],
            'customer_id' => $this->id,
        ])->orderBy(['id' => SORT_DESC])->limit(1)->one();
        if (!$model) {
            return true;
        } else {
            if ($model->status == InvestCustomerRequest::STATUS_COMPLETED) {
                $timeDiff = (int)(time() - $model->updated_at);
                if ($timeDiff > Yii::$app->params["POUNDAGE_TIME_DELAY_CASH_OUT"]) {
                    //Nếu lớn hơn 3h thì đc rút
                    return true;
                }
            }
        }
        return false;
    }

    public function deleteCase()
    {
        if ($this->id) {
            Yii::$app->db->createCommand()->delete(InvestCustomerCase::tableName(), ['customer_id' => $this->id])->execute();
        }
    }

    public static function getName($id)
    {
        if ($id) {
            $model = static::findOne($id);
            return $model ? $model->username : '';
        }
        return '';
    }

    public function calculatePoundage()
    {
        //gói của customer
        $package = $this->getCurrentPackage();
        $currentAmount = $package->amount; //Số tiền nhập vào ban đầu
        $currentProfit = 0;
        //Lấy danh sách case cấp cha cho root
        $cases = InvestCustomerCase::find()
//            ->joinWith('parent')
            ->where(['customer_id' => $this->id])
            ->andWhere(['<>', 'level', 0])
            ->indexBy('parent_id')
            ->orderBy('level')
            ->all();
//        $transaction = Yii::$app->db->beginTransaction();
        if (!empty($cases)) {
            foreach ($cases as $branch) {
                $fxPackage = InvestCustomerPackage::find()
                    ->where([
                        'customer_id' => $branch->parent_id,
                        'is_active' => InvestCustomerPackage::IS_ACTIVE_SUCCESS,
                        'status' => InvestPackage::STATUS_ACTIVE
                    ])
                    ->limit(1)
                    ->one();
                if ($fxPackage) { //Thằng cha thỏa mãn đk
                    //Tính hh trực tiếp
                    $params = [
                        'children_id' => $this->id,
                        'children_package_id' => $package->id,
                        'children_package_amount' => $currentAmount,
                        'children_level' => $branch->level,
                        'children_position' => $branch->position,
                        'children_profit' => $currentProfit,
                        'parent_id' => $branch->parent_id,
                    ];
                    $currentProfit = $fxPackage->calculateProfit($params);
                    //hoa hồng trực tiếp
                }
            }
        }

    }


    public function finishProviderHelper($request)
    {
        switch ($request->status) {
            case InvestCustomerRequest::STATUS_SENT:
                $request->status = InvestCustomerRequest::STATUS_COMPLETED;
                $request->approved_by = Yii::$app->user->identity->username;
                $customerWallet = $this->getCurrentWallet();
                if (!$customerWallet) {
                    throw new Exception("Not found customer's wallet");
                }
                if ($request->save(true, ['status', 'updated_at', 'approved_by'])) {

                    //Cộng tiên vào ví người chơi
                    $balanceBefore = $customerWallet->balance;
                    $totalAmount = ($request->amount);
                    $customerWallet->balance += $totalAmount;
                    if ($customerWallet->save(true, ['balance', 'updated_at'])) {
                        $billId = 0;
                        //Lưu transaction
                        $customerTransaction = new InvestCustomerTransaction();
                        $customerTransaction->customer_id = $this->id;
                        $customerTransaction->customer_username = $this->username;
                        $customerTransaction->bill_id = $request->id;
                        $customerTransaction->amount = $totalAmount;
                        $customerTransaction->sign = InvestCustomerTransaction::SIGN_ADD;
                        $customerTransaction->type = InvestCustomerTransaction::TYPE_PH_TO_WALLET;
                        $customerTransaction->balance_before = $balanceBefore;
                        $customerTransaction->balance_after = $customerWallet->balance;
                        if (!$customerTransaction->save()) {
                            throw new Exception(Yii::t('messages', 'Giao dịch không thể hoàn thành'));
                        }
                        //Nếu là dạng cho vay thì sẽ trừ tiền từ ví người chơi -> luu vào package_history
                        if ($request->type == InvestCustomerRequest::TYPE_LOAN_OPEN_PACKAGE) {
                            //kich hoạt gói
                            $customerPackage = $this->getCurrentPackage();
                            if (!$customerPackage) {
                                throw new Exception(Yii::t('messages', 'Gói đầu tư không tồn tại, Vui lòng kiểm tra lại'));
                            }
                            $customerPackage->is_active = InvestCustomerPackage::IS_ACTIVE_SUCCESS;
                            if (!$customerPackage->save(true, ['is_active', 'updated_at'])) {
                                throw new Exception(Yii::t('messages', 'Gói đầu tư chưa được xác thực, Vui lòng xác thực gói đầu tư trước.'));
                            }
                            //Trừ tiền
                            $balanceBefore = $customerWallet->balance;
                            $customerWallet->balance -= $customerPackage->amount;
                            if (!$customerWallet->save(true, ['balance', 'updated_at'])) {
                                throw new Exception(Yii::t('messages', 'Không cập nhật được số dư tài khoản'));
                            }
                            //Lưu transaction
                            $customerTransaction = new InvestCustomerTransaction();
                            $customerTransaction->customer_id = $this->id;
                            $customerTransaction->customer_username = $this->username;
                            $customerTransaction->bill_id = $customerPackage->id;
                            $customerTransaction->amount = $customerPackage->amount;
                            $customerTransaction->sign = InvestCustomerTransaction::SIGN_SUB;
                            $customerTransaction->type = InvestCustomerTransaction::TYPE_ACTIVE_PACKAGE;
                            $customerTransaction->balance_before = $balanceBefore;
                            $customerTransaction->balance_after = $customerWallet->balance;
                            if (!$customerTransaction->save()) {
                                throw new Exception(Yii::t('messages', 'Giao dịch không thể hoàn thành'));
                            }
                            $packageHistory = new InvestCustomerPackageHistory();
                            $packageHistory->customer_package_id = $customerPackage->id;
                            $packageHistory->sign = InvestCustomerPackageHistory::SIGN_ADD;
                            $packageHistory->amount = $customerPackage->amount;
                            $packageHistory->type = InvestCustomerPackageHistory::TYPE_ACTIVE_PACKAGE;
                            $packageHistory->bill_id = $customerTransaction->id;
                            if (!$packageHistory->save()) {

                                throw new Exception(Yii::t('messages', 'Lịch sử gói đầu tư không  thể cập nhật'));
                            }
                            $billId = $packageHistory->id;
                            //Cập nhật lại thời gian đc nhận tiền lãi
                            $customerPackage->last_cash_out_at = time();
                            $customerPackage->last_profit_receiver = time();
                            $customerPackage->last_cash_out_poundage = time();
                            if (!$customerPackage->save(true, ['last_cash_out_at', 'last_profit_receiver', 'last_cash_out_poundage', 'updated_at'])) {
                                throw new Exception(Yii::t('messages', 'Không cập nhật được thời gian active tài khoản'));
                            }

                            $this->calculatePoundage();

                        }
                    } else {
                        throw new Exception(Yii::t('messages', 'Không cập nhật số dư tài khoản'));
                    }

                } else {
                    throw new Exception(Yii::t('messages', 'Không cập nhật số dư tài khoản'));

                }
                break;
            default:
                break;
        }

    }

    public function finishGetHelp($request)
    {
        $customerWallet = $this->getCurrentWallet();
        //check số dư có đủ rút ko
        if ($customerWallet->balance >= $this->amount) {
            $balanceBefore = $customerWallet->balance;
            $customerWallet->balance -= $request->amount;
            if ($customerWallet->save(true, ['balance', 'updated_at'])) {
                //Lưu transaction
                $customerTransaction = new InvestCustomerTransaction();
                $customerTransaction->customer_id = $this->id;
                $customerTransaction->customer_username = $this->username;
                $customerTransaction->bill_id = $request->id;
                $customerTransaction->amount = $request->amount;
                $customerTransaction->sign = InvestCustomerTransaction::SIGN_SUB;
                $customerTransaction->type = InvestCustomerTransaction::TYPE_GET_HELP;
                $customerTransaction->balance_before = $balanceBefore;
                $customerTransaction->balance_after = $customerWallet->balance;
                if (!$customerTransaction->save()) {
                    throw new Exception(Yii::t('messages', 'Giao dịch không thể hoàn thành.'));
                }
            } else {
                throw new Exception(Yii::t('messages', 'Không cập nhật số dư cho tài khoản được.'));
            }
        } else {
            throw new Exception(Yii::t('messages', "Số dư không đủ để thực hiện giao dịch."));
        }
    }

}
