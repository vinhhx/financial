<?php

namespace app\models\investments;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "invest_customer_package_history".
 *
 * @property integer $id
 * @property string $customer_package_id
 * @property integer $sign
 * @property string $amount
 * @property integer $source_id
 * @property integer $bill_id
 * @property integer $type
 * @property string $created_at
 * @property string $updated_at
 */
class InvestCustomerPackageHistory extends \yii\db\ActiveRecord
{
    const SIGN_ADD = 1;
    const SIGN_SUB = 2;

    const TYPE_OPEN_PACKAGE = 1;
    const TYPE_ACTIVE_PACKAGE = 10;
    const TYPE_CAPITAL_PACKAGE = 20;
    const TYPE_PROFIT_PACKAGE = 30;
    const TYPE_UPGRADE = 40;
    const TYPE_WITHDRAW = 50;
    const TYPE_POUNDAGE_PACKAGE = 60;
    const TYPE_EARNING_FROM_F1 = 61;
    const TYPE_EARNING_FROM_PAID_LEG = 62;
    const TYPE_EARNING_FROM_MATCHING_F1 = 63;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invest_customer_package_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_package_id', 'amount'], 'required'],
            [['customer_package_id', 'sign', 'type', 'created_at', 'updated_at', 'source_id', 'bill_id'], 'integer'],
            [['amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('investment', 'Mã'),
            'customer_package_id' => Yii::t('investment', 'Mã gói khách hàng đã đầu tư'),
            'sign' => Yii::t('investment', 'Kí hiệu dấu'),
            'amount' => Yii::t('investment', 'Số BTC'),
            'source_id' => Yii::t('investment', 'Nguồn thu'),
            'type' => Yii::t('investment', 'Loại'),
            'created_at' => Yii::t('investment', 'Tạo lúc'),
            'updated_at' => Yii::t('investment', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function getTypeLabels()
    {
        return [
            static::TYPE_OPEN_PACKAGE => Yii::t('investment', 'Gói đầu tư đã được mở'),
            static::TYPE_ACTIVE_PACKAGE => Yii::t('investment', 'Gói đầu tư đã được kích hoạt'),
            static::TYPE_CAPITAL_PACKAGE => Yii::t('investment', 'BTC vốn'),
            static::TYPE_PROFIT_PACKAGE => Yii::t('investment', 'Lợi nhuận từ gói đầu tư'),
            static::TYPE_UPGRADE => Yii::t('investment', 'Nâng cấp gói đầu tư'),
            static::TYPE_WITHDRAW => Yii::t('investment', 'Rút BTC từ gói đầu tư'),
            static::TYPE_POUNDAGE_PACKAGE => Yii::t('investment', 'Lợi nhuận từ người chơi'),
            static::TYPE_EARNING_FROM_F1 => Yii::t('investment', 'Profit earning direct from F1'),
            static::TYPE_EARNING_FROM_MATCHING_F1 => Yii::t('investment', 'Profit earning from matching direct F1 '),
            static::TYPE_EARNING_FROM_PAID_LEG => Yii::t('investment', 'Profit earning from paid leg '),
        ];
    }


    public function getTypeLabel()
    {
        $label = static::getTypeLabels();
        if (isset($label[$this->type])) {
            return $label[$this->type];
        }
        return '';
    }

    public static function getSignLabels()
    {
        return [
            static::SIGN_ADD => ' + ',
            static::SIGN_SUB => ' - ',
        ];
    }

    public function getSignLabel()
    {
        $labels = static::getSignLabels();
        if (isset($labels[$this->sign])) {
            return $labels[$this->sign];
        }
        return '';
    }

    public function getInvestPackage()
    {
        return $this->hasOne(InvestCustomerPackage::className(), ['id' => 'customer_package_id']);
    }

    public function getInvestment()
    {
        return $this->hasOne(InvestCustomer::className(), ['id' => 'customer_id'])->via('investPackage');
    }

    /**
     * @param $customer_packageId
     * @param $amount
     * @param $type
     * @param $sign
     * @param int $billId
     * @param int $source_id
     * @return bool
     * @throws \Exception
     */

    public static function createHistory($customer_packageId, $amount, $type, $sign, $billId = 0, $source_id = 0)
    {
        $history = new self();
        $history->customer_package_id = $customer_packageId;
        $history->amount = $amount;
        $history->bill_id = $billId;
        $history->source_id = $source_id;
        $history->type = $type;
        $history->sign = $sign;
        try {
            if (!$history->save()) {
                throw new \Exception('Not save history transaction');
            }
        } catch (\Exception $ex) {
            throw new \Exception($ex->getMessage());
        }

    }

    public static function addAmountTransactionHistory($customer_packageId, $amount, $type, $billId = 0, $source_id = 0)
    {
        return static::createHistory($customer_packageId, $amount, $type, self::SIGN_ADD, $billId, $source_id);

    }

    public static function subAmountTransactionHistory($customer_packageId, $amount, $type, $billId = 0, $source_id = 0)
    {
        return static::createHistory($customer_packageId, $amount, $type, self::SIGN_SUB, $billId, $source_id);

    }
}
