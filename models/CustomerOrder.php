<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customer_order".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $customer_username
 * @property string $quantity
 * @property integer $status
 * @property integer $type
 * @property string $attachment
 * @property string $upload_attachment_at
 * @property string $approved_by
 * @property string $approved_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $type_module
 */
class CustomerOrder extends \yii\db\ActiveRecord
{
    const STATUS_PENDING = 0;
    const STATUS_PAID = 1;
    const STATUS_COMPLETED = 10;

    const TYPE_BUY_TOKEN = 1;
    const TYPE_EXCHANGE_TOKEN = 2;

    const TYPE_MODULE_CUSTOMER = 1;
    const TYPE_MODULE_INVEST = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'customer_username', 'quantity'], 'required'],
            [['customer_id', 'quantity', 'status', 'type', 'upload_attachment_at', 'approved_by', 'approved_at', 'created_at', 'updated_at', 'type_module'], 'integer'],
            [['customer_username', 'attachment'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'customer_id' => Yii::t('app', 'Mã người chơi'),
            'customer_username' => Yii::t('customers', 'Người chơi'),
            'quantity' => Yii::t('customers', 'Số lượng'),
            'status' => Yii::t('customers', 'Trạng thái'),
            'type' => Yii::t('customers', 'Loại'),
            'attachment' => Yii::t('customers', 'File đính kèm'),
            'upload_attachment_at' => Yii::t('customers', 'Upload file lúc'),
            'approved_by' => Yii::t('customers', 'Xác thực bởi'),
            'approved_at' => Yii::t('customers', 'Xác thực lúc'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
        ];
    }

    public function canUploadAttachment()
    {
        if ($this->status == self::STATUS_PENDING && !$this->attachment && $this->type == self::TYPE_BUY_TOKEN) {
            return true;
        }
        return false;
    }

    public function removeAttachment()
    {
        if ($this->status !== self::STATUS_PAID || !$this->attachment) {
            return false;
        }
        $attachment = $this->attachment;
        $this->attachment = '';
        $this->status = self::STATUS_PENDING;
        if ($this->save(true, ['attachment', 'status', 'updated_at'])) {
            return Yii::$app->image->deleteImage($attachment);
        }

    }

    public function canDeleteImage()
    {
        if ($this->status === self::STATUS_PAID && $this->attachment && $this->type == self::TYPE_BUY_TOKEN) {
            return true;
        }
        return false;
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function getStatusLabels()
    {
        return [
            static::STATUS_PENDING => Yii::t('customers', 'Chờ thanh toán'),
            static::STATUS_PAID => Yii::t('customers', 'Đã thanh toán'),
            static::STATUS_COMPLETED => Yii::t('customers', 'Hoàn thành'),
        ];
    }

    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        if (isset($labels[$this->status])) {
            return $labels[$this->status];
        }
        return '';
    }

    public static function getTypeLabels()
    {
        return [
            static::TYPE_BUY_TOKEN => Yii::t('customers', 'Mua token'),
            static::TYPE_EXCHANGE_TOKEN => Yii::t('customers', 'Sent token'),
        ];
    }

    public function getTypeLabel()
    {
        $labels = static::getTypeLabels();
        if (isset($labels[$this->type])) {
            return $labels[$this->type];
        }
        return '';
    }

    public static function getTypeModuleLabels()
    {
        return [
            static::TYPE_MODULE_CUSTOMER => Yii::t('customers', 'Member'),
            static::TYPE_MODULE_INVEST => Yii::t('customers', 'Investor'),
        ];
    }

    public function getTypeModuleLabel()
    {
        $labels = static::getTypeModuleLabels();
        if (isset($labels[$this->type_module])) {
            return $labels[$this->type_module];
        }
        return '';
    }


}
