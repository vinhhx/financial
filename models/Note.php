<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "note".
 *
 * @property integer $id
 * @property integer $object_id
 * @property string $object_table
 * @property string $content
 * @property integer $type
 * @property string $created_id
 * @property string $created_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class Note extends \yii\db\ActiveRecord
{

    const TYPE_CUSTOMER = 1; //Type Customer
    const TYPE_ADMIN = 2; // Type Admin
    //const TYPE_INVEST = 3; //Type Invest

    /**
     * @inheritdoc
     */

    public static function tableName()
    {
        return 'note';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_id', 'type', 'created_id', 'created_at', 'updated_at'], 'integer'],
            [['content'], 'string'],
            [['content', 'object_id', 'object_table'], 'required', 'on' => 'addnote'],
            [['object_table'], 'string', 'max' => 100],
            [['created_by'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','Mã'),
            'object_id' => Yii::t('app','Mã bản ghi'),
            'object_table' => Yii::t('app','Bảng dữ liệu'),
            'content' => Yii::t('app','Nội dung'),
            'type' => Yii::t('app','Loại'),
            'created_id' => Yii::t('app','Mã người tạo'),
            'created_by' =>Yii::t('app','Người tạo'),
            'created_at' => Yii::t('app','Tạo lúc'),
            'updated_at' => Yii::t('app','Cập nhật lúc'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    public static function getTypeLabels()
    {
        return [
            self::TYPE_ADMIN => 'Admin',
            self::TYPE_CUSTOMER => 'Customer',
            //self::TYPE_INVEST => 'Invest',
        ];
    }

}
