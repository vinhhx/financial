<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "system_email_queue".
 *
 * @property integer $id
 * @property string $to_email
 * @property string $subject
 * @property string $template
 * @property string $params
 * @property integer $result_code
 * @property string $result_message
 * @property integer $type
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class SystemEmailQueue extends \yii\db\ActiveRecord
{
    const STATUS_PENDING = 1;
    const STATUS_SENT = 2;
    const STATUS_ERROR = 3;

    const TYPE_CUSTOMER_REGISTERED = 10;
    const TYPE_INVEST_REGISTERED = 11;

    const TYPE_CUSTOMER_OPEN_PACKAGE = 20;
    const TYPE_INVEST_OPEN_PACKAGE = 21;
    const TYPE_INVEST_ACTIVE_PACKAGE = 22;
    const TYPE_CUSTOMER_RE_OPEN_PACKAGE = 25;

    const TYPE_CUSTOMER_PROVIDER_HELP = 30;
    const TYPE_INVEST_PROVIDER_HELP = 31;

    const TYPE_CUSTOMER_GET_HELP = 40;
    const TYPE_INVEST_GET_HELP = 41;

    const TYPE_CUSTOMER_WITHDRAW_POUNDAGE = 50;
    const TYPE_INVEST_WITHDRAW_POUNDAGE = 51;

    const TYPE_CUSTOMER_BUY_TICKET = 60;
    const TYPE_INVEST_BUY_TICKET = 61;

    const TYPE_CUSTOMER_ACTIVE_COMPLETED=70;
    const TYPE_INVEST_ACTIVE_COMPLETED=71;

    const TYPE_CUSTOMER_FORGET_PASSWORD= 80;
    const TYPE_INVEST_FORGET_PASSWORD = 81;

    const TYPE_CONTACT = 99;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'system_email_queue';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['params'], 'string'],
            [['result_code', 'type', 'status', 'created_at', 'updated_at'], 'integer'],
            [['to_email', 'subject', 'template', 'result_message'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'to_email' => Yii::t('app', 'To Email'),
            'subject' => Yii::t('app', 'Subject'),
            'template' => Yii::t('app', 'Template'),
            'params' => Yii::t('app', 'Params'),
            'result_code' => Yii::t('app', 'Result Code'),
            'result_message' => Yii::t('app', 'Result Message'),
            'type' => Yii::t('app', 'Type'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function createEmailQueue($type, $toEmail, $subject, $template = 'default', $params)
    {
        $email = new SystemEmailQueue();
        $email->type = $type;
        $email->to_email = $toEmail;
        $email->subject = trim($subject);
        $email->template = $template;
        $email->params = json_encode($params);
        $email->result_code = 0;
        $email->result_message = '';
        $email->status = Email::STATUS_PENDING;
        if (!$email->save()) {
//            print_r($email->getErrors());
            return false;
        }
        return true;
    }


}
