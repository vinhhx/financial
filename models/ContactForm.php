<?php

namespace app\models;

use himiklab\yii2\recaptcha\ReCaptchaValidator;
use Yii;
use yii\base\Model;
use app\models\SystemEmailQueue;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $email;
    public $subject;
    public $body;
    public $verifyCode;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // name, email, subject and body are required
            [['name', 'email', 'subject', 'body'], 'required'],
            // email has to be a valid email address
            ['email', 'email'],
            // verifyCode needs to be entered correctly
            //['verifyCode', 'captcha'],
            [['verifyCode'], ReCaptchaValidator::className(), 'secret' => Yii::$app->params["GOOGLE_CAPTCHA_SECRET_KEY"]]
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => Yii::t('app', 'Captcha'),
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param  string $email the target email address
     * @return boolean whether the model passes validation
     */
    public function contact($email)
    {
        $content = '';
        $content .= 'Name: ' . $this->name . "\n";
        $content .= 'Email: ' . $this->email . "\n";
        $content .= 'Subject: ' . $this->subject . "\n";
        $content .= 'Body: ' . $this->body;
        if ($this->validate()) {
//            Yii::$app->mailer->compose()
//                ->setTo($email)
//                ->setFrom([$this->email => $this->name])
//                ->setSubject('Contact from Bitoness at ' . date('H:i d/m/Y'))
//                ->setTextBody($content)
//                ->send();

            SystemEmailQueue::createEmailQueue(
                SystemEmailQueue::TYPE_CONTACT,
                $email,
                'Contact from Bitoness at ' . date('H:i d/m/Y'),
                'contact',
                [
                    'name' => $this->name,
                    'email' => $this->email,
                    'subject' => $this->subject,
                    'body' => $this->body,
                ]
            );
            return true;
        }
        return false;
    }
}
