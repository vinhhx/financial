<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "menu".
 *
 * @property integer $id
 * @property string $title
 * @property string $link
 * @property integer $parent_id
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 */
class Menu extends \yii\db\ActiveRecord
{

    const TYPE_MENU_TOP = 1;
    const TYPE_MENU_BOTTOM = 2;

    public $level;

    /**
     * @var $item self
     */
    private static $item;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'type', 'created_at', 'updated_at'], 'integer'],
            [['title', 'link'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Mã'),
            'title' => Yii::t('app', 'Tiêu đề'),
            'link' => Yii::t('app', 'Đường dẫn'),
            'parent_id' => Yii::t('app', 'Cấp cha'),
            'type' => Yii::t('app', 'Loại'),
            'created_at' => Yii::t('app', 'Tạo lúc'),
            'updated_at' => Yii::t('app', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    public static function getTypeLabels()
    {
        return [
            self::TYPE_MENU_TOP => 'Menu top',
            self::TYPE_MENU_BOTTOM => 'Menu bottom',
        ];
    }

    public function getItems()
    {
        if (self::$item === null) {
            self::$item = self::find()->all();
        }

        return self::$item;
    }


    /**
     * @param int $parentId
     * @param int $level
     * @return NewsCategory[]
     */
    public function getItemByLevel($parentId = 0, $level = 1)
    {
        $items = $this->getItems();

        $result = [];

        if ($items) {
            foreach ($items as $item) {
                if ($item->parent_id == $parentId) {
                    $item->level = $level;
                    $result[] = $item;

                    $result = ArrayHelper::merge($result, $this->getItemByLevel($item->id, $level + 1));
                }
            }
        }

        return $result;
    }

    public function getItemOptionByLevel($includeRoot = true)
    {
        $items = $this->getItemByLevel();

        $result = [];

        if ($includeRoot) {
            $result[0] = '*** ROOT ***';
        }

        foreach ($items as $item) {
            $result[$item->id] = str_repeat('-- ', $item->level) . $item->title;
        }

        return $result;
    }

    public function getItemsFrontend($type)
    {
        return self::find()->where(['type' => $type])->all();
    }

    public static function getItemByParentId($parentId = 0, $type = self::TYPE_MENU_TOP)
    {
        $items = static::getItemsFrontend($type);
        $result = [];

        if ($items) {
            foreach ($items as $item) {
                if ($item->parent_id == $parentId) {
                    $result[] = $item;
                }
            }
        }

        return $result;
    }
}
