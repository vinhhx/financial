<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\db\Transaction;

/**
 * This is the model class for table "customer_transaction".
 *
 * @property integer $id
 * @property integer $customer_sent_request_id
 * @property integer $customer_sent_queue_id
 * @property integer $customer_sent_id
 * @property integer $customer_sent_ref_code
 * @property string $customer_sent_username
 * @property string $customer_sent_bank_address
 * @property string $amount
 * @property integer $customer_receiver_request_id
 * @property integer $customer_receiver_queue_id
 * @property string $customer_receiver_id
 * @property string $customer_receiver_ref_code
 * @property string $customer_receiver_username
 * @property string $customer_receiver_bank_address
 * @property integer $status
 * @property integer $customer_sent_type
 * @property integer $customer_receiver_type
 * @property integer $type_step
 * @property string $expired_sent
 * @property string $expired_approve
 * @property string $created_at
 * @property string $updated_at
 * @property string $attachment
 * @property string $note
 */
class CustomerTransaction extends \yii\db\ActiveRecord
{
    const STATUS_SEND_PENDING = 1;
    const STATUS_SEND_COMPLETED = 2;
    const STATUS_RECEIVER_APPROVE = 10;
    const STATUS_FINISH = 99;

    const TYPE_OPEN_PACKAGE = 1;
    const TYPE_CASH_OUT_PACKAGE = 2;
    const TYPE_CASH_OUT_POUNDAGE = 3;
    const TYPE_RECEIVER_BY_SPECIAL = 10;
    const TYPE_SENT_BY_SPECIAL = 20;

    const TYPE_STEP_OPEN_PACKAGE_1 = 10;
    const TYPE_STEP_OPEN_PACKAGE_2 = 11;

    const TYPE_STEP_CASH_OUT_PACKAGE_1 = 20;

    const TYPE_STEP_CASH_OUT_POUNDAGE_1 = 30;
    const TYPE_STEP_GET_HELP = 50;
    const TYPE_STEP_PROVIDE_HELP = 60;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_transaction';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_sent_id', 'customer_receiver_id', 'status', 'expired_sent', 'expired_approve',
                'created_at', 'updated_at', 'customer_sent_type', 'customer_receiver_type', 'type_step', 'customer_sent_request_id',
                'customer_sent_queue_id', 'customer_receiver_request_id', 'customer_receiver_queue_id'
            ], 'integer'],
            [['customer_sent_id', 'customer_sent_username', 'amount', 'customer_receiver_id',
                'customer_receiver_username', 'expired_sent', 'expired_approve', 'customer_sent_request_id',
                'customer_sent_queue_id', 'customer_receiver_request_id', 'customer_receiver_queue_id',
            ], 'required'],
            [['amount'], 'number'],
            [['customer_sent_username', 'customer_sent_bank_address',
                'customer_receiver_username', 'customer_receiver_bank_address', 'attachment', 'note'
            ], 'string', 'max' => 255],
            [['customer_sent_ref_code', 'customer_receiver_ref_code'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'customer_sent_queue_id' => Yii::t('customers', 'Mã người cho chờ'),
            'customer_sent_request_id' => Yii::t('customers', 'Mã người cho yêu cầu'),
            'customer_receiver_request_id' => Yii::t('customers', 'Mã người nhận yêu cầu'),
            'customer_receiver_queue_id' => Yii::t('customers', 'Mã người nhận chờ'),
            'customer_sent_id' => Yii::t('customers', 'Mã người gửi'),
            'customer_sent_ref_code' => Yii::t('customers', 'Code người gửi'),
            'customer_sent_username' => Yii::t('customers', 'Người gửi'),
            'customer_sent_bank_address' => Yii::t('customers', 'Địa chỉ ngân hàng người gửi'),
            'amount' => Yii::t('customers', 'Số BTC'),
            'customer_receiver_id' => Yii::t('customers', 'Mã người nhận'),
            'customer_receiver_ref_code' => Yii::t('customers', 'Code người nhận'),
            'customer_receiver_username' => Yii::t('customers', 'Người nhận'),
            'customer_receiver_bank_address' => Yii::t('customers', 'Địa chỉ ngân hàng người nhận'),
            'status' => Yii::t('customers', 'Trạng thái'),
            'expired_sent' => Yii::t('customers', 'Hết hạn thời gian gửi'),
            'expired_approve' => Yii::t('customers', 'Hết hạn thời gian xác thực'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
            'attachment' => Yii::t('customers', 'File xác thực'),
            'note' => Yii::t('customers', 'Ghi chú'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    public function getRequest()
    {
        return $this->hasOne(CustomerRequest::className(), ['id' => 'customer_sent_request_id'])->orOnCondition(['id' => 'customer_receiver_request_id']);
    }

    public static function getTransactionCustomers()
    {
        if (!Yii::$app->user->isGuest) {
            $data = [];
            $query = new Query();
            $query->select('*');
            $query->from(CustomerTransaction::tableName());
            $query->orderBy(['id' => SORT_DESC]);
            $query->limit(20);
            $data["receiver"] = $query->where(['customer_receiver_id' => Yii::$app->user->id])->indexBy('id')->all();
            $data["sent"] = $query->where(['customer_sent_id' => Yii::$app->user->id])->indexBy('id')->all();
            return $data;
        }
    }

    public function canUpload()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        if (Yii::$app->user->id == $this->customer_sent_id && $this->status == self::STATUS_SEND_PENDING) {
            return true;
        }
        return false;
    }

    public function canDeleteAttachment()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        if (Yii::$app->user->id == $this->customer_sent_id && $this->status == self::STATUS_SEND_COMPLETED && $this->attachment) {
            return true;
        }
        return false;
    }

    public function removeAttachment()
    {
        if (!$this->canDeleteAttachment()) {
            return false;
        }
        $attachment = $this->attachment;
        $this->attachment = '';
        $this->status = self::STATUS_SEND_PENDING;
        if ($this->save(true, ['attachment', 'status', 'updated_at'])) {
            return Yii::$app->image->deleteImage($attachment);
        }

    }

    public function adminRemoveAttachment()
    {
        if ($this->status == self::STATUS_SEND_COMPLETED && !$this->attachment) {
            return false;
        }
        $attachment = $this->attachment;
        $this->attachment = '';
        $this->status = self::STATUS_SEND_PENDING;
        if ($this->save(true, ['attachment', 'status', 'updated_at'])) {
            return Yii::$app->image->deleteImage($attachment);
        }

    }

    public function canApproved()
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        if (Yii::$app->user->id == $this->customer_receiver_id && $this->status == self::STATUS_SEND_COMPLETED) {
            return true;
        }
        return false;
    }

    public function finish($admin = false)
    {
        //Xử lý request gửi
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $requestAll = CustomerRequest::find(['id' => [$this->customer_sent_request_id, $this->customer_receiver_request_id]])->indexBy('id')->all();
            $requestSend = $requestAll[$this->customer_sent_request_id];
            $requestReceiver = $requestAll[$this->customer_receiver_request_id];
            $customers = Customer::find(['id' => [$this->customer_sent_id, $this->customer_receiver_id]])->indexBy('id')->all();
            $customerSent = $customers[$this->customer_sent_id];
            $customerReceiver = $customers[$this->customer_receiver_id];
            if ($requestSend && $requestReceiver && $customerSent && $customerReceiver) {
                //Xử lý request sent
                switch ($requestSend->type) {
                    //Cập nhật trạng thái của người chuyển tiền
                    case CustomerRequest::TYPE_SENT_BY_SPECIAL:
                        $requestSend->status = CustomerRequest::STATUS_COMPLETED;
                        if (!$requestSend->save(true, ['status', 'updated_at'])) {
                            throw new \Exception(Yii::t('messages', 'Không cập nhật đc trạng thái yêu câu nhận đặc biệt'));
                        }
                        break;

                    case CustomerRequest::TYPE_PAY_WHEN_OPEN_PACKAGE: //Chuyển tiền tạo tài khoản

                        switch ($requestSend->status) {
                            case CustomerRequest::STATUS_CREATED://Chuyển tiền lần t1
                                $requestSend->status = CustomerRequest::STATUS_COMPLETE_FIRST;
                                break;
                            case CustomerRequest::STATUS_COMPLETE_FIRST;//Chuyển tiền lần t2
                                $requestSend->status = CustomerRequest::STATUS_COMPLETED;
                                break;
                            default:
                                throw new \Exception(Yii::t('messages', 'Trạng thái yêu cầu chuyển không xác định.'));
                                break;
                        }
                        //đã cập nhật đc status
                        if ($requestSend->save(true, ['status', 'updated_at'])) {
                            if ($requestSend->status == CustomerRequest::STATUS_COMPLETED) {
//                                if (!$admin) {
//                                    $customerSent->checkInvest(); //Xem người gửi là gói tái đầu tư hay gói ban đầu
//                                }
                                //update package package có thể rút tiền
                                if ($requestSend->package_id) {
                                    $package = CustomerPackage::findOne(["id" => $requestSend->package_id]);
                                    if ($package) {
                                        $package->status = CustomerPackage::STATUS_RUNNING;
                                        $package->time_sent_completed = time();
//                                        if (!$package->is_reinvestment) {
//                                            $package->is_reinvestment = ($customerSent->is_invest) ? 1 : 0;
//                                        }
                                        if (!$package->save(true, ['status', 'is_reinvestment', 'time_sent_completed', 'updated_at'])) {
                                            throw new \Exception(YIi::t('messages', 'Không cập nhật đc gói đầu tư.'));
                                        }

                                        //Tính hoa hồng
                                        $package->caculatePoundage();

                                    } else {
                                        throw new \Exception('Không tồn tại gói đầu tư.');
                                    }
                                }
                            } elseif ($requestSend->status == CustomerRequest::STATUS_COMPLETE_FIRST) {
                                if ($requestSend->package_id) {
                                    $package = CustomerPackage::findOne(["id" => $requestSend->package_id]);
                                    if ($package) {
                                        $package->status = CustomerPackage::STATUS_REQUEST_IN_STEP_TWO;
//                                        $package->time_sent_completed = time();
                                        if (!$package->save(true, ['status', 'time_sent_completed', 'updated_at'])) {
                                            throw new \Exception(Yii::t('messages', 'Không cập nhật đc gói đầu tư.'));
                                        }
                                        //Cap nhat queue ve complete
                                        Yii::$app->db->createCommand()->update(CustomerTransactionQueue::tableName(), [
                                            'status' => CustomerTransactionQueue::STATUS_COMPLETED
                                        ], [
                                            'id' => $this->customer_sent_queue_id,
                                        ])->execute();
                                        //Cập nhật queue chuyển  lần 2 sang chờ
                                        Yii::$app->db->createCommand()->update(CustomerTransactionQueue::tableName(), [
                                            'status' => CustomerTransactionQueue::STATUS_PENDING,
                                        ], ['customer_request_id' => $requestSend->id, 'status' => CustomerTransactionQueue::STATUS_REQUEST_COMPLETED_FIRST])->execute();

                                    } else {
                                        throw new \Exception(Yii::t('messages', 'Không tồn tại gói đầu tư.'));
                                    }
                                }

                            }
                        }

                        break;
                    default:
                        break;
                }

                switch ($requestReceiver->type) {
                    case CustomerRequest::TYPE_RECEIVER_BY_SPECIAL:
                        $requestReceiver->status = CustomerRequest::STATUS_COMPLETED;
                        if (!$requestReceiver->save(true, ['status', 'updated_at'])) {
                            throw new \Exception(Yii::t('messages', 'Không cập nhật đc trạng thái nhận đặc biệt'));
                        }
                        break;
                    case CustomerRequest::TYPE_RECEIVE_WHEN_CASHOUT_PACKAGE: //1 bước
                        // Xử lý rút tiền luôn
                        if ($requestReceiver->status == CustomerRequest::STATUS_CREATED) {
                            //update customer transaction queue ve 1
                            Yii::$app->db->createCommand()->update(CustomerTransactionQueue::tableName(), [
                                'status' => CustomerTransactionQueue::STATUS_COMPLETED
                            ], [
                                'id' => $this->customer_receiver_queue_id,
                            ])->execute();
                            //Check nếu còn queue yêu cầu chuyển tiền thì ko cập nhập Completed
                            $countRequestPending = $requestReceiver->countRequestQueuePending();
                            if ($countRequestPending == 0) {
                                $requestReceiver->status = CustomerRequest::STATUS_COMPLETED;
                                if (!$requestReceiver->save(true, ['status', 'updated_at'])) {
                                    throw new \Exception(Yii::t('messages', 'Không hoàn thành yêu cầu nhận.'));
                                }
                            }
                            //Nếu trạng thái người cho là đã tạo //chờ duyệt
                        } else {
                            throw new \Exception(Yii::t('messages', 'Trạng thái yêu cầu nhận không hợp lệ.'));
                        }
                        break;
                    case CustomerRequest::TYPE_RECEIVE_WHEN_CASHOUT_POUNDAGE: //1 bước
                        // Xử lý rút tiền luôn
                        if ($requestReceiver->status == CustomerRequest::STATUS_CREATED) {
                            //update customer transaction queue ve 1
                            Yii::$app->db->createCommand()->update(CustomerTransactionQueue::tableName(), [
                                'status' => CustomerTransactionQueue::STATUS_COMPLETED
                            ], [
                                'id' => $this->customer_receiver_queue_id,
                            ])->execute();
                            //Nếu trạng thái người cho là đã tạo //chờ duyệt

                            $countRequestPending = $requestReceiver->countRequestQueuePending();
                            if ($countRequestPending == 0) {
                                $requestReceiver->status = CustomerRequest::STATUS_COMPLETED;
                                if (!$requestReceiver->save(true, ['status', 'updated_at'])) {
                                    throw new \Exception(Yii::t('messages', 'Không hoàn thành được yêu cầu nhận'));
                                }
                                //cập nhật  bản ghi vào pound_transaction completed
                                Yii::$app->db->createCommand()->update(CustomerPoundageTransaction::tableName(), [
                                    'status' => CustomerPoundageTransaction::STATUS_COMPLETED
                                ], [
                                    'bill_id' => $requestReceiver->id,
                                    'type' => CustomerPoundageTransaction::TYPE_CASH_OUT_POUND,
                                ])->execute();

                            }

                        } else {
                            throw new \Exception(Yii::t('messages', 'Trạng thái yêu cầu nhận không hợp lệ.'));
                        }
                        break;
                    default:
                        break;

                }
            }

            $this->status = self::STATUS_FINISH;
            if ($this->save(true, ['status', 'updated_at'])) {
                $transaction->commit();
                return true;
            }
        } catch (\Exception $ex) {
            echo '<pre>';
            print_r($ex->getMessage());

            $transaction->rollBack();
            return false;
        }

    }

    public static function getStatusLabels($status = '')
    {
        $labels = [
            static::STATUS_SEND_PENDING => Yii::t('customers', 'Đang chờ'),
            static::STATUS_SEND_COMPLETED => Yii::t('customers', 'Chờ người nhận duyệt'),
//            static::STATUS_RECEIVER_APPROVE=>'Chờ người nhận duyệt',
            static::STATUS_FINISH => Yii::t('customers', 'Hoàn thành'),
        ];
        if ($status) {
            if (isset($labels[$status])) {
                return $labels[$status];
            } else {
                return '';
            }
        }
        return $labels;
    }

    public static function getTypeLabels($type = '')
    {
        $labels = [
            static::TYPE_OPEN_PACKAGE => Yii::t('customers', 'Kích hoạt gói đầu tư'),
            static::TYPE_CASH_OUT_POUNDAGE => Yii::t('customers', 'Nhận tiền hoa hồng'),
            static::TYPE_CASH_OUT_PACKAGE => Yii::t('customers', 'Nhận tiền từ gói đầu tư'),
            static::TYPE_SENT_BY_SPECIAL => Yii::t('customers', 'Cho người yêu cầu nhận'),
            static::TYPE_RECEIVER_BY_SPECIAL => Yii::t('customers', 'Nhận từ người đầu tư'),
        ];
        if ($type) {
            if (isset($labels[$type])) {
                return $labels[$type];
            } else {
                return '';
            }
        }
        return $labels;
    }

    public static function getTypeStepLabels()
    {
        return [
            static::TYPE_STEP_OPEN_PACKAGE_1 => Yii::t('customers', 'Cho lần đầu'),
            static::TYPE_STEP_OPEN_PACKAGE_2 => Yii::t('customers', 'Cho lần cuối'),
            static::TYPE_STEP_CASH_OUT_POUNDAGE_1 => Yii::t('customers', 'Nhận tiền hoa hồng'),
            static::TYPE_STEP_CASH_OUT_PACKAGE_1 => Yii::t('customers', 'Nhận tiền đầu tư'),
        ];
    }


}
