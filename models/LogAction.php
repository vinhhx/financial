<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "log_action".
 *
 * @property integer $id
 * @property integer $type
 * @property string $params
 * @property string $user_id
 * @property string $user_username
 * @property integer $user_type
 * @property string $user_ip
 * @property string $object_id
 * @property string $object_table
 * @property integer $created_at
 * @property integer $updated_at
 */
class LogAction extends \yii\db\ActiveRecord
{
    const USER_TYPE_ADMIN = 1;
    const USER_TYPE_CUSTOMER = 2;
    const USER_TYPE_SYSTEM = 3;

    const TYPE_USER_CREATE = 1;
    const TYPE_USER_DELETE = 2;
    const TYPE_CUSTOMER_CREATE_PACKAGE=10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log_action';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'user_id', 'user_type', 'object_id', 'created_at', 'updated_at'], 'integer'],
            [['params'], 'string'],
            [['user_username', 'object_table'], 'string', 'max' => 255],
            [['user_ip'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'type' => Yii::t('customers', 'Loại'),
            'params' => Yii::t('customers', 'Tham số'),
            'user_id' => Yii::t('customers', 'Mã user'),
            'user_username' => Yii::t('customers', 'user'),
            'user_type' => Yii::t('customers', 'Loại user'),
            'user_ip' => Yii::t('customers', 'Địa chỉ IP user'),
            'object_id' => Yii::t('customers', 'Mã bản ghi'),
            'object_table' => Yii::t('customers', 'Bảng dữ liệu'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    public static function log($objectId, $objectTable, $type, $userType, $params = [])
    {
        $model = new LogAction();
        $model->object_id = $objectId;
        $model->object_table = $objectTable;
        $model->type = $type;
        $model->params = json_encode($params);
        $model->user_type = $userType;

        // Web Request
        if (isset($_SERVER) && isset($_SERVER['HTTP_HOST'])) {
            if (!Yii::$app->user->isGuest) {
                $model->user_id = Yii::$app->user->identity->id;
                $model->user_username = Yii::$app->user->identity->username;
                $model->user_ip = Yii::$app->request->getUserIP();
            }
        }

        return $model->save();
    }

    public static function logAdmin($objectId, $objectTable, $type, $params = [])
    {
        return static::log($objectId, $objectTable, $type, self::USER_TYPE_ADMIN, $params);
    }

    public static function logUser($objectId, $objectTable, $type, $params = [])
    {
        return static::log($objectId, $objectTable, $type, self::USER_TYPE_CUSTOMER, $params);
    }
}
