<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customer_token".
 *
 * @property integer $id
 * @property string $token
 * @property string $customer_id
 * @property integer $status
 * @property integer $type
 * @property integer $expired_at
 * @property integer $created_at
 * @property integer $updated_at
 */
class CustomerToken extends \yii\db\ActiveRecord
{
    const STATUS_PENDING = 0;
    const STATUS_USED = 1;
    const TYPE_CUSTOMER = 1;
    const TYPE_INVEST = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_token';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['token', 'customer_id'], 'required'],
            [['customer_id', 'status', 'type', 'expired_at', 'created_at', 'updated_at'], 'integer'],
            [['token'], 'string', 'max' => 255],
            [['token'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'token' => Yii::t('app', 'Token'),
            'customer_id' => Yii::t('app', 'Customer ID'),
            'status' => Yii::t('app', 'Status'),
            'type' => Yii::t('app', 'Type'),
            'expired_at' => Yii::t('app', 'Expired At'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function generateToken()
    {
        $randomString = Yii::$app->getSecurity()->generateRandomString(16);

        if (!static::findOne(['token' => $randomString]))
            return $randomString;
        else
            return static::generateToken();

    }

    public static function findCurrentToken($customer_id, $type = 0)
    {
        if (!$type) {
            $type = static::TYPE_CUSTOMER;
        }
        return static::find()->where(['status' => static::STATUS_PENDING, 'customer_id' => $customer_id, 'type' => $type])->all();
    }

    public static function countCurrentToken($customer_id, $type = 0)
    {
        if (!$type) {
            $type = static::TYPE_CUSTOMER;
        }
        return static::find()->where(['status' => static::STATUS_PENDING, 'customer_id' => $customer_id, 'type' => $type])->count();
    }

    public static function getStatusLabels()
    {
        return [
            static::STATUS_PENDING => Yii::t('investment', 'Waiting'),
            static::STATUS_USED => Yii::t('investment', 'Used'),
        ];
    }

    public function getStatusLabel()
    {
        $labels = static::getStatusLabels();
        if (isset($labels[$this->status])) {
            return $labels[$this->status];
        }
        return '';
    }


}
