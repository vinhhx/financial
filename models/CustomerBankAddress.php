<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customer_bank_address".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $bank_address
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 */
class CustomerBankAddress extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_bank_address';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'bank_address'], 'required'],
            [['customer_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['bank_address'], 'string', 'max' => 255],
            [['customer_id', 'bank_address'], 'unique', 'targetAttribute' => ['customer_id', 'bank_address'], 'message' => 'The combination of Customer ID and Bank Address has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'customer_id' => Yii::t('customers', 'Mã người chơi'),
            'bank_address' => Yii::t('customers', 'Bitcoin wallet address'),
            'status' => Yii::t('customers', 'Trạng thái'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
}
