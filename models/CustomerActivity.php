<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customer_activity".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $customer_username
 * @property integer $type
 * @property string $message
 * @property string $params
 * @property integer $created_at
 * @property integer $updated_at
 */
class CustomerActivity extends \yii\db\ActiveRecord
{
    const TYPE_DANG_KY_TAI_KHOAN = 1;
    const TYPE_KICH_HOAT_TAI_KHOAN = 2;
    const TYPE_NHAN_TOKEN=3;
    const TYPE_THEM_THANH_VIEN=4;
    const TYPE_MO_GOI_DAU_TU=10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_activity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'customer_username'], 'required'],
            [['customer_id', 'type', 'created_at', 'updated_at'], 'integer'],
            [['params'], 'string'],
            [['customer_username'], 'string', 'max' => 255],
            [['message'], 'string', 'max' => 1000],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'Mã'),
            'customer_id' => Yii::t('customers', 'Mã người chơi'),
            'customer_username' => Yii::t('customers', 'Người chơi'),
            'type' => Yii::t('customers', 'Loại'),
            'message' => Yii::t('customers', 'Thông báo'),
            'params' => Yii::t('customers', 'Tham số'),
            'created_at' => Yii::t('customers', 'Tạo lúc'),
            'updated_at' => Yii::t('customers', 'Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public static function log($id, $name, $type, $message, $params)
    {
        $activity = new CustomerActivity();
        $activity->customer_id = $id;
        $activity->customer_username = $name;
        $activity->type = $type;
        $activity->message = $message;
        $activity->params = json_encode($params);
        return $activity->save();

    }

    public static function customerCreate($type, $message, $params = [])
    {
        return static::log(Yii::$app->user->identity->id, Yii::$app->user->identity->username, $type, $message, $params);
    }

    public static function systemCreate($customer_id, $customer_username, $type, $message, $params = [])
    {
        return static::log($customer_id, $customer_username, $type, $message, $params);
    }
}
