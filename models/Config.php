<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "config".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 */
class Config extends \yii\db\ActiveRecord
{
    const TYPE_HEADER_STATICTICS = 1; // Header Statictics
    const TYPE_MAKET_STATICTICS = 2; // Maket Statictics
    const TYPE_FOOTER_ABOUT = 3; // Footer About
    const TYPE_FOOTER_CONTACT = 4; // Footer Contact
    const TYPE_FOOTER_SOCIAL = 5; // Footer Contact
    const TYPE_HOME_TRANSACTION = 6; // Footer Contact

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'config';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['title', 'content'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app','Mã'),
            'title' => Yii::t('app','Tiêu đề'),
            'content' => Yii::t('app','Nội dung'),
            'type' => Yii::t('app','Loại'),
            'created_at' =>Yii::t('app','Tạo lúc'),
            'updated_at' => Yii::t('app','Cập nhật lúc'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    public static function getTypeLabels()
    {
        return [
            self::TYPE_HEADER_STATICTICS => 'Header Statictics',
            self::TYPE_MAKET_STATICTICS => 'Market Statictics',
            self::TYPE_FOOTER_ABOUT => 'Footer About',
            self::TYPE_FOOTER_CONTACT => 'Footer Contact',
            self::TYPE_FOOTER_SOCIAL => 'Footer Social',
            self::TYPE_HOME_TRANSACTION => 'Home Transaction',
        ];
    }

    public static function getConfigByType($type)
    {
        if ($type) {
            return static::find()->where(['type' => $type])->all();
        }
    }
}
