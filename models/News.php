<?php

namespace app\models;

use app\components\helpers\StringHelper;
use app\components\helpers\UploadHelpers;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\BaseFileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property integer $news_category_id
 * @property string $title
 * @property string $alias
 * @property string $intro
 * @property string $content
 * @property string $image
 * @property integer $status
 * @property integer $deleted
 * @property integer $created_at
 * @property integer $updated_at
 */
class News extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['news_category_id', 'status', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['title', 'news_category_id'], 'required'],
            [['intro', 'content'], 'string'],
            [['title', 'alias', 'image'], 'string', 'max' => 255],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Mã'),
            'news_category_id' => Yii::t('app', 'Danh mục tin tức'),
            'title' => Yii::t('app', 'Tiêu đề'),
            'alias' => Yii::t('app', 'Tên khác'),
            'intro' => Yii::t('app', 'Giới thiệu'),
            'content' => Yii::t('app', 'Nội dung'),
            'image' => Yii::t('app', 'Hình ảnh'),
            'file' => Yii::t('app', 'File hình ảnh'),
            'status' => Yii::t('app', 'Trạng thái'),
            'deleted' => Yii::t('app', 'Đã xóa'),
            'created_at' => Yii::t('app', 'Tạo lúc'),
            'updated_at' => Yii::t('app', 'Cập nhật lúc'),
        ];
    }

    public function getCategoryOptions()
    {
        return (new NewsCategory())->getItemOptionByLevel(false);
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className()
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->alias = StringHelper::rewrite($this->title);

        // Upload file
        $this->upload();

        return parent::beforeSave($insert);
    }


    public function upload()
    {
        $this->file = UploadedFile::getInstance($this, 'file');

        if ($this->file && $this->validate(['file'])) {

            $fileName = md5(uniqid() . $this->file->name) . '.' . $this->file->extension;

            // Uploaded dir
            $folderId = strtotime(date('Y-m-01 00:00:00'));

            $uploadDir = UploadHelpers::getUploadDirById($folderId, 'news');

            // Create folder upload
            BaseFileHelper::createDirectory($uploadDir);

            if ($this->file->saveAs($uploadDir . $fileName)) {
                $this->image = UploadHelpers::getImageById($folderId, $fileName, 'news');
                $this->file = null;
            }
        }
    }

    public function getImage()
    {
        return $this->image;
    }
}
