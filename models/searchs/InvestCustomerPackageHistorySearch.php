<?php

namespace app\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\investments\InvestCustomerPackageHistory;

/**
 * InvestCustomerPackageHistorySearch represents the model behind the search form about `app\models\investments\InvestCustomerPackageHistory`.
 */
class InvestCustomerPackageHistorySearch extends InvestCustomerPackageHistory
{
    public $username;
    public $phone;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_package_id', 'sign', 'type', 'created_at', 'updated_at'], 'integer'],
            [['amount'], 'number'],
            [['username','phone'],'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvestCustomerPackageHistory::find();
        $query->joinWith('investment');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'invest_customer_package_history.id' => $this->id,
            'invest_customer_package_history.customer_package_id' => $this->customer_package_id,
            'invest_customer_package_history.sign' => $this->sign,
            'invest_customer_package_history.amount' => $this->amount,
            'invest_customer_package_history.type' => $this->type,
            'invest_customer_package_history.created_at' => $this->created_at,
            'invest_customer_package_history.updated_at' => $this->updated_at,
            'invest_customer.username' => $this->username,
            'invest_customer.phone' => $this->phone,
        ]);

        return $dataProvider;
    }
}
