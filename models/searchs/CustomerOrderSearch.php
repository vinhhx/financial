<?php

namespace app\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerOrder;

/**
 * CustomerOrderSearch represents the model behind the search form about `app\models\CustomerOrder`.
 */
class CustomerOrderSearch extends CustomerOrder
{

    public $start_date;
    public $end_date;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'customer_id', 'quantity', 'status', 'type', 'upload_attachment_at', 'approved_by', 'approved_at', 'created_at', 'updated_at','type_module'], 'integer'],
            [['customer_username', 'attachment'], 'safe'],
            [['start_date', 'end_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->start_date) {
            $query->andFilterWhere(['>=', 'created_at', strtotime(Yii::$app->formatter->asDate($this->start_date, 'php:Y-m-d' . ' 00:00'))]);
        }
        if ($this->end_date) {
            $query->andFilterWhere(['<=', 'created_at', strtotime(Yii::$app->formatter->asDate($this->end_date, 'php:Y-m-d' . ' 23:59'))]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'quantity' => $this->quantity,
            'status' => $this->status,
            'type' => $this->type,
            'type_module' => $this->type_module,
            'upload_attachment_at' => $this->upload_attachment_at,
            'approved_by' => $this->approved_by,
            'approved_at' => $this->approved_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'customer_username', $this->customer_username])
            ->andFilterWhere(['like', 'attachment', $this->attachment]);

        return $dataProvider;
    }
}
