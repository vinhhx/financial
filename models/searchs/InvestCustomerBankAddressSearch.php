<?php

namespace app\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\investments\InvestCustomerBankAddress;

/**
 * InvestCustomerBankAddressSearch represents the model behind the search form about `app\models\investments\InvestCustomerBankAddress`.
 */
class InvestCustomerBankAddressSearch extends InvestCustomerBankAddress
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'invest_customer_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['bank_address'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = InvestCustomerBankAddress::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'invest_customer_id' => $this->invest_customer_id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'bank_address', $this->bank_address]);

        return $dataProvider;
    }
}
