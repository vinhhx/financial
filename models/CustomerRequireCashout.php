<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "customer_require_cashout".
 *
 * @property integer $id
 * @property string $customer_id
 * @property string $customer_username
 * @property string $amount
 * @property string $out_of_date
 * @property integer $status
 * @property integer $type
 * @property integer $admin_created_by
 * @property string $approved_by
 * @property integer $created_at
 * @property integer $updated_at
 */
class CustomerRequireCashout extends \yii\db\ActiveRecord
{
    const STATUS_PENDING = 1;
    const STATUS_APPROVED = 2;
    const STATUS_DENIED = 3;
    const STATUS_COMPLETED = 4;
    const STATUS_DELETED = 5;
    const ALLOW_CASH_OUT = [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday"
    ];
    const ALLOW_CASH_ALL = [
        "Monday",
        "Tuesday",
        "Wednesday",
    ];
    const TYPE_CUSTOMER = 1;
    const TYPE_ADMIN = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer_require_cashout';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'status', 'created_at', 'updated_at', 'type', 'admin_created_by'], 'integer'],
            [['amount'], 'number'],
            [['customer_username', 'approved_by', 'out_of_date'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('customers', 'ID'),
            'customer_id' => Yii::t('customers', 'Customer ID'),
            'customer_username' => Yii::t('customers', 'Customer Username'),
            'amount' => Yii::t('customers', 'Amount'),
            'status' => Yii::t('customers', 'Status'),
            'approved_by' => Yii::t('customers', 'Approved By'),
            'created_at' => Yii::t('customers', 'Created At'),
            'updated_at' => Yii::t('customers', 'Updated At'),
            'out_of_date' => Yii::t('customers', 'Out of date'),
            'type' => Yii::t('customers', 'Type'),
            'admin_created_by' => Yii::t('customers', 'Admin Created'),
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function getUser()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    public static function getStatusLabels()
    {
        return [
            self::STATUS_PENDING => Yii::t('customers', 'Pendding'),
            self::STATUS_APPROVED => Yii::t('customers', 'Approved'),
            self::STATUS_DENIED => Yii::t('customers', 'Denied'),
            self::STATUS_COMPLETED => Yii::t('customers', 'Completed'),
            self::STATUS_DELETED => Yii::t('customers', 'Customer Cancel'),
        ];
    }


}
