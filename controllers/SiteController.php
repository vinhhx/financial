<?php

namespace app\controllers;

use app\components\controllers\MultilLanguageController;
use app\models\Banner;
use app\models\Content;
use app\models\CustomerTransaction;
use app\models\News;
use app\models\Video;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends MultilLanguageController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        // Videos
        $videoModels = Video::find()
            ->where(['status' => 1, 'deleted' => 0])
            ->limit(3)
            ->all();

        // Contents
        $contentModels = Content::findAll(['status' => 1, 'deleted' => 0]);
        $contents = [];

        if ($contentModels) {
            foreach ($contentModels as $c) {
                $contents[$c->type_id][] = $c;
            }
        }

        $modelBanners = Banner::find()
            ->where(['type' => Banner::TYPE_SLIDE_HOME, 'status' => Banner::STATUS_ACTIVE])
            ->orderBy(['title' => SORT_ASC])
            ->all();

        $modelTransactions = CustomerTransaction::find()
            ->where(['status' => CustomerTransaction::STATUS_FINISH])
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(15)
            ->all();

        $modelNews = News::find()->where(['status' => 1, 'news_category_id' => 1])->orderBy(['created_at' => SORT_DESC])->one();

        return $this->render('index', [
            'videoModels' => $videoModels,
            'contents' => $contents,
            'modelBanners' => $modelBanners,
            'modelTransactions' => $modelTransactions,
            'modelNews' => $modelNews,
        ]);
    }

    public function actionLogin()
    {
        $this->layout = 'login';
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionTest()
    {
        try {
            $message = \Yii::$app->mailer->compose('test');
            $result_code = $message->setFrom([Yii::$app->params["emailSend"] => 'FINANCIAL.VN'])
                ->setTo('hoangxuanvinh88@gmail.com')
                ->setSubject('Email test')
                ->send();
            echo '<pre>';
            var_dump($result_code);
            die;
        } catch (\Exception $ex) {
            $msg = $ex->getMessage();
            echo '<pre>';
            var_dump($msg);
            die;
//            print_r($ex->getTraceAsString());
        }
    }
}
