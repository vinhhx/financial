<?php

namespace app\controllers;

use app\models\Content;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;

class ContentController extends \yii\web\Controller
{

    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    protected function findModel($id)
    {
        if (($model = Content::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
