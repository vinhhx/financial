<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 06-Oct-15
 * Time: 1:01 AM
 */
namespace app\components\controllers;

use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class InvestBaseController extends MultilLanguageController
{

    public function beforeAction($action)
    {
        Yii::$app->setHomeUrl(['/invest']);
        $actionGuest = [
            Yii::$app->user->loginUrl,
            '/invest/default/register',
            '/invest/my-account/active',
            '/invest/my-account/buy-token',
            '/invest/my-account/check-out',
            '/invest/my-account/token',
            '/invest/default/forget-password',
            '/invest/default/logout',
            '/invest/note/ajax-get-all-note',
        ];
        $actionText = isset(explode('?', Yii::$app->request->getUrl())[0]) ? explode('?', Yii::$app->request->getUrl())[0] : '';

        if (!Yii::$app->user->isGuest && $actionText == Url::to(['default/login'])) {
            $this->redirect(Url::to(['/invest/default/index']));
            return false;
        }
        if (Yii::$app->user->isGuest && !in_array($actionText, $actionGuest)) {
            $this->redirect(Yii::$app->user->loginUrl);
            return false;
        } else {

            // Logout after 10 minutes
            $timeSessionLogin = isset(Yii::$app->params['timeSessionLogin']) ? Yii::$app->params['timeSessionLogin'] : 600;
            if (isset($_SESSION['timestamp']) && ((time() - $_SESSION['timestamp']) > $timeSessionLogin)) {
                unset($_SESSION['timestamp']);
                Yii::$app->user->logout(false);
                $this->redirect(Yii::$app->user->loginUrl);
            } else {
                $_SESSION['timestamp'] = time();
            }

            $url = Yii::$app->urlManager->createUrl(['/invest/my-account/active']);
            if (!in_array($actionText, $actionGuest)) {
                if (!Yii::$app->user->isGuest && !Yii::$app->user->identity->is_active && Yii::$app->request->getUrl() !== Url::to(['my-account/active'])) {
                    $this->redirect($url);
                    return false;
                }
            }


//            else if(!Yii::$app->user->isGuest && Yii::$app->request->getUrl() == Yii::$app->user->loginUrl){
//                return $this->goHome();
//
//            }
        }


        return parent::beforeAction($action);
    }

}