<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 06-Oct-15
 * Time: 1:01 AM
 */
namespace app\components\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class AdminBaseController extends MultilLanguageController
{

    public function beforeAction($action)
    {
        Yii::$app->setHomeUrl('/admin');

        if (Yii::$app->user->isGuest && Yii::$app->request->getUrl() !== Url::to(Yii::$app->user->loginUrl)) {
            return $this->redirect(Yii::$app->user->loginUrl);
        }

        // Logout after 10 minutes
        $timeSessionLogin = isset(Yii::$app->params['timeSessionLogin']) ? Yii::$app->params['timeSessionLogin'] : 600;
        if (isset($_SESSION['timestamp']) && ((time() - $_SESSION['timestamp']) > $timeSessionLogin)) {
            unset($_SESSION['timestamp']);
            Yii::$app->user->logout(false);
            $this->redirect(Yii::$app->user->loginUrl);
        } else {
            $_SESSION['timestamp'] = time();
        }

        $auth = Yii::$app->authManager;

        $controllerName = Yii::$app->controller->id;
        $actionName = Yii::$app->controller->action->id;
        $userId = Yii::$app->user->id;

        $operation = $controllerName . '/' . $actionName;

        if (in_array($operation, ['default/login', 'default/index'])) {
            return true;
        }

        $parent = parent::beforeAction($action);

        // Bỏ check phân quyền nếu tài khoản là system-admin
        $isSystemAdmin = $auth->getAssignment(RBAC_FULL, $userId);
        if ($isSystemAdmin) {
            return $parent;
        }

        if ($auth->checkAccess($userId, $operation)) {
            return $parent;
        }

        echo $this->render('/default/error', [
            'name' => Yii::t('messages', 'Từ chối truy cập'),
            'message' => Yii::t('messages', 'Bạn không có quyền truy cập chức năng này'),
            'exception' => []
        ]);
        return false;
        // khi nao mở phân quyền  thì dóng dóng dưới vì ở trên đã check

//        return parent::beforeAction($action);
    }

}