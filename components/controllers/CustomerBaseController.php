<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 06-Oct-15
 * Time: 1:01 AM
 */
namespace app\components\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class CustomerBaseController extends MultilLanguageController
{

    public function beforeAction($action)
    {
        Yii::$app->setHomeUrl(['/customer']);
        $actionGuest = [
            Yii::$app->user->loginUrl,
            '/customer/default/register',
            '/customer/my-account/buy-token',
            '/customer/default/forget-password',
            '/customer/default/reset-password',
            '/customer/note/ajax-get-all-note',
            '/member/default/register',
            '/member/my-account/buy-token',
            '/member/default/forget-password',
            '/member/default/reset-password',
            '/member/note/ajax-get-all-note',
        ];
        $actionNotActive = [
            '/customer/my-account/buy-token',
            '/customer/my-account/order-index',
            '/customer/my-account/check-out',
            '/customer/default/logout',
            '/customer/default/forget-password',
            '/customer/note/ajax-get-all-note',
            '/member/my-account/buy-token',
            '/member/my-account/order-index',
            '/member/my-account/check-out',
            '/member/default/logout',
            '/member/default/forget-password',
            '/member/note/ajax-get-all-note'
        ];
        $actionText = isset(explode('?', Yii::$app->request->getUrl())[0]) ? explode('?', Yii::$app->request->getUrl())[0] : '';

        if (!Yii::$app->user->isGuest && $actionText == Url::to(['default/login'])) {
            $this->redirect(Url::to(['/customer/default/index']));
            return false;
        }
        if (Yii::$app->user->isGuest && !in_array($actionText, $actionGuest)) {
            $this->redirect(Yii::$app->user->loginUrl);
            return false;
        } else {

            // Logout after 10 minutes
            $timeSessionLogin = isset(Yii::$app->params['timeSessionLogin']) ? Yii::$app->params['timeSessionLogin'] : 600;
            if (isset($_SESSION['timestamp']) && ((time() - $_SESSION['timestamp']) > $timeSessionLogin)) {
                unset($_SESSION['timestamp']);
                Yii::$app->user->logout(false);
                $this->redirect(Yii::$app->user->loginUrl);
            } else {
                $_SESSION['timestamp'] = time();
            }

            $url = Yii::$app->urlManager->createUrl(['/customer/my-account/active']);
            if (!in_array($actionText, $actionNotActive)) {
                if (!Yii::$app->user->isGuest && !Yii::$app->user->identity->is_active && Yii::$app->request->getUrl() !== Url::to(['my-account/active'])) {
                    $this->redirect($url);
                    return false;
                }
            }


//            else if(!Yii::$app->user->isGuest && Yii::$app->request->getUrl() == Yii::$app->user->loginUrl){
//                return $this->goHome();
//
//            }
        }


//        $auth = Yii::$app->authManager;
//
//        $controllerName = Yii::$app->controller->id;
//        $actionName = Yii::$app->controller->action->id;
//        $userId = Yii::$app->user->id;
//
//        $operation = $controllerName.'/'.$actionName;
//
//        if(in_array($operation, ['default/login', 'default/index'])){
//            return true;
//        }
//
//        $parent = parent::beforeAction($action);
//
//        // Bỏ check phân quyền nếu tài khoản là system-admin
//        $isSystemAdmin = $auth->getAssignment(HZ_RBAC_FULL, $userId);
//        if($isSystemAdmin){
//            return $parent;
//        }
//
//        if($auth->checkAccess($userId, $operation)){
//            return $parent;
//        }
//
//        echo $this->render('/default/error', [
//            'name' => 'Quyền truy cập bị từ chối',
//            'message' => 'Quyền truy cập bị từ chối',
//            'exception' => []
//        ]);
//        return false;
        // khi nao mở phân quyền  thì dóng dóng dưới vì ở trên đã check

        return parent::beforeAction($action);
    }

}