<?php

namespace app\components\validators;

use yii\validators\Validator;

class PhoneNumberValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        if (!preg_match('/^0([0-9]{9,10})$/i', $model->$attribute)) {
            $this->addError($model, $attribute, 'Số điện thoại không hợp lệ');
        }else{
            $value = $model->$attribute;
            $providers = [
                'Viettel' => ['096', '097', '098', '0162', '0163', '0164', '0165', '0166', '0167', '0168', '0169'],
                'Vinaphone' => ['091', '094', '0123', '0124', '0125', '0127', '0129'],
                'MobiFone' => ['090', '093', '0120', '0121', '0122', '0126', '0128'],
                'Vietnamobile' => ['092', '0186', '0188'],
                'Gmobile' => ['0993', '0994', '0995', '0996', '0997', '0199'],
            ];

            $validate = true;

            $phoneNumberBegin = '';
            if(strpos($value, '01') === 0 || strpos($value, '099') === 0){
                // Sử dụng các đầu số bắt đầu bằng 01 và 099
                $phoneNumberBegin = substr($value, 0, -strlen($value) + 4);
            }elseif(strpos($value, '09') === 0){
                // Sử dụng các đầu số bắt đầu bằng 09
                $phoneNumberBegin = substr($value, 0, -strlen($value) + 3);
            }else{
                $validate = false;
            }

            if($validate === true){
                $providerName = '';
                foreach($providers as $name => $provider){
                    if(in_array($phoneNumberBegin, $provider)){
                        $providerName = $name;
                        break;
                    }
                }

                if($providerName == ''){
                    $this->addError($model, $attribute, 'Số điện thoại không hợp lệ');
                }
            }
        }
    }
}