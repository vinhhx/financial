<?php

namespace app\components\validators;

use yii\validators\Validator;

class UsernameValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        if (!preg_match('/^([a-z0-9]+)$/i', $model->$attribute)) {
            if (!$this->message) {
                $this->message = $model->getAttributeLabel($attribute) . ' chỉ bao gồm các chữ cái từ a-z và các chữ số từ 0-9.';
            }
            $this->addError($model, $attribute, $this->message);
        }
    }
}