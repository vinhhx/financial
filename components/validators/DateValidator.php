<?php
/**
 * Created by PhpStorm.
 * User: quyetnx
 * Date: 7/16/2015
 * Time: 4:00 PM
 */

namespace app\components\validators;

use Yii;
use yii\base\InvalidParamException;
use yii\validators\Validator;

class DateValidator extends Validator
{
    public function validateAttribute($model, $attribute, $params = [])
    {
        try {
            $date = Yii::$app->formatter->asDate($model->$attribute, 'yyyy-MM-dd');
            if ($date !== false) {
                $model->$attribute = $date;
            } else {
                $this->addError($model, $attribute, 'Không đúng định dạng ngày.', $params);
            }
        } catch(InvalidParamException $e) {
            $this->addError($model, $attribute, 'Ngày tháng phải có dạng dd-MM-yyyy, ví dụ: 21-12-2012.');
        }

    }
}