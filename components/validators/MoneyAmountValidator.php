<?php
/**
 * Created by PhpStorm.
 * User: Quan
 * Date: 8/21/2015
 * Time: 4:49 PM
 */

namespace app\components\validators;


use yii\validators\Validator;

class MoneyAmountValidator extends Validator
{
    public function validateAttribute($model, $attribute)
    {
        $amount = $model->$attribute;
        if (!is_numeric($amount) || $amount < 100 || $amount % 100) {
            $model->addError($attribute, 'Số tiền không hợp lệ.');
        }
    }
}