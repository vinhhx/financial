<?php

namespace app\components\images;

use app\models\Media;
use yii\helpers\BaseFileHelper;
use yii\helpers\FileHelper;
use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use yii\imagine\Image;
use Imagine\Image\ManipulatorInterface;
use Yii;

class ImageUpload extends \yii\base\Component
{

    public function init()
    {

    }

    public function upload($objectId = 0, $folder, $file, $name = "")
    {
        //file after getInstance;
        if ($file && $file->baseName && $file->extension && $file->size && $file->type) {
            $name = ($name) ? $name : md5(uniqid() . $file->baseName);

            if ((int)$objectId) {
                $folder = $folder . '/' . $objectId;
            }
            $uploadDir = $this->getUploadDir($folder);
            // Tạo thư mục upload nếu chưa tồn tại
            BaseFileHelper::createDirectory($uploadDir);
            try {
                $fileName = $name . '.' . $file->extension;
                if ($file->saveAs($uploadDir . $fileName)) {
                    //chmod 0777 file upload
                    chmod($uploadDir . $fileName, 0777);
                    $pathImage = $folder . '/' . $fileName;
                    $sizes = (!empty($sizes)) ? $sizes : ((isset(\Yii::$app->params["imageResizes"]) && !empty(Yii::$app->params["imageResizes"])) ? Yii::$app->params["imageResizes"] : []);
                    if (!empty($sizes)) {
                        $this->resize($pathImage, $sizes);
                    }
                    return $pathImage;
                }
            } catch (\Exception $ex) {
                return null;
            }
        }
        return null;
    }

    public function uploadWithUrl($project, $module, $file, $name = "")
    {

    }

    public function getImg($imageFile, $size = "")
    {
        if (filter_var($imageFile, FILTER_VALIDATE_URL)) {
            return $imageFile;
        } else {
            $imageFile = str_replace('/uploads', '', $imageFile);
            if ($size != '') {
                $pos = strrpos($imageFile, '.');
                $extension = substr($imageFile, $pos);
                $extension = $size . $extension;
                $imageFile = substr($imageFile, 0, $pos) . '.' . $extension;
            }
            return \Yii::$app->params['CDNServer'] . $imageFile;
        }
    }

    /**
     * Build đường dẫn tuyệt đối đến thư mục ảnh
     * @param $objectId
     * @param $folder
     * @return string
     */
    public function getUploadDir($folder)
    {
        return \Yii::getAlias('@app/web/uploads/') . $folder . '/';
    }

    /*
     * Hàm trả về name khi đầu vào là đường dẫn ảnh
     */

    public function getPathImage($link)
    {
        $lastPost = strripos($link, '/', -1);
        $startPost = stripos($link, '/uploads');
        $path = substr($link, $startPost, $lastPost - $startPost);
        return $path;
    }

    /*
    * Hàm trả về path khi đầu vào là đường dẫn ảnh
    */

    public function getPathUpload($link, $replace = 'uploads/')
    {
        $startPost = stripos($link, '/uploads');
        $path = substr($link, $startPost + 1);
        $path = str_replace($replace, "", $path);
        return $path;

    }

    /*
     * Hàm trả về filename khi đầu vào là đường dẫn ảnh
     */

    public function getNameImage($link)
    {
        $lastPost = strripos($link, '/', -1);
        $filename = substr($link, $lastPost + 1, strlen($link));
        return $filename;
    }

    public function deleteAllThumbs($imageFile)
    {
        if (!empty(\Yii::$app->params["imageResizes"])) {
            foreach (\Yii::$app->params["imageResizes"] as $size) {
                $pos = strrpos($imageFile, '.');
                $extension = substr($imageFile, $pos);
                $extension = $size . $extension;
                $thumbImageFile = substr($imageFile, 0, $pos) . '.' . $extension;
                $uploadDir = $this->getUploadRoot();
                if (file_exists($uploadDir . $thumbImageFile)) {
                    unlink($uploadDir . $thumbImageFile);
                }
            }
        }

    }

    public function getUploadRoot()
    {
        return \Yii::getAlias('@app/web/uploads/');
    }

    public function deleteImage($file)
    {
        $uploadDir = $this->getUploadRoot();
        if (file_exists($uploadDir . $file)) {
            unlink($uploadDir . $file);
            $this->deleteAllThumbs($file);
            return true;
        }
        return false;
    }


    public function resize($filename, $sizes = [])
    {
        $sizes = empty($sizes) ? Yii::$app->params['imageResizes'] : $sizes;
        $uploadDir = Yii::getAlias('@app/web/uploads/');
        $sourceFile = $uploadDir . $filename;
        if (!file_exists($sourceFile) || empty($sizes)) {
            return;
        }
        $slugs = explode('.', $filename);
        if (count($slugs) == 2) { //resize những file có dạng xxx.jpg/xxx.png/xxx.gif
            foreach ($sizes as $strSize) {
                $newFile = $slugs[0] . '.' . $strSize . '.' . $slugs[1];
                $size = explode('x', $strSize);
                $newWidth = $width = $size[0];
                $newHeight = $height = $size[1];

                $imagine = Image::getImagine()->open($sourceFile);
                $originSize = $imagine->getSize();

                $oldWidth = $originSize->getWidth();
                $oldHeight = $originSize->getHeight();

                // Scale image size
                if ($oldWidth > $width || $oldHeight > $height) {
                    if ($oldWidth >= $oldHeight) {
                        $ratio = $oldWidth / $width;
                        $newHeight = floor($oldHeight / $ratio);
                    } else {
                        $ratio = $oldHeight / $height;
                        $newWidth = floor($oldWidth / $ratio);
                    }

                    $imagine->resize(new Box($newWidth, $newHeight))
                        ->save($uploadDir . $newFile, ['quality' => 100]);
                    unset($newFile);
                } else {
                    copy($sourceFile, $uploadDir . $newFile);
                }
            }

        }
    }

}
