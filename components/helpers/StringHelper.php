<?php
/**
 * Created by PhpStorm.
 * User: quyet
 * Date: 8/13/2015
 * Time: 21:25
 */

namespace app\components\helpers;


use app\models\LanguageEntry;

class StringHelper extends \yii\helpers\StringHelper
{
    /**
     * Xóa trọng âm
     * @param $string
     * @return string
     */
    public static function removeMark($string)
    {
        $trans = array(
            'à' => 'a', 'á' => 'a', 'ả' => 'a', 'ã' => 'a', 'ạ' => 'a',
            'ă' => 'a', 'ằ' => 'a', 'ắ' => 'a', 'ẳ' => 'a', 'ẵ' => 'a', 'ặ' => 'a',
            'â' => 'a', 'ầ' => 'a', 'ấ' => 'a', 'ẩ' => 'a', 'ẫ' => 'a', 'ậ' => 'a',
            'À' => 'A', 'Á' => 'A', 'Ả' => 'A', 'Ã' => 'A', 'Ạ' => 'A',
            'Ă' => 'A', 'Ằ' => 'A', 'Ắ' => 'A', 'Ẳ' => 'A', 'Ẵ' => 'A', 'Ặ' => 'A',
            'Â' => 'A', 'Ầ' => 'A', 'Ấ' => 'A', 'Ẩ' => 'A', 'Ẫ' => 'A', 'Ậ' => 'A',
            'đ' => 'd', 'Đ' => 'D',
            'è' => 'e', 'é' => 'e', 'ẻ' => 'e', 'ẽ' => 'e', 'ẹ' => 'e',
            'ê' => 'e', 'ề' => 'e', 'ế' => 'e', 'ể' => 'e', 'ễ' => 'e', 'ệ' => 'e',
            'È' => 'E', 'É' => 'E', 'Ẻ' => 'E', 'Ẽ' => 'E', 'Ẹ' => 'E',
            'Ê' => 'E', 'Ề' => 'E', 'Ế' => 'E', 'Ể' => 'E', 'Ễ' => 'E', 'Ệ' => 'E',
            'ì' => 'i', 'í' => 'i', 'ỉ' => 'i', 'ĩ' => 'i', 'ị' => 'i',
            'Ì' => 'I', 'Í' => 'I', 'Ỉ' => 'I', 'Ĩ' => 'I', 'Ị' => 'I',
            'ò' => 'o', 'ó' => 'o', 'ỏ' => 'o', 'õ' => 'o', 'ọ' => 'o',
            'ô' => 'o', 'ồ' => 'o', 'ố' => 'o', 'ổ' => 'o', 'ỗ' => 'o', 'ộ' => 'o',
            'ơ' => 'o', 'ờ' => 'o', 'ớ' => 'o', 'ở' => 'o', 'ỡ' => 'o', 'ợ' => 'o',
            'Ò' => 'O', 'Ó' => 'O', 'Ỏ' => 'O', 'Õ' => 'O', 'Ọ' => 'O',
            'Ô' => 'O', 'Ồ' => 'O', 'Ố' => 'O', 'Ổ' => 'O', 'Ỗ' => 'O', 'Ộ' => 'O',
            'Ơ' => 'O', 'Ờ' => 'O', 'Ớ' => 'O', 'Ở' => 'O', 'Ỡ' => 'O', 'Ợ' => 'O',
            'ù' => 'u', 'ú' => 'u', 'ủ' => 'u', 'ũ' => 'u', 'ụ' => 'u',
            'ư' => 'u', 'ừ' => 'u', 'ứ' => 'u', 'ử' => 'u', 'ữ' => 'u', 'ự' => 'u',
            'Ù' => 'U', 'Ú' => 'U', 'Ủ' => 'U', 'Ũ' => 'U', 'Ụ' => 'U',
            'Ư' => 'U', 'Ừ' => 'U', 'Ứ' => 'U', 'Ử' => 'U', 'Ữ' => 'U', 'Ự' => 'U',
            'ỳ' => 'y', 'ý' => 'y', 'ỷ' => 'y', 'ỹ' => 'y', 'ỵ' => 'y',
            'Y' => 'Y', 'Ỳ' => 'Y', 'Ý' => 'Y', 'Ỷ' => 'Y', 'Ỹ' => 'Y', 'Ỵ' => 'Y'
        );
        return strtr($string, $trans);
    }

    /**
     * @param $string
     * @return string
     */
    public static function rewrite($string)
    {
        $string = static::removeMark(trim($string));
        $string = strtolower($string);
        // Chuyển các ký tự đặc biệt thành khoảng trắng
        $string = preg_replace('/([^a-z0-9\s])/i', '', $string);
        $string = preg_replace('/\s+/', '-', $string); // Xoá các khoảng trắng lặp lại

        return $string;
    }

    public static function mbTruncate($string, $max_chars = 200, $append = "\xC2\xA0…")
    {
        $string = strip_tags($string);
        $string = html_entity_decode($string, ENT_QUOTES, 'utf-8');
        // \xC2\xA0 is the no-break space
        $string = trim($string, "\n\r\t .-;–,—\xC2\xA0");
        $length = strlen(utf8_decode($string));

        // Nothing to do.
        if ($length < $max_chars) {
            return $string;
        }

        // mb_substr() is in /wp-includes/compat.php as a fallback if
        // your the current PHP installation doesn't have it.
        $string = mb_substr($string, 0, $max_chars, 'utf-8');

        // No white space. One long word or chinese/korean/japanese text.
        if (FALSE === strpos($string, ' ')) {
            return $string . $append;
        }

        // Avoid breaks within words. Find the last white space.
        if (extension_loaded('mbstring')) {
            $pos = mb_strrpos($string, ' ', 'utf-8');
            $short = mb_substr($string, 0, $pos, 'utf-8');
        } else {
            // Workaround. May be slow on long strings.
            $words = explode(' ', $string);
            // Drop the last word.
            array_pop($words);
            $short = implode(' ', $words);
        }

        return $short . $append;
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function startWith($haystack, $needle)
    {
        return isset($haystack[0]) && $haystack[0] === $needle;
    }

    /**
     * @param $haystack
     * @param $needle
     * @return bool
     */
    public static function endWith($haystack, $needle)
    {
        return isset($haystack[strlen($haystack) - strlen($needle)]) && $haystack[strlen($haystack) - strlen($needle)] === $needle;
    }
}