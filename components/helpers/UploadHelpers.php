<?php
/**
 * Created by PhpStorm.
 * User: quyet
 * Date: 8/13/2015
 * Time: 21:32
 */

namespace app\components\helpers;

use Yii;
use yii\helpers\BaseFileHelper;

class UploadHelpers
{

    public static function getBaseDir()
    {
        return Yii::getAlias('@app/web/uploads/');
    }

    /**
     * Build đường dẫn tuyệt đối đến thư mục ảnh
     * @param $objectId
     * @param $folder
     * @return string
     */
    public static function getUploadDirById($objectId, $folder)
    {
        return static::getBaseDir() . $folder . '/' . ceil($objectId / 2048) . '/';
    }

    /**
     * Lấy đường dẫn ảnh (http://...) theo ID của sản phẩm, thuộc tính, ...
     * @param $objectId
     * @param $imageFile
     * @param $folder
     * @param string $size
     * @param boolean $useCDN
     * @return mixed|string
     */
    public static function getImageById($objectId, $imageFile, $folder, $size = '', $useCDN = false)
    {
        if (filter_var($imageFile, FILTER_VALIDATE_URL)) {
            return $imageFile;
        } else {
            $filePath = '/' . $folder . '/' . ceil($objectId / 2048) . '/' . $imageFile;
            return static::createImageUrl($filePath, $size, $useCDN);
        }
    }

    /**
     * Lấy đường dẫn ảnh trên CDN server.
     * @param $imageFile
     * @param string $size
     * @param boolean $useCDN
     * @return mixed|string
     */
    public static function createImageUrl($imageFile, $size = '', $useCDN = true)
    {
        if (filter_var($imageFile, FILTER_VALIDATE_URL)) {
            return $imageFile;
        } else {
            $imageFile = str_replace('/uploads', '', $imageFile);
            if ($size != '') {
                $pos = strrpos($imageFile, '.');
                $extension = substr($imageFile, $pos);
                $extension = $size . $extension;
                $imageFile = substr($imageFile, 0, $pos) . '.' . $extension;
            }
            return ($useCDN ? rtrim(Yii::$app->params['CDNServer'], '/') : '/uploads') . '/' . ltrim($imageFile, '/');
        }
    }

    /**
     * @param $size
     * @return string
     */
    public static function getFileSize($size)
    {
        $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $power = $size > 0 ? floor(log($size, 1024)) : 0;
        return number_format($size / pow(1024, $power), 2, '.', ',') . ' ' . $units[$power];
    }

    /**
     * @param $fileName
     * @return string
     */
    public static function getShortName($fileName)
    {
        return substr_replace($fileName, ' ... ', 10, -10);
    }

    /**
     * @param string|null $fileName
     * @return string
     * @throws \yii\base\Exception
     */
    public static function encryptFileName($fileName = null)
    {
        return md5(uniqid() . $fileName);
    }
}