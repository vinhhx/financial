<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 11-Nov-15
 * Time: 3:40 PM
 */
namespace app\components;


use app\models\Email;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class SentEmail extends Component
{
    public $fromEmail = 'lincogsy@lincolnmgt.com';
    public $fromName = 'LINCOGSY VN';

    /**
     * Gửi email tới 1 địa chỉ, sau đó log lại chi tiết email.
     * @param $toEmail
     * @param $subject
     * @param $template
     * @param $params
     * @return bool
     */

    public function send($type, $toEmail, $subject, $template, $params, $sending = true)
    {
        /*
        $resultCode = false;
        $resultMessage = '';

        try {

            $message = \Yii::$app->mailer->compose($template, $params);

            $resultCode = $message->setFrom([$this->fromEmail => $this->fromName])
                ->setTo($toEmail)
                ->setSubject($subject)
                ->send();
        } catch(\Exception $ex) {
            $resultMessage = $ex->getMessage();
            echo $ex->getMessage();
        }
        $email = new Email();
        $email->type = $type;
        $email->to_email = $toEmail;
        $email->subject = $subject;
        $email->template = $template;
        $email->params = json_encode($params);
        $email->result_code = ($resultCode)?1:0;
        $email->result_message = $resultMessage;
        if(!$email->save()) {
//			echo '<pre>'; print_r($email->getErrors()); echo '</pre>';
        }
        return $resultCode;
        */
        $email = new Email();
        $email->type = $type;
        $email->to_email = $toEmail;
        $email->subject = trim($subject);
        $email->template = $template;
        $email->params = json_encode($params);
        $email->result_code = 0;
        $email->result_message = '';
        $email->status = Email::STATUS_PENDING;
        if (!$email->save()) {
            print_r($email->getErrors());
        }
        if ($sending) {
            //Lưu và gửi email luôn.
            $this->sentEmail($email);
        }

        return 1;
    }

    public function sentEmail($object)
    {
        if (!$object) {
            return false;
        }
        try {
            $params = ArrayHelper::toArray(json_decode($object->params));
            $message = \Yii::$app->mailer->compose($object->template, $params);
            $object->result_code = $message->setFrom([\Yii::$app->params["emailSend"] => 'LINCOLNMGT.COM'])
                ->setTo($object->to_email)
                ->setSubject($object->subject)
                ->send();
            $object->status = Email::STATUS_SENT_COMPLETE;

        } catch (\Exception $ex) {
            $object->result_message = $ex->getMessage();
            $object->status = Email::STATUS_SENT_ERROR;
//            echo $ex->getMessage();
//            print_r($ex->getTraceAsString());
        }
        $object->save();
    }


}