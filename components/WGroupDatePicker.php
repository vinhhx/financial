<?php

namespace app\components;

use Yii;
use yii\base\InvalidParamException;
use yii\base\InvalidConfigException;
use yii\helpers\FormatConverter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\jui\DatePickerLanguageAsset;
use yii\jui\JuiAsset;
use yii\jui\DatePicker;

class WGroupDatePicker extends DatePicker {

    /**
     * Mảng attribute tương ứng
     * @var array
     */
    public $attributes = [];

    /**
     * Nhãn của các thẻ input tương ứng với attribute
     * @var array
     */
    public $labels = [];

    /**
     * Tùy chọn về attribute cho thẻ div ngoài cùng
     * @var array
     */
    public $divOptions = [];

    /**
     * Nếu tùy chọn là false thì hiển thị label
     * Hoặc true thì không hiển thị label thay vào là placeholder
     * @var boolean true|false
     */
    public $formInline = false;

    public function init() {
        // Thông báo lỗi nếu không có $attributes
        if (empty($this->attributes)) {
            throw new InvalidConfigException("Tham số 'attributes' chưa được xác định.");
        }

        $this->attribute = reset($this->attributes);
        parent::init();

        if (!$this->labels || empty($this->labels)) {
            foreach ($this->attributes as $attribute) {
                $this->labels[$attribute] = $this->model->getAttributeLabel($attribute);
            }
        } else {
            $this->labels = array_combine($this->attributes, $this->labels);
        }
    }

    public function run() {
        echo $this->renderWidget() . "\n";

        $juiAsset = JuiAsset::register($this->getView());

        $containerID = $this->inline ? $this->containerOptions['id'] : $this->options['id'];
        $language = $this->language ? $this->language : Yii::$app->language;

        foreach ($this->attributes as $attribute) {
            $containerArr=explode('-', $containerID);
            if(reset($containerArr)){
                $containerIds[$attribute] = reset($containerArr) . '-' . $attribute;

            }
        }

        if (strncmp($this->dateFormat, 'php:', 4) === 0) {
            $this->clientOptions['dateFormat'] = FormatConverter::convertDatePhpToJui(substr($this->dateFormat, 4), 'date', $language);
        } else {
            $this->clientOptions['dateFormat'] = FormatConverter::convertDateIcuToJui($this->dateFormat, 'date', $language);
        }

        if ($language != 'en-US') {
            $view = $this->getView();
            $bundle = new DatePickerLanguageAsset([
                'language' => Yii::$app->language,
                'baseUrl' => $juiAsset->baseUrl
            ]);
            if ($bundle->autoGenerate) {
                $view->registerJsFile($bundle->baseUrl . "/ui/i18n/datepicker-$language.js", [
                    'depends' => [JuiAsset::className()],
                ]);
            }
            $options = Json::encode($this->clientOptions);
            $view->registerJs("$('.group-datepicker input').focus(function(){ $(this).parent().parent().addClass('group-datepicker-focus'); }); $('.group-datepicker input').blur(function(){ $(this).parent().parent().removeClass('group-datepicker-focus'); });");
            foreach ($containerIds as $item) {
                $view->registerJs("$('#{$item}').datepicker($.extend({}, $.datepicker.regional['{$language}'], $options));");
            }
        } else {
            foreach ($containerIds as $item) {
                $this->registerClientOptions('datepicker', $item);
            }
        }
        foreach ($containerIds as $item) {
            $this->registerClientEvents('datepicker', $item);
        }
    }

    /**
     * Renders the DatePicker widget.
     * @return string the rendering result.
     */
    protected function renderWidget() {
        $contents = [];

        // get formatted date value
        if ($this->hasModel()) {
            foreach ($this->attributes as $attributeItem) {
                $values[$attributeItem] = Html::getAttributeValue($this->model, $attributeItem);
            }
        } else {
            foreach ($this->attributes as $attributeItem) {
                $values[$attributeItem] = $this->value;
            }
        }

        foreach ($values as $attr => $value) {
            if ($value != null || $value != '') {
                // format value according to dateFormat
                try {
                    $values[$attr] = Yii::$app->formatter->asDate($value, $this->dateFormat);
                } catch (InvalidParamException $e) {
                    // ignore exception and keep original value if it is not a valid date
                }
            }
        }

        foreach ($this->attributes as $attributeItem) {
            foreach ($this->options as $keyOption => $option) {
                //$extendClass = isset($option['class']) ? [$option['class']] : [];
                $options[$attributeItem][$keyOption] = $option;
                $arrayOptions=explode('-', $option);
                if(reset($arrayOptions)){
                    $options[$attributeItem] = [
                        'id' => reset($arrayOptions) . '-' . $attributeItem,
                        'value' => $values[$attributeItem],
                        //'class' => implode(' ', array_merge(['form-control'], $extendClass))
                    ];
                }

                if ($this->formInline === true) {
                    $label = isset($this->labels[$attributeItem]) ? $this->labels[$attributeItem] : $this->model->getAttributeLabel($attributeItem);
                    $options[$attributeItem]['placeholder'] = $label;
                }
            }
        }
//        $options = $this->options;
//        $options['value'] = $value;

        if ($this->inline === false) {
            // render a text input
            if ($this->hasModel()) {
                foreach ($this->attributes as $attributeItem) {
                    $contents[$attributeItem] = Html::activeTextInput($this->model, $attributeItem, $options[$attributeItem]);
                }
            } else {
                foreach ($this->attributes as $attributeItem) {
                    $contents[$attributeItem] = Html::textInput($this->model, $values[$attributeItem], $options[$attributeItem]);
                }
            }
        } else {
            // render an inline date picker with hidden input
            if ($this->hasModel()) {
                foreach ($this->attributes as $attributeItem) {
                    $contents[$attributeItem] = Html::activeHiddenInput($this->model, $attributeItem, $options[$attributeItem]);
                }
            } else {
                foreach ($this->attributes as $attributeItem) {
                    $contents[$attributeItem] = Html::hiddenInput($this->model, $values[$attributeItem], $options[$attributeItem]);
                }
            }
            $this->clientOptions['defaultDate'] = $value;
            $this->clientOptions['altField'] = '#' . $this->options['id'];
            $contents[] = Html::tag('div', null, $this->containerOptions);
        }

        $return = [];

        foreach ($contents as $attr => $content) {
            $label = isset($this->labels[$attr]) ? $this->labels[$attr] : $this->model->getAttributeLabel($attr);

            if ($this->formInline === false) {
                $ct = Html::tag('label', $label, ['for' => $options[$attr]['id'], 'class' => 'control-label']) . $content;
            } else {
                $ct = $content;
            }

            $opts = [];
            $opts['class'] = 'group-field field-' . $options[$attr]['id'];
            $opts['style'] = 'float: left; width: ' . (100 / count($this->attributes)) . '%;';
            foreach ($this->divOptions as $k => $v) {
                $opts[$k] = isset($opts[$k]) ? implode(' ', [$opts[$k], $v]) : $v;
            }

            $return[] = Html::tag('div', $ct . '<div class="help-block"></div>', $opts);
        }
        return Html::tag('div', implode("\n", $return), ['class' => 'group-datepicker']);
    }

}
