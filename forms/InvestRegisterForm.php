<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 07-Jun-16
 * Time: 5:24 AM
 */
namespace app\forms;

use app\models\Customer;
use app\models\CustomerActivity;
use app\models\CustomerBankAddress;
use app\models\CustomerPackage;
use app\models\investments\InvestCustomer;
use yii\base\Model;

class InvestRegisterForm extends Model
{
    const SCENARIO_ADMIN_REGISTER = 'admin-create';
    const SCENARIO_CUSTOMER_REGISTER = 'customer-create';
    public $username;
    public $password;
    public $passwordConfirmation;
    public $customer_parent_id;
    public $customer_parent_email;
    public $full_name;
    public $phone;
    public $identity_card;
    public $sex;
    public $dob;
    public $email;
    public $address;
    public $nation;
    public $bank_address;
    public $ref_code;


    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\investments\InvestCustomer', 'message' => 'Username is existed.'],
            ['username', 'string', 'min' => 4, 'max' => 50],
            ['username', '\app\components\validators\UsernameValidator'],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            //['email', 'unique', 'targetClass' => '\app\models\Customer', 'message' => 'Email address is existed.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            ['passwordConfirmation', 'required', 'on' => 'default'],
            ['passwordConfirmation', 'compare', 'compareAttribute' => 'password'],

            ['phone', 'required'],
            //['phone', 'unique', 'targetClass' => '\app\models\Customer', 'message' => 'Phone number is existed.'],
            ['phone', '\app\components\validators\PhoneNumberValidator'],

            [['ref_code'], 'required','on' => 'default'],
            [['ref_code'], 'string'],
            [['ref_code'], 'ValidatorCustomerInvite'],

            [['customer_parent_id', 'sex', 'nation'], 'integer'],
            [['customer_parent_id'], 'required','on' => 'default'],
            ['customer_parent_email', 'email'],

            ['full_name', 'filter', 'filter' => 'trim'],
            ['bank_address', 'filter', 'filter' => 'trim'],
            [['full_name', 'nation'], 'required'],

            ['identity_card', 'filter', 'filter' => 'trim'],
            ['identity_card', 'required'],
            //['identity_card', 'unique', 'targetClass' => '\app\models\Customer', 'message' => 'Indentity card number is existed.'],

            [['dob'], 'app\components\validators\DateValidator'],

            [['address'], 'string'],
            [['bank_address'], 'safe'],


        ];

    }

    public function attributeLabels()
    {
        return [
            'username' => \Yii::t('system', 'Tên đăng nhập'),
            'password' =>  \Yii::t('system', 'Mật khẩu'),
            'passwordConfirmation' =>\Yii::t('system', 'Xác nhận mật khẩu'),
            'email' => \Yii::t('system', 'Email'),
            'phone' => \Yii::t('system', 'Số điện thoại'),
            'dob' => \Yii::t('system','Ngày sinh'),
            'full_name' => \Yii::t('system','Họ tên'),
            'customer_parent_id' =>\Yii::t('system','Người giới thiệu'),
            'customer_parent_email' => \Yii::t('system','Email giới thiệu'),
            'sex' =>  \Yii::t('system','Giới tính'),
            'address' =>\Yii::t('system','Địa chỉ'),
            'identity_card' => \Yii::t('system','Số CMTND'),
            'nation' =>  \Yii::t('system','Quốc gia'),
            'bank_address' => \Yii::t('system','Địa chỉ bit coin'),
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_ADMIN_REGISTER] = [
            'username',
            'password',
            'email',
            'phone',
            'customer_parent_id',
            'sex',
            'full_name',
            'nation',
            'identity_card',
            'dob',
            'address',
            'bank_address',
            'is_active',
            'created_type',
            'created_by'
        ];
        return $scenarios;
    }

    public function ValidatorCustomerInvite($attribute, $params)
    {
        if ($this->$attribute) {
            $customer = InvestCustomer::findOne(['ref_code' => $this->$attribute]);
            if (!$customer) {
                $this->addError($attribute, \Yii::t('system', 'Mã giới thiệu không tồn tại.'));
            } else {
                $this->customer_parent_id = $customer->id;
            }
        }
    }


    public function createCustomer($isAdmin = false)
    {
        $customer = new InvestCustomer();
        $customer->username = $this->username;
        $customer->password_hash = $this->password;
        $customer->full_name = $this->full_name;
        $customer->date_join = time();
        $customer->identity_card = $this->identity_card;
        $customer->email = $this->email;
        $customer->dob = $this->dob;
        $customer->parent_id = ($this->customer_parent_id) ? $this->customer_parent_id : 0;
        $customer->phone = $this->phone;
        $customer->sex = ($this->sex)?$this->sex:0;
//        $customer->address = $this->address;
        $customer->nation = $this->nation;
        $customer->setPassword($customer->password_hash);
        $customer->generateAuthKey();
        $customer->createRefCode();
        $customer->status = InvestCustomer::STATUS_ACTIVE;
        $customer->is_active = InvestCustomer::IS_ACTIVE_PENDING;
        if ($isAdmin) {
            $customer->created_type = InvestCustomer::TYPE_ADMIN;
            $customer->is_active = InvestCustomer::IS_ACTIVE_COMPLETED;

        } else {
            $customer->created_type = InvestCustomer::TYPE_CUSTOMER;
        }
        if (!\Yii::$app->user->isGuest) {
            $customer->created_by = \Yii::$app->user->identity->username;
        }
        if ($customer->save()) {
//            CustomerActivity::systemCreate($customer->id, $customer->username, CustomerActivity::TYPE_DANG_KY_TAI_KHOAN, 'Đăng ký thành công tài khoản', $customer->attributes);
            $customer->createInvestWallet();
//            if ($isAdmin && $this->bank_address) {
//                $bankAddress = new CustomerBankAddress();
//                $bankAddress->customer_id = $customer->id;
//                $bankAddress->bank_address = $this->bank_address;
//                $bankAddress->status = CustomerBankAddress::STATUS_ACTIVE;
//                $bankAddress->save();
//            }
        } else {
        }
        return $customer;
    }


}
