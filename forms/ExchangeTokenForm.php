<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 16-Jul-16
 * Time: 1:45 AM
 */
namespace app\forms;

use app\models\Customer;
use app\models\CustomerOrder;
use app\models\CustomerToken;
use app\models\CustomerTokenTransaction;
use app\models\CustomerTransaction;
use app\models\investments\InvestCustomer;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Query;

class ExchangeTokenForm extends Model
{
    public $customer_id;
    public $quantity;
    public $type;
    public $customer_receiver_id;
    public $customer_username;
    public $password;


    public function rules()
    {
        return [
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['password', 'validateWalletPassword'],

            ['quantity', 'required'],
            ['quantity', 'integer', 'min' => 1],
            ['quantity', 'ValidateQuantityToken'],

            [['customer_username'], 'required'],
            [['customer_username'], 'string'],
            [['customer_username'], 'ValidatorCustomer'],

            [['customer_id'], 'string'],

            [['customer_receiver_id', 'type'], 'integer'],
            [['customer_receiver_id'], 'required'],

        ];
    }


    public function ValidatorCustomer($attribute, $params)
    {

        if ($this->$attribute) {
            switch ($this->type) {
                case CustomerToken::TYPE_INVEST:
                    $customer = InvestCustomer::findOne(['username' => $this->$attribute]);
                    break;
                case CustomerToken::TYPE_CUSTOMER:
                    $customer = Customer::findOne(['username' => $this->$attribute]);
                    break;
                default:
                    $customer = null;
                    break;
            }
            if (!$customer) {
                $this->addError($attribute, \Yii::t('systems', 'This username is not existed.'));
            } else {
                $this->customer_receiver_id = $customer->id;
            }
        }
    }

    public function ValidateQuantityToken($attribute, $params)
    {
        if ($this->$attribute) {
            $tokens = \Yii::$app->user->identity->countCurrentToken();
            if ($tokens > 0 && $tokens >= $this->$attribute) {

            } else {
                $this->addError($attribute, \Yii::t('system', 'Nuber of token lest than your quantity input'));
            }
        }
    }

    public function validateWalletPassword($attribute, $params)
    {
        if (!\Yii::$app->user->identity->validatePassword($this->$attribute)) {
            $this->addError($attribute, 'Password not exactly.');
        }
    }

    public function process()
    {
        $result = [];
        if (!in_array($this->type, [CustomerToken::TYPE_CUSTOMER, CustomerToken::TYPE_INVEST])) {
            $result = [
                'status' => 'error',
                'code' => 200,
                'message' => 'Token module type not exactly',
            ];

        }
        //xử lý token
        $query = (new Query());
        $query->select('token');
        $query->from(CustomerToken::tableName());
        $query->where([
            'customer_id' => \Yii::$app->user->identity->id,
            'status' => CustomerToken::STATUS_PENDING,
        ]);
        if ($this->type == CustomerToken::TYPE_CUSTOMER) {
            $query->andWhere(['type' => CustomerToken::TYPE_CUSTOMER]);
        } elseif ($this->type == CustomerToken::TYPE_INVEST) {
            $query->andWhere(['type' => CustomerToken::TYPE_INVEST]);
        }
        $query->limit($this->quantity);
        $tokens = $query->column();
        if (count($tokens) == $this->quantity) {
            $transaction = \Yii::$app->db->beginTransaction();
            $msgtoken = [];
            //create customer request

            try {
                $cusOrder = new CustomerOrder();
                $cusOrder->customer_id = \Yii::$app->user->identity->id;
                $cusOrder->customer_username = \Yii::$app->user->identity->username;
                $cusOrder->quantity = $this->quantity;
                $cusOrder->status = CustomerOrder::STATUS_PENDING;
                $cusOrder->type = CustomerOrder::TYPE_EXCHANGE_TOKEN;
                $cusOrder->attachment = $this->customer_username;
                if ($this->type == CustomerToken::TYPE_CUSTOMER) {
                    $cusOrder->type_module = CustomerOrder::TYPE_MODULE_CUSTOMER;
                } elseif ($this->type == CustomerToken::TYPE_INVEST) {
                    $cusOrder->type_module = CustomerOrder::TYPE_MODULE_INVEST;
                }
                if ($cusOrder->save()) {
                    $dataResult = \Yii::$app->db->createCommand()->update(CustomerToken::tableName(), ['customer_id' => $this->customer_receiver_id], ['token' => $tokens])->execute();
                    if ($dataResult) {
                        $typeModule=0;
                        if ($this->type == CustomerToken::TYPE_CUSTOMER) {
                           $typeModule=CustomerTokenTransaction::TYPE_MODULE_CUSTOMER;
                        } elseif ($this->type == CustomerToken::TYPE_INVEST) {
                            $typeModule=CustomerTokenTransaction::TYPE_MODULE_INVEST;
                        }
                        //Luưu transaction
                        $cusTokenTransaction = [];
                        $cusTokenTransaction[] = [
                            \Yii::$app->user->identity->id,
                            $cusOrder->id,
                            $typeModule,
                            $this->quantity,
                            CustomerTokenTransaction::SIGN_SUB,
                            $this->customer_username,
                            json_encode($tokens),
                            CustomerTokenTransaction::TYPE_SENT,
                            time(),
                            time()
                        ];
                        $cusTokenTransaction[] = [
                            $this->customer_receiver_id,
                            $cusOrder->id,
                            $typeModule,
                            $this->quantity,
                            CustomerTokenTransaction::SIGN_ADD,
                            \Yii::$app->user->identity->username,
                            json_encode($tokens),
                            CustomerTokenTransaction::TYPE_RECEIVED,
                            time(),
                            time()
                        ];
                        $batchInsert = \Yii::$app->db->createCommand()->batchInsert(
                            CustomerTokenTransaction::tableName(),
                            [
                                'customer_id',
                                'bill_id',
                                'type_module',
                                'quantity',
                                'sign',
                                'receiver',
                                'content',
                                'type',
                                'created_at',
                                'updated_at',
                            ], $cusTokenTransaction)->execute();
                        if ($batchInsert) {
                            $cusOrder->status = CustomerOrder::STATUS_COMPLETED;
                            if (!$cusOrder->save(true, ['status', 'updated_at'])) {
                                throw new Exception('Require sent token not completed');
                            }
                            $transaction->commit();
                            $result = [
                                'status' => 'success',
                                'code' => 200,
                                'message' => 'Completed',
                            ];


                        }
                    }
                } else {
                    $result = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'Sent token completed',
                    ];
                }

            } catch (\Exception $ex) {
                $transaction->rollBack();
                $result = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => $ex->getMessage(),
                ];
            }

        } else {
            $result = [
                'status' => 'error',
                'code' => 400,
                'message' => 'You total token not enough to sent',
            ];
        }
        return $result;

    }

}