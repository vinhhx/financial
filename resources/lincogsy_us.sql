-- MySQL dump 10.13  Distrib 5.5.45-37.4, for Linux (x86_64)
--
-- Host: localhost    Database: lincogsy_us
-- ------------------------------------------------------
-- Server version	5.5.45-37.4-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES ('system-admin','1',1450480429);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES ('customer',2,'Customer Controller',NULL,NULL,1450480390,1450480390),('customer/change-password',2,'Customer Controller Action Change Password',NULL,NULL,1450480390,1450480390),('customer/create',2,'Customer Controller Action Create',NULL,NULL,1450480390,1450480390),('customer/delete',2,'Customer Controller Action Delete',NULL,NULL,1450480390,1450480390),('customer/index',2,'Customer Controller Action Index',NULL,NULL,1450480390,1450480390),('customer/update',2,'Customer Controller Action Update',NULL,NULL,1450480390,1450480390),('default',2,'Default Controller',NULL,NULL,1450480390,1450480390),('default/error',2,'Default Controller Action Error',NULL,NULL,1450480390,1450480390),('default/index',2,'Default Controller Action Index',NULL,NULL,1450480390,1450480390),('default/login',2,'Default Controller Action Login',NULL,NULL,1450480390,1450480390),('default/logout',2,'Default Controller Action Logout',NULL,NULL,1450480390,1450480390),('package',2,'Package Controller',NULL,NULL,1450480390,1450480390),('package/create',2,'Package Controller Action Create',NULL,NULL,1450480390,1450480390),('package/delete',2,'Package Controller Action Delete',NULL,NULL,1450480390,1450480390),('package/index',2,'Package Controller Action Index',NULL,NULL,1450480390,1450480390),('package/update',2,'Package Controller Action Update',NULL,NULL,1450480390,1450480390),('package/view',2,'Package Controller Action View',NULL,NULL,1450480390,1450480390),('permission',2,'Permission Controller',NULL,NULL,1450480390,1450480390),('permission/index',2,'Permission Controller Action Index',NULL,NULL,1450480390,1450480390),('permission/render-action',2,'Permission Controller Action Render Action',NULL,NULL,1450480390,1450480390),('system-admin',1,'Quyền thực thi tất cả tác vụ của hệ thống',NULL,NULL,1450480390,1450480390),('tool',2,'Tool Controller',NULL,NULL,1450480390,1450480390),('tool/addmountbyweek',2,'Tool Controller Action Addmountbyweek',NULL,NULL,1450480390,1450480390),('transaction',2,'Transaction Controller',NULL,NULL,1450480390,1450480390),('transaction/create',2,'Transaction Controller Action Create',NULL,NULL,1450480390,1450480390),('transaction/delete',2,'Transaction Controller Action Delete',NULL,NULL,1450480390,1450480390),('transaction/index',2,'Transaction Controller Action Index',NULL,NULL,1450480390,1450480390),('transaction/update',2,'Transaction Controller Action Update',NULL,NULL,1450480390,1450480390),('transaction/view',2,'Transaction Controller Action View',NULL,NULL,1450480390,1450480390),('user',2,'User Controller',NULL,NULL,1450480390,1450480390),('user/create',2,'User Controller Action Create',NULL,NULL,1450480390,1450480390),('user/index',2,'User Controller Action Index',NULL,NULL,1450480390,1450480390),('user/permission',2,'User Controller Action Permission',NULL,NULL,1450480390,1450480390),('user/update',2,'User Controller Action Update',NULL,NULL,1450480390,1450480390),('user/view',2,'User Controller Action View',NULL,NULL,1450480390,1450480390),('withdrawal',2,'Withdrawal Controller',NULL,NULL,1450480390,1450480390),('withdrawal/accept-request',2,'Withdrawal Controller Action Accept Request',NULL,NULL,1450480390,1450480390),('withdrawal/create',2,'Withdrawal Controller Action Create',NULL,NULL,1450480390,1450480390),('withdrawal/delete',2,'Withdrawal Controller Action Delete',NULL,NULL,1450480390,1450480390),('withdrawal/index',2,'Withdrawal Controller Action Index',NULL,NULL,1450480390,1450480390),('withdrawal/reject-request',2,'Withdrawal Controller Action Reject Request',NULL,NULL,1450480390,1450480390),('withdrawal/update',2,'Withdrawal Controller Action Update',NULL,NULL,1450480390,1450480390),('withdrawal/view',2,'Withdrawal Controller Action View',NULL,NULL,1450480390,1450480390);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` (`parent`, `child`) VALUES ('customer','customer/change-password'),('customer','customer/create'),('customer','customer/delete'),('customer','customer/index'),('customer','customer/update'),('default','default/error'),('default','default/index'),('default','default/login'),('default','default/logout'),('package','package/create'),('package','package/delete'),('package','package/index'),('package','package/update'),('package','package/view'),('permission','permission/index'),('permission','permission/render-action'),('tool','tool/addmountbyweek'),('transaction','transaction/create'),('transaction','transaction/delete'),('transaction','transaction/index'),('transaction','transaction/update'),('transaction','transaction/view'),('user','user/create'),('user','user/index'),('user','user/permission'),('user','user/update'),('user','user/view'),('withdrawal','withdrawal/accept-request'),('withdrawal','withdrawal/create'),('withdrawal','withdrawal/delete'),('withdrawal','withdrawal/index'),('withdrawal','withdrawal/reject-request'),('withdrawal','withdrawal/update'),('withdrawal','withdrawal/view');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;



DROP TABLE IF EXISTS `customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset` varchar(255) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `amount` int(11) unsigned DEFAULT '0',
  `status` smallint(2) DEFAULT '1',
  `created_type` smallint(2) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `date_join` date DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `sex` smallint(2) DEFAULT '0',
  `dob` date DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `nation` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `last_auto_increase` date DEFAULT NULL COMMENT 'lan cuoi he thogn tu cong tien',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`id`, `username`, `auth_key`, `password_hash`, `password_reset`, `package_id`, `parent_id`, `amount`, `status`, `created_type`, `created_by`, `date_join`, `full_name`, `sex`, `dob`, `phone`, `email`, `nation`, `address`, `last_auto_increase`, `created_at`, `updated_at`) VALUES (1,'kimoanh57','81aaBMd9y7OQo5lIzPAhxrZBICAcyX3Y','$2y$13$.zgkGv5mu8DR7vzps4Mzb.0Wx/r0Hy.ARgM0AfPywU92t4KbXJ71q',NULL,1,NULL,2620,1,NULL,NULL,'2015-09-02','Vu Thi Kim Oanh',2,NULL,'','',NULL,'','2015-09-02',1450480737,1450482750),(2,'trantua58','TBctEiejtEARAlqBtFplW-UjthmqwvlU','$2y$13$3UMo95ItFc/mwFTe1Rc.PO6N8a5/NrtjbQUkLK..yjzpZZGrmtbqC',NULL,2,1,1560,1,NULL,NULL,'2015-09-04','Tran Thi Tu',2,NULL,'','',NULL,'','2015-09-04',1450480826,1450482108),(3,'kimoanh68','UkLl7MdkCEWABB7PppFc4a0HjZMt_oAU','$2y$13$.jHfx6pGM.P/xTPTeJTl7O7ZJSO7Otosz85bHAv9cyQsk9xswH9Fu',NULL,3,2,0,1,NULL,NULL,'2015-09-06','Vu Thi Kim Oanh',2,NULL,'','',NULL,'','2015-09-06',1450480936,1450480936),(4,'kimthinh61','KFnW3dhOxmdhuKdRkjK5PGFXyTrEsEBy','$2y$13$1KQXZSXU.XI9sYjkOagxxOzkPvj2Fy8fWMoPkFaNt9z0H3neQVWTy',NULL,1,1,475,1,NULL,NULL,'2015-09-04','Thai Thi Kim Trinh',2,NULL,'','',NULL,'','2015-09-04',1450481019,1450482525),(5,'phihung54','3go5gBfaw2hHN_o3h9MHZPrWgR1pUKJa','$2y$13$vBZb2FBWI4/oBc3BxJ7Y2OKSwPpXfBAMZxKbtLTRwkowJMl1c9G1K',NULL,1,1,180,1,NULL,NULL,'2015-09-02','Ha Phi Hung',1,NULL,'','',NULL,'','2015-09-02',1450481083,1450481657),(6,'ledang49','Y0JS_3DHvMkk5YnpQp708ILjV1WtvVWf','$2y$13$ZqcXyllRbCNuSl1owhWA7ef66Y8x5cYTOD8y4Vx4bfytkI/9.7jMq',NULL,1,5,0,1,NULL,NULL,'2015-09-04','Le Thi Dang',2,NULL,'','',NULL,'','2015-09-04',1450481160,1450481160),(7,'trungkien77','Szh7QD4sR6OIYWH6oqqkuO856jXC1Qyy','$2y$13$OMYIajcFwx.0uDeDmq60wesLaTciKSGTov7GUjvmrTV5S2rbhVMz2',NULL,1,5,0,1,NULL,NULL,'2015-09-04','Nguyen Trung Kien',1,NULL,'','',NULL,'','2015-09-04',1450481230,1450481230),(8,'thinh50','8SsJNg3GucQQBPZNxBvMlq3uaREsCG9F','$2y$13$MuVHRiq0bIjIBT9jgS4Ms.8mBvfpO8bStSE5mnLEmImp58lac7PaW',NULL,1,4,405,1,NULL,NULL,'2015-09-04','Nguyen Thi Thinh',2,NULL,'','',NULL,'','2015-09-04',1450481297,1450482252),(9,'lienhuong46','pQZK7X_KZoW388qfNdJNI2lN8Wdi3JHp','$2y$13$TCH66iE/3.FrZ.bnblhzDeGoGK5xpSrdLcQdhWD8LoKJLDeiEln.G',NULL,2,8,150,1,NULL,NULL,'2015-09-04','Nguyen Thi Lien Huong',2,NULL,'','',NULL,'','2015-09-04',1450481360,1450482252),(10,'thanhha51','fjbW-5nO9PQ8AhPuzCt3cYCA79eRcGTZ','$2y$13$EE9M5m1gyi0/YwOGTIDb1.e1hlCEk4j6bxHNJva2H9m05Dufl0N3O',NULL,1,9,0,1,NULL,NULL,'2015-09-04','Tran Thi Thanh Ha',2,NULL,'','',NULL,'','2015-09-04',1450481429,1450481429),(11,'thanhmai58','6g5Ygsd-l5stlewXdcMA5RGwanBLrIqt','$2y$13$EZ9a07.YGDCr22rKqUDSjuPszBSUMyhnCe9o.RzFF.z010gxPrxHy',NULL,1,8,60,1,NULL,NULL,'2015-09-04','Vu Thi Thanh Mai',2,NULL,'','',NULL,'','2015-09-04',1450481486,1450481549),(12,'phammai52','mI6RuGnhMTMQEcaVySVAIFJLExjAUkSl','$2y$13$iY21V2PeSxYxRGBMa5d9i.B.VhbfwWOSURkG1dh/huvMXRERFd2Ae',NULL,1,11,0,1,NULL,NULL,'2015-09-04','Pham Thi Mai',2,NULL,'','',NULL,'','2015-09-04',1450481549,1450481549),(13,'nguyenxa56','YKid7-Bq6bwrh2_JdxLHvT5ok1DLD8KW','$2y$13$Nxa5.4YDJhViMErYxZJAZOdeSEPeEnyiZtuuPs6.krpQHA9gm/o1e',NULL,1,8,0,1,NULL,NULL,'2015-09-04','Nguyen Thi Xa',2,NULL,'','',NULL,'','2015-09-04',1450481601,1450481601),(14,'hien48','0SpzBsXjw7AZgBWMEML5jaud0qcodJzK','$2y$13$Yp5ffK88zurcV4El.p41S.CY1La6t4KdtKt85htIoei91wNa7BzNO',NULL,1,5,0,1,NULL,NULL,'2015-09-19','Le Thi Hien',2,NULL,'','',NULL,'','2015-09-19',1450481657,1450481657),(15,'thuymai58','H5niL-hPs8LW73bucJR0O3QYkjKO1tn1','$2y$13$zFRw/oIbEo4sZujckT.Ppuycm.8qPvz3oZxJew.cZDipyYnJpf7iS',NULL,1,2,60,1,NULL,NULL,'2015-09-23','Bui Thi Thuy Mai',2,NULL,'','',NULL,'','2015-09-23',1450481736,1450482108),(16,'minhnguyen1','VBJeXvwgnqU_8wJ82nxU_kJlTTsCejsY','$2y$13$yC9KgV/xEucKTsKG9oukxOLOcHaKHsQXCRPRaucTjuAXuTrTRc/I.',NULL,3,2,60,1,NULL,NULL,'2015-10-12','Nguyen Duc Minh',1,NULL,'','',NULL,'','2015-10-12',1450481798,1450481867),(17,'minhnguyen2','lRJsAs2akdNAGmqTTWqmsyYpdVza88Rs','$2y$13$yysb8pln2iWks0fLocGoCO.s57XEd55F/i8fPziurocbKxh9BpUqi',NULL,1,16,0,1,NULL,NULL,'2015-10-17','Nguyen Duc Minh',1,NULL,'','',NULL,'','2015-10-17',1450481866,1450481866),(18,'danhien52','ddye-1vwGv2lbbxUy3B4JNqx6PbpDlgJ','$2y$13$RVTGtieev6Gc9jlQGzk6/Ox44rBAFklfdIHeD8eQ6mZd9H/bHc4jm',NULL,1,4,300,1,NULL,NULL,'2015-09-25','Ngo Dan Hien',2,NULL,'','',NULL,'','2015-09-25',1450481931,1450482525),(19,'trantub58','peZFynpE_CKalgCS_I4eL0cE03yxuS-G','$2y$13$6td0SUr8ZEgsclhUoocSCuc2qjJmGbv9QlAQWYrsxGWFsw.B.wYju',NULL,2,2,0,1,NULL,NULL,'2015-09-24','Tran Thi Tu',2,NULL,'','',NULL,'','2015-09-24',1450481999,1450481999),(20,'trantuc58','OTuUCXC1LR8sUUtryCpbysGrcSEMNnko','$2y$13$yvkUk/U15uPMkO/p.ixycu3R6KAEzpCAcdXY.egWmDgZxwUmHNit6',NULL,1,2,0,1,NULL,NULL,'2015-09-24','Tran Thi Tu',2,NULL,'','',NULL,'','2015-09-24',1450482042,1450482042),(21,'thuymaia58','rH9Zb2_A3Z4vWs6EKLEmruF9Iae6zpN1','$2y$13$bDsgni.4MPuUXgOueoEHd.8BLvQAS6itYbiEwqPxPkrygYzx9LJ.q',NULL,1,15,0,1,NULL,NULL,'2015-09-30','Bui Thi Thuy Mai',2,NULL,'','',NULL,'','2015-09-30',1450482108,1450482108),(22,'dung56','eO-6yxJdSH2MhToLz95K1FOyitNXTkjE','$2y$13$LG/zXsevMtq0bZB.x6ZZU.cFtNKHUUGxNUePHgroApMLw6yaLQswW',NULL,1,9,60,1,NULL,NULL,'2015-09-30','Tran Thi Dung',2,NULL,'','',NULL,'','2015-09-30',1450482195,1450482252),(23,'vunhan68','3suiNl1lgPzfyAZslpSzdGgCdGXuKPSo','$2y$13$IO.ER/IvkifwIvKDGlprluFFWsAl5i5upSqXxdkDhM4FJ1zYPDGr6',NULL,1,22,0,1,NULL,NULL,'2015-10-07','Vu Nhan',2,NULL,'','',NULL,'','2015-10-07',1450482251,1450482251),(24,'dieuthuy62','yeAvyhNVlaxlNNOpHX7lBELDlkNvgZmY','$2y$13$VpBlmhFyhR5VOTOEYdJF.u4hsFatMnGsqHBOmJD2npfyNzinf9JkO',NULL,2,18,0,1,NULL,NULL,'2015-10-13','Le Dieu Thuy',2,NULL,'','',NULL,'','2015-10-13',1450482321,1450482321),(25,'thuhuyen','i-uc1Yo6uFo-fZ1canlhEQAZaz177atC','$2y$13$2xlcX95Uqi9GCcMfN5jntev5DSMErdj1jPWg5ey9YB2nnLXGFhzbe',NULL,1,18,0,1,NULL,NULL,'2015-10-12','Le Thu Huyen',2,NULL,'','',NULL,'','2015-10-12',1450482428,1450482428),(26,'huong47','9M_3O1JxCBPve3RQSHCLLQc_OjFPp91y','$2y$13$K2KSRHu73sO6mMEPSUCSf.j2rGDql1GwHxschySzvcZJbLBZRxiMe',NULL,1,1,0,1,NULL,NULL,'2015-10-15','Nguyen Thi Huong',2,NULL,'','',NULL,'','2015-10-15',1450482478,1450482478),(27,'chimai55','Dlj3b4Fdva7CUwVpUtgP2lKhcqDLaJX1','$2y$13$e5C5mGHYeV.vXCBmRsERzOy9CLSM/hipfbh0s0D8YxYTSV/D35gUC',NULL,1,18,0,1,NULL,NULL,'2015-10-15','Luong Chi Mai',2,NULL,'','',NULL,'','2015-10-15',1450482525,1450482525),(28,'thimai58','Ej5BwZgLWqGojMh2Ck9OT8ZXWfouqbLe','$2y$13$mh3RQt7cu6D2XNqLhIkbVuFhSHkafdHJaJ7JCcbSddcXDQS5QOuIW',NULL,3,1,600,1,NULL,NULL,'2015-10-15','Le Thi Mai',2,NULL,'','',NULL,'','2015-10-15',1450482577,1450482692),(29,'phuongdung59','JE3dqLd9bvqGKrXQpPhyQn2klQKvUKeB','$2y$13$5aP0biy73qzuoCaLoX7jkO6QFEIKMau3P/Cb3KMehcsGHatZLu99S',NULL,1,1,0,1,NULL,NULL,'2015-10-22','To Phuong Dung',2,NULL,'','',NULL,'','2015-10-22',1450482621,1450482621),(30,'viettrieu52','2e4dQ0WhF1SFOYtTHKJldDHylzw7Nrqw','$2y$13$t53kme8rKeVsu/ijkv5qn.KmM7lKPwjyE9DxDpA4e.wBogY/imjpG',NULL,3,28,0,1,NULL,NULL,'2015-10-25','Nguyen Thi Viet Trieu',2,NULL,'','',NULL,'','2015-10-25',1450482692,1450482692),(31,'huonglien60','34yH34IB8xLoOA3fg5pZwDqUSEYqJkUZ','$2y$13$ri7xrnXsPB/bK.h.8YwZMu8D4p8c/JTWDwNAXvIYfDhpJ09EUsElO',NULL,2,1,0,1,NULL,NULL,'2015-10-31','Le Thi Huong Lien',2,NULL,'','',NULL,'','2015-10-31',1450482750,1450482750);
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_case`
--

DROP TABLE IF EXISTS `customer_case`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_case` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `parent_id` smallint(4) unsigned NOT NULL DEFAULT '0',
  `level` smallint(2) DEFAULT '0',
  `status` smallint(2) unsigned DEFAULT '1',
  `created_at` int(11) DEFAULT '0',
  `updated_at` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `CUSTOMER_PARENT` (`customer_id`,`parent_id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_case`
--

LOCK TABLES `customer_case` WRITE;
/*!40000 ALTER TABLE `customer_case` DISABLE KEYS */;
INSERT INTO `customer_case` (`id`, `customer_id`, `parent_id`, `level`, `status`, `created_at`, `updated_at`) VALUES (1,2,1,1,1,1450480827,1450480827),(2,3,1,2,1,1450480936,1450480936),(3,3,2,1,1,1450480936,1450480936),(4,4,1,1,1,1450481019,1450481019),(5,5,1,1,1,1450481083,1450481083),(6,6,1,2,1,1450481160,1450481160),(7,6,5,1,1,1450481160,1450481160),(8,7,1,2,1,1450481230,1450481230),(9,7,5,1,1,1450481230,1450481230),(10,8,1,2,1,1450481297,1450481297),(11,8,4,1,1,1450481297,1450481297),(12,9,1,3,1,1450481360,1450481360),(13,9,4,2,1,1450481360,1450481360),(14,9,8,1,1,1450481360,1450481360),(15,10,1,4,1,1450481429,1450481429),(16,10,4,3,1,1450481429,1450481429),(17,10,8,2,1,1450481429,1450481429),(18,10,9,1,1,1450481429,1450481429),(19,11,1,3,1,1450481487,1450481487),(20,11,4,2,1,1450481487,1450481487),(21,11,8,1,1,1450481487,1450481487),(22,12,1,4,1,1450481549,1450481549),(23,12,4,3,1,1450481549,1450481549),(24,12,8,2,1,1450481549,1450481549),(25,12,11,1,1,1450481549,1450481549),(26,13,1,3,1,1450481601,1450481601),(27,13,4,2,1,1450481601,1450481601),(28,13,8,1,1,1450481601,1450481601),(29,14,1,2,1,1450481657,1450481657),(30,14,5,1,1,1450481657,1450481657),(31,15,1,2,1,1450481736,1450481736),(32,15,2,1,1,1450481736,1450481736),(33,16,1,2,1,1450481798,1450481798),(34,16,2,1,1,1450481798,1450481798),(35,17,1,3,1,1450481866,1450481866),(36,17,2,2,1,1450481866,1450481866),(37,17,16,1,1,1450481866,1450481866),(38,18,1,2,1,1450481931,1450481931),(39,18,4,1,1,1450481931,1450481931),(40,19,1,2,1,1450481999,1450481999),(41,19,2,1,1,1450481999,1450481999),(42,20,1,2,1,1450482042,1450482042),(43,20,2,1,1,1450482042,1450482042),(44,21,1,3,1,1450482108,1450482108),(45,21,2,2,1,1450482108,1450482108),(46,21,15,1,1,1450482108,1450482108),(47,22,1,4,1,1450482195,1450482195),(48,22,4,3,1,1450482195,1450482195),(49,22,8,2,1,1450482195,1450482195),(50,22,9,1,1,1450482195,1450482195),(51,23,1,5,1,1450482252,1450482252),(52,23,4,4,1,1450482252,1450482252),(53,23,8,3,1,1450482252,1450482252),(54,23,9,2,1,1450482252,1450482252),(55,23,22,1,1,1450482252,1450482252),(56,24,1,3,1,1450482321,1450482321),(57,24,4,2,1,1450482321,1450482321),(58,24,18,1,1,1450482321,1450482321),(59,25,1,3,1,1450482428,1450482428),(60,25,4,2,1,1450482428,1450482428),(61,25,18,1,1,1450482428,1450482428),(62,26,1,1,1,1450482478,1450482478),(63,27,1,3,1,1450482525,1450482525),(64,27,4,2,1,1450482525,1450482525),(65,27,18,1,1,1450482525,1450482525),(66,28,1,1,1,1450482577,1450482577),(67,29,1,1,1,1450482621,1450482621),(68,30,1,2,1,1450482692,1450482692),(69,30,28,1,1,1450482692,1450482692),(70,31,1,1,1,1450482750,1450482750);
/*!40000 ALTER TABLE `customer_case` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_require_cashout`
--

DROP TABLE IF EXISTS `customer_require_cashout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_require_cashout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned DEFAULT '0',
  `customer_username` varchar(255) DEFAULT NULL,
  `amount` int(11) NOT NULL DEFAULT '0',
  `status` smallint(2) NOT NULL DEFAULT '1',
  `approved_by` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_require_cashout`
--

LOCK TABLES `customer_require_cashout` WRITE;
/*!40000 ALTER TABLE `customer_require_cashout` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_require_cashout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customer_transaction`
--

DROP TABLE IF EXISTS `customer_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customer_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `customer_username` varchar(45) DEFAULT NULL,
  `amount` int(10) unsigned DEFAULT '0',
  `bill_id` int(10) unsigned DEFAULT '0' COMMENT 'mã phiếu',
  `type` smallint(2) unsigned DEFAULT '0',
  `status` smallint(2) unsigned DEFAULT '0',
  `sign` smallint(2) unsigned DEFAULT NULL,
  `balance_before` int(10) unsigned DEFAULT NULL,
  `balance_after` int(10) unsigned DEFAULT NULL,
  `created_at` int(10) unsigned DEFAULT NULL,
  `updated_at` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customer_transaction`
--

LOCK TABLES `customer_transaction` WRITE;
/*!40000 ALTER TABLE `customer_transaction` DISABLE KEYS */;
INSERT INTO `customer_transaction` (`id`, `customer_id`, `customer_username`, `amount`, `bill_id`, `type`, `status`, `sign`, `balance_before`, `balance_after`, `created_at`, `updated_at`) VALUES (1,1,'kimoanh57',180,0,3,1,1,0,180,1450480827,1450480827),(2,1,'kimoanh57',300,0,3,1,1,180,480,1450480936,1450480936),(3,2,'trantua58',600,0,3,1,1,0,600,1450480936,1450480936),(4,1,'kimoanh57',60,0,3,1,1,480,540,1450481019,1450481019),(5,1,'kimoanh57',60,0,3,1,1,540,600,1450481083,1450481083),(6,1,'kimoanh57',30,0,3,1,1,600,630,1450481160,1450481160),(7,5,'phihung54',60,0,3,1,1,0,60,1450481160,1450481160),(8,1,'kimoanh57',30,0,3,1,1,630,660,1450481230,1450481230),(9,5,'phihung54',60,0,3,1,1,60,120,1450481230,1450481230),(10,1,'kimoanh57',30,0,3,1,1,660,690,1450481297,1450481297),(11,4,'kimthinh61',60,0,3,1,1,0,60,1450481297,1450481297),(12,1,'kimoanh57',45,0,3,1,1,690,735,1450481360,1450481360),(13,4,'kimthinh61',90,0,3,1,1,60,150,1450481360,1450481360),(14,8,'thinh50',180,0,3,1,1,0,180,1450481360,1450481360),(15,1,'kimoanh57',10,0,3,1,1,735,745,1450481429,1450481429),(16,4,'kimthinh61',15,0,3,1,1,150,165,1450481430,1450481430),(17,8,'thinh50',30,0,3,1,1,180,210,1450481430,1450481430),(18,9,'lienhuong46',60,0,3,1,1,0,60,1450481430,1450481430),(19,1,'kimoanh57',15,0,3,1,1,745,760,1450481487,1450481487),(20,4,'kimthinh61',30,0,3,1,1,165,195,1450481487,1450481487),(21,8,'thinh50',60,0,3,1,1,210,270,1450481487,1450481487),(22,1,'kimoanh57',10,0,3,1,1,760,770,1450481549,1450481549),(23,4,'kimthinh61',15,0,3,1,1,195,210,1450481549,1450481549),(24,8,'thinh50',30,0,3,1,1,270,300,1450481549,1450481549),(25,11,'thanhmai58',60,0,3,1,1,0,60,1450481549,1450481549),(26,1,'kimoanh57',15,0,3,1,1,770,785,1450481601,1450481601),(27,4,'kimthinh61',30,0,3,1,1,210,240,1450481601,1450481601),(28,8,'thinh50',60,0,3,1,1,300,360,1450481601,1450481601),(29,1,'kimoanh57',30,0,3,1,1,785,815,1450481657,1450481657),(30,5,'phihung54',60,0,3,1,1,120,180,1450481657,1450481657),(31,1,'kimoanh57',30,0,3,1,1,815,845,1450481736,1450481736),(32,2,'trantua58',60,0,3,1,1,600,660,1450481736,1450481736),(33,1,'kimoanh57',300,0,3,1,1,845,1145,1450481798,1450481798),(34,2,'trantua58',600,0,3,1,1,660,1260,1450481798,1450481798),(35,1,'kimoanh57',15,0,3,1,1,1145,1160,1450481867,1450481867),(36,2,'trantua58',30,0,3,1,1,1260,1290,1450481867,1450481867),(37,16,'Nguyen Duc Minh',60,0,3,1,1,0,60,1450481867,1450481867),(38,1,'kimoanh57',30,0,3,1,1,1160,1190,1450481931,1450481931),(39,4,'kimthinh61',60,0,3,1,1,240,300,1450481931,1450481931),(40,1,'kimoanh57',90,0,3,1,1,1190,1280,1450481999,1450481999),(41,2,'trantua58',180,0,3,1,1,1290,1470,1450481999,1450481999),(42,1,'kimoanh57',30,0,3,1,1,1280,1310,1450482042,1450482042),(43,2,'trantua58',60,0,3,1,1,1470,1530,1450482042,1450482042),(44,1,'kimoanh57',15,0,3,1,1,1310,1325,1450482108,1450482108),(45,2,'trantua58',30,0,3,1,1,1530,1560,1450482108,1450482108),(46,15,'thuymai58',60,0,3,1,1,0,60,1450482108,1450482108),(47,1,'kimoanh57',10,0,3,1,1,1325,1335,1450482195,1450482195),(48,4,'kimthinh61',15,0,3,1,1,300,315,1450482195,1450482195),(49,8,'thinh50',30,0,3,1,1,360,390,1450482195,1450482195),(50,9,'lienhuong46',60,0,3,1,1,60,120,1450482195,1450482195),(51,1,'kimoanh57',10,0,3,1,1,1335,1345,1450482252,1450482252),(52,4,'kimthinh61',10,0,3,1,1,315,325,1450482252,1450482252),(53,8,'thinh50',15,0,3,1,1,390,405,1450482252,1450482252),(54,9,'lienhuong46',30,0,3,1,1,120,150,1450482252,1450482252),(55,22,'dung56',60,0,3,1,1,0,60,1450482252,1450482252),(56,1,'kimoanh57',45,0,3,1,1,1345,1390,1450482321,1450482321),(57,4,'kimthinh61',90,0,3,1,1,325,415,1450482321,1450482321),(58,18,'danhien52',180,0,3,1,1,0,180,1450482321,1450482321),(59,1,'kimoanh57',15,0,3,1,1,1390,1405,1450482428,1450482428),(60,4,'kimthinh61',30,0,3,1,1,415,445,1450482428,1450482428),(61,18,'danhien52',60,0,3,1,1,180,240,1450482428,1450482428),(62,1,'kimoanh57',60,0,3,1,1,1405,1465,1450482478,1450482478),(63,1,'kimoanh57',15,0,3,1,1,1465,1480,1450482525,1450482525),(64,4,'kimthinh61',30,0,3,1,1,445,475,1450482525,1450482525),(65,18,'danhien52',60,0,3,1,1,240,300,1450482525,1450482525),(66,1,'kimoanh57',600,0,3,1,1,1480,2080,1450482578,1450482578),(67,1,'kimoanh57',60,0,3,1,1,2080,2140,1450482621,1450482621),(68,1,'kimoanh57',300,0,3,1,1,2140,2440,1450482692,1450482692),(69,28,'thimai58',600,0,3,1,1,0,600,1450482692,1450482692),(70,1,'kimoanh57',180,0,3,1,1,2440,2620,1450482751,1450482751);
/*!40000 ALTER TABLE `customer_transaction` ENABLE KEYS */;
UNLOCK TABLES;



DROP TABLE IF EXISTS `log_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` smallint(6) unsigned DEFAULT '0',
  `params` text,
  `user_id` int(11) unsigned DEFAULT '0',
  `user_username` varchar(255) DEFAULT NULL,
  `user_type` smallint(1) unsigned DEFAULT '0',
  `user_ip` varchar(20) DEFAULT NULL,
  `object_id` int(11) unsigned DEFAULT '0',
  `object_table` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `log_action`
--

LOCK TABLES `log_action` WRITE;
/*!40000 ALTER TABLE `log_action` DISABLE KEYS */;
INSERT INTO `log_action` (`id`, `type`, `params`, `user_id`, `user_username`, `user_type`, `user_ip`, `object_id`, `object_table`, `created_at`, `updated_at`) VALUES (1,1,'{\"id\":1,\"username\":\"kimoanh57\",\"auth_key\":\"81aaBMd9y7OQo5lIzPAhxrZBICAcyX3Y\",\"password_hash\":\"$2y$13$.zgkGv5mu8DR7vzps4Mzb.0Wx\\/r0Hy.ARgM0AfPywU92t4KbXJ71q\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-02\",\"full_name\":\"Vu Thi Kim Oanh\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450480737,\"updated_at\":1450480737}',1,'admin',1,'171.234.217.5',1,'customer',1450480737,1450480737),(2,1,'{\"id\":2,\"username\":\"trantua58\",\"auth_key\":\"TBctEiejtEARAlqBtFplW-UjthmqwvlU\",\"password_hash\":\"$2y$13$3UMo95ItFc\\/mwFTe1Rc.PO6N8a5\\/NrtjbQUkLK..yjzpZZGrmtbqC\",\"password_reset\":null,\"package_id\":\"2\",\"parent_id\":\"1\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-04\",\"full_name\":\"Tran Thi Tu\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450480826,\"updated_at\":1450480826}',1,'admin',1,'171.234.217.5',2,'customer',1450480827,1450480827),(3,1,'{\"id\":3,\"username\":\"kimoanh68\",\"auth_key\":\"UkLl7MdkCEWABB7PppFc4a0HjZMt_oAU\",\"password_hash\":\"$2y$13$.jHfx6pGM.P\\/xTPTeJTl7O7ZJSO7Otosz85bHAv9cyQsk9xswH9Fu\",\"password_reset\":null,\"package_id\":\"3\",\"parent_id\":\"2\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-06\",\"full_name\":\"Vu Thi Kim Oanh\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450480936,\"updated_at\":1450480936}',1,'admin',1,'171.234.217.5',3,'customer',1450480936,1450480936),(4,1,'{\"id\":4,\"username\":\"kimthinh61\",\"auth_key\":\"KFnW3dhOxmdhuKdRkjK5PGFXyTrEsEBy\",\"password_hash\":\"$2y$13$1KQXZSXU.XI9sYjkOagxxOzkPvj2Fy8fWMoPkFaNt9z0H3neQVWTy\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"1\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-04\",\"full_name\":\"Thai Thi Kim Trinh\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481019,\"updated_at\":1450481019}',1,'admin',1,'171.234.217.5',4,'customer',1450481019,1450481019),(5,1,'{\"id\":5,\"username\":\"phihung54\",\"auth_key\":\"3go5gBfaw2hHN_o3h9MHZPrWgR1pUKJa\",\"password_hash\":\"$2y$13$vBZb2FBWI4\\/oBc3BxJ7Y2OKSwPpXfBAMZxKbtLTRwkowJMl1c9G1K\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"1\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-02\",\"full_name\":\"Ha Phi Hung\",\"sex\":\"1\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481083,\"updated_at\":1450481083}',1,'admin',1,'171.234.217.5',5,'customer',1450481083,1450481083),(6,1,'{\"id\":6,\"username\":\"ledang49\",\"auth_key\":\"Y0JS_3DHvMkk5YnpQp708ILjV1WtvVWf\",\"password_hash\":\"$2y$13$ZqcXyllRbCNuSl1owhWA7ef66Y8x5cYTOD8y4Vx4bfytkI\\/9.7jMq\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"5\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-04\",\"full_name\":\"Le Thi Dang\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481160,\"updated_at\":1450481160}',1,'admin',1,'171.234.217.5',6,'customer',1450481160,1450481160),(7,1,'{\"id\":7,\"username\":\"trungkien77\",\"auth_key\":\"Szh7QD4sR6OIYWH6oqqkuO856jXC1Qyy\",\"password_hash\":\"$2y$13$OMYIajcFwx.0uDeDmq60wesLaTciKSGTov7GUjvmrTV5S2rbhVMz2\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"5\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-04\",\"full_name\":\"Nguyen Trung Kien\",\"sex\":\"1\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481230,\"updated_at\":1450481230}',1,'admin',1,'171.234.217.5',7,'customer',1450481230,1450481230),(8,1,'{\"id\":8,\"username\":\"thinh50\",\"auth_key\":\"8SsJNg3GucQQBPZNxBvMlq3uaREsCG9F\",\"password_hash\":\"$2y$13$MuVHRiq0bIjIBT9jgS4Ms.8mBvfpO8bStSE5mnLEmImp58lac7PaW\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"4\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-04\",\"full_name\":\"Nguyen Thi Thinh\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481297,\"updated_at\":1450481297}',1,'admin',1,'171.234.217.5',8,'customer',1450481297,1450481297),(9,1,'{\"id\":9,\"username\":\"lienhuong46\",\"auth_key\":\"pQZK7X_KZoW388qfNdJNI2lN8Wdi3JHp\",\"password_hash\":\"$2y$13$TCH66iE\\/3.FrZ.bnblhzDeGoGK5xpSrdLcQdhWD8LoKJLDeiEln.G\",\"password_reset\":null,\"package_id\":\"2\",\"parent_id\":\"8\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-04\",\"full_name\":\"Nguyen Thi Lien Huong\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481360,\"updated_at\":1450481360}',1,'admin',1,'171.234.217.5',9,'customer',1450481360,1450481360),(10,1,'{\"id\":10,\"username\":\"thanhha51\",\"auth_key\":\"fjbW-5nO9PQ8AhPuzCt3cYCA79eRcGTZ\",\"password_hash\":\"$2y$13$EE9M5m1gyi0\\/YwOGTIDb1.e1hlCEk4j6bxHNJva2H9m05Dufl0N3O\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"9\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-04\",\"full_name\":\"Tran Thi Thanh Ha\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481429,\"updated_at\":1450481429}',1,'admin',1,'171.234.217.5',10,'customer',1450481430,1450481430),(11,1,'{\"id\":11,\"username\":\"thanhmai58\",\"auth_key\":\"6g5Ygsd-l5stlewXdcMA5RGwanBLrIqt\",\"password_hash\":\"$2y$13$EZ9a07.YGDCr22rKqUDSjuPszBSUMyhnCe9o.RzFF.z010gxPrxHy\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"8\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-04\",\"full_name\":\"Vu Thi Thanh Mai\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481486,\"updated_at\":1450481486}',1,'admin',1,'171.234.217.5',11,'customer',1450481487,1450481487),(12,1,'{\"id\":12,\"username\":\"phammai52\",\"auth_key\":\"mI6RuGnhMTMQEcaVySVAIFJLExjAUkSl\",\"password_hash\":\"$2y$13$iY21V2PeSxYxRGBMa5d9i.B.VhbfwWOSURkG1dh\\/huvMXRERFd2Ae\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"11\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-04\",\"full_name\":\"Pham Thi Mai\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481549,\"updated_at\":1450481549}',1,'admin',1,'171.234.217.5',12,'customer',1450481549,1450481549),(13,1,'{\"id\":13,\"username\":\"nguyenxa56\",\"auth_key\":\"YKid7-Bq6bwrh2_JdxLHvT5ok1DLD8KW\",\"password_hash\":\"$2y$13$Nxa5.4YDJhViMErYxZJAZOdeSEPeEnyiZtuuPs6.krpQHA9gm\\/o1e\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"8\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-04\",\"full_name\":\"Nguyen Thi Xa\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481601,\"updated_at\":1450481601}',1,'admin',1,'171.234.217.5',13,'customer',1450481601,1450481601),(14,1,'{\"id\":14,\"username\":\"hien48\",\"auth_key\":\"0SpzBsXjw7AZgBWMEML5jaud0qcodJzK\",\"password_hash\":\"$2y$13$Yp5ffK88zurcV4El.p41S.CY1La6t4KdtKt85htIoei91wNa7BzNO\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"5\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-19\",\"full_name\":\"Le Thi Hien\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481657,\"updated_at\":1450481657}',1,'admin',1,'171.234.217.5',14,'customer',1450481657,1450481657),(15,1,'{\"id\":15,\"username\":\"thuymai58\",\"auth_key\":\"H5niL-hPs8LW73bucJR0O3QYkjKO1tn1\",\"password_hash\":\"$2y$13$zFRw\\/oIbEo4sZujckT.Ppuycm.8qPvz3oZxJew.cZDipyYnJpf7iS\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"2\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-23\",\"full_name\":\"Bui Thi Thuy Mai\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481736,\"updated_at\":1450481736}',1,'admin',1,'171.234.217.5',15,'customer',1450481736,1450481736),(16,1,'{\"id\":16,\"username\":\"Nguyen Duc Minh\",\"auth_key\":\"VBJeXvwgnqU_8wJ82nxU_kJlTTsCejsY\",\"password_hash\":\"$2y$13$yC9KgV\\/xEucKTsKG9oukxOLOcHaKHsQXCRPRaucTjuAXuTrTRc\\/I.\",\"password_reset\":null,\"package_id\":\"3\",\"parent_id\":\"2\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-10-12\",\"full_name\":\"Nguyen Duc Minh\",\"sex\":\"1\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481798,\"updated_at\":1450481798}',1,'admin',1,'171.234.217.5',16,'customer',1450481798,1450481798),(17,1,'{\"id\":17,\"username\":\"minhnguyen2\",\"auth_key\":\"lRJsAs2akdNAGmqTTWqmsyYpdVza88Rs\",\"password_hash\":\"$2y$13$yysb8pln2iWks0fLocGoCO.s57XEd55F\\/i8fPziurocbKxh9BpUqi\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"16\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-10-17\",\"full_name\":\"Nguyen Duc Minh\",\"sex\":\"1\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481866,\"updated_at\":1450481866}',1,'admin',1,'171.234.217.5',17,'customer',1450481867,1450481867),(18,1,'{\"id\":18,\"username\":\"danhien52\",\"auth_key\":\"ddye-1vwGv2lbbxUy3B4JNqx6PbpDlgJ\",\"password_hash\":\"$2y$13$RVTGtieev6Gc9jlQGzk6\\/Ox44rBAFklfdIHeD8eQ6mZd9H\\/bHc4jm\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"4\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-25\",\"full_name\":\"Ngo Dan Hien\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481931,\"updated_at\":1450481931}',1,'admin',1,'171.234.217.5',18,'customer',1450481932,1450481932),(19,1,'{\"id\":19,\"username\":\"trantub58\",\"auth_key\":\"peZFynpE_CKalgCS_I4eL0cE03yxuS-G\",\"password_hash\":\"$2y$13$6td0SUr8ZEgsclhUoocSCuc2qjJmGbv9QlAQWYrsxGWFsw.B.wYju\",\"password_reset\":null,\"package_id\":\"2\",\"parent_id\":\"2\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-24\",\"full_name\":\"Tran Thi Tu\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450481999,\"updated_at\":1450481999}',1,'admin',1,'171.234.217.5',19,'customer',1450481999,1450481999),(20,1,'{\"id\":20,\"username\":\"trantuc58\",\"auth_key\":\"OTuUCXC1LR8sUUtryCpbysGrcSEMNnko\",\"password_hash\":\"$2y$13$yvkUk\\/U15uPMkO\\/p.ixycu3R6KAEzpCAcdXY.egWmDgZxwUmHNit6\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"2\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-24\",\"full_name\":\"Tran Thi Tu\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450482042,\"updated_at\":1450482042}',1,'admin',1,'171.234.217.5',20,'customer',1450482042,1450482042),(21,1,'{\"id\":21,\"username\":\"thuymaia58\",\"auth_key\":\"rH9Zb2_A3Z4vWs6EKLEmruF9Iae6zpN1\",\"password_hash\":\"$2y$13$bDsgni.4MPuUXgOueoEHd.8BLvQAS6itYbiEwqPxPkrygYzx9LJ.q\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"15\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-30\",\"full_name\":\"Bui Thi Thuy Mai\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450482108,\"updated_at\":1450482108}',1,'admin',1,'171.234.217.5',21,'customer',1450482108,1450482108),(22,1,'{\"id\":22,\"username\":\"dung56\",\"auth_key\":\"eO-6yxJdSH2MhToLz95K1FOyitNXTkjE\",\"password_hash\":\"$2y$13$LG\\/zXsevMtq0bZB.x6ZZU.cFtNKHUUGxNUePHgroApMLw6yaLQswW\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"9\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-09-30\",\"full_name\":\"Tran Thi Dung\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450482195,\"updated_at\":1450482195}',1,'admin',1,'171.234.217.5',22,'customer',1450482195,1450482195),(23,1,'{\"id\":23,\"username\":\"vunhan68\",\"auth_key\":\"3suiNl1lgPzfyAZslpSzdGgCdGXuKPSo\",\"password_hash\":\"$2y$13$IO.ER\\/IvkifwIvKDGlprluFFWsAl5i5upSqXxdkDhM4FJ1zYPDGr6\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"22\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-10-07\",\"full_name\":\"Vu Nhan\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450482251,\"updated_at\":1450482251}',1,'admin',1,'171.234.217.5',23,'customer',1450482252,1450482252),(24,1,'{\"id\":24,\"username\":\"dieuthuy62\",\"auth_key\":\"yeAvyhNVlaxlNNOpHX7lBELDlkNvgZmY\",\"password_hash\":\"$2y$13$VpBlmhFyhR5VOTOEYdJF.u4hsFatMnGsqHBOmJD2npfyNzinf9JkO\",\"password_reset\":null,\"package_id\":\"2\",\"parent_id\":\"18\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-10-13\",\"full_name\":\"Le Dieu Thuy\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450482321,\"updated_at\":1450482321}',1,'admin',1,'171.234.217.5',24,'customer',1450482321,1450482321),(25,1,'{\"id\":25,\"username\":\"thuhuyen\",\"auth_key\":\"i-uc1Yo6uFo-fZ1canlhEQAZaz177atC\",\"password_hash\":\"$2y$13$2xlcX95Uqi9GCcMfN5jntev5DSMErdj1jPWg5ey9YB2nnLXGFhzbe\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"18\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-10-12\",\"full_name\":\"Le Thu Huyen\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450482428,\"updated_at\":1450482428}',1,'admin',1,'171.234.217.5',25,'customer',1450482428,1450482428),(26,1,'{\"id\":26,\"username\":\"huong47\",\"auth_key\":\"9M_3O1JxCBPve3RQSHCLLQc_OjFPp91y\",\"password_hash\":\"$2y$13$K2KSRHu73sO6mMEPSUCSf.j2rGDql1GwHxschySzvcZJbLBZRxiMe\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"1\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-10-15\",\"full_name\":\"Nguyen Thi Huong\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450482478,\"updated_at\":1450482478}',1,'admin',1,'171.234.217.5',26,'customer',1450482478,1450482478),(27,1,'{\"id\":27,\"username\":\"chimai55\",\"auth_key\":\"Dlj3b4Fdva7CUwVpUtgP2lKhcqDLaJX1\",\"password_hash\":\"$2y$13$e5C5mGHYeV.vXCBmRsERzOy9CLSM\\/hipfbh0s0D8YxYTSV\\/D35gUC\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"18\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-10-15\",\"full_name\":\"Luong Chi Mai\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450482525,\"updated_at\":1450482525}',1,'admin',1,'171.234.217.5',27,'customer',1450482525,1450482525),(28,1,'{\"id\":28,\"username\":\"thimai58\",\"auth_key\":\"Ej5BwZgLWqGojMh2Ck9OT8ZXWfouqbLe\",\"password_hash\":\"$2y$13$mh3RQt7cu6D2XNqLhIkbVuFhSHkafdHJaJ7JCcbSddcXDQS5QOuIW\",\"password_reset\":null,\"package_id\":\"3\",\"parent_id\":\"1\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-10-15\",\"full_name\":\"Le Thi Mai\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450482577,\"updated_at\":1450482577}',1,'admin',1,'171.234.217.5',28,'customer',1450482578,1450482578),(29,1,'{\"id\":29,\"username\":\"phuongdung59\",\"auth_key\":\"JE3dqLd9bvqGKrXQpPhyQn2klQKvUKeB\",\"password_hash\":\"$2y$13$5aP0biy73qzuoCaLoX7jkO6QFEIKMau3P\\/Cb3KMehcsGHatZLu99S\",\"password_reset\":null,\"package_id\":\"1\",\"parent_id\":\"1\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-10-22\",\"full_name\":\"To Phuong Dung\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450482621,\"updated_at\":1450482621}',1,'admin',1,'171.234.217.5',29,'customer',1450482621,1450482621),(30,1,'{\"id\":30,\"username\":\"viettrieu52\",\"auth_key\":\"2e4dQ0WhF1SFOYtTHKJldDHylzw7Nrqw\",\"password_hash\":\"$2y$13$t53kme8rKeVsu\\/ijkv5qn.KmM7lKPwjyE9DxDpA4e.wBogY\\/imjpG\",\"password_reset\":null,\"package_id\":\"3\",\"parent_id\":\"28\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-10-25\",\"full_name\":\"Nguyen Thi Viet Trieu\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450482692,\"updated_at\":1450482692}',1,'admin',1,'171.234.217.5',30,'customer',1450482692,1450482692),(31,1,'{\"id\":31,\"username\":\"huonglien60\",\"auth_key\":\"34yH34IB8xLoOA3fg5pZwDqUSEYqJkUZ\",\"password_hash\":\"$2y$13$ri7xrnXsPB\\/bK.h.8YwZMu8D4p8c\\/JTWDwNAXvIYfDhpJ09EUsElO\",\"password_reset\":null,\"package_id\":\"2\",\"parent_id\":\"1\",\"amount\":null,\"status\":null,\"created_type\":null,\"created_by\":null,\"date_join\":\"2015-10-31\",\"full_name\":\"Le Thi Huong Lien\",\"sex\":\"2\",\"dob\":\"\",\"phone\":\"\",\"email\":\"\",\"nation\":null,\"address\":\"\",\"last_auto_increase\":null,\"created_at\":1450482750,\"updated_at\":1450482750}',1,'admin',1,'171.234.217.5',31,'customer',1450482751,1450482751);
/*!40000 ALTER TABLE `log_action` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` (`version`, `apply_time`) VALUES ('m000000_000000_base',1448563630),('m140506_102106_rbac_init',1448563767),('m151203_034102_add_coloumn_to_customer',1449229602),('m151204_013758_add_comlumn_customer_case',1449229605),('m151212_022108_add_feild_parent_id_to_customer',1450052549),('m151214_005549_log_action_table',1450054827),('m151214_183241_create_table_request_cash_out',1450118322);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2015_07_11_144046_tbl_backend_menus',1),('2015_06_12_161949_create_posts_table',2),('2015_07_01_070801_create_table_category',2),('2015_07_03_101756_create_airport_table',2),('2015_07_03_101940_create_roomtype_table',2),('2015_07_03_102102_create_roommoney_table',2),('2015_07_03_102223_create_roomtime_table',2),('2015_07_04_025812_create_table_menus',2),('2015_07_07_014335_create_options_table',2),('2015_07_08_032119_create_booking_table',2),('2015_07_08_034643_create_comment_table',2),('2015_07_09_010327_crate_slides_table',2),('2015_07_09_020455_create_credit_table',2),('2015_07_16_093421_create_language_tbl',3),('2015_07_17_163309_create_tbl_customers',4),('2015_07_19_173642_create_post_desc_tbl',5),('2015_07_20_002105_create_category_desc_tbl',6),('2015_07_21_015142_create_menu_desc_tbl',7),('2015_08_03_072840_create_service_lang_table',8),('2015_08_07_180839_create_list_transfer_table',9),('2015_08_08_104807_create_request_user_table',10);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package`
--

DROP TABLE IF EXISTS `package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(45) DEFAULT NULL,
  `amount` int(10) unsigned DEFAULT NULL,
  `status` smallint(2) unsigned DEFAULT NULL,
  `created_at` int(10) unsigned DEFAULT NULL,
  `updated_at` int(10) unsigned DEFAULT NULL,
  `increase_percent` smallint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `package_name_UNIQUE` (`package_name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package`
--

LOCK TABLES `package` WRITE;
/*!40000 ALTER TABLE `package` DISABLE KEYS */;
INSERT INTO `package` (`id`, `package_name`, `amount`, `status`, `created_at`, `updated_at`, `increase_percent`) VALUES (1,'BONZE',1000,1,1449235961,1449236742,10),(2,'SILVER',3000,1,1449235996,1449236759,12),(3,'GOLD',10000,1,1449236022,1449236774,14),(4,'PLATINUM',30000,1,1449236054,1449236786,16),(5,'DIAMOND',50000,1,1449236083,1449236800,18);
/*!40000 ALTER TABLE `package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package_config`
--

DROP TABLE IF EXISTS `package_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) DEFAULT NULL,
  `increase_percent` int(11) DEFAULT NULL,
  `status` smallint(2) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `INCREASE_PERCENT` (`increase_percent`,`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package_config`
--

LOCK TABLES `package_config` WRITE;
/*!40000 ALTER TABLE `package_config` DISABLE KEYS */;
/*!40000 ALTER TABLE `package_config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `auth_key` varchar(45) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `branch` smallint(6) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Bảng user hệ thống';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `full_name`, `email`, `phone`, `status`, `branch`, `created_at`, `updated_at`) VALUES (1,'admin','8VJPn1viHVjwi2M6acjmrPP_7VVCpNCK','$2y$13$6BMTlSBSBlFL05T/KwkUW.0l5zyhjJ99YIMhoJJjuF2PaFiB8eDiq',NULL,NULL,'vinhhx.register@gmail.com','0986118464',1,NULL,1448886309,1448886309),(2,'minhhp','kN4sBdb-DvoDVhADPNatT3AoewGabJuF','$2y$13$ODNrQ6RotYZEG2IF/6kPZOVIk1ZTb6MeKJvr71vvTgqTIXm.iLLz2',NULL,NULL,'test@gmail.com',NULL,1,NULL,1450389006,1450389006),(4,'minhnguyen','vlcy7-ytFBgUcldPcGeKVQObP7pfQ42g','$2y$13$w1epFCuVYcXY2.r19/MWGu8x7ISiRcmnc.z57COxxV0kPVXsIxAjq',NULL,'Nguyen Duc Minh','123@gmail.com','0986118464',1,NULL,1450389178,1450389527);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;