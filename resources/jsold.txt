
$('.open-modal-sent-view').on('click', function (e) {
    e.preventDefault();
    $.ajax({
        url: location.href,
        dataType: "JSON",
        type: "POST",
        data: {type: 'view', tid: $(this).attr('data-order-id')},
        success: function (data) {
            $("#pageModal .modal-body").html(data);
            $("#pageModal").modal("show");
        }
    });
});
$('body').on('submit', 'form#upload-transaction-bill', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var formData = new FormData(this);
    formData.append('type', 'upload');
    var _this = jQuery(this);
    jQuery('form#upload-transaction-bill #upload-bill').attr('disabled', 'disabled');
    jQuery('form#upload-transaction-bill #upload-bill').html('processing ...');
    jQuery.ajax({
        url: jQuery(this).attr('action'),
        type: 'POST',
        dataType: 'JSON',
        async: false,
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            if (data) {
                if (data.status == 'success') {
                    $("#pageModal .modal-body").html(data.content);
                    $("#pageModal").modal("show");
                    setTimeout(function () {
                        $("#pageModal").modal('hide');
                        localRefresh();
                    }, 5000);
                } else if (data.status == "error") {
                    alert(data.message);
                }

            }
//                modal.modal('hide');
        }
    });
}).on('submit', 'form#create-new-package', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
//        var min=parseFloat($('#customercreatepackageform-sent_bit_coin_first').attr('amount-min'));
//        var max=parseFloat($('#customercreatepackageform-sent_bit_coin_first').attr('amount-max'));
//        var amount=parseFloat($('#customercreatepackageform-sent_bit_coin_first').val());
//        if(amount>0 &&(amount>max || amount<min)){
//            alert('Số bitcoin bạn nhập vào không chính xác');
//            return false;
//        }
    var formData = new FormData(this);
    formData.append('type', 'create-package');
    var _this = jQuery(this);
    jQuery.ajax({
        url: jQuery(this).attr('action'),
        type: 'POST',
        dataType: 'JSON',
        async: false,
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            if (data) {
                $("#pageModal .modal-body").html(data);
                $("#pageModal").modal("show");
                setTimeout(function () {
                    $("#pageModal").modal('hide');
                    localRefresh();
                }, 5000);
            }
        }
    });
}).on('click', '.btn-approved-transaction', function (e) {
    e.preventDefault();
    var confirmation = confirm('Bạn có chắc chắn đã nhận được btc từ người gửi theo mẫu trên? ');
    if (confirmation) {
        $.ajax({
            url: location.href,
            dataType: "JSON",
            type: "POST",
            data: {type: 'approved', tid: $(this).attr('data-order-id')},
            success: function (data) {
                $("#pageModal .modal-body").html(data);
                $("#pageModal").modal("show");
                setTimeout(function () {
                    $("#pageModal").modal('hide');
                    localRefresh();
                }, 5000);
            }
        });
    }
}).on('click', '.btn-submit-question-add-request', function (e) {
    if ($('.check-request').is(':checked')) {
        e.preventDefault();
        var type = 'open-package';
        $.ajax({
            url: location.href,
            dataType: "JSON",
            type: "POST",
            data: {type: type},
            success: function (data) {
                $("#pageModal .modal-body").html(data);
                $("#pageModal").modal("show");
            }
        });
    } else {
        $("#pageModal").modal("hide");
    }
}).on('click', '.btn-submit-cancel', function (e) {
    $("#pageModal").modal("hide");
    $("#pageModal .modal-body").html('');
}).on('click', '#widthdraw-package', function (e) {
    e.preventDefault();
    $.ajax({
        url: location.href,
        dataType: "JSON",
        type: "POST",
        data: {type: 'open-request-widthdraw'},
        success: function (data) {
            $("#pageModal .modal-body").html(data);
            $("#pageModal").modal("show");
        }
    });
}).on('click', '#my-poundage-balance', function (e) {
    e.preventDefault();
    $.ajax({
        url: location.href,
        dataType: "JSON",
        type: "POST",
        data: {type: 'open-commission-cash-out'},
        success: function (data) {
            $("#pageModal .modal-body").html(data);
            $("#pageModal").modal("show");
        }
    });
}).on('submit', 'form#customer-cash-out-commission', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var formData = new FormData(this);
    formData.append('type', 'create-commission-cash-out');
//    if(parseFloat($('#cashoutform-amount').val())>parseFloat($('#cashoutform-amount').attr('data-balance'))){
//    $('#show-error').html('Số tiền bạn nhập vào vượt quá số tiền có thể rút.');
//     $('#show-error').show();
//    return false;
//    }else{
    jQuery.ajax({
        url: jQuery(this).attr('action'),
        type: 'POST',
        dataType: 'JSON',
        async: false,
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
            $("#pageModal .modal-body").html(data);
            $("#pageModal").modal("show");
            setTimeout(function () {
                $("#pageModal").modal('hide');
                localRefresh();
            }, 5000);
        }
    });
    return false;
//    }
}).on('change', 'select#cashoutform-customer_package_id', function (e) {
    e.preventDefault()
    var packageId = $(this).val();
    if (parseInt(packageId)) {
        $.ajax({
            url: location.href,
            dataType: "JSON",
            type: "POST",
            data: {type: 'select-package', packageId: packageId},
            success: function (data) {
                if (data.status == "success") {
                    $('#show-error').hide();
                    $('#cashoutform-amount').val(data.balance);
                    $('#total-package-balance').html(data.balance);
                } else {
                    $('#show-error').html(data.message);
                    $('#show-error').show();
                }
            }
        });
    }
}).on('submit', 'form#customer-cash-out', function (e) {
    e.preventDefault();
    e.stopImmediatePropagation();
    var formData = new FormData(this);
    formData.append('type', 'cash-out');
    if (parseFloat($('#cashoutform-amount').val()) > parseFloat($('#cashoutform-amount').attr('data-balance'))) {
        $('#show-error').html('Số tiền bạn nhập vào vượt quá số tiền có thể rút.');
        $('#show-error').show();
        return false;
    } else {
        jQuery.ajax({
            url: jQuery(this).attr('action'),
            type: 'POST',
            dataType: 'JSON',
            async: false,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
                $("#pageModal .modal-body").html(data);
                $("#pageModal").modal("show");
                setTimeout(function () {
                    $("#pageModal").modal('hide');
                    localRefresh();
                }, 5000);
            }
        });
        return false;
    }
}).on('click', '.show-hide-transaction-success-sent', function (e) {
    if ($('.info-box.bg-green-2.transaction-item-box').is(':hidden')) {
        $('.info-box.bg-green-2.transaction-item-box').show();
    } else {
        $('.info-box.bg-green-2.transaction-item-box').hide();
    }
}).on('click', '.show-hide-transaction-success-receiver', function (e) {
    if ($('.info-box.bg-green.transaction-item-box').is(':hidden')) {
        $('.info-box.bg-green.transaction-item-box').show();
    } else {
        $('.info-box.bg-green.transaction-item-box').hide();
    }
}).on('click', '.transaction-remove-attachment', function (e) {
    var _confirm = confirm('You sure want to delete this attachment ?');
    if (_confirm) {
        var _parent = $(this).parent();
        var _transactionId = $(this).attr('data-transaction-id');
        var _images = _parent.find('img.img-item');
        if (_images.length) {
            $.post('', {type: 'remove-attachment', tid: _transactionId}, function (data) {
            }).done(function () {
                location.reload();
            });
        }
    }


});

$("#create-package").on('click', function (e) {
    $("#pageModal .modal-body").html(htmlBidPip('open-package'));
    $("#pageModal").modal("show");
});

var htmlBidPip = function () {
    var html = '';
    if ($('#has-ph').length) {
        html = $('#has-ph').html();
    }
    return html;
}