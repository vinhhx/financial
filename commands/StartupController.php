<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;
use  app\models\User;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class StartupController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";
    }
	
	public function actionCreateAdmin()
	{
		$usernameDefault='admin';
		$passwordDefault='pwd123!';		
		$model=new User();
		$model->username=$usernameDefault;
		$model->email='vinhhx.register@gmail.com';
		$model->phone='0986118464';
		$model->status=User::STATUS_ACTIVE;
		$model->setPassword($passwordDefault);
        $model->generateAuthKey();
		if($model->save()){
			
			echo "Create username complete: \n username:".$usernameDefault." \n password:".$passwordDefault;
		}else{
			echo (reset($model->getErrors()[0]));
			
		}
	}
}
