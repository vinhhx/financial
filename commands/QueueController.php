<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 11-Jun-16
 * Time: 8:54 AM
 */
namespace app\commands;

use app\models\Customer;
use app\models\CustomerPoundageTransaction;
use app\models\CustomerTransactionQueue;
use app\models\ReCalculatePoundageQueue;
use yii\base\Exception;
use yii\console\Controller;
use yii\db\Query;

class QueueController extends Controller
{
    /**
     * Function tạo transaction chuyển tiền giữa các tài khoản cho và nhận
     */
    public function actionQueueCreateTransaction()
    {
        if(\Yii::$app->params['mapTransaction']=='on'){
            $query = CustomerTransactionQueue::find();
            $query->select('id');
            $query->where(['status' => CustomerTransactionQueue::STATUS_PENDING]);
            $query->andFilterWhere(['<=', 'start_at', time()]);
            $query->orderBy(['start_at' => SORT_DESC]);
            $query->limit(20);
            $queues = $query->column();
            if ($queues) {
                foreach ($queues as $item) {
                    $currentQueue = CustomerTransactionQueue::findOne([
                        'id' => $item,
                        'status' => CustomerTransactionQueue::STATUS_PENDING]);
                    if ($currentQueue) {
                        $result = $currentQueue->createTransaction();
                        if ($result) {
                            echo $item . " vua duoc mapping \n";
                        }
                    } else {
                        echo $item . " da duoc mapping \n";
                    }

                }
            }
        }


    }

    /**
     * Function tính lại tiền hoa hồng hệ thống
     */
    public function  actionReCalcPoundage()
    {
        $query = ReCalculatePoundageQueue::find();
        $query->where([
            'status' => ReCalculatePoundageQueue::STATUS_PENDING,
        ]);
        $query->limit(20);
        $query->indexBy('id');
        $queues = $query->all();
        if ($queues) {
            $param = [];
            $queueKeys = array_keys($queues);
            foreach ($queues as $q) {
                $param = array_merge($param, json_decode($q->params));

            }
            if (!empty($param)) {
                $param = array_unique($param);
                $customers = Customer::find()->where(['id' => $param])->all();
                $query = new Query();
                $query->select(['SUM(amount) as total', 'customer_id']);
                $query->from(CustomerPoundageTransaction::tableName());
                $query->where([
                    'customer_id' => $param,
                    'level' => [CustomerPoundageTransaction::F1, CustomerPoundageTransaction::F2],
                    'type' => [CustomerPoundageTransaction::TYPE_POUNDAGE_FIRST, CustomerPoundageTransaction::TYPE_POUNDAGE_SECOND],
                    'sign' => CustomerPoundageTransaction::SIGN_ADD

                ]);
                $query->groupBy('customer_id');
                $query->indexBy('customer_id');
                $customerPoundage = $query->all();

                $transaction = \Yii::$app->db->beginTransaction();
                try {
                    foreach ($customers as $c) {
                        //Tính tổng lãi xuất

                        if (isset($customerPoundage[$c->id]["total"])) {
                            $clv = round($customerPoundage[$c->id]["total"] / 16);
                            if ($clv > 0 && $clv > $c->poundage_level) {
                                $i = $c->poundage_level + 1;
                                for ($i; $i <= $clv; $i++) {
                                    if (!$c->addPoundageSystem($i)) {
                                        throw new Exception('Không cộng lãi vào tài khoản người chơi');
                                    }else{
                                        echo 'Check thành công tài khoản #' . $c->username. "\n";
                                    }
                                }
                            }
                        }
                    }
                    //Cập nhật lại trạng thái đã tính lại lãi
                    \Yii::$app->db->createCommand()->update(ReCalculatePoundageQueue::tableName(), [
                        'status' => ReCalculatePoundageQueue::STATUS_COMPLETED,
                    ], [
                        'id' => $queueKeys
                    ])->execute();
                    $transaction->commit();
                } catch (\Exception $ex) {
                    $transaction->rollBack();
                    echo $ex->getMessage();
                }


            }
        }


    }
}