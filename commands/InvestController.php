<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 11-Jun-16
 * Time: 8:54 AM
 */
namespace app\commands;

use app\models\Customer;
use app\models\investments\InvestCustomerPackage;
use app\models\investments\InvestCustomerPackageHistory;
use app\models\investments\InvestPackage;
use yii\base\Exception;
use yii\console\Controller;
use yii\db\Query;

class InvestController extends Controller
{
    public function actionIncreaseDay()
    {
        $dateName = date("l");
        if ($dateName == "Sunday" || $dateName == "Monday") {
            echo 'Saturday and Sunday  not increase package invest.';
            return false;
        }
        $packages = InvestPackage::find()->where(['status' => InvestPackage::STATUS_ACTIVE])->indexBy('id')->all();
        //Lấy các gói tính đến hôm nay chưa đc cộng lãi
        $customerPackages = InvestCustomerPackage::find()
            ->where([
                'is_active' => InvestCustomerPackage::IS_ACTIVE_SUCCESS,
                'status' => InvestCustomerPackage::STATUS_ACTIVE
            ])
            ->andFilterWhere([
                '<', 'DATE(FROM_UNIXTIME(last_profit_receiver))', date('Y-m-d')
            ])
            ->all();
        if ($customerPackages) {
            foreach ($customerPackages as $cusPack) {
                $currentPack = isset($packages[$cusPack->package_id]) ? $packages[$cusPack->package_id] : null;
                if ($currentPack) {
                    $increase = round(($cusPack->amount * $currentPack->increase_day / 100), 4);
                    $cusPack->current_profit += $increase;
                    $cusPack->total_current_profit += $increase;
                    $cusPack->last_profit_receiver = time();
                    if ($cusPack->save(true, ['current_profit', 'last_profit_receiver', 'total_current_profit', 'updated_at'])) {
                        $cusPackHistory = new InvestCustomerPackageHistory();
                        $cusPackHistory->customer_package_id = $cusPack->id;
                        $cusPackHistory->sign = InvestCustomerPackageHistory::SIGN_ADD;
                        $cusPackHistory->amount = $increase;
                        $cusPackHistory->type = InvestCustomerPackageHistory::TYPE_PROFIT_PACKAGE;
                        $cusPackHistory->save();
                        echo 'Increase CustomerPackage: ' . $cusPack->id . "\n";
                    } else {
                    }
                } else {
                }

            }
        } else {
            echo 'Không có gói nào chưa đc cộng lãi tính đến ngày hôm nay.' . date('Y-m-d');
        }
    }

}