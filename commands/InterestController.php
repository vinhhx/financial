<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\Customer;
use app\models\CustomerPackage;
use app\models\Package;
use yii\console\Controller;

class InterestController extends Controller
{

//    public function actionAddAmountByWeek() {
//        ini_set('max_execution_time', 600);
//        $query = (new \yii\db\Query())
//                ->select('id,username,package_id,amount,last_auto_increase')
//                ->from('customer');
//        $query->andFilterWhere(['status' => 1]);
//        $query->andFilterWhere(["!=", "is_deleted", \app\models\Customer::IS_DELETED]);
//        $query->andFilterWhere(['>=', 'DATEDIFF(NOW(),DATE(last_auto_increase))', 7]);
//        $data = $query->all();
//        if ($data) {
//            foreach ($data as $item) {
//                if (strtotime(date('Y-m-d')) >= strtotime(date('Y-m-d', strtotime($item['last_auto_increase'] . "+7 days")))) {
//                    $package = \app\models\Package::findOne($item['package_id']);
//                    if ($package) {
//                        $interest = ((float) $package->amount * (float) $package->increase_percent / 100) / 4;
//                        $amount_new = (float) $item['amount'] + $interest;
//                        \Yii::$app->db->createCommand()
//                                ->update('customer', [
//                                    'amount' => $amount_new,
//                                    'last_auto_increase' => date('Y-m-d', strtotime($item['last_auto_increase'] . "+7 days")),
//                                        ], 'id=' . $item['id'])
//                                ->execute();
//                        \app\models\Customer::saveCustomerTransaction($item['id'], $item['username'], $interest, (int) $item['amount'], $amount_new, \app\models\CustomerTransaction::TYPE_ADD_INTEREST_WEEK, \app\models\CustomerTransaction::SIGN_ADD);
//                    }
//                }
//            }
//        }
//        exit('Done');
//    }
    public function actionAddAmountPackagedCompleted()
    {
        $query = CustomerPackage::find();
        $query->with('package');
        $query->where(['status' => CustomerPackage::STATUS_RUNNING]);
        $query->andFilterWhere([
            'and',
            ['<=', 'reinvestment_date', (time() - \Yii::$app->params["TimeRequestLimit"])],
            ['<=', 'time_sent_completed', (time() - \Yii::$app->params["TimeCompletePackage"])]
        ]);
        //Nếu gói đang hoạt động mà
        //Thời gian vào gói  >12
        //Thời gian hoàn thành gói >12h
        $query->limit(100);
        $customerPackages = $query->all();
        if ($customerPackages) {
            foreach ($customerPackages as $cp) {
                $amount = $cp->package_amount;
                $increasePercent = $cp->package->increase_percent;
                $increaseAmount = round(($amount / 100) * $increasePercent, 8);
                $cp->balance += ($amount + $increaseAmount);
                $cp->package_amount = 0;
//                $cp->package_id = 0;
//                $cp->package_name = 'empty';
                $cp->total = 0;
                $cp->increase_amount = 0;
                $cp->status = CustomerPackage::STATUS_EMPTY;
                if ($cp->save(true, ['balance', 'package_amount', 'package_id', 'package_name', 'total', 'increase_amount', 'status', 'updated_at'])) {
                    echo ' Cat lai thanh cong cho package: ' . $cp->package_name . ' cua customer_id' . $cp->customer_id . "\n";
                } else {
                    echo 'ERROR: khong cat lai cho package: ' . $cp->package_name . ' cua customer_id' . $cp->customer_id . "\n";
                }

            }
        }

    }
}
