<?php
/**
 * Created by PhpStorm.
 * User: Dev
 * Date: 11-Nov-15
 * Time: 3:51 PM
 */

namespace app\commands;

use app\models\Email;
use app\models\SystemEmailQueue;
use Yii;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class EmailController extends Controller
{
    public function  actionSend()
    {
        /*
        *
        */
        $emails = SystemEmailQueue::find()->where([
            ' <> ', 'status', SystemEmailQueue::STATUS_SENT
        ])->all();
        if ($emails) {
            foreach ($emails as $email) {
                echo $email->to_email . "\n";
                try {
                    $params = ArrayHelper::toArray(json_decode($email->params));
                    $params["email_type"] = $email->type;
                    $message = \Yii::$app->mailer->compose($email->template, $params);
                    $email->result_code = $message->setFrom([Yii::$app->params["emailSend"] => 'BITONESS.COM'])
                        ->setTo($email->to_email)
                        ->setSubject($email->subject)
                        ->send();
                    $email->status = SystemEmailQueue::STATUS_SENT;

                } catch (\Exception $ex) {
                    $email->result_message = $ex->getMessage();
                    $email->status = SystemEmailQueue::STATUS_ERROR;
                    echo $ex->getMessage();
//                    print_r($ex->getTraceAsString());
                }
                $email->save(true,['status','result_message']);
                echo "--------------------------------\n";
            }
        }
    }

    public function actionTest()
    {
        $message = \Yii::$app->mailer->compose('test', [
            "id" => "1000000",
            "username" => "hoangxuanvinh",
            "phone" => "0986118464",
            "email" => "theduong88@gmai.com",
            'ref_code'=>"10101010101",
            "email_type"=>10
        ]);
        try{
            $result_code = $message->setFrom([Yii::$app->params["emailSend"] => 'BITONESS.COM'])
                ->setTo('hoangxuanvinh88@gmail.com')
                ->setSubject('Test send email ' . date('d/m/Y H:i:s'))
                ->send();

        }catch (\Exception $ex){
            var_dump($ex->getMessage());

        }

    }

    public function actionTest2()
    {
        $emails = SystemEmailQueue::find()->where([
            'id' => 1000177
        ])->all();
        if ($emails) {
            foreach ($emails as $email) {
                echo $email->id . "\n";
                try {
                    $params = ArrayHelper::toArray(json_decode($email->params));
                    $message = \Yii::$app->mailer->compose($email->template, $params);
                    $email->result_code = $message->setFrom([Yii::$app->params["emailSend"] => 'BITONESS.COM'])
                        ->setTo($email->to_email)
                        ->setSubject($email->subject)
                        ->send();
                    $email->status = SystemEmailQueue::STATUS_SENT;

                } catch (\Exception $ex) {
                    $email->result_message = $ex->getMessage();
                    $email->status = SystemEmailQueue::STATUS_ERROR;
                    echo $ex->getMessage();
                    print_r($ex->getTraceAsString());
                }
                $email->save();
                echo "--------------------------------\n";
            }
        }
    }
}
