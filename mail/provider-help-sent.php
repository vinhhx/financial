<?php
Yii::$app->urlManager->setBaseUrl('http://bitoness.com');
Yii::$app->urlManager->setHostInfo('http://bitoness.com');
$baseUrl = Yii::$app->urlManager->getBaseUrl();
$this->title = 'Your request withdraw is completed.';
$title = $this->title;
?>
<table width="530" border="0">
    <tr>
        <td width="530" style="text-wrap:suppress;">
            <p>Dear <?= isset($username) ? $username : '' ?>,</p>
            <p style="text-align: center;">
                <b>Your provider help amount<?= Yii::$app->formatter->asDecimal($amount, 8) ?>
                    &nbsp;<?= Yii::$app->params["unit"] ?></b> <br/>
                <img src="<?= Yii::$app->image->getImg($image) ?>"/>
            </p>
            <p>
                You have successfully provided 50% of investment which you chose; your fund is in initial rotation in BitOness community. From the next 48 h to 120 h, you will receive our support.<br/>
                Yours sincerely,<br/>
                BitOness is the project with mission to create pre-algorithm boom.<br/>
            </p>
        </td>
    </tr>
</table>
