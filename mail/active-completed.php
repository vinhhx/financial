<?php
Yii::$app->urlManager->setBaseUrl('http://bitoness.com');
Yii::$app->urlManager->setHostInfo('http://bitoness.com');
$baseUrl = Yii::$app->urlManager->getBaseUrl();
$this->title = 'Your account actived!';
$title = $this->title;
?>
<table width="530" border="0">
    <tr>
        <td width="530" style="text-wrap:suppress;">
            Dear <?= isset($username) ? $username : '' ?>,<br>
            <b>Thanks you, Your account actived! </b> <br/>
            <p>
                Congratulations, you have participated in the Bitoness system, to ensure your interests in the project, you need to buy 1 token to activate your account.<br/>
                If you meet any problems, please contact us.<br/>
                Yours sincerely,<br/>
                BitOness is the project with mission to create pre-algorithm boom.<br/>
            </p>
        </td>
    </tr>
</table>