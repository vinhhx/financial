<?php
Yii::$app->urlManager->setBaseUrl('http://bitoness.com');
Yii::$app->urlManager->setHostInfo('http://bitoness.com');
$baseUrl = Yii::$app->urlManager->getBaseUrl();
$this->title = 'Register Completed';
$title = $this->title;
?>
<table width="530" border="0">
    <tr>
        <td width="530" style="text-wrap:suppress;">
            <p>Dear <?= isset($username) ? $username : '' ?>,</p>
            <p style="text-align: center;">
                <b>Your account username: <?= $username ?> </b> <br/>
                <b>Your account password: ********* </b> <br/>
                <b>Your sponsor code: <?= $ref_code ?> </b> <br/>
                <?php if ($email_type == \app\models\SystemEmailQueue::TYPE_CUSTOMER_REGISTERED): ?>
                    <b>Your link
                        sponsor: <?= Yii::$app->urlManager->createAbsoluteUrl(['/customer/default/register', 'ref' => $ref_code]) ?> </b>
                    <br/>
                <?php else: ?>
                    <b>Your link
                        sponsor: <?= Yii::$app->urlManager->createAbsoluteUrl(['/invest/default/register', 'ref' => $ref_code]) ?> </b>
                    <br/>
                <?php endif; ?>
            </p>
            <p>
                Congratulations, you have participated in the Bitoness system, to ensure your interests in the project, you need to buy 1 token to activate your account.<br/>
                If you meet any problems, please contact us.<br/>
                Yours sincerely,<br/>
                BitOness is the project with mission to create pre-algorithm boom.<br/>
            </p>
        </td>
    </tr>
</table>