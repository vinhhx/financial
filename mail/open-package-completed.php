<?php
Yii::$app->urlManager->setBaseUrl('http://bitoness.com');
Yii::$app->urlManager->setHostInfo('http://bitoness.com');
$baseUrl = Yii::$app->urlManager->getBaseUrl();
$this->title = 'You opened new package completed!';
$title = $this->title;
?>
<table width="530" border="0">
    <tr>
        <td width="530" style="text-wrap:suppress;">
            <p>Dear <?= isset($username) ? $username : '' ?>,</p>
            <p><b>Your account username: <?= $username ?> </b></p>
            <p style="text-align: center;">
                <b>Your package name: </b> <?= $package_name ?> <br/>
                <b>Your package
                    value: </b> <?= Yii::$app->formatter->asDecimal($package_amount, 8) ?><?= Yii::$app->params["unit"] ?>
                <br/>
            </p>
            <p>
                Congratulations, you have successfully chosen investment package, this is an important milestone helping your success during working process of BitOness.<br/>
                Yours sincerely,<br/>
                BitOness is the project with mission to create pre-algorithm boom.<br/>
            </p>
        </td>
    </tr>
</table>