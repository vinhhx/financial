<?php
Yii::$app->urlManager->setBaseUrl('http://bitoness.com');
Yii::$app->urlManager->setHostInfo('http://bitoness.com');
$baseUrl = Yii::$app->urlManager->getBaseUrl() . '/customer/default/reset-password?token=' . $token;
$this->title = 'Forget Password';
?>
<table width="530" border="0">
    <tr>
        <td width="530" style="text-wrap:suppress;">
            <p>Dear <?= isset($username) ? $username : '' ?>,</p>
            <p><b>Your account username: <?= $username ?> </b></p>
            <p style="text-align: center;">
                <table border="0" cellpadding="0" cellspacing="0" width="50%" class="emailButton"
                       style="background-color: #3498DB; margin: auto;">
                    <tr>
                        <td align="center" valign="middle" class="buttonContent"
                            style="padding-top:15px;padding-bottom:15px;padding-right:15px;padding-left:15px;">
                            <a style="color:#FFFFFF;text-decoration:none;font-family:Helvetica,Arial,sans-serif;font-size:20px;line-height:135%;"
                               href="<?= $baseUrl ?>" target="_blank">Confirmation</a>
                        </td>
                    </tr>
                </table>
            </p>
        </td>
    </tr>
</table>