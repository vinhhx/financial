<?php
Yii::$app->urlManager->setBaseUrl('http://bitoness.com');
Yii::$app->urlManager->setHostInfo('http://bitoness.com');
$baseUrl = Yii::$app->urlManager->getBaseUrl();
$this->title = 'You received ' . $quantity . ' token.';
$title = $this->title;
?>
<table width="530" border="0">
    <tr>
        <td width="530" style="text-wrap:suppress;">
            <p>Dear <?= isset($username) ? $username : '' ?>,</p>
            <p style="text-align: center;">
                <b>Your tokens: </b> <br/>
                <?php foreach ($tokens as $token): ?>
                    <p><?php echo trim($token) ?></p>
                <?php endforeach; ?>
            </p>
            <p>
                We have sent you tokens according to demand into your account, you are free to use it.<br/>
                Yours sincerely,<br/>
                BitOness is the project with mission to create pre-algorithm boom.<br/>
            </p>
        </td>
    </tr>
</table>

