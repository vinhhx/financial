<?php
Yii::$app->urlManager->setBaseUrl('http://bitoness.com');
Yii::$app->urlManager->setHostInfo('http://bitoness.com');
$baseUrl = Yii::$app->urlManager->getBaseUrl();
?>
<?php if ($type_step == 10):
    $title = '[PH] request at ' . date('d-m-Y', $created_at);
    ?>
    <table width="530" border="0">
        <tr>
            <td width="530" style="text-wrap:suppress;">
                <p>Dear <?= isset($customer_sent_username) ? $customer_sent_username : '' ?>,</p>
                <p style="text-align: center;">
                    <b>You have provide help request, amount <?= Yii::$app->formatter->asDecimal($amount, 8) ?>
                        &nbsp;<?= Yii::$app->params["unit"] ?></b>
                    to bitcoin address: <b><?= $customer_receiver_bank_address ?></b>
                </p>
                <p>
                    You have 12h to complete this PH request.<br/>
                    Yours sincerely,<br/>
                    BitOness is the project with mission to create pre-algorithm boom.<br/>
                </p>
            </td>
        </tr>
    </table>
<?php elseif ($type_step == 11):
    $title = 'PH request last 50%';
    ?>
    <table width="530" border="0">
        <tr>
            <td width="530" style="text-wrap:suppress;">
                <p>Dear <?= isset($customer_sent_username) ? $customer_sent_username : '' ?>,</p>
                <p style="text-align: center;">
                    <b>You have last provide help request, amount <?= Yii::$app->formatter->asDecimal($amount, 8) ?>
                        &nbsp;<?= Yii::$app->params["unit"] ?></b>
                    to bitcoin address: <b><?= $customer_receiver_bank_address ?></b>

                </p>
                <p>
                    You have 12h to complete this PH request.<br/>
                    Yours sincerely,<br/>
                    BitOness is the project with mission to create pre-algorithm boom.<br/>
                </p>
            </td>
        </tr>
    </table>
<?php endif; ?>
