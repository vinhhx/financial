<?php
Yii::$app->urlManager->setBaseUrl('http://bitoness.com');
Yii::$app->urlManager->setHostInfo('http://bitoness.com');
$baseUrl = Yii::$app->urlManager->getBaseUrl();
$this->title = 'Your request withdraw is completed.';
$title = $this->title;
?>
<table width="530" border="0">
    <tr>
        <td width="530" style="text-wrap:suppress;">
            <p>Dear <?= isset($username) ? $username : '' ?>,</p>
            <p style="text-align: center;">
                <b>You request withdraw amount<?= Yii::$app->formatter->asDecimal($amount, 8) ?> &nbsp;<?= Yii::$app->params["unit"] ?></b> <br/>
                <img src="<?= Yii::$app->image->getImg($image) ?>"/>
            </p>
            <p>
                Your commission in the account you may use it according to your purpose.<br/>
                Yours sincerely,<br/>
                BitOness is the project with mission to create pre-algorithm boom.<br/>
            </p>
        </td>
    </tr>
</table>