<?php
/**
 * Created by PhpStorm.
 * User: quyet
 * Date: 4/10/2016
 * Time: 12:31 PM
 */

namespace app\assets;


use yii\web\AssetBundle;

class FroalaEditorAsset extends AssetBundle
{
    public $sourcePath = '@bower/froala-wysiwyg-editor';

    public $js = [
        'js/froala_editor.min.js',
        'js/plugins/align.min.js',
        'js/plugins/char_counter.min.js',
        'js/plugins/code_beautifier.min.js',
        'js/plugins/code_view.min.js',
        'js/plugins/colors.min.js',
        'js/plugins/emoticons.min.js',
        'js/plugins/entities.min.js',
        'js/plugins/file.min.js',
        'js/plugins/font_family.min.js',
        'js/plugins/font_size.min.js',
        'js/plugins/fullscreen.min.js',
        'js/plugins/image.min.js',
        'js/plugins/image_manager.min.js',
        'js/plugins/inline_style.min.js',
        'js/plugins/line_breaker.min.js',
        'js/plugins/link.min.js',
        'js/plugins/lists.min.js',
        'js/plugins/paragraph_format.min.js',
        'js/plugins/paragraph_style.min.js',
        'js/plugins/quick_insert.min.js',
        'js/plugins/quote.min.js',
        'js/plugins/table.min.js',
        'js/plugins/save.min.js',
        'js/plugins/url.min.js',
        'js/plugins/video.min.js',
    ];

    public $css = [
        'css/froala_editor.min.css',
        'css/froala_style.min.css',
        'css/plugins/char_counter.css',
        'css/plugins/code_view.css',
        'css/plugins/colors.css',
        'css/plugins/emoticons.css',
        'css/plugins/file.css',
        'css/plugins/fullscreen.css',
        'css/plugins/image.css',
        'css/plugins/image_manager.css',
        'css/plugins/line_breaker.css',
        'css/plugins/quick_insert.css',
        'css/plugins/table.css',
        'css/plugins/video.css',
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}