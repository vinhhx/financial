<?php
/**
 * Created by PhpStorm.
 * User: quan
 * Date: 6/24/15
 * Time: 3:09 PM
 */

namespace app\assets;


use yii\web\AssetBundle;

class IoniconsAsset extends AssetBundle{
    public $sourcePath = '@bower/ionicons';

    public $css = [
        'css/ionicons.min.css',
    ];
}