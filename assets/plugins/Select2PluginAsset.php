<?php
/**
 * Created by PhpStorm.
 * User: vinhhx
 * Date: 18-Oct-15
 * Time: 4:55 AM
 */
namespace app\assets\Plugins;

use Yii;
use yii\web\AssetBundle;
use app\assets\AdminLteAsset;

class Select2PluginAsset extends AssetBundle
{
    public $sourcePath = '@bower/admin-lte';

    public $css = [
        'plugins/select2/select2.min.css',
    ];
    public $js = [
        'plugins/select2/select2.full.min.js',
    ];
    public $depends = [
        'app\assets\AdminLteAsset'
    ];
}
