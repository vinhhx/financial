<?php

namespace app\assets;

use yii\web\AssetBundle;

class AdminLteAsset extends AssetBundle {

    public $sourcePath = '@bower/admin-lte';

    public $js = [
        'dist/js/app.min.js',
    ];

    public $css = [
        'dist/css/AdminLTE.min.css',
        'dist/css/skins/skin-red.css',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
        'yii\web\YiiAsset',
        'app\assets\FontAwesomeAsset',
        'app\assets\IoniconsAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

}