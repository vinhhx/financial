<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Created by PhpStorm.
 * User: quan
 * Date: 6/24/15
 * Time: 3:06 PM
 */

class FontAwesomeAsset extends AssetBundle {
    public $sourcePath = '@bower/fontawesome';

    public $css = [
        'css/font-awesome.css',
    ];

}