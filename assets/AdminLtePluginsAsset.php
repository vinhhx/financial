<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminLtePluginsAsset extends AssetBundle {

    public $sourcePath = '@bower/admin-lte';
//    public $baseUrl = '@web';
    public $css = [
        'plugins/colorpicker/bootstrap-colorpicker.min.css',
    ];
    public $js = [
        'plugins/colorpicker/bootstrap-colorpicker.min.js',
    ];
    public $depends = [
        'app\haza\assets\AdminLteAsset',
    ];

}
