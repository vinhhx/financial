<?php
namespace app\assets;

use yii\web\AssetBundle;

class SymbolColorsAsset extends AssetBundle
{
    public $css = [
        'symbol-colors.css'
    ];

    public $publishOptions = [
        'only' => ['*.css']
    ];

    public function init()
    {
        parent::init();

        $this->sourcePath = __DIR__ . DIRECTORY_SEPARATOR;
    }
}