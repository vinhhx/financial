-- phpMyAdmin SQL Dump
-- version 4.0.10.14
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 03, 2016 at 10:35 PM
-- Server version: 5.5.50-cll
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `nh3dwbnv_3dworldvn`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_assignment`
--

CREATE TABLE IF NOT EXISTS `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('system-admin', '1', 1450480429),
('system-admin', '5', 1452223713);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item`
--

CREATE TABLE IF NOT EXISTS `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('customer', 2, 'Customer Controller', NULL, NULL, 1450480390, 1450480390),
('customer/change-password', 2, 'Customer Controller Action Change Password', NULL, NULL, 1450480390, 1450480390),
('customer/create', 2, 'Customer Controller Action Create', NULL, NULL, 1450480390, 1450480390),
('customer/delete', 2, 'Customer Controller Action Delete', NULL, NULL, 1450480390, 1450480390),
('customer/index', 2, 'Customer Controller Action Index', NULL, NULL, 1450480390, 1450480390),
('customer/update', 2, 'Customer Controller Action Update', NULL, NULL, 1450480390, 1450480390),
('default', 2, 'Default Controller', NULL, NULL, 1450480390, 1450480390),
('default/error', 2, 'Default Controller Action Error', NULL, NULL, 1450480390, 1450480390),
('default/index', 2, 'Default Controller Action Index', NULL, NULL, 1450480390, 1450480390),
('default/login', 2, 'Default Controller Action Login', NULL, NULL, 1450480390, 1450480390),
('default/logout', 2, 'Default Controller Action Logout', NULL, NULL, 1450480390, 1450480390),
('package', 2, 'Package Controller', NULL, NULL, 1450480390, 1450480390),
('package/create', 2, 'Package Controller Action Create', NULL, NULL, 1450480390, 1450480390),
('package/delete', 2, 'Package Controller Action Delete', NULL, NULL, 1450480390, 1450480390),
('package/index', 2, 'Package Controller Action Index', NULL, NULL, 1450480390, 1450480390),
('package/update', 2, 'Package Controller Action Update', NULL, NULL, 1450480390, 1450480390),
('package/view', 2, 'Package Controller Action View', NULL, NULL, 1450480390, 1450480390),
('permission', 2, 'Permission Controller', NULL, NULL, 1450480390, 1450480390),
('permission/index', 2, 'Permission Controller Action Index', NULL, NULL, 1450480390, 1450480390),
('permission/render-action', 2, 'Permission Controller Action Render Action', NULL, NULL, 1450480390, 1450480390),
('system-admin', 1, 'Quyền thực thi tất cả tác vụ của hệ thống', NULL, NULL, 1450480390, 1450480390),
('tool', 2, 'Tool Controller', NULL, NULL, 1450480390, 1450480390),
('tool/addmountbyweek', 2, 'Tool Controller Action Addmountbyweek', NULL, NULL, 1450480390, 1450480390),
('transaction', 2, 'Transaction Controller', NULL, NULL, 1450480390, 1450480390),
('transaction/create', 2, 'Transaction Controller Action Create', NULL, NULL, 1450480390, 1450480390),
('transaction/delete', 2, 'Transaction Controller Action Delete', NULL, NULL, 1450480390, 1450480390),
('transaction/index', 2, 'Transaction Controller Action Index', NULL, NULL, 1450480390, 1450480390),
('transaction/update', 2, 'Transaction Controller Action Update', NULL, NULL, 1450480390, 1450480390),
('transaction/view', 2, 'Transaction Controller Action View', NULL, NULL, 1450480390, 1450480390),
('user', 2, 'User Controller', NULL, NULL, 1450480390, 1450480390),
('user/create', 2, 'User Controller Action Create', NULL, NULL, 1450480390, 1450480390),
('user/index', 2, 'User Controller Action Index', NULL, NULL, 1450480390, 1450480390),
('user/permission', 2, 'User Controller Action Permission', NULL, NULL, 1450480390, 1450480390),
('user/update', 2, 'User Controller Action Update', NULL, NULL, 1450480390, 1450480390),
('user/view', 2, 'User Controller Action View', NULL, NULL, 1450480390, 1450480390),
('withdrawal', 2, 'Withdrawal Controller', NULL, NULL, 1450480390, 1450480390),
('withdrawal/accept-request', 2, 'Withdrawal Controller Action Accept Request', NULL, NULL, 1450480390, 1450480390),
('withdrawal/create', 2, 'Withdrawal Controller Action Create', NULL, NULL, 1450480390, 1450480390),
('withdrawal/delete', 2, 'Withdrawal Controller Action Delete', NULL, NULL, 1450480390, 1450480390),
('withdrawal/index', 2, 'Withdrawal Controller Action Index', NULL, NULL, 1450480390, 1450480390),
('withdrawal/reject-request', 2, 'Withdrawal Controller Action Reject Request', NULL, NULL, 1450480390, 1450480390),
('withdrawal/update', 2, 'Withdrawal Controller Action Update', NULL, NULL, 1450480390, 1450480390),
('withdrawal/view', 2, 'Withdrawal Controller Action View', NULL, NULL, 1450480390, 1450480390);

-- --------------------------------------------------------

--
-- Table structure for table `auth_item_child`
--

CREATE TABLE IF NOT EXISTS `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('customer', 'customer/change-password'),
('customer', 'customer/create'),
('customer', 'customer/delete'),
('customer', 'customer/index'),
('customer', 'customer/update'),
('default', 'default/error'),
('default', 'default/index'),
('default', 'default/login'),
('default', 'default/logout'),
('package', 'package/create'),
('package', 'package/delete'),
('package', 'package/index'),
('package', 'package/update'),
('package', 'package/view'),
('permission', 'permission/index'),
('permission', 'permission/render-action'),
('tool', 'tool/addmountbyweek'),
('transaction', 'transaction/create'),
('transaction', 'transaction/delete'),
('transaction', 'transaction/index'),
('transaction', 'transaction/update'),
('transaction', 'transaction/view'),
('user', 'user/create'),
('user', 'user/index'),
('user', 'user/permission'),
('user', 'user/update'),
('user', 'user/view'),
('withdrawal', 'withdrawal/accept-request'),
('withdrawal', 'withdrawal/create'),
('withdrawal', 'withdrawal/delete'),
('withdrawal', 'withdrawal/index'),
('withdrawal', 'withdrawal/reject-request'),
('withdrawal', 'withdrawal/update'),
('withdrawal', 'withdrawal/view');

-- --------------------------------------------------------

--
-- Table structure for table `auth_rule`
--

CREATE TABLE IF NOT EXISTS `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type` smallint(2) unsigned DEFAULT '1',
  `status` smallint(2) unsigned DEFAULT '1',
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `title`, `image`, `url`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Banner Slide', '/uploads/banner/715205/14229f39940c3d4188c2b3048a2979ed.jpg', '/', 1, 1, 1466497258, 1466497258);

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` smallint(2) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `content` text,
  `status` smallint(1) DEFAULT '1',
  `deleted` smallint(1) DEFAULT '0',
  `config` text,
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `type_id`, `name`, `alias`, `content`, `status`, `deleted`, `config`, `created_at`, `updated_at`) VALUES
(1, 2, ' Lorem Ipsum is simply dummy text of the printing', 'lorem-ipsum-is-simply-dummy-text-of-the-printing', '<p style="">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 1, 0, ' fa-apple', 1466090480, 1466090480),
(2, 2, 'It is a long established fact that a reader', 'it-is-a-long-established-fact-that-a-reader', '<p style="">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &apos;Content here, content here&apos;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &apos;lorem ipsum&apos; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 1, 0, ' fa-bank', 1466090602, 1466090602),
(3, 3, 'Content here, content here', 'content-here-content-here', '<p style="">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &apos;Content here, content here&apos;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &apos;lorem ipsum&apos; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 1, 0, '', 1466090635, 1466090635),
(4, 3, 'passages of Lorem Ipsum available,', 'passages-of-lorem-ipsum-available', '<p style="">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&apos;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&apos;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 1, 0, '', 1466090667, 1466090667),
(5, 4, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry', 'lorem-ipsum-is-simply-dummy-text-of-the-printing-and-typesetting-industry', '<p style="">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 1, 0, '', 1466090714, 1466090714),
(6, 4, 'Where does it come from?', 'where-does-it-come-from', '<p style="">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32.</p>', 1, 0, '', 1466090735, 1466090735),
(7, 4, 'Where can I get some?', 'where-can-i-get-some', '<p style="">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don&apos;t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn&apos;t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>', 1, 0, '', 1466090774, 1466090774),
(8, 1, 'What is Lorem Ipsum?', 'what-is-lorem-ipsum', '<p style="">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>', 1, 0, ' fa-anchor', 1466090833, 1466090851);

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `ref_code` varchar(255) NOT NULL DEFAULT '',
  `auth_key` varchar(255) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset` varchar(255) DEFAULT NULL,
  `package_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `user_id_first_branch` int(11) DEFAULT NULL,
  `amount` decimal(10,1) DEFAULT NULL,
  `status` smallint(2) DEFAULT '0',
  `is_invest` smallint(2) NOT NULL DEFAULT '0',
  `is_active` smallint(2) NOT NULL DEFAULT '0',
  `is_loan_admin` smallint(2) DEFAULT '0',
  `is_deleted` smallint(2) DEFAULT '0',
  `created_type` smallint(2) DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `date_join` datetime DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `sex` smallint(2) DEFAULT '0',
  `dob` date DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `identity_card` varchar(20) NOT NULL DEFAULT '',
  `email` varchar(45) DEFAULT NULL,
  `nation` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `last_auto_increase` datetime DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `poundage_level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  UNIQUE KEY `ref_code` (`ref_code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `username`, `ref_code`, `auth_key`, `password_hash`, `password_reset`, `package_id`, `parent_id`, `user_id_first_branch`, `amount`, `status`, `is_invest`, `is_active`, `is_loan_admin`, `is_deleted`, `created_type`, `created_by`, `date_join`, `full_name`, `sex`, `dob`, `phone`, `identity_card`, `email`, `nation`, `address`, `last_auto_increase`, `created_at`, `updated_at`, `poundage_level`) VALUES
(1, 'test1', '5766e2ac92d71', 'bht8Y-7bRpPNiz-NAXu1EfVC36pvWYih', '$2y$13$ZbkCjpbuK1k22E6U/Up4CO1mwvfn4nuFZXuN7nU8nUVhFn6a.aVM.', NULL, NULL, 0, NULL, NULL, 1, 1, 1, 0, 0, 2, NULL, '2016-06-20 00:00:00', 'test1', 1, NULL, '0986118464', '012519648', 'vinhhx.register@gmail.com', '1', 'Hà nội', NULL, 1466360492, 1466498193, 0),
(2, 'test2', '5766e324bfe2e', 'muckMY8chqSS6lPqt5Y9Csevjmrpwxdi', '$2y$13$iHY210b/o22h2GrPj4efNeX/VwyDP5Xy/yVaWGFCppXzYod0MySr2', NULL, NULL, 1, NULL, NULL, 1, 0, 1, 0, 0, 2, 'test1', '2016-06-20 00:00:00', 'test2', 1, NULL, '0986118465', '0125198587', 'vinhhx.register01@gmail.com', '1', 'Văn Cao-HaNoi', NULL, 1466360612, 1466360661, 0),
(3, 'test3', '5766e3861f881', 'an8KFQet_2P_3o30H-LOc7QIAAubzK9O', '$2y$13$A7DSoB1xl7bD4xWOHxnQmOgrIUIXz1mEWjtjS/gRmk5.rzOReHv5i', NULL, NULL, 2, NULL, NULL, 1, 0, 1, 0, 0, 2, 'test2', '2016-06-20 00:00:00', 'test3', 1, NULL, '0986118466', '0125859988', 'vinhhx.register02@gmail.com', '1', 'Văn Cao-HaNoi', NULL, 1466360710, 1466360761, 0),
(4, 'test4', '5766e3f7aa827', 'RWZ6C0yS9RuAqQytB5KT2nQ2mqT4OM2m', '$2y$13$C2pXpFfFDjtlqLruU1xcRuXaAW0mr65sKhyHL/FIDXFKxLfVyF.Mi', NULL, NULL, 3, NULL, NULL, 1, 0, 1, 0, 0, 2, 'test3', '2016-06-20 00:00:00', 'test4', 1, NULL, '0986118467', '0125198787', 'vinhhx.register03@gmail.com', '1', 'Văn Cao-HaNoi', NULL, 1466360823, 1466360868, 0),
(5, 'test5', '5766e45f69f39', 'c1Th-tOz-ElCC71XdhUgaQgXw-gKzD8h', '$2y$13$8cJT6hTS8jdl6FgLYs9HNuG8bsv7UBQWiFn7PpErh9Z7jhUIwgbP2', NULL, NULL, 4, NULL, NULL, 1, 0, 1, 0, 0, 2, 'test4', '2016-06-20 00:00:00', 'test5', 1, NULL, '0987161773', '01258969888', 'vinhhx.register05@gmail.com', '1', 'Văn Cao-HaNoi', NULL, 1466360927, 1466360943, 0),
(6, 'CapCha', '5766e6ffe606a', 'EQQY_5ve0V4sQeDG51JgLZkVdXjoPhQ_', '$2y$13$fPa77z4sq/DFaX8oolQrUuJjqa.QAxnmIVPzLVV9bZ5CHspxlGF5S', NULL, NULL, 1, NULL, NULL, 1, 0, 1, 0, 0, 2, NULL, '2016-06-20 00:00:00', 'Cấp Cha', 1, NULL, '0912345678', '164343913', 'vukhanh.dgr@gmail.com', '1', 'Tây Hồ, Hà Nội', NULL, 1466361599, 1466361642, 0),
(7, 'CapF1', '5766e7d5382ba', 'NfrlCnfvSZDO8Cmwinui9_jyKwwl8ws0', '$2y$13$uhXh0nPTrS6royWK7kj.CuaUHJGcheoMUe6otv4Fs.7MA/tniIU3C', NULL, NULL, 6, NULL, NULL, 1, 0, 1, 0, 0, 2, NULL, '2016-06-20 00:00:00', 'Cấp F1', 1, NULL, '0912345458', '164343935', 'vukhanhvx@gmail.com', '1', 'Đống Đa, Hà Nội', NULL, 1466361813, 1466361875, 0),
(8, 'CapF2', '5766e84585c8c', 'fxvpNaDwAFrnvy9qx_T-Pkz4PCqb-ChT', '$2y$13$VoGVPx6rFnvjCMEk5zt4jeXUw3eStuQtaZ4BG7w2hxLnN3GU/dONq', NULL, NULL, 6, NULL, NULL, 1, 0, 1, 0, 0, 2, NULL, '2016-06-20 00:00:00', 'Cấp F2', 1, NULL, '0915645678', '164983913', 'vukhanhkh@gmail.com', '1', 'Xã Đàn, Hà Nội', NULL, 1466361925, 1466362292, 0),
(9, 'CapF3', '5766e87e39052', 'HT_foAAA4tLhuObMMNhuUYNL0glMZDPO', '$2y$13$if1Fm.ikzG5iUG4gjEcC6.NPWTc8Vle.9nUWyVCohnyhqIwqfU7M6', NULL, NULL, 6, NULL, NULL, 1, 0, 1, 0, 0, 2, NULL, '2016-06-20 00:00:00', 'Cấp F3', 1, NULL, '0915648458', '164234913', 'vukhanhhh@gmail.com', '1', 'Đông Anh', NULL, 1466361982, 1466362340, 0),
(10, 'CapF4', '5766e8aad2b1a', 'qtNF7eJMnUDZ4CpfIG8vWiMOjqCnwGRp', '$2y$13$dWv0h2t.lVuI2aTl2qyqMOcfe8/KPKOtGiWBRRfYuozURdCMD1wy2', NULL, NULL, 6, NULL, NULL, 1, 0, 1, 0, 0, 2, NULL, '2016-06-20 00:00:00', 'Cấp F4', 1, NULL, '0975648458', '154234913', 'vuhanhkh@gmail.com', '1', 'Xã Đàn', NULL, 1466362026, 1466362347, 0),
(11, 'CapF5', '5766e8dd2aa18', 'dN-YYXytJ23S64JmkSL6XlVIox6qGOFk', '$2y$13$kIGrXLKve4qAAJD5bWTawe2aQZH/iBOwqINY9pqZdctRnhuDUJlV6', NULL, NULL, 6, NULL, NULL, 1, 0, 1, 0, 0, 2, NULL, '2016-06-20 00:00:00', 'Cấp F5', 1, NULL, '0975665458', '154232513', 'vuhanhht@gmail.com', '1', 'Thanh Xuân', NULL, 1466362077, 1466362352, 0),
(12, 'conf1', '5768ee2191e13', 'uWpDwJkLcqwWajDAjfLCXvze0lpNYQE4', '$2y$13$8gWH1qIXrjDefp90x7mj.elolS8nLgY1RGd5A/cGMjY/MuldFZJ.i', NULL, NULL, 7, NULL, NULL, 1, 0, 1, 0, 0, 2, NULL, '2016-06-21 00:00:00', 'Con F1', 1, NULL, '0123456789', '12346785', 'test@gmail.com', '1', 'Hà Nội', NULL, 1466494498, 1466494579, 0),
(13, 'chauf1', '5768fd07c06c3', '4CcCLGOXRMmkn52x5k5Z37NSN4gRLTfv', '$2y$13$s0VjITJuTiKL.cjjCf/PeuZkUZ6QWQioh4J2qtrg/H1BXPPQykIoO', NULL, NULL, 12, NULL, NULL, 1, 0, 1, 0, 0, 2, NULL, '2016-06-21 00:00:00', 'Cháu f1', 1, NULL, '0213546789', '65493210', 'chauf1@gmail.com', '1', 'Hà Nội', NULL, 1466498311, 1466498327, 0),
(14, 'testvn', '57690aeb552e1', 'vReMK6DKZ6F7v2D5Qo4k_tFIwl1KPAOG', '$2y$13$cJlifbwYMvSOtcnylp6sQeDIsGSEpwaJAw/HAwSdgxGqGfyiSiKsS', NULL, NULL, 6, NULL, NULL, 1, 0, 1, 0, 0, 2, NULL, '2016-06-21 00:00:00', 'anh', 1, NULL, '01223105698', '987789900', 'lljjlll@gmail.com', '1', 'hanoi', NULL, 1466501867, 1466502270, 0),
(15, 'testvn1', '5769135588cb5', '0hdl9s7hKYkS3FNYCpnIRVsZRa2cTI-w', '$2y$13$dtc/CKAWpXpEnTHfYyU7AujLPeJwzJGSOqIYEhsLzz76Tww/DIFSO', NULL, NULL, 14, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-06-21 00:00:00', 'testvn1', 1, NULL, '01233105698', '775322789', 'dhfgjghk@gmail.com', '1', 'hanoi', NULL, 1466504021, 1466504021, 0),
(16, 'admin2', '5770a5a64378b', 'DBVlpuIQdFBF4v7ufseIoXUzfTonUw0p', '$2y$13$mcLT.Lj2DqtRgTX5DWf8ruJ91OUI1xOGj4PzzoyGMN1Q.gQF7JNAa', NULL, NULL, 0, NULL, NULL, 1, 1, 1, 0, 0, 1, NULL, '2016-06-27 00:00:00', 'Admin 2', 1, '2016-06-27', '0123456798', '0102354689', 'vukhanh.dgr1@gmail.com', NULL, 'Hà Nội', NULL, 1467000230, 1467002203, 0),
(17, 'admin1', '5770a5c5f32f1', 'cApMtVhUFTaYzjsFHzQql6pmd9MM7Ykr', '$2y$13$vTBdJJ2aDUjythSHqnrh6.GMbK0YyWtOnc9TyX6t06zxrq9nJVRyC', NULL, NULL, 0, NULL, NULL, 1, 0, 1, 0, 0, 1, 'admin', '2016-06-27 00:00:00', 'hoang vinh', 1, NULL, '0986118458', '01259888855', 'vinhhx.register0099@gmail.com', NULL, 'Văn Cao-HaNoi', NULL, 1467000262, 1467000262, 0),
(18, 'abc123345', '577223b3b9d32', 'jPgEbV3R03gsVdPyeIpXJWEbQTXuHe36', '$2y$13$OLxKr5eDm9J5ExKwrLw39.rG.dD9NAvbnvfHq2jw9r2EQNQ0Xig9K', NULL, NULL, 2, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-06-28 00:00:00', 'hoang vinh', 1, NULL, '09871617733', '02548777', 'vinhhx.register808@gmail.com', '1', 'Văn Cao-HaNoi', NULL, 1467098035, 1467098035, 0),
(19, 'chatf1', '5772252195e94', 'Tq09bw9UJ2iA7QJy5IGXoaQqQDb3D4tC', '$2y$13$pV68I2/DL/aaes45/IOxbuXSkivoYXrLKmSRERn6vnUz8dSYyObZm', NULL, NULL, 13, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-06-28 00:00:00', 'Chắt F1', 1, NULL, '0354268970', '154252814', 'test8845@gmail.com', '1', 'Hà Nội', NULL, 1467098401, 1467098401, 0),
(20, 'test123', '5774e3ecaa81f', '_KCTpKGsTEluk2K-Bi6PhvYrhGpI1cO7', '$2y$13$I9EYJ46vM..4dJTVP41.QePrIkhGd2YQE1ngJxZbPwLBwkg8g9Oc6', NULL, NULL, 6, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-06-30 00:00:00', 'Test 123', 1, NULL, '0912345670', '01234567', 'test123@gmail.com', '1', 'Ha Noi', NULL, 1467278316, 1467278316, 0),
(21, 'test10', '57753c70c90e7', 'Mr-A8sWGM-3LS999fLtr28xA_UC135__', '$2y$13$tUbqhCGMH9pP1diroAaBY..uufnjK7p/fsUYNrtjwNEQewTeA9AJC', NULL, NULL, 1, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-06-30 00:00:00', 'test10', 1, NULL, '0986552244', '089855877788', 'abc123@gmail.com', '1', 'HÀ NỘI', NULL, 1467300976, 1467300976, 0),
(22, 'test20', '5775adacea3d1', 'aobDfsyyb4B-lwGwbRzpWpMArcD_M1r1', '$2y$13$.33tnixopNrd2AiUhxYCquG/mD/eGR40qAU8rp9NqUSC/NtRUTNXi', NULL, NULL, 21, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-01 00:00:00', 'test20', 1, NULL, '0986445824', '012569888', '123@gmail.com', '1', 'Ha noi', NULL, 1467329964, 1467329964, 0),
(23, 'test101', '5775ea7f681fe', 'u3YKq3NvH0TPICv3KN5YadPQigiG7Cjj', '$2y$13$ijvhnTisXA3.YBorCVvdy.fSEvm/3/ybmcMHvmeJRcZekrmWR7vsW', NULL, NULL, 1, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-01 00:00:00', 'test101', 1, NULL, '0986554144', '0125488588', '123443@gmail.com', '1', 'Hà nội', NULL, 1467345535, 1467345535, 0),
(24, 'tet102', '577600cf1ede2', 'i875CNMTw0wSmQbBTyI1JyYm5rOJTwPd', '$2y$13$WGjH8ZzLSMzDiCFgRtWVtOgZ1cbV1YbWQB23lqNo0qJ7ceJ45qgd2', NULL, NULL, 23, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-01 00:00:00', 'test102', 1, NULL, '0986114585', '0215584885', '123456@gmail.com', '1', 'Ha noi', NULL, 1467351247, 1467351247, 0),
(25, 'test102', '5776017a37658', 'WiQnVRv02wyNslIdOrrztglAxMhXYV4i', '$2y$13$1GQFkXndM1ujnU0kCtJDL.RqjTBN.48ifzYz3EIn/MZsCFt36Wtei', NULL, NULL, 23, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-01 00:00:00', 'test102', 1, NULL, '0986558553', '0125198548', 'vnhhh@gmail.com', '1', 'Ha noi', NULL, 1467351418, 1467351418, 0),
(26, 'test1010', '57760a6189231', 'QIyzKwrh1gro0Ez88dm8ERW2rHeBxroX', '$2y$13$6U2I5cOAxNLmV3pD4uRirOcciUMLuAKtw41qUpMJRcX61akvYyTUW', NULL, NULL, 23, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-01 00:00:00', 'test1010', 1, NULL, '0987558544', '0125488955', '2335ee@gmail.com', '1', 'Hà nội', NULL, 1467353697, 1467353697, 0),
(27, 'test1020', '57760b1ff3e88', '5lKCfuupWbtaB2ceXAzncPMs0G277O3S', '$2y$13$K1mW.YNNgjg5FvcZM43Py.J67cOVEWrkvTPWTtA7dQrv7higfvm3S', NULL, NULL, 25, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-01 00:00:00', 'test1020', 1, NULL, '0984458528', '0125488778', 'ddddd@gmail.com', '1', 'Ha noi', NULL, 1467353888, 1467353888, 0),
(28, 'test101010', '57763fa4ee691', '1E9x7JqIpm0hcpau4h4gsdHF6sPpUC0G', '$2y$13$K2/e5B/FPgEvE6EadUwSk.nwoY7aSKo9a4h9XQxiM.2ylK2iyayy6', NULL, NULL, 26, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-01 00:00:00', 'test101010', 1, NULL, '0321654897', '032156498', 'test101010@gmail.com', '1', 'Hà Nội', NULL, 1467367333, 1467367333, 0),
(29, 'test101011', '57764843eb8c1', 'GCM1vyQMpttSJlY575675xnZkI0SsPk4', '$2y$13$0hTG1RBHgFKEmDpRQBeUDONKeFRucU/TbqGhn5pYdnp0B6Rhc5nGy', NULL, NULL, 28, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-01 00:00:00', 'test101011', 1, NULL, '0321645987', '031254687', 'test101011lk@gmail.com', '1', 'Ha Noi', NULL, 1467369540, 1467369540, 0),
(30, 'test101012', '57764b48beaaf', 'BBmzwEw_0J4i4uUXYqbfCQzyxy5lzwIZ', '$2y$13$jJ0n34gLFqe31O.HJHKHperHxRUSAXGerB8bLrZox7Y8het/iZNke', NULL, NULL, 28, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-01 00:00:00', 'test101012', 1, NULL, '0977704585', '0102354687', 'test101012@gmail.com', '1', 'Đống Đa, Hà Nội', NULL, 1467370312, 1467370312, 0),
(31, 'test10101', '5776971a051be', 'Ceh2CzEbkPl4KrAYPTzud3JbL91lMASj', '$2y$13$UZtpxYQ.nLCkAcUVI2W2xuWtblAIYplFaj5l/m8kSh2IeYBMnQRh6', NULL, NULL, 26, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-01 00:00:00', 'test10101', 1, NULL, '0321456987', '031256478', 'test10101@gmail.com', '1', 'Hà Nội', NULL, 1467389722, 1467389722, 0),
(32, 'capcon', '577782e9014ed', 'yDkzt7ocqftEV13y8CRrOn-hlHi9P-aW', '$2y$13$QpeUAhNTO1JqSaaJRzrJNeZlcVRebv5cAAa.DienY5YZQHMhfCMT.', NULL, NULL, 6, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-02 00:00:00', 'capcon', 1, NULL, '0123452289', '134242914', 'capcontest1@gmail.com', '1', 'Ha noi', NULL, 1467450089, 1467450089, 0),
(33, 'capcon02', '57778634e503b', 'kWxWzhCRYFFnDDAxSRL5UVErRasmYEil', '$2y$13$A3jrtKHyWDKig3D/je7aCumgf.sVi4kI8Odwj8Yb2xwkLVL/mnkl6', NULL, NULL, 6, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-02 00:00:00', 'capcon02', 1, NULL, '0931684448', '164245553', 'capcon02te@gmail.com', '1', 'Hà Nội', NULL, 1467450932, 1467450932, 0),
(34, 'capcon01', '57778766e969f', '5A_q5L2wWSrk8RvU-ZEjdP9o4m13Lqc-', '$2y$13$.nPIyyoY8GjwJWwMC8lWwuOPuonZqQUnhF8S5NY2tJU0xhV9AovAi', NULL, NULL, 32, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-02 00:00:00', 'capcon01', 1, NULL, '0354263370', '12342285', 'capcon01test@gmail.com', '1', 'Hà Nội', NULL, 1467451238, 1467451238, 0),
(35, 'capcon011', '5777881f4312c', 'jqw3HWXhle-F0hFE3wm9Tc3f81xXUKcc', '$2y$13$DMi6QJO9MKfuI/BHKmra5OUZESCczHwJ3RQmkJYvteW8/fve9ywsS', NULL, NULL, 32, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-02 00:00:00', 'capcon011', 1, NULL, '0913456978', '143612549', 'capcon011@gmail.com', '1', 'Ha Noi', NULL, 1467451423, 1467451423, 0),
(36, 'capcon021', '57778f02b1e86', 'jaCNCxXYLZxTSYWpC4hQgZp_u-o--3CK', '$2y$13$5x3C70R3brjsMbKv0qBJg.B2rpQUnT15bHj5mnbQ/QpOK88qTSu5u', NULL, NULL, 33, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-02 00:00:00', 'capcon021', 1, NULL, '0213654789', '321654897', 'capcon021test@gmail.com', '1', 'Hà Nội', NULL, 1467453186, 1467453186, 0),
(37, 'capchaf1', '5778b29321a6e', '1vjbZX2ra4BMIube-icBfnzhKfqu1nKQ', '$2y$13$MsFuA48VluCLzUtwvpS8ZeFFEAP9tXvYdmMdPOUIOaoYySa26B0A2', NULL, NULL, 6, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-03 00:00:00', 'capchaf1', 1, NULL, '0124563798', '121343568', 'capchaf1@gmail.com', '1', 'Hà Nội', NULL, 1467527827, 1467527827, 0),
(38, 'capchaf11', '5778b6bdc7a87', 'WWv1bUgVk1qzUhBIRxgbljpuHRuV6YbG', '$2y$13$tFL54Hzmq4g6WGAPaW0oq.FA7.tBz37FrzdAQoCpHwJE1eS.3E1BW', NULL, NULL, 6, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-03 00:00:00', 'capchaf11', 1, NULL, '0931689277', '164244988', 'capchaf11@gmail.com', '1', 'Ha noi', NULL, 1467528893, 1467528893, 0),
(39, 'capchaf2', '5778b8524af17', 'TEvjql8jNyk7a36QtlwVwKwHCULox62_', '$2y$13$Tq3td4WBM891jboCaD7Zd./HNFSF/YUfEFnd.bn3au0fnPRmbQzXG', NULL, NULL, 37, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-03 00:00:00', 'capchaf2', 1, NULL, '0123456669', '12346555', 'capchaf2@gmail.com', '1', 'Hà Nội', NULL, 1467529298, 1467529298, 0),
(40, 'capchaf22', '5778bbe558aac', 'Ufp5a2x2CaIJJTx3Av5dTGkYE1m_nXkb', '$2y$13$hrmMLXq8DlodQ1pUUuDZNO4muvrn1SupXbRLMRTOq2u9R2TjsahVG', NULL, NULL, 37, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-03 00:00:00', 'capchaf22', 1, NULL, '03251647977', '132456857', 'capchaf22@gmail.com', '1', 'Hà Nội', NULL, 1467530213, 1467530213, 0),
(41, 'capchaf3', '5778cade8c889', 'y3pasMC7M9BfrEpFZxrVhwS_1lRp4ivW', '$2y$13$gbmRp5tgjsQGNhXrmRvBW.8ncv0oslOvBTRdvtX2CtcoP/9ajXr9C', NULL, NULL, 39, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-03 00:00:00', 'capchaf3', 1, NULL, '0512346789', '321564798', 'capchaf3@gmail.com', '1', 'Hanoi', NULL, 1467534046, 1467534046, 0),
(42, 'capchaf33', '5778cbb417097', '6Ntlg-oX7cTs8OavvjqWqGlwynTbGZgj', '$2y$13$YnNH8G0mSSYiLUL5R.LOaelnC2Jj1G/VVb66.hGSJg5HezoBLxfzy', NULL, NULL, 39, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-03 00:00:00', 'capchaf33', 1, NULL, '0987321456', '867945132', 'capchaf33@gmail.com', '1', 'Ha Noi', NULL, 1467534260, 1467534260, 0),
(43, 'capchaf1101', '5778d14e3032f', 'PilDPCqj_uvz1FPhejo6MKufmMttESAU', '$2y$13$KeDkA8lNutZshBw85dpwQe30l.ymGuiTSQX8wOGoTEza2gCpEiyMO', NULL, NULL, 38, NULL, NULL, 1, 0, 0, 0, 0, 2, NULL, '2016-07-03 00:00:00', 'capchaf1101', 1, NULL, '0925123648', '582369741', 'capchaf1101@gmail.com', '2', 'Hanoi', NULL, 1467535694, 1467535694, 0);

-- --------------------------------------------------------

--
-- Table structure for table `customer_activity`
--

CREATE TABLE IF NOT EXISTS `customer_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `customer_username` varchar(255) NOT NULL,
  `type` smallint(4) NOT NULL DEFAULT '0',
  `message` varchar(1000) NOT NULL DEFAULT '',
  `params` text,
  `created_at` int(11) NOT NULL DEFAULT '0',
  `updated_at` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=83 ;

--
-- Dumping data for table `customer_activity`
--

INSERT INTO `customer_activity` (`id`, `customer_id`, `customer_username`, `type`, `message`, `params`, `created_at`, `updated_at`) VALUES
(1, 1, 'test1', 1, 'Đăng ký thành công tài khoản', '{"id":1,"username":"test1","ref_code":"5766e2ac92d71","auth_key":"bht8Y-7bRpPNiz-NAXu1EfVC36pvWYih","password_hash":"$2y$13$ZbkCjpbuK1k22E6U\\/Up4CO1mwvfn4nuFZXuN7nU8nUVhFn6a.aVM.","password_reset":null,"package_id":null,"parent_id":0,"user_id_first_branch":null,"amount":null,"status":1,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-20","full_name":"test1","sex":"1","dob":null,"phone":"0986118464","identity_card":"012519648","email":"vinhhx.register@gmail.com","nation":"1","address":"H\\u00e0 n\\u1ed9i","last_auto_increase":null,"created_at":1466360492,"updated_at":1466360492,"poundage_level":null}', 1466360492, 1466360492),
(2, 1, 'test1', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 0.5000 bitcoin', '{"id":1,"customer_id":1,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466360557,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466360557,"updated_at":1466360557,"balance":null}', 1466360557, 1466360557),
(3, 2, 'test2', 1, 'Đăng ký thành công tài khoản', '{"id":2,"username":"test2","ref_code":"5766e324bfe2e","auth_key":"muckMY8chqSS6lPqt5Y9Csevjmrpwxdi","password_hash":"$2y$13$iHY210b\\/o22h2GrPj4efNeX\\/VwyDP5Xy\\/yVaWGFCppXzYod0MySr2","password_reset":null,"package_id":null,"parent_id":1,"user_id_first_branch":null,"amount":null,"status":1,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":"test1","date_join":"2016-06-20","full_name":"test2","sex":"1","dob":null,"phone":"0986118465","identity_card":"0125198587","email":"vinhhx.register01@gmail.com","nation":"1","address":"V\\u0103n Cao-HaNoi","last_auto_increase":null,"created_at":1466360612,"updated_at":1466360612,"poundage_level":null}', 1466360612, 1466360612),
(4, 2, 'test2', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 1.0000 bitcoin', '{"id":2,"customer_id":2,"package_id":2,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"1.0000","increase_amount":25,"total":null,"is_reinvestment":null,"reinvestment_date":1466360673,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466360673,"updated_at":1466360673,"balance":null}', 1466360673, 1466360673),
(5, 3, 'test3', 1, 'Đăng ký thành công tài khoản', '{"id":3,"username":"test3","ref_code":"5766e3861f881","auth_key":"an8KFQet_2P_3o30H-LOc7QIAAubzK9O","password_hash":"$2y$13$A7DSoB1xl7bD4xWOHxnQmOgrIUIXz1mEWjtjS\\/gRmk5.rzOReHv5i","password_reset":null,"package_id":null,"parent_id":2,"user_id_first_branch":null,"amount":null,"status":1,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":"test2","date_join":"2016-06-20","full_name":"test3","sex":"1","dob":null,"phone":"0986118466","identity_card":"0125859988","email":"vinhhx.register02@gmail.com","nation":"1","address":"V\\u0103n Cao-HaNoi","last_auto_increase":null,"created_at":1466360710,"updated_at":1466360710,"poundage_level":null}', 1466360710, 1466360710),
(6, 3, 'test3', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 5.0000 bitcoin', '{"id":3,"customer_id":3,"package_id":4,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"5.0000","increase_amount":45,"total":null,"is_reinvestment":null,"reinvestment_date":1466360775,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466360775,"updated_at":1466360775,"balance":null}', 1466360775, 1466360775),
(7, 4, 'test4', 1, 'Đăng ký thành công tài khoản', '{"id":4,"username":"test4","ref_code":"5766e3f7aa827","auth_key":"RWZ6C0yS9RuAqQytB5KT2nQ2mqT4OM2m","password_hash":"$2y$13$C2pXpFfFDjtlqLruU1xcRuXaAW0mr65sKhyHL\\/FIDXFKxLfVyF.Mi","password_reset":null,"package_id":null,"parent_id":3,"user_id_first_branch":null,"amount":null,"status":1,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":"test3","date_join":"2016-06-20","full_name":"test4","sex":"1","dob":null,"phone":"0986118467","identity_card":"0125198787","email":"vinhhx.register03@gmail.com","nation":"1","address":"V\\u0103n Cao-HaNoi","last_auto_increase":null,"created_at":1466360823,"updated_at":1466360823,"poundage_level":null}', 1466360823, 1466360823),
(8, 4, 'test4', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 10.0000 bitcoin', '{"id":4,"customer_id":4,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":null,"reinvestment_date":1466360882,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466360882,"updated_at":1466360882,"balance":null}', 1466360882, 1466360882),
(9, 5, 'test5', 1, 'Đăng ký thành công tài khoản', '{"id":5,"username":"test5","ref_code":"5766e45f69f39","auth_key":"c1Th-tOz-ElCC71XdhUgaQgXw-gKzD8h","password_hash":"$2y$13$8cJT6hTS8jdl6FgLYs9HNuG8bsv7UBQWiFn7PpErh9Z7jhUIwgbP2","password_reset":null,"package_id":null,"parent_id":4,"user_id_first_branch":null,"amount":null,"status":1,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":"test4","date_join":"2016-06-20","full_name":"test5","sex":"1","dob":null,"phone":"0987161773","identity_card":"01258969888","email":"vinhhx.register05@gmail.com","nation":"1","address":"V\\u0103n Cao-HaNoi","last_auto_increase":null,"created_at":1466360927,"updated_at":1466360927,"poundage_level":null}', 1466360927, 1466360927),
(10, 5, 'test5', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 10.0000 bitcoin', '{"id":5,"customer_id":5,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":null,"reinvestment_date":1466360961,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466360961,"updated_at":1466360961,"balance":null}', 1466360961, 1466360961),
(11, 6, 'CapCha', 1, 'Đăng ký thành công tài khoản', '{"id":6,"username":"CapCha","ref_code":"5766e6ffe606a","auth_key":"EQQY_5ve0V4sQeDG51JgLZkVdXjoPhQ_","password_hash":"$2y$13$fPa77z4sq\\/DFaX8oolQrUuJjqa.QAxnmIVPzLVV9bZ5CHspxlGF5S","password_reset":null,"package_id":null,"parent_id":1,"user_id_first_branch":null,"amount":null,"status":1,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-20","full_name":"C\\u1ea5p Cha","sex":"1","dob":null,"phone":"0912345678","identity_card":"164343913","email":"vukhanh.dgr@gmail.com","nation":"1","address":"T\\u00e2y H\\u1ed3, H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1466361599,"updated_at":1466361599,"poundage_level":null}', 1466361599, 1466361599),
(12, 7, 'CapF1', 1, 'Đăng ký thành công tài khoản', '{"id":7,"username":"CapF1","ref_code":"5766e7d5382ba","auth_key":"NfrlCnfvSZDO8Cmwinui9_jyKwwl8ws0","password_hash":"$2y$13$uhXh0nPTrS6royWK7kj.CuaUHJGcheoMUe6otv4Fs.7MA\\/tniIU3C","password_reset":null,"package_id":null,"parent_id":6,"user_id_first_branch":null,"amount":null,"status":1,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-20","full_name":"C\\u1ea5p F1","sex":"1","dob":null,"phone":"0912345458","identity_card":"164343935","email":"vukhanhvx@gmail.com","nation":"1","address":"\\u0110\\u1ed1ng \\u0110a, H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1466361813,"updated_at":1466361813,"poundage_level":null}', 1466361813, 1466361813),
(13, 8, 'CapF2', 1, 'Đăng ký thành công tài khoản', '{"id":8,"username":"CapF2","ref_code":"5766e84585c8c","auth_key":"fxvpNaDwAFrnvy9qx_T-Pkz4PCqb-ChT","password_hash":"$2y$13$VoGVPx6rFnvjCMEk5zt4jeXUw3eStuQtaZ4BG7w2hxLnN3GU\\/dONq","password_reset":null,"package_id":null,"parent_id":6,"user_id_first_branch":null,"amount":null,"status":1,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-20","full_name":"C\\u1ea5p F2","sex":"1","dob":null,"phone":"0915645678","identity_card":"164983913","email":"vukhanhkh@gmail.com","nation":"1","address":"X\\u00e3 \\u0110\\u00e0n, H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1466361925,"updated_at":1466361925,"poundage_level":null}', 1466361925, 1466361925),
(14, 9, 'CapF3', 1, 'Đăng ký thành công tài khoản', '{"id":9,"username":"CapF3","ref_code":"5766e87e39052","auth_key":"HT_foAAA4tLhuObMMNhuUYNL0glMZDPO","password_hash":"$2y$13$if1Fm.ikzG5iUG4gjEcC6.NPWTc8Vle.9nUWyVCohnyhqIwqfU7M6","password_reset":null,"package_id":null,"parent_id":6,"user_id_first_branch":null,"amount":null,"status":1,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-20","full_name":"C\\u1ea5p F3","sex":"1","dob":null,"phone":"0915648458","identity_card":"164234913","email":"vukhanhhh@gmail.com","nation":"1","address":"\\u0110\\u00f4ng Anh","last_auto_increase":null,"created_at":1466361982,"updated_at":1466361982,"poundage_level":null}', 1466361982, 1466361982),
(15, 10, 'CapF4', 1, 'Đăng ký thành công tài khoản', '{"id":10,"username":"CapF4","ref_code":"5766e8aad2b1a","auth_key":"qtNF7eJMnUDZ4CpfIG8vWiMOjqCnwGRp","password_hash":"$2y$13$dWv0h2t.lVuI2aTl2qyqMOcfe8\\/KPKOtGiWBRRfYuozURdCMD1wy2","password_reset":null,"package_id":null,"parent_id":6,"user_id_first_branch":null,"amount":null,"status":1,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-20","full_name":"C\\u1ea5p F4","sex":"1","dob":null,"phone":"0975648458","identity_card":"154234913","email":"vuhanhkh@gmail.com","nation":"1","address":"X\\u00e3 \\u0110\\u00e0n","last_auto_increase":null,"created_at":1466362026,"updated_at":1466362026,"poundage_level":null}', 1466362026, 1466362026),
(16, 11, 'CapF5', 1, 'Đăng ký thành công tài khoản', '{"id":11,"username":"CapF5","ref_code":"5766e8dd2aa18","auth_key":"dN-YYXytJ23S64JmkSL6XlVIox6qGOFk","password_hash":"$2y$13$kIGrXLKve4qAAJD5bWTawe2aQZH\\/iBOwqINY9pqZdctRnhuDUJlV6","password_reset":null,"package_id":null,"parent_id":6,"user_id_first_branch":null,"amount":null,"status":1,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-20","full_name":"C\\u1ea5p F5","sex":"1","dob":null,"phone":"0975665458","identity_card":"154232513","email":"vuhanhht@gmail.com","nation":"1","address":"Thanh Xu\\u00e2n","last_auto_increase":null,"created_at":1466362077,"updated_at":1466362077,"poundage_level":null}', 1466362077, 1466362077),
(17, 6, 'CapCha', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 0.5000 bitcoin', '{"id":6,"customer_id":6,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466362111,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466362111,"updated_at":1466362111,"balance":null}', 1466362111, 1466362111),
(18, 7, 'CapF1', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 0.5000 bitcoin', '{"id":7,"customer_id":7,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466362248,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466362248,"updated_at":1466362248,"balance":null}', 1466362248, 1466362248),
(19, 8, 'CapF2', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 0.5000 bitcoin', '{"id":8,"customer_id":8,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466362321,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466362321,"updated_at":1466362321,"balance":null}', 1466362321, 1466362321),
(20, 10, 'CapF4', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 0.5000 bitcoin', '{"id":9,"customer_id":10,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466362449,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466362449,"updated_at":1466362449,"balance":null}', 1466362449, 1466362449),
(21, 9, 'CapF3', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 0.5000 bitcoin', '{"id":10,"customer_id":9,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466362664,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466362664,"updated_at":1466362664,"balance":null}', 1466362664, 1466362664),
(22, 11, 'CapF5', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 1.0000 bitcoin', '{"id":11,"customer_id":11,"package_id":2,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"1.0000","increase_amount":25,"total":null,"is_reinvestment":null,"reinvestment_date":1466362680,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466362680,"updated_at":1466362680,"balance":null}', 1466362680, 1466362680),
(23, 6, 'CapCha', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 10.0000 bitcoin', '{"id":12,"customer_id":6,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":null,"reinvestment_date":1466363510,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466363510,"updated_at":1466363510,"balance":null}', 1466363510, 1466363510),
(24, 10, 'CapF4', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 5.0000 bitcoin', '{"id":13,"customer_id":10,"package_id":4,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"5.0000","increase_amount":45,"total":null,"is_reinvestment":null,"reinvestment_date":1466363539,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466363539,"updated_at":1466363539,"balance":null}', 1466363539, 1466363539),
(25, 11, 'CapF5', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 2.0000 bitcoin', '{"id":14,"customer_id":11,"package_id":3,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"2.0000","increase_amount":30,"total":null,"is_reinvestment":null,"reinvestment_date":1466363556,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466363556,"updated_at":1466363556,"balance":null}', 1466363556, 1466363556),
(26, 7, 'CapF1', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 1.0000 bitcoin', '{"id":15,"customer_id":7,"package_id":2,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"1.0000","increase_amount":25,"total":null,"is_reinvestment":null,"reinvestment_date":1466366036,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466366036,"updated_at":1466366036,"balance":null}', 1466366036, 1466366036),
(27, 8, 'CapF2', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 5.0000 bitcoin', '{"id":16,"customer_id":8,"package_id":4,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"5.0000","increase_amount":45,"total":null,"is_reinvestment":null,"reinvestment_date":1466366082,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466366082,"updated_at":1466366082,"balance":null}', 1466366082, 1466366082),
(28, 9, 'CapF3', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 2.0000 bitcoin', '{"id":17,"customer_id":9,"package_id":3,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"2.0000","increase_amount":30,"total":null,"is_reinvestment":null,"reinvestment_date":1466366096,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466366096,"updated_at":1466366096,"balance":null}', 1466366096, 1466366096),
(29, 1, 'test1', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 10.0000 bitcoin', '{"id":18,"customer_id":1,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":null,"reinvestment_date":1466367652,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466367652,"updated_at":1466367652,"balance":null}', 1466367652, 1466367652),
(30, 4, 'test4', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 5.0000 bitcoin', '{"id":19,"customer_id":4,"package_id":4,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"5.0000","increase_amount":45,"total":null,"is_reinvestment":null,"reinvestment_date":1466367677,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466367677,"updated_at":1466367677,"balance":null}', 1466367677, 1466367677),
(31, 2, 'test2', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 2.0000 bitcoin', '{"id":20,"customer_id":2,"package_id":3,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"2.0000","increase_amount":30,"total":null,"is_reinvestment":null,"reinvestment_date":1466367710,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466367710,"updated_at":1466367710,"balance":null}', 1466367710, 1466367710),
(32, 3, 'test3', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 5.0000 bitcoin', '{"id":21,"customer_id":3,"package_id":4,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"5.0000","increase_amount":45,"total":null,"is_reinvestment":null,"reinvestment_date":1466367725,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466367725,"updated_at":1466367725,"balance":null}', 1466367725, 1466367725),
(33, 5, 'test5', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 5.0000 bitcoin', '{"id":22,"customer_id":5,"package_id":4,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"5.0000","increase_amount":45,"total":null,"is_reinvestment":null,"reinvestment_date":1466367740,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466367740,"updated_at":1466367740,"balance":null}', 1466367740, 1466367740),
(34, 5, 'test5', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 3 trị giá 10.0000 bitcoin', '{"id":23,"customer_id":5,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":null,"reinvestment_date":1466368704,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466368704,"updated_at":1466368704,"balance":null}', 1466368704, 1466368704),
(35, 2, 'test2', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 3 trị giá 10.0000 bitcoin', '{"id":24,"customer_id":2,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":null,"reinvestment_date":1466369367,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466369367,"updated_at":1466369367,"balance":null}', 1466369367, 1466369367),
(36, 6, 'CapCha', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 10.0000 bitcoin', '{"id":6,"customer_id":6,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"10.0000","increase_amount":65,"total":"0.0000","is_reinvestment":1,"reinvestment_date":1466384608,"status":1,"time_sent_completed":1466365837,"is_active":10,"type":0,"block_chain_wallet_id":null,"created_at":1466362111,"updated_at":1466384608,"balance":"0.0000"}', 1466384608, 1466384608),
(37, 12, 'conf1', 1, 'Đăng ký thành công tài khoản', '{"id":12,"username":"conf1","ref_code":"5768ee2191e13","auth_key":"uWpDwJkLcqwWajDAjfLCXvze0lpNYQE4","password_hash":"$2y$13$8gWH1qIXrjDefp90x7mj.elolS8nLgY1RGd5A\\/cGMjY\\/MuldFZJ.i","password_reset":null,"package_id":null,"parent_id":7,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-21","full_name":"Con F1","sex":"1","dob":null,"phone":"0123456789","identity_card":"12346785","email":"test@gmail.com","nation":"1","address":"H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1466494498,"updated_at":1466494498,"poundage_level":null}', 1466494498, 1466494498),
(38, 12, 'conf1', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 10.0000 bitcoin', '{"id":25,"customer_id":12,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1466494765,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466494765,"updated_at":1466494765,"balance":null}', 1466494765, 1466494765),
(39, 13, 'chauf1', 1, 'Đăng ký thành công tài khoản', '{"id":13,"username":"chauf1","ref_code":"5768fd07c06c3","auth_key":"4CcCLGOXRMmkn52x5k5Z37NSN4gRLTfv","password_hash":"$2y$13$s0VjITJuTiKL.cjjCf\\/PeuZkUZ6QWQioh4J2qtrg\\/H1BXPPQykIoO","password_reset":null,"package_id":null,"parent_id":12,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-21","full_name":"Ch\\u00e1u f1","sex":"1","dob":null,"phone":"0213546789","identity_card":"65493210","email":"chauf1@gmail.com","nation":"1","address":"H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1466498311,"updated_at":1466498311,"poundage_level":null}', 1466498311, 1466498311),
(40, 13, 'chauf1', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 10.0000 bitcoin', '{"id":26,"customer_id":13,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1466498360,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466498360,"updated_at":1466498360,"balance":null}', 1466498360, 1466498360),
(41, 14, 'testvn', 1, 'Đăng ký thành công tài khoản', '{"id":14,"username":"testvn","ref_code":"57690aeb552e1","auth_key":"vReMK6DKZ6F7v2D5Qo4k_tFIwl1KPAOG","password_hash":"$2y$13$cJlifbwYMvSOtcnylp6sQeDIsGSEpwaJAw\\/HAwSdgxGqGfyiSiKsS","password_reset":null,"package_id":null,"parent_id":6,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-21","full_name":"anh","sex":"1","dob":null,"phone":"01223105698","identity_card":"987789900","email":"lljjlll@gmail.com","nation":"1","address":"hanoi","last_auto_increase":null,"created_at":1466501867,"updated_at":1466501867,"poundage_level":null}', 1466501867, 1466501867),
(42, 14, 'testvn', 2, 'Kích hoạt tài khoản thành công', '{"id":14,"token":"R_bvkdtStgqWV1BBHzOsexABO88M2ecZ_1466502193","customer_id":14,"status":1,"type":0,"expired_at":0,"created_at":1466502193,"updated_at":1466502270}', 1466502270, 1466502270),
(43, 14, 'testvn', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 1 trị giá 0.5000 bitcoin', '{"id":27,"customer_id":14,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":0,"reinvestment_date":1466502367,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466502367,"updated_at":1466502367,"balance":null}', 1466502367, 1466502367),
(44, 14, 'testvn', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 10.0000 bitcoin', '{"id":28,"customer_id":14,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1466503503,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466503503,"updated_at":1466503503,"balance":null}', 1466503503, 1466503503),
(45, 15, 'testvn1', 1, 'Đăng ký thành công tài khoản', '{"id":15,"username":"testvn1","ref_code":"5769135588cb5","auth_key":"0hdl9s7hKYkS3FNYCpnIRVsZRa2cTI-w","password_hash":"$2y$13$dtc\\/CKAWpXpEnTHfYyU7AujLPeJwzJGSOqIYEhsLzz76Tww\\/DIFSO","password_reset":null,"package_id":null,"parent_id":14,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-21","full_name":"testvn1","sex":"1","dob":null,"phone":"01233105698","identity_card":"775322789","email":"dhfgjghk@gmail.com","nation":"1","address":"hanoi","last_auto_increase":null,"created_at":1466504021,"updated_at":1466504021,"poundage_level":null}', 1466504021, 1466504021),
(46, 1, 'test1', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 3 trị giá 1.0000 bitcoin', '{"id":29,"customer_id":1,"package_id":2,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"1.0000","increase_amount":25,"total":null,"is_reinvestment":1,"reinvestment_date":1466930949,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466930949,"updated_at":1466930949,"balance":null}', 1466930949, 1466930949),
(47, 16, 'admin2', 1, 'Đăng ký thành công tài khoản', '{"id":16,"username":"admin2","ref_code":"5770a5a64378b","auth_key":"DBVlpuIQdFBF4v7ufseIoXUzfTonUw0p","password_hash":"$2y$13$mcLT.Lj2DqtRgTX5DWf8ruJ91OUI1xOGj4PzzoyGMN1Q.gQF7JNAa","password_reset":null,"package_id":null,"parent_id":0,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":1,"is_loan_admin":null,"is_deleted":null,"created_type":1,"created_by":null,"date_join":"2016-06-27","full_name":"Admin 2","sex":"1","dob":"2016-06-27","phone":"0123456798","identity_card":"0102354689","email":"vukhanh.dgr1@gmail.com","nation":null,"address":"H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1467000230,"updated_at":1467000230,"poundage_level":null}', 1467000230, 1467000230),
(48, 17, 'admin1', 1, 'Đăng ký thành công tài khoản', '{"id":17,"username":"admin1","ref_code":"5770a5c5f32f1","auth_key":"cApMtVhUFTaYzjsFHzQql6pmd9MM7Ykr","password_hash":"$2y$13$vTBdJJ2aDUjythSHqnrh6.GMbK0YyWtOnc9TyX6t06zxrq9nJVRyC","password_reset":null,"package_id":null,"parent_id":0,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":1,"is_loan_admin":null,"is_deleted":null,"created_type":1,"created_by":"admin","date_join":"2016-06-27","full_name":"hoang vinh","sex":"1","dob":"","phone":"0986118458","identity_card":"01259888855","email":"vinhhx.register0099@gmail.com","nation":null,"address":"V\\u0103n Cao-HaNoi","last_auto_increase":null,"created_at":1467000262,"updated_at":1467000262,"poundage_level":null}', 1467000262, 1467000262),
(49, 7, 'CapF1', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 3 trị giá 10.0000 bitcoin', '{"id":30,"customer_id":7,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467087132,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467087132,"updated_at":1467087132,"balance":null}', 1467087133, 1467087133),
(50, 9, 'CapF3', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 3 trị giá 10.0000 bitcoin', '{"id":31,"customer_id":9,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467088062,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467088062,"updated_at":1467088062,"balance":null}', 1467088062, 1467088062),
(51, 12, 'conf1', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 10.0000 bitcoin', '{"id":32,"customer_id":12,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467089045,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467089045,"updated_at":1467089045,"balance":null}', 1467089045, 1467089045),
(52, 7, 'CapF1', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 4 trị giá 10.0000 bitcoin', '{"id":33,"customer_id":7,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 4","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467089188,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467089188,"updated_at":1467089188,"balance":null}', 1467089188, 1467089188),
(53, 6, 'CapCha', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 3 trị giá 10.0000 bitcoin', '{"id":34,"customer_id":6,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467090625,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467090625,"updated_at":1467090625,"balance":null}', 1467090625, 1467090625),
(54, 12, 'conf1', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 3 trị giá 10.0000 bitcoin', '{"id":35,"customer_id":12,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467090737,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467090737,"updated_at":1467090737,"balance":null}', 1467090737, 1467090737),
(55, 13, 'chauf1', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 2 trị giá 10.0000 bitcoin', '{"id":36,"customer_id":13,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467092663,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467092663,"updated_at":1467092663,"balance":null}', 1467092663, 1467092663),
(56, 18, 'abc123345', 1, 'Đăng ký thành công tài khoản', '{"id":18,"username":"abc123345","ref_code":"577223b3b9d32","auth_key":"jPgEbV3R03gsVdPyeIpXJWEbQTXuHe36","password_hash":"$2y$13$OLxKr5eDm9J5ExKwrLw39.rG.dD9NAvbnvfHq2jw9r2EQNQ0Xig9K","password_reset":null,"package_id":null,"parent_id":2,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-28","full_name":"hoang vinh","sex":"1","dob":null,"phone":"09871617733","identity_card":"02548777","email":"vinhhx.register808@gmail.com","nation":"1","address":"V\\u0103n Cao-HaNoi","last_auto_increase":null,"created_at":1467098035,"updated_at":1467098035,"poundage_level":null}', 1467098035, 1467098035),
(57, 19, 'chatf1', 1, 'Đăng ký thành công tài khoản', '{"id":19,"username":"chatf1","ref_code":"5772252195e94","auth_key":"Tq09bw9UJ2iA7QJy5IGXoaQqQDb3D4tC","password_hash":"$2y$13$pV68I2\\/DL\\/aaes45\\/IOxbuXSkivoYXrLKmSRERn6vnUz8dSYyObZm","password_reset":null,"package_id":null,"parent_id":13,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-28","full_name":"Ch\\u1eaft F1","sex":"1","dob":null,"phone":"0354268970","identity_card":"154252814","email":"test8845@gmail.com","nation":"1","address":"H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1467098401,"updated_at":1467098401,"poundage_level":null}', 1467098401, 1467098401),
(58, 20, 'test123', 1, 'Đăng ký thành công tài khoản', '{"id":20,"username":"test123","ref_code":"5774e3ecaa81f","auth_key":"_KCTpKGsTEluk2K-Bi6PhvYrhGpI1cO7","password_hash":"$2y$13$I9EYJ46vM..4dJTVP41.QePrIkhGd2YQE1ngJxZbPwLBwkg8g9Oc6","password_reset":null,"package_id":null,"parent_id":6,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-30","full_name":"Test 123","sex":"1","dob":null,"phone":"0912345670","identity_card":"01234567","email":"test123@gmail.com","nation":"1","address":"Ha Noi","last_auto_increase":null,"created_at":1467278316,"updated_at":1467278316,"poundage_level":null}', 1467278316, 1467278316),
(59, 21, 'test10', 1, 'Đăng ký thành công tài khoản', '{"id":21,"username":"test10","ref_code":"57753c70c90e7","auth_key":"Mr-A8sWGM-3LS999fLtr28xA_UC135__","password_hash":"$2y$13$tUbqhCGMH9pP1diroAaBY..uufnjK7p\\/fsUYNrtjwNEQewTeA9AJC","password_reset":null,"package_id":null,"parent_id":1,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-06-30","full_name":"test10","sex":"1","dob":null,"phone":"0986552244","identity_card":"089855877788","email":"abc123@gmail.com","nation":"1","address":"H\\u00c0 N\\u1ed8I","last_auto_increase":null,"created_at":1467300976,"updated_at":1467300976,"poundage_level":null}', 1467300977, 1467300977),
(60, 22, 'test20', 1, 'Đăng ký thành công tài khoản', '{"id":22,"username":"test20","ref_code":"5775adacea3d1","auth_key":"aobDfsyyb4B-lwGwbRzpWpMArcD_M1r1","password_hash":"$2y$13$.33tnixopNrd2AiUhxYCquG\\/mD\\/eGR40qAU8rp9NqUSC\\/NtRUTNXi","password_reset":null,"package_id":null,"parent_id":21,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-01","full_name":"test20","sex":"1","dob":null,"phone":"0986445824","identity_card":"012569888","email":"123@gmail.com","nation":"1","address":"Ha noi","last_auto_increase":null,"created_at":1467329964,"updated_at":1467329964,"poundage_level":null}', 1467329965, 1467329965),
(61, 23, 'test101', 1, 'Đăng ký thành công tài khoản', '{"id":23,"username":"test101","ref_code":"5775ea7f681fe","auth_key":"u3YKq3NvH0TPICv3KN5YadPQigiG7Cjj","password_hash":"$2y$13$ijvhnTisXA3.YBorCVvdy.fSEvm\\/3\\/ybmcMHvmeJRcZekrmWR7vsW","password_reset":null,"package_id":null,"parent_id":1,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-01","full_name":"test101","sex":"1","dob":null,"phone":"0986554144","identity_card":"0125488588","email":"123443@gmail.com","nation":"1","address":"H\\u00e0 n\\u1ed9i","last_auto_increase":null,"created_at":1467345535,"updated_at":1467345535,"poundage_level":null}', 1467345535, 1467345535),
(62, 24, 'tet102', 1, 'Đăng ký thành công tài khoản', '{"id":24,"username":"tet102","ref_code":"577600cf1ede2","auth_key":"i875CNMTw0wSmQbBTyI1JyYm5rOJTwPd","password_hash":"$2y$13$WGjH8ZzLSMzDiCFgRtWVtOgZ1cbV1YbWQB23lqNo0qJ7ceJ45qgd2","password_reset":null,"package_id":null,"parent_id":23,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-01","full_name":"test102","sex":"1","dob":null,"phone":"0986114585","identity_card":"0215584885","email":"123456@gmail.com","nation":"1","address":"Ha noi","last_auto_increase":null,"created_at":1467351247,"updated_at":1467351247,"poundage_level":null}', 1467351247, 1467351247),
(63, 25, 'test102', 1, 'Đăng ký thành công tài khoản', '{"id":25,"username":"test102","ref_code":"5776017a37658","auth_key":"WiQnVRv02wyNslIdOrrztglAxMhXYV4i","password_hash":"$2y$13$1GQFkXndM1ujnU0kCtJDL.RqjTBN.48ifzYz3EIn\\/MZsCFt36Wtei","password_reset":null,"package_id":null,"parent_id":23,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-01","full_name":"test102","sex":"1","dob":null,"phone":"0986558553","identity_card":"0125198548","email":"vnhhh@gmail.com","nation":"1","address":"Ha noi","last_auto_increase":null,"created_at":1467351418,"updated_at":1467351418,"poundage_level":null}', 1467351418, 1467351418),
(64, 26, 'test1010', 1, 'Đăng ký thành công tài khoản', '{"id":26,"username":"test1010","ref_code":"57760a6189231","auth_key":"QIyzKwrh1gro0Ez88dm8ERW2rHeBxroX","password_hash":"$2y$13$6U2I5cOAxNLmV3pD4uRirOcciUMLuAKtw41qUpMJRcX61akvYyTUW","password_reset":null,"package_id":null,"parent_id":23,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-01","full_name":"test1010","sex":"1","dob":null,"phone":"0987558544","identity_card":"0125488955","email":"2335ee@gmail.com","nation":"1","address":"H\\u00e0 n\\u1ed9i","last_auto_increase":null,"created_at":1467353697,"updated_at":1467353697,"poundage_level":null}', 1467353697, 1467353697),
(65, 27, 'test1020', 1, 'Đăng ký thành công tài khoản', '{"id":27,"username":"test1020","ref_code":"57760b1ff3e88","auth_key":"5lKCfuupWbtaB2ceXAzncPMs0G277O3S","password_hash":"$2y$13$K1mW.YNNgjg5FvcZM43Py.J67cOVEWrkvTPWTtA7dQrv7higfvm3S","password_reset":null,"package_id":null,"parent_id":25,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-01","full_name":"test1020","sex":"1","dob":null,"phone":"0984458528","identity_card":"0125488778","email":"ddddd@gmail.com","nation":"1","address":"Ha noi","last_auto_increase":null,"created_at":1467353888,"updated_at":1467353888,"poundage_level":null}', 1467353888, 1467353888),
(66, 28, 'test101010', 1, 'Đăng ký thành công tài khoản', '{"id":28,"username":"test101010","ref_code":"57763fa4ee691","auth_key":"1E9x7JqIpm0hcpau4h4gsdHF6sPpUC0G","password_hash":"$2y$13$K2\\/e5B\\/FPgEvE6EadUwSk.nwoY7aSKo9a4h9XQxiM.2ylK2iyayy6","password_reset":null,"package_id":null,"parent_id":26,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-01","full_name":"test101010","sex":"1","dob":null,"phone":"0321654897","identity_card":"032156498","email":"test101010@gmail.com","nation":"1","address":"H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1467367333,"updated_at":1467367333,"poundage_level":null}', 1467367333, 1467367333),
(67, 29, 'test101011', 1, 'Đăng ký thành công tài khoản', '{"id":29,"username":"test101011","ref_code":"57764843eb8c1","auth_key":"GCM1vyQMpttSJlY575675xnZkI0SsPk4","password_hash":"$2y$13$0hTG1RBHgFKEmDpRQBeUDONKeFRucU\\/TbqGhn5pYdnp0B6Rhc5nGy","password_reset":null,"package_id":null,"parent_id":28,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-01","full_name":"test101011","sex":"1","dob":null,"phone":"0321645987","identity_card":"031254687","email":"test101011lk@gmail.com","nation":"1","address":"Ha Noi","last_auto_increase":null,"created_at":1467369540,"updated_at":1467369540,"poundage_level":null}', 1467369540, 1467369540),
(68, 30, 'test101012', 1, 'Đăng ký thành công tài khoản', '{"id":30,"username":"test101012","ref_code":"57764b48beaaf","auth_key":"BBmzwEw_0J4i4uUXYqbfCQzyxy5lzwIZ","password_hash":"$2y$13$jJ0n34gLFqe31O.HJHKHperHxRUSAXGerB8bLrZox7Y8het\\/iZNke","password_reset":null,"package_id":null,"parent_id":28,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-01","full_name":"test101012","sex":"1","dob":null,"phone":"0977704585","identity_card":"0102354687","email":"test101012@gmail.com","nation":"1","address":"\\u0110\\u1ed1ng \\u0110a, H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1467370312,"updated_at":1467370312,"poundage_level":null}', 1467370312, 1467370312),
(69, 1, 'test1', 10, 'customer đã đăng ký gói đầu tư Gói đầu tư số 4 trị giá 10.0000 bitcoin', '{"id":37,"customer_id":1,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 4","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":1,"reinvestment_date":1467372276,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467372276,"updated_at":1467372276,"balance":null}', 1467372276, 1467372276),
(70, 31, 'test10101', 1, 'Đăng ký thành công tài khoản', '{"id":31,"username":"test10101","ref_code":"5776971a051be","auth_key":"Ceh2CzEbkPl4KrAYPTzud3JbL91lMASj","password_hash":"$2y$13$UZtpxYQ.nLCkAcUVI2W2xuWtblAIYplFaj5l\\/m8kSh2IeYBMnQRh6","password_reset":null,"package_id":null,"parent_id":26,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-01","full_name":"test10101","sex":"1","dob":null,"phone":"0321456987","identity_card":"031256478","email":"test10101@gmail.com","nation":"1","address":"H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1467389722,"updated_at":1467389722,"poundage_level":null}', 1467389722, 1467389722),
(71, 32, 'capcon', 1, 'Đăng ký thành công tài khoản', '{"id":32,"username":"capcon","ref_code":"577782e9014ed","auth_key":"yDkzt7ocqftEV13y8CRrOn-hlHi9P-aW","password_hash":"$2y$13$QpeUAhNTO1JqSaaJRzrJNeZlcVRebv5cAAa.DienY5YZQHMhfCMT.","password_reset":null,"package_id":null,"parent_id":6,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-02","full_name":"capcon","sex":"1","dob":null,"phone":"0123452289","identity_card":"134242914","email":"capcontest1@gmail.com","nation":"1","address":"Ha noi","last_auto_increase":null,"created_at":1467450089,"updated_at":1467450089,"poundage_level":null}', 1467450089, 1467450089),
(72, 33, 'capcon02', 1, 'Đăng ký thành công tài khoản', '{"id":33,"username":"capcon02","ref_code":"57778634e503b","auth_key":"kWxWzhCRYFFnDDAxSRL5UVErRasmYEil","password_hash":"$2y$13$A3jrtKHyWDKig3D\\/je7aCumgf.sVi4kI8Odwj8Yb2xwkLVL\\/mnkl6","password_reset":null,"package_id":null,"parent_id":6,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-02","full_name":"capcon02","sex":"1","dob":null,"phone":"0931684448","identity_card":"164245553","email":"capcon02te@gmail.com","nation":"1","address":"H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1467450932,"updated_at":1467450932,"poundage_level":null}', 1467450932, 1467450932),
(73, 34, 'capcon01', 1, 'Đăng ký thành công tài khoản', '{"id":34,"username":"capcon01","ref_code":"57778766e969f","auth_key":"5A_q5L2wWSrk8RvU-ZEjdP9o4m13Lqc-","password_hash":"$2y$13$.nPIyyoY8GjwJWwMC8lWwuOPuonZqQUnhF8S5NY2tJU0xhV9AovAi","password_reset":null,"package_id":null,"parent_id":32,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-02","full_name":"capcon01","sex":"1","dob":null,"phone":"0354263370","identity_card":"12342285","email":"capcon01test@gmail.com","nation":"1","address":"H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1467451238,"updated_at":1467451238,"poundage_level":null}', 1467451239, 1467451239),
(74, 35, 'capcon011', 1, 'Đăng ký thành công tài khoản', '{"id":35,"username":"capcon011","ref_code":"5777881f4312c","auth_key":"jqw3HWXhle-F0hFE3wm9Tc3f81xXUKcc","password_hash":"$2y$13$DMi6QJO9MKfuI\\/BHKmra5OUZESCczHwJ3RQmkJYvteW8\\/fve9ywsS","password_reset":null,"package_id":null,"parent_id":32,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-02","full_name":"capcon011","sex":"1","dob":null,"phone":"0913456978","identity_card":"143612549","email":"capcon011@gmail.com","nation":"1","address":"Ha Noi","last_auto_increase":null,"created_at":1467451423,"updated_at":1467451423,"poundage_level":null}', 1467451423, 1467451423),
(75, 36, 'capcon021', 1, 'Đăng ký thành công tài khoản', '{"id":36,"username":"capcon021","ref_code":"57778f02b1e86","auth_key":"jaCNCxXYLZxTSYWpC4hQgZp_u-o--3CK","password_hash":"$2y$13$5x3C70R3brjsMbKv0qBJg.B2rpQUnT15bHj5mnbQ\\/QpOK88qTSu5u","password_reset":null,"package_id":null,"parent_id":33,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-02","full_name":"capcon021","sex":"1","dob":null,"phone":"0213654789","identity_card":"321654897","email":"capcon021test@gmail.com","nation":"1","address":"H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1467453186,"updated_at":1467453186,"poundage_level":null}', 1467453186, 1467453186),
(76, 37, 'capchaf1', 1, 'Đăng ký thành công tài khoản', '{"id":37,"username":"capchaf1","ref_code":"5778b29321a6e","auth_key":"1vjbZX2ra4BMIube-icBfnzhKfqu1nKQ","password_hash":"$2y$13$MsFuA48VluCLzUtwvpS8ZeFFEAP9tXvYdmMdPOUIOaoYySa26B0A2","password_reset":null,"package_id":null,"parent_id":6,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-03","full_name":"capchaf1","sex":"1","dob":null,"phone":"0124563798","identity_card":"121343568","email":"capchaf1@gmail.com","nation":"1","address":"H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1467527827,"updated_at":1467527827,"poundage_level":null}', 1467527827, 1467527827),
(77, 38, 'capchaf11', 1, 'Đăng ký thành công tài khoản', '{"id":38,"username":"capchaf11","ref_code":"5778b6bdc7a87","auth_key":"WWv1bUgVk1qzUhBIRxgbljpuHRuV6YbG","password_hash":"$2y$13$tFL54Hzmq4g6WGAPaW0oq.FA7.tBz37FrzdAQoCpHwJE1eS.3E1BW","password_reset":null,"package_id":null,"parent_id":6,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-03","full_name":"capchaf11","sex":"1","dob":null,"phone":"0931689277","identity_card":"164244988","email":"capchaf11@gmail.com","nation":"1","address":"Ha noi","last_auto_increase":null,"created_at":1467528893,"updated_at":1467528893,"poundage_level":null}', 1467528893, 1467528893);
INSERT INTO `customer_activity` (`id`, `customer_id`, `customer_username`, `type`, `message`, `params`, `created_at`, `updated_at`) VALUES
(78, 39, 'capchaf2', 1, 'Đăng ký thành công tài khoản', '{"id":39,"username":"capchaf2","ref_code":"5778b8524af17","auth_key":"TEvjql8jNyk7a36QtlwVwKwHCULox62_","password_hash":"$2y$13$Tq3td4WBM891jboCaD7Zd.\\/HNFSF\\/YUfEFnd.bn3au0fnPRmbQzXG","password_reset":null,"package_id":null,"parent_id":37,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-03","full_name":"capchaf2","sex":"1","dob":null,"phone":"0123456669","identity_card":"12346555","email":"capchaf2@gmail.com","nation":"1","address":"H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1467529298,"updated_at":1467529298,"poundage_level":null}', 1467529298, 1467529298),
(79, 40, 'capchaf22', 1, 'Đăng ký thành công tài khoản', '{"id":40,"username":"capchaf22","ref_code":"5778bbe558aac","auth_key":"Ufp5a2x2CaIJJTx3Av5dTGkYE1m_nXkb","password_hash":"$2y$13$hrmMLXq8DlodQ1pUUuDZNO4muvrn1SupXbRLMRTOq2u9R2TjsahVG","password_reset":null,"package_id":null,"parent_id":37,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-03","full_name":"capchaf22","sex":"1","dob":null,"phone":"03251647977","identity_card":"132456857","email":"capchaf22@gmail.com","nation":"1","address":"H\\u00e0 N\\u1ed9i","last_auto_increase":null,"created_at":1467530213,"updated_at":1467530213,"poundage_level":null}', 1467530213, 1467530213),
(80, 41, 'capchaf3', 1, 'Đăng ký thành công tài khoản', '{"id":41,"username":"capchaf3","ref_code":"5778cade8c889","auth_key":"y3pasMC7M9BfrEpFZxrVhwS_1lRp4ivW","password_hash":"$2y$13$gbmRp5tgjsQGNhXrmRvBW.8ncv0oslOvBTRdvtX2CtcoP\\/9ajXr9C","password_reset":null,"package_id":null,"parent_id":39,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-03","full_name":"capchaf3","sex":"1","dob":null,"phone":"0512346789","identity_card":"321564798","email":"capchaf3@gmail.com","nation":"1","address":"Hanoi","last_auto_increase":null,"created_at":1467534046,"updated_at":1467534046,"poundage_level":null}', 1467534046, 1467534046),
(81, 42, 'capchaf33', 1, 'Đăng ký thành công tài khoản', '{"id":42,"username":"capchaf33","ref_code":"5778cbb417097","auth_key":"6Ntlg-oX7cTs8OavvjqWqGlwynTbGZgj","password_hash":"$2y$13$YnNH8G0mSSYiLUL5R.LOaelnC2Jj1G\\/VVb66.hGSJg5HezoBLxfzy","password_reset":null,"package_id":null,"parent_id":39,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-03","full_name":"capchaf33","sex":"1","dob":null,"phone":"0987321456","identity_card":"867945132","email":"capchaf33@gmail.com","nation":"1","address":"Ha Noi","last_auto_increase":null,"created_at":1467534260,"updated_at":1467534260,"poundage_level":null}', 1467534260, 1467534260),
(82, 43, 'capchaf1101', 1, 'Đăng ký thành công tài khoản', '{"id":43,"username":"capchaf1101","ref_code":"5778d14e3032f","auth_key":"PilDPCqj_uvz1FPhejo6MKufmMttESAU","password_hash":"$2y$13$KeDkA8lNutZshBw85dpwQe30l.ymGuiTSQX8wOGoTEza2gCpEiyMO","password_reset":null,"package_id":null,"parent_id":38,"user_id_first_branch":null,"amount":null,"status":1,"is_invest":null,"is_active":0,"is_loan_admin":null,"is_deleted":null,"created_type":2,"created_by":null,"date_join":"2016-07-03","full_name":"capchaf1101","sex":"1","dob":null,"phone":"0925123648","identity_card":"582369741","email":"capchaf1101@gmail.com","nation":"2","address":"Hanoi","last_auto_increase":null,"created_at":1467535694,"updated_at":1467535694,"poundage_level":null}', 1467535694, 1467535694);

-- --------------------------------------------------------

--
-- Table structure for table `customer_bank_address`
--

CREATE TABLE IF NOT EXISTS `customer_bank_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `bank_address` varchar(255) NOT NULL,
  `status` smallint(2) unsigned NOT NULL DEFAULT '1',
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `CUSTOMER_BANK_ADDRESS` (`customer_id`,`bank_address`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `customer_bank_address`
--

INSERT INTO `customer_bank_address` (`id`, `customer_id`, `bank_address`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'm8eilp-9mo0ei-bvn80a', 1, 1466361728, 1466361728),
(2, 2, '3125afhdsloi', 1, 1466362753, 1466362753),
(3, 3, '894loiu546', 1, 1466362794, 1466362794),
(4, 4, 'bghy31y5ui36eww585', 1, 1466363443, 1466363443),
(5, 5, 'nb78j90-lkh02a36', 1, 1466364226, 1466364226),
(6, 6, '09oiul98gh-mnb7jk', 1, 1466367157, 1466367157),
(7, 9, 'mnalkj325', 1, 1466367440, 1466367440),
(8, 10, '36afgh6nh', 1, 1466367485, 1466367485),
(9, 11, 'mnjuy870poi83', 1, 1466367543, 1466367543),
(10, 8, '64afg4ve2', 1, 1466367594, 1466367594),
(11, 7, 'lkjai098oei', 1, 1466369087, 1466369087),
(12, 14, 'nhukhhfjhl2', 1, 1466504326, 1466504326),
(13, 16, '09oiuy8e93mhau7a', 1, 1467000230, 1467000230),
(14, 17, '1245e44e4e131r1r1re4r341rrr1ew43341w', 1, 1467000262, 1467000262),
(15, 12, 'gfda6587jhg90-09poiũb-01', 1, 1467089821, 1467089821);

-- --------------------------------------------------------

--
-- Table structure for table `customer_case`
--

CREATE TABLE IF NOT EXISTS `customer_case` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned NOT NULL,
  `customer_username` varchar(255) DEFAULT NULL,
  `level` smallint(2) DEFAULT '0',
  `status` smallint(2) unsigned DEFAULT '1',
  `created_at` int(11) DEFAULT '0',
  `updated_at` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `CUSTOMER_PARENT` (`customer_id`,`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=107 ;

--
-- Dumping data for table `customer_case`
--

INSERT INTO `customer_case` (`id`, `customer_id`, `parent_id`, `customer_username`, `level`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'test1', 0, 1, 1466360492, 1466360492),
(2, 2, 1, NULL, 1, 1, 1466360612, 1466360612),
(3, 3, 2, NULL, 1, 1, 1466360710, 1466360710),
(4, 3, 1, NULL, 2, 1, 1466360710, 1466360710),
(5, 4, 3, NULL, 1, 1, 1466360823, 1466360823),
(6, 4, 1, NULL, 3, 1, 1466360823, 1466360823),
(7, 4, 2, NULL, 2, 1, 1466360823, 1466360823),
(8, 5, 4, NULL, 1, 1, 1466360927, 1466360927),
(9, 5, 3, NULL, 2, 1, 1466360927, 1466360927),
(10, 5, 1, NULL, 4, 1, 1466360927, 1466360927),
(11, 5, 2, NULL, 3, 1, 1466360927, 1466360927),
(12, 6, 1, NULL, 1, 1, 1466361600, 1466361600),
(13, 7, 6, NULL, 1, 1, 1466361813, 1466361813),
(14, 7, 1, NULL, 2, 1, 1466361813, 1466361813),
(15, 8, 6, NULL, 1, 1, 1466361925, 1466361925),
(16, 8, 1, NULL, 2, 1, 1466361925, 1466361925),
(17, 9, 6, NULL, 1, 1, 1466361982, 1466361982),
(18, 9, 1, NULL, 2, 1, 1466361982, 1466361982),
(19, 10, 6, NULL, 1, 1, 1466362026, 1466362026),
(20, 10, 1, NULL, 2, 1, 1466362026, 1466362026),
(21, 11, 6, NULL, 1, 1, 1466362077, 1466362077),
(22, 11, 1, NULL, 2, 1, 1466362077, 1466362077),
(23, 12, 7, NULL, 1, 1, 1466494498, 1466494498),
(24, 12, 1, NULL, 3, 1, 1466494498, 1466494498),
(25, 12, 6, NULL, 2, 1, 1466494498, 1466494498),
(26, 13, 12, NULL, 1, 1, 1466498311, 1466498311),
(27, 13, 1, NULL, 4, 1, 1466498311, 1466498311),
(28, 13, 6, NULL, 3, 1, 1466498311, 1466498311),
(29, 13, 7, NULL, 2, 1, 1466498311, 1466498311),
(30, 14, 6, NULL, 1, 1, 1466501867, 1466501867),
(31, 14, 1, NULL, 2, 1, 1466501867, 1466501867),
(32, 15, 14, NULL, 1, 1, 1466504021, 1466504021),
(33, 15, 1, NULL, 3, 1, 1466504021, 1466504021),
(34, 15, 6, NULL, 2, 1, 1466504021, 1466504021),
(35, 16, 16, 'admin2', 0, 1, 1467000230, 1467000230),
(36, 17, 17, 'admin1', 0, 1, 1467000262, 1467000262),
(37, 18, 2, NULL, 1, 1, 1467098035, 1467098035),
(38, 18, 1, NULL, 2, 1, 1467098035, 1467098035),
(39, 19, 13, NULL, 1, 1, 1467098401, 1467098401),
(40, 19, 1, NULL, 5, 1, 1467098401, 1467098401),
(41, 19, 6, NULL, 4, 1, 1467098401, 1467098401),
(42, 19, 7, NULL, 3, 1, 1467098401, 1467098401),
(43, 19, 12, NULL, 2, 1, 1467098401, 1467098401),
(44, 20, 6, NULL, 1, 1, 1467278316, 1467278316),
(45, 20, 1, NULL, 2, 1, 1467278316, 1467278316),
(46, 21, 1, NULL, 1, 1, 1467300977, 1467300977),
(47, 22, 21, NULL, 1, 1, 1467329965, 1467329965),
(48, 22, 1, NULL, 2, 1, 1467329965, 1467329965),
(49, 23, 1, NULL, 1, 1, 1467345535, 1467345535),
(50, 24, 23, NULL, 1, 1, 1467351247, 1467351247),
(51, 24, 1, NULL, 2, 1, 1467351247, 1467351247),
(52, 25, 23, NULL, 1, 1, 1467351418, 1467351418),
(53, 25, 1, NULL, 2, 1, 1467351418, 1467351418),
(54, 26, 23, NULL, 1, 1, 1467353697, 1467353697),
(55, 26, 1, NULL, 2, 1, 1467353697, 1467353697),
(56, 27, 25, NULL, 1, 1, 1467353889, 1467353889),
(57, 27, 1, NULL, 3, 1, 1467353889, 1467353889),
(58, 27, 23, NULL, 2, 1, 1467353889, 1467353889),
(59, 28, 26, NULL, 1, 1, 1467367333, 1467367333),
(60, 28, 1, NULL, 3, 1, 1467367333, 1467367333),
(61, 28, 23, NULL, 2, 1, 1467367333, 1467367333),
(62, 29, 28, NULL, 1, 1, 1467369540, 1467369540),
(63, 29, 1, NULL, 4, 1, 1467369540, 1467369540),
(64, 29, 23, NULL, 3, 1, 1467369540, 1467369540),
(65, 29, 26, NULL, 2, 1, 1467369540, 1467369540),
(66, 30, 28, NULL, 1, 1, 1467370312, 1467370312),
(67, 30, 1, NULL, 4, 1, 1467370312, 1467370312),
(68, 30, 23, NULL, 3, 1, 1467370312, 1467370312),
(69, 30, 26, NULL, 2, 1, 1467370312, 1467370312),
(70, 31, 26, NULL, 1, 1, 1467389722, 1467389722),
(71, 31, 1, NULL, 3, 1, 1467389722, 1467389722),
(72, 31, 23, NULL, 2, 1, 1467389722, 1467389722),
(73, 32, 6, NULL, 1, 1, 1467450089, 1467450089),
(74, 32, 1, NULL, 2, 1, 1467450089, 1467450089),
(75, 33, 6, NULL, 1, 1, 1467450933, 1467450933),
(76, 33, 1, NULL, 2, 1, 1467450933, 1467450933),
(77, 34, 32, NULL, 1, 1, 1467451239, 1467451239),
(78, 34, 1, NULL, 3, 1, 1467451239, 1467451239),
(79, 34, 6, NULL, 2, 1, 1467451239, 1467451239),
(80, 35, 32, NULL, 1, 1, 1467451423, 1467451423),
(81, 35, 1, NULL, 3, 1, 1467451423, 1467451423),
(82, 35, 6, NULL, 2, 1, 1467451423, 1467451423),
(83, 36, 33, NULL, 1, 1, 1467453186, 1467453186),
(84, 36, 1, NULL, 3, 1, 1467453186, 1467453186),
(85, 36, 6, NULL, 2, 1, 1467453186, 1467453186),
(86, 37, 6, NULL, 1, 1, 1467527827, 1467527827),
(87, 37, 1, NULL, 2, 1, 1467527827, 1467527827),
(88, 38, 6, NULL, 1, 1, 1467528893, 1467528893),
(89, 38, 1, NULL, 2, 1, 1467528893, 1467528893),
(90, 39, 37, NULL, 1, 1, 1467529298, 1467529298),
(91, 39, 1, NULL, 3, 1, 1467529298, 1467529298),
(92, 39, 6, NULL, 2, 1, 1467529298, 1467529298),
(93, 40, 37, NULL, 1, 1, 1467530213, 1467530213),
(94, 40, 1, NULL, 3, 1, 1467530213, 1467530213),
(95, 40, 6, NULL, 2, 1, 1467530213, 1467530213),
(96, 41, 39, NULL, 1, 1, 1467534046, 1467534046),
(97, 41, 1, NULL, 4, 1, 1467534046, 1467534046),
(98, 41, 6, NULL, 3, 1, 1467534046, 1467534046),
(99, 41, 37, NULL, 2, 1, 1467534046, 1467534046),
(100, 42, 39, NULL, 1, 1, 1467534260, 1467534260),
(101, 42, 1, NULL, 4, 1, 1467534260, 1467534260),
(102, 42, 6, NULL, 3, 1, 1467534260, 1467534260),
(103, 42, 37, NULL, 2, 1, 1467534260, 1467534260),
(104, 43, 38, NULL, 1, 1, 1467535694, 1467535694),
(105, 43, 1, NULL, 3, 1, 1467535694, 1467535694),
(106, 43, 6, NULL, 2, 1, 1467535694, 1467535694);

-- --------------------------------------------------------

--
-- Table structure for table `customer_loan`
--

CREATE TABLE IF NOT EXISTS `customer_loan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) NOT NULL,
  `amount_loan` decimal(10,1) NOT NULL,
  `amount_loan_vnd` decimal(10,1) DEFAULT NULL,
  `status` smallint(2) DEFAULT '1',
  `type` smallint(2) DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_order`
--

CREATE TABLE IF NOT EXISTS `customer_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `customer_username` varchar(255) NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  `status` smallint(2) unsigned NOT NULL DEFAULT '0',
  `type` smallint(2) unsigned NOT NULL DEFAULT '1',
  `attachment` varchar(255) DEFAULT NULL,
  `upload_attachment_at` int(11) unsigned NOT NULL DEFAULT '0',
  `approved_by` int(11) unsigned DEFAULT NULL,
  `approved_at` int(11) unsigned NOT NULL DEFAULT '0',
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `customer_order`
--

INSERT INTO `customer_order` (`id`, `customer_id`, `customer_username`, `quantity`, `status`, `type`, `attachment`, `upload_attachment_at`, `approved_by`, `approved_at`, `created_at`, `updated_at`) VALUES
(1, 1, 'test1', 1, 10, 1, 'orders/1/8852c5615042714c781a22aa05ef520a.png', 1467000915, 1, 1467224376, 1467000798, 1467224376),
(2, 1, 'test1', 1, 10, 1, 'orders/1/964ec2bf4b10af8fc0946b2b8c08f3f7.jpg', 1467003002, 1, 1467003034, 1467002990, 1467003034),
(3, 1, 'test1', 1, 10, 1, 'orders/1/312020e1e6e102fc6a76b66b3ed677f4.jpg', 1467003388, 1, 1467003406, 1467003379, 1467003407),
(4, 7, 'CapF1', 10, 10, 1, 'orders/7/283022be83d290d4d9d78591bd82a1e6.jpg', 1467088352, 1, 1467088404, 1467088341, 1467088404),
(5, 6, 'CapCha', 1, 10, 1, 'orders/6/e0551bff1afdd77466b85f8d7e57d5a8.jpg', 1467090541, 1, 1467090569, 1467090534, 1467090569),
(6, 6, 'CapCha', 10, 10, 1, 'orders/6/361d2262ac6f9c2543eec82725fdb481.jpg', 1467221978, 1, 1467221993, 1467221971, 1467221993);

-- --------------------------------------------------------

--
-- Table structure for table `customer_package`
--

CREATE TABLE IF NOT EXISTS `customer_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `package_name` varchar(255) DEFAULT NULL,
  `package_amount` decimal(10,4) DEFAULT '0.0000',
  `increase_amount` decimal(10,4) DEFAULT '0.0000',
  `total` decimal(10,4) DEFAULT '0.0000',
  `is_reinvestment` smallint(2) DEFAULT '0',
  `reinvestment_date` int(11) DEFAULT NULL,
  `status` smallint(2) unsigned NOT NULL DEFAULT '1',
  `time_sent_completed` int(11) NOT NULL DEFAULT '0',
  `is_active` smallint(2) unsigned NOT NULL DEFAULT '10',
  `type` smallint(2) unsigned NOT NULL DEFAULT '0',
  `block_chain_wallet_id` varchar(255) DEFAULT NULL,
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0',
  `balance` decimal(10,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `customer_package`
--

INSERT INTO `customer_package` (`id`, `customer_id`, `package_id`, `package_name`, `package_amount`, `increase_amount`, `total`, `is_reinvestment`, `reinvestment_date`, `status`, `time_sent_completed`, `is_active`, `type`, `block_chain_wallet_id`, `created_at`, `updated_at`, `balance`) VALUES
(1, 1, 1, 'Gói đầu tư số 1', '0.5000', '20.0000', '0.0000', 0, 1466360557, 20, 1466360961, 10, 0, NULL, 1466360557, 1467000662, '0.0000'),
(2, 2, 2, 'Gói đầu tư số 1', '1.0000', '25.0000', '0.0000', 0, 1466360673, 20, 1466360961, 10, 0, NULL, 1466360673, 1466367038, '193.1500'),
(3, 3, 4, 'Gói đầu tư số 1', '5.0000', '45.0000', '0.0000', 0, 1466360775, 20, 1466360961, 10, 0, NULL, 1466360775, 1466368794, '291.1000'),
(4, 4, 5, 'Gói đầu tư số 1', '10.0000', '65.0000', '0.0000', 0, 1466360882, 20, 1466360961, 10, 0, NULL, 1466360882, 1466365755, '390.8500'),
(5, 5, 5, 'Gói đầu tư số 1', '10.0000', '65.0000', '0.0000', 0, 1466360961, 20, 1466360961, 10, 0, NULL, 1466360961, 1466368532, '477.5500'),
(6, 6, 0, 'Gói đầu tư số 1', '0.0000', '0.0000', '0.0000', 1, 1466384608, 20, 1467002903, 10, 0, NULL, 1466362111, 1467222742, '0.0000'),
(7, 7, 0, 'Gói đầu tư số 1', '0.0000', '0.0000', '0.0000', 0, 1466362248, 20, 1466365618, 10, 0, NULL, 1466362248, 1466369103, '0.0000'),
(8, 8, 0, 'Gói đầu tư số 1', '0.0000', '0.0000', '0.0000', 0, 1466362321, 20, 1466365966, 10, 0, NULL, 1466362321, 1466368557, '0.0000'),
(9, 10, 0, 'Gói đầu tư số 1', '0.0000', '0.0000', '0.0000', 0, 1466362449, 20, 1466365583, 10, 0, NULL, 1466362449, 1466369317, '0.0000'),
(10, 9, 0, 'Gói đầu tư số 1', '0.0000', '0.0000', '0.0000', 0, 1466362664, 20, 1466365377, 10, 0, NULL, 1466362664, 1466369168, '0.0000'),
(11, 11, 0, 'Gói đầu tư số 1', '0.0000', '0.0000', '0.0000', 0, 1466362680, 20, 1466366851, 10, 0, NULL, 1466362680, 1466367553, '0.0000'),
(12, 6, 0, 'Gói đầu tư số 2', '0.0000', '0.0000', '0.0000', 0, 1466363510, 20, 1466365998, 10, 0, NULL, 1466363510, 1466369261, '0.0000'),
(13, 10, 0, 'Gói đầu tư số 2', '0.0000', '0.0000', '0.0000', 0, 1466363539, 20, 1466366176, 10, 0, NULL, 1466363539, 1466369338, '0.0000'),
(14, 11, 0, 'Gói đầu tư số 2', '0.0000', '0.0000', '0.0000', 0, 1466363556, 20, 1466366008, 10, 0, NULL, 1466363556, 1466368662, '0.0000'),
(15, 7, 0, 'Gói đầu tư số 2', '0.0000', '0.0000', '0.0000', 0, 1466366036, 20, 1466368242, 10, 0, NULL, 1466366036, 1466369117, '0.0000'),
(16, 8, 0, 'Gói đầu tư số 2', '0.0000', '0.0000', '0.0000', 0, 1466366082, 20, 1466366772, 10, 0, NULL, 1466366082, 1466368567, '0.0000'),
(17, 9, 0, 'Gói đầu tư số 2', '0.0000', '0.0000', '0.0000', 0, 1466366096, 20, 1466367402, 10, 0, NULL, 1466366096, 1466369184, '0.0000'),
(18, 1, 0, 'Gói đầu tư số 2', '0.0000', '0.0000', '0.0000', 0, 1466367652, 20, 1466369010, 10, 0, NULL, 1466367652, 1466498413, '0.0000'),
(19, 4, 0, 'Gói đầu tư số 2', '0.0000', '0.0000', '0.0000', 0, 1466367677, 20, 1467086378, 10, 0, NULL, 1466367677, 1467086702, '7.2500'),
(20, 2, 0, 'Gói đầu tư số 2', '0.0000', '0.0000', '0.0000', 0, 1466367710, 20, 1467087555, 10, 0, NULL, 1466367710, 1467087904, '2.6000'),
(21, 3, 0, 'Gói đầu tư số 2', '0.0000', '0.0000', '0.0000', 0, 1466367725, 20, 1466369037, 10, 0, NULL, 1466367725, 1466369402, '7.2500'),
(22, 5, 0, 'Gói đầu tư số 2', '0.0000', '0.0000', '0.0000', 1, 1466367740, 20, 1467029108, 10, 0, NULL, 1466367740, 1467029701, '7.2500'),
(23, 5, 0, 'Gói đầu tư số 3', '0.0000', '0.0000', '0.0000', 0, 1466368704, 20, 1466369610, 10, 0, NULL, 1466368704, 1467029380, '0.0000'),
(24, 2, 0, 'Gói đầu tư số 3', '0.0000', '0.0000', '0.0000', 1, 1466369367, 20, 1467021975, 10, 0, NULL, 1466369367, 1467022501, '16.5000'),
(25, 12, 0, 'Gói đầu tư số 1', '0.0000', '0.0000', '0.0000', 1, 1466494765, 20, 1466498193, 10, 0, NULL, 1466494765, 1466498701, '16.5000'),
(26, 13, 0, 'Gói đầu tư số 1', '0.0000', '0.0000', '0.0000', 0, 1466498360, 20, 1467090403, 10, 0, NULL, 1466498360, 1467090901, '16.5000'),
(27, 14, 0, 'Gói đầu tư số 1', '0.0000', '0.0000', '0.0000', 1, 1466502367, 20, 1466503429, 10, 0, NULL, 1466502367, 1466503803, '0.6000'),
(28, 14, 5, 'Gói đầu tư số 2', '10.0000', '65.0000', '0.0000', 0, 1466503503, 2, 1467223861, 10, 0, NULL, 1466503503, 1467223861, '0.0000'),
(29, 1, 0, 'Gói đầu tư số 3', '0.0000', '0.0000', '0.0000', 1, 1466930949, 20, 1467002203, 10, 0, NULL, 1466930949, 1467003228, '0.0000'),
(30, 7, 0, 'Gói đầu tư số 3', '0.0000', '0.0000', '0.0000', 0, 1467087132, 20, 1467087452, 10, 0, NULL, 1467087132, 1467088692, '0.0000'),
(31, 9, 0, 'Gói đầu tư số 3', '0.0000', '0.0000', '0.0000', 0, 1467088062, 20, 1467090496, 10, 0, NULL, 1467088062, 1467223045, '0.0000'),
(32, 12, 0, 'Gói đầu tư số 2', '0.0000', '0.0000', '0.0000', 0, 1467089045, 20, 1467089664, 10, 0, NULL, 1467089045, 1467090314, '0.0000'),
(33, 7, 0, 'Gói đầu tư số 4', '0.0000', '0.0000', '0.0000', 0, 1467089188, 20, 1467089417, 10, 0, NULL, 1467089188, 1467480599, '0.0000'),
(34, 6, 0, 'Gói đầu tư số 3', '0.0000', '0.0000', '0.0000', 0, 1467090625, 20, 1467091629, 10, 0, NULL, 1467090625, 1467222327, '0.0000'),
(35, 12, 0, 'Gói đầu tư số 3', '0.0000', '0.0000', '0.0000', 0, 1467090737, 20, 1467092627, 10, 0, NULL, 1467090737, 1467093002, '16.5000'),
(36, 13, 0, 'Gói đầu tư số 2', '0.0000', '0.0000', '0.0000', 0, 1467092663, 20, 1467092787, 10, 0, NULL, 1467092663, 1467093301, '16.5000'),
(37, 1, 5, 'Gói đầu tư số 4', '10.0000', '65.0000', '0.0000', 1, 1467372276, 2, 1467372400, 10, 0, NULL, 1467372276, 1467372400, '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `customer_poundage_transaction`
--

CREATE TABLE IF NOT EXISTS `customer_poundage_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_id` int(11) DEFAULT '0',
  `sign` smallint(6) NOT NULL DEFAULT '1',
  `customer_id` int(11) unsigned NOT NULL,
  `customer_username` varchar(255) NOT NULL,
  `from_id` int(11) DEFAULT '0',
  `from_username` varchar(255) DEFAULT NULL,
  `amount` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000',
  `balance_before` decimal(10,4) DEFAULT '0.0000',
  `balance_after` decimal(10,4) DEFAULT '0.0000',
  `level` smallint(4) unsigned NOT NULL DEFAULT '0',
  `type` smallint(2) unsigned DEFAULT '0',
  `status` smallint(2) NOT NULL DEFAULT '1',
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `customer_poundage_transaction`
--

INSERT INTO `customer_poundage_transaction` (`id`, `bill_id`, `sign`, `customer_id`, `customer_username`, `from_id`, `from_username`, `amount`, `balance_before`, `balance_after`, `level`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 1, 1, 'test1', 9, 'CapF3', '0.0150', '0.0000', '0.0150', 2, 1, 1, 1466365377, 1466365377),
(2, 0, 1, 6, 'CapCha', 9, 'CapF3', '0.0500', '0.0000', '0.0500', 1, 1, 1, 1466365377, 1466365377),
(3, 0, 1, 1, 'test1', 10, 'CapF4', '0.0150', '0.0150', '0.0300', 2, 1, 1, 1466365583, 1466365583),
(4, 0, 1, 6, 'CapCha', 10, 'CapF4', '0.0500', '0.0500', '0.1000', 1, 1, 1, 1466365583, 1466365583),
(5, 0, 1, 1, 'test1', 7, 'CapF1', '0.0150', '0.0300', '0.0450', 2, 1, 1, 1466365618, 1466365618),
(6, 0, 1, 6, 'CapCha', 7, 'CapF1', '0.0500', '0.1000', '0.1500', 1, 1, 1, 1466365618, 1466365618),
(7, 0, 1, 1, 'test1', 6, 'CapCha', '0.0500', '0.0450', '0.0950', 1, 1, 1, 1466365837, 1466365837),
(8, 0, 1, 1, 'test1', 8, 'CapF2', '0.0150', '0.0950', '0.1100', 2, 1, 1, 1466365966, 1466365966),
(9, 0, 1, 6, 'CapCha', 8, 'CapF2', '0.0500', '0.1500', '0.2000', 1, 1, 1, 1466365966, 1466365966),
(10, 0, 1, 1, 'test1', 6, 'CapCha', '1.0000', '0.1100', '1.1100', 1, 1, 1, 1466365998, 1466365998),
(11, 0, 1, 1, 'test1', 11, 'CapF5', '0.0600', '1.1100', '1.1700', 2, 1, 1, 1466366008, 1466366008),
(12, 0, 1, 6, 'CapCha', 11, 'CapF5', '0.2000', '0.2000', '0.4000', 1, 1, 1, 1466366008, 1466366008),
(13, 0, 1, 1, 'test1', 10, 'CapF4', '0.1500', '1.1700', '1.3200', 2, 1, 1, 1466366176, 1466366176),
(14, 0, 1, 6, 'CapCha', 10, 'CapF4', '0.5000', '0.4000', '0.9000', 1, 1, 1, 1466366176, 1466366176),
(15, 0, 1, 1, 'test1', 8, 'CapF2', '0.1500', '1.3200', '1.4700', 2, 1, 1, 1466366772, 1466366772),
(16, 0, 1, 6, 'CapCha', 8, 'CapF2', '0.5000', '0.9000', '1.4000', 1, 1, 1, 1466366772, 1466366772),
(17, 0, 1, 1, 'test1', 11, 'CapF5', '0.0300', '1.4700', '1.5000', 2, 1, 1, 1466366851, 1466366851),
(18, 0, 1, 6, 'CapCha', 11, 'CapF5', '0.1000', '1.4000', '1.5000', 1, 1, 1, 1466366851, 1466366851),
(19, 0, 1, 1, 'test1', 9, 'CapF3', '0.0600', '1.5000', '1.5600', 2, 1, 1, 1466367402, 1466367402),
(20, 0, 1, 6, 'CapCha', 9, 'CapF3', '0.2000', '1.5000', '1.7000', 1, 1, 1, 1466367402, 1466367402),
(21, 0, 1, 1, 'test1', 7, 'CapF1', '0.0300', '1.5600', '1.5900', 2, 1, 1, 1466368242, 1466368242),
(22, 0, 1, 6, 'CapCha', 7, 'CapF1', '0.1000', '1.7000', '1.8000', 1, 1, 1, 1466368242, 1466368242),
(23, 0, 1, 1, 'test1', 3, 'test3', '0.1500', '1.5900', '1.7400', 2, 1, 1, 1466369037, 1466369037),
(24, 0, 1, 2, 'test2', 3, 'test3', '0.5000', '0.0000', '0.5000', 1, 1, 1, 1466369037, 1466369037),
(25, 0, 1, 1, 'test1', 5, 'test5', '0.1000', '1.7400', '1.8400', 4, 1, 1, 1466369610, 1466369610),
(26, 0, 1, 2, 'test2', 5, 'test5', '0.2000', '0.5000', '0.7000', 3, 1, 1, 1466369610, 1466369610),
(27, 0, 1, 3, 'test3', 5, 'test5', '0.3000', '0.0000', '0.3000', 2, 1, 1, 1466369610, 1466369610),
(28, 0, 1, 4, 'test4', 5, 'test5', '1.0000', '0.0000', '1.0000', 1, 1, 1, 1466369610, 1466369610),
(29, 0, 1, 1, 'test1', 12, 'conf1', '0.2000', '1.8400', '2.0400', 3, 2, 1, 1466498193, 1466498193),
(30, 0, 1, 6, 'CapCha', 12, 'conf1', '0.3000', '1.8000', '2.1000', 2, 2, 1, 1466498193, 1466498193),
(31, 0, 1, 7, 'CapF1', 12, 'conf1', '1.5000', '0.0000', '1.5000', 1, 2, 1, 1466498193, 1466498193),
(32, 77, 2, 6, 'CapCha', 0, 'system', '1.5000', '2.1000', '0.6000', 0, 10, 1, 1466503216, 1466503216),
(33, 0, 1, 1, 'test1', 14, 'testvn', '0.0150', '2.0400', '2.0550', 2, 2, 1, 1466503429, 1466503429),
(34, 0, 1, 6, 'CapCha', 14, 'testvn', '0.0750', '2.1000', '2.1750', 1, 2, 1, 1466503429, 1466503429),
(35, 82, 2, 1, 'test1', 0, 'system', '0.5000', '2.0550', '1.5550', 0, 10, 1, 1467000639, 1467000639),
(36, 0, 1, 1, 'test1', 6, 'CapCha', '1.5000', '2.0550', '3.5550', 1, 2, 1, 1467002903, 1467002903),
(37, 86, 2, 1, 'test1', 0, 'system', '0.5000', '3.0550', '2.5550', 0, 10, 1, 1467003464, 1467003464),
(38, 0, 1, 1, 'test1', 2, 'test2', '1.5000', '3.5550', '5.0550', 1, 2, 1, 1467021975, 1467021975),
(39, 0, 1, 1, 'test1', 5, 'test5', '0.0500', '5.0550', '5.1050', 4, 2, 1, 1467029108, 1467029108),
(40, 0, 1, 2, 'test2', 5, 'test5', '0.1000', '0.7000', '0.8000', 3, 2, 1, 1467029108, 1467029108),
(41, 0, 1, 3, 'test3', 5, 'test5', '0.1500', '0.3000', '0.4500', 2, 2, 1, 1467029108, 1467029108),
(42, 0, 1, 4, 'test4', 5, 'test5', '0.7500', '1.0000', '1.7500', 1, 2, 1, 1467029108, 1467029108),
(43, 92, 2, 6, 'CapCha', 0, 'system', '0.5000', '0.6750', '0.1750', 0, 10, 1, 1467029941, 1467029941),
(44, 0, 1, 1, 'test1', 4, 'test4', '0.1000', '5.1050', '5.2050', 3, 1, 1, 1467086378, 1467086378),
(45, 0, 1, 2, 'test2', 4, 'test4', '0.1500', '0.8000', '0.9500', 2, 1, 1, 1467086378, 1467086378),
(46, 0, 1, 3, 'test3', 4, 'test4', '0.5000', '0.4500', '0.9500', 1, 1, 1, 1467086378, 1467086378),
(47, 0, 1, 1, 'test1', 7, 'CapF1', '0.3000', '5.2050', '5.5050', 2, 1, 1, 1467087452, 1467087452),
(48, 0, 1, 6, 'CapCha', 7, 'CapF1', '1.0000', '2.1750', '3.1750', 1, 1, 1, 1467087452, 1467087452),
(49, 0, 1, 1, 'test1', 2, 'test2', '0.2000', '5.5050', '5.7050', 1, 1, 1, 1467087555, 1467087555),
(50, 105, 2, 7, 'CapF1', 0, 'system', '1.5000', '1.5000', '0.0000', 0, 10, 1, 1467088180, 1467088180),
(51, 107, 2, 2, 'test2', 0, 'system', '0.9500', '0.9500', '0.0000', 0, 10, 1, 1467088475, 1467088475),
(52, 112, 2, 6, 'CapCha', 0, 'system', '1.1800', '1.1750', '-0.0050', 0, 10, 1, 1467088884, 1467088884),
(53, 0, 1, 1, 'test1', 7, 'CapF1', '0.3000', '5.7050', '6.0050', 2, 1, 1, 1467089417, 1467089417),
(54, 0, 1, 6, 'CapCha', 7, 'CapF1', '1.0000', '3.1750', '4.1750', 1, 1, 1, 1467089417, 1467089417),
(55, 0, 1, 1, 'test1', 12, 'conf1', '0.2000', '6.0050', '6.2050', 3, 1, 1, 1467089664, 1467089664),
(56, 0, 1, 6, 'CapCha', 12, 'conf1', '0.3000', '4.1750', '4.4750', 2, 1, 1, 1467089664, 1467089664),
(57, 0, 1, 7, 'CapF1', 12, 'conf1', '1.0000', '1.5000', '2.5000', 1, 1, 1, 1467089664, 1467089664),
(58, 0, 1, 1, 'test1', 13, 'chauf1', '0.1000', '6.2050', '6.3050', 4, 1, 1, 1467090403, 1467090403),
(59, 0, 1, 6, 'CapCha', 13, 'chauf1', '0.2000', '4.4750', '4.6750', 3, 1, 1, 1467090404, 1467090404),
(60, 0, 1, 7, 'CapF1', 13, 'chauf1', '0.3000', '2.5000', '2.8000', 2, 1, 1, 1467090404, 1467090404),
(61, 0, 1, 12, 'conf1', 13, 'chauf1', '1.0000', '0.0000', '1.0000', 1, 1, 1, 1467090404, 1467090404),
(62, 0, 1, 1, 'test1', 9, 'CapF3', '0.3000', '6.3050', '6.6050', 2, 1, 1, 1467090496, 1467090496),
(63, 0, 1, 6, 'CapCha', 9, 'CapF3', '1.0000', '4.6750', '5.6750', 1, 1, 1, 1467090496, 1467090496),
(64, 0, 1, 1, 'test1', 6, 'CapCha', '1.0000', '6.6050', '7.6050', 1, 1, 1, 1467091629, 1467091629),
(65, 0, 1, 1, 'test1', 12, 'conf1', '0.2000', '7.6050', '7.8050', 3, 1, 1, 1467092627, 1467092627),
(66, 0, 1, 6, 'CapCha', 12, 'conf1', '0.3000', '5.6750', '5.9750', 2, 1, 1, 1467092627, 1467092627),
(67, 0, 1, 7, 'CapF1', 12, 'conf1', '1.0000', '2.8000', '3.8000', 1, 1, 1, 1467092627, 1467092627),
(68, 0, 1, 1, 'test1', 13, 'chauf1', '0.1000', '7.8050', '7.9050', 4, 1, 1, 1467092787, 1467092787),
(69, 0, 1, 6, 'CapCha', 13, 'chauf1', '0.2000', '5.9750', '6.1750', 3, 1, 1, 1467092787, 1467092787),
(70, 0, 1, 7, 'CapF1', 13, 'chauf1', '0.3000', '3.8000', '4.1000', 2, 1, 1, 1467092787, 1467092787),
(71, 0, 1, 12, 'conf1', 13, 'chauf1', '1.0000', '1.0000', '2.0000', 1, 1, 1, 1467092787, 1467092787),
(72, 134, 2, 6, 'CapCha', 0, 'system', '1.5000', '3.0000', '1.5000', 0, 10, 1, 1467221708, 1467221708),
(73, 143, 2, 6, 'CapCha', 0, 'system', '1.5000', '1.5000', '0.0000', 0, 10, 1, 1467223326, 1467223326),
(74, 155, 2, 1, 'test1', 0, 'system', '1.5000', '6.9050', '5.4050', 0, 10, 1, 1467224201, 1467224201),
(75, 161, 2, 7, 'CapF1', 0, 'system', '1.5000', '2.6000', '1.1000', 0, 10, 10, 1467480695, 1467480695),
(76, 162, 2, 7, 'CapF1', 0, 'system', '1.1000', '1.1000', '0.0000', 0, 10, 10, 1467480723, 1467480723);

-- --------------------------------------------------------

--
-- Table structure for table `customer_request`
--

CREATE TABLE IF NOT EXISTS `customer_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `block_chain_wallet_id` varchar(255) DEFAULT NULL,
  `type` smallint(2) unsigned NOT NULL,
  `status` smallint(2) unsigned NOT NULL,
  `amount` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `created_at` int(11) NOT NULL DEFAULT '0',
  `package_id` int(11) DEFAULT NULL,
  `updated_at` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=165 ;

--
-- Dumping data for table `customer_request`
--

INSERT INTO `customer_request` (`id`, `customer_id`, `block_chain_wallet_id`, `type`, `status`, `amount`, `created_at`, `package_id`, `updated_at`) VALUES
(1, 6, NULL, 10, 99, '0.5000', 1466362111, 6, 1466365837),
(2, 7, NULL, 10, 99, '0.5000', 1466362248, 7, 1466365618),
(3, 8, NULL, 10, 99, '0.5000', 1466362321, 8, 1466365966),
(4, 10, NULL, 10, 99, '0.5000', 1466362449, 9, 1466365583),
(5, 1, 'm8eilp-9mo0ei-bvn80a', 20, 99, '0.2000', 1466362637, 1, 1466363095),
(6, 9, NULL, 10, 99, '0.5000', 1466362664, 10, 1466365377),
(7, 11, NULL, 10, 99, '1.0000', 1466362680, 11, 1466366851),
(8, 3, '894loiu546', 20, 99, '1.6500', 1466362875, 3, 1466364069),
(9, 2, '3125afhdsloi', 20, 99, '1.0000', 1466363143, 2, 1466363918),
(10, 4, 'bghy31y5ui36eww585', 20, 99, '0.3000', 1466363459, 4, 1466364289),
(11, 6, NULL, 10, 99, '10.0000', 1466363510, 12, 1466365998),
(12, 10, NULL, 10, 99, '5.0000', 1466363539, 13, 1466366176),
(13, 11, NULL, 10, 99, '2.0000', 1466363556, 14, 1466366008),
(14, 1, 'm8eilp-9mo0ei-bvn80a', 20, 99, '0.4000', 1466364122, 1, 1466364894),
(15, 5, 'nb78j90-lkh02a36', 20, 99, '2.6500', 1466364244, 5, 1466364352),
(16, 5, 'nb78j90-lkh02a36', 20, 99, '5.0000', 1466364379, 5, 1466368616),
(17, 4, 'bghy31y5ui36eww585', 20, 99, '0.3000', 1466364530, 4, 1466365307),
(18, 2, '3125afhdsloi', 20, 99, '0.4000', 1466364560, 2, 1466364861),
(19, 3, '894loiu546', 20, 99, '0.5000', 1466364594, 3, 1466366239),
(20, 5, 'nb78j90-lkh02a36', 20, 99, '7.2000', 1466364630, 5, 1466369660),
(21, 1, 'm8eilp-9mo0ei-bvn80a', 20, 99, '0.2000', 1466364710, 1, 1466365377),
(22, 3, '894loiu546', 20, 99, '1.2500', 1466364773, 3, 1466366306),
(23, 5, 'nb78j90-lkh02a36', 20, 99, '0.3000', 1466364820, 5, 1466365114),
(24, 5, 'nb78j90-lkh02a36', 20, 99, '0.1000', 1466365136, 5, 1466365583),
(25, 3, '894loiu546', 20, 99, '0.3000', 1466365439, 3, 1466366344),
(26, 5, 'nb78j90-lkh02a36', 20, 99, '0.1000', 1466365459, 5, 1466365618),
(27, 4, 'bghy31y5ui36eww585', 20, 99, '1.0000', 1466365506, 4, 1466366008),
(28, 4, 'bghy31y5ui36eww585', 20, 99, '7.3500', 1466365687, 4, 1466365998),
(29, 3, '894loiu546', 20, 99, '0.2000', 1466365725, 3, 1466365837),
(30, 4, 'bghy31y5ui36eww585', 20, 99, '0.2000', 1466365755, 4, 1466365966),
(31, 7, NULL, 10, 99, '1.0000', 1466366036, 15, 1466368242),
(32, 5, 'nb78j90-lkh02a36', 20, 99, '3.3500', 1466366061, 5, 1466366176),
(33, 8, NULL, 10, 99, '5.0000', 1466366082, 16, 1466366772),
(34, 9, NULL, 10, 99, '2.0000', 1466366096, 17, 1466367402),
(35, 2, '3125afhdsloi', 20, 99, '0.2000', 1466366387, 2, 1466502799),
(36, 2, '3125afhdsloi', 20, 99, '3.7500', 1466366471, 2, 1466366772),
(37, 1, 'm8eilp-9mo0ei-bvn80a', 20, 99, '0.8000', 1466366544, 1, 1466366851),
(38, 2, '3125afhdsloi', 20, 99, '1.5000', 1466367038, 2, 1466367402),
(39, 6, '09oiul98gh-mnb7jk', 20, 99, '0.6000', 1466367317, 6, 1466368401),
(40, 9, 'mnalkj325', 20, 99, '0.4000', 1466367453, 10, 1467085216),
(41, 10, '36afgh6nh', 20, 99, '1.2500', 1466367512, 13, 1466368203),
(42, 11, 'mnjuy870poi83', 20, 99, '1.2500', 1466367553, 11, 1466368356),
(43, 8, '64afg4ve2', 20, 99, '5.0000', 1466367606, 16, 1466368887),
(44, 1, NULL, 10, 99, '10.0000', 1466367652, 18, 1466369010),
(45, 4, NULL, 10, 99, '5.0000', 1466367677, 19, 1467086377),
(46, 2, NULL, 10, 99, '2.0000', 1466367710, 20, 1467087555),
(47, 3, NULL, 10, 99, '5.0000', 1466367725, 21, 1466369037),
(48, 5, NULL, 10, 99, '5.0000', 1466367740, 22, 1467029108),
(49, 11, 'mnjuy870poi83', 20, 99, '0.7000', 1466367835, 14, 1466368242),
(50, 5, 'nb78j90-lkh02a36', 20, 99, '3.7500', 1466368532, 5, 1466369037),
(51, 8, '64afg4ve2', 20, 99, '0.6000', 1466368557, 8, 1467085171),
(52, 8, '64afg4ve2', 20, 99, '2.2500', 1466368567, 16, 1466368939),
(53, 11, 'mnjuy870poi83', 20, 99, '1.9000', 1466368662, 14, 1467087766),
(54, 5, NULL, 10, 99, '10.0000', 1466368704, 23, 1466369610),
(55, 3, '894loiu546', 20, 99, '5.0000', 1466368794, 3, 1466369010),
(56, 7, 'lkjai098oei', 20, 99, '0.6000', 1466369103, 7, 1467087227),
(57, 7, 'lkjai098oei', 20, 99, '1.2500', 1466369117, 15, 1467222748),
(58, 9, 'mnalkj325', 20, 99, '0.2000', 1466369168, 10, 1467222992),
(59, 9, 'mnalkj325', 20, 99, '2.6000', 1466369184, 17, 1466384997),
(60, 6, '09oiul98gh-mnb7jk', 20, 99, '6.5000', 1466369230, 12, 1466495247),
(61, 6, '09oiul98gh-mnb7jk', 20, 99, '5.0000', 1466369241, 12, 1466369610),
(62, 6, '09oiul98gh-mnb7jk', 20, 99, '5.0000', 1466369261, 12, 1467090201),
(63, 10, '36afgh6nh', 20, 99, '0.6000', 1466369317, 9, 1467223990),
(64, 10, '36afgh6nh', 20, 99, '3.0000', 1466369332, 13, 1467223861),
(65, 10, '36afgh6nh', 20, 99, '3.0000', 1466369338, 13, 1467223866),
(66, 2, NULL, 10, 99, '10.0000', 1466369367, 24, 1467021975),
(67, 6, NULL, 10, 99, '10.0000', 1466384608, 6, 1467002903),
(68, 12, NULL, 10, 99, '10.0000', 1466494765, 25, 1466498193),
(69, 1, 'm8eilp-9mo0ei-bvn80a', 20, 99, '5.5000', 1466494867, 18, 1467224132),
(70, 1, 'm8eilp-9mo0ei-bvn80a', 20, 99, '4.5000', 1466495913, 18, 1467224138),
(71, 1, 'm8eilp-9mo0ei-bvn80a', 20, 99, '3.5000', 1466497212, 18, 1466498193),
(72, 13, NULL, 10, 99, '10.0000', 1466498360, 26, 1467090403),
(73, 1, 'm8eilp-9mo0ei-bvn80a', 20, 99, '3.0000', 1466498413, 18, 1467224142),
(74, 14, NULL, 10, 99, '0.5000', 1466502367, 27, 1466503429),
(75, 1, 'm8eilp-9mo0ei-bvn80a', 20, 99, '0.2000', 1466502479, 1, 1467224147),
(76, 1, 'm8eilp-9mo0ei-bvn80a', 20, 99, '0.3000', 1466502853, 1, 1466503429),
(77, 6, '09oiul98gh-mnb7jk', 30, 99, '1.5000', 1466503216, 0, 1467000878),
(78, 14, NULL, 10, 10, '10.0000', 1466503503, 28, 1467223861),
(79, 1, 'm8eilp-9mo0ei-bvn80a', 20, 99, '4.0000', 1466503760, 1, 1467224153),
(80, 1, NULL, 10, 99, '1.0000', 1466930949, 29, 1467002203),
(81, 16, '09oiuy8e93mhau7a', 40, 1, '7.4000', 1467000288, 0, 1467000288),
(82, 1, 'm8eilp-9mo0ei-bvn80a', 30, 99, '0.5000', 1467000639, 0, 1467224158),
(83, 1, 'm8eilp-9mo0ei-bvn80a', 20, 99, '93.9000', 1467000662, 1, 1467218165),
(84, 16, '09oiuy8e93mhau7a', 40, 1, '0.5000', 1467001202, 0, 1467001202),
(85, 1, 'm8eilp-9mo0ei-bvn80a', 20, 99, '1.2500', 1467003228, 29, 1467029279),
(86, 1, 'm8eilp-9mo0ei-bvn80a', 30, 99, '0.5000', 1467003464, 0, 1467003854),
(87, 16, '', 50, 1, '0.5000', 1467003530, 0, 1467003530),
(88, 16, '09oiuy8e93mhau7a', 40, 1, '2.8000', 1467021843, 0, 1467021843),
(89, 16, '09oiuy8e93mhau7a', 40, 1, '3.7500', 1467028979, 0, 1467028979),
(90, 16, '', 50, 1, '1.2500', 1467029191, 0, 1467029191),
(91, 5, 'nb78j90-lkh02a36', 20, 99, '16.5000', 1467029380, 23, 1467089706),
(92, 6, '09oiul98gh-mnb7jk', 30, 99, '0.5000', 1467029941, 0, 1467086457),
(93, 16, '', 50, 99, '0.5000', 1467029962, 0, 1467086457),
(94, 16, '09oiuy8e93mhau7a', 40, 99, '2.7500', 1467084995, 0, 1467086378),
(95, 16, '', 50, 1, '0.4000', 1467085014, 0, 1467085014),
(96, 16, '', 50, 1, '0.6000', 1467085022, 0, 1467085022),
(97, 16, '', 50, 99, '1.9000', 1467087051, 0, 1467087766),
(98, 16, '', 50, 99, '0.6000', 1467087063, 0, 1467087227),
(99, 7, NULL, 10, 99, '10.0000', 1467087132, 30, 1467087452),
(100, 16, '09oiuy8e93mhau7a', 40, 99, '1.4000', 1467087279, 0, 1467087555),
(101, 16, '09oiuy8e93mhau7a', 40, 99, '5.0000', 1467087338, 0, 1467087384),
(102, 17, '1245e44e4e131r1r1re4r341rrr1ew43341w', 40, 99, '5.0000', 1467087410, 0, 1467087452),
(103, 9, NULL, 10, 99, '10.0000', 1467088062, 31, 1467090496),
(104, 17, '1245e44e4e131r1r1re4r341rrr1ew43341w', 40, 99, '5.0000', 1467088089, 0, 1467088138),
(105, 7, 'lkjai098oei', 30, 99, '1.5000', 1467088180, 0, 1467088276),
(106, 16, '', 50, 99, '1.5000', 1467088221, 0, 1467088276),
(107, 2, '3125afhdsloi', 30, 99, '0.9500', 1467088475, 0, 1467088638),
(108, 16, '', 50, 99, '0.9500', 1467088508, 0, 1467088638),
(109, 7, 'lkjai098oei', 20, 99, '16.5000', 1467088692, 30, 1467089359),
(110, 16, '', 50, 99, '16.5000', 1467088740, 0, 1467089706),
(111, 16, '', 50, 99, '16.5000', 1467088754, 0, 1467089359),
(112, 6, '09oiul98gh-mnb7jk', 30, 99, '1.1800', 1467088884, 0, 1467088965),
(113, 16, '09oiuy8e93mhau7a', 40, 99, '5.0000', 1467088912, 0, 1467090496),
(114, 16, '', 50, 99, '1.1800', 1467088927, 0, 1467088965),
(115, 12, NULL, 10, 99, '10.0000', 1467089045, 32, 1467089664),
(116, 7, NULL, 10, 99, '10.0000', 1467089188, 33, 1467089417),
(117, 16, '09oiuy8e93mhau7a', 40, 99, '5.0000', 1467089205, 0, 1467089248),
(118, 16, '09oiuy8e93mhau7a', 40, 99, '5.0000', 1467089336, 0, 1467089417),
(119, 16, '09oiuy8e93mhau7a', 40, 99, '5.0000', 1467089399, 0, 1467089472),
(120, 16, '09oiuy8e93mhau7a', 40, 99, '5.0000', 1467089598, 0, 1467089664),
(121, 16, '09oiuy8e93mhau7a', 40, 99, '5.0000', 1467090270, 0, 1467090404),
(122, 12, 'gfda6587jhg90-09poiũb-01', 20, 99, '16.5000', 1467090314, 32, 1467090419),
(123, 16, '', 50, 99, '16.5000', 1467090337, 0, 1467090419),
(124, 6, NULL, 10, 99, '10.0000', 1467090625, 34, 1467091629),
(125, 16, '09oiuy8e93mhau7a', 40, 99, '5.0000', 1467090649, 0, 1467090716),
(126, 12, NULL, 10, 99, '10.0000', 1467090737, 35, 1467092627),
(127, 17, '1245e44e4e131r1r1re4r341rrr1ew43341w', 40, 99, '5.0000', 1467090755, 0, 1467091629),
(128, 16, '09oiuy8e93mhau7a', 40, 99, '5.0000', 1467090766, 0, 1467091639),
(129, 16, '09oiuy8e93mhau7a', 40, 99, '5.0000', 1467092589, 0, 1467092627),
(130, 13, NULL, 10, 99, '10.0000', 1467092663, 36, 1467092787),
(131, 16, '09oiuy8e93mhau7a', 40, 99, '5.0000', 1467092680, 0, 1467092718),
(132, 16, '09oiuy8e93mhau7a', 40, 99, '5.0000', 1467092732, 0, 1467092787),
(133, 16, '', 50, 99, '93.9000', 1467218098, 0, 1467218165),
(134, 6, '09oiul98gh-mnb7jk', 30, 99, '1.5000', 1467221708, 0, 1467222455),
(135, 6, '09oiul98gh-mnb7jk', 20, 99, '16.5000', 1467222327, 34, 1467222431),
(136, 16, '', 50, 99, '16.5000', 1467222368, 0, 1467222431),
(137, 17, '', 50, 99, '1.5000', 1467222377, 0, 1467222455),
(138, 16, '', 50, 1, '1.0000', 1467222493, 0, 1467222493),
(139, 16, '', 50, 99, '1.2500', 1467222502, 0, 1467222748),
(140, 16, '', 50, 99, '0.2000', 1467222508, 0, 1467222992),
(141, 6, '09oiul98gh-mnb7jk', 20, 99, '16.5000', 1467222742, 6, 1467223437),
(142, 9, 'mnalkj325', 20, 99, '16.5000', 1467223045, 31, 1467223559),
(143, 6, '09oiul98gh-mnb7jk', 30, 99, '1.5000', 1467223326, 0, 1467223431),
(144, 16, '', 50, 99, '1.5000', 1467223340, 0, 1467223431),
(145, 16, '', 50, 99, '16.5000', 1467223380, 0, 1467223559),
(146, 16, '', 50, 99, '16.5000', 1467223387, 0, 1467223437),
(147, 16, '', 50, 99, '3.0000', 1467223832, 0, 1467223866),
(148, 16, '', 50, 99, '0.6000', 1467223965, 0, 1467223990),
(149, 16, '', 50, 99, '5.5000', 1467224007, 0, 1467224132),
(150, 16, '', 50, 99, '4.5000', 1467224025, 0, 1467224138),
(151, 16, '', 50, 99, '3.0000', 1467224034, 0, 1467224142),
(152, 16, '', 50, 99, '0.2000', 1467224041, 0, 1467224147),
(153, 16, '', 50, 99, '4.0000', 1467224050, 0, 1467224153),
(154, 16, '', 50, 99, '0.5000', 1467224057, 0, 1467224158),
(155, 1, 'm8eilp-9mo0ei-bvn80a', 30, 99, '1.5000', 1467224201, 0, 1467224265),
(156, 16, '', 50, 99, '1.5000', 1467224221, 0, 1467224265),
(157, 1, NULL, 10, 10, '10.0000', 1467372276, 37, 1467372400),
(158, 16, '09oiuy8e93mhau7a', 40, 99, '5.0000', 1467372326, 0, 1467372400),
(159, 7, 'lkjai098oei', 20, 99, '16.5000', 1467480599, 33, 1467480660),
(160, 16, '', 50, 99, '16.5000', 1467480615, 0, 1467480660),
(161, 7, 'lkjai098oei', 30, 1, '1.5000', 1467480695, 0, 1467480695),
(162, 7, 'lkjai098oei', 30, 1, '1.1000', 1467480723, 0, 1467480723),
(163, 16, '', 50, 1, '1.5000', 1467480748, 0, 1467480748),
(164, 17, '', 50, 1, '1.1000', 1467480758, 0, 1467480758);

-- --------------------------------------------------------

--
-- Table structure for table `customer_require_cashout`
--

CREATE TABLE IF NOT EXISTS `customer_require_cashout` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned DEFAULT '0',
  `customer_username` varchar(255) DEFAULT NULL,
  `amount` decimal(10,1) DEFAULT NULL,
  `status` smallint(2) NOT NULL DEFAULT '1',
  `type` smallint(2) DEFAULT '1',
  `admin_created_by` int(11) DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `out_of_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `customer_token`
--

CREATE TABLE IF NOT EXISTS `customer_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `status` smallint(2) NOT NULL DEFAULT '0',
  `type` smallint(2) NOT NULL DEFAULT '0',
  `expired_at` int(11) NOT NULL DEFAULT '0',
  `created_at` int(11) NOT NULL DEFAULT '0',
  `updated_at` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `customer_token`
--

INSERT INTO `customer_token` (`id`, `token`, `customer_id`, `status`, `type`, `expired_at`, `created_at`, `updated_at`) VALUES
(1, 'ffHJOr0oVCLQ-PqM_QPiVhpGaRWhGzLg_1466360535', 1, 1, 0, 0, 1466360535, 1467000662),
(2, 'mAVeK-SzDDadymtuUp9dNBFqjIkodklJ_1466360661', 2, 1, 0, 0, 1466360661, 1467088475),
(3, 'QvWSpS9YHu_dh3hja8iafRZKPA3-dRfZ_1466360761', 3, 0, 0, 0, 1466360761, 1466360761),
(4, 'QQTee32QjyqolwxYiZAwS0E4dS00R4Xn_1466360868', 4, 0, 0, 0, 1466360868, 1466360868),
(5, 'fQCpiBHHdP3jNGd2SImWfYoAZNw3PHtK_1466360943', 5, 1, 0, 0, 1466360943, 1467029380),
(6, '97Q2f3bM5gwnXbgL5Grx_KHbOdYOXHsi_1466361642', 6, 1, 0, 0, 1466361642, 1467088884),
(7, 'UvbP-Ii9YtAj6Gxzi5kIYLunsaALQE7B_1466361875', 7, 1, 0, 0, 1466361875, 1467088180),
(8, '9sy5hmtDYN6uIM1rtkHz4WElJkGMpECO_1466362292', 8, 0, 0, 0, 1466362292, 1466362292),
(9, 'WfWlzdtjaKAi4AfawYtzsX77KET4aJT1_1466362340', 9, 1, 0, 0, 1466362340, 1467223045),
(10, 'FELZO3lXYEHCi69GNmHi9MJ95izSB_4l_1466362347', 10, 0, 0, 0, 1466362347, 1466362347),
(11, 'KVat7HMtVfR8rjiXBX2Gy9YG7DBZUgDY_1466362352', 11, 0, 0, 0, 1466362352, 1466362352),
(12, '-_oidGucfukwmAwP5ac59Ig7VcVDbVSO_1466494579', 12, 1, 0, 0, 1466494579, 1467090314),
(13, 'EFhDE4r1_jWIV1tn59qWS5E3r7MAl6UL_1466498327', 13, 0, 0, 0, 1466498327, 1466498327),
(14, 'R_bvkdtStgqWV1BBHzOsexABO88M2ecZ_1466502193', 14, 1, 0, 0, 1466502193, 1466502270),
(15, 'hnEb-NXgid39p7mRqfBG43c_0IbE8H0V_1467003034', 1, 1, 0, 0, 1467003034, 1467003228),
(16, '8Z1_6bpGHsQSHr5hXR4g51xd8RTBnOJa_1467003406', 1, 1, 0, 0, 1467003406, 1467224201),
(17, 'Zwzh2WcB9rge_dEjt3D57Og85X35EStm_1467031259', 17, 0, 0, 0, 1467031259, 1467031259),
(18, 'MlpN-2ylYUdhRc37y_tbfIuy4tUPUO2A_1467088404', 7, 1, 0, 0, 1467088404, 1467088692),
(19, 'lO8osxJHCWOEzW-8RlddBJ3tV7Ij3FBF_1467088404', 7, 1, 0, 0, 1467088404, 1467480599),
(20, 'pkXgZ7eRuAjhoFCagwGbKDyxB07lV-Ef_1467088404', 7, 1, 0, 0, 1467088404, 1467480695),
(21, 'qEEiqOQD_b1BQJXaZNYS_OBkCnW6VKaB_1467088404', 7, 1, 0, 0, 1467088404, 1467480723),
(22, 'HKcBTRSwhcVxkq3oqPazbN3F5rPt6Hm4_1467088404', 7, 0, 0, 0, 1467088404, 1467088404),
(23, 'ihPKgWG-E4KA7juUkVeClkjXNqoMowWe_1467088404', 7, 0, 0, 0, 1467088404, 1467088404),
(24, '17zUjl_Mad92cLsoUr5wkO_v6gLNwiGE_1467088404', 7, 0, 0, 0, 1467088404, 1467088404),
(25, 'EiHz24_C7IKv8X7tQiDSCkGWv7e9_n05_1467088404', 7, 0, 0, 0, 1467088404, 1467088404),
(26, 'Muy14YVF7KW-zrbaOzzDpYAhw_6SUK9C_1467088404', 7, 0, 0, 0, 1467088404, 1467088404),
(27, 'E0CfZ48x1rJuzbctsJznadtSMCqWYowk_1467088404', 7, 0, 0, 0, 1467088404, 1467088404),
(28, 'mjS7BU9XjO_WKWU_ca17cFAHaUp1AtSG_1467090569', 6, 1, 0, 0, 1467090569, 1467221708),
(29, '12puowB63ZXohdQOjOCTVtImNBNS48Ok_1467221993', 6, 1, 0, 0, 1467221993, 1467222327),
(30, 'ThtVgH-dWIqf7cJjfHuB62HHhtMi7yjo_1467221993', 6, 1, 0, 0, 1467221993, 1467222742),
(31, 'EgrTlZ4JeEiKusXaabgxXIpH7Y3WNMIH_1467221993', 6, 1, 0, 0, 1467221993, 1467223326),
(32, 'dKrSns0GUjUB_Aao79gY5jhUqf7mJqgR_1467221993', 6, 0, 0, 0, 1467221993, 1467221993),
(33, 'V5l4urmvCHerzs0-1c2uDzj8t-Ml6Cqg_1467221993', 6, 0, 0, 0, 1467221993, 1467221993),
(34, '9_BF5QLs0GtqTto89nBU83j4aTUMD-W1_1467221993', 6, 0, 0, 0, 1467221993, 1467221993),
(35, 'VhQo1OCu_QawoXspBMcWZ0Vy2K8RJkJL_1467221993', 6, 0, 0, 0, 1467221993, 1467221993),
(36, '8WkFQQSYVNM1skBo3rWSr_AkFdyGmjFJ_1467221993', 6, 0, 0, 0, 1467221993, 1467221993),
(37, 'Cg0NiZ8a973eXju4THk1CjD5uK6MdyyD_1467221993', 6, 0, 0, 0, 1467221993, 1467221993),
(38, 'TNaHEGjaGbDWW3xDzMueFX5oidxeEpk-_1467221993', 6, 0, 0, 0, 1467221993, 1467221993),
(39, 'pmkumBzWfzAOKAbcukFF3usIfo3ood5s_1467224376', 1, 0, 0, 0, 1467224376, 1467224376);

-- --------------------------------------------------------

--
-- Table structure for table `customer_transaction`
--

CREATE TABLE IF NOT EXISTS `customer_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_sent_request_id` int(11) NOT NULL,
  `customer_sent_queue_id` int(11) unsigned NOT NULL DEFAULT '0',
  `customer_sent_id` int(11) unsigned NOT NULL,
  `customer_sent_username` varchar(255) NOT NULL,
  `customer_sent_bank_address` varchar(255) DEFAULT NULL,
  `amount` decimal(10,4) unsigned NOT NULL,
  `customer_receiver_request_id` int(11) NOT NULL,
  `customer_receiver_queue_id` int(11) NOT NULL,
  `customer_receiver_id` int(11) unsigned NOT NULL,
  `customer_receiver_username` varchar(255) NOT NULL,
  `customer_receiver_bank_address` varchar(255) DEFAULT NULL,
  `status` smallint(4) unsigned NOT NULL DEFAULT '10',
  `customer_sent_type` smallint(2) NOT NULL DEFAULT '0',
  `customer_receiver_type` smallint(2) NOT NULL DEFAULT '0',
  `type_step` smallint(4) NOT NULL DEFAULT '0',
  `expired_sent` int(11) unsigned NOT NULL,
  `expired_approve` int(11) unsigned NOT NULL,
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0',
  `attachment` varchar(255) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=99 ;

--
-- Dumping data for table `customer_transaction`
--

INSERT INTO `customer_transaction` (`id`, `customer_sent_request_id`, `customer_sent_queue_id`, `customer_sent_id`, `customer_sent_username`, `customer_sent_bank_address`, `amount`, `customer_receiver_request_id`, `customer_receiver_queue_id`, `customer_receiver_id`, `customer_receiver_username`, `customer_receiver_bank_address`, `status`, `customer_sent_type`, `customer_receiver_type`, `type_step`, `expired_sent`, `expired_approve`, `created_at`, `updated_at`, `attachment`, `note`) VALUES
(1, 7, 12, 11, 'CapF5', NULL, '0.2000', 5, 9, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 1, 2, 10, 1466363101, 1466363401, 1466362801, 1466363095, 'customer/11/d4771bf4b34a18ee8e5e0d440e38c8f3.jpg', NULL),
(2, 13, 21, 11, 'CapF5', NULL, '1.0000', 9, 15, 2, 'test2', '3125afhdsloi', 99, 1, 2, 10, 1466364002, 1466364302, 1466363702, 1466363918, 'customer/11/02361fe191b9964ddd6baf8f376a0579.jpg', NULL),
(3, 12, 19, 10, 'CapF4', NULL, '1.6500', 8, 14, 3, 'test3', '894loiu546', 99, 1, 2, 10, 1466364002, 1466364302, 1466363702, 1466364069, 'customer/10/9b25fe0bf9b0607e73910c1ff4ee586b.jpg', NULL),
(4, 6, 10, 9, 'CapF3', NULL, '0.3000', 10, 16, 4, 'test4', 'bghy31y5ui36eww585', 99, 1, 2, 10, 1466364002, 1466364302, 1466363702, 1466364289, 'customer/9/8183b0b9a1e6a357a866d694af04d53f.png', NULL),
(5, 11, 17, 6, 'CapCha', NULL, '2.6500', 15, 24, 5, 'test5', 'nb78j90-lkh02a36', 99, 1, 2, 10, 1466364601, 1466364901, 1466364301, 1466364352, 'customer/6/a2e7c46ed5abdc4870f0117df3bc2df8.jpg', NULL),
(6, 4, 7, 10, 'CapF4', NULL, '0.4000', 14, 23, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 1, 2, 10, 1466364601, 1466364901, 1466364301, 1466364894, 'customer/10/bb9897f075722b12f2d548e28884925c.jpg', NULL),
(7, 3, 5, 8, 'CapF2', NULL, '0.3000', 17, 26, 4, 'test4', 'bghy31y5ui36eww585', 99, 1, 2, 10, 1466364901, 1466365201, 1466364601, 1466365307, 'customer/8/cc9b96579cde2b1bc5b3a7e753ed3727.jpg', NULL),
(8, 2, 3, 7, 'CapF1', NULL, '0.4000', 18, 27, 2, 'test2', '3125afhdsloi', 99, 1, 2, 10, 1466364901, 1466365201, 1466364601, 1466364861, 'customer/7/60692a5c54ca06f7031c3edac833e762.jpg', NULL),
(9, 6, 11, 9, 'CapF3', NULL, '0.2000', 21, 30, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 1, 2, 11, 1466365201, 1466365501, 1466364901, 1466365377, 'customer/9/221e537e06316cb0fcf99e6900fa33ef.jpg', NULL),
(10, 1, 1, 6, 'CapCha', NULL, '0.3000', 23, 32, 5, 'test5', 'nb78j90-lkh02a36', 99, 1, 2, 10, 1466365201, 1466365501, 1466364901, 1466365114, 'customer/6/ab4efa5f795d37e3ea5d97203275ad84.jpg', NULL),
(11, 4, 8, 10, 'CapF4', NULL, '0.1000', 24, 33, 5, 'test5', 'nb78j90-lkh02a36', 99, 1, 2, 11, 1466365502, 1466365802, 1466365202, 1466365583, 'customer/10/c4c944c87e513f1179776a83a73c1217.jpg', NULL),
(12, 2, 4, 7, 'CapF1', NULL, '0.1000', 26, 35, 5, 'test5', 'nb78j90-lkh02a36', 99, 1, 2, 11, 1466365801, 1466366101, 1466365501, 1466365618, 'customer/7/8e74ed7cb924859ad3f7638625f3bee7.jpg', NULL),
(13, 13, 22, 11, 'CapF5', NULL, '1.0000', 27, 36, 4, 'test4', 'bghy31y5ui36eww585', 99, 1, 2, 11, 1466366101, 1466366401, 1466365801, 1466366008, 'customer/11/bd8266a631d225280bbadb3ceba43c6b.jpg', NULL),
(14, 11, 18, 6, 'CapCha', NULL, '7.3500', 28, 37, 4, 'test4', 'bghy31y5ui36eww585', 99, 1, 2, 11, 1466366101, 1466366401, 1466365801, 1466365998, 'customer/6/09e07262744e19a4db43e1b150dd21f2.jpg', NULL),
(15, 1, 2, 6, 'CapCha', NULL, '0.2000', 29, 38, 3, 'test3', '894loiu546', 99, 1, 2, 11, 1466366101, 1466366401, 1466365801, 1466365837, 'customer/6/fa8f965bd2d9f8d4dd71f88d7467f722.jpg', NULL),
(16, 3, 6, 8, 'CapF2', NULL, '0.2000', 30, 39, 4, 'test4', 'bghy31y5ui36eww585', 99, 1, 2, 11, 1466366101, 1466366401, 1466365801, 1466365966, 'customer/8/fe135e17442a6f323047b544cb9cd490.jpg', NULL),
(17, 34, 45, 9, 'CapF3', NULL, '0.5000', 19, 28, 3, 'test3', '894loiu546', 99, 1, 2, 10, 1466366402, 1466366702, 1466366102, 1466366239, 'customer/9/53cb45937a651148d26678c1826b70b8.jpg', NULL),
(18, 33, 43, 8, 'CapF2', NULL, '1.2500', 22, 31, 3, 'test3', '894loiu546', 99, 1, 2, 10, 1466366402, 1466366702, 1466366102, 1466366306, 'customer/8/4a0bd68ef2968a5a837829f7eba87d91.jpg', NULL),
(19, 31, 40, 7, 'CapF1', NULL, '0.3000', 25, 34, 3, 'test3', '894loiu546', 99, 1, 2, 10, 1466366402, 1466366702, 1466366102, 1466366344, 'customer/7/543e74796855b0f50b86ba0add422798.jpg', NULL),
(20, 12, 20, 10, 'CapF4', NULL, '3.3500', 32, 42, 5, 'test5', 'nb78j90-lkh02a36', 99, 1, 2, 11, 1466366402, 1466366702, 1466366102, 1466366176, 'customer/10/363eec040e8b2ff654e60f83155fa77d.jpg', NULL),
(21, 33, 44, 8, 'CapF2', NULL, '3.7500', 36, 48, 2, 'test2', '3125afhdsloi', 99, 1, 2, 11, 1466367002, 1466367302, 1466366702, 1466366772, 'customer/8/0be66a115a8be22d77a3c7c67673b3db.jpg', NULL),
(22, 7, 13, 11, 'CapF5', NULL, '0.8000', 37, 49, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 1, 2, 11, 1466367002, 1466367302, 1466366702, 1466366851, 'customer/11/a927f9247634c3b3c70f11b254ea7f29.jpg', NULL),
(23, 34, 46, 9, 'CapF3', NULL, '1.5000', 38, 50, 2, 'test2', '3125afhdsloi', 99, 1, 2, 11, 1466367602, 1466367902, 1466367302, 1466367402, 'customer/9/bdd0c73064be510362a46f05492f0020.jpg', NULL),
(24, 48, 64, 5, 'test5', NULL, '1.2500', 41, 53, 10, 'CapF4', '36afgh6nh', 99, 1, 2, 10, 1466368201, 1466368501, 1466367901, 1466368203, 'customer/5/f443c9a95181e67c235e4b70b63ca6f5.jpg', NULL),
(25, 47, 62, 3, 'test3', NULL, '1.2500', 42, 54, 11, 'CapF5', 'mnjuy870poi83', 99, 1, 2, 10, 1466368201, 1466368501, 1466367901, 1466368356, 'customer/3/099539023c670c0ccb8b05cdf919b5a6.jpg', NULL),
(26, 46, 60, 2, 'test2', NULL, '0.6000', 39, 51, 6, 'CapCha', '09oiul98gh-mnb7jk', 99, 1, 2, 10, 1466368201, 1466368501, 1466367901, 1466368401, 'customer/2/aebc15a0083b964b064e7b55e7fafc37.jpg', NULL),
(27, 44, 56, 1, 'test1', NULL, '5.0000', 16, 25, 5, 'test5', 'nb78j90-lkh02a36', 99, 1, 2, 10, 1466368201, 1466368501, 1466367901, 1466368616, 'customer/1/0138a194551751760f49050b03af7642.jpg', NULL),
(28, 31, 41, 7, 'CapF1', NULL, '0.7000', 49, 66, 11, 'CapF5', 'mnjuy870poi83', 99, 1, 2, 11, 1466368201, 1466368501, 1466367901, 1466368242, 'customer/7/4907610687786210630de35c186947f4.jpg', NULL),
(29, 54, 71, 5, 'test5', NULL, '5.0000', 43, 55, 8, 'CapF2', '64afg4ve2', 99, 1, 2, 10, 1466369103, 1466369403, 1466368803, 1466368887, 'customer/5/0ad16da9e986a50d5e5c9a74cf1404b4.jpg', NULL),
(30, 47, 63, 3, 'test3', NULL, '3.7500', 50, 67, 5, 'test5', 'nb78j90-lkh02a36', 99, 1, 2, 11, 1466369103, 1466369403, 1466368803, 1466369037, 'customer/3/7389ffc0b9339748e4729b97fb8b23d1.jpg', NULL),
(31, 44, 57, 1, 'test1', NULL, '5.0000', 55, 73, 3, 'test3', '894loiu546', 99, 1, 2, 11, 1466369103, 1466369403, 1466368803, 1466369010, 'customer/1/e02044fbcb75d9cece42ffd872a66f15.jpg', NULL),
(32, 45, 58, 4, 'test4', NULL, '2.2500', 52, 69, 8, 'CapF2', '64afg4ve2', 99, 1, 2, 10, 1466369103, 1466369403, 1466368803, 1466368939, 'customer/4/1d25a174c1001ccc1d0f9562207825c0.jpg', NULL),
(33, 66, 84, 2, 'test2', NULL, '7.2000', 20, 29, 5, 'test5', 'nb78j90-lkh02a36', 99, 1, 2, 10, 1466369701, 1466370001, 1466369401, 1466369660, 'customer/2/71712dfc37a400e455d56c91a1f55319.jpg', NULL),
(34, 54, 72, 5, 'test5', NULL, '5.0000', 61, 79, 6, 'CapCha', '09oiul98gh-mnb7jk', 99, 1, 2, 11, 1466369701, 1466370001, 1466369401, 1466369610, 'customer/5/e950ab0181fe92f8278dce1ab317e2d8.jpg', NULL),
(35, 67, 86, 6, 'CapCha', NULL, '2.6000', 59, 77, 9, 'CapF3', 'mnalkj325', 99, 1, 2, 10, 1466385001, 1466385301, 1466384701, 1466384997, 'customer/6/b205be6d580baba0cb0940eabf8060e3.png', NULL),
(36, 68, 88, 12, 'conf1', NULL, '6.5000', 60, 78, 6, 'CapCha', '09oiul98gh-mnb7jk', 99, 1, 2, 10, 1466495102, 1466495402, 1466494802, 1466495247, 'customer/12/9e6ae8041672b8573a256c96557408e6.jpg', NULL),
(37, 68, 89, 12, 'conf1', NULL, '3.5000', 71, 92, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 1, 2, 11, 1466497802, 1466498102, 1466497502, 1466498193, 'customer/12/0bfe0c6ce11dbfbbb70c582a28137de1.jpg', NULL),
(38, 72, 93, 13, 'chauf1', NULL, '5.0000', 62, 80, 6, 'CapCha', '09oiul98gh-mnb7jk', 99, 1, 2, 10, 1466498702, 1466499002, 1466498402, 1467090201, 'customer/13/5e36e8e02643f1810891fd93ba3bbb3a.jpg', NULL),
(39, 74, 96, 14, 'testvn', NULL, '0.2000', 35, 47, 2, 'test2', '3125afhdsloi', 99, 1, 2, 10, 1466502902, 1466503202, 1466502602, 1466502799, 'customer/14/edf05dcc86e963f02bc3a51febd8974a.jpg', NULL),
(40, 74, 97, 14, 'testvn', NULL, '0.3000', 76, 99, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 1, 2, 11, 1466503202, 1466503502, 1466502902, 1466503429, 'customer/14/574dd6ae8dae70330e4d574194bc694d.jpg', NULL),
(41, 78, 101, 14, 'testvn', NULL, '3.0000', 64, 82, 10, 'CapF4', '36afgh6nh', 99, 1, 2, 10, 1466504103, 1466504403, 1466503803, 1467223861, 'customer/14/aca80f79cb88d9b66bd14f1450293e71.png', NULL),
(42, 80, 104, 1, 'test1', NULL, '0.5000', 77, 100, 6, 'CapCha', '09oiul98gh-mnb7jk', 99, 1, 3, 30, 1466931301, 1466931601, 1466931001, 1467000878, 'customer/1/b3c7272f66d86ab69c67a51277c8c575.jpg', NULL),
(43, 67, 87, 6, 'CapCha', NULL, '7.4000', 81, 107, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 11, 1467000888, 1467000888, 1467000288, 1467002903, 'customer/6/52e577b3694cf17790fcc659e7512ac7.jpg', NULL),
(44, 80, 105, 1, 'test1', NULL, '0.5000', 84, 110, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 11, 1467001802, 1467001802, 1467001202, 1467002203, 'customer/1/356cdbd2dab83f3544ccb4849a1cab4c.png', NULL),
(45, 87, 112, 16, 'admin2', NULL, '0.5000', 86, 112, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 20, 3, 30, 1467004130, 1467004130, 1467003530, 1467003854, 'customer/16/b53b60f02403684f3be8362ce43f1484.jpg', NULL),
(46, 66, 85, 2, 'test2', NULL, '2.8000', 88, 114, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 11, 1467022443, 1467022443, 1467021843, 1467021976, 'customer/2/3ca477b0b48f312655926b1e48771414.jpg', NULL),
(47, 48, 65, 5, 'test5', NULL, '3.7500', 89, 115, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 11, 1467029579, 1467029579, 1467028979, 1467029108, 'customer/5/8a6060d4022fb53785598048603e7abf.jpg', NULL),
(48, 90, 111, 16, 'admin2', NULL, '1.2500', 85, 111, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 20, 2, 20, 1467029791, 1467029791, 1467029191, 1467029279, 'customer/16/2acad61dc8cd211e565abf3126aaceb8.jpg', NULL),
(49, 93, 118, 16, 'admin2', NULL, '0.5000', 92, 118, 6, 'CapCha', '09oiul98gh-mnb7jk', 99, 20, 3, 30, 1467030562, 1467030562, 1467029962, 1467086457, 'customer/16/20bc4fa33434664cfb17ec46cde36f38.jpg', NULL),
(50, 45, 59, 4, 'test4', NULL, '2.7500', 94, 120, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 11, 1467085595, 1467085595, 1467084995, 1467086378, 'customer/4/67e1b697271f4f05be2230fbab75898e.jpg', NULL),
(51, 95, 52, 16, 'admin2', NULL, '0.4000', 40, 52, 9, 'CapF3', 'mnalkj325', 99, 20, 2, 20, 1467085614, 1467085614, 1467085014, 1467085216, 'customer/16/b4ce8f2470f594ad26a154baff13c075.jpg', NULL),
(52, 96, 68, 16, 'admin2', NULL, '0.6000', 51, 68, 8, 'CapF2', '64afg4ve2', 99, 20, 2, 20, 1467085622, 1467085622, 1467085022, 1467085171, 'customer/16/323ca6f00041e6458859041ed215852f.jpg', NULL),
(53, 97, 70, 16, 'admin2', NULL, '1.9000', 53, 70, 11, 'CapF5', 'mnjuy870poi83', 99, 20, 2, 20, 1467087651, 1467087651, 1467087051, 1467087766, 'customer/16/08acbd50a259ca1ae5b32b4791c78c23.jpg', NULL),
(54, 98, 74, 16, 'admin2', NULL, '0.6000', 56, 74, 7, 'CapF1', 'lkjai098oei', 99, 20, 2, 20, 1467087663, 1467087663, 1467087063, 1467087227, 'customer/16/c725927865f242ebb9bd72bfe368a8f3.jpg', NULL),
(55, 46, 61, 2, 'test2', NULL, '1.4000', 100, 127, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 11, 1467087879, 1467087879, 1467087279, 1467087555, 'customer/2/e126a82106573f37bcd0ce868bb84745.jpg', NULL),
(56, 99, 125, 7, 'CapF1', NULL, '5.0000', 101, 128, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 10, 1467087938, 1467087938, 1467087338, 1467087384, 'customer/7/87c7aa749efc17a6f6891d9cf83db59f.jpg', NULL),
(57, 99, 126, 7, 'CapF1', NULL, '5.0000', 102, 129, 17, 'admin1', '1245e44e4e131r1r1re4r341rrr1ew43341w', 99, 1, 10, 11, 1467088010, 1467088010, 1467087410, 1467087452, 'customer/7/129a354113ef71e635df957abcffe093.jpg', NULL),
(58, 103, 130, 9, 'CapF3', NULL, '5.0000', 104, 132, 17, 'admin1', '1245e44e4e131r1r1re4r341rrr1ew43341w', 99, 1, 10, 10, 1467088689, 1467088689, 1467088089, 1467088138, 'customer/9/8a812ec0c20abc69c3b31abcbdadba44.jpg', NULL),
(59, 106, 133, 16, 'admin2', NULL, '1.5000', 105, 133, 7, 'CapF1', 'lkjai098oei', 99, 20, 3, 30, 1467088821, 1467088821, 1467088221, 1467088276, 'customer/16/a512919090ff1685beb0e50c46c90a9f.jpg', NULL),
(60, 108, 135, 16, 'admin2', NULL, '0.9500', 107, 135, 2, 'test2', '3125afhdsloi', 99, 20, 3, 30, 1467089108, 1467089108, 1467088508, 1467088638, 'customer/16/934063bb1111afc26b90f41676ae7f1d.jpg', NULL),
(61, 110, 117, 16, 'admin2', NULL, '16.5000', 91, 117, 5, 'test5', 'nb78j90-lkh02a36', 99, 20, 2, 20, 1467089340, 1467089340, 1467088740, 1467089706, 'customer/16/4176110f4f620b248bc5d9a7b20c252e.png', NULL),
(62, 111, 137, 16, 'admin2', NULL, '16.5000', 109, 137, 7, 'CapF1', 'lkjai098oei', 99, 20, 2, 20, 1467089354, 1467089354, 1467088754, 1467089359, 'customer/16/037361478e7ea139782e63d2cc57902c.jpg', NULL),
(63, 103, 131, 9, 'CapF3', NULL, '5.0000', 113, 141, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 11, 1467089512, 1467089512, 1467088912, 1467090496, 'customer/9/ee4f513c09fd0101a06fe9f7361f3912.jpg', NULL),
(64, 114, 140, 16, 'admin2', NULL, '1.1800', 112, 140, 6, 'CapCha', '09oiul98gh-mnb7jk', 99, 20, 3, 30, 1467089527, 1467089527, 1467088927, 1467088965, 'customer/16/eed19d4bbd4a39943ad87d149a5527e7.jpg', NULL),
(65, 116, 145, 7, 'CapF1', NULL, '5.0000', 117, 147, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 10, 1467089805, 1467089805, 1467089205, 1467089248, 'customer/7/4a00685dce743a17aec71984211fa606.jpg', NULL),
(66, 116, 146, 7, 'CapF1', NULL, '5.0000', 118, 148, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 11, 1467089936, 1467089936, 1467089336, 1467089417, 'customer/7/96ebc5fa107c33e9f727d19a5fd5d3a1.jpg', NULL),
(67, 115, 143, 12, 'conf1', NULL, '5.0000', 119, 149, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 10, 1467089999, 1467089999, 1467089399, 1467089472, 'customer/12/29c7f58f9180eaec40183a65010795d1.jpg', NULL),
(68, 115, 144, 12, 'conf1', NULL, '5.0000', 120, 150, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 11, 1467090198, 1467090198, 1467089598, 1467089664, 'customer/12/d1ea45abc8651f1af0f77f1820e47cb6.jpg', NULL),
(69, 72, 94, 13, 'chauf1', NULL, '5.0000', 121, 151, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 11, 1467090870, 1467090870, 1467090270, 1467090404, 'customer/13/7345b5b447ccf09a91f35bfc5facad99.jpg', NULL),
(70, 123, 152, 16, 'admin2', NULL, '16.5000', 122, 152, 12, 'conf1', 'gfda6587jhg90-09poiũb-01', 99, 20, 2, 20, 1467090937, 1467090937, 1467090337, 1467090419, 'customer/16/646a1cfa09825fde00fcb13debfb3b4c.jpg', NULL),
(71, 124, 154, 6, 'CapCha', NULL, '5.0000', 125, 156, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 10, 1467091249, 1467091249, 1467090649, 1467090716, 'customer/6/096e96cd8aa0a8c38b81a6ca3fcb88ed.jpg', NULL),
(72, 124, 155, 6, 'CapCha', NULL, '5.0000', 127, 159, 17, 'admin1', '1245e44e4e131r1r1re4r341rrr1ew43341w', 99, 1, 10, 11, 1467091355, 1467091355, 1467090755, 1467091629, 'customer/6/8054fd95078da7340d7c1dfb6349fc3f.jpg', NULL),
(73, 126, 157, 12, 'conf1', NULL, '5.0000', 128, 160, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 10, 1467091366, 1467091366, 1467090766, 1467091639, 'customer/12/072c0ef64e470c00a25fb2cb33b959e3.jpg', NULL),
(74, 126, 158, 12, 'conf1', NULL, '5.0000', 129, 161, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 11, 1467093189, 1467093189, 1467092589, 1467092627, 'customer/12/13f6cfce351471a4f73ab1f1f361a345.jpg', NULL),
(75, 130, 162, 13, 'chauf1', NULL, '5.0000', 131, 164, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 10, 1467093280, 1467093280, 1467092680, 1467092718, 'customer/13/f2c6ca4af26b7e5e9acc7cd57c991429.jpg', NULL),
(76, 130, 163, 13, 'chauf1', NULL, '5.0000', 132, 165, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 11, 1467093332, 1467093332, 1467092732, 1467092787, 'customer/13/9f2410548a461dc2ce44f1a2ac46bbae.jpg', NULL),
(77, 133, 109, 16, 'admin2', NULL, '93.9000', 83, 109, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 20, 2, 20, 1467218698, 1467218698, 1467218098, 1467218165, 'customer/16/09a9ea76f2648f1f651104de2edb7762.jpg', NULL),
(78, 136, 168, 16, 'admin2', NULL, '16.5000', 135, 168, 6, 'CapCha', '09oiul98gh-mnb7jk', 99, 20, 2, 20, 1467222968, 1467222968, 1467222368, 1467222431, 'customer/16/8fdbb07c9078a0aa187913af00a93bd3.jpg', NULL),
(79, 137, 167, 17, 'admin1', NULL, '1.5000', 134, 167, 6, 'CapCha', '09oiul98gh-mnb7jk', 99, 20, 3, 30, 1467222977, 1467222977, 1467222377, 1467222455, 'customer/17/ee5fa82a87fd0f54ba5d8a564e874c8c.jpg', NULL),
(80, 138, 106, 16, 'admin2', NULL, '1.0000', 77, 106, 6, 'CapCha', '09oiul98gh-mnb7jk', 2, 20, 3, 30, 1467223093, 1467223093, 1467222493, 1467222540, 'customer/16/1bf54ef9253083713f9a2fed4051f81f.jpg', NULL),
(81, 139, 75, 16, 'admin2', NULL, '1.2500', 57, 75, 7, 'CapF1', 'lkjai098oei', 99, 20, 2, 20, 1467223102, 1467223102, 1467222502, 1467222748, 'customer/16/c932563bdf42e75171e52d2e2423ef57.jpg', NULL),
(82, 140, 76, 16, 'admin2', NULL, '0.2000', 58, 76, 9, 'CapF3', 'mnalkj325', 99, 20, 2, 20, 1467223108, 1467223108, 1467222508, 1467222992, 'customer/16/3f3d1e08b0dbb8a4eedfea56c66d234a.jpg', NULL),
(83, 144, 176, 16, 'admin2', NULL, '1.5000', 143, 176, 6, 'CapCha', '09oiul98gh-mnb7jk', 99, 20, 3, 30, 1467223940, 1467223940, 1467223340, 1467223431, 'customer/16/f8b5a9f680c4f848a8c375a0b05dcffb.jpg', NULL),
(84, 145, 175, 16, 'admin2', NULL, '16.5000', 142, 175, 9, 'CapF3', 'mnalkj325', 99, 20, 2, 20, 1467223980, 1467223980, 1467223380, 1467223559, 'customer/16/d527574837fc3897bd0ffcbecabd7141.jpg', NULL),
(85, 146, 174, 16, 'admin2', NULL, '16.5000', 141, 174, 6, 'CapCha', '09oiul98gh-mnb7jk', 99, 20, 2, 20, 1467223987, 1467223987, 1467223387, 1467223437, 'customer/16/41a1273c88446fa620571e560105231f.jpg', NULL),
(86, 147, 83, 16, 'admin2', NULL, '3.0000', 65, 83, 10, 'CapF4', '36afgh6nh', 99, 20, 2, 20, 1467224432, 1467224432, 1467223832, 1467223866, 'customer/16/564ce02ea8bfcb10bdac2e9345cec6cd.jpg', NULL),
(87, 148, 81, 16, 'admin2', NULL, '0.6000', 63, 81, 10, 'CapF4', '36afgh6nh', 99, 20, 2, 20, 1467224565, 1467224565, 1467223965, 1467223990, 'customer/16/48fb019a658252af08f4cbb339f7cf2f.jpg', NULL),
(88, 149, 90, 16, 'admin2', NULL, '5.5000', 69, 90, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 20, 2, 20, 1467224607, 1467224607, 1467224007, 1467224132, 'customer/16/e0b28fffe5071b396f3e108721cabd85.png', NULL),
(89, 150, 91, 16, 'admin2', NULL, '4.5000', 70, 91, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 20, 2, 20, 1467224625, 1467224625, 1467224025, 1467224138, 'customer/16/53338268149e2ac0cf1efb6317be7169.jpg', NULL),
(90, 151, 95, 16, 'admin2', NULL, '3.0000', 73, 95, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 20, 2, 20, 1467224634, 1467224634, 1467224034, 1467224142, 'customer/16/1753cba94881db401da1413b6e9c9592.jpg', NULL),
(91, 152, 98, 16, 'admin2', NULL, '0.2000', 75, 98, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 20, 2, 20, 1467224641, 1467224641, 1467224041, 1467224147, 'customer/16/531214e9f9657d4e296dd18523ba4b99.jpg', NULL),
(92, 153, 103, 16, 'admin2', NULL, '4.0000', 79, 103, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 20, 2, 20, 1467224650, 1467224650, 1467224050, 1467224153, 'customer/16/1abc42d96ecea22f2e772589c8a26c73.jpg', NULL),
(93, 154, 108, 16, 'admin2', NULL, '0.5000', 82, 108, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 20, 3, 30, 1467224657, 1467224657, 1467224057, 1467224158, 'customer/16/7e87950c82ec8cc88acff1d96c4b9d7b.jpg', NULL),
(94, 156, 188, 16, 'admin2', NULL, '1.5000', 155, 188, 1, 'test1', 'm8eilp-9mo0ei-bvn80a', 99, 20, 3, 30, 1467224821, 1467224821, 1467224221, 1467224265, 'customer/16/f7f60638b5ef4fc9659de14ae222d338.jpg', NULL),
(95, 157, 190, 1, 'test1', NULL, '5.0000', 158, 192, 16, 'admin2', '09oiuy8e93mhau7a', 99, 1, 10, 10, 1467372926, 1467372926, 1467372326, 1467372400, 'customer/1/874df306e2a7eb9f79ac39a160fdc84e.jpg', NULL),
(96, 160, 193, 16, 'admin2', NULL, '16.5000', 159, 193, 7, 'CapF1', 'lkjai098oei', 99, 20, 2, 20, 1467481215, 1467481215, 1467480615, 1467480660, 'customer/16/ef392d04bebcccc86f19d1bc4ff6322c.jpg', NULL),
(97, 163, 195, 16, 'admin2', NULL, '1.5000', 161, 195, 7, 'CapF1', 'lkjai098oei', 2, 20, 3, 30, 1467481348, 1467481348, 1467480748, 1467480803, 'customer/16/fd30d37d132fd5a6e911ba4a9c71363d.jpg', NULL),
(98, 164, 196, 17, 'admin1', NULL, '1.1000', 162, 196, 7, 'CapF1', 'lkjai098oei', 1, 20, 3, 30, 1467481358, 1467481358, 1467480758, 1467480758, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customer_transaction_queue`
--

CREATE TABLE IF NOT EXISTS `customer_transaction_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `customer_username` varchar(255) NOT NULL,
  `customer_request_id` int(11) unsigned DEFAULT NULL,
  `type` smallint(2) NOT NULL DEFAULT '0',
  `type_step` smallint(2) NOT NULL,
  `status` smallint(2) NOT NULL DEFAULT '1',
  `start_at` int(11) NOT NULL,
  `amount` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `created_at` int(11) NOT NULL DEFAULT '0',
  `updated_at` int(11) NOT NULL DEFAULT '0',
  `min_amount` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `max_amount` decimal(10,4) NOT NULL DEFAULT '0.0000',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=199 ;

--
-- Dumping data for table `customer_transaction_queue`
--

INSERT INTO `customer_transaction_queue` (`id`, `customer_id`, `customer_username`, `customer_request_id`, `type`, `type_step`, `status`, `start_at`, `amount`, `created_at`, `updated_at`, `min_amount`, `max_amount`) VALUES
(1, 6, 'CapCha', 1, 1, 10, 10, 1466362111, '0.3000', 1466362111, 1466364901, '0.1000', '0.4000'),
(2, 6, 'CapCha', 1, 1, 11, 10, 1466362681, '0.2000', 1466362111, 1466365801, '0.2000', '0.2000'),
(3, 7, 'CapF1', 2, 1, 10, 10, 1466362248, '0.4000', 1466362248, 1466364601, '0.1000', '0.4000'),
(4, 7, 'CapF1', 2, 1, 11, 10, 1466362611, '0.1000', 1466362248, 1466365501, '0.1000', '0.1000'),
(5, 8, 'CapF2', 3, 1, 10, 10, 1466362321, '0.3000', 1466362321, 1466364601, '0.1000', '0.4000'),
(6, 8, 'CapF2', 3, 1, 11, 10, 1466362679, '0.2000', 1466362321, 1466365801, '0.2000', '0.2000'),
(7, 10, 'CapF4', 4, 1, 10, 10, 1466362449, '0.4000', 1466362449, 1466364301, '0.1000', '0.4000'),
(8, 10, 'CapF4', 4, 1, 11, 10, 1466362780, '0.1000', 1466362449, 1466365202, '0.1000', '0.1000'),
(9, 1, 'test1', 5, 2, 20, 10, 1466362637, '0.2000', 1466362637, 1466362801, '0.0000', '0.0000'),
(10, 9, 'CapF3', 6, 1, 10, 10, 1466362664, '0.3000', 1466362664, 1466363702, '0.1000', '0.4000'),
(11, 9, 'CapF3', 6, 1, 11, 10, 1466363135, '0.2000', 1466362664, 1466364901, '0.2000', '0.2000'),
(12, 11, 'CapF5', 7, 1, 10, 10, 1466362680, '0.2000', 1466362680, 1466362801, '0.2000', '0.8000'),
(13, 11, 'CapF5', 7, 1, 11, 10, 1466363019, '0.8000', 1466362680, 1466366702, '0.8000', '0.8000'),
(14, 3, 'test3', 8, 2, 20, 10, 1466362875, '1.6500', 1466362875, 1466363702, '0.0000', '0.0000'),
(15, 2, 'test2', 9, 2, 20, 10, 1466363143, '1.0000', 1466363143, 1466363702, '0.0000', '0.0000'),
(16, 4, 'test4', 10, 2, 20, 10, 1466363459, '0.3000', 1466363459, 1466363702, '0.0000', '0.0000'),
(17, 6, 'CapCha', 11, 1, 10, 10, 1466363510, '2.6500', 1466363510, 1466364301, '2.0000', '8.0000'),
(18, 6, 'CapCha', 11, 1, 11, 10, 1466364001, '7.3500', 1466363510, 1466365801, '7.3500', '7.3500'),
(19, 10, 'CapF4', 12, 1, 10, 10, 1466363539, '1.6500', 1466363539, 1466363702, '1.0000', '4.0000'),
(20, 10, 'CapF4', 12, 1, 11, 10, 1466364036, '3.3500', 1466363539, 1466366102, '3.3500', '3.3500'),
(21, 11, 'CapF5', 13, 1, 10, 10, 1466363556, '1.0000', 1466363556, 1466363702, '0.4000', '1.6000'),
(22, 11, 'CapF5', 13, 1, 11, 10, 1466364029, '1.0000', 1466363556, 1466365801, '1.0000', '1.0000'),
(23, 1, 'test1', 14, 2, 20, 10, 1466364122, '0.4000', 1466364122, 1466364301, '0.0000', '0.0000'),
(24, 5, 'test5', 15, 2, 20, 10, 1466364244, '2.6500', 1466364244, 1466364301, '0.0000', '0.0000'),
(25, 5, 'test5', 16, 2, 20, 10, 1466364379, '5.0000', 1466364379, 1466367901, '0.0000', '0.0000'),
(26, 4, 'test4', 17, 2, 20, 10, 1466364530, '0.3000', 1466364530, 1466364601, '0.0000', '0.0000'),
(27, 2, 'test2', 18, 2, 20, 10, 1466364560, '0.4000', 1466364560, 1466364601, '0.0000', '0.0000'),
(28, 3, 'test3', 19, 2, 20, 10, 1466364594, '0.5000', 1466364594, 1466366102, '0.0000', '0.0000'),
(29, 5, 'test5', 20, 2, 20, 10, 1466364630, '7.2000', 1466364630, 1466369401, '0.0000', '0.0000'),
(30, 1, 'test1', 21, 2, 20, 10, 1466364710, '0.2000', 1466364710, 1466364901, '0.0000', '0.0000'),
(31, 3, 'test3', 22, 2, 20, 10, 1466364773, '1.2500', 1466364773, 1466366102, '0.0000', '0.0000'),
(32, 5, 'test5', 23, 2, 20, 10, 1466364820, '0.3000', 1466364820, 1466364901, '0.0000', '0.0000'),
(33, 5, 'test5', 24, 2, 20, 10, 1466365136, '0.1000', 1466365136, 1466365202, '0.0000', '0.0000'),
(34, 3, 'test3', 25, 2, 20, 10, 1466365439, '0.3000', 1466365439, 1466366102, '0.0000', '0.0000'),
(35, 5, 'test5', 26, 2, 20, 10, 1466365459, '0.1000', 1466365459, 1466365501, '0.0000', '0.0000'),
(36, 4, 'test4', 27, 2, 20, 10, 1466365506, '1.0000', 1466365506, 1466365801, '0.0000', '0.0000'),
(37, 4, 'test4', 28, 2, 20, 10, 1466365687, '7.3500', 1466365687, 1466365801, '0.0000', '0.0000'),
(38, 3, 'test3', 29, 2, 20, 10, 1466365725, '0.2000', 1466365725, 1466365801, '0.0000', '0.0000'),
(39, 4, 'test4', 30, 2, 20, 10, 1466365755, '0.2000', 1466365755, 1466365801, '0.0000', '0.0000'),
(40, 7, 'CapF1', 31, 1, 10, 10, 1466366036, '0.3000', 1466366036, 1466366102, '0.2000', '0.8000'),
(41, 7, 'CapF1', 31, 1, 11, 10, 1466366406, '0.7000', 1466366036, 1466367901, '0.7000', '0.7000'),
(42, 5, 'test5', 32, 2, 20, 10, 1466366061, '3.3500', 1466366061, 1466366102, '0.0000', '0.0000'),
(43, 8, 'CapF2', 33, 1, 10, 10, 1466366082, '1.2500', 1466366082, 1466366102, '1.0000', '4.0000'),
(44, 8, 'CapF2', 33, 1, 11, 10, 1466366667, '3.7500', 1466366082, 1466366702, '3.7500', '3.7500'),
(45, 9, 'CapF3', 34, 1, 10, 10, 1466366096, '0.5000', 1466366096, 1466366102, '0.4000', '1.6000'),
(46, 9, 'CapF3', 34, 1, 11, 10, 1466366461, '1.5000', 1466366096, 1466367302, '1.5000', '1.5000'),
(47, 2, 'test2', 35, 2, 20, 10, 1466366387, '0.2000', 1466366387, 1466502602, '0.0000', '0.0000'),
(48, 2, 'test2', 36, 2, 20, 10, 1466366471, '3.7500', 1466366471, 1466366702, '0.0000', '0.0000'),
(49, 1, 'test1', 37, 2, 20, 10, 1466366544, '0.8000', 1466366544, 1466366702, '0.0000', '0.0000'),
(50, 2, 'test2', 38, 2, 20, 10, 1466367038, '1.5000', 1466367038, 1466367302, '0.0000', '0.0000'),
(51, 6, 'CapCha', 39, 2, 20, 10, 1466367317, '0.6000', 1466367317, 1466367901, '0.0000', '0.0000'),
(52, 9, 'CapF3', 40, 2, 20, 10, 1466367453, '0.4000', 1466367453, 1467085014, '0.0000', '0.0000'),
(53, 10, 'CapF4', 41, 2, 20, 10, 1466367512, '1.2500', 1466367512, 1466367901, '0.0000', '0.0000'),
(54, 11, 'CapF5', 42, 2, 20, 10, 1466367553, '1.2500', 1466367553, 1466367901, '0.0000', '0.0000'),
(55, 8, 'CapF2', 43, 2, 20, 10, 1466367606, '5.0000', 1466367606, 1466368803, '0.0000', '0.0000'),
(56, 1, 'test1', 44, 1, 10, 10, 1466367652, '5.0000', 1466367652, 1466367901, '2.0000', '8.0000'),
(57, 1, 'test1', 44, 1, 11, 10, 1466368089, '5.0000', 1466367652, 1466368803, '5.0000', '5.0000'),
(58, 4, 'test4', 45, 1, 10, 10, 1466367677, '2.2500', 1466367677, 1466368803, '1.0000', '4.0000'),
(59, 4, 'test4', 45, 1, 11, 9, 1466368226, '2.7500', 1466367677, 1467084995, '2.7500', '2.7500'),
(60, 2, 'test2', 46, 1, 10, 10, 1466367710, '0.6000', 1466367710, 1466367901, '0.4000', '1.6000'),
(61, 2, 'test2', 46, 1, 11, 9, 1466368300, '1.4000', 1466367710, 1467087279, '1.4000', '1.4000'),
(62, 3, 'test3', 47, 1, 10, 10, 1466367725, '1.2500', 1466367725, 1466367901, '1.0000', '4.0000'),
(63, 3, 'test3', 47, 1, 11, 10, 1466368100, '3.7500', 1466367725, 1466368803, '3.7500', '3.7500'),
(64, 5, 'test5', 48, 1, 10, 10, 1466367740, '1.2500', 1466367740, 1466367901, '1.0000', '4.0000'),
(65, 5, 'test5', 48, 1, 11, 9, 1466368049, '3.7500', 1466367740, 1467028979, '3.7500', '3.7500'),
(66, 11, 'CapF5', 49, 2, 20, 10, 1466367835, '0.7000', 1466367835, 1466367901, '0.0000', '0.0000'),
(67, 5, 'test5', 50, 2, 20, 10, 1466368532, '3.7500', 1466368532, 1466368803, '0.0000', '0.0000'),
(68, 8, 'CapF2', 51, 2, 20, 10, 1466368557, '0.6000', 1466368557, 1467085022, '0.0000', '0.0000'),
(69, 8, 'CapF2', 52, 2, 20, 10, 1466368567, '2.2500', 1466368567, 1466368803, '0.0000', '0.0000'),
(70, 11, 'CapF5', 53, 2, 20, 10, 1466368662, '1.9000', 1466368662, 1467087051, '0.0000', '0.0000'),
(71, 5, 'test5', 54, 1, 10, 10, 1466368704, '5.0000', 1466368704, 1466368803, '2.0000', '8.0000'),
(72, 5, 'test5', 54, 1, 11, 10, 1466369169, '5.0000', 1466368704, 1466369401, '5.0000', '5.0000'),
(73, 3, 'test3', 55, 2, 20, 10, 1466368794, '5.0000', 1466368794, 1466368803, '0.0000', '0.0000'),
(74, 7, 'CapF1', 56, 2, 20, 10, 1466369103, '0.6000', 1466369103, 1467087063, '0.0000', '0.0000'),
(75, 7, 'CapF1', 57, 2, 20, 10, 1466369117, '1.2500', 1466369117, 1467222502, '0.0000', '0.0000'),
(76, 9, 'CapF3', 58, 2, 20, 10, 1466369168, '0.2000', 1466369168, 1467222508, '0.0000', '0.0000'),
(77, 9, 'CapF3', 59, 2, 20, 10, 1466369184, '2.6000', 1466369184, 1466384701, '0.0000', '0.0000'),
(78, 6, 'CapCha', 60, 2, 20, 10, 1466369230, '6.5000', 1466369230, 1466494802, '0.0000', '0.0000'),
(79, 6, 'CapCha', 61, 2, 20, 10, 1466369241, '5.0000', 1466369241, 1466369401, '0.0000', '0.0000'),
(80, 6, 'CapCha', 62, 2, 20, 10, 1466369261, '5.0000', 1466369261, 1466498402, '0.0000', '0.0000'),
(81, 10, 'CapF4', 63, 2, 20, 10, 1466369317, '0.6000', 1466369317, 1467223965, '0.0000', '0.0000'),
(82, 10, 'CapF4', 64, 2, 20, 10, 1466369332, '3.0000', 1466369332, 1466503803, '0.0000', '0.0000'),
(83, 10, 'CapF4', 65, 2, 20, 10, 1466369338, '3.0000', 1466369338, 1467223832, '0.0000', '0.0000'),
(84, 2, 'test2', 66, 1, 10, 10, 1466369367, '7.2000', 1466369367, 1466369401, '2.0000', '8.0000'),
(85, 2, 'test2', 66, 1, 11, 9, 1466369871, '2.8000', 1466369367, 1467021843, '2.8000', '2.8000'),
(86, 6, 'CapCha', 67, 1, 10, 10, 1466384608, '2.6000', 1466384608, 1466384701, '2.0000', '8.0000'),
(87, 6, 'CapCha', 67, 1, 11, 9, 1466385191, '7.4000', 1466384608, 1467000288, '7.4000', '7.4000'),
(88, 12, 'conf1', 68, 1, 10, 10, 1466494765, '6.5000', 1466494765, 1466494802, '2.0000', '8.0000'),
(89, 12, 'conf1', 68, 1, 11, 10, 1466495106, '3.5000', 1466494765, 1466497502, '3.5000', '3.5000'),
(90, 1, 'test1', 69, 2, 20, 10, 1466494867, '5.5000', 1466494867, 1467224007, '0.0000', '0.0000'),
(91, 1, 'test1', 70, 2, 20, 10, 1466495913, '4.5000', 1466495913, 1467224025, '0.0000', '0.0000'),
(92, 1, 'test1', 71, 2, 20, 10, 1466497212, '3.5000', 1466497212, 1466497502, '0.0000', '0.0000'),
(93, 13, 'chauf1', 72, 1, 10, 10, 1466498360, '5.0000', 1466498360, 1466498402, '2.0000', '8.0000'),
(94, 13, 'chauf1', 72, 1, 11, 9, 1466498830, '5.0000', 1466498360, 1467090270, '5.0000', '5.0000'),
(95, 1, 'test1', 73, 2, 20, 10, 1466498413, '3.0000', 1466498413, 1467224034, '0.0000', '0.0000'),
(96, 14, 'testvn', 74, 1, 10, 10, 1466502367, '0.2000', 1466502367, 1466502602, '0.1000', '0.4000'),
(97, 14, 'testvn', 74, 1, 11, 10, 1466502861, '0.3000', 1466502367, 1466502902, '0.3000', '0.3000'),
(98, 1, 'test1', 75, 2, 20, 10, 1466502479, '0.2000', 1466502479, 1467224041, '0.0000', '0.0000'),
(99, 1, 'test1', 76, 2, 20, 10, 1466502853, '0.3000', 1466502853, 1466502902, '0.0000', '0.0000'),
(100, 6, 'CapCha', 77, 3, 30, 10, 1466503216, '0.5000', 1466503216, 1466931001, '0.0000', '0.0000'),
(101, 14, 'testvn', 78, 1, 10, 10, 1466503503, '3.0000', 1466503503, 1466503803, '2.0000', '8.0000'),
(102, 14, 'testvn', 78, 1, 11, 1, 1466503986, '7.0000', 1466503503, 1466503503, '7.0000', '7.0000'),
(103, 1, 'test1', 79, 2, 20, 10, 1466503760, '4.0000', 1466503760, 1467224050, '0.0000', '0.0000'),
(104, 1, 'test1', 80, 1, 10, 10, 1466930949, '0.5000', 1466930949, 1466931001, '0.2000', '0.8000'),
(105, 1, 'test1', 80, 1, 11, 9, 1466931293, '0.5000', 1466930949, 1467001202, '0.5000', '0.5000'),
(106, 6, 'CapCha', 77, 3, 30, 9, 1466503216, '1.0000', 1466931001, 1467222493, '0.0000', '0.0000'),
(107, 16, 'admin2', 81, 10, 50, 9, 1467000288, '7.4000', 1467000288, 1467000288, '0.0000', '0.0000'),
(108, 1, 'test1', 82, 3, 30, 10, 1467000639, '0.5000', 1467000639, 1467224057, '0.0000', '0.0000'),
(109, 1, 'test1', 83, 2, 20, 10, 1467000662, '93.9000', 1467000662, 1467218098, '0.0000', '0.0000'),
(110, 16, 'admin2', 84, 10, 50, 9, 1467001202, '0.5000', 1467001202, 1467001202, '0.0000', '0.0000'),
(111, 1, 'test1', 85, 2, 20, 10, 1467003228, '1.2500', 1467003228, 1467029191, '0.0000', '0.0000'),
(112, 1, 'test1', 86, 3, 30, 10, 1467003464, '0.5000', 1467003464, 1467003530, '0.0000', '0.0000'),
(113, 16, 'admin2', 87, 20, 60, 9, 1467003530, '0.5000', 1467003530, 1467003530, '0.0000', '0.0000'),
(114, 16, 'admin2', 88, 10, 50, 9, 1467021843, '2.8000', 1467021843, 1467021843, '0.0000', '0.0000'),
(115, 16, 'admin2', 89, 10, 50, 9, 1467028979, '3.7500', 1467028979, 1467028979, '0.0000', '0.0000'),
(116, 16, 'admin2', 90, 20, 60, 9, 1467029191, '1.2500', 1467029191, 1467029191, '0.0000', '0.0000'),
(117, 5, 'test5', 91, 2, 20, 10, 1467029380, '16.5000', 1467029380, 1467088740, '0.0000', '0.0000'),
(118, 6, 'CapCha', 92, 3, 30, 10, 1467029941, '0.5000', 1467029941, 1467029962, '0.0000', '0.0000'),
(119, 16, 'admin2', 93, 20, 60, 9, 1467029962, '0.5000', 1467029962, 1467029962, '0.0000', '0.0000'),
(120, 16, 'admin2', 94, 10, 50, 9, 1467084995, '2.7500', 1467084995, 1467084995, '0.0000', '0.0000'),
(121, 16, 'admin2', 95, 20, 60, 9, 1467085014, '0.4000', 1467085014, 1467085014, '0.0000', '0.0000'),
(122, 16, 'admin2', 96, 20, 60, 9, 1467085022, '0.6000', 1467085022, 1467085022, '0.0000', '0.0000'),
(123, 16, 'admin2', 97, 20, 60, 9, 1467087051, '1.9000', 1467087051, 1467087051, '0.0000', '0.0000'),
(124, 16, 'admin2', 98, 20, 60, 9, 1467087063, '0.6000', 1467087063, 1467087063, '0.0000', '0.0000'),
(125, 7, 'CapF1', 99, 1, 10, 10, 1467087132, '5.0000', 1467087132, 1467087338, '2.0000', '8.0000'),
(126, 7, 'CapF1', 99, 1, 11, 9, 1467087730, '5.0000', 1467087132, 1467087410, '0.0000', '0.0000'),
(127, 16, 'admin2', 100, 10, 50, 9, 1467087279, '1.4000', 1467087279, 1467087279, '0.0000', '0.0000'),
(128, 16, 'admin2', 101, 10, 50, 9, 1467087338, '5.0000', 1467087338, 1467087338, '0.0000', '0.0000'),
(129, 17, 'admin1', 102, 10, 50, 9, 1467087410, '5.0000', 1467087410, 1467087410, '0.0000', '0.0000'),
(130, 9, 'CapF3', 103, 1, 10, 10, 1467088062, '5.0000', 1467088062, 1467088089, '2.0000', '8.0000'),
(131, 9, 'CapF3', 103, 1, 11, 9, 1467088440, '5.0000', 1467088062, 1467088912, '0.0000', '0.0000'),
(132, 17, 'admin1', 104, 10, 50, 9, 1467088089, '5.0000', 1467088089, 1467088089, '0.0000', '0.0000'),
(133, 7, 'CapF1', 105, 3, 30, 10, 1467088180, '1.5000', 1467088180, 1467088221, '0.0000', '0.0000'),
(134, 16, 'admin2', 106, 20, 60, 9, 1467088221, '1.5000', 1467088221, 1467088221, '0.0000', '0.0000'),
(135, 2, 'test2', 107, 3, 30, 10, 1467088475, '0.9500', 1467088475, 1467088508, '0.0000', '0.0000'),
(136, 16, 'admin2', 108, 20, 60, 9, 1467088508, '0.9500', 1467088508, 1467088508, '0.0000', '0.0000'),
(137, 7, 'CapF1', 109, 2, 20, 10, 1467088692, '16.5000', 1467088692, 1467088754, '0.0000', '0.0000'),
(138, 16, 'admin2', 110, 20, 60, 9, 1467088740, '16.5000', 1467088740, 1467088740, '0.0000', '0.0000'),
(139, 16, 'admin2', 111, 20, 60, 9, 1467088754, '16.5000', 1467088754, 1467088754, '0.0000', '0.0000'),
(140, 6, 'CapCha', 112, 3, 30, 10, 1467088884, '1.1800', 1467088884, 1467088927, '0.0000', '0.0000'),
(141, 16, 'admin2', 113, 10, 50, 9, 1467088912, '5.0000', 1467088912, 1467088912, '0.0000', '0.0000'),
(142, 16, 'admin2', 114, 20, 60, 9, 1467088927, '1.1800', 1467088927, 1467088927, '0.0000', '0.0000'),
(143, 12, 'conf1', 115, 1, 10, 10, 1467089045, '5.0000', 1467089045, 1467089399, '2.0000', '8.0000'),
(144, 12, 'conf1', 115, 1, 11, 9, 1467089514, '5.0000', 1467089045, 1467089598, '0.0000', '0.0000'),
(145, 7, 'CapF1', 116, 1, 10, 10, 1467089188, '5.0000', 1467089188, 1467089205, '2.0000', '8.0000'),
(146, 7, 'CapF1', 116, 1, 11, 9, 1467089558, '5.0000', 1467089188, 1467089336, '0.0000', '0.0000'),
(147, 16, 'admin2', 117, 10, 50, 9, 1467089205, '5.0000', 1467089205, 1467089205, '0.0000', '0.0000'),
(148, 16, 'admin2', 118, 10, 50, 9, 1467089336, '5.0000', 1467089336, 1467089336, '0.0000', '0.0000'),
(149, 16, 'admin2', 119, 10, 50, 9, 1467089399, '5.0000', 1467089399, 1467089399, '0.0000', '0.0000'),
(150, 16, 'admin2', 120, 10, 50, 9, 1467089598, '5.0000', 1467089598, 1467089598, '0.0000', '0.0000'),
(151, 16, 'admin2', 121, 10, 50, 9, 1467090270, '5.0000', 1467090270, 1467090270, '0.0000', '0.0000'),
(152, 12, 'conf1', 122, 2, 20, 10, 1467090314, '16.5000', 1467090314, 1467090337, '0.0000', '0.0000'),
(153, 16, 'admin2', 123, 20, 60, 9, 1467090337, '16.5000', 1467090337, 1467090337, '0.0000', '0.0000'),
(154, 6, 'CapCha', 124, 1, 10, 10, 1467090625, '5.0000', 1467090625, 1467090649, '2.0000', '8.0000'),
(155, 6, 'CapCha', 124, 1, 11, 9, 1467091202, '5.0000', 1467090625, 1467090755, '0.0000', '0.0000'),
(156, 16, 'admin2', 125, 10, 50, 9, 1467090649, '5.0000', 1467090649, 1467090649, '0.0000', '0.0000'),
(157, 12, 'conf1', 126, 1, 10, 10, 1467090737, '5.0000', 1467090737, 1467090766, '2.0000', '8.0000'),
(158, 12, 'conf1', 126, 1, 11, 9, 1467091087, '5.0000', 1467090737, 1467092589, '0.0000', '0.0000'),
(159, 17, 'admin1', 127, 10, 50, 9, 1467090755, '5.0000', 1467090755, 1467090755, '0.0000', '0.0000'),
(160, 16, 'admin2', 128, 10, 50, 9, 1467090766, '5.0000', 1467090766, 1467090766, '0.0000', '0.0000'),
(161, 16, 'admin2', 129, 10, 50, 9, 1467092589, '5.0000', 1467092589, 1467092589, '0.0000', '0.0000'),
(162, 13, 'chauf1', 130, 1, 10, 10, 1467092663, '5.0000', 1467092663, 1467092680, '2.0000', '8.0000'),
(163, 13, 'chauf1', 130, 1, 11, 9, 1467093173, '5.0000', 1467092663, 1467092732, '0.0000', '0.0000'),
(164, 16, 'admin2', 131, 10, 50, 9, 1467092680, '5.0000', 1467092680, 1467092680, '0.0000', '0.0000'),
(165, 16, 'admin2', 132, 10, 50, 9, 1467092732, '5.0000', 1467092732, 1467092732, '0.0000', '0.0000'),
(166, 16, 'admin2', 133, 20, 60, 9, 1467218098, '93.9000', 1467218098, 1467218098, '0.0000', '0.0000'),
(167, 6, 'CapCha', 134, 3, 30, 10, 1467221708, '1.5000', 1467221708, 1467222377, '0.0000', '0.0000'),
(168, 6, 'CapCha', 135, 2, 20, 10, 1467222327, '16.5000', 1467222327, 1467222368, '0.0000', '0.0000'),
(169, 16, 'admin2', 136, 20, 60, 9, 1467222368, '16.5000', 1467222368, 1467222368, '0.0000', '0.0000'),
(170, 17, 'admin1', 137, 20, 60, 9, 1467222377, '1.5000', 1467222377, 1467222377, '0.0000', '0.0000'),
(171, 16, 'admin2', 138, 20, 60, 9, 1467222493, '1.0000', 1467222493, 1467222493, '0.0000', '0.0000'),
(172, 16, 'admin2', 139, 20, 60, 9, 1467222502, '1.2500', 1467222502, 1467222502, '0.0000', '0.0000'),
(173, 16, 'admin2', 140, 20, 60, 9, 1467222508, '0.2000', 1467222508, 1467222508, '0.0000', '0.0000'),
(174, 6, 'CapCha', 141, 2, 20, 10, 1467222742, '16.5000', 1467222742, 1467223387, '0.0000', '0.0000'),
(175, 9, 'CapF3', 142, 2, 20, 10, 1467223045, '16.5000', 1467223045, 1467223380, '0.0000', '0.0000'),
(176, 6, 'CapCha', 143, 3, 30, 10, 1467223326, '1.5000', 1467223326, 1467223340, '0.0000', '0.0000'),
(177, 16, 'admin2', 144, 20, 60, 9, 1467223340, '1.5000', 1467223340, 1467223340, '0.0000', '0.0000'),
(178, 16, 'admin2', 145, 20, 60, 9, 1467223380, '16.5000', 1467223380, 1467223380, '0.0000', '0.0000'),
(179, 16, 'admin2', 146, 20, 60, 9, 1467223387, '16.5000', 1467223387, 1467223387, '0.0000', '0.0000'),
(180, 16, 'admin2', 147, 20, 60, 9, 1467223832, '3.0000', 1467223832, 1467223832, '0.0000', '0.0000'),
(181, 16, 'admin2', 148, 20, 60, 9, 1467223965, '0.6000', 1467223965, 1467223965, '0.0000', '0.0000'),
(182, 16, 'admin2', 149, 20, 60, 9, 1467224007, '5.5000', 1467224007, 1467224007, '0.0000', '0.0000'),
(183, 16, 'admin2', 150, 20, 60, 9, 1467224025, '4.5000', 1467224025, 1467224025, '0.0000', '0.0000'),
(184, 16, 'admin2', 151, 20, 60, 9, 1467224034, '3.0000', 1467224034, 1467224034, '0.0000', '0.0000'),
(185, 16, 'admin2', 152, 20, 60, 9, 1467224041, '0.2000', 1467224041, 1467224041, '0.0000', '0.0000'),
(186, 16, 'admin2', 153, 20, 60, 9, 1467224050, '4.0000', 1467224050, 1467224050, '0.0000', '0.0000'),
(187, 16, 'admin2', 154, 20, 60, 9, 1467224057, '0.5000', 1467224057, 1467224057, '0.0000', '0.0000'),
(188, 1, 'test1', 155, 3, 30, 10, 1467224201, '1.5000', 1467224201, 1467224221, '0.0000', '0.0000'),
(189, 16, 'admin2', 156, 20, 60, 9, 1467224221, '1.5000', 1467224221, 1467224221, '0.0000', '0.0000'),
(190, 1, 'test1', 157, 1, 10, 10, 1467372276, '5.0000', 1467372276, 1467372326, '2.0000', '8.0000'),
(191, 1, 'test1', 157, 1, 11, 1, 1467372619, '5.0000', 1467372276, 1467372276, '0.0000', '0.0000'),
(192, 16, 'admin2', 158, 10, 50, 9, 1467372326, '5.0000', 1467372326, 1467372326, '0.0000', '0.0000'),
(193, 7, 'CapF1', 159, 2, 20, 10, 1467480599, '16.5000', 1467480599, 1467480615, '0.0000', '0.0000'),
(194, 16, 'admin2', 160, 20, 60, 9, 1467480615, '16.5000', 1467480615, 1467480615, '0.0000', '0.0000'),
(195, 7, 'CapF1', 161, 3, 30, 9, 1467480695, '1.5000', 1467480695, 1467480748, '0.0000', '0.0000'),
(196, 7, 'CapF1', 162, 3, 30, 9, 1467480723, '1.1000', 1467480723, 1467480758, '0.0000', '0.0000'),
(197, 16, 'admin2', 163, 20, 60, 9, 1467480748, '1.5000', 1467480748, 1467480748, '0.0000', '0.0000'),
(198, 17, 'admin1', 164, 20, 60, 9, 1467480758, '1.1000', 1467480758, 1467480758, '0.0000', '0.0000');

-- --------------------------------------------------------

--
-- Table structure for table `customer_wallet`
--

CREATE TABLE IF NOT EXISTS `customer_wallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `balance` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000',
  `poundage` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000',
  `type` smallint(4) unsigned NOT NULL DEFAULT '10',
  `status` smallint(2) unsigned NOT NULL DEFAULT '1',
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `customer_wallet`
--

INSERT INTO `customer_wallet` (`id`, `customer_id`, `balance`, `poundage`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, '7.9050', '5.4050', 10, 1, 1466360492, 1467224201),
(2, 2, '0.9500', '0.0000', 10, 1, 1466360612, 1467088475),
(3, 3, '0.9500', '0.9500', 10, 1, 1466360710, 1467086378),
(4, 4, '1.7500', '1.7500', 10, 1, 1466360823, 1467029108),
(5, 5, '0.0000', '0.0000', 10, 1, 1466360927, 1466360927),
(6, 6, '6.1750', '0.0000', 10, 1, 1466361599, 1467223326),
(7, 7, '4.1000', '0.0000', 10, 1, 1466361813, 1467480723),
(8, 8, '0.0000', '0.0000', 10, 1, 1466361925, 1466361925),
(9, 9, '0.0000', '0.0000', 10, 1, 1466361982, 1466361982),
(10, 10, '0.0000', '0.0000', 10, 1, 1466362026, 1466362026),
(11, 11, '0.0000', '0.0000', 10, 1, 1466362077, 1466362077),
(12, 12, '2.0000', '2.0000', 10, 1, 1466494498, 1467092787),
(13, 13, '0.0000', '0.0000', 10, 1, 1466498311, 1466498311),
(14, 14, '0.0000', '0.0000', 10, 1, 1466501867, 1466501867),
(15, 15, '0.0000', '0.0000', 10, 1, 1466504021, 1466504021),
(16, 16, '0.0000', '0.0000', 10, 1, 1467000230, 1467000230),
(17, 17, '0.0000', '0.0000', 10, 1, 1467000262, 1467000262),
(18, 18, '0.0000', '0.0000', 10, 1, 1467098035, 1467098035),
(19, 19, '0.0000', '0.0000', 10, 1, 1467098401, 1467098401),
(20, 20, '0.0000', '0.0000', 10, 1, 1467278316, 1467278316),
(21, 21, '0.0000', '0.0000', 10, 1, 1467300977, 1467300977),
(22, 22, '0.0000', '0.0000', 10, 1, 1467329965, 1467329965),
(23, 23, '0.0000', '0.0000', 10, 1, 1467345535, 1467345535),
(24, 24, '0.0000', '0.0000', 10, 1, 1467351247, 1467351247),
(25, 25, '0.0000', '0.0000', 10, 1, 1467351418, 1467351418),
(26, 26, '0.0000', '0.0000', 10, 1, 1467353697, 1467353697),
(27, 27, '0.0000', '0.0000', 10, 1, 1467353889, 1467353889),
(28, 28, '0.0000', '0.0000', 10, 1, 1467367333, 1467367333),
(29, 29, '0.0000', '0.0000', 10, 1, 1467369540, 1467369540),
(30, 30, '0.0000', '0.0000', 10, 1, 1467370312, 1467370312),
(31, 31, '0.0000', '0.0000', 10, 1, 1467389722, 1467389722),
(32, 32, '0.0000', '0.0000', 10, 1, 1467450089, 1467450089),
(33, 33, '0.0000', '0.0000', 10, 1, 1467450933, 1467450933),
(34, 34, '0.0000', '0.0000', 10, 1, 1467451239, 1467451239),
(35, 35, '0.0000', '0.0000', 10, 1, 1467451423, 1467451423),
(36, 36, '0.0000', '0.0000', 10, 1, 1467453186, 1467453186),
(37, 37, '0.0000', '0.0000', 10, 1, 1467527827, 1467527827),
(38, 38, '0.0000', '0.0000', 10, 1, 1467528893, 1467528893),
(39, 39, '0.0000', '0.0000', 10, 1, 1467529298, 1467529298),
(40, 40, '0.0000', '0.0000', 10, 1, 1467530213, 1467530213),
(41, 41, '0.0000', '0.0000', 10, 1, 1467534046, 1467534046),
(42, 42, '0.0000', '0.0000', 10, 1, 1467534260, 1467534260),
(43, 43, '0.0000', '0.0000', 10, 1, 1467535694, 1467535694);

-- --------------------------------------------------------

--
-- Table structure for table `invest_customer_case`
--

CREATE TABLE IF NOT EXISTS `invest_customer_case` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned NOT NULL,
  `level` smallint(4) unsigned NOT NULL,
  `position` smallint(2) unsigned NOT NULL DEFAULT '1',
  `status` smallint(2) unsigned NOT NULL DEFAULT '1',
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `invest_customer_case`
--

INSERT INTO `invest_customer_case` (`id`, `customer_id`, `parent_id`, `level`, `position`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 6, 1, 1, 1, 1467538442, 1467538442),
(2, 2, 6, 1, 2, 1, 1467538585, 1467538585),
(3, 5, 1, 1, 1, 1, 1467538638, 1467538638),
(4, 5, 6, 2, 1, 1, 1467538638, 1467538638),
(5, 12, 1, 1, 2, 1, 1467540234, 1467540234),
(6, 12, 6, 2, 1, 1, 1467540234, 1467540234),
(7, 13, 12, 1, 1, 1, 1467540378, 1467540378),
(8, 13, 1, 2, 2, 1, 1467540378, 1467540378),
(9, 13, 6, 3, 1, 1, 1467540378, 1467540378);

-- --------------------------------------------------------

--
-- Table structure for table `invest_customer_package`
--

CREATE TABLE IF NOT EXISTS `invest_customer_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `package_id` int(11) unsigned NOT NULL,
  `amount` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000',
  `status` smallint(2) unsigned NOT NULL DEFAULT '1',
  `total_current_profit` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `current_profit` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `is_active` smallint(2) unsigned NOT NULL DEFAULT '10',
  `type` smallint(2) unsigned NOT NULL DEFAULT '0',
  `last_cash_out_at` int(11) unsigned NOT NULL DEFAULT '0',
  `current_poundage` decimal(10,4) DEFAULT '0.0000',
  `last_cash_out_poundage` int(11) DEFAULT '0',
  `last_profit_receiver` int(11) NOT NULL DEFAULT '0',
  `joined_at` int(11) unsigned NOT NULL DEFAULT '0',
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `invest_customer_package`
--

INSERT INTO `invest_customer_package` (`id`, `customer_id`, `package_id`, `amount`, `status`, `total_current_profit`, `current_profit`, `is_active`, `type`, `last_cash_out_at`, `current_poundage`, `last_cash_out_poundage`, `last_profit_receiver`, `joined_at`, `created_at`, `updated_at`) VALUES
(1, 6, 1000000, '1.0000', 1, '3.6400', '2.9800', 10, 0, 1467542187, '0.0000', 1467542523, 1467560041, 1467538309, 1467538309, 1467560041),
(2, 6, 1000000, '1.0000', 1, '3.6200', '3.6200', 10, 0, 1467538371, '0.0000', 0, 1467560041, 1467538371, 1467538371, 1467560041),
(3, 1, 1000003, '10.0000', 1, '59.2000', '59.2000', 10, 0, 1467538442, '0.0000', 1467538442, 1467560041, 1467538442, 1467538442, 1467560041),
(4, 2, 1000001, '2.5000', 1, '0.0000', '0.0000', 1, 0, 1467538585, '0.0000', 1467538585, 1467538585, 1467538585, 1467538585, 1467538585),
(5, 5, 1000000, '1.0000', 1, '0.0000', '0.0000', 1, 0, 1467538638, '0.0000', 1467538638, 1467538638, 1467538638, 1467538638, 1467538638),
(6, 12, 1000001, '2.5000', 1, '0.0000', '0.0000', 1, 0, 1467540234, '0.0000', 1467540234, 1467540234, 1467540234, 1467540234, 1467540234),
(7, 13, 1000002, '5.5000', 1, '0.0000', '0.0000', 1, 0, 1467540378, '0.0000', 1467540378, 1467540378, 1467540378, 1467540378, 1467540378);

-- --------------------------------------------------------

--
-- Table structure for table `invest_customer_package_history`
--

CREATE TABLE IF NOT EXISTS `invest_customer_package_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_package_id` int(11) unsigned NOT NULL,
  `sign` smallint(2) unsigned NOT NULL DEFAULT '1',
  `amount` decimal(10,4) unsigned NOT NULL,
  `type` smallint(2) unsigned NOT NULL DEFAULT '0',
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=521 ;

--
-- Dumping data for table `invest_customer_package_history`
--

INSERT INTO `invest_customer_package_history` (`id`, `customer_package_id`, `sign`, `amount`, `type`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '0.0000', 1, 1467538309, 1467538309),
(2, 1, 1, '0.0200', 30, 1467538321, 1467538321),
(3, 2, 1, '0.0000', 1, 1467538371, 1467538371),
(4, 1, 1, '0.0200', 30, 1467538441, 1467538441),
(5, 2, 1, '0.0200', 30, 1467538441, 1467538441),
(6, 3, 1, '0.0000', 1, 1467538442, 1467538442),
(7, 1, 1, '0.0200', 30, 1467538562, 1467538562),
(8, 2, 1, '0.0200', 30, 1467538562, 1467538562),
(9, 4, 1, '0.0000', 1, 1467538585, 1467538585),
(10, 5, 1, '0.0000', 1, 1467538638, 1467538638),
(11, 1, 1, '0.0200', 30, 1467538681, 1467538681),
(12, 2, 1, '0.0200', 30, 1467538681, 1467538681),
(13, 1, 1, '0.0200', 30, 1467538801, 1467538801),
(14, 2, 1, '0.0200', 30, 1467538801, 1467538801),
(15, 1, 1, '0.0200', 30, 1467538921, 1467538921),
(16, 2, 1, '0.0200', 30, 1467538921, 1467538921),
(17, 1, 1, '0.0200', 30, 1467539041, 1467539041),
(18, 2, 1, '0.0200', 30, 1467539041, 1467539041),
(19, 1, 1, '0.0200', 30, 1467539161, 1467539161),
(20, 2, 1, '0.0200', 30, 1467539161, 1467539161),
(21, 1, 1, '0.0200', 30, 1467539281, 1467539281),
(22, 2, 1, '0.0200', 30, 1467539281, 1467539281),
(23, 1, 1, '0.0200', 30, 1467539402, 1467539402),
(24, 2, 1, '0.0200', 30, 1467539402, 1467539402),
(25, 1, 1, '0.0200', 30, 1467539521, 1467539521),
(26, 2, 1, '0.0200', 30, 1467539521, 1467539521),
(27, 1, 1, '0.0200', 30, 1467539641, 1467539641),
(28, 2, 1, '0.0200', 30, 1467539641, 1467539641),
(29, 1, 1, '0.0200', 30, 1467539762, 1467539762),
(30, 2, 1, '0.0200', 30, 1467539762, 1467539762),
(31, 1, 1, '0.0200', 30, 1467539881, 1467539881),
(32, 2, 1, '0.0200', 30, 1467539881, 1467539881),
(33, 1, 1, '0.0200', 30, 1467540001, 1467540001),
(34, 2, 1, '0.0200', 30, 1467540001, 1467540001),
(35, 1, 1, '0.0200', 30, 1467540122, 1467540122),
(36, 2, 1, '0.0200', 30, 1467540122, 1467540122),
(37, 6, 1, '0.0000', 1, 1467540234, 1467540234),
(38, 1, 1, '0.0200', 30, 1467540242, 1467540242),
(39, 2, 1, '0.0200', 30, 1467540242, 1467540242),
(40, 1, 1, '0.0200', 30, 1467540361, 1467540361),
(41, 2, 1, '0.0200', 30, 1467540361, 1467540361),
(42, 7, 1, '0.0000', 1, 1467540378, 1467540378),
(43, 1, 1, '0.0200', 30, 1467540482, 1467540482),
(44, 2, 1, '0.0200', 30, 1467540482, 1467540482),
(45, 1, 1, '0.0200', 30, 1467540602, 1467540602),
(46, 2, 1, '0.0200', 30, 1467540602, 1467540602),
(47, 1, 1, '0.0200', 30, 1467540721, 1467540721),
(48, 2, 1, '0.0200', 30, 1467540721, 1467540721),
(49, 1, 1, '0.0200', 30, 1467540841, 1467540841),
(50, 2, 1, '0.0200', 30, 1467540841, 1467540841),
(51, 1, 1, '0.0200', 30, 1467540962, 1467540962),
(52, 2, 1, '0.0200', 30, 1467540962, 1467540962),
(53, 1, 1, '0.0200', 30, 1467541081, 1467541081),
(54, 2, 1, '0.0200', 30, 1467541081, 1467541081),
(55, 1, 1, '0.0200', 30, 1467541201, 1467541201),
(56, 2, 1, '0.0200', 30, 1467541201, 1467541201),
(57, 1, 1, '0.0200', 30, 1467541321, 1467541321),
(58, 2, 1, '0.0200', 30, 1467541321, 1467541321),
(59, 1, 1, '0.0200', 30, 1467541442, 1467541442),
(60, 2, 1, '0.0200', 30, 1467541442, 1467541442),
(61, 1, 1, '0.0200', 30, 1467541562, 1467541562),
(62, 2, 1, '0.0200', 30, 1467541562, 1467541562),
(63, 1, 1, '0.0200', 30, 1467541681, 1467541681),
(64, 2, 1, '0.0200', 30, 1467541681, 1467541681),
(65, 1, 1, '0.0200', 30, 1467541802, 1467541802),
(66, 2, 1, '0.0200', 30, 1467541802, 1467541802),
(67, 1, 1, '0.0200', 30, 1467541922, 1467541922),
(68, 2, 1, '0.0200', 30, 1467541922, 1467541922),
(69, 1, 1, '0.0200', 30, 1467542041, 1467542041),
(70, 2, 1, '0.0200', 30, 1467542041, 1467542041),
(71, 1, 1, '0.0200', 30, 1467542161, 1467542161),
(72, 2, 1, '0.0200', 30, 1467542161, 1467542161),
(73, 1, 1, '0.0200', 30, 1467542282, 1467542282),
(74, 2, 1, '0.0200', 30, 1467542282, 1467542282),
(75, 3, 1, '10.0000', 10, 1467542303, 1467542303),
(76, 1, 1, '1.5000', 50, 1467542303, 1467542303),
(77, 1, 1, '0.0200', 30, 1467542402, 1467542402),
(78, 2, 1, '0.0200', 30, 1467542402, 1467542402),
(79, 3, 1, '0.4000', 30, 1467542402, 1467542402),
(80, 1, 1, '0.0200', 30, 1467542521, 1467542521),
(81, 2, 1, '0.0200', 30, 1467542521, 1467542521),
(82, 3, 1, '0.4000', 30, 1467542521, 1467542521),
(83, 1, 1, '0.0200', 30, 1467542641, 1467542641),
(84, 2, 1, '0.0200', 30, 1467542641, 1467542641),
(85, 3, 1, '0.4000', 30, 1467542641, 1467542641),
(86, 1, 1, '0.0200', 30, 1467542761, 1467542761),
(87, 2, 1, '0.0200', 30, 1467542761, 1467542761),
(88, 3, 1, '0.4000', 30, 1467542761, 1467542761),
(89, 1, 1, '0.0200', 30, 1467542882, 1467542882),
(90, 2, 1, '0.0200', 30, 1467542882, 1467542882),
(91, 3, 1, '0.4000', 30, 1467542882, 1467542882),
(92, 1, 1, '0.0200', 30, 1467543002, 1467543002),
(93, 2, 1, '0.0200', 30, 1467543002, 1467543002),
(94, 3, 1, '0.4000', 30, 1467543002, 1467543002),
(95, 1, 1, '0.0200', 30, 1467543121, 1467543121),
(96, 2, 1, '0.0200', 30, 1467543121, 1467543121),
(97, 3, 1, '0.4000', 30, 1467543121, 1467543121),
(98, 1, 1, '0.0200', 30, 1467543242, 1467543242),
(99, 2, 1, '0.0200', 30, 1467543242, 1467543242),
(100, 3, 1, '0.4000', 30, 1467543242, 1467543242),
(101, 1, 1, '0.0200', 30, 1467543361, 1467543361),
(102, 2, 1, '0.0200', 30, 1467543361, 1467543361),
(103, 3, 1, '0.4000', 30, 1467543361, 1467543361),
(104, 1, 1, '0.0200', 30, 1467543481, 1467543481),
(105, 2, 1, '0.0200', 30, 1467543481, 1467543481),
(106, 3, 1, '0.4000', 30, 1467543481, 1467543481),
(107, 1, 1, '0.0200', 30, 1467543605, 1467543605),
(108, 2, 1, '0.0200', 30, 1467543605, 1467543605),
(109, 3, 1, '0.4000', 30, 1467543605, 1467543605),
(110, 1, 1, '0.0200', 30, 1467543721, 1467543721),
(111, 2, 1, '0.0200', 30, 1467543721, 1467543721),
(112, 3, 1, '0.4000', 30, 1467543721, 1467543721),
(113, 1, 1, '0.0200', 30, 1467543841, 1467543841),
(114, 2, 1, '0.0200', 30, 1467543841, 1467543841),
(115, 3, 1, '0.4000', 30, 1467543841, 1467543841),
(116, 1, 1, '0.0200', 30, 1467543961, 1467543961),
(117, 2, 1, '0.0200', 30, 1467543961, 1467543961),
(118, 3, 1, '0.4000', 30, 1467543961, 1467543961),
(119, 1, 1, '0.0200', 30, 1467544081, 1467544081),
(120, 2, 1, '0.0200', 30, 1467544081, 1467544081),
(121, 3, 1, '0.4000', 30, 1467544081, 1467544081),
(122, 1, 1, '0.0200', 30, 1467544201, 1467544201),
(123, 2, 1, '0.0200', 30, 1467544201, 1467544201),
(124, 3, 1, '0.4000', 30, 1467544201, 1467544201),
(125, 1, 1, '0.0200', 30, 1467544321, 1467544321),
(126, 2, 1, '0.0200', 30, 1467544321, 1467544321),
(127, 3, 1, '0.4000', 30, 1467544321, 1467544321),
(128, 1, 1, '0.0200', 30, 1467544441, 1467544441),
(129, 2, 1, '0.0200', 30, 1467544441, 1467544441),
(130, 3, 1, '0.4000', 30, 1467544441, 1467544441),
(131, 1, 1, '0.0200', 30, 1467544562, 1467544562),
(132, 2, 1, '0.0200', 30, 1467544562, 1467544562),
(133, 3, 1, '0.4000', 30, 1467544562, 1467544562),
(134, 1, 1, '0.0200', 30, 1467544681, 1467544681),
(135, 2, 1, '0.0200', 30, 1467544681, 1467544681),
(136, 3, 1, '0.4000', 30, 1467544681, 1467544681),
(137, 1, 1, '0.0200', 30, 1467544801, 1467544801),
(138, 2, 1, '0.0200', 30, 1467544801, 1467544801),
(139, 3, 1, '0.4000', 30, 1467544801, 1467544801),
(140, 1, 1, '0.0200', 30, 1467544921, 1467544921),
(141, 2, 1, '0.0200', 30, 1467544921, 1467544921),
(142, 3, 1, '0.4000', 30, 1467544921, 1467544921),
(143, 1, 1, '0.0200', 30, 1467545041, 1467545041),
(144, 2, 1, '0.0200', 30, 1467545041, 1467545041),
(145, 3, 1, '0.4000', 30, 1467545041, 1467545041),
(146, 1, 1, '0.0200', 30, 1467545161, 1467545161),
(147, 2, 1, '0.0200', 30, 1467545161, 1467545161),
(148, 3, 1, '0.4000', 30, 1467545161, 1467545161),
(149, 1, 1, '0.0200', 30, 1467545282, 1467545282),
(150, 2, 1, '0.0200', 30, 1467545282, 1467545282),
(151, 3, 1, '0.4000', 30, 1467545282, 1467545282),
(152, 1, 1, '0.0200', 30, 1467545402, 1467545402),
(153, 2, 1, '0.0200', 30, 1467545402, 1467545402),
(154, 3, 1, '0.4000', 30, 1467545402, 1467545402),
(155, 1, 1, '0.0200', 30, 1467545521, 1467545521),
(156, 2, 1, '0.0200', 30, 1467545521, 1467545521),
(157, 3, 1, '0.4000', 30, 1467545521, 1467545521),
(158, 1, 1, '0.0200', 30, 1467545641, 1467545641),
(159, 2, 1, '0.0200', 30, 1467545641, 1467545641),
(160, 3, 1, '0.4000', 30, 1467545641, 1467545641),
(161, 1, 1, '0.0200', 30, 1467545761, 1467545761),
(162, 2, 1, '0.0200', 30, 1467545761, 1467545761),
(163, 3, 1, '0.4000', 30, 1467545761, 1467545761),
(164, 1, 1, '0.0200', 30, 1467545881, 1467545881),
(165, 2, 1, '0.0200', 30, 1467545881, 1467545881),
(166, 3, 1, '0.4000', 30, 1467545881, 1467545881),
(167, 1, 1, '0.0200', 30, 1467546003, 1467546003),
(168, 2, 1, '0.0200', 30, 1467546003, 1467546003),
(169, 3, 1, '0.4000', 30, 1467546003, 1467546003),
(170, 1, 1, '0.0200', 30, 1467546121, 1467546121),
(171, 2, 1, '0.0200', 30, 1467546121, 1467546121),
(172, 3, 1, '0.4000', 30, 1467546121, 1467546121),
(173, 1, 1, '0.0200', 30, 1467546241, 1467546241),
(174, 2, 1, '0.0200', 30, 1467546241, 1467546241),
(175, 3, 1, '0.4000', 30, 1467546241, 1467546241),
(176, 1, 1, '0.0200', 30, 1467546361, 1467546361),
(177, 2, 1, '0.0200', 30, 1467546361, 1467546361),
(178, 3, 1, '0.4000', 30, 1467546361, 1467546361),
(179, 1, 1, '0.0200', 30, 1467546481, 1467546481),
(180, 2, 1, '0.0200', 30, 1467546481, 1467546481),
(181, 3, 1, '0.4000', 30, 1467546481, 1467546481),
(182, 1, 1, '0.0200', 30, 1467546602, 1467546602),
(183, 2, 1, '0.0200', 30, 1467546602, 1467546602),
(184, 3, 1, '0.4000', 30, 1467546602, 1467546602),
(185, 1, 1, '0.0200', 30, 1467546721, 1467546721),
(186, 2, 1, '0.0200', 30, 1467546721, 1467546721),
(187, 3, 1, '0.4000', 30, 1467546721, 1467546721),
(188, 1, 1, '0.0200', 30, 1467546842, 1467546842),
(189, 2, 1, '0.0200', 30, 1467546842, 1467546842),
(190, 3, 1, '0.4000', 30, 1467546842, 1467546842),
(191, 1, 1, '0.0200', 30, 1467546961, 1467546961),
(192, 2, 1, '0.0200', 30, 1467546961, 1467546961),
(193, 3, 1, '0.4000', 30, 1467546961, 1467546961),
(194, 1, 1, '0.0200', 30, 1467547081, 1467547081),
(195, 2, 1, '0.0200', 30, 1467547081, 1467547081),
(196, 3, 1, '0.4000', 30, 1467547081, 1467547081),
(197, 1, 1, '0.0200', 30, 1467547202, 1467547202),
(198, 2, 1, '0.0200', 30, 1467547202, 1467547202),
(199, 3, 1, '0.4000', 30, 1467547202, 1467547202),
(200, 1, 1, '0.0200', 30, 1467547321, 1467547321),
(201, 2, 1, '0.0200', 30, 1467547321, 1467547321),
(202, 3, 1, '0.4000', 30, 1467547321, 1467547321),
(203, 1, 1, '0.0200', 30, 1467547442, 1467547442),
(204, 2, 1, '0.0200', 30, 1467547442, 1467547442),
(205, 3, 1, '0.4000', 30, 1467547442, 1467547442),
(206, 1, 1, '0.0200', 30, 1467547561, 1467547561),
(207, 2, 1, '0.0200', 30, 1467547561, 1467547561),
(208, 3, 1, '0.4000', 30, 1467547561, 1467547561),
(209, 1, 1, '0.0200', 30, 1467547681, 1467547681),
(210, 2, 1, '0.0200', 30, 1467547681, 1467547681),
(211, 3, 1, '0.4000', 30, 1467547681, 1467547681),
(212, 1, 1, '0.0200', 30, 1467547801, 1467547801),
(213, 2, 1, '0.0200', 30, 1467547801, 1467547801),
(214, 3, 1, '0.4000', 30, 1467547801, 1467547801),
(215, 1, 1, '0.0200', 30, 1467547921, 1467547921),
(216, 2, 1, '0.0200', 30, 1467547921, 1467547921),
(217, 3, 1, '0.4000', 30, 1467547921, 1467547921),
(218, 1, 1, '0.0200', 30, 1467548042, 1467548042),
(219, 2, 1, '0.0200', 30, 1467548042, 1467548042),
(220, 3, 1, '0.4000', 30, 1467548042, 1467548042),
(221, 1, 1, '0.0200', 30, 1467548161, 1467548161),
(222, 2, 1, '0.0200', 30, 1467548161, 1467548161),
(223, 3, 1, '0.4000', 30, 1467548161, 1467548161),
(224, 1, 1, '0.0200', 30, 1467548281, 1467548281),
(225, 2, 1, '0.0200', 30, 1467548281, 1467548281),
(226, 3, 1, '0.4000', 30, 1467548281, 1467548281),
(227, 1, 1, '0.0200', 30, 1467548402, 1467548402),
(228, 2, 1, '0.0200', 30, 1467548402, 1467548402),
(229, 3, 1, '0.4000', 30, 1467548402, 1467548402),
(230, 1, 1, '0.0200', 30, 1467548521, 1467548521),
(231, 2, 1, '0.0200', 30, 1467548521, 1467548521),
(232, 3, 1, '0.4000', 30, 1467548521, 1467548521),
(233, 1, 1, '0.0200', 30, 1467548641, 1467548641),
(234, 2, 1, '0.0200', 30, 1467548641, 1467548641),
(235, 3, 1, '0.4000', 30, 1467548641, 1467548641),
(236, 1, 1, '0.0200', 30, 1467548761, 1467548761),
(237, 2, 1, '0.0200', 30, 1467548761, 1467548761),
(238, 3, 1, '0.4000', 30, 1467548761, 1467548761),
(239, 1, 1, '0.0200', 30, 1467548881, 1467548881),
(240, 2, 1, '0.0200', 30, 1467548881, 1467548881),
(241, 3, 1, '0.4000', 30, 1467548881, 1467548881),
(242, 1, 1, '0.0200', 30, 1467549002, 1467549002),
(243, 2, 1, '0.0200', 30, 1467549002, 1467549002),
(244, 3, 1, '0.4000', 30, 1467549002, 1467549002),
(245, 1, 1, '0.0200', 30, 1467549121, 1467549121),
(246, 2, 1, '0.0200', 30, 1467549121, 1467549121),
(247, 3, 1, '0.4000', 30, 1467549121, 1467549121),
(248, 1, 1, '0.0200', 30, 1467549241, 1467549241),
(249, 2, 1, '0.0200', 30, 1467549241, 1467549241),
(250, 3, 1, '0.4000', 30, 1467549241, 1467549241),
(251, 1, 1, '0.0200', 30, 1467549361, 1467549361),
(252, 2, 1, '0.0200', 30, 1467549361, 1467549361),
(253, 3, 1, '0.4000', 30, 1467549361, 1467549361),
(254, 1, 1, '0.0200', 30, 1467549481, 1467549481),
(255, 2, 1, '0.0200', 30, 1467549481, 1467549481),
(256, 3, 1, '0.4000', 30, 1467549481, 1467549481),
(257, 1, 1, '0.0200', 30, 1467549601, 1467549601),
(258, 2, 1, '0.0200', 30, 1467549601, 1467549601),
(259, 3, 1, '0.4000', 30, 1467549601, 1467549601),
(260, 1, 1, '0.0200', 30, 1467549721, 1467549721),
(261, 2, 1, '0.0200', 30, 1467549721, 1467549721),
(262, 3, 1, '0.4000', 30, 1467549721, 1467549721),
(263, 1, 1, '0.0200', 30, 1467549841, 1467549841),
(264, 2, 1, '0.0200', 30, 1467549841, 1467549841),
(265, 3, 1, '0.4000', 30, 1467549841, 1467549841),
(266, 1, 1, '0.0200', 30, 1467549961, 1467549961),
(267, 2, 1, '0.0200', 30, 1467549961, 1467549961),
(268, 3, 1, '0.4000', 30, 1467549961, 1467549961),
(269, 1, 1, '0.0200', 30, 1467550081, 1467550081),
(270, 2, 1, '0.0200', 30, 1467550081, 1467550081),
(271, 3, 1, '0.4000', 30, 1467550081, 1467550081),
(272, 1, 1, '0.0200', 30, 1467550201, 1467550201),
(273, 2, 1, '0.0200', 30, 1467550201, 1467550201),
(274, 3, 1, '0.4000', 30, 1467550201, 1467550201),
(275, 1, 1, '0.0200', 30, 1467550321, 1467550321),
(276, 2, 1, '0.0200', 30, 1467550321, 1467550321),
(277, 3, 1, '0.4000', 30, 1467550321, 1467550321),
(278, 1, 1, '0.0200', 30, 1467550441, 1467550441),
(279, 2, 1, '0.0200', 30, 1467550441, 1467550441),
(280, 3, 1, '0.4000', 30, 1467550441, 1467550441),
(281, 1, 1, '0.0200', 30, 1467550561, 1467550561),
(282, 2, 1, '0.0200', 30, 1467550561, 1467550561),
(283, 3, 1, '0.4000', 30, 1467550561, 1467550561),
(284, 1, 1, '0.0200', 30, 1467550681, 1467550681),
(285, 2, 1, '0.0200', 30, 1467550681, 1467550681),
(286, 3, 1, '0.4000', 30, 1467550681, 1467550681),
(287, 1, 1, '0.0200', 30, 1467550801, 1467550801),
(288, 2, 1, '0.0200', 30, 1467550801, 1467550801),
(289, 3, 1, '0.4000', 30, 1467550801, 1467550801),
(290, 1, 1, '0.0200', 30, 1467550922, 1467550922),
(291, 2, 1, '0.0200', 30, 1467550922, 1467550922),
(292, 3, 1, '0.4000', 30, 1467550922, 1467550922),
(293, 1, 1, '0.0200', 30, 1467551042, 1467551042),
(294, 2, 1, '0.0200', 30, 1467551042, 1467551042),
(295, 3, 1, '0.4000', 30, 1467551042, 1467551042),
(296, 1, 1, '0.0200', 30, 1467551161, 1467551161),
(297, 2, 1, '0.0200', 30, 1467551161, 1467551161),
(298, 3, 1, '0.4000', 30, 1467551161, 1467551161),
(299, 1, 1, '0.0200', 30, 1467551281, 1467551281),
(300, 2, 1, '0.0200', 30, 1467551281, 1467551281),
(301, 3, 1, '0.4000', 30, 1467551281, 1467551281),
(302, 1, 1, '0.0200', 30, 1467551401, 1467551401),
(303, 2, 1, '0.0200', 30, 1467551401, 1467551401),
(304, 3, 1, '0.4000', 30, 1467551401, 1467551401),
(305, 1, 1, '0.0200', 30, 1467551522, 1467551522),
(306, 2, 1, '0.0200', 30, 1467551522, 1467551522),
(307, 3, 1, '0.4000', 30, 1467551522, 1467551522),
(308, 1, 1, '0.0200', 30, 1467551641, 1467551641),
(309, 2, 1, '0.0200', 30, 1467551641, 1467551641),
(310, 3, 1, '0.4000', 30, 1467551641, 1467551641),
(311, 1, 1, '0.0200', 30, 1467551762, 1467551762),
(312, 2, 1, '0.0200', 30, 1467551762, 1467551762),
(313, 3, 1, '0.4000', 30, 1467551762, 1467551762),
(314, 1, 1, '0.0200', 30, 1467551881, 1467551881),
(315, 2, 1, '0.0200', 30, 1467551881, 1467551881),
(316, 3, 1, '0.4000', 30, 1467551881, 1467551881),
(317, 1, 1, '0.0200', 30, 1467552002, 1467552002),
(318, 2, 1, '0.0200', 30, 1467552002, 1467552002),
(319, 3, 1, '0.4000', 30, 1467552002, 1467552002),
(320, 1, 1, '0.0200', 30, 1467552121, 1467552121),
(321, 2, 1, '0.0200', 30, 1467552121, 1467552121),
(322, 3, 1, '0.4000', 30, 1467552121, 1467552121),
(323, 1, 1, '0.0200', 30, 1467552241, 1467552241),
(324, 2, 1, '0.0200', 30, 1467552241, 1467552241),
(325, 3, 1, '0.4000', 30, 1467552241, 1467552241),
(326, 1, 1, '0.0200', 30, 1467552361, 1467552361),
(327, 2, 1, '0.0200', 30, 1467552361, 1467552361),
(328, 3, 1, '0.4000', 30, 1467552361, 1467552361),
(329, 1, 1, '0.0200', 30, 1467552481, 1467552481),
(330, 2, 1, '0.0200', 30, 1467552481, 1467552481),
(331, 3, 1, '0.4000', 30, 1467552481, 1467552481),
(332, 1, 1, '0.0200', 30, 1467552601, 1467552601),
(333, 2, 1, '0.0200', 30, 1467552601, 1467552601),
(334, 3, 1, '0.4000', 30, 1467552601, 1467552601),
(335, 1, 1, '0.0200', 30, 1467552721, 1467552721),
(336, 2, 1, '0.0200', 30, 1467552721, 1467552721),
(337, 3, 1, '0.4000', 30, 1467552721, 1467552721),
(338, 1, 1, '0.0200', 30, 1467552841, 1467552841),
(339, 2, 1, '0.0200', 30, 1467552841, 1467552841),
(340, 3, 1, '0.4000', 30, 1467552841, 1467552841),
(341, 1, 1, '0.0200', 30, 1467552961, 1467552961),
(342, 2, 1, '0.0200', 30, 1467552961, 1467552961),
(343, 3, 1, '0.4000', 30, 1467552961, 1467552961),
(344, 1, 1, '0.0200', 30, 1467553081, 1467553081),
(345, 2, 1, '0.0200', 30, 1467553081, 1467553081),
(346, 3, 1, '0.4000', 30, 1467553081, 1467553081),
(347, 1, 1, '0.0200', 30, 1467553202, 1467553202),
(348, 2, 1, '0.0200', 30, 1467553202, 1467553202),
(349, 3, 1, '0.4000', 30, 1467553202, 1467553202),
(350, 1, 1, '0.0200', 30, 1467553321, 1467553321),
(351, 2, 1, '0.0200', 30, 1467553321, 1467553321),
(352, 3, 1, '0.4000', 30, 1467553321, 1467553321),
(353, 1, 1, '0.0200', 30, 1467553442, 1467553442),
(354, 2, 1, '0.0200', 30, 1467553442, 1467553442),
(355, 3, 1, '0.4000', 30, 1467553442, 1467553442),
(356, 1, 1, '0.0200', 30, 1467553562, 1467553562),
(357, 2, 1, '0.0200', 30, 1467553562, 1467553562),
(358, 3, 1, '0.4000', 30, 1467553562, 1467553562),
(359, 1, 1, '0.0200', 30, 1467553681, 1467553681),
(360, 2, 1, '0.0200', 30, 1467553681, 1467553681),
(361, 3, 1, '0.4000', 30, 1467553681, 1467553681),
(362, 1, 1, '0.0200', 30, 1467553801, 1467553801),
(363, 2, 1, '0.0200', 30, 1467553801, 1467553801),
(364, 3, 1, '0.4000', 30, 1467553801, 1467553801),
(365, 1, 1, '0.0200', 30, 1467553921, 1467553921),
(366, 2, 1, '0.0200', 30, 1467553921, 1467553921),
(367, 3, 1, '0.4000', 30, 1467553921, 1467553921),
(368, 1, 1, '0.0200', 30, 1467554041, 1467554041),
(369, 2, 1, '0.0200', 30, 1467554041, 1467554041),
(370, 3, 1, '0.4000', 30, 1467554041, 1467554041),
(371, 1, 1, '0.0200', 30, 1467554161, 1467554161),
(372, 2, 1, '0.0200', 30, 1467554161, 1467554161),
(373, 3, 1, '0.4000', 30, 1467554161, 1467554161),
(374, 1, 1, '0.0200', 30, 1467554281, 1467554281),
(375, 2, 1, '0.0200', 30, 1467554281, 1467554281),
(376, 3, 1, '0.4000', 30, 1467554281, 1467554281),
(377, 1, 1, '0.0200', 30, 1467554401, 1467554401),
(378, 2, 1, '0.0200', 30, 1467554401, 1467554401),
(379, 3, 1, '0.4000', 30, 1467554401, 1467554401),
(380, 1, 1, '0.0200', 30, 1467554522, 1467554522),
(381, 2, 1, '0.0200', 30, 1467554522, 1467554522),
(382, 3, 1, '0.4000', 30, 1467554522, 1467554522),
(383, 1, 1, '0.0200', 30, 1467554641, 1467554641),
(384, 2, 1, '0.0200', 30, 1467554641, 1467554641),
(385, 3, 1, '0.4000', 30, 1467554641, 1467554641),
(386, 1, 1, '0.0200', 30, 1467554761, 1467554761),
(387, 2, 1, '0.0200', 30, 1467554761, 1467554761),
(388, 3, 1, '0.4000', 30, 1467554761, 1467554761),
(389, 1, 1, '0.0200', 30, 1467554881, 1467554881),
(390, 2, 1, '0.0200', 30, 1467554881, 1467554881),
(391, 3, 1, '0.4000', 30, 1467554881, 1467554881),
(392, 1, 1, '0.0200', 30, 1467555001, 1467555001),
(393, 2, 1, '0.0200', 30, 1467555001, 1467555001),
(394, 3, 1, '0.4000', 30, 1467555001, 1467555001),
(395, 1, 1, '0.0200', 30, 1467555121, 1467555121),
(396, 2, 1, '0.0200', 30, 1467555121, 1467555121),
(397, 3, 1, '0.4000', 30, 1467555121, 1467555121),
(398, 1, 1, '0.0200', 30, 1467555241, 1467555241),
(399, 2, 1, '0.0200', 30, 1467555241, 1467555241),
(400, 3, 1, '0.4000', 30, 1467555241, 1467555241),
(401, 1, 1, '0.0200', 30, 1467555362, 1467555362),
(402, 2, 1, '0.0200', 30, 1467555362, 1467555362),
(403, 3, 1, '0.4000', 30, 1467555362, 1467555362),
(404, 1, 1, '0.0200', 30, 1467555481, 1467555481),
(405, 2, 1, '0.0200', 30, 1467555481, 1467555481),
(406, 3, 1, '0.4000', 30, 1467555481, 1467555481),
(407, 1, 1, '0.0200', 30, 1467555601, 1467555601),
(408, 2, 1, '0.0200', 30, 1467555602, 1467555602),
(409, 3, 1, '0.4000', 30, 1467555602, 1467555602),
(410, 1, 1, '0.0200', 30, 1467555721, 1467555721),
(411, 2, 1, '0.0200', 30, 1467555721, 1467555721),
(412, 3, 1, '0.4000', 30, 1467555721, 1467555721),
(413, 1, 1, '0.0200', 30, 1467555841, 1467555841),
(414, 2, 1, '0.0200', 30, 1467555841, 1467555841),
(415, 3, 1, '0.4000', 30, 1467555841, 1467555841),
(416, 1, 1, '0.0200', 30, 1467555961, 1467555961),
(417, 2, 1, '0.0200', 30, 1467555961, 1467555961),
(418, 3, 1, '0.4000', 30, 1467555961, 1467555961),
(419, 1, 1, '0.0200', 30, 1467556081, 1467556081),
(420, 2, 1, '0.0200', 30, 1467556081, 1467556081),
(421, 3, 1, '0.4000', 30, 1467556081, 1467556081),
(422, 1, 1, '0.0200', 30, 1467556202, 1467556202),
(423, 2, 1, '0.0200', 30, 1467556202, 1467556202),
(424, 3, 1, '0.4000', 30, 1467556202, 1467556202),
(425, 1, 1, '0.0200', 30, 1467556322, 1467556322),
(426, 2, 1, '0.0200', 30, 1467556322, 1467556322),
(427, 3, 1, '0.4000', 30, 1467556322, 1467556322),
(428, 1, 1, '0.0200', 30, 1467556442, 1467556442),
(429, 2, 1, '0.0200', 30, 1467556442, 1467556442),
(430, 3, 1, '0.4000', 30, 1467556442, 1467556442),
(431, 1, 1, '0.0200', 30, 1467556561, 1467556561),
(432, 2, 1, '0.0200', 30, 1467556561, 1467556561),
(433, 3, 1, '0.4000', 30, 1467556561, 1467556561),
(434, 1, 1, '0.0200', 30, 1467556681, 1467556681),
(435, 2, 1, '0.0200', 30, 1467556681, 1467556681),
(436, 3, 1, '0.4000', 30, 1467556681, 1467556681),
(437, 1, 1, '0.0200', 30, 1467556801, 1467556801),
(438, 2, 1, '0.0200', 30, 1467556801, 1467556801),
(439, 3, 1, '0.4000', 30, 1467556801, 1467556801),
(440, 1, 1, '0.0200', 30, 1467556922, 1467556922),
(441, 2, 1, '0.0200', 30, 1467556922, 1467556922),
(442, 3, 1, '0.4000', 30, 1467556922, 1467556922),
(443, 1, 1, '0.0200', 30, 1467557041, 1467557041),
(444, 2, 1, '0.0200', 30, 1467557041, 1467557041),
(445, 3, 1, '0.4000', 30, 1467557041, 1467557041),
(446, 1, 1, '0.0200', 30, 1467557161, 1467557161),
(447, 2, 1, '0.0200', 30, 1467557161, 1467557161),
(448, 3, 1, '0.4000', 30, 1467557161, 1467557161),
(449, 1, 1, '0.0200', 30, 1467557281, 1467557281),
(450, 2, 1, '0.0200', 30, 1467557281, 1467557281),
(451, 3, 1, '0.4000', 30, 1467557281, 1467557281),
(452, 1, 1, '0.0200', 30, 1467557401, 1467557401),
(453, 2, 1, '0.0200', 30, 1467557401, 1467557401),
(454, 3, 1, '0.4000', 30, 1467557401, 1467557401),
(455, 1, 1, '0.0200', 30, 1467557522, 1467557522),
(456, 2, 1, '0.0200', 30, 1467557522, 1467557522),
(457, 3, 1, '0.4000', 30, 1467557522, 1467557522),
(458, 1, 1, '0.0200', 30, 1467557641, 1467557641),
(459, 2, 1, '0.0200', 30, 1467557641, 1467557641),
(460, 3, 1, '0.4000', 30, 1467557641, 1467557641),
(461, 1, 1, '0.0200', 30, 1467557761, 1467557761),
(462, 2, 1, '0.0200', 30, 1467557761, 1467557761),
(463, 3, 1, '0.4000', 30, 1467557761, 1467557761),
(464, 1, 1, '0.0200', 30, 1467557881, 1467557881),
(465, 2, 1, '0.0200', 30, 1467557881, 1467557881),
(466, 3, 1, '0.4000', 30, 1467557881, 1467557881),
(467, 1, 1, '0.0200', 30, 1467558002, 1467558002),
(468, 2, 1, '0.0200', 30, 1467558002, 1467558002),
(469, 3, 1, '0.4000', 30, 1467558002, 1467558002),
(470, 1, 1, '0.0200', 30, 1467558121, 1467558121),
(471, 2, 1, '0.0200', 30, 1467558121, 1467558121),
(472, 3, 1, '0.4000', 30, 1467558121, 1467558121),
(473, 1, 1, '0.0200', 30, 1467558241, 1467558241),
(474, 2, 1, '0.0200', 30, 1467558241, 1467558241),
(475, 3, 1, '0.4000', 30, 1467558241, 1467558241),
(476, 1, 1, '0.0200', 30, 1467558362, 1467558362),
(477, 2, 1, '0.0200', 30, 1467558362, 1467558362),
(478, 3, 1, '0.4000', 30, 1467558362, 1467558362),
(479, 1, 1, '0.0200', 30, 1467558481, 1467558481),
(480, 2, 1, '0.0200', 30, 1467558481, 1467558481),
(481, 3, 1, '0.4000', 30, 1467558481, 1467558481),
(482, 1, 1, '0.0200', 30, 1467558601, 1467558601),
(483, 2, 1, '0.0200', 30, 1467558601, 1467558601),
(484, 3, 1, '0.4000', 30, 1467558601, 1467558601),
(485, 1, 1, '0.0200', 30, 1467558721, 1467558721),
(486, 2, 1, '0.0200', 30, 1467558721, 1467558721),
(487, 3, 1, '0.4000', 30, 1467558721, 1467558721),
(488, 1, 1, '0.0200', 30, 1467558841, 1467558841),
(489, 2, 1, '0.0200', 30, 1467558841, 1467558841),
(490, 3, 1, '0.4000', 30, 1467558841, 1467558841),
(491, 1, 1, '0.0200', 30, 1467558961, 1467558961),
(492, 2, 1, '0.0200', 30, 1467558961, 1467558961),
(493, 3, 1, '0.4000', 30, 1467558961, 1467558961),
(494, 1, 1, '0.0200', 30, 1467559081, 1467559081),
(495, 2, 1, '0.0200', 30, 1467559081, 1467559081),
(496, 3, 1, '0.4000', 30, 1467559081, 1467559081),
(497, 1, 1, '0.0200', 30, 1467559202, 1467559202),
(498, 2, 1, '0.0200', 30, 1467559202, 1467559202),
(499, 3, 1, '0.4000', 30, 1467559202, 1467559202),
(500, 1, 1, '0.0200', 30, 1467559321, 1467559321),
(501, 2, 1, '0.0200', 30, 1467559321, 1467559321),
(502, 3, 1, '0.4000', 30, 1467559321, 1467559321),
(503, 1, 1, '0.0200', 30, 1467559441, 1467559441),
(504, 2, 1, '0.0200', 30, 1467559441, 1467559441),
(505, 3, 1, '0.4000', 30, 1467559441, 1467559441),
(506, 1, 1, '0.0200', 30, 1467559561, 1467559561),
(507, 2, 1, '0.0200', 30, 1467559561, 1467559561),
(508, 3, 1, '0.4000', 30, 1467559561, 1467559561),
(509, 1, 1, '0.0200', 30, 1467559682, 1467559682),
(510, 2, 1, '0.0200', 30, 1467559682, 1467559682),
(511, 3, 1, '0.4000', 30, 1467559682, 1467559682),
(512, 1, 1, '0.0200', 30, 1467559802, 1467559802),
(513, 2, 1, '0.0200', 30, 1467559802, 1467559802),
(514, 3, 1, '0.4000', 30, 1467559802, 1467559802),
(515, 1, 1, '0.0200', 30, 1467559921, 1467559921),
(516, 2, 1, '0.0200', 30, 1467559921, 1467559921),
(517, 3, 1, '0.4000', 30, 1467559921, 1467559921),
(518, 1, 1, '0.0200', 30, 1467560041, 1467560041),
(519, 2, 1, '0.0200', 30, 1467560041, 1467560041),
(520, 3, 1, '0.4000', 30, 1467560041, 1467560041);

-- --------------------------------------------------------

--
-- Table structure for table `invest_customer_request`
--

CREATE TABLE IF NOT EXISTS `invest_customer_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned DEFAULT '0',
  `customer_username` varchar(255) DEFAULT NULL,
  `amount` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `bank_address` varchar(255) NOT NULL,
  `status` smallint(2) NOT NULL DEFAULT '1',
  `type` smallint(2) NOT NULL DEFAULT '1',
  `transaction_code` varchar(255) DEFAULT NULL,
  `approved_by` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `invest_customer_request`
--

INSERT INTO `invest_customer_request` (`id`, `customer_id`, `customer_username`, `amount`, `bank_address`, `status`, `type`, `transaction_code`, `approved_by`, `created_at`, `updated_at`) VALUES
(1, 1, 'test1', '10.0300', '16vxtbrYia59Y5tPk4ThtJzf6p6Vu82LPE', 99, 20, 'invest/requests/1/72e90d745a2e312d589997f2b6a37f53.jpg', 'admin', 1467538442, 1467542303),
(2, 2, 'test2', '2.5300', '16vxtbrYia59Y5tPk4ThtJzf6p6Vu82LPE', 1, 20, NULL, NULL, 1467538585, 1467538585),
(3, 5, 'test5', '1.0300', '16vxtbrYia59Y5tPk4ThtJzf6p6Vu82LPE', 1, 20, NULL, NULL, 1467538638, 1467538638),
(4, 12, 'conf1', '2.5300', '16vxtbrYia59Y5tPk4ThtJzf6p6Vu82LPE', 1, 20, NULL, NULL, 1467540234, 1467540234),
(5, 13, 'chauf1', '5.5300', '16vxtbrYia59Y5tPk4ThtJzf6p6Vu82LPE', 1, 20, NULL, NULL, 1467540378, 1467540378),
(6, 6, 'CapCha', '0.8071', '16vxtbrYia59Y5tPk4ThtJzf6p6Vu82LPE', 99, 1, 'admin/requests/1/b20c69f68df040ef3a04c8dcc343bd2c.jpg', 'admin', 1467542454, 1467542476);

-- --------------------------------------------------------

--
-- Table structure for table `invest_customer_transaction`
--

CREATE TABLE IF NOT EXISTS `invest_customer_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned DEFAULT '0',
  `customer_username` varchar(255) DEFAULT NULL,
  `bill_id` int(11) NOT NULL DEFAULT '0',
  `amount` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `sign` smallint(2) NOT NULL DEFAULT '1',
  `type` smallint(2) NOT NULL DEFAULT '1',
  `balance_before` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `balance_after` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `invest_customer_transaction`
--

INSERT INTO `invest_customer_transaction` (`id`, `customer_id`, `customer_username`, `bill_id`, `amount`, `sign`, `type`, `balance_before`, `balance_after`, `created_at`, `updated_at`) VALUES
(1, 6, 'CapCha', 1, '0.7108', 1, 13, '0.0000', '0.7108', 1467541505, 1467541505),
(2, 6, 'CapCha', 1, '0.1170', 1, 13, '0.7108', '0.8278', 1467542187, 1467542187),
(3, 1, 'test1', 1, '10.0000', 1, 20, '0.0000', '10.0000', 1467542303, 1467542303),
(4, 1, 'test1', 3, '10.0000', 2, 1, '10.0000', '0.0000', 1467542303, 1467542303),
(5, 6, 'CapCha', 6, '0.8071', 2, 10, '0.8278', '0.0207', 1467542476, 1467542476),
(6, 6, 'CapCha', 1, '1.7812', 1, 11, '0.0207', '1.8019', 1467542523, 1467542523);

-- --------------------------------------------------------

--
-- Table structure for table `invest_customer_wallet`
--

CREATE TABLE IF NOT EXISTS `invest_customer_wallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) unsigned NOT NULL,
  `balance` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000',
  `poundage` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000',
  `type` smallint(4) unsigned NOT NULL DEFAULT '10',
  `status` smallint(2) unsigned NOT NULL DEFAULT '1',
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_id` (`customer_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `invest_customer_wallet`
--

INSERT INTO `invest_customer_wallet` (`id`, `customer_id`, `balance`, `poundage`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 6, '1.8019', '0.0000', 10, 1, 1467538309, 1467542523),
(2, 1, '0.0000', '0.0000', 10, 1, 1467538442, 1467542303),
(3, 2, '0.0000', '0.0000', 10, 1, 1467538585, 1467538585),
(4, 5, '0.0000', '0.0000', 10, 1, 1467538638, 1467538638),
(5, 12, '0.0000', '0.0000', 10, 1, 1467540234, 1467540234),
(6, 13, '0.0000', '0.0000', 10, 1, 1467540378, 1467540378);

-- --------------------------------------------------------

--
-- Table structure for table `invest_package`
--

CREATE TABLE IF NOT EXISTS `invest_package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(255) NOT NULL,
  `amount_min` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `amount_max` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `increase_day` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `increase_month` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `branch_full` decimal(10,4) NOT NULL,
  `branch_short` decimal(10,4) NOT NULL,
  `matching` decimal(10,4) NOT NULL,
  `status` smallint(2) unsigned NOT NULL DEFAULT '1',
  `created_at` int(11) unsigned NOT NULL DEFAULT '0',
  `updated_at` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1000006 ;

--
-- Dumping data for table `invest_package`
--

INSERT INTO `invest_package` (`id`, `package_name`, `amount_min`, `amount_max`, `increase_day`, `increase_month`, `branch_full`, `branch_short`, `matching`, `status`, `created_at`, `updated_at`) VALUES
(1000000, 'P1', '0.5000', '1.5000', '2.0000', '0.0000', '15.0000', '5.0000', '1.5000', 1, 1467158976, 1467242113),
(1000001, 'P2', '2.5000', '2.5000', '3.0000', '0.0000', '15.0000', '7.0000', '3.0000', 1, 1467159239, 1467242264),
(1000002, 'P3', '5.5000', '5.5000', '3.5000', '0.0000', '15.0000', '10.0000', '4.0000', 1, 1467159274, 1467242305),
(1000003, 'P4', '10.0000', '10.0000', '4.0000', '0.0000', '15.0000', '13.0000', '5.5000', 1, 1467159302, 1467242341),
(1000005, 'P5', '15.0000', '25.0000', '5.5000', '0.0000', '15.0000', '15.0000', '7.0000', 1, 1467158976, 1467242113);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL,
  `default` int(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `lname`, `slug`, `code`, `icon`, `folder`, `status`, `order`, `default`, `created_at`, `updated_at`) VALUES
(1, 'Tiếng Việt', 'tieng-viet', 'vi', 'vi.png', 'vietnamese', 1, 1, 1, '2015-07-16 03:28:24', '2015-07-23 11:45:38'),
(2, 'English', 'english', 'en', 'en.png', 'english', 1, 2, 0, '2015-07-16 03:28:49', '2015-07-23 11:45:38');

-- --------------------------------------------------------

--
-- Table structure for table `log_action`
--

CREATE TABLE IF NOT EXISTS `log_action` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` smallint(6) unsigned DEFAULT '0',
  `params` text,
  `user_id` int(11) unsigned DEFAULT '0',
  `user_username` varchar(255) DEFAULT NULL,
  `user_type` smallint(1) unsigned DEFAULT '0',
  `user_ip` varchar(20) DEFAULT NULL,
  `object_id` int(11) unsigned DEFAULT '0',
  `object_table` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=121 ;

--
-- Dumping data for table `log_action`
--

INSERT INTO `log_action` (`id`, `type`, `params`, `user_id`, `user_username`, `user_type`, `user_ip`, `object_id`, `object_table`, `created_at`, `updated_at`) VALUES
(1, 1, '{"id":"1","username":"vinhhx","full_name":"Ho\\u00e0ng Xu\\u00e2n Vinh","sex":"1","phone":"0986118464","email":"vinhhx.register@gmail.com","date_join":"2016-06-18","created_at":1466187083}', 0, NULL, 2, NULL, 1, 'customer', 1466187083, 1466187083),
(2, 1, '{"id":"10","username":"vinhhx1","full_name":"hoang vinh","sex":"1","phone":"0987161773","email":"vinhhx.register01@gmail.com","date_join":"2016-06-18","created_at":1466189352}', 1, 'vinhhx', 2, '127.0.0.1', 10, 'customer', 1466189352, 1466189352),
(3, 1, '{"id":"11","username":"vinhhx2","full_name":"hoang vinh","sex":"1","phone":"0987161771","email":"vinhhx.register02@gmail.com","date_join":"2016-06-18","created_at":1466189498}', 10, 'vinhhx1', 2, '127.0.0.1', 11, 'customer', 1466189499, 1466189499),
(4, 1, '{"id":"12","username":"vinhhx3","full_name":"hoang vinh","sex":"1","phone":"0987161772","email":"vinhhx.register03@gmail.com","date_join":"2016-06-18","created_at":1466189655}', 11, 'vinhhx2', 2, '127.0.0.1', 12, 'customer', 1466189656, 1466189656),
(5, 1, '{"id":"13","username":"vinhhx4","full_name":"hoang vinh","sex":"1","phone":"0987161774","email":"vinhhx.register04@gmail.com","date_join":"2016-06-18","created_at":1466189823}', 12, 'vinhhx3', 2, '127.0.0.1', 13, 'customer', 1466189824, 1466189824),
(6, 1, '{"id":"14","username":"vinhhx5","full_name":"hoang vinh","sex":"1","phone":"0987161775","email":"vinhhx.register05@gmail.com","date_join":"2016-06-18","created_at":1466189967}', 13, 'vinhhx4', 2, '127.0.0.1', 14, 'customer', 1466189968, 1466189968),
(7, 10, '{"id":1,"customer_id":"14","package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"1000.0000","increase_amount":null,"total":null,"is_reinvestment":null,"reinvestment_date":null,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":"adsdsaddsamkdkdkdkkk12l1l2121l12kkk21","created_at":1466204341,"updated_at":1466204341,"balance":null}', 14, 'vinhhx5', 2, '127.0.0.1', 1, 'customer_package', 1466204342, 1466204342),
(8, 1, '{"id":"15","username":"test","full_name":"test","sex":"1","phone":"0986118425","email":"vinhhx.android@gmail.com","date_join":"2016-06-18","created_at":1466204541}', 0, NULL, 2, NULL, 15, 'customer', 1466204541, 1466204541),
(9, 10, '{"id":2,"customer_id":"15","package_id":1,"package_name":"BONZE-2016-06-18","package_amount":"1000.0000","increase_amount":null,"total":null,"is_reinvestment":null,"reinvestment_date":null,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":"zzcjdkklkc092dksld00923kdei8223","created_at":1466204683,"updated_at":1466204683,"balance":null}', 15, 'test', 2, '127.0.0.1', 2, 'customer_package', 1466204683, 1466204683),
(10, 10, '{"id":4,"customer_id":"14","package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"1000.0000","increase_amount":null,"total":null,"is_reinvestment":null,"reinvestment_date":1466232394,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":"adsdsaddsamkdkdkdkkk12l1l2121l12kkk21","created_at":1466232394,"updated_at":1466232394,"balance":null}', 14, 'vinhhx5', 2, '127.0.0.1', 4, 'customer_package', 1466232395, 1466232395),
(11, 10, '{"id":5,"customer_id":"14","package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"1000.0000","increase_amount":10,"total":null,"is_reinvestment":null,"reinvestment_date":1466232566,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":"adsdsaddsamkdkdkdkkk12l1l2121l12kkk21","created_at":1466232566,"updated_at":1466232566,"balance":null}', 14, 'vinhhx5', 2, '127.0.0.1', 5, 'customer_package', 1466232566, 1466232566),
(12, 10, '{"id":6,"customer_id":"15","package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"1000.0000","increase_amount":10,"total":null,"is_reinvestment":null,"reinvestment_date":1466232655,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":"zzcjdkklkc092dksld00923kdei8223","created_at":1466232655,"updated_at":1466232655,"balance":null}', 15, 'test', 2, '127.0.0.1', 6, 'customer_package', 1466232655, 1466232655),
(13, 10, '{"id":7,"customer_id":"14","package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"1000.0000","increase_amount":10,"total":null,"is_reinvestment":null,"reinvestment_date":1466286360,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":"adsdsaddsamkdkdkdkkk12l1l2121l12kkk21","created_at":1466286360,"updated_at":1466286360,"balance":null}', 14, 'vinhhx5', 2, '127.0.0.1', 7, 'customer_package', 1466286360, 1466286360),
(14, 10, '{"id":8,"customer_id":"13","package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"1000.0000","increase_amount":10,"total":null,"is_reinvestment":null,"reinvestment_date":1466311707,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466311707,"updated_at":1466311707,"balance":null}', 13, 'vinhhx4', 2, '127.0.0.1', 8, 'customer_package', 1466311707, 1466311707),
(15, 10, '{"id":9,"customer_id":"13","package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"1000.0000","increase_amount":10,"total":null,"is_reinvestment":null,"reinvestment_date":1466314768,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466314768,"updated_at":1466314768,"balance":null}', 13, 'vinhhx4', 2, '127.0.0.1', 9, 'customer_package', 1466314768, 1466314768),
(16, 10, '{"id":10,"customer_id":"13","package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"1000.0000","increase_amount":10,"total":null,"is_reinvestment":null,"reinvestment_date":1466315219,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466315219,"updated_at":1466315219,"balance":null}', 13, 'vinhhx4', 2, '127.0.0.1', 10, 'customer_package', 1466315219, 1466315219),
(17, 10, '{"id":11,"customer_id":"13","package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 4","package_amount":"1000.0000","increase_amount":10,"total":null,"is_reinvestment":null,"reinvestment_date":1466315551,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466315551,"updated_at":1466315551,"balance":null}', 13, 'vinhhx4', 2, '127.0.0.1', 11, 'customer_package', 1466315552, 1466315552),
(18, 10, '{"id":12,"customer_id":"13","package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 5","package_amount":"1000.0000","increase_amount":10,"total":null,"is_reinvestment":null,"reinvestment_date":1466315712,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466315712,"updated_at":1466315712,"balance":null}', 13, 'vinhhx4', 2, '127.0.0.1', 12, 'customer_package', 1466315712, 1466315712),
(19, 10, '{"id":13,"customer_id":"13","package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 6","package_amount":"1000.0000","increase_amount":10,"total":null,"is_reinvestment":null,"reinvestment_date":1466315931,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466315931,"updated_at":1466315931,"balance":null}', 13, 'vinhhx4', 2, '127.0.0.1', 13, 'customer_package', 1466315931, 1466315931),
(20, 10, '{"id":14,"customer_id":"13","package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 7","package_amount":"1000.0000","increase_amount":10,"total":null,"is_reinvestment":null,"reinvestment_date":1466316037,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466316037,"updated_at":1466316037,"balance":null}', 13, 'vinhhx4', 2, '127.0.0.1', 14, 'customer_package', 1466316038, 1466316038),
(21, 1, '{"id":1000000,"username":"khanhvx","full_name":"V\\u0169 Kh\\u00e1nh","sex":"1","phone":"0977704586","email":"vukhanh.dgr@gmail.com","date_join":"2016-06-19","created_at":1466319828}', 0, NULL, 2, NULL, 1000000, 'customer', 1466319828, 1466319828),
(22, 10, '{"id":1000000,"customer_id":1000000,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466319929,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466319929,"updated_at":1466319929,"balance":null}', 1000000, 'khanhvx', 2, '123.16.244.143', 1000000, 'customer_package', 1466319929, 1466319929),
(23, 1, '{"id":1000001,"username":"hoangnam","full_name":"Hoang Nam","sex":"1","phone":"0916806866","email":"vukhanh@dotlight.vn","date_join":"2016-06-19","created_at":1466320002}', 1000000, 'khanhvx', 2, '123.16.244.143', 1000001, 'customer', 1466320002, 1466320002),
(24, 10, '{"id":1000001,"customer_id":1000001,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466320087,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466320087,"updated_at":1466320087,"balance":null}', 1000001, 'hoangnam', 2, '123.16.244.143', 1000001, 'customer_package', 1466320087, 1466320087),
(25, 1, '{"id":1000002,"username":"duongnt","full_name":"\\u0110\\u01b0\\u01a1ng Kim","sex":"1","phone":"0123456789","email":"theduong88@gmail.com","date_join":"2016-06-19","created_at":1466320400}', 1000000, 'khanhvx', 2, '123.16.244.143', 1000002, 'customer', 1466320400, 1466320400),
(26, 1, '{"id":1000003,"username":"kimduong","full_name":"Kim \\u0110\\u01b0\\u01a1ng","sex":"1","phone":"0123487569","email":"romanticboykh@gmail.com","date_join":"2016-06-19","created_at":1466320483}', 0, NULL, 2, NULL, 1000003, 'customer', 1466320483, 1466320483),
(27, 1, '{"id":1000004,"username":"test1","full_name":"Test Nam","sex":"1","phone":"0321654789","email":"romanticboyh@gmail.com","date_join":"2016-06-19","created_at":1466320548}', 0, NULL, 2, NULL, 1000004, 'customer', 1466320548, 1466320548),
(28, 10, '{"id":1000002,"customer_id":1000003,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466320778,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466320778,"updated_at":1466320778,"balance":null}', 1000003, 'kimduong', 2, '123.16.244.143', 1000002, 'customer_package', 1466320778, 1466320778),
(29, 10, '{"id":1000003,"customer_id":1000003,"package_id":2,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"1.0000","increase_amount":25,"total":null,"is_reinvestment":null,"reinvestment_date":1466325797,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466325797,"updated_at":1466325797,"balance":null}', 1000003, 'kimduong', 2, '118.70.185.55', 1000003, 'customer_package', 1466325797, 1466325797),
(30, 10, '{"id":1000004,"customer_id":1000001,"package_id":2,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"1.0000","increase_amount":25,"total":null,"is_reinvestment":null,"reinvestment_date":1466325852,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466325852,"updated_at":1466325852,"balance":null}', 1000001, 'hoangnam', 2, '118.70.185.55', 1000004, 'customer_package', 1466325853, 1466325853),
(31, 10, '{"id":1000005,"customer_id":1000002,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466325928,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466325928,"updated_at":1466325928,"balance":null}', 1000002, 'duongnt', 2, '118.70.185.55', 1000005, 'customer_package', 1466325928, 1466325928),
(32, 1, '{"id":1000005,"username":"abc123","full_name":"abc123","sex":"1","phone":"0986117847","email":"abc123@gmail.com","date_join":"2016-06-19","created_at":1466348128}', 0, NULL, 2, NULL, 1000005, 'customer', 1466348128, 1466348128),
(33, 10, '{"id":1000006,"customer_id":1000005,"package_id":2,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"1.0000","increase_amount":25,"total":null,"is_reinvestment":null,"reinvestment_date":1466348299,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466348299,"updated_at":1466348299,"balance":null}', 1000005, 'abc123', 2, '117.1.161.37', 1000006, 'customer_package', 1466348299, 1466348299),
(34, 10, '{"id":1000007,"customer_id":1000004,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466348758,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466348758,"updated_at":1466348758,"balance":null}', 1000004, 'test1', 2, '42.113.97.10', 1000007, 'customer_package', 1466348758, 1466348758),
(35, 10, '{"id":1000008,"customer_id":1000004,"package_id":2,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"1.0000","increase_amount":25,"total":null,"is_reinvestment":null,"reinvestment_date":1466349998,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466349998,"updated_at":1466349998,"balance":null}', 1000004, 'test1', 2, '42.113.97.10', 1000008, 'customer_package', 1466349998, 1466349998),
(36, 10, '{"id":1000009,"customer_id":1000005,"package_id":4,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"5.0000","increase_amount":45,"total":null,"is_reinvestment":null,"reinvestment_date":1466350903,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466350903,"updated_at":1466350903,"balance":null}', 1000005, 'abc123', 2, '42.113.97.10', 1000009, 'customer_package', 1466350903, 1466350903),
(37, 10, '{"id":1000010,"customer_id":1000000,"package_id":2,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"1.0000","increase_amount":25,"total":null,"is_reinvestment":null,"reinvestment_date":1466353497,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466353497,"updated_at":1466353497,"balance":null}', 1000000, 'khanhvx', 2, '42.113.97.10', 1000010, 'customer_package', 1466353497, 1466353497),
(38, 10, '{"id":1000011,"customer_id":1000002,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466353789,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466353789,"updated_at":1466353789,"balance":null}', 1000002, 'duongnt', 2, '42.113.97.10', 1000011, 'customer_package', 1466353789, 1466353789),
(39, 10, '{"id":1000012,"customer_id":1000001,"package_id":3,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"2.0000","increase_amount":30,"total":null,"is_reinvestment":null,"reinvestment_date":1466353850,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466353850,"updated_at":1466353850,"balance":null}', 1000001, 'hoangnam', 2, '42.113.97.10', 1000012, 'customer_package', 1466353850, 1466353850),
(40, 1, '{"id":1,"username":"test1","full_name":"test1","sex":"1","phone":"0986118464","email":"vinhhx.register@gmail.com","date_join":"2016-06-20","created_at":1466360492}', 0, NULL, 2, NULL, 1, 'customer', 1466360492, 1466360492),
(41, 10, '{"id":1,"customer_id":1,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466360557,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466360557,"updated_at":1466360557,"balance":null}', 1, 'test1', 2, '117.1.161.37', 1, 'customer_package', 1466360557, 1466360557),
(42, 1, '{"id":2,"username":"test2","full_name":"test2","sex":"1","phone":"0986118465","email":"vinhhx.register01@gmail.com","date_join":"2016-06-20","created_at":1466360612}', 1, 'test1', 2, '117.1.161.37', 2, 'customer', 1466360612, 1466360612),
(43, 10, '{"id":2,"customer_id":2,"package_id":2,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"1.0000","increase_amount":25,"total":null,"is_reinvestment":null,"reinvestment_date":1466360673,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466360673,"updated_at":1466360673,"balance":null}', 2, 'test2', 2, '117.1.161.37', 2, 'customer_package', 1466360673, 1466360673),
(44, 1, '{"id":3,"username":"test3","full_name":"test3","sex":"1","phone":"0986118466","email":"vinhhx.register02@gmail.com","date_join":"2016-06-20","created_at":1466360710}', 2, 'test2', 2, '117.1.161.37', 3, 'customer', 1466360710, 1466360710),
(45, 10, '{"id":3,"customer_id":3,"package_id":4,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"5.0000","increase_amount":45,"total":null,"is_reinvestment":null,"reinvestment_date":1466360775,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466360775,"updated_at":1466360775,"balance":null}', 3, 'test3', 2, '117.1.161.37', 3, 'customer_package', 1466360775, 1466360775),
(46, 1, '{"id":4,"username":"test4","full_name":"test4","sex":"1","phone":"0986118467","email":"vinhhx.register03@gmail.com","date_join":"2016-06-20","created_at":1466360823}', 3, 'test3', 2, '117.1.161.37', 4, 'customer', 1466360823, 1466360823),
(47, 10, '{"id":4,"customer_id":4,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":null,"reinvestment_date":1466360882,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466360882,"updated_at":1466360882,"balance":null}', 4, 'test4', 2, '117.1.161.37', 4, 'customer_package', 1466360882, 1466360882),
(48, 1, '{"id":5,"username":"test5","full_name":"test5","sex":"1","phone":"0987161773","email":"vinhhx.register05@gmail.com","date_join":"2016-06-20","created_at":1466360927}', 4, 'test4', 2, '117.1.161.37', 5, 'customer', 1466360927, 1466360927),
(49, 10, '{"id":5,"customer_id":5,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":null,"reinvestment_date":1466360961,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466360961,"updated_at":1466360961,"balance":null}', 5, 'test5', 2, '117.1.161.37', 5, 'customer_package', 1466360961, 1466360961),
(50, 1, '{"id":6,"username":"CapCha","full_name":"C\\u1ea5p Cha","sex":"1","phone":"0912345678","email":"vukhanh.dgr@gmail.com","date_join":"2016-06-20","created_at":1466361599}', 0, NULL, 2, NULL, 6, 'customer', 1466361600, 1466361600),
(51, 1, '{"id":7,"username":"CapF1","full_name":"C\\u1ea5p F1","sex":"1","phone":"0912345458","email":"vukhanhvx@gmail.com","date_join":"2016-06-20","created_at":1466361813}', 0, NULL, 2, NULL, 7, 'customer', 1466361813, 1466361813),
(52, 1, '{"id":8,"username":"CapF2","full_name":"C\\u1ea5p F2","sex":"1","phone":"0915645678","email":"vukhanhkh@gmail.com","date_join":"2016-06-20","created_at":1466361925}', 0, NULL, 2, NULL, 8, 'customer', 1466361925, 1466361925),
(53, 1, '{"id":9,"username":"CapF3","full_name":"C\\u1ea5p F3","sex":"1","phone":"0915648458","email":"vukhanhhh@gmail.com","date_join":"2016-06-20","created_at":1466361982}', 0, NULL, 2, NULL, 9, 'customer', 1466361982, 1466361982),
(54, 1, '{"id":10,"username":"CapF4","full_name":"C\\u1ea5p F4","sex":"1","phone":"0975648458","email":"vuhanhkh@gmail.com","date_join":"2016-06-20","created_at":1466362026}', 0, NULL, 2, NULL, 10, 'customer', 1466362026, 1466362026),
(55, 1, '{"id":11,"username":"CapF5","full_name":"C\\u1ea5p F5","sex":"1","phone":"0975665458","email":"vuhanhht@gmail.com","date_join":"2016-06-20","created_at":1466362077}', 0, NULL, 2, NULL, 11, 'customer', 1466362077, 1466362077),
(56, 10, '{"id":6,"customer_id":6,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466362111,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466362111,"updated_at":1466362111,"balance":null}', 6, 'CapCha', 2, '42.113.97.10', 6, 'customer_package', 1466362111, 1466362111),
(57, 10, '{"id":7,"customer_id":7,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466362248,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466362248,"updated_at":1466362248,"balance":null}', 7, 'CapF1', 2, '42.113.97.10', 7, 'customer_package', 1466362248, 1466362248),
(58, 10, '{"id":8,"customer_id":8,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466362321,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466362321,"updated_at":1466362321,"balance":null}', 8, 'CapF2', 2, '42.113.97.10', 8, 'customer_package', 1466362321, 1466362321),
(59, 10, '{"id":9,"customer_id":10,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466362449,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466362449,"updated_at":1466362449,"balance":null}', 10, 'CapF4', 2, '42.113.97.10', 9, 'customer_package', 1466362449, 1466362449),
(60, 10, '{"id":10,"customer_id":9,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":null,"reinvestment_date":1466362664,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466362664,"updated_at":1466362664,"balance":null}', 9, 'CapF3', 2, '42.113.97.10', 10, 'customer_package', 1466362664, 1466362664),
(61, 10, '{"id":11,"customer_id":11,"package_id":2,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"1.0000","increase_amount":25,"total":null,"is_reinvestment":null,"reinvestment_date":1466362680,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466362680,"updated_at":1466362680,"balance":null}', 11, 'CapF5', 2, '42.113.97.10', 11, 'customer_package', 1466362680, 1466362680),
(62, 10, '{"id":12,"customer_id":6,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":null,"reinvestment_date":1466363510,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466363510,"updated_at":1466363510,"balance":null}', 6, 'CapCha', 2, '42.113.97.10', 12, 'customer_package', 1466363510, 1466363510),
(63, 10, '{"id":13,"customer_id":10,"package_id":4,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"5.0000","increase_amount":45,"total":null,"is_reinvestment":null,"reinvestment_date":1466363539,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466363539,"updated_at":1466363539,"balance":null}', 10, 'CapF4', 2, '42.113.97.10', 13, 'customer_package', 1466363539, 1466363539),
(64, 10, '{"id":14,"customer_id":11,"package_id":3,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"2.0000","increase_amount":30,"total":null,"is_reinvestment":null,"reinvestment_date":1466363556,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466363556,"updated_at":1466363556,"balance":null}', 11, 'CapF5', 2, '42.113.97.10', 14, 'customer_package', 1466363556, 1466363556),
(65, 10, '{"id":15,"customer_id":7,"package_id":2,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"1.0000","increase_amount":25,"total":null,"is_reinvestment":null,"reinvestment_date":1466366036,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466366036,"updated_at":1466366036,"balance":null}', 7, 'CapF1', 2, '42.113.97.10', 15, 'customer_package', 1466366036, 1466366036),
(66, 10, '{"id":16,"customer_id":8,"package_id":4,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"5.0000","increase_amount":45,"total":null,"is_reinvestment":null,"reinvestment_date":1466366082,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466366082,"updated_at":1466366082,"balance":null}', 8, 'CapF2', 2, '42.113.97.10', 16, 'customer_package', 1466366082, 1466366082),
(67, 10, '{"id":17,"customer_id":9,"package_id":3,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"2.0000","increase_amount":30,"total":null,"is_reinvestment":null,"reinvestment_date":1466366096,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466366096,"updated_at":1466366096,"balance":null}', 9, 'CapF3', 2, '42.113.97.10', 17, 'customer_package', 1466366096, 1466366096),
(68, 10, '{"id":18,"customer_id":1,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":null,"reinvestment_date":1466367652,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466367652,"updated_at":1466367652,"balance":null}', 1, 'test1', 2, '42.113.97.10', 18, 'customer_package', 1466367652, 1466367652),
(69, 10, '{"id":19,"customer_id":4,"package_id":4,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"5.0000","increase_amount":45,"total":null,"is_reinvestment":null,"reinvestment_date":1466367677,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466367677,"updated_at":1466367677,"balance":null}', 4, 'test4', 2, '42.113.97.10', 19, 'customer_package', 1466367677, 1466367677),
(70, 10, '{"id":20,"customer_id":2,"package_id":3,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"2.0000","increase_amount":30,"total":null,"is_reinvestment":null,"reinvestment_date":1466367710,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466367710,"updated_at":1466367710,"balance":null}', 2, 'test2', 2, '42.113.97.10', 20, 'customer_package', 1466367710, 1466367710),
(71, 10, '{"id":21,"customer_id":3,"package_id":4,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"5.0000","increase_amount":45,"total":null,"is_reinvestment":null,"reinvestment_date":1466367725,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466367725,"updated_at":1466367725,"balance":null}', 3, 'test3', 2, '42.113.97.10', 21, 'customer_package', 1466367725, 1466367725),
(72, 10, '{"id":22,"customer_id":5,"package_id":4,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"5.0000","increase_amount":45,"total":null,"is_reinvestment":null,"reinvestment_date":1466367740,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466367740,"updated_at":1466367740,"balance":null}', 5, 'test5', 2, '42.113.97.10', 22, 'customer_package', 1466367740, 1466367740),
(73, 10, '{"id":23,"customer_id":5,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":null,"reinvestment_date":1466368704,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466368704,"updated_at":1466368704,"balance":null}', 5, 'test5', 2, '42.113.97.10', 23, 'customer_package', 1466368704, 1466368704),
(74, 10, '{"id":24,"customer_id":2,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":null,"reinvestment_date":1466369367,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466369367,"updated_at":1466369367,"balance":null}', 2, 'test2', 2, '42.113.97.10', 24, 'customer_package', 1466369367, 1466369367),
(75, 10, '{"id":6,"customer_id":6,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"10.0000","increase_amount":65,"total":"0.0000","is_reinvestment":1,"reinvestment_date":1466384608,"status":1,"time_sent_completed":1466365837,"is_active":10,"type":0,"block_chain_wallet_id":null,"created_at":1466362111,"updated_at":1466384608,"balance":"0.0000"}', 6, 'CapCha', 2, '42.113.97.10', 6, 'customer_package', 1466384608, 1466384608),
(76, 1, '{"id":12,"username":"conf1","full_name":"Con F1","sex":"1","phone":"0123456789","email":"test@gmail.com","date_join":"2016-06-21","created_at":1466494498}', 0, NULL, 2, NULL, 12, 'customer', 1466494498, 1466494498),
(77, 10, '{"id":25,"customer_id":12,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1466494765,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466494765,"updated_at":1466494765,"balance":null}', 12, 'conf1', 2, '113.23.7.53', 25, 'customer_package', 1466494765, 1466494765),
(78, 1, '{"id":13,"username":"chauf1","full_name":"Ch\\u00e1u f1","sex":"1","phone":"0213546789","email":"chauf1@gmail.com","date_join":"2016-06-21","created_at":1466498311}', 0, NULL, 2, NULL, 13, 'customer', 1466498311, 1466498311),
(79, 10, '{"id":26,"customer_id":13,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1466498360,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466498360,"updated_at":1466498360,"balance":null}', 13, 'chauf1', 2, '113.23.7.53', 26, 'customer_package', 1466498360, 1466498360),
(80, 1, '{"id":14,"username":"testvn","full_name":"anh","sex":"1","phone":"01223105698","email":"lljjlll@gmail.com","date_join":"2016-06-21","created_at":1466501867}', 0, NULL, 2, NULL, 14, 'customer', 1466501867, 1466501867),
(81, 10, '{"id":27,"customer_id":14,"package_id":1,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 1","package_amount":"0.5000","increase_amount":20,"total":null,"is_reinvestment":0,"reinvestment_date":1466502367,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466502367,"updated_at":1466502367,"balance":null}', 14, 'testvn', 2, '113.23.8.56', 27, 'customer_package', 1466502367, 1466502367),
(82, 10, '{"id":28,"customer_id":14,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1466503503,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466503503,"updated_at":1466503503,"balance":null}', 14, 'testvn', 2, '113.23.8.56', 28, 'customer_package', 1466503503, 1466503503),
(83, 1, '{"id":15,"username":"testvn1","full_name":"testvn1","sex":"1","phone":"01233105698","email":"dhfgjghk@gmail.com","date_join":"2016-06-21","created_at":1466504021}', 0, NULL, 2, NULL, 15, 'customer', 1466504021, 1466504021),
(84, 10, '{"id":29,"customer_id":1,"package_id":2,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"1.0000","increase_amount":25,"total":null,"is_reinvestment":1,"reinvestment_date":1466930949,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1466930949,"updated_at":1466930949,"balance":null}', 1, 'test1', 2, '117.1.117.166', 29, 'customer_package', 1466930949, 1466930949),
(85, 1, '{"id":16,"username":"admin2","full_name":"Admin 2","sex":"1","phone":"0123456798","email":"vukhanh.dgr1@gmail.com","date_join":"2016-06-27","created_at":1467000230}', 0, NULL, 1, NULL, 16, 'customer', 1467000230, 1467000230),
(86, 1, '{"id":17,"username":"admin1","full_name":"hoang vinh","sex":"1","phone":"0986118458","email":"vinhhx.register0099@gmail.com","date_join":"2016-06-27","created_at":1467000262}', 1, 'admin', 1, '118.70.144.70', 17, 'customer', 1467000262, 1467000262),
(87, 10, '{"id":30,"customer_id":7,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467087132,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467087132,"updated_at":1467087132,"balance":null}', 7, 'CapF1', 2, '113.23.7.53', 30, 'customer_package', 1467087133, 1467087133),
(88, 10, '{"id":31,"customer_id":9,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467088062,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467088062,"updated_at":1467088062,"balance":null}', 9, 'CapF3', 2, '113.23.7.53', 31, 'customer_package', 1467088062, 1467088062),
(89, 10, '{"id":32,"customer_id":12,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467089045,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467089045,"updated_at":1467089045,"balance":null}', 12, 'conf1', 2, '113.23.7.53', 32, 'customer_package', 1467089045, 1467089045),
(90, 10, '{"id":33,"customer_id":7,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 4","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467089188,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467089188,"updated_at":1467089188,"balance":null}', 7, 'CapF1', 2, '113.23.7.53', 33, 'customer_package', 1467089188, 1467089188),
(91, 10, '{"id":34,"customer_id":6,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467090625,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467090625,"updated_at":1467090625,"balance":null}', 6, 'CapCha', 2, '113.23.7.53', 34, 'customer_package', 1467090625, 1467090625),
(92, 10, '{"id":35,"customer_id":12,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 3","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467090737,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467090737,"updated_at":1467090737,"balance":null}', 12, 'conf1', 2, '113.23.7.53', 35, 'customer_package', 1467090737, 1467090737),
(93, 10, '{"id":36,"customer_id":13,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 2","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":0,"reinvestment_date":1467092663,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467092663,"updated_at":1467092663,"balance":null}', 13, 'chauf1', 2, '113.23.7.53', 36, 'customer_package', 1467092663, 1467092663),
(94, 1, '{"id":18,"username":"abc123345","full_name":"hoang vinh","sex":"1","phone":"09871617733","email":"vinhhx.register808@gmail.com","date_join":"2016-06-28","created_at":1467098035}', 0, NULL, 2, NULL, 18, 'customer', 1467098035, 1467098035),
(95, 1, '{"id":19,"username":"chatf1","full_name":"Ch\\u1eaft F1","sex":"1","phone":"0354268970","email":"test8845@gmail.com","date_join":"2016-06-28","created_at":1467098401}', 0, NULL, 2, NULL, 19, 'customer', 1467098401, 1467098401),
(96, 1, '{"id":20,"username":"test123","full_name":"Test 123","sex":"1","phone":"0912345670","email":"test123@gmail.com","date_join":"2016-06-30","created_at":1467278316}', 0, NULL, 2, NULL, 20, 'customer', 1467278316, 1467278316),
(97, 1, '{"id":21,"username":"test10","full_name":"test10","sex":"1","phone":"0986552244","email":"abc123@gmail.com","date_join":"2016-06-30","created_at":1467300976}', 0, NULL, 2, NULL, 21, 'customer', 1467300977, 1467300977),
(98, 1, '{"id":22,"username":"test20","full_name":"test20","sex":"1","phone":"0986445824","email":"123@gmail.com","date_join":"2016-07-01","created_at":1467329964}', 0, NULL, 2, NULL, 22, 'customer', 1467329965, 1467329965),
(99, 1, '{"id":23,"username":"test101","full_name":"test101","sex":"1","phone":"0986554144","email":"123443@gmail.com","date_join":"2016-07-01","created_at":1467345535}', 0, NULL, 2, NULL, 23, 'customer', 1467345535, 1467345535),
(100, 1, '{"id":24,"username":"tet102","full_name":"test102","sex":"1","phone":"0986114585","email":"123456@gmail.com","date_join":"2016-07-01","created_at":1467351247}', 0, NULL, 2, NULL, 24, 'customer', 1467351247, 1467351247),
(101, 1, '{"id":25,"username":"test102","full_name":"test102","sex":"1","phone":"0986558553","email":"vnhhh@gmail.com","date_join":"2016-07-01","created_at":1467351418}', 0, NULL, 2, NULL, 25, 'customer', 1467351418, 1467351418),
(102, 1, '{"id":26,"username":"test1010","full_name":"test1010","sex":"1","phone":"0987558544","email":"2335ee@gmail.com","date_join":"2016-07-01","created_at":1467353697}', 0, NULL, 2, NULL, 26, 'customer', 1467353697, 1467353697),
(103, 1, '{"id":27,"username":"test1020","full_name":"test1020","sex":"1","phone":"0984458528","email":"ddddd@gmail.com","date_join":"2016-07-01","created_at":1467353888}', 0, NULL, 2, NULL, 27, 'customer', 1467353889, 1467353889),
(104, 1, '{"id":28,"username":"test101010","full_name":"test101010","sex":"1","phone":"0321654897","email":"test101010@gmail.com","date_join":"2016-07-01","created_at":1467367333}', 0, NULL, 2, NULL, 28, 'customer', 1467367333, 1467367333),
(105, 1, '{"id":29,"username":"test101011","full_name":"test101011","sex":"1","phone":"0321645987","email":"test101011lk@gmail.com","date_join":"2016-07-01","created_at":1467369540}', 0, NULL, 2, NULL, 29, 'customer', 1467369540, 1467369540),
(106, 1, '{"id":30,"username":"test101012","full_name":"test101012","sex":"1","phone":"0977704585","email":"test101012@gmail.com","date_join":"2016-07-01","created_at":1467370312}', 0, NULL, 2, NULL, 30, 'customer', 1467370312, 1467370312),
(107, 10, '{"id":37,"customer_id":1,"package_id":5,"package_name":"G\\u00f3i \\u0111\\u1ea7u t\\u01b0 s\\u1ed1 4","package_amount":"10.0000","increase_amount":65,"total":null,"is_reinvestment":1,"reinvestment_date":1467372276,"status":1,"time_sent_completed":null,"is_active":null,"type":null,"block_chain_wallet_id":null,"created_at":1467372276,"updated_at":1467372276,"balance":null}', 1, 'test1', 2, '1.55.96.70', 37, 'customer_package', 1467372276, 1467372276),
(108, 1, '{"id":31,"username":"test10101","full_name":"test10101","sex":"1","phone":"0321456987","email":"test10101@gmail.com","date_join":"2016-07-01","created_at":1467389722}', 0, NULL, 2, NULL, 31, 'customer', 1467389722, 1467389722),
(109, 1, '{"id":32,"username":"capcon","full_name":"capcon","sex":"1","phone":"0123452289","email":"capcontest1@gmail.com","date_join":"2016-07-02","created_at":1467450089}', 0, NULL, 2, NULL, 32, 'customer', 1467450089, 1467450089),
(110, 1, '{"id":33,"username":"capcon02","full_name":"capcon02","sex":"1","phone":"0931684448","email":"capcon02te@gmail.com","date_join":"2016-07-02","created_at":1467450932}', 0, NULL, 2, NULL, 33, 'customer', 1467450933, 1467450933),
(111, 1, '{"id":34,"username":"capcon01","full_name":"capcon01","sex":"1","phone":"0354263370","email":"capcon01test@gmail.com","date_join":"2016-07-02","created_at":1467451238}', 0, NULL, 2, NULL, 34, 'customer', 1467451239, 1467451239),
(112, 1, '{"id":35,"username":"capcon011","full_name":"capcon011","sex":"1","phone":"0913456978","email":"capcon011@gmail.com","date_join":"2016-07-02","created_at":1467451423}', 0, NULL, 2, NULL, 35, 'customer', 1467451423, 1467451423),
(113, 1, '{"id":36,"username":"capcon021","full_name":"capcon021","sex":"1","phone":"0213654789","email":"capcon021test@gmail.com","date_join":"2016-07-02","created_at":1467453186}', 0, NULL, 2, NULL, 36, 'customer', 1467453186, 1467453186),
(114, 1, '{"id":37,"username":"capchaf1","full_name":"capchaf1","sex":"1","phone":"0124563798","email":"capchaf1@gmail.com","date_join":"2016-07-03","created_at":1467527827}', 0, NULL, 2, NULL, 37, 'customer', 1467527827, 1467527827),
(115, 1, '{"id":38,"username":"capchaf11","full_name":"capchaf11","sex":"1","phone":"0931689277","email":"capchaf11@gmail.com","date_join":"2016-07-03","created_at":1467528893}', 0, NULL, 2, NULL, 38, 'customer', 1467528893, 1467528893),
(116, 1, '{"id":39,"username":"capchaf2","full_name":"capchaf2","sex":"1","phone":"0123456669","email":"capchaf2@gmail.com","date_join":"2016-07-03","created_at":1467529298}', 0, NULL, 2, NULL, 39, 'customer', 1467529298, 1467529298),
(117, 1, '{"id":40,"username":"capchaf22","full_name":"capchaf22","sex":"1","phone":"03251647977","email":"capchaf22@gmail.com","date_join":"2016-07-03","created_at":1467530213}', 0, NULL, 2, NULL, 40, 'customer', 1467530213, 1467530213),
(118, 1, '{"id":41,"username":"capchaf3","full_name":"capchaf3","sex":"1","phone":"0512346789","email":"capchaf3@gmail.com","date_join":"2016-07-03","created_at":1467534046}', 0, NULL, 2, NULL, 41, 'customer', 1467534046, 1467534046),
(119, 1, '{"id":42,"username":"capchaf33","full_name":"capchaf33","sex":"1","phone":"0987321456","email":"capchaf33@gmail.com","date_join":"2016-07-03","created_at":1467534260}', 0, NULL, 2, NULL, 42, 'customer', 1467534260, 1467534260),
(120, 1, '{"id":43,"username":"capchaf1101","full_name":"capchaf1101","sex":"1","phone":"0925123648","email":"capchaf1101@gmail.com","date_join":"2016-07-03","created_at":1467535694}', 0, NULL, 2, NULL, 43, 'customer', 1467535694, 1467535694);

-- --------------------------------------------------------

--
-- Table structure for table `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1448563630),
('m140506_102106_rbac_init', 1448563767),
('m151203_034102_add_coloumn_to_customer', 1449229602),
('m151204_013758_add_comlumn_customer_case', 1449229605),
('m151212_022108_add_feild_parent_id_to_customer', 1450052549),
('m151214_005549_log_action_table', 1450054827),
('m151214_183241_create_table_request_cash_out', 1450118322),
('m160104_014233_modify_column_amount_to_decimal_add_column_is_deleted', 1452825177),
('m160104_040745_modify_amount_to_decimal', 1452825177),
('m160115_023024_add_column', 1452825177),
('m160115_075806_modify_table_parent_username_to_customer_username', 1452845211),
('m160202_082049_add_column_out_date_to_request_cashout', 1454401579),
('m160214_080914_add_column_to_requirecashout', 1456455998),
('m160226_024541_create_table_customer_loan', 1456540065),
('m160227_023932_add_column_created_by_to_table_customer_loan', 1456540846),
('m160301_035326_create_table_email', 1456804577),
('m160531_023640_create_table_wallet', 1465250968),
('m160603_034305_add_column_customer_indentity_card_and_customer_ref', 1465251207),
('m160607_020822_modify_datetime_to_customer', 1465312599),
('m160608_073135_update_table_customer_add_column_is_active', 1465500619),
('m160609_080122_add_column_package_amount_to_customer_package', 1465500623),
('m160609_205647_create_table_customer_activity', 1465506313),
('m160610_152443_create_table_transaction_queue', 1465594375),
('m160610_221321_add_block_chain_wallet_id_to_customer_package', 1465596937),
('m160611_004529_add_date_start_to_queue', 1465606038),
('m160611_005105_add_column_customer_request_id_to_queue', 1465618171),
('m160611_031924_re_create_table_customerTransaction', 1465618172),
('m160611_225247_add_customer_request_block_chain_wallet_id', 1465685935),
('m160612_160352_create_news_category', 1465861609),
('m160612_160359_create_news', 1465861609),
('m160612_160409_create_video', 1465861610),
('m160613_021152_create_table_customer_bank_address', 1465861610),
('m160613_024025_add_column_transaction_type_and_transaction_step', 1466001826),
('m160613_234732_create_table_token', 1465862028),
('m160614_001619_alter_customer_id_is_null_for_customer_token', 1465863476),
('m160615_014059_add_column_request_id_to_transaction', 1466001827),
('m160615_022613_add_column_type_step_to_transaction_queue', 1466001827),
('m160615_051749_alter_customer_transaction', 1466001829),
('m160615_063431_alter_package_amount_to_number', 1466001830),
('m160615_065132_add_customer_username_to_table_transaction_queue', 1466001830),
('m160615_072538_modify_sent_queue_id', 1466001831),
('m160615_082223_add_column_time_sent_completed', 1466001831),
('m160615_144215_add_column_balance_to_customer_package', 1466001832),
('m160616_002122_create_table_customer_poundage', 1466036824),
('m160616_141647_add_column_poundage_level_to_customer', 1466088936),
('m160616_160433_alter_table_name_poundage_to_poundadge_transaction', 1466094783),
('m160616_220627_alter_parent_id_customer_case', 1466114996),
('m160617_000537_add_column_package_name_to_customer', 1466122075),
('m160618_074945_modify_balance', 1466236323),
('m160619_031710_add_column_min_max_to_queue', 1466306394),
('m160619_044607_change_require_wallet_block_chain', 1466311687);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `news_category_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `intro` text,
  `content` text,
  `image` varchar(255) DEFAULT NULL,
  `status` smallint(1) DEFAULT '1',
  `deleted` smallint(1) DEFAULT '0',
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `news_category`
--

CREATE TABLE IF NOT EXISTS `news_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `intro` text,
  `image` varchar(255) DEFAULT NULL,
  `status` smallint(1) DEFAULT '1',
  `deleted` smallint(1) DEFAULT '0',
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `note`
--

CREATE TABLE IF NOT EXISTS `note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `object_table` varchar(100) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `type` smallint(2) DEFAULT '1',
  `created_id` int(11) DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `note`
--

INSERT INTO `note` (`id`, `object_id`, `object_table`, `content`, `type`, `created_id`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 31, 'customer_transaction', 'Note save money', 1, 1, 'test1', 1466825793, 1466825793),
(2, 31, 'customer_transaction', 'ok thank test1', 1, 1, 'test1', 1466825877, 1466825877),
(3, 27, 'customer_transaction', 'ố kề', 1, 1, 'test1', 1466826389, 1466826389),
(4, 27, 'customer_transaction', 'tesst message', 1, 1, 'test1', 1466827156, 1466827156),
(5, 30, 'customer_transaction', '11255', 1, 3, 'test3', 1466827303, 1466827303),
(6, 46, 'customer_transaction', 'Cảm ơn bạn đã xác nhận', 1, 2, 'test2', 1467022006, 1467022006),
(7, 46, 'customer_transaction', 'không có gì', 1, 16, 'admin2', 1467022059, 1467022059),
(8, 47, 'customer_transaction', 'toi da chuyen', 1, 5, 'test5', 1467029060, 1467029060),
(9, 47, 'customer_transaction', 'ok', 1, 16, 'admin2', 1467029118, 1467029118),
(10, 42, 'customer_transaction', 'tesst ok', 1, 1, 'test1', 1467219735, 1467219735);

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE IF NOT EXISTS `package` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_name` varchar(45) DEFAULT NULL,
  `amount` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `status` smallint(2) unsigned DEFAULT NULL,
  `created_at` int(10) unsigned DEFAULT NULL,
  `updated_at` int(10) unsigned DEFAULT NULL,
  `increase_percent` smallint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `package_name_UNIQUE` (`package_name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`id`, `package_name`, `amount`, `status`, `created_at`, `updated_at`, `increase_percent`) VALUES
(1, 'BONZE', '0.5000', 1, 1449235961, 1449236742, 20),
(2, 'SILVER', '1.0000', 1, 1449235996, 1449236759, 25),
(3, 'GOLD', '2.0000', 1, 1449236022, 1449236774, 30),
(4, 'PLATINUM', '5.0000', 1, 1449236054, 1449236786, 45),
(5, 'DIAMOND', '10.0000', 1, 1449236083, 1449236800, 65);

-- --------------------------------------------------------

--
-- Table structure for table `package_config`
--

CREATE TABLE IF NOT EXISTS `package_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `package_id` int(11) DEFAULT NULL,
  `increase_percent` int(11) DEFAULT NULL,
  `status` smallint(2) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `INCREASE_PERCENT` (`increase_percent`,`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `re_calculate_poundage_queue`
--

CREATE TABLE IF NOT EXISTS `re_calculate_poundage_queue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `params` text,
  `status` smallint(2) NOT NULL DEFAULT '1',
  `created_at` int(11) NOT NULL DEFAULT '0',
  `updated_at` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `re_calculate_poundage_queue`
--

INSERT INTO `re_calculate_poundage_queue` (`id`, `params`, `status`, `created_at`, `updated_at`) VALUES
(1, '[1,6]', 10, 1466365377, 1466365377),
(2, '[1,6]', 10, 1466365583, 1466365583),
(3, '[1,6]', 10, 1466365618, 1466365618),
(4, '[1]', 10, 1466365837, 1466365837),
(5, '[1,6]', 10, 1466365966, 1466365966),
(6, '[1]', 10, 1466365998, 1466365998),
(7, '[1,6]', 10, 1466366008, 1466366008),
(8, '[1,6]', 10, 1466366176, 1466366176),
(9, '[1,6]', 10, 1466366772, 1466366772),
(10, '[1,6]', 10, 1466366851, 1466366851),
(11, '[1,6]', 10, 1466367402, 1466367402),
(12, '[1,6]', 10, 1466368242, 1466368242),
(13, '[1,2]', 10, 1466369037, 1466369037),
(14, '[1,2,3,4]', 10, 1466369610, 1466369610),
(15, '[1,6,7]', 10, 1466498193, 1466498193),
(16, '[1,6]', 10, 1466503429, 1466503429),
(17, '[1]', 10, 1467002903, 1467002903),
(18, '[1]', 10, 1467021976, 1467021976),
(19, '[1,2,3,4]', 10, 1467029108, 1467029108),
(20, '[1,2,3]', 10, 1467086378, 1467086378),
(21, '[1,6]', 10, 1467087452, 1467087452),
(22, '[1]', 10, 1467087555, 1467087555),
(23, '[1,6]', 10, 1467089417, 1467089417),
(24, '[1,6,7]', 10, 1467089664, 1467089664),
(25, '[1,6,7,12]', 10, 1467090404, 1467090404),
(26, '[1,6]', 10, 1467090496, 1467090496),
(27, '[1]', 10, 1467091629, 1467091629),
(28, '[1,6,7]', 10, 1467092627, 1467092627),
(29, '[1,6,7,12]', 10, 1467092787, 1467092787);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(150) NOT NULL,
  `auth_key` varchar(45) NOT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `status` smallint(6) DEFAULT NULL,
  `branch` smallint(6) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Bảng user hệ thống' AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `full_name`, `email`, `phone`, `status`, `branch`, `created_at`, `updated_at`) VALUES
(1, 'admin', '8VJPn1viHVjwi2M6acjmrPP_7VVCpNCK', '$2y$13$6BMTlSBSBlFL05T/KwkUW.0l5zyhjJ99YIMhoJJjuF2PaFiB8eDiq', NULL, NULL, 'vinhhx.register@gmail.com', '0986118464', 1, NULL, 1448886309, 1448886309),
(2, 'minhhp', 'kN4sBdb-DvoDVhADPNatT3AoewGabJuF', '$2y$13$ODNrQ6RotYZEG2IF/6kPZOVIk1ZTb6MeKJvr71vvTgqTIXm.iLLz2', NULL, NULL, 'test@gmail.com', NULL, 1, NULL, 1450389006, 1450389006),
(4, 'minhnguyen', 'vlcy7-ytFBgUcldPcGeKVQObP7pfQ42g', '$2y$13$w1epFCuVYcXY2.r19/MWGu8x7ISiRcmnc.z57COxxV0kPVXsIxAjq', NULL, 'Nguyen Duc Minh', '123@gmail.com', '0986118464', 1, NULL, 1450389178, 1450389527),
(5, 'cungnv', 'MvbYsyopsW7cM5-NCmdHz9UGQGqhz8xv', '$2y$13$Up7fw9WQ3x.XHBCdEzXsw.ZS5WR6oE0MZMI7c5PKkyDfdZiheCkVK', NULL, '', 'vancung295@gmail.com', '', 1, NULL, 1452223697, 1452223697);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE IF NOT EXISTS `video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `video_url` varchar(255) NOT NULL,
  `video_id` varchar(25) DEFAULT NULL,
  `use_thumb` smallint(1) DEFAULT '0',
  `video_thumb` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` smallint(1) DEFAULT '1',
  `deleted` smallint(1) DEFAULT '0',
  `created_at` int(10) DEFAULT NULL,
  `updated_at` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `title`, `alias`, `video_url`, `video_id`, `use_thumb`, `video_thumb`, `image`, `status`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 'video', 'video', 'https://www.youtube.com/watch?v=j-_seScw1vk', 'j-_seScw1vk', 1, 'http://img.youtube.com/vi/j-_seScw1vk/0.jpg', NULL, 1, 0, 1466495406, 1466495406),
(2, 'video 2 ', 'video-2', 'https://www.youtube.com/watch?v=j-_seScw1vk', 'j-_seScw1vk', 1, 'http://img.youtube.com/vi/j-_seScw1vk/0.jpg', NULL, 1, 0, 1466495413, 1466495413),
(3, 'video 3', 'video-3', 'https://www.youtube.com/watch?v=j-_seScw1vk', 'j-_seScw1vk', 1, 'http://img.youtube.com/vi/j-_seScw1vk/0.jpg', NULL, 1, 0, 1466495423, 1466495423);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
