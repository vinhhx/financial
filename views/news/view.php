<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = Html::encode($model->title);
?>
<div class="container">

    <?php
    /**
     * @var $model \app\models\News
     */
    if ($model):
        ?>
        <div class="title-category">
            <h1><?= $model->title ?></h1>
        </div>
        <p><?= $model->content ?></p>
    <?php endif; ?>
</div>