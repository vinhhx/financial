<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
?>
<div class="container">
    <div class="title-category">
        <h1>NEWS</h1>
    </div>

    <?php
    /**
     * @var $item \app\models\News
     */
    foreach ($models as $item):
        ?>
        <div class="news-list">
            <div class="row">
                <div class="col-lg-3">
                    <?= Html::a(Html::img($item->getImage()), ['/news/view', 'id' => $item->id]) ?>
                </div>
                <div class="col-lg-9">
                    <h4>
                        <?= Html::a($item->title, ['/news/view', 'id' => $item->id]) ?>
                    </h4>
                    <p><?= Html::encode($item->intro) ?></p>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
    <?php echo \yii\widgets\LinkPager::widget([
        'pagination' => $pages,
    ]); ?>
</div>