<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login" style="text-align: center">
    <h1 class="title-login"><?= Html::encode($this->title) ?></h1>

    <div class="login-form-border">
        <?php $form = ActiveForm::begin([
            'id' => 'login-form',
            'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'template' => "<div>{input}</div>",
                'labelOptions' => ['class' => 'col-lg-1 control-label'],
            ],
        ]); ?>

        <?= $form->field($model, 'username')->textInput(['placeholder' => 'Username / Email']) ?>

        <?= $form->field($model, 'password')->passwordInput()->textInput(['placeholder' => 'Password']) ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"text-left\">{input} {label}</div>",
        ]) ?>

        <div class="form-group">
            <?= Html::submitButton('Clear', ['class' => 'btn', 'name' => 'login-button', 'style' => 'background-color:#959595; color:#fff']) ?>
            <?= Html::submitButton('Submit', ['class' => 'btn', 'name' => 'login-button', 'style' => 'background-color:#f44336; color:#fff']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
