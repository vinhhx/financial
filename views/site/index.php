<?php
use \yii\helpers\Html;
use app\models\Content;
use yii\helpers\StringHelper;
use app\models\Config;

/* @var $this yii\web\View */
/* @var $videoModels \app\models\Video[] */
/* @var $contents [] */

$this->title = 'Bitoness';
?>
<div class="header-info">
    <div class="container" style="padding: 10px 15px;">
        <div class="slide-icon-ensign">
            <?php for ($i = 1; $i < 21; $i++): ?>
                <a style="padding-right: 5px;">
                    <img src="/images/ensign/ensign-<?= $i ?>.png">
                </a>
            <?php endfor; ?>
            <?php for ($i = 1; $i < 21; $i++): ?>
                <a style="padding-right: 5px;">
                    <img src="/images/ensign/ensign-<?= $i ?>.png">
                </a>
            <?php endfor; ?>
        </div>
    </div>
</div>
<div class="banner-home" style="background-color: #787878;">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php
            $i = 0;
            foreach ($modelBanners as $modelBanner):
                $i++;
                ?>
                <div class="item <?= $i == 1 ? 'active' : '' ?>">
                    <?= Html::a(Html::img($modelBanner->getImage()), $modelBanner->url) ?>
                </div>
            <?php endforeach; ?>
        </div>

        <!-- Left and right controls -->
        <!--<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"
           style="width: auto;">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"
           style="width: auto;">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>-->
    </div>
</div>

<div class="home-text-static">
    <div class="container cleafix">
        <div class="pull-right">
            <?= Html::a(Yii::t('app', 'Join now'), '/customer', ['class' => 'btn btn-button-leftjoin']) ?>
        </div>
        <div class="text-center">
            <?= Yii::t('app', 'Connection & Sharing') ?>
        </div>
    </div>
</div>
<?php if (isset($contents[Content::TYPE_ID_WHY_CHOICE_US])): ?>
    <div class="home-why-choice-us">
        <div class="container">
            <h2><?= Yii::t('app', 'Why choice us') ?></h2>

            <div class="row">
                <?php $w = 0; ?>
                <?php foreach ($contents[Content::TYPE_ID_WHY_CHOICE_US] as $why): ?>
                    <?php $w++;
                    if ($w > 5) break; ?>
                    <div class="col-md-2">
                        <a href="<?= \yii\helpers\Url::to(['content/view', 'id' => $why->id]) ?>" class="link-hover"
                           title="<?= strip_tags($why->content) ?>">
                            <div class="ycs-icon">
                                <i class="fa <?= $why->config ?>"></i>
                            </div>
                            <?= Html::encode($why->name) ?>
                        </a>
                    </div>
                <?php endforeach; ?>
                <div class="col-md-2">
                    <a href="" class="link-hover">
                        <div class="ycs-icon">
                            <i class="fa fa-info-circle"></i>
                        </div>
                        More >>
                    </a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="home-bieudo">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div style="text-align: center;">
                    <h2><?= Yii::t('app', 'Market statistics') ?></h2>

                    <p><?= Yii::t('app', 'Around the world') ?></p>
                </div>
                <!--<div class="content">
                    <?/*= Yii::t('app', 'Curabitur laoreet fringilla lorem ipsum porta. Nullam rutrum velit. Maecenas sit amet tincidunt
                    elit. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis
                    egestas') */?>
                </div>-->
                <div>
                    <div class="progress-wrap">
                        <?php
                        $listConfig = Config::getConfigByType(Config::TYPE_MAKET_STATICTICS);
                        foreach ($listConfig as $itemConfig):
                            if ($listStatictics = explode("\n", $itemConfig->content)):
                                foreach ($listStatictics as $statictic):
                                    $detail = explode(':', $statictic);
                                    if ($detail && isset($detail[0]) && isset($detail[1])):
                                        ?>
                                        <div><?= $detail[0] ?>: <?= $detail[1] ?></div>
                                        <div class="progress">
                                            <span data-width="<?= $detail[1] ?>"></span>
                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </div>
                </div>
                <!--<img src="../images/home-image-2.jpg">-->
            </div>
            <div class="col-md-7" style="text-align: center;">
                <h2><?= Yii::t('app', 'Live Transaction') ?></h2>

                <p><?= Yii::t('app', 'Current PH to GH transaction in Oursite') ?></p>
                <ul class="transaction-list" id="customer-transaction-statictics" style="overflow: hidden">
                    <?php
                    $listTransactions = Config::getConfigByType(Config::TYPE_HOME_TRANSACTION);
                    foreach ($listTransactions as $itemTransaction):
                        if ($listTransaction = explode(';', $itemTransaction->content)):?>
                            <li>
                                <div class="row">
                                    <div class="col-md-3">
                                        <img class="transaction-icon-person" src="/images/icon-person.png?v=0.0.1">
                                        <div class="transaction-id">
                                            <a><?= isset($listTransaction[3]) ? $listTransaction[3] : '' ?></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-transfer">
                                            Transfer <a><?= isset($listTransaction[0]) ? $listTransaction[0] : 1 ?>
                                                BTC</a> <?= isset($listTransaction[1]) ? $listTransaction[1] : '' ?>
                                        </div>
                                        <div class="text-address">
                                            <?= isset($listTransaction[2]) ? $listTransaction[2] : '' ?>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <img class="transaction-icon-person" style="float:right;"
                                             src="/images/icon-person.png?v=0.0.1">
                                        <div class="transaction-id" style="float:right; padding-right: 10px;">
                                            <a><?= isset($listTransaction[4]) ? $listTransaction[4] : '' ?></a>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php endif;
                    endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>

<?php if (isset($contents[Content::TYPE_ID_INTRO])): ?>
    <div class="home-post-content">
        <div class="container">
            <div class="row">
                <?php $m = 0; ?>
                <?php foreach ($contents[Content::TYPE_ID_INTRO] as $intro): ?>
                    <?php $m++;
                    if ($m > 3) break; ?>
                    <div class="col-md-4">
                        <div class="intro-item">
                            <span class="intro-icon">
                                <i class="fa <?= $intro->config ?>"></i>
                            </span>

                            <h3><?= Html::encode($intro->name) ?></h3>

                            <p><?= StringHelper::truncateWords($intro->content, 50) ?></p>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if ($videoModels): ?>
    <div class="home-clip">
        <div class="container" style="text-align: center; padding-bottom: 30px;">
            <h2 style="margin-bottom: 30px"><?= Yii::t('app', 'REFERENCE VIDEO CLIP') ?></h2>
            <div class="row">
                <?php foreach ($videoModels as $video): ?>
                    <div class="col-md-4">
                        <a href="#" class="play-video" data-toggle="modal" data-target=".video-modal-sm"
                           data-item="<?= $video->video_id ?>">
                            <?= $video->getImage() ? Html::img($video->getImage()) : '' ?>
                        </a>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="modal fade video-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
        <div class="modal-dialog">
            <div class="modal-content">
                ...
            </div>
        </div>
    </div>
    <?php
    $js = <<< JS
    $(function() {
        var modal = $('.video-modal-sm');
        $('.play-video').each(function(){
            var o = $(this);
            o.click(function() {
                var c = $('.modal-content', modal),
                videoFrame = '<iframe width="100%" height="350" src="https://www.youtube.com/embed/'+o.data('item')+'?autoplay=1" frameborder="0" allowfullscreen style="margin-bottom: -10px;"></iframe>';
                c.html(videoFrame);
                modal.modal('show');
                return false;
            });
        });
        
        modal.on('hidden.bs.modal', function (e) {
            $('.modal-content', modal).html('');
        });
    });
JS;
    $this->registerJs($js);
    ?>
<?php endif; ?>

<div class="last-block">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <h4 class="block-heading">NEWS</h4>
                <?php if ($modelNews): ?>
                    <h4 class="block-heading" style="font-size: 18px; text-align: left">
                        <?= Html::a($modelNews->title, ['/news/view', 'id' => $modelNews->id]) ?>
                    </h4>
                    <div class="row">
                        <div class="col-lg-5 new-image">
                            <?= Html::a(Html::img($modelNews->getImage()), ['/news/view', 'id' => $modelNews->id]) ?>
                        </div>
                        <div class="col-lg-7 new-content">
                            <?= Html::encode($modelNews->intro) ?>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-lg-6">
                <h4 class="block-heading" style="text-transform: uppercase">Verified payments systems</h4>
                <div class="row home-list-payment">
                    <div class="col-lg-4">
                        <a href="https://perfectmoney.is/" target="_blank">
                            <img src="/images/payment/payment-1.jpg">
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a href="https://www.bitcoin.com/" target="_blank">
                            <img src="/images/payment/payment-2.jpg">
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a href="https://payeer.com/en/" target="_blank">
                            <img src="/images/payment/payment-3.jpg">
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a href="https://blockchain.info/" target="_blank">
                            <img src="/images/payment/payment-4.jpg">
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a href="https://www.nixmoney.com/" target="_blank">
                            <img src="/images/payment/payment-5.jpg">
                        </a>
                    </div>
                    <div class="col-lg-4">
                        <a href="http://advcash.com/" target="_blank">
                            <img src="/images/payment/payment-6.jpg">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="home-clip">
    <div class="container" style="text-align: center; padding-bottom: 30px;">
        <h2 style="margin-bottom: 30px">VALUED PARTNERS</h2>
        <div class="row">
            <div class="slide-banner-partner">
                <a href="https://bter.com/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-1.png">
                </a>
                <a href="https://www.bitbond.com/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-2.png">
                </a>
                <a href="https://www.bitnexo.com/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-3.png">
                </a>
                <a href="https://bitnet.io/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-4.png">
                </a>
                <a href="https://www.bitpagos.com/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-5.png">
                </a>
                <a href="https://blockchain.info/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-6.png">
                </a>
                <a href="http://www.btcc.net/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-7.png">
                </a>
                <a href="https://bter.com/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-1.png">
                </a>
                <a href="https://www.bitbond.com/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-2.png">
                </a>
                <a href="https://www.bitnexo.com/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-3.png">
                </a>
                <a href="https://bitnet.io/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-4.png">
                </a>
                <a href="https://www.bitpagos.com/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-5.png">
                </a>
                <a href="https://blockchain.info/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-6.png">
                </a>
                <a href="http://www.btcc.net/" target="_blank" style="padding-right: 5px;">
                    <img src="/images/partner/partner-7.png">
                </a>
            </div>
        </div>
    </div>
</div>
<?php
$js = <<< EOD
    $(document).ready(function () {
        $('.slide-icon-ensign').slick({
            slidesToShow: 20,
            arrows: false,
            slidesToScroll: 1,
            dots: false,
            speed: 50,
            autoplay: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 10,
                        arrows: false,
                        slidesToScroll: 1,
                        dots: false,
                        speed: 50,
                        autoplay: true,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 5,
                        arrows: false,
                        slidesToScroll: 1,
                        dots: false,
                        speed: 50,
                        autoplay: true,
                    }
                },
            ]
        });
        $('.slide-banner-partner').slick({
            slidesToShow: 7,
            arrows: false,
            slidesToScroll: 1,
            dots: false,
            speed: 50,
            autoplay: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 5,
                        arrows: false,
                        slidesToScroll: 1,
                        dots: false,
                        speed: 50,
                        autoplay: true,
                    }
                },
                {
                    breakpoint: 600,
                    settings: {
                        slidesToShow: 2,
                        arrows: false,
                        slidesToScroll: 1,
                        dots: false,
                        speed: 50,
                        autoplay: true,
                    }
                },
            ]
        });
        $('#customer-transaction-statictics').slick({
            slidesToShow: 5,
            arrows: false,
            slidesToScroll: 1,
            dots: false,
            speed: 100,
            autoplay: true,
            vertical: true,
        });
    });
    (function($){
        $.fn.progressBar = function(){
            var o = this;
            var c = o.children();
            var g = o.offset().top - $(window).height();
            var magic = function() {
                c.each(function(){
                    var d = $(this);
                    var p = $('span', d);
                    p.width(p.data('width'));
                });
            }
            $('body').scroll(function(){
                var pos = $(this).scrollTop();
                if(!o.hasClass('rendered') && pos > g){
                    o.addClass('rendered');
                    magic();
                }
            });
            magic();
        }
    })(jQuery);

    $('.progress-wrap').progressBar();
EOD;
$this->registerJs($js);
?>
<style>
    .progress-wrap {
        font-size: 11px;
    }

    .progress {
        height: 10px;
        background-color: #e0e0e0;
        border-radius: 5px;
        display: inline-block;
        width: 100%;
        overflow: hidden;
        margin-top: 5px;
    }

    .progress > span {
        width: 0%;
        background-color: #0099cc;
        height: 10px;
        display: inline-block;
        float: left;
        transition: width 0.5s ease;
    }

    .new-title {
        margin: 10px 0;
    }

    .new-title a {
        color: #666666;
    }

    .new-image img {
        max-width: 100%;;
    }

    .home-list-payment {
        margin: 10px 0;
    }

    .home-list-payment > div {
        margin-bottom: 10px;
        text-align: center;
    }
</style>
