<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\models\Config;

?>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <?php if ($configDetail = Config::getConfigByType(Config::TYPE_FOOTER_ABOUT)) : ?>
                    <h4 class="footer-heading"><?= $configDetail[0]->title ?></h4>
                    <div style="color: #d6d6d6; padding-bottom:15px;">
                        <?= $configDetail[0]->content ?>
                    </div>
                <?php endif; ?>
                <div class="social-links">
                    <?php if ($configSocial = Config::getConfigByType(Config::TYPE_FOOTER_SOCIAL)) :
                        $detailSocial = explode("\n", $configSocial[0]->content);
                        foreach ($detailSocial as $detail) {
                            $social = explode('|', $detail);
                            echo Html::a('<i class="fa fa-' . $social[0] . '"></i>', $social[1], ['target' => '_blank']);
                        }
                        ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-lg-4">
                <?php
                if ($configDetail = Config::getConfigByType(Config::TYPE_FOOTER_CONTACT)) :
                    $detailContact = explode("\n", $configDetail[0]->content);
                    ?>
                    <h4 class="footer-heading"><?= $configDetail[0]->title ?></h4>
                    <?php
                    foreach ($detailContact as $i => $detail) {
                        switch ($i) {
                            case 0:
                                $icon = '<i class="glyphicon glyphicon-map-marker"></i>';
                                break;
                            case 1:
                                $icon = '<i class="glyphicon glyphicon-phone"></i>';
                                break;
                            case 2:
                                $icon = '<i class="glyphicon glyphicon-envelope"></i>';
                                break;
                            case 3:
                                $icon = '<i class="glyphicon glyphicon-time"></i>';
                                break;
                            default:
                                $icon = '';
                                break;
                        }
                        ?>
                        <div class="footer-list-info">
                            <?= $icon . ' ' . $detail ?>
                        </div>
                    <?php } ?>
                <?php endif; ?>
            </div>
            <!--<div class="col-lg-4">
                <h4 class="footer-heading">Helpful links</h4>
                <div class="row">
                    <?php
            /*                    $modelMenuFooter = \app\models\Menu::getItemByParentId(0, \app\models\Menu::TYPE_MENU_BOTTOM);
                                foreach ($modelMenuFooter as $menu): */ ?>
                        <div class="col-lg-6">
                            <div class="footer-list-info">
                                <a href="<? /*= $menu->link */ ?>"><? /*= $menu->title */ ?></a>
                            </div>
                        </div>
                    <?php /*endforeach; */ ?>
                </div>
            </div>-->
            <div class="col-lg-4">
                <div class="row">
                    <?php for ($i = 1; $i <= 8; $i++) { ?>
                        <div class="col-lg-3" style="margin-bottom: 10px;">
                            <a>
                                <img src="/images/footer/logo-footer-<?= $i ?>.jpg">
                            </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            Copyright © 2010 Bitoness - All Rights Reserved.
        </div>
    </div>
</footer>
<style>
    .social-links a {
        margin-right: 3px;
    }
</style>