<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Nav;
use app\models\Config;

$this->registerCssFile('@web/css/slick.css', ['depends' => \app\assets\AppAsset::className()]);
$this->registerCssFile('@web/css/slick-theme.css', ['depends' => \app\assets\AppAsset::className()]);
$this->registerJsFile('@web/js/slick.min.js', ['depends' => \app\assets\AppAsset::className()]);
?>
<div style="background-color: #f4f4f4">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <a href="/">
                    <img src="/images/logo.png">
                </a>
            </div>
            <?php
            $listConfig = Config::getConfigByType(Config::TYPE_HEADER_STATICTICS);
            $i = 0;
            foreach ($listConfig as $itemConfig):
                $i++;
                switch ($i) {
                    case 1:
                        $linkIcon = '/images/icon-member.png';
                        $classColor = 'member';
                        break;
                    case 2:
                        $linkIcon = '/images/icon-exchange.png';
                        $classColor = 'exchange';
                        break;
                    case 3:
                        $linkIcon = '/images/icon-bitcoin.png';
                        $classColor = 'bitcoin';
                        break;
                    case 4:
                        $linkIcon = '/images/icon-bitcoin-2.png';
                        $classColor = 'bitcoin-2';
                        break;
                    default:
                        $linkIcon = '';
                        $classColor = '';
                        break;
                }
                //$number = explode('|', $itemConfig->content);

                /**
                 *
                 * Tính số thành viên, số tiền theo config
                 */
                //$configStatic = '1359636|0.5|1|0.75|2|1.5|0.1|0.3';
                $configStatic = $itemConfig->content;
                $configArray = explode('|', $configStatic);
                $startTime = 1469358938;
                $totalDay = ceil((time() - $startTime) / 46800);
                $startNumber = isset($configArray[0]) ? $configArray[0] : 0;
                $total = count($configArray);
                $currentNumber = $startNumber;
                for ($dk = 1; $dk < $totalDay; $dk++) {
                    if ($dk >= $total) {
                        $id = ($dk % $total);
                        if ($id == 0) {
                            $id = 1;
                        }
                    } else {
                        $id = $dk;
                    }
                    if (isset($configArray[$id])) {
                        $currentNumber += ceil(($currentNumber * $configArray[$id]));
                    }
                }
                ?>
                <div class="col-md-2">
                    <div class="header-transaction">
                        <div class="pull-left">
                            <img src="<?= $linkIcon ?>">
                        </div>
                        <div class="<?= $classColor ?>">
                            <div
                                class="text-big"><?= $i != 1 ? '<i class="fa fa-btc"></i>' : '' ?><?= $currentNumber ?></div>
                            <div class="text-small"><?= $itemConfig->title ?></div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<div class="header-logo clearfix">
    <div class="container">
        <nav class="navbar nav-pills">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav header-menu-top">
                        <?php
                        $modelMenus = \app\models\Menu::getItemByParentId();
                        $itemMenu = [];
                        foreach ($modelMenus as $menu) {
                            if ($menuChild = \app\models\Menu::getItemByParentId($menu->id)) {
                                ?>
                                <li>
                                    <a href="<?= $menu->link ?>" class="dropdown-toggle" data-toggle="dropdown"
                                       role="button"
                                       aria-haspopup="true" aria-expanded="false"><?= $menu->title ?> <span
                                            class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <?php
                                        foreach ($menuChild as $menuC) {
                                            ?>
                                            <li><a href="<?= $menuC->link ?>"><?= $menuC->title ?></a></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                </li>
                                <?php
                            } else {
                                ?>
                                <li>
                                    <a href="<?= $menu->link ?>"><?= $menu->title ?></a>
                                </li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
</div>